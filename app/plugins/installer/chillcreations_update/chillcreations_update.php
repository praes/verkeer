<?php
	/**
	 * Handle updates for Chill Creations extensions (ccNewsletter, cciDEAL, ccInvoices)
	 * @package     Joomla.Plugin
	 * @subpackage  Installer.ChillCreations
	 * @author     Chill Creations <info@chillcreations.com>
	 * @link     http://www.chillcreations.com
	 * @copyright    Copyright (C) Chill Creations, see COPYRIGHT.php for details.
	 * @license    GNU/GPL, see LICENSE.php for full license.
	 * Based on code by nikosdion and shumisha on github -- Thank you!
	 **/

	defined('_JEXEC') or die;

	class PlgInstallerChillCreations_update extends JPlugin
	{
		/**
		 * Handle adding credentials to package download request
		 * @param   string  $url        url from which package is going to be downloaded
		 * @param   array   $headers    headers to be sent along the download request (key => value format)
		 * @return  boolean true if credentials have been added to request or not, false otherwise (credentials not set by user)
		 * @since   2.5
		 */
		public function onInstallerBeforePackageDownload(&$url, &$headers)
		{
			$uri = JUri::getInstance($url);
			$dlid = NULL;

			// Trigger plugin for all extensions that communicate to chillcreations.com
			$host = $uri->getHost();
			if ($host != "www.chillcreations.com")
			{
				return true;
			}

			// Get all possible download ID's
			$db = JFactory::getDBO();

			// Check whether extensions are installed (in extensions database table)
			$db->setQuery("SELECT enabled FROM #__extensions WHERE element = 'com_ccnewsletter'");
			$ccnewsletter_enabled = $db->loadResult();

			$db->setQuery("SELECT enabled FROM #__extensions WHERE element = 'com_ccidealplatform'");
			$ccideal_enabled = $db->loadResult();

			$db->setQuery("SELECT enabled FROM #__extensions WHERE element = 'com_ccinvoices'");
			$ccinvoices_enabled = $db->loadResult();

			$ccnewsletter_dlid = null;
			$ccideal_dlid = null;
			$ccinvoices_dlid = null;

			if ($ccnewsletter_enabled==1)
			{
				$query = "SELECT download_id FROM #__ccnewsletter_configuration" ;
				$db->setQuery($query);
				$ccnewsletter_dlid = $db->loadResult();
			}

			if ($ccideal_enabled==1)
			{
				$query = "SELECT download_id FROM #__ccidealplatform_config" ;
				$db->setQuery($query);
				$ccideal_dlid = $db->loadResult();
			}

			if ($ccinvoices_enabled==1)
			{
				$query = "SELECT download_id FROM #__ccinvoices_configuration" ;
				$db->setQuery($query);
				$ccinvoices_dlid = $db->loadResult();
			}

			// Set $dlid, but only if it's not empty and looks like an actual download ID
			if (preg_match('/^([0-9]{1,}:)?[0-9a-f]{32}$/i', $ccnewsletter_dlid)) 	{$dlid = $ccnewsletter_dlid;}
			if (preg_match('/^([0-9]{1,}:)?[0-9a-f]{32}$/i', $ccideal_dlid)) 		{$dlid = $ccideal_dlid;}
			if (preg_match('/^([0-9]{1,}:)?[0-9a-f]{32}$/i', $ccinvoices_dlid))		{$dlid = $ccinvoices_dlid;}

			// If the download ID is invalid, ret//urn without any further action
			if (!preg_match('/^([0-9]{1,}:)?[0-9a-f]{32}$/i', $dlid)){return true;}

			// Appent the Download ID to the download URL
			if (!empty($dlid))
			{
				$uri->setVar('dlid', $dlid);
				$url = $uri->toString();
			}
			return true;

		}


	}