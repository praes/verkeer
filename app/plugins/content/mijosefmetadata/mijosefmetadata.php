<?php
/**
* @version		1.0.0
* @package		MijoSEF
* @subpackage	MijoSEF
* @copyright	2009-2014 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @license		2009-2012 GNU/GPL based on AceSEF joomace.net
*/

// No Permission
defined('_JEXEC') or die('Restricted access');

// Imports

class plgContentMijosefmetadata extends JPlugin {

	public function __construct(& $subject, $config) {
		parent::__construct($subject, $config);
		
		$factory_file = JPATH_ADMINISTRATOR.'/components/com_mijosef/library/mijosef.php';

		if (file_exists($factory_file)) {
			require_once($factory_file);
			
			if (!class_exists('MijoDatabase')) {
				require_once(JPATH_ADMINISTRATOR.'/components/com_mijosef/library/database.php');
			}
			
			require_once(JPATH_ADMINISTRATOR.'/components/com_mijosef/library/metadata.php');
			require_once(JPATH_ADMINISTRATOR.'/components/com_mijosef/library/utility.php');
			
			$this->MijosefConfig = Mijosef::getConfig();
		}
	}

    public function onContentPrepareForm($form, $article) {
        if (!self::_systemCheckup(true)) {
            return;
        }

        if (!($form instanceof JForm))
        {
            $this->_subject->setError('JERROR_NOT_A_FORM');
            return false;
        }

        // Check we are manipulating a valid form.
        $name = $form->getName();

        if (!in_array($name, array('com_content.article'))) {
            return true;
        }

        JFactory::getLanguage()->load('com_mijosef');

        // Add the registration fields to the form.
        JForm::addFormPath(dirname(__FILE__).'/fields');

        $form->loadFile('mijosefmetadata', false);

        $url_1 = "index.php?option=com_content";

        // Get item id
        $item_id = isset($article->id) ? $article->id : 0;
        $url_2 = "id={$item_id}&view=article";
        $url_3 = "format=pdf";

        // Get row
        $url = MijoDatabase::loadResult("SELECT url_sef FROM #__mijosef_urls WHERE url_real LIKE '{$url_1}%' AND url_real LIKE '%{$url_2}%' AND url_real NOT LIKE '%{$url_3}%'");

        if ($url && !Mijosef::get('utility')->JoomfishInstalled()) {
            $row = MijoDatabase::loadObject("SELECT id, url_sef, title, description, keywords, lang, robots, googlebot FROM #__mijosef_metadata WHERE url_sef = '{$url}'");

            if (!$row) {
                $row = new stdClass();
                $row->id = 0;
                $row->url_sef = $url;
                $row->title = '';
                $row->description = '';
                $row->keywords = '';
                $row->lang = '';
                $row->robots = '';
                $row->googlebot = '';
            }
        }

        if (!empty($row)) {
			$article->attribs['mijosef_id'] = $row->id;
			$article->attribs['mijosef_url_sef'] = $row->url_sef;
			//$article->attribs['mijosef_title'] = Mijosef::get('utility')->replaceSpecialChars(htmlspecialchars($row->title), true);
			$article->attribs['mijosef_title'] = Mijosef::get('utility')->replaceSpecialChars($row->title, true);
			$article->attribs['mijosef_desc'] = Mijosef::get('utility')->replaceSpecialChars($row->description, true);
			$article->attribs['mijosef_key'] = Mijosef::get('utility')->replaceSpecialChars($row->keywords, true);
			$article->attribs['mijosef_lang'] = Mijosef::get('utility')->replaceSpecialChars($row->lang, true);
			$article->attribs['mijosef_robots'] = Mijosef::get('utility')->replaceSpecialChars($row->robots, true);
			$article->attribs['mijosef_googlebot'] = Mijosef::get('utility')->replaceSpecialChars($row->googlebot, true);
		}
        return true;
    }

    function onContentAfterSave($context, $article, $isNew) {
		if ($isNew) {
			return true;
		}
		
		// Check we are manipulating a valid form.
        if (!in_array($context, array('com_content.article'))) {
			return true;
		}
		
		if (!self::_systemCheckup()) {
			return true;
		}

        $attribs = json_decode($article->attribs);
		
		$id 			= $attribs->mijosef_id;
        $url_sef 		= $attribs->mijosef_url_sef;
        $title 			= Mijosef::get('utility')->replaceSpecialChars($attribs->mijosef_title);
        $description 	= Mijosef::get('utility')->replaceSpecialChars($attribs->mijosef_desc);
        $keywords 		= Mijosef::get('utility')->replaceSpecialChars($attribs->mijosef_key);
        $lang 			= $attribs->mijosef_lang;
        $robots 	    = $attribs->mijosef_robots;
        $googlebot 		= $attribs->mijosef_googlebot;
		
		if ($id == 0) {
			MijoDatabase::query("INSERT IGNORE INTO #__mijosef_metadata (url_sef, title, description, keywords, lang, robots, googlebot) VALUES('{$url_sef}', '{$title}', '{$description}', '{$keywords}', '{$lang}', '{$robots}', '{$googlebot}')");
		}
		else {
			MijoDatabase::query("UPDATE #__mijosef_metadata SET title = '{$title}', description = '{$description}', keywords = '{$keywords}', lang = '{$lang}', robots = '{$robots}', googlebot = '{$googlebot}' WHERE id = {$id}");
		}
	}

    public function _systemCheckup($layout = false) {
		// Is backend
		$mainframe = JFactory::getApplication();
		if (!$mainframe->isAdmin()) {
			return false;
		}

		// Joomla SEF is disabled
		if (!JFactory::getConfig()->get('sef')) {
			return false;
		}

		// Check if MijoSEF is enabled
		if ($this->MijosefConfig->mode == 0) {
			return false;
		}
		
		// Is plugin enabled
		if (!JPluginHelper::isEnabled('system', 'mijosef')) {
			return false;
		}
		
		// Is plugin enabled
		if (!JPluginHelper::isEnabled('content', 'mijosefmetadata')) {
			return false;
		}
		
		// Is com_content
		if (JRequest::getCmd('option') != 'com_content') {
			return false;
		}
		
		// Is edit page
		if ($layout && JRequest::getCmd('layout') != 'edit') {
			return false;
		}
		
		return true;
	}
}