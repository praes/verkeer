<?php


/**
* Menu parameters plugin
* @Copyright (C) 2015 Praes.nl
* @ All rights reserved
* @ Joomla! is Free Software
* @ Released under GNU/GPL License : http://www.gnu.org/copyleft/gpl.html
**/


defined( '_JEXEC' ) or die();

jimport( 'joomla.plugin.plugin' );

jimport('joomla.application.module.helper');

jimport( 'joomla.event.plugin' );

jimport( 'joomla.html.parameter' );





class plgSystemPrExtendedMenu extends JPlugin {

	var $_params;

	var $_pluginPath;

	

	function __construct( &$subject ) {
		
		parent::__construct( $subject );
		$this->_plugin = JPluginHelper::getPlugin( 'system', 'prextendedmenu' );

		$this->_params = new JRegistry( $this->_plugin->params );

		$this->_pluginPath = JPATH_PLUGINS."/system/prextendedmenu/";

	}

	// add Praes menu parameter

	function onContentPrepareForm($form, $data) {
		
		
		if ($form->getName()=='com_menus.item') {

			JForm::addFormPath($this->_pluginPath);

			$form->loadFile('parameters', false);

		}

	}

}

?>