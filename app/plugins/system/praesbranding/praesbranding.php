<?php

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

/**
 * PraesBranding system plugin
 */
class plgSystemPraesBranding extends JPlugin {

    public function __construct(&$subject, $config) {
        parent::__construct($subject, $config);
        if(!$this->params) return;

        $this->_joomla32ge = false;
        if(version_compare(JVERSION, '3.2', '>=') == 1) {
            $this->_joomla32ge = true;
        }
        $app = JFactory::getApplication();
        if(!$app->isAdmin()) return;
        $user = JFactory::getUser();
        $this->_loginpage = $user->guest;

        $this->_customlogologin    = $this->params->get('customlogologin', '');
        $this->_customlogologinh   = $this->params->get('customlogologinh', '75');
        $this->_lbodycolor         = $this->params->get('lbodycolor', 0);
        $this->_lbodycolorval      = $this->params->get('lbodycolorval', '#E0DEDF');
        $this->_lbodyimage         = $this->params->get('lbodyimage', 1);
        $this->_lbodyimageval      = $this->params->get('lbodyimageval', '');
        $this->_lbodyimagetile     = $this->params->get('lbodyimagetile', 0);
        $this->_lbodyimageposx     = $this->params->get('lbodyimageposx', 'center');
        $this->_lbodyimageposy     = $this->params->get('lbodyimageposy', 'center');

        $this->_customlogo        = $this->params->get('customlogo', '');
        $this->_customlogow       = $this->params->get('customlogow', '40');
        $this->_customlogoh       = $this->params->get('customlogoh', '30');
        $this->_headerheight      = $this->params->get('headerheight', '36');
        $this->_headrchnge         = intval($this->params->get('headrchnge' ,0));
        $this->_headerbg           = $this->params->get('headerbg', '#184A7D');
        $this->_headrgrad          = intval($this->params->get('headrgrad', 0));
        $this->_headergradstart    = $this->params->get('headergradstart', '#17568c');
        $this->_headergradend      = $this->params->get('headergradend', '#1a3867');

        $this->_showfooter        = $this->params->get('showfooter', 2);
        $this->_customfooter      = $this->params->get('customfooter', '');

        if($this->_customlogologin == '') $this->_customlogologin = self::getImagesPath() . 'login-logo.png';
        if($this->_lbodyimageval == '') $this->_lbodyimageval = self::getImagesPath() . 'login-bg.jpg';
        if($this->_customlogo == '') $this->_customlogo = self::getImagesPath() . 'dashboard-logo.png';
    }

    static function getImagesPath() {
        return JURI::root() . 'media/plg_praesbranding/images/';
    }

    function onAfterDispatch() {
		$app = JFactory::getApplication();
		if(!$app->isAdmin()) return;

		$doc = JFactory::getDocument();
		$extracss = '';

		if(intval($this->_loginpage)) {
			$extracss .= '#element-box img { display:none !important; }';
			$extracss .= '#element-box hr {
				display: block !important;
				width: 260px !important;
				background: url("' . $this->_customlogologin . '") 50% 50% no-repeat !important;
				height: ' . $this->_customlogologinh . 'px !important;
				background-size: ' . $this->_customlogologinh . 'px !important;
				border: none !important;
			}';

			$extracss .= '.view-login .login-joomla { display:none !important; }';

			if($this->_lbodycolor) {
				$extracss .= '.view-login { background: ' . $this->_lbodycolorval . ' !important; }';
			}

			if($this->_lbodyimage) {
				if($this->_lbodyimagetile) {
					$extracss .= '.view-login { background: ' . $this->_lbodycolorval . ' url("' . $this->_lbodyimageval . '") 0 0 repeat !important; }';
				} else {
					$extracss .= '.view-login { 
						background: ' . $this->_lbodycolorval . ' url("' . $this->_lbodyimageval . '") ' . $this->_lbodyimageposx . ' ' . $this->_lbodyimageposy . ' no-repeat !important;
						background-size: cover !important; 
					}';
				}
			}
		
			if(intval($this->_showfooter) == 0 or intval($this->_showfooter) == 2) {
				$extracss .= 'body .navbar-fixed-bottom p { display:none !important; text-indent:-99999px !important; }';
			}
		}
		
		if($this->_joomla32ge) {
			$extracss .= '.header { height: ' . $this->_headerheight . 'px !important; }';
			$extracss .= '.header img.logo { display: none !important; }';
			$extracss .= '.header .container-logo { 
				display: block !important; 
				max-width: ' . $this->_customlogow . 'px !important; 
				width: ' . $this->_customlogow . 'px  !important; 
				height: ' . $this->_customlogoh . 'px  !important; 
				background: url("'.$this->_customlogo.'") 0 0 no-repeat  !important;
			}';
		}
		else {
			$extracss .= 'body a.logo img { display: none !important; }';
			$extracss .= 'body a.logo { 
				display: block !important;
				max-width: ' . $this->_customlogow . 'px !important;
				width: ' . $this->_customlogow . 'px !important; 
				height: ' . $this->_customlogoh . 'px !important;
				background: url("'.$this->_customlogo.'") 0 0 no-repeat !important;
			}';
		}

		if($this->_headrchnge) {
			$extracss .= ' .header { background-color: ' . $this->_headerbg . ' !important; }';
			if($this->_headrgrad ) {
				$extracss .= '.header {
					background-image: -moz-linear-gradient(top, ' . $this->_headergradstart . ', ' . $this->_headergradend . ') !important;
					background-image: -webkit-gradient(linear, 0 0, 0 100%, from(' . $this->_headergradstart . '), to(' . $this->_headergradend . ')) !important;
					background-image: -webkit-linear-gradient(top, ' . $this->_headergradstart . ', ' . $this->_headergradend . ') !important;
					background-image: -o-linear-gradient(top, ' . $this->_headergradstart . ', ' . $this->_headergradend . ') !important;
					background-image: linear-gradient(to bottom, ' . $this->_headergradstart . ', ' . $this->_headergradend . ') !important;
					background-repeat: repeat-x;
					filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="' . $this->_headergradstart . '", endColorstr="' . $this->_headergradend . '", GradientType=0) !important;
				}';
			} else {
				$extracss .= '.header {
					background-image: none !important;
					filter: none !important;
				}';
			}
		}

		$doc->addStyleDeclaration($extracss);
    }

    function onAfterRender(){
		$app = JFactory::getApplication();
		if(!$app->isAdmin()) return;
		
		if(intval($this->_loginpage) && intval($this->_showfooter) == 2) {
			$extracss = 'body .navbar-fixed-bottom p { display:none !important; text-indent:-99999px !important; }';
			
			$doc = JFactory::getDocument();
			$output = '<div style="position: absolute; bottom: 0px; right: 20px; color: #fcfcfc; text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.5);"><p class="pull-right">' . $this->_customfooter . '</p></div>';
			if($output){
			   JResponse::appendBody($output);
			}
			
			$doc->addStyleDeclaration($extracss);
		}
    }
}
