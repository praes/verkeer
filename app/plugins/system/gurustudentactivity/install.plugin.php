<?php 
/*------------------------------------------------------------------------
# com_publisher
# ------------------------------------------------------------------------
# author    iJoomla
# copyright Copyright (C) 2013 ijoomla.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.ijoomla.com
# Technical Support:  Forum - http://www.ijoomla.com/forum/index/
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');
if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

$db = JFactory::getDBO();
$sql = "select `installed_plugin_user` from #__guru_config";
$db->setQuery($sql);
$db->query();
$installed_plugin_user = $db->loadColumn();
$installed_plugin_user = @$installed_plugin_user["0"];

if(isset($installed_plugin_user) && trim($installed_plugin_user) == "0000-00-00 00:00:00"){
	$sql = "update #__guru_config set `installed_plugin_user`=now()";
	$db->setQuery($sql);
	$db->query();
}

?>