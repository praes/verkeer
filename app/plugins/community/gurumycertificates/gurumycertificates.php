<?php
/**
 * Guru My Certificates Plugin for Jomsocial
 * @package Guru.plg_community_gurumycertificates
 *
 * @copyright (C) 2013 ijoomla.com. All Rights Reserved.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @link http://www.ijoomla.com
 **/
defined('_JEXEC') or die('Restricted access');
defined('DS') or define("DS", DIRECTORY_SEPARATOR);
class plgCommunityGuruMyCertificates extends CApplications{
	var $_user		= null;
	var $name = "Guru My Certificates";
	var $_name = 'gurumycertificates';
	function plgCommunityGuruMyCertificates(& $subject, $config) {
		//Load Language file.
		JPlugin::loadLanguage ( 'plg_community_gurumycertificates', JPATH_ADMINISTRATOR );
		parent::__construct ( $subject, $config );
	}
	function overrideLanguage($params){
		jimport('joomla.filesystem.file');
				
		if(!file_exists(JPATH_SITE.DS."language".DS."overrides".DS."en-GB.override.ini")){
			$source = JPATH_SITE.DS."language".DS."overrides".DS."en-GB.override.ini";
			$content = 'PLG_GURUMYCERTIFICATES_TITLE = "'.$params->get("pluginInFront", "").'"';
			JFile::write($source, $content);
		}
		else{
			$source = JPATH_SITE.DS."language".DS."overrides".DS."en-GB.override.ini";
			$content = 'PLG_GURUMYCERTIFICATES_TITLE = "'.$params->get("pluginInFront", "").'"';
			$file_content = file_get_contents($source);
			
			if(strpos(" ".$file_content, "PLG_GURUMYCERTIFICATES_TITLE") !== FALSE){
				$file_content = preg_replace("/^PLG_GURUMYCERTIFICATES_TITLE(.*)=(.*)\"(.*)\"/msU", $content, $file_content);
			}
			else{
				$file_content .= "\n".$content;
			}
			
			JFile::write($source, $file_content);
		}
	}
	function getAllCert($id){
		$db = JFactory::getDBO();
		$sql = "select id from `#__guru_mycertificates` where user_id=".intval($id);
		$db->setQuery($sql);
		$db->query();
		$all_c = $db->loadColumn();
		$all_c = count($all_c);
		return $all_c;
	}
	function getAuthor($course){
		$db = JFactory::getDBO();
		$authorname = "SELECT name from #__users where id=".intval($course);
		$db->setQuery($authorname);
		$db->query();
		$authorname = $db->loadColumn();
		return $authorname["0"];
	}	
	function getCertificateImage(){
		$db = JFactory::getDBO();
		$sql = "select design_background from `#__guru_certificates` where id=1";
		$db->setQuery($sql);
		$db->query();
		$certificate_background = $db->loadColumn();
		$certificate_background = $certificate_background[0];
		return $certificate_background;
	}
	function getCoursetoCertificate($params){
		$db = JFactory::getDBO();
		$my	= CFactory::getUser();
		$reqid = intval(JRequest::getVar('userid', 0));	
		if(intval($reqid) == 0){
			$reqid = $my->id;		
		}
		$sql = "select course_id from `#__guru_mycertificates` where user_id=".intval($reqid);
		$db->setQuery($sql);
		$db->query();
		$course_id = $db->loadColumn();
		$course_id = implode(",",$course_id);
		
		if($course_id == ""){
			return "";	
		}
		else{
			$sql = "select * from `#__guru_program` where id IN(".$course_id.") LIMIT 0,".$params->get("howManyC")."";
			$db->setQuery($sql);
			$db->query();
			$courses = $db->loadAssocList();
			return $courses;	
		}	
		
	} 
	function showCourseImage($params){
		if($params->get("showthumb", "1") == 1){
			return true;
		}
		return false;
	}
	function getMyCertificatesALL($uid,$course_id){
		$db = JFactory::getDBO();
		$sql = "SELECT * FROM #__guru_mycertificates
				WHERE user_id = ".intval($uid)." and course_id=". intval($course_id) ;
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadAssocList();
		return $result;
	}
	function getCourseTittleLenght($course, $params){
		$return = "";
		$audio_p_desc_length = $params->get("coursetitlelength");
		$audio_p_desc_length_type = $params->get("coursetitlelengthtype");
		$description = strip_tags($course["name"]);
		
		if($audio_p_desc_length != '' && trim($description) != ""){
			 $new_description = "";
			 if($audio_p_desc_length_type == 0){
				//words
				$desc_array = explode(" ", $description);
				$desc = array();
				if(count($desc_array) > $audio_p_desc_length){
					foreach($desc_array as $key => $val){									
						if($key < $audio_p_desc_length){
							$desc[] = $val;
						}									
					 }
					$new_description = implode(" ", $desc)."..."; 								
				}
				else{
					$new_description = $description;
				}							 
			 }
			 elseif($audio_p_desc_length_type == 1){
				//characters						 	
				$descr_nr = strlen($description);
				if($descr_nr > $audio_p_desc_length){
					$new_description = substr(trim($description), 0, $audio_p_desc_length)."...";
				}
				else{
					$new_description = $description;
				}
			 }
			 $return = $new_description;
		}
		return $return;
	}
	
	function create_module_thumbnails($images, $width, $height, $width_old, $height_old){
		$image_path = JURI::root().$images;
		if(strpos($images, "http") !== FALSE){
			$image_path = $images;
		}
		$thumb_src = "plugins/community/gurumycertificates/gurumycertificates/createthumbs.php?src=".$image_path."&amp;w=".$width."&amp;h=".$height;//."&zc=1";
		return $thumb_src;
	}
	
	function templateReplacementModal($course_id,$course_auth){
		$my	= CFactory::getUser();
		$reqid = intval(JRequest::getVar('userid', 0));	
		if(intval($reqid) == 0){
			$reqid = $my->id;		
		}
		$db = JFactory::getDBO();
		$config = JFactory::getConfig();
		$imagename = "SELECT * FROM #__guru_certificates WHERE id=1";
		$db->setQuery($imagename);
		$db->query();
		$imagename = $db->loadAssocList();
		
		$sql = "select name from `#__guru_program` where id =".intval($course_id);
		$db->setQuery($sql);
		$db->query();
		$coursename = $db->loadColumn();
		
		$site_url = JURI::root();
		$sitename = $config->get( 'sitename');
		$all_cerfiicates = $this->getMyCertificatesALL($reqid, $course_id);
		$authorname = $this->getAuthor($course_auth);
		$certificateid = $all_cerfiicates[0]["id"];
		$completiondate = $all_cerfiicates[0]["datecertificate"];
		
		$firstnamelastname = "SELECT firstname, lastname FROM #__guru_customer WHERE id=".intval($reqid);
		$db->setQuery($firstnamelastname);
		$db->query();
		$firstnamelastname = $db->loadAssocList();
		

		$imagename[0]["templates1"]  = str_replace("[SITENAME]", $sitename, $imagename[0]["templates1"]);
		$imagename[0]["templates1"]  = str_replace("[STUDENT_FIRST_NAME]", $firstnamelastname[0]["firstname"], $imagename[0]["templates1"]);
		$imagename[0]["templates1"]  = str_replace("[STUDENT_LAST_NAME]", $firstnamelastname[0]["lastname"], $imagename[0]["templates1"]);
		$imagename[0]["templates1"]  = str_replace("[SITEURL]", $site_url, $imagename[0]["templates1"]);
		$imagename[0]["templates1"]  = str_replace("[CERTIFICATE_ID]", $certificateid, $imagename[0]["templates1"]);
		$imagename[0]["templates1"]  = str_replace("[COMPLETION_DATE]", $completiondate, $imagename[0]["templates1"]);
		$imagename[0]["templates1"]  = str_replace("[COURSE_NAME]", $coursename[0], $imagename[0]["templates1"]);
		$imagename[0]["templates1"]  = str_replace("[AUTHOR_NAME]", $authorname, $imagename[0]["templates1"]);
		$imagename[0]["templates1"]  = str_replace("[CERT_URL]", @$certificate_url, $imagename[0]["templates1"]);
		$imagename[0]["templates1"]  = str_replace("[COURSE_MSG]", @$coursemsg, $imagename[0]["templates1"]);
		$imagename[0]["templates1"]  = str_replace("[COURSE_AVG_SCORE]", @$avg_certc1, $imagename[0]["templates1"]);
		$imagename[0]["templates1"]  = str_replace("[COURSE_FINAL_SCORE]", @$avg_certc, $imagename[0]["templates1"]);
		
		return $imagename[0]["templates1"];	
	}

	
	function onProfileDisplay() {
		$this->overrideLanguage($this->params);
		JPlugin::loadLanguage( 'plg_community_gurumycertificates', JPATH_ADMINISTRATOR );
		jimport( 'joomla.application.component.controller' );
		$db = JFactory::getDBO();
		$document = JFactory::getDocument ();
		$document->addStyleSheet ( JURI::base () . 'plugins/community/gurumycertificates/gurumycertificates/style.css' );	
		$document->addScript('plugins/community/gurumycertificates/gurumycertificates/jquery-1.9.1.min.js');	
		$document->addScript("plugins/community/gurumycertificates/gurumycertificates/modal_certificate.js");
		$item_id = JRequest::getVar('Itemid', 0);
		$_SESSION["ITEMID"] = $item_id;
		$_SESSION["CB"] = "1";
		
		$image = $this->getCertificateImage();	
		$image_theme = explode("/", $image);
		if(trim($image_theme[4]) =='thumbs'){
			$image_theme = $image_theme[5];
		}
		else{

			$image_theme = $image_theme[4];
		}
		$courses = $this->getCoursetoCertificate($this->params);		
		$my	= JFactory::getUser();
		$global_user = $my->id;
		$view_all_links = 0;
		$reqid = intval(JRequest::getVar('userid', 0));	
		if(intval($reqid) == 0){
			$reqid = $my->id;		
		}
		if($global_user == $reqid){
			$view_all_links = 1;
		}			
		$k = 0;
		

		$all_c = $this->getAllCert($reqid);
		if($this->params->get("showthumb",1) == 0){
			$span = 'span12';
		}
		else{
		}
		
		$sql = "select count(*) from #__extensions where element='com_guru' and `enabled`=1";
		$db->setQuery($sql);
		$db->query();
		$count = $db->loadColumn();
		$count_guru = $count["0"];
		if ( $count_guru <= 0){
		   $content = '<div class="guru_plugin_empty">'.JText::_("PLG_GURUMYCERT_NOCOMP").'</div>';
		}
		elseif($courses != ""){
			$content ="<div id='mod_gurucpanel'>";
		    if(isset($courses) && count($courses) > 0){
				foreach($courses as $key=>$course){
					$course["name"] = $this->getCourseTittleLenght($course, $this->params);
					$content .= '<div class="row-fluid mod_guru_courses">
						<div class="span12">
							<div>
								<div>';
									if($this->showCourseImage($this->params)){
										$image_url =  $image;
										$image_url = str_replace("thumbs/", "", $image_url);
										$img_size = array();
										$host = $_SERVER['HTTP_HOST'];
										$myImg = str_replace("http://", "", $image_url);
										$myImg = str_replace($host, "", $myImg);
										if($myImg != $image_url){
											$myImg =str_replace("/", DS."", $myImg);			
											$img_size = @getimagesize(JPATH_SITE.DS.$myImg);					
										}
										else{
											$img_size = @getimagesize(urldecode($image_url));
										}
										
										$width_old = $img_size["0"];
										$height_old = $img_size["1"];
						
										$width_th = "70";
										$height_th = "70";
									}
								
									if(trim($image) && $this->params->get("showthumb",1) == 1){
										$src =  $this->create_module_thumbnails($image_url, $width_th, $height_th, $width_old, $height_old);
										$content .= '<a onclick="javascript:loadPopup(\''.$k.'\'); return false;" class="ij_plg_thumb pull-left" href="#"><img src="'.$src.'" alt="" title=""></a>';
									}
									else{
										$content .= '';
									}
									if($this->params->get("showcoursename",1) == 1){
										$content .='<a onclick="javascript:loadPopup(\''.$k.'\'); return false;" class="guru_course_name '.@$span.'" href="#"><h5>'.$course["name"].'</h5></a>';
									}
										
								$content .='</div>
							</div> 
						</div>
					</div>';
					
					
					$modal_content = $this->templateReplacementModal($course["id"],$course["author"]);
					$content .= '<div class="toPopup" id="toPopup'.$k.'" style="display:none; z-index:10000;">
								<div class="close" onclick="javascript:disablePopup('.$k.'); return false;"></div>
									<div id="popup_content"> <!--your content start-->
										<div>
											<div style="background-size:100% 500px; background-repeat:no-repeat; background-image:url('.JUri::base().'images/stories/guru/certificates/'.$image_theme.');">
												<div align="center" style="padding-top:180px;">
													'.$modal_content.'
												</div>
											</div>
										</div> 
									</div> <!--your content end-->
								</div> <!--toPopup end-->
								<div class="loader"></div>';
					$k ++;
				}
			}

			if($view_all_links == 1){
				$content .='			
				<div class="guru-module-box-footer">
					<a href="index.php?option=com_guru&view=guruorders&layout=mycertificates&Itemid='.@$item_id.'">
						<span>'.JText::_("PLG_GURUCOURSESCERT_VIEWALLC").'</span>
						<span>('.$all_c.')</span>
					</a>
				</div>';
			}	
			$content .='</div>
			<input type="hidden" id="certificate_total" name="certificate_total" value="'.($all_c*2).'">
			';
		}
		else{
			$content .='<div class="guru_plugin_empty">'.JText::_("PLG_MYCERT_NOCOURSE").'</div>';
		}
		return $content;
		}	
	}