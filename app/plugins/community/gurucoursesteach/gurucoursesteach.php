<?php
/**
 * Guru Course I teach Plugin for Jomsocial
 * @package Guru.plg_community_gurucoursesteach
 *
 * @copyright (C) 2013 ijoomla.com. All Rights Reserved.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @link http://www.ijoomla.com
 **/
defined('_JEXEC') or die('Restricted access');
defined('DS') or define("DS", DIRECTORY_SEPARATOR);
class plgCommunityGuruCoursesTeach extends CApplications{
	var $_user		= null;
	var $name = "Guru Courses I Teach";
	var $_name = 'gurucoursesteach';
	function plgCommunityGuruCoursesTeach(& $subject, $config) {
		//Load Language file.
		JPlugin::loadLanguage ( 'plg_community_gurucoursesteach', JPATH_ADMINISTRATOR );
		parent::__construct ( $subject, $config );
	}
	function overrideLanguage($params){
		jimport('joomla.filesystem.file');

		if(!file_exists(JPATH_SITE.DS."language".DS."overrides".DS."en-GB.override.ini")){
			$source = JPATH_SITE.DS."language".DS."overrides".DS."en-GB.override.ini";
			$content = 'PLG_GURUCOURSESTEACH_TITLE = "'.$params->get("pluginInFront", "").'"';
			JFile::write($source, $content);
		}
		else{
			$source = JPATH_SITE.DS."language".DS."overrides".DS."en-GB.override.ini";
			$content = 'PLG_GURUCOURSESTEACH_TITLE = "'.$params->get("pluginInFront", "").'"';
			$file_content = file_get_contents($source);
			
			if(strpos(" ".$file_content, "PLG_GURUCOURSESTEACH_TITLE") !== FALSE){
				$file_content = preg_replace("/^PLG_GURUCOURSESTEACH_TITLE(.*)=(.*)\"(.*)\"/msU", $content, $file_content);
			}
			else{
				$file_content .= "\n".$content;
			}
			
			JFile::write($source, $file_content);
		}
	}
	
	private function _getCourses($params){
		$db = JFactory::getDBO();
		$sortby = $params->get("sortby", "0");
		$my			= CFactory::getUser();
		$reqid = intval(JRequest::getVar('userid', 0));	
		if(intval($reqid) == 0){
			$reqid = $my->id;		
		}
		$and = "";
		switch($sortby){
			case "0" : { // most popular
				$sql = "select * from `#__guru_program`";
				$db->setQuery($sql);
				$db->query();
				$courses = $db->loadAssocList();
				if(isset($courses) && count($courses) > 0){
					$courses_temp = array();
					foreach($courses as $key=>$course){
						$nr = $this->_getStudentsNumber($course, null);
						if(count($courses_temp) == 0){
							$courses_temp[] = array($course['id']=>$nr);
						}
						else{							
							foreach($courses_temp as $key=>$c_id_nr){
								if(current($c_id_nr) <= $nr){
									array_splice($courses_temp, $key, 0, array(array($course['id']=>$nr)));
									break;
								}
								elseif(!isset($courses_temp[$key + 1])){
									array_splice($courses_temp, $key+1, 0, array(array($course['id']=>$nr)));
									break;
								}
							}
						}
					}
					$courses_temp = array_slice($courses_temp, 0, $params->get("howManyC"));
					$courses = array();
					foreach($courses_temp as $key=>$c_id_nr){
						$sql = "select * from `#__guru_program` where `id`=".intval(key($c_id_nr)). " and (`author`=".intval($reqid)." OR `author` like '%|".intval($reqid)."|%' )";
						$db->setQuery($sql);
						$db->query();
						$course_temp = $db->loadAssocList();
						$courses = array_merge($courses, $course_temp);
					}
				}
				return $courses;
				break;
			}
			case "1" : { // most recent
				$and .= " and author=".intval($reqid)." ORDER BY `startpublish` DESC LIMIT 0,".$params->get("howManyC")."";
				break;
			}
			case "2" : { // random
				$and .= " and author=".intval($reqid)." ORDER BY RAND() LIMIT 0,".$params->get("howManyC")."";
				break;
			}
		}
		$sql = "select * from `#__guru_program` where 1=1 and published=1 ".$and;
		$db->setQuery($sql);
		$db->query();
		$courses = $db->loadAssocList();
		return $courses;
	}
	function _getStudentsNumber($course, $params){
		$db = JFactory::getDBO();
		$sql = "SELECT distinct o.userid FROM  #__guru_customer c, #__guru_order o  where courses like '".intval($course["id"])."-%' and c.id=o.userid and o.status='Paid' GROUP BY o.userid";
		$db->setQuery($sql);
		$db->query();
		$result = $db->loadColumn();
		$nr_studv = count($result);
		return $nr_studv;
	}
	function showCourseImage($params){
		if($params->get("showthumb", "1") == 1){
			return true;
		}
		return false;
	}
	
	function createThumb($course, $params){
		return $course["image"];
	}
	function getAllCoursesTH($id){
		$db = JFactory::getDBO();
		$sql = "select id from `#__guru_program` where (`author`=".intval($id)." OR `author` like '%|".intval($id)."|%')";
		$db->setQuery($sql);
		$db->query();
		$all_c = $db->loadColumn();
		$all_c = count($all_c);
		return $all_c;
	}
	function getDescription($course, $params){
		$return = "";
		$audio_p_desc_length = $params->get("desclength");
		$audio_p_desc_length_type = $params->get("desclengthtype");
		$description = strip_tags($course["description"]);
		
		if($audio_p_desc_length != '' && trim($description) != ""){
			 $new_description = "";
			 if($audio_p_desc_length_type == 0){
				//words
				$desc_array = explode(" ", $description);
				$desc = array();
				if(count($desc_array) > $audio_p_desc_length){
					foreach($desc_array as $key => $val){									
						if($key < $audio_p_desc_length){
							$desc[] = $val;
						}									
					 }
					$new_description = implode(" ", $desc)."..."; 								
				}
				else{
					$new_description = $description;
				}							 
			 }
			 elseif($audio_p_desc_length_type == 1){
				//characters						 	
				$descr_nr = strlen($description);
				if($descr_nr > $audio_p_desc_length){
					$new_description = substr(trim($description), 0, $audio_p_desc_length)."...";
				}
				else{
					$new_description = $description;
				}
			 }
			 $return = $new_description;
		}
		return $return;
	}
	function getCourseTittleLenght($course, $params){
		$return = "";
		$audio_p_desc_length = $params->get("coursetitlelength");
		$audio_p_desc_length_type = $params->get("coursetitlelengthtype");
		$description = strip_tags($course["name"]);
		
		if($audio_p_desc_length != '' && trim($description) != ""){
			 $new_description = "";
			 if($audio_p_desc_length_type == 0){
				//words
				$desc_array = explode(" ", $description);
				$desc = array();
				if(count($desc_array) > $audio_p_desc_length){
					foreach($desc_array as $key => $val){									
						if($key < $audio_p_desc_length){
							$desc[] = $val;
						}									
					 }
					$new_description = implode(" ", $desc)."..."; 								
				}
				else{
					$new_description = $description;
				}							 
			 }
			 elseif($audio_p_desc_length_type == 1){
				//characters						 	
				$descr_nr = strlen($description);
				if($descr_nr > $audio_p_desc_length){
					$new_description = substr(trim($description), 0, $audio_p_desc_length)."...";
				}
				else{
					$new_description = $description;
				}
			 }
			 $return = $new_description;
		}
		return $return;
	}
	
	function getAuthor($course, $params){
		$db = JFactory::getDBO();
		$authorname = "SELECT name from #__users where id=".intval($course["author"]);
		$db->setQuery($authorname);
		$db->query();
		$authorname = $db->loadColumn();
		return $authorname["0"];
	}
	function getAuthorID($course, $params){
		$db = JFactory::getDBO();
		$authorname = "SELECT id from #__guru_authors where userid=".intval($course["author"]);
		$db->setQuery($authorname);
		$db->query();
		$authorname = $db->loadColumn();
		return $authorname["0"];
	}
	function getAuthorIDForALL($id){
		$db = JFactory::getDBO();
		$authorname = "SELECT id from #__guru_authors where userid=".intval($id);
		$db->setQuery($authorname);
		$db->query();
		$authorname = $db->loadColumn();
		return $authorname["0"];
	}

	function create_module_thumbnails($images, $width, $height, $width_old, $height_old){
			$image_path = JURI::root().$images;
			if(strpos($images, "http") !== FALSE){
				$image_path = $images;
			}
			$thumb_src = "plugins/community/gurucoursesteach/gurucoursesteach/createthumbs.php?src=".$image_path."&amp;w=".$width."&amp;h=".$height;//."&zc=1";
			return $thumb_src;
		}
		
	function getAudioDescription($audio, $params){
		$return = "";
		$audio_p_desc_length = $params->get("desclength");
		$audio_p_desc_length_type = $params->get("desclengthtype");
		$description = $audio["description"];
		
		if($audio_p_desc_length != '' && trim($description) != ""){
			 $new_description = "";
			 if($audio_p_desc_length_type == 0){
				//words
				$desc_array = explode(" ", $description);
				$desc = array();
				if(count($desc_array) > $audio_p_desc_length){
					foreach($desc_array as $key => $val){									
						if($key < $audio_p_desc_length){
							$desc[] = $val;
						}									
					 }
					$new_description = implode(" ", $desc)."..."; 								
				}
				else{
					$new_description = $description;
				}							 
			 }
			 elseif($audio_p_desc_length_type == 1){
				//characters						 	
				$descr_nr = strlen($description);
				if($descr_nr > $audio_p_desc_length){
					$new_description = substr($description, 0, $audio_p_desc_length)."...";
				}
				else{
					$new_description = $description;
				}
			 }
			 $return = $new_description;
		}
		return $return;
	}
	function getCourseLevel($course, $params){
		switch($course["level"]){
			case "0" : { 
				$return = JText::_("GURU_BEGINNERS");
				break;
			}
			case "1" : { 
				$return = JText::_("GURU_INTERMEDIATE");
				break;
			}
			case "2" : { 
				$return = JText::_("GURU_ADVANCED");
				break;
			}
		}
		return $return;
	
	}
	
	function onProfileDisplay() {
		$this->overrideLanguage($this->params);
		JPlugin::loadLanguage( 'plg_community_gurucoursesteach', JPATH_ADMINISTRATOR );
		jimport( 'joomla.application.component.controller' );
		$document = JFactory::getDocument ();
		$document->addStyleSheet ( JURI::base () . 'plugins/community/gurucoursesteach/gurucoursesteach/style.css' );		
		$courses = $this->_getCourses($this->params);
		$my			= CFactory::getUser();
		$global_user = $my->id;
		$view_all_links = 0;
		$reqid = intval(JRequest::getVar('userid', 0));	
		if(intval($reqid) == 0){
			$reqid = $my->id;		
		}
		if($global_user == $reqid){
			$view_all_links = 1;
		}		
		$auth_view_all = $this->getAuthorIDForALL($reqid);
		$item_id = JRequest::getVar('Itemid', 0);
		$_SESSION["ITEMID"] = $item_id;
		$_SESSION["CB"] = "1";
		$all_c = $this->getAllCoursesTH($reqid);
		$db = JFactory::getDBO();
		// Check if component is installed
		$sql = "select count(*) from #__extensions where element='com_guru' and `enabled`=1";
		$db->setQuery($sql);
		$db->query();
		$count = $db->loadColumn();
		$count_guru = $count["0"];
		
		if ( $count_guru <= 0){
		   $content = '<div class="guru_plugin_empty">'.JText::_("PLG_GURUCOURSESTEACH_NOCOMP").'</div>';
		}
		else{
			$content ="<div id='mod_gurucpanel'>";
			if(($this->params->get("teachername", "1") == 1) && ($this->params->get("showamountstud", "1") == 1) && ($this->params->get("showdescription", "1") == 1)){
		    	$style = "style='padding-bottom:10px'";
		    	$class = "";
		    }
		    else{
		    	$style = "style='padding-bottom:0px'";
		    	$class = "plg_guru_all_disabled";
		    }
		    if(isset($courses) && count($courses) > 0){
				foreach($courses as $key=>$course){
				$course["name"] = $this->getCourseTittleLenght($course, $this->params);
				$content .= '<div class="row-fluid mod_guru_courses"'. $style.'>
	            	<div class="span12">
	                	<div>
	                    	<div>
									 <a  class="guru_course_name '.$class.'" href="'.JRoute::_('index.php?option=com_guru&view=guruPrograms&task=view&cid='.$course["id"]."-".$course["alias"]."&Itemid=".intval(@$item_id)).'"><h5>'.$course["name"].'</h5></a>';
										
									if(($this->params->get("showamountstud", "1") == 1) || ($this->params->get("showdescription", "1") == 1)){	
										$content .= '<ul class="ij_mod_details">';
											if($this->params->get("showamountstud", "1") == 1){
												$nr_students = $this->_getStudentsNumber($course, $this->params);
												$language = JText::_('GURU_MODULE_AMOUNT_STUDENTS_FRONT');
												if($nr_students == 1){
													$language = JText::_('GURU_MODULE_AMOUNT_STUDENT_FRONT');
												}
												$content .= "<li><i class='mod_guru-icon-group'></i>"." ".$nr_students." ".$language."</li>";
											}										
											
										$content .= '</ul>';
									}
									if($this->showCourseImage($this->params)){
											$image_url = $course["image_avatar"];
											$image_url = str_replace("thumbs/", "", $image_url);
											$img_size = array();
											$host = $_SERVER['HTTP_HOST'];
											$myImg = str_replace("http://", "", $image_url);
											$myImg = str_replace($host, "", $myImg);
											if($myImg != $image_url){
												$myImg =str_replace("/", DS."", $myImg);			
												$img_size = @getimagesize(JPATH_SITE.DS.$myImg);					
											}
											else{
												$img_size = @getimagesize(urldecode($image_url));
											}
											
											$width_old = $img_size["0"];
											$height_old = $img_size["1"];
							
											$width_th = "0";
											$height_th = "0";
											
											if($this->params->get("thumbsizetype", "1") == 0 && isset($img_size)){
												if($width_old > $this->params->get("thumbsize", "0") && $this->params->get("thumbsize", "0") > 0){
													//proportional by width
													$raport = $width_old/$height_old;
													$width_th = $this->params->get("thumbsize", "0");
													$height_th = intval($this->params->get("thumbsize", "0") / $raport);
													$width_bullet_margin = $this->params->get("thumbsize", "0");					
												}
												else{
													$width_th = $width_old;
													$height_th = $height_old;					
												}
											}
											else{
												if($height_old > $this->params->get("thumbsize", "0") && $this->params->get("thumbsize", "0") > 0){
													//proportional by height			
												$raport = $height_old/$width_old;
												$height_th = $this->params->get("thumbsize", "0");						
												$width_th  = intval($this->params->get("thumbsize", "0") / $raport);
												$width_bullet_margin = intval($this->params->get("thumbsize", "0") / $raport);					
											}
											else{
												$width_th = $width_old;
												$height_th = $height_old;					
											}
										}
							
										if(trim($course["image_avatar"])){
											$src =  $this->create_module_thumbnails($image_url, $width_th, $height_th, $width_old, $height_old);
											$content .= '<a class="ij_mod_thumb pull-left" href="'.JRoute::_('index.php?option=com_guru&view=guruPrograms&task=view&cid='.$course["id"]."-".$course["alias"]."&Itemid=".intval(@$item_id)).'"><img src="'.$src.'" alt="" title=""></a>';
										}
										else{
											$content .= '';
										}
									}
									
									if(($this->params->get("showdescription", "1") == 1)){
										$content .= '<div  class="ij_course">';
											if($this->params->get("showdescription", "1") == 1){
												$description =$this->getDescription($course, $this->params);
												$content .= '<p class="ij_couurse_cont">'.$description.'</p>';
											}
											else{
												$content .= '&nbsp;';
											}
										$content .='</div>';
									}
	                		$content .='</div>
						</div> 
	                </div>
	            </div>
	            ';
				}
			}
			else{
				$content .='<div class="guru_plugin_empty">'.JText::_("PLG_GURUCOURSESTEACH_NOCOURSE").'</div>';
			}

			if(isset($courses) && count($courses) > 0 && $view_all_links == 1){
				$content .='
				<div class="guru-module-box-footer">
					<a href="index.php?option=com_guru&view=guruauthor&layout=view&cid='.$auth_view_all.'&Itemid='.@$item_id.'">
						<span>'.JText::_("PLG_GURUCOURSESTEACH_VIEWALLC").'</span>
						<span>('.$all_c.')</span>
					</a>
				</div>';
			}
			$content .='</div>';
		}	
		return $content;
	}
}
