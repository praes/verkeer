a:6:{s:8:"feedinfo";a:3:{s:4:"type";s:3:"RSS";s:7:"version";s:3:"2.0";s:8:"encoding";s:5:"UTF-8";}s:4:"info";a:4:{s:4:"link";a:2:{s:4:"self";a:1:{i:0;s:33:"http://www.ijoomla.com/blog/feed/";}s:9:"alternate";a:1:{i:0;s:27:"http://www.ijoomla.com/blog";}}s:5:"title";s:12:"iJoomla Blog";s:11:"description";s:12:"iJoomla Blog";s:8:"language";s:5:"en-US";}s:5:"items";a:10:{i:0;O:12:"NewsRss_Item":1:{s:4:"data";a:8:{s:5:"title";s:55:"Ad Agency 6.0.21 updated for bug fixes of Geo-Targeting";s:7:"encoded";s:1799:"<p><img src="http://www.ijoomla.com/blog/wp-content/uploads/2019/02/Ad-agency6.0.21.jpg" alt="Ad Agency 6.0.21 updated for bug fixes of Geo-Targeting" /></p>
<p>Hi<br />
Our Joomla Advertising extension: Ad Agency Pro 6.0.21 updated for bug fixes of Geo-Targeting.</p>
<p>Ad Agency is the best advertising extension for Joomla! It helps generate income from your traffic by creating an advertising program. Ad Agency supports all major ad types: Standard Banners, Flash Banners, HTML5 ads, Floating Ads, Pop-Up and Pop-Under Ads, Ad Code and Text Ads.<br />
<span></span></p>
<h3>About Geo-Targeting:</h3>
<p>Geo-Targeting allows advertisers to display ads based on the geographic area of the viewer and helps to ensure that their ads are local, their products are reachable and the ad appears in the reader’s language. For example, a restaurant owner would only want to show his ads to people within his catchment area. Another plus is that an advertiser in Europe can create different ads for different countries, showing the right language based on the geo-targeting data.</p>
<h3>We fixed 2 issues for Geo-Targeting in this version.</h3>
<p>Since Maxmind changed the data files for location, this leads to Geolocation fields working issues. We fixed the issue by adding the latest code to support the latest support for City, country, PHAR and zip list.</p>
<h3>Release changelog</h3>
<h4>Bug fixes: </h4>
<ul>
<li>Maxmind city file no longer exist</li>
<li>Got installation error when drag and drop package to install</li>
</ul>
<h4>Improvement: </h4>
<ul>
<li>module rotate refresh time</li>
</ul>
<p align="center"><a href="https://adagency.ijoomla.com/changelog-2">View Changelog</a> <a href="https://www.ijoomla.com/products">Get it Now</a> <a href="https://ijoomlademo.com/">Demo</a></p>";s:11:"description";s:198:"Hi Our Joomla Advertising extension: Ad Agency Pro 6.0.21 updated for bug fixes of Geo-Targeting. Ad Agency is the best advertising extension for Joomla! It helps generate income from your [&#8230;]";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:84:"http://www.ijoomla.com/blog/ad-agency-6-0-21-updated-for-bug-fixes-of-geo-targeting/";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:35:"http://www.ijoomla.com/blog/?p=2445";}s:7:"pubdate";i:1551349573;s:8:"category";a:1:{i:0;s:21:"iJoomla Announcements";}s:7:"creator";a:1:{i:0;O:14:"NewsRss_Author":3:{s:4:"name";s:5:"iTeam";s:4:"link";N;s:5:"email";N;}}}}i:1;O:12:"NewsRss_Item":1:{s:4:"data";a:8:{s:5:"title";s:54:"Dedicated Guru Template Released | Guru 5.1.17 Updated";s:7:"encoded";s:3504:"<p><img src="http://static.joomlart.com/images/blog/2018/ja-sensei-preview/ja-sensei-education-joomla-template.jpg" alt=" best education Joomla template with LMS " /></p>
<h3>JA Sensei: Education Joomla template released and it&rsquo;s FREE for all Guru pro and Ultimate plan users</h3>
<p>JA Sensei is pro Joomla template for education, online courses with LMS based on the best Joomla LMS extension &ndash; Guru with powerful features and all required pages for education, online courses: Course category, course list, course detail, teacher list, teacher detail, course registration and more. </p>
<p>The LMS Joomla template is built with T3 Framework, it is easy to customize based on the powerful admin control panel with built-in functionalities: visual layout configuration, responsive layout configuration, megamenu builder, optimization, etc.</p>
<p><strong>You can check out the complete list of features <a href="https://www.joomlart.com/joomla/templates/ja-sensei" target="_blank">here</a> and download JA Sensei from <a href="https://www.joomlart.com/member/downloads/ijoomla/guru/guru-template" target="_blank"> this link</a></strong></p>
<hr />
<h3>Guru 5.1.17 Updated for new features, improvements and bug fixes</h3>
<p><img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/guru-5.1.17.jpg" alt="guru 5.1.17 updates released" /></p>
<p>Today we are also releasing updates for <strong>Joomla LMS Extension: Guru 5.1.17</strong>. This release comes with Dedicated Joomla LMS template JA Sensei and 2 new modules for all Guru Pro users.<br />
The template fully supports the latest version of Guru with stylish course views and Course list module for Guru. Guru 5.1.17 comes with 5 new features, 4 improvements and 11 bug fixes.</p>
<p><span></span></p>
<p>Please Check release info below:</p>
<h3>New Features:</h3>
<ul>
<li>Check enrolled courses and expired orders : Allow a professional way to enroll student and activatation of course.
</li>
<li>New Course List Module : A  New list layout to display selected courses. You can use it anywhere on a module position.
</li>
<li>Module Course: Add link to Category that course belongs to, show teacher&#039;s Avatar </li>
<li>Mailchimp integration for offline payments : Now offline paid students also get an option to subscribe newsletter.
</li>
<li>Strip Payment: support Japan Yen currency.
</li>
</ul>
<h3>Improvements:</h3>
<ul>
<li>Support Invisible captcha in Register Page
</li>
<li>Create email template option for courses
</li>
<li>Mod Search Course: Results page will navigate to another page
</li>
<li> New Changes for Guru Courses Module: add Layout Advanced Option
</li>
</ul>
<h4>Bug fixes</h4>
<ul>
<li>Teacher can not upload Course cover image
</li>
<li>Last Viewed Lesson
</li>
<li>Module Course: SEO function &amp; active menu doesn&#039;t map correctly
</li>
<li>The router of Guru doesn&#039;t set in right Menu Item
</li>
<li>JA Login module is not working on guru page
</li>
<li>Site is hanged on  after open a lession with a video
</li>
<li>Got notice error when search courses
</li>
<li>[guru] Got type error on course page
</li>
<li>Can&#039;t close window when view a lession
</li>
<li>Some error on Stripe payment
</li>
<li>Teacher &ndash; Create Course: Can&#039;t click to switch tabs
</li>
</ul>
<p align="center"><a href="https://guru.ijoomla.com/changelog">View Changelog</a> <a href="https://www.ijoomla.com/products">Get it Now</a> <a href="https://ijoomlademo.com/">Demo</a></p>";s:11:"description";s:194:"JA Sensei: Education Joomla template released and it&#8217;s FREE for all Guru pro and Ultimate plan users JA Sensei is pro Joomla template for education, online courses with LMS based [&#8230;]";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:81:"http://www.ijoomla.com/blog/dedicated-guru-template-released-guru-5-1-17-updated/";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:35:"http://www.ijoomla.com/blog/?p=2426";}s:7:"pubdate";i:1548146283;s:8:"category";a:2:{i:0;s:21:"iJoomla Announcements";i:1;s:18:"iJoomla Extensions";}s:7:"creator";a:1:{i:0;O:14:"NewsRss_Author":3:{s:4:"name";s:5:"iTeam";s:4:"link";N;s:5:"email";N;}}}}i:2;O:12:"NewsRss_Item":1:{s:4:"data";a:8:{s:5:"title";s:64:"Guru 5.1.16 updated for new features, improvements and bug fixes";s:7:"encoded";s:3191:"<p><img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/5.1.16/Joomla-lms-extension-Guru-5.1.16-updated.jpg" alt="guru 5.1.16 updates released" /><br />
Hi,<br />
This week we are releasing updates for <strong>Joomla LMS Extension: Guru 5.1.16</strong>. This release comes with new features in Teacher panel, advance payments overview and more new improvements and bug fixes. Guru 5.1.16 comes with 4 new features, 4 improvements and 8 bug fixes.<br />
Please Check release info below:</p>
<h3>New Features:</h3>
<ul>
<li>Teacher registration :A User can Become Teacher from frontend.</li>
<li>New menu type My Account : A new Menu item My account is added to login on site and show user account information in Guru</li>
<li>Mailchimp integration improvements: Each course has the option to Add Mailchimp API list to add subscriber based on course</li>
<p><img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/5.1.16/joomla-lms-extension-guru-mailchimp-setting-for-course.jpg" alt="guru 5.1.16 updates released for mailchimp  option in courses" /></p>
<li>CC JSON export: For payments in Guru, To use this Open Guru admin > Settings, API &ndash; Order details.<br />
A key must be generated and saved, then the URL created will give the admin all the CC details from orders in JSON format they can use with other programs.</li>
<p><img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/5.1.16/joomla-lms-extension-guru-order-api-settings.jpg" alt="guru 5.1.16 updates released for CC Json export" /></p>
</ul>
<h4>Improvements</h4>
<ul>
<li>Teacher Registration Menu : Registration menu will show direct registration form.</li>
<li>Incorrect average quiz score : Improvement in average quiz score calculation</li>
<li>Add Language selection option for Guru Course Category: Each category has option to select language package install in Joomla.</li>
<p><img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/5.1.16/joomla-lms-extension-guru-category-language-setting.jpg" alt="guru 5.1.16 updates released for multi language support in category" /></p>
<li>Back end: New Page Certificate List : A new certificate page added to see certificate reports</li>
<p><img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/5.1.16/joomla-lms-extension-guru-list-certificate.jpg" alt="guru 5.1.16 updates for certificate overview" />
</ul>
<h4>Bug fixes</h4>
<ul>
<li>[Veritrans] Rollback URL is wrong
</li>
<li>[Veritrans] Payment success but Status Payment is not changed
</li>
<li>Veritrans Payment: Course still in the Cart event Payment done
</li>
<li>Veritrans Payment: Use Promo Code, still have to pay the original price.
</li>
<li>Number question of final exam is wrong
</li>
<li>Certificate List: The list might be add link to some attribute
</li>
<li>Certificate List: get error when click to view PDF
</li>
<li>[Veritrans] Payment with Promo code is not count correct
</li>
</ul>
<p align="center"><a href="https://guru.ijoomla.com/changelog">View Changelog</a> <a href="https://www.ijoomla.com/products">Get it Now</a> <a href="https://ijoomlademo.com/">Demo</a></p>";s:11:"description";s:198:"Hi, This week we are releasing updates for Joomla LMS Extension: Guru 5.1.16. This release comes with new features in Teacher panel, advance payments overview and more new improvements and [&#8230;]";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:92:"http://www.ijoomla.com/blog/guru-5-1-16-updated-for-new-features-improvements-and-bug-fixes/";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:35:"http://www.ijoomla.com/blog/?p=2414";}s:7:"pubdate";i:1545137142;s:8:"category";a:1:{i:0;s:18:"iJoomla Extensions";}s:7:"creator";a:1:{i:0;O:14:"NewsRss_Author":3:{s:4:"name";s:5:"iTeam";s:4:"link";N;s:5:"email";N;}}}}i:3;O:12:"NewsRss_Item":1:{s:4:"data";a:8:{s:5:"title";s:69:"Guru 5.1.15 updated for new payment plugin, improvement and bug fixes";s:7:"encoded";s:3194:"<p><img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/5.1.15/Joomla-lms-extension-Guru-5.1.15-updated.jpg" alt="guru 5.1.15 updates released" /><br />
Hi,<br />
This weekend iJoomla team released updates for <strong>Joomla LMS Extension: Guru 5.1.15</strong>. This release has major improvements in the payment section and Teachers section. Guru 5.1.15 comes with a new Payment plugin : Paypal Pro, certificate view for teachers with an improvement and 5 bug fixes.<br />
Please Check release info below:</p>
<h3>New payment plugin</h3>
<h4>1. Paypal Pro</h4>
<p>Guru introduced a new payment plugin for PayPal payments, Before Guru supports PayPal plugin that required users to have a PayPal account, Paypal Pro payment plugin allows to pay directly from the credit card without PayPal accounts, It will give a hassle-free buying experience.</p>
<p><img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/5.1.15/paypal-pro-payment-plugin-guru-ijoomla.jpg" alt="guru 5.1.15 update for paypal pro payment plugin" /></p>
<h3>New Feature and improvement</h3>
<h4>2. Strip Payment plugin updated</h4>
<p>Strip payment plugin is updated for providing additional user details, So when user will pay using strip he needs to provide his name, addresses information, that makes the payments more secure.</p>
<p><img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/5.1.15/strip-payment-plugin-guru-ijoomla.jpg" alt="guru 5.1.15 updates for strip payment plugin" /></p>
<p>Now Guru has a total of <strong>8 payment plugins</strong> including Paypal and offline payment plugin that comes with Guru installation package. Additional 5 payment plugin can be downloaded from Payment plugins section <a href="https://www.joomlart.com/member/downloads/ijoomla/guru/guru-payment-plugins">here</a><br />
<img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/5.1.15/guru-paymentr-plugins.jpg" alt="Joomla LMS Extension - Guru payment plugins" /></p>
<h4>3. Certificate Reports</h4>
<p>Guru LMS Joomla extension variety of features to give a complete LMS experience, in latest update we have added an additional tab for a teacher to see the student&rsquo;s certificate reports. With this section, teachers can log in on frontned and view the students&rsquo; certificate reports.</p>
<p><strong> Note:</strong> Teacher must have a course that uses certificates</p>
<p><img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/5.1.15/certificate-option-for-teachers.jpg" alt="guru 5.1.15 updates for strip payment plugin" /></p>
<h3>Bug fixes</h3>
<ul>
<li>Stripe Payment: Disable the Checkout button to avoid error</li>
<li>Stripe Payment: Course still in the Cart event Payment done</li>
<li>Certificate: Get error when viewing details</li>
<li>Certificate: Some option of Course Information doesn&rsquo;t work when student get the certificate</li>
<li>Backend: Payment link to the wrong plugin</li>
</ul>
<p align="center"><a href="https://guru.ijoomla.com/changelog">View Changelog</a> <a href="https://www.ijoomla.com/products">Get it Now</a> <a href="https://ijoomlademo.com/">Demo</a></p>";s:11:"description";s:201:"Hi, This weekend iJoomla team released updates for Joomla LMS Extension: Guru 5.1.15. This release has major improvements in the payment section and Teachers section. Guru 5.1.15 comes with a [&#8230;]";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:97:"http://www.ijoomla.com/blog/guru-5-1-15-updated-for-new-payment-plugin-improvement-and-bug-fixes/";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:35:"http://www.ijoomla.com/blog/?p=2402";}s:7:"pubdate";i:1542253864;s:8:"category";a:2:{i:0;s:21:"iJoomla Announcements";i:1;s:18:"iJoomla Extensions";}s:7:"creator";a:1:{i:0;O:14:"NewsRss_Author":3:{s:4:"name";s:5:"iTeam";s:4:"link";N;s:5:"email";N;}}}}i:4;O:12:"NewsRss_Item":1:{s:4:"data";a:8:{s:5:"title";s:68:"Ad Agency 6.0.20 updated or new features, improvements and bug fixes";s:7:"encoded";s:2159:"<p><img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/joomla-ad-extension-adagency-6.0.20-released.jpg" alt="Ad Agency 6.0.20 updated or new features, improvements and a bug fix" /><br />
Hi,<br />
This weekend we are releasing updates for <strong>Joomla Ad Extension: Ad Agency 6.0.20</strong> for major new features and improvements. This release contains the new features for the HTML 5 Ads type and Access level control for Ad packages.<br />
Please Check release info below:</p>
<h3>New Features</h3>
<h4>1. Access level for Ad packages </h4>
<p>Ad Agency 6.0.20 comes with a new access level for the ad packages, Now you can restrict different Joomla user groups to access the ad packages. You can select single or multiple Joomla user groups.</p>
<p><img src="http://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/ad-agency-packages.jpg" alt="Ad Agency 6.0.20 updated or new features, improvements and a bug fix" /></p>
<p>To Check this option on Ad Agency extension, Login to Joomla admin page > Components > Ad Agency Pro > Managers > Packages > Creat a new package or open Already exist ad package.</p>
<h4>2. HTML 5 Ads</h4>
<p>This is one of the important features that allows creating HTML5 ads using Ad Agency. We added a dedicated option to add this type of ad.<br />
To create a new Ad with HTML5 option Components > Ad Agency Pro > Add new ads > HTML5 . Put the html5 ad URL in the Properties tab.</p>
<p><img src="http://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/ad-agency-html5-ads.jpg" alt="guru 5.1.14 update for course open graph tags" /></p>
<h3>Improvements</h3>
<ul>
<li>Daily List Result in Reports &ndash; Overview page</li>
<li>Support for multiple URLs in affiliate ads</li>
</ul>
<h3>Bug Fixes:</h3>
<ul>
<li>Fixed some issue in Zone module </li>
<li>JavaScript to pop up ads</li>
<li>Fixed some issues in Zone modul</li>
<li>Fixed some issues in Geo-targeting settings</li>
</ul>
<p align="center"><a href="https://guru.ijoomla.com/changelog">View Changelog</a> <a href="https://www.ijoomla.com/products">Get it Now</a> <a href="https://ijoomlademo.com/">Demo</a></p>";s:11:"description";s:188:"Hi, This weekend we are releasing updates for Joomla Ad Extension: Ad Agency 6.0.20 for major new features and improvements. This release contains the new features for the HTML 5 [&#8230;]";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:96:"http://www.ijoomla.com/blog/ad-agency-6-0-20-updated-or-new-features-improvements-and-bug-fixes/";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:35:"http://www.ijoomla.com/blog/?p=2388";}s:7:"pubdate";i:1541130129;s:8:"category";a:2:{i:0;s:21:"iJoomla Announcements";i:1;s:18:"iJoomla Extensions";}s:7:"creator";a:1:{i:0;O:14:"NewsRss_Author":3:{s:4:"name";s:5:"iTeam";s:4:"link";N;s:5:"email";N;}}}}i:5;O:12:"NewsRss_Item":1:{s:4:"data";a:8:{s:5:"title";s:50:"Guru 5.1.14 updated for improvements and bug fixes";s:7:"encoded";s:2227:"<p><img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/Joomla-lms-extension-Guru5.1.14-updated.jpg" alt="guru 5.1.14 update released" /><br />
Hi,<br />
This weekend we are releasing updates for <strong>Joomla LMS Extension: Guru 5.1.14</strong>. This release comes with 2 improvements and 7 bug fixes. The updates contains major improvement in course view to allow open graph tags for social sharing.<br />
Please Check release info below:</p>
<h3>Improvements</h3>
<h4>1. Open Graph tag improvements for course page</h4>
<p> Guru 5.1.14 updates come with major changes in Guru edit course view, Now Admin can add open graph tags for the course view to allow user share the course view on the social platform (Facebook). To add the custom open graph tags for each course you need to edit the Course > Course/Avatar. </p>
<p><img src="http://www.ijoomla.com/blog/wp-content/uploads/2018/10/Joomla-lms-extension-Guru5.1.14.jpg" alt="guru 5.1.14 update for course open graph tags" /></p>
<p> Here is list of supported Open Graph tags</p>
<ul>
<li>OG Title : The title of your Course as it should appear within the graph</li>
<li>OG Type: The type of your course &ldquo;video.movie</li>
<li>OG Image: An image URL which should represent your course within the graph.</li>
<li>OG URL: he canonical URL of your course that will be used</li>
<li>OG Desc:  A one to two sentence description of your course</li>
</ul>
<h4>2. Course edit: Add Exercise must have noticed for file type acceptance</h4>
<h3>Bug Fixes:</h3>
<ul>
<li>Time issue on certificate </li>
<li>Set Course category to Public, can&#039;t Category in Course Category list</li>
<li>Attach media in quiz not working</li>
<li>Pending orders shouldn&#039;t count into Revenue</li>
<li>jwallvideos Plugin compatible</li>
<li>Backend add audio</li>
<li>Media: Save &amp; new doesn&#039;t validate the Teacher field</li>
</ul>
<p align="center"><a href="https://guru.ijoomla.com/changelog">View Changelog</a> <a href="https://www.ijoomla.com/products">Get it Now</a></p>
<p>Get Flat 25% OFF on All iJoomla and Network sites Proucts. Use discount code : HALLOWEEN (Limited Time Offer)<br />
<a href="https://www.ijoomla.com/products">Buy Now</a></p>";s:11:"description";s:195:"Hi, This weekend we are releasing updates for Joomla LMS Extension: Guru 5.1.14. This release comes with 2 improvements and 7 bug fixes. The updates contains major improvement in course [&#8230;]";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:79:"http://www.ijoomla.com/blog/guru-5-1-14-updated-for-improvements-and-bug-fixes/";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:35:"http://www.ijoomla.com/blog/?p=2378";}s:7:"pubdate";i:1540534149;s:8:"category";a:2:{i:0;s:21:"iJoomla Announcements";i:1;s:18:"iJoomla Extensions";}s:7:"creator";a:1:{i:0;O:14:"NewsRss_Author":3:{s:4:"name";s:5:"iTeam";s:4:"link";N;s:5:"email";N;}}}}i:6;O:12:"NewsRss_Item":1:{s:4:"data";a:8:{s:5:"title";s:63:"Guru 5.1.13 updated for new features, improvement and bug fixes";s:7:"encoded";s:2605:"<p><img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/Joomla-lms-extension-Guru5.1.13-updated.jpg" alt="guru 5.1.13 update released" /><br />
Hi,<br />
We are happy to announce the release for <strong>Joomla LMS Extension: Guru 5.1.13</strong>. This release comes with new features, improvement and bug fixes. The release contains a new search module to search the Guru courses with more features improvements and bug fixes.<br />
Please Check release info below:</p>
<h3>New Feature details:</h3>
<ul>
<li>New Feature: New Search Module Guru courses</li>
<li>New Feature: More options for how many time that quiz is taken</li>
<li>New Feature: change &#039;status&#039; actions</li>
</ul>
<p><strong>New Search Module: </strong> In this release we added a new module : Guru Search Courses. That allow stuents to search courses from different category with sorting options</p>
<p><img src="http://www.ijoomla.com/blog/wp-content/uploads/2018/09/guru-search-course-module.jpg" alt="guru 5.1.13 update for course module" /></p>
<p><strong>More options for how many time that quiz is taken:</strong> This option allow admin to add the limit for how many time a quiz can be taken. You can set the option via Edit a quiz.</p>
<p><img src="https://www.ijoomla.com/blog/wp-content/uploads/2018/09/quiz-limit-option-guru.jpg" alt="guru 5.1.13 update for quiz limit" /></p>
<p><strong>Change &#039;status&#039; actions:</strong> This feature allows admin to publish the course only for the older user, Once a course is not active it will work only for older students and not show to a new student.</p>
<ul>
<li> Improvement : Add &quot;+ Copy Question&quot; button inside quizz edit form</li>
</ul>
<h3>Bug Fixes:</h3>
<ul>
<li>Bug fixes: Front-end: No message after finish order</li>
<li>Bug fixes: Course: Can&#039;t view detail if Set ACL to Course&#039;s Category </li>
<li>Bug fixes: Teacher Page: Get JS error after login &amp; go to Teacher Page</li>
<li>Bug fixes: Course detail: Get error when try to add Exercise</li>
<li>Bug fixes: JS error when add quizzes</li>
<li>Bug fixes: Show password not working in login form</li>
<li>Bug fixes: change certificate path</li>
<li>Bug fixes: very slow load on category view</li>
<li>Bug fixes: Time left not reset each time retake the quiz
</li>
</ul>
<p align="center"><a href="https://guru.ijoomla.com/changelog">View Changelog</a> <a href="https://www.ijoomla.com/products">Get it Now</a></p>
<p>Access 7 clubs at price of 1 (Special discounts on Upgrades)<br />
<a href="https://www.joomlart.com/ultimate-developer">Buy Now</a></p>";s:11:"description";s:188:"Hi, We are happy to announce the release for Joomla LMS Extension: Guru 5.1.13. This release comes with new features, improvement and bug fixes. The release contains a new search [&#8230;]";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:91:"http://www.ijoomla.com/blog/guru-5-1-13-updated-for-new-features-improvement-and-bug-fixes/";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:35:"http://www.ijoomla.com/blog/?p=2368";}s:7:"pubdate";i:1538113115;s:8:"category";a:2:{i:0;s:21:"iJoomla Announcements";i:1;s:18:"iJoomla Extensions";}s:7:"creator";a:1:{i:0;O:14:"NewsRss_Author":3:{s:4:"name";s:5:"iTeam";s:4:"link";N;s:5:"email";N;}}}}i:7;O:12:"NewsRss_Item":1:{s:4:"data";a:8:{s:5:"title";s:49:"Guru 5.1.11 updated for new feature and bug fixes";s:7:"encoded";s:1675:"<p><img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/joomla-lms-extension-guru-5.1.11-updated.jpg" alt="joomla lms extnesion guru 5.1.11 update released" /><br />
Hi,<br />
We are happy to announce the release for <strong>Joomla LMS Extension: Guru 5.1.11</strong>. This update contains access level for Guru categories, that allow Joomla user groups to access Guru categories with more improvement and bug fixes.</p>
<p><span></span><br />
Please Check release info below:</p>
<h3>New Feature details:</h3>
<ul>
<li>New Feature: Option to apply access level on default Joomla user group for Guru course categories.</li>
</ul>
<p>This option allow you to set different access level to guru categories, you can disallow a user group to not accesss the courses.</p>
<p><img src="https://static.joomlart.com/images/blog/2018/weekend-updates/ijoomla/joomla-lms-extension-guru-category-acl.jpg"alt="Joomla LMS Extension Guru access level" /></p>
<h3>Improvement and bug fixes details:</h3>
<ul>
<li>Fixed bug: teacher register form hidden on mobile</li>
<li>Fixed issue &ndash; can not upload xls doc files</li>
<li>Enhancement: separate administrator editors for certificates, one for each HTML and PDF views</li>
<li>Added price &ldquo;space&rdquo; separator for thousands and decimals</li>
</ul>
<p align="center"><a href="https://guru.ijoomla.com/changelog">View Changelog</a> <a href="https://www.ijoomla.com/products">Get it Now</a></p>
<p>Get everything from JoomlArt, Gavick, JomSocial, iJoomla and Joomlabamboo for just $299 or less. (Special discounts on Upgrades)</p>
<p><a href="https://www.joomlart.com/ultimate-developer">Buy Now</a></p>";s:11:"description";s:189:"Hi, We are happy to announce the release for Joomla LMS Extension: Guru 5.1.11. This update contains access level for Guru categories, that allow Joomla user groups to access Guru [&#8230;]";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:78:"http://www.ijoomla.com/blog/guru-5-1-11-updated-for-new-feature-and-bug-fixes/";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:35:"http://www.ijoomla.com/blog/?p=2361";}s:7:"pubdate";i:1535106433;s:8:"category";a:2:{i:0;s:21:"iJoomla Announcements";i:1;s:18:"iJoomla Extensions";}s:7:"creator";a:1:{i:0;O:14:"NewsRss_Author":3:{s:4:"name";s:5:"iTeam";s:4:"link";N;s:5:"email";N;}}}}i:8;O:12:"NewsRss_Item":1:{s:4:"data";a:8:{s:5:"title";s:56:"Joomla LMS Extension : Guru 5.1.10 updated for bug fixes";s:7:"encoded";s:1157:"<p><img src="http://www.ijoomla.com/blog/wp-content/uploads/2018/08/Joomla-lms-extension-guru-5.1.10-1.jpg" alt="Joomla LMS Extension guru 5.1.10 updated" /><br />
Hi,<br />
This week we are releasing updates for <strong>Joomla LMS Extension: Guru 5.1.10</strong>.This release is for Guru Pro and Guru light version, The update contains the bug fixes for wrong redirection in course page, Offline payment plugin and some more changes to fix course layouts issues.<br />
<span></span><br />
Please Check release info below:</p>
<h3>Bug Fixes details:</h3>
<ul>
<li>Fixed issue: wrong student redirect page after course completed</li>
<li>Changes for Offline Payment plugin</li>
<li>Changes and fixes for Jump buttons</li>
<li>Fixed links for help section on Guru backend</li>
</ul>
<p align="center"><a href="https://guru.ijoomla.com/changelog">View Changelog</a> <a href="https://www.ijoomla.com/products">Get it Now</a></p>
<p>Get everything from JoomlArt, Gavick, JomSocial, iJoomla, Shape5, DTHDevelopment and Joomlabamboo for just $299 or less. (Special discounts on Upgrades)</p>
<p><a href="https://www.joomlart.com/ultimate-developer">Buy Now</a></p>";s:11:"description";s:183:"Hi, This week we are releasing updates for Joomla LMS Extension: Guru 5.1.10.This release is for Guru Pro and Guru light version, The update contains the bug fixes for wrong [&#8230;]";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:83:"http://www.ijoomla.com/blog/joomla-lms-extension-guru-5-1-10-updated-for-bug-fixes/";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:35:"http://www.ijoomla.com/blog/?p=2350";}s:7:"pubdate";i:1533781977;s:8:"category";a:2:{i:0;s:21:"iJoomla Announcements";i:1;s:18:"iJoomla Extensions";}s:7:"creator";a:1:{i:0;O:14:"NewsRss_Author":3:{s:4:"name";s:5:"iTeam";s:4:"link";N;s:5:"email";N;}}}}i:9;O:12:"NewsRss_Item":1:{s:4:"data";a:8:{s:5:"title";s:62:"Guru 5.1.9 updated for new feature, improvements and bug fixes";s:7:"encoded";s:2927:"<p><img src="https://www.ijoomla.com/blog/wp-content/uploads/2018/07/guru5.1.9-1.jpg" alt="guru 5.1.9 update released" /><br />
Hi,<br />
We are happy to announce the release for <strong>Joomla LMS Extension: Guru 5.1.9</strong>. It is an important update release with new feature, improvements and bug fixes that are reported after the last release of Guru. We would like to thank you for the positive feedback and reports in version 5.1.8.</p>
<p>In this version, we have added a new feature in the Guru course module to control the courses view, some improvements in backend configuration for of students, courses and quiz view with 8 bug fixes.<br />
<span></span><br />
Please Check release info below:</p>
<h3>New Feature details:</h3>
<ul>
<li>New Feature: Option to display courses based on categories in Guru Courses Module</li>
</ul>
<p>This option allow you to select the category for your choice to display the courses via course module. before the module display all the courses from all categories. To use this feature download the latest version 3.0.5 of Guru course module and install it on your Joomla site.</p>
<p><img src="https://www.ijoomla.com/blog/wp-content/uploads/2018/07/guru-course-module.jpg"alt="guru 5.1.9 update released" /></p>
<h3>Improvement details:</h3>
<ul>
<li>Improvement &ndash; Limit number of users that can enroll for free  to a course</li>
</ul>
<p>This option allow restricting no. of free users to access the course free of cost. Now you can limit the no. of people to join the course free, that helps you to make an offer for free seats. To check this option navigate to Guru > courses > edit a course > Set a limit number for students that can enroll for free to this course  </p>
<p><img src="https://www.ijoomla.com/blog/wp-content/uploads/2018/07/edit-guru-course.jpg"alt="guru 5.1.9 update released" /></p>
<ul>
<li>Improvement &ndash; Remove Super User group for add/edit new students on backend.</li>
<li>Improvement &ndash; Load JS files asynchronous</li>
</ul>
<h3>Bug Fixes details:</h3>
<ul>
<li>Fixed some issues with student retake final exam</li>
<li>Fixed issue with JCE Editor don&rsquo;t save media text</li>
<li>Fixed issue: can not add media text from lesson  page, in front-end</li>
<li>Fixed issue: wrong number from quiz attempts left.</li>
<li>Fixed some issues with quizzes languages on sites  multi language</li>
<li>Fixed issue: can not order course lessons in  course</li>
<li>Fixed issue: user do not receive the  registration email</li>
<li>New language variables for quiz retake action</li>
</ul>
<p align="center"><a href="https://guru.ijoomla.com/changelog">View Changelog</a> <a href="https://www.ijoomla.com/products">Get it Now</a></p>
<p>Get everything from JoomlArt, Gavick, JomSocial, iJoomla and Joomlabamboo for just $299 or less. (Special discounts on Upgrades)</p>
<p><a href="https://www.joomlart.com/ultimate-developer">Buy Now</a></p>";s:11:"description";s:185:"Hi, We are happy to announce the release for Joomla LMS Extension: Guru 5.1.9. It is an important update release with new feature, improvements and bug fixes that are reported [&#8230;]";s:4:"link";a:1:{s:9:"alternate";a:1:{i:0;s:90:"http://www.ijoomla.com/blog/guru-5-1-9-updated-for-new-feature-improvements-and-bug-fixes/";}}s:4:"guid";a:2:{s:9:"permalink";b:0;s:4:"data";s:35:"http://www.ijoomla.com/blog/?p=2334";}s:7:"pubdate";i:1532425563;s:8:"category";a:2:{i:0;s:21:"iJoomla Announcements";i:1;s:18:"iJoomla Extensions";}s:7:"creator";a:1:{i:0;O:14:"NewsRss_Author":3:{s:4:"name";s:5:"iTeam";s:4:"link";N;s:5:"email";N;}}}}}s:3:"url";s:37:"http://www.ijoomla.com/blog/feed/rss/";s:13:"last-modified";s:29:"Fri, 01 Mar 2019 03:16:19 GMT";s:4:"etag";s:34:""02caa3e9bb7801960c19a802eba57364"";}