DROP TABLE IF EXISTS `#__studentsimporter_schools`;
DROP TABLE IF EXISTS `#__studentsimporter_subscriptions`;
DROP TABLE IF EXISTS `#__studentsimporter_teachers`;
DROP TABLE IF EXISTS `#__studentsimporter_students`;

DELETE FROM `#__content_types` WHERE (type_alias LIKE 'com_studentsimporter.%');