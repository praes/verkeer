<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Studentsimporter
 * @author     Jelger Visser <jelger@praes.nl>
 * @copyright  2016 Internetbureau Praes
 * @license    GNU General Public License versie 2 of hoger; Zie LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_studentsimporter/assets/css/studentsimporter.css');
$document->addStyleSheet(JUri::root() . 'administrator/components/com_studentsimporter/assets/css/uikit.css');
$document->addStyleSheet(JUri::root() . 'media/com_studentsimporter/css/list.css');

$user      = JFactory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_studentsimporter');
$saveOrder = $listOrder == 'a.`ordering`';

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_studentsimporter&task=subscriptions.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'subscriptionList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function () {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	};

	jQuery(document).ready(function () {
		jQuery('#clear-search-button').on('click', function () {
			jQuery('#filter_search').val('');
			jQuery('#adminForm').submit();
		});
	});

	window.toggleField = function (id, task, field) {

		var f = document.adminForm,
			i = 0, cbx,
			cb = f[ id ];

		if (!cb) return false;

		while (true) {
			cbx = f[ 'cb' + i ];

			if (!cbx) break;

			cbx.checked = false;
			i++;
		}

		var inputField   = document.createElement('input');
		inputField.type  = 'hidden';
		inputField.name  = 'field';
		inputField.value = field;
		f.appendChild(inputField);

		cb.checked = true;
		f.boxchecked.value = 1;
		window.submitform(task);

		return false;
	};

</script>

<?php

// Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar))
{
	$this->sidebar .= $this->extra_sidebar;
}

?>

<form action="<?php echo JRoute::_('index.php?option=com_studentsimporter&view=subscriptions'); ?>" method="post"
	name="adminForm" id="adminForm">
	
	<?php if (!empty($this->sidebar)): ?>
	
		<div id="j-sidebar-container" class="span2">
		
			<?php echo $this->sidebar; ?>
	
		</div>
	
		<div id="j-main-container" class="span10">
	
	<?php else : ?>
	
		<div id="j-main-container">
	
	<?php endif; ?>
	
			<table class="uk-table uk-table-striped uk-table-hover" id="subscriptionList">
			
				<thead>
				
					<tr>
					
						<th>Onderwijsinstelling</th>
						
						<th>afloopdatum van licentie</th>
						
						<td></td>
					
					</tr>
				
				</thead>
				
				<tbody>
			
					<?php foreach($this->subscriptions AS $i=> $licentie) : ?>
				
						<tr>
					
							<td><?php echo $licentie->title; ?></td>
							
							<td>
								<?php if($licentie->license_date == "0000-00-00") : ?>
							
									<span>Licentie niet actief</span>
									
								<?php elseif($licentie->license_date == NULL) : ?>
									
									<span>Er zijn nog geen gebruikers gekoppeld aan deze licentie.</span>
									
								<?php else : ?>
								
									<?php echo $licentie->license_date; ?>
								
								<?php endif; ?>
								
							</td>
							
							<td><a href="index.php?option=com_studentsimporter&view=subscription&layout=edit&oid=<?php echo $licentie->uid; ?>" class="uk-button uk-button-success">Beheren</a></td>
						
						</tr>
				
					<?php endforeach; ?>
										
				</tbody>
			

			</table>
	
	</div>
	
</form>        
