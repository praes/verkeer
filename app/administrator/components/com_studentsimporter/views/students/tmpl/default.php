<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Studentsimporter
 * @author     Jelger Visser <jelger@praes.nl>
 * @copyright  2016 Internetbureau Praes
 * @license    GNU General Public License versie 2 of hoger; Zie LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_studentsimporter/assets/css/studentsimporter.css');
$document->addStyleSheet(JUri::root() . 'administrator/components/com_studentsimporter/assets/css/uikit.css');
$document->addStyleSheet(JUri::root() . 'media/com_studentsimporter/css/list.css');


JFormHelper::addFieldPath(JPATH_COMPONENT . '/models/fields');
$teacher = JFormHelper::loadFieldType('Filterteacher', false);
$teacherOptions = $teacher->getOptions();

$school = JFormHelper::loadFieldType('Filterschool', false);
$schoolOptions = $school->getOptions();

$user      = JFactory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_studentsimporter');
$saveOrder = $listOrder == 'a.`ordering`';

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_studentsimporter&task=students.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'studentList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function () {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	};

	jQuery(document).ready(function () {
		jQuery('#clear-search-button').on('click', function () {
			jQuery('#filter_search').val('');
			jQuery('#adminForm').submit();
		});
	});

	window.toggleField = function (id, task, field) {

		var f = document.adminForm,
			i = 0, cbx,
			cb = f[ id ];

		if (!cb) return false;

		while (true) {
			cbx = f[ 'cb' + i ];

			if (!cbx) break;

			cbx.checked = false;
			i++;
		}

		var inputField   = document.createElement('input');
		inputField.type  = 'hidden';
		inputField.name  = 'field';
		inputField.value = field;
		f.appendChild(inputField);

		cb.checked = true;
		f.boxchecked.value = 1;
		window.submitform(task);

		return false;
	};

</script>


<?php

// Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar))
{
	$this->sidebar .= $this->extra_sidebar;
}

?>
    
<div id="j-sidebar-container" class="span2">

	<?php echo $this->sidebar; ?>
	
</div>

<div id="j-main-container" class="span10">

	<div class="student-control uk-margin-large-bottom">

		<a href="index.php?option=com_studentsimporter&view=student&layout=edit" class="uk-button uk-button-success uk-button-large uk-margin-right">Leerling toevoegen</a>
		
		<a href="index.php?option=com_studentsimporter&view=student&layout=import" class="uk-button uk-button-success uk-button-large uk-margin-large-right">.CSV bestand importeren</a>
		
		<a target="_blank" href="/images/csv/csv-format-leerlingen.csv" class="uk-button uk-button-primary uk-button-large uk-margin-right" download>Download het .CSV format</a>

		<a target="_blank" href="/images/csv/csv-format-leerlingen-demo.csv" class="uk-button uk-button-primary uk-button-large" download>Download een .CSV met demo-data</a>
		
	</div>

	<form action="<?php echo JRoute::_('index.php?option=com_studentsimporter&view=students'); ?>" method="post"
		name="adminForm" id="adminForm">
		
			<div id="filter-bar" class="btn-toolbar">

				<div class="btn-group pull-right hidden-phone">
					<label for="limit"
						   class="element-invisible">
						<?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?>
					</label>
					<?php echo $this->pagination->getLimitBox(); ?>
				</div>
				
				<select name="filter_filterteacher" class="inputbox uk-margin-bottom" onchange="this.form.submit()">
					<option value=""> - Selecteer docent - </option>
					<?php echo JHtml::_('select.options', $teacherOptions, 'value', 'text', $this->state->get('filter.filterteacher'));?>
				</select>
				
				<select name="filter_filterschool" class="inputbox uk-margin-bottom" onchange="this.form.submit()">
					<option value=""> - Selecteer school - </option>
					<?php echo JHtml::_('select.options', $schoolOptions, 'value', 'text', $this->state->get('filter.filterschool'));?>
				</select>
				
				
				
			</div>
	
	<div class="student-list">
	
		<table class="uk-table uk-table-striped uk-table-hover">
		
			<thead>
			
				<tr>
				
					<th>gebruikers ID</th>
					
					<th>Naam</th>
				
					<th>Klas</th>
					
					<th>Gerelateerde docent</th>
					
					<th>Onderwijsinstelling</th>
					
					<th></th>
				
				</tr>
			
			</thead>
			
				<tfoot>
				<tr>
					<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
				</tfoot>
			
			<tbody>
			
				<?php foreach($this->items as $i => $student) : ?>
				
					<?php foreach($this->teachers as $i => $teacher) : ?>
					
						<?php if($teacher->userid == $student->id) : ?>
			
							<tr>
						
								<td><?php echo $student->id; ?></td>
								
								<td><?php echo $student->firstname; ?> <?php echo $student->insertion; ?> <?php echo $student->lastname; ?></td>
								
								<td><?php echo $student->group; ?></td>
								
								<td><?php echo $teacher->name; ?></td>
								
								<td><?php echo $student->company; ?></td>
								
								<td><a class="uk-button uk-button-mini uk-button-danger" href="index.php?option=com_studentsimporter&task=student.remove&id=<?php echo $student->id; ?>">Verwijderen</a></td>
							
							</tr>
						
						<?php break; ?>
						
						<?php endif; ?>
						
					<?php endforeach; ?>
			
				<?php endforeach; ?>
			
			</tbody>
			
		</div>
	
	</div>
	
	
	
			<input type="hidden" name="task" value=""/>
			<input type="hidden" name="boxchecked" value="0"/>
			<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
			<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
	
	<?php echo JHtml::_('form.token'); ?>
	
	</form>
	
</div>
