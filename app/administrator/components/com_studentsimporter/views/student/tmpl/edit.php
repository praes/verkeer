<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Studentsimporter
 * @author     Jelger Visser <jelger@praes.nl>
 * @copyright  2016 Internetbureau Praes
 * @license    GNU General Public License versie 2 of hoger; Zie LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_studentsimporter/css/form.css');
$document->addStyleSheet(JUri::root() . 'administrator/components/com_studentsimporter/assets/css/uikit.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'student.cancel') {
			Joomla.submitform(task, document.getElementById('student-form'));
		}
		else {
			
			if (task != 'student.cancel' && document.formvalidator.isValid(document.id('student-form'))) {
				
				Joomla.submitform(task, document.getElementById('student-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_studentsimporter&task=student.save'); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="student-form" class="form-validate uk-form uk-form-large">

	<div class="form-horizontal">

			<div class="span10 form-horizontal">
				<fieldset class="adminform">
				
					<h3 class="uk-margin-large-bottom">Voeg een leerling toe</h3>
					
					<div class="uk-width-1-3">
				
						<div class="uk-grid">
						
							<div class="uk-width-1-1 uk-margin-bottom">
					
								<?php echo $this->form->getInput("firstname"); ?>
								
							</div>
							
							<div class="uk-width-1-1 uk-margin-bottom">
					
								<?php echo $this->form->getInput("insertion"); ?>
					
							</div>
							
							<div class="uk-width-1-1 uk-margin-bottom">
							
								<?php echo $this->form->getInput("lastname"); ?>
							
							</div>
					
							<div class="uk-width-1-1 uk-margin-bottom">
							
								<?php echo $this->form->getInput("email"); ?>
							
							</div>
							
							<div class="uk-width-1-1 uk-margin-bottom">
							
								<h4>Selecteer de docent</h4>
							
								<?php echo $this->form->getInput("getteachers"); ?>
							
							</div>

							<div class="uk-width-1-1 uk-margin-bottom">
							
								<h4>Selecteer de school</h4>
							
								<?php echo $this->form->getInput("getschools"); ?>
							
							</div>		

							<div class="uk-width-1-1 uk-margin-bottom">
							
								<h4>Selecteer de lesbrief</h4>
							
								<?php echo $this->form->getInput("getlessons"); ?>
							
							</div>
					
							<div class="uk-width-1-1 uk-margin-bottom">
							
								<?php echo $this->form->getInput("group"); ?>
							
							</div>

						</div>
				
					</div>
					
					<input type="hidden" name="jform[password]" value="$2y$10$ouJWEBwFJfFoXYfvghHN/O9aLHpBrm0bBIRau4Z19uEEMkHNEtwjC" />	
					
					<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
					<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
					<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
					<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
					<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

					<?php if(empty($this->item->created_by)){ ?>
						<input type="hidden" name="jform[created_by]" value="<?php echo JFactory::getUser()->id; ?>" />

					<?php } 
					else{ ?>
						<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />

					<?php } ?>
					<?php if(empty($this->item->modified_by)){ ?>
						<input type="hidden" name="jform[modified_by]" value="<?php echo JFactory::getUser()->id; ?>" />

					<?php } 
					else{ ?>
						<input type="hidden" name="jform[modified_by]" value="<?php echo $this->item->modified_by; ?>" />

					<?php } ?>

						<?php if ($this->state->params->get('save_history', 1)) : ?>
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
						</div>
						<?php endif; ?>
						
						
					<input type="hidden" name="option" value="com_studentsimporter"/>

					<input type="hidden" name="task" value="student.save"/>	
						
				</fieldset>
			</div>

		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
