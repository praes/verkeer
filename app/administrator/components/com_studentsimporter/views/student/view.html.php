<?php

/**
 * @version    CVS: 1.0.1
 * @package    Com_Studentsimporter
 * @author     Jelger Visser <jelger@praes.nl>
 * @copyright  2016 Internetbureau Praes
 * @license    GNU General Public License versie 2 of hoger; Zie LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class StudentsimporterViewStudent extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->item  = $this->get('Item');
		$this->form  = $this->get('Form');
		
		
		$teachers = JModelLegacy::getInstance('Teachers', 'StudentsimporterModel');
		
		$this->teachers = $teachers->getTeachers();
		
		

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$user  = JFactory::getUser();
		$isNew = ($this->item->id == 0);

		if (isset($this->item->checked_out))
		{
			$checkedOut = !($this->item->checked_out == 0 || $this->item->checked_out == $user->get('id'));
		}
		else
		{
			$checkedOut = false;
		}

		$canDo = StudentsimporterHelpersStudentsimporter::getActions();

		JToolBarHelper::title(JText::_('COM_STUDENTSIMPORTER_TITLE_STUDENT'), 'student.png');
		
		if($this->getLayout() == 'edit'){

			// If not checked out, can save the item.
			if (!$checkedOut && ($canDo->get('core.edit') || ($canDo->get('core.create'))))
			{
				JToolBarHelper::apply('student.apply', 'JTOOLBAR_APPLY');
				JToolBarHelper::save('student.save', 'JTOOLBAR_SAVE');
			}

			if (!$checkedOut && ($canDo->get('core.create')))
			{
				JToolBarHelper::custom('student.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
			}

			// If an existing item, can save to a copy.
			if (!$isNew && $canDo->get('core.create'))
			{
				JToolBarHelper::custom('student.save2copy', 'save-copy.png', 'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
			}

			// Button for version control
			if ($this->state->params->get('save_history', 1) && $user->authorise('core.edit')) {
				JToolbarHelper::versions('com_studentsimporter.student', $this->item->id);
			}

			if (empty($this->item->id))
			{
				JToolBarHelper::cancel('student.cancel', 'JTOOLBAR_CANCEL');
			}
			else
			{
				JToolBarHelper::cancel('student.cancel', 'JTOOLBAR_CLOSE');
			}
		
		}
		
	}
}
