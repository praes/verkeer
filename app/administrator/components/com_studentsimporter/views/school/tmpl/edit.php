<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Studentsimporter
 * @author     Jelger Visser <jelger@praes.nl>
 * @copyright  2016 Internetbureau Praes
 * @license    GNU General Public License versie 2 of hoger; Zie LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_studentsimporter/css/form.css');
$document->addStyleSheet(JUri::root() . 'administrator/components/com_studentsimporter/assets/css/uikit.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'school.cancel') {
			Joomla.submitform(task, document.getElementById('school-form'));
		}
		else {
			
			if (task != 'school.cancel' && document.formvalidator.isValid(document.id('school-form'))) {
				
				Joomla.submitform(task, document.getElementById('school-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_studentsimporter&task=school.save'); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="school-form" class="form-validate uk-form uk-form-large">


	<div class="form-horizontal">
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

					<h3 class="uk-margin-large-bottom">Nieuwe onderwijsinstelling toevoegen</h3>
				
					<?php echo $this->form->getInput("school"); ?>
				
				
					
					<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
					<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
					<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
					<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
					<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

					<?php if(empty($this->item->created_by)){ ?>
						<input type="hidden" name="jform[created_by]" value="<?php echo JFactory::getUser()->id; ?>" />

					<?php } 
					else{ ?>
						<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />

					<?php } ?>
					<?php if(empty($this->item->modified_by)){ ?>
						<input type="hidden" name="jform[modified_by]" value="<?php echo JFactory::getUser()->id; ?>" />

					<?php } 
					else{ ?>
						<input type="hidden" name="jform[modified_by]" value="<?php echo $this->item->modified_by; ?>" />

					<?php } ?>

						<?php if ($this->state->params->get('save_history', 1)) : ?>
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
						</div>
						<?php endif; ?>
				</fieldset>
			</div>
		</div>

		<input type="hidden" name="option" value="com_studentsimporter"/>

		<input type="hidden" name="task" value="school.save"/>	

		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
