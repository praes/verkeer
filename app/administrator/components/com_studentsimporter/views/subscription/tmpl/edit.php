<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Studentsimporter
 * @author     Jelger Visser <jelger@praes.nl>
 * @copyright  2016 Internetbureau Praes
 * @license    GNU General Public License versie 2 of hoger; Zie LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_studentsimporter/css/form.css');
$document->addStyleSheet(JUri::root() . 'administrator/components/com_studentsimporter/assets/css/uikit.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'subscription.cancel') {
			Joomla.submitform(task, document.getElementById('subscription-form'));
		}
		else {
			
			if (task != 'subscription.cancel' && document.formvalidator.isValid(document.id('subscription-form'))) {
				
				Joomla.submitform(task, document.getElementById('subscription-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_studentsimporter&task=subscription.license'); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="student-form" class="form-validate uk-form uk-form-large">
		
		<h1 class="uk-margin-large-bottom"><?php echo $this->subscription->title; ?></h1>
		
		
		<input type="hidden" name="oid" value="<?php echo JRequest::getVar('oid'); ?>">
		
		<button value="single" type="submit" name="single" class="uk-button uk-button-large uk-button-success">Licentie 1 jaar verlengen</button>
		
		<button value="double" type="submit" name="double" class="uk-button uk-button-large uk-button-success">Licentie 2 jaar verlengen</button>
		
		<button value="end" type="submit" name="end" class="uk-button uk-button-large uk-button-danger uk-margin-large-left">Licentie beëindigen</button>
		
		<a class="uk-button uk-button-large uk-button-primary" href="index.php?option=com_studentsimporter&view=subscriptions">Annuleren</a>
			
		<input type="hidden" name="option" value="com_studentsimporter"/>

		<input type="hidden" name="task" value="subscription.license"/>	
		
		<?php echo JHtml::_('form.token'); ?>

</form>
