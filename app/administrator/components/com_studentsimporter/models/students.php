<?php

/**
 * @version    CVS: 1.0.1
 * @package    Com_Studentsimporter
 * @author     Jelger Visser <jelger@praes.nl>
 * @copyright  2016 Internetbureau Praes
 * @license    GNU General Public License versie 2 of hoger; Zie LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Studentsimporter records.
 *
 * @since  1.6
 */
class StudentsimporterModelStudents extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'author_id', 'g.`author_id`',
				'company', 'g.company'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		
		//Filter (dropdown) teacher
		$teacher= $app->getUserStateFromRequest($this->context.'.filter.filterteacher', 'filter_filterteacher', '', 'string');
		$this->setState('filter.filterteacher', $teacher);

		//Filter (dropdown) school
		$school= $app->getUserStateFromRequest($this->context.'.filter.filterschool', 'filter_filterschool', '', 'string');
		$this->setState('filter.filterschool', $school);
		
		// Load the parameters.
		$params = JComponentHelper::getParams('com_studentsimporter');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');
		$id .= ':' . $this->getState('filter.filterteacher'); 
		$id .= ':' . $this->getState('filter.filterschool'); 

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		$db    = $this->getDbo();
		
		$query = $db->getQuery(true);
		
		$query->select('g.*, bc.userid, bc.author_id')
			  ->from('#__guru_customer AS g')
			  ->join('LEFT', '#__user_usergroup_map AS u ON u.user_id = g.id')
			  ->join('LEFT', '#__guru_buy_courses AS bc ON bc.userid = g.id')
			  ->where('u.group_id = 15');
			
		$db->setQuery($query);
		
		// Filter teacher
		$teacher= $db->escape($this->getState('filter.filterteacher'));
		
		
		if (!empty($teacher)) {
			$query->where('(author_id='.$teacher.')');
		}
		
		// Filter school
		$school= $db->escape($this->getState('filter.filterschool'));
		
		
		if (!empty($school)) {
			$query->where('(g.company="'.$school.'")');
		}
		
		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		return $items;
	}
	
	
	
	
	
	/** PRAES CUSTOM
	***
	*** ================================================================================================
	***
	**/
	
	
	/** Function getStudent
	***
	*** Method to display all the students in the "students view"
	***
	*/
	
	public function getStudents(){
		
		$db    = $this->getDbo();
		
		$query = $db->getQuery(true);
		
		$query->select('*')
			  ->from('#__guru_customer AS g')
			  ->join('LEFT', '#__user_usergroup_map AS u ON u.user_id = g.id')
			  ->where('u.group_id = 15');
			
		$db->setQuery($query);
		
		$students = $db->loadObjectList();

		return $students;
		
	}
	
	/** Function getAuthor
	***
	*** Method to display the related teachers in the "students view"
	***
	*/
	
	public function getAuthors(){
		
		$db    = $this->getDbo();
		
		$query = $db->getQuery(true);
		
		$query->select('*')
			  ->from('#__guru_buy_courses AS bc')
			  ->join('LEFT', '#__users AS us ON us.id = bc.author_id')
			  ->where('bc.author_id = us.id');
			  
		
			
		$db->setQuery($query);
		
		$teachers = $db->loadObjectList();
		
		return $teachers;
		
		
	}
	
	
}
