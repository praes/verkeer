<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Studentsimporter
 * @author     Jelger Visser <jelger@praes.nl>
 * @copyright  2016 Internetbureau Praes
 * @license    GNU General Public License versie 2 of hoger; Zie LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Studentsimporter model.
 *
 * @since  1.6
 */
class StudentsimporterModelSubscription extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_STUDENTSIMPORTER';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_studentsimporter.subscription';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Subscription', $prefix = 'StudentsimporterTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_studentsimporter.subscription', 'subscription',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return   mixed  The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_studentsimporter.edit.subscription.data', array());

		if (empty($data))
		{
			if ($this->item === null)
			{
				$this->item = $this->getItem();
			}

			$data = $this->item;
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			// Do any procesing on fields here if needed
		}

		return $item;
	}

	/**
	 * Method to duplicate an Subscription
	 *
	 * @param   array  &$pks  An array of primary key IDs.
	 *
	 * @return  boolean  True if successful.
	 *
	 * @throws  Exception
	 */
	public function duplicate(&$pks)
	{
		$user = JFactory::getUser();

		// Access checks.
		if (!$user->authorise('core.create', 'com_studentsimporter'))
		{
			throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
		}

		$dispatcher = JEventDispatcher::getInstance();
		$context    = $this->option . '.' . $this->name;

		// Include the plugins for the save events.
		JPluginHelper::importPlugin($this->events_map['save']);

		$table = $this->getTable();

		foreach ($pks as $pk)
		{
			if ($table->load($pk, true))
			{
				// Reset the id to create a new record.
				$table->id = 0;

				if (!$table->check())
				{
					throw new Exception($table->getError());
				}
				

				// Trigger the before save event.
				$result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

				if (in_array(false, $result, true) || !$table->store())
				{
					throw new Exception($table->getError());
				}

				// Trigger the after save event.
				$dispatcher->trigger($this->event_after_save, array($context, &$table, true));
			}
			else
			{
				throw new Exception($table->getError());
			}
		}

		// Clean cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__studentsimporter_subscriptions');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}
	
	
	public function getSchoolSubscription(){
		
		$oid = JRequest::getVar('oid');
		
		$db    = $this->getDbo();
		$query = $db->getQuery(true);
		
		$query->select("u.*, u.id as uid, um.*, s.*")
			  ->from("#__usergroups as u")
			  ->join("LEFT", "#__user_usergroup_map as um ON u.id = um.group_id")
			  ->join("LEFT", "#__studentsimporter_subscriptions as s ON um.group_id = s.school_id")
			  ->where("u.id = " . $oid)
			  ->group("u.id DESC");
			  
		$db->setQuery($query);
		
		
		$result = $db->loadObject();
		
		return $result;
		
	}
	
	
	public function blockUsers($oid){
		
		$db    = $this->getDbo();
		$query = $db->getQuery(true);
		
		$query->select('*')
			  ->from('#__user_usergroup_map')
			  ->where('group_id = ' . $oid);
			   
		$db->setQuery($query);

		$users = $db->loadObjectList();
		
		foreach($users as $user){
			
			$userAdmin  = JFactory::getUser($user->user_id);
			
			$isAdmin = $userAdmin->authorise('core.admin');
			
			if(!$isAdmin){
				
				//user is not admin. Activate users.
				
				var_dump($user->user_id);
				
				$query = "UPDATE #__users SET block='1' WHERE id='" . $user->user_id . "'";
			   
				$db->setQuery($query);
				
				$db->execute();
				
			}			
			
		}
		
		return true;
		
	}
	
	public function activateUsers($oid){
		
		$db    = $this->getDbo();
		$query = $db->getQuery(true);
		
		$query->select('*')
			  ->from('#__user_usergroup_map')
			  ->where('group_id = ' . $oid);
			   
		$db->setQuery($query);

		$users = $db->loadObjectList();
		
		foreach($users as $user){
			
			$userAdmin  = JFactory::getUser($user->user_id);
			
			$isAdmin = $userAdmin->authorise('core.admin');
			
			if(!$isAdmin){
				
				//user is not admin. Activate users.
				
				$query = "UPDATE #__users SET block='0' WHERE id= " . $user->user_id;
			   
				$db->setQuery($query);
				
				$db->execute();
				
			}			
			
		}
		
		return true;
		
	}
	
	
	public function updateLicense($license){
		
		$oid = $license["oid"];
		
		$db    = $this->getDbo();
		$query = $db->getQuery(true);
		
		$date = JFactory::getDate()->Format('Y');
		
		$single = $date + 1;
		$double = $date + 2;
		$end = "0000-00-00";
		
		//create different scenario queries
		
		if(isset($license["single"])){
			
			//date extends with a year.
			
			$query = "UPDATE #__studentsimporter_subscriptions SET license_date='" . $single . "-08-01' WHERE school_id=" . $oid;
			
			$db->setQuery($query);
		 
			$result = $db->execute();
			
			$this->activateUsers($oid);
			
			return true;
			
		}
		
		if(isset($license["double"])){
			
			$query = "UPDATE #__studentsimporter_subscriptions SET license_date='" . $double . "-08-01' WHERE school_id=" . $oid;
			
			$db->setQuery($query);
		 
			$result = $db->execute();
			
			$this->activateUsers($oid);
			
			return true;
			
		}
		
		if(isset($license["end"])){
			
			$query = "UPDATE #__studentsimporter_subscriptions SET license_date='" . $end . "' WHERE school_id =" . $oid;
			
			$db->setQuery($query);
		 
			$result = $db->execute();
			
			$finished = $this->blockUsers($oid);
			
			if($finished == true){
			
				return true;
				
			}
			
		}	
		
	}
	
	
}
