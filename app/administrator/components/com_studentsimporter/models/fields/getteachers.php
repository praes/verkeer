<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Studentsimporter
 * @author     Jelger Visser <jelger@praes.nl>
 * @copyright  2016 Internetbureau Praes
 * @license    GNU General Public License versie 2 of hoger; Zie LICENSE.txt
 */

defined('JPATH_BASE') or die;

//jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('list');

/**
 * Supports an HTML select list of categories
 *
 * @since  1.6
 */
class JFormFieldGetteachers extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var        string
	 * @since    1.6
	 */
	protected $type = 'getteachers';

	/**
	 * Method to get the field input markup.
	 *
	 * @return    string    The field input markup.
	 *
	 * @since    1.6
	 */

	
	public function getOptions() {
			
		$app = JFactory::getApplication();
	   
		$db = JFactory::getDbo();
		
		$query = $db->getQuery(true);
		
		$query->select("g.*")
			  ->from("#__guru_customer AS g")
			  ->join("LEFT", "#__user_usergroup_map AS u ON g.id = u.user_id")
			  ->where("u.group_id = 14");
			  
		$rows = $db->setQuery($query)->loadObjectlist();
		
		foreach($rows as $row){
			
			$teachername = $row->firstname . " " . $row->insertion . " " . $row->lastname . " - " . $row->company;
			
			$teachers[] = JHtml::_('select.option', $row->id , $teachername);
			
		}
		
		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $teachers);
		
		return $options;
	}
	
}

