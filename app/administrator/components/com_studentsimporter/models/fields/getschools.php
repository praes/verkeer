<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Studentsimporter
 * @author     Jelger Visser <jelger@praes.nl>
 * @copyright  2016 Internetbureau Praes
 * @license    GNU General Public License versie 2 of hoger; Zie LICENSE.txt
 */

defined('JPATH_BASE') or die;

//jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('list');

/**
 * Supports an HTML select list of categories
 *
 * @since  1.6
 */
class JFormFieldGetschools extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var        string
	 * @since    1.6
	 */
	protected $type = 'getschools';

	/**
	 * Method to get the field input markup.
	 *
	 * @return    string    The field input markup.
	 *
	 * @since    1.6
	 */

	
	public function getOptions() {
			
		$app = JFactory::getApplication();
	   
		$db = JFactory::getDbo();
		
		$query = $db->getQuery(true);
		
		$query->select("u.*")
			  ->from("#__usergroups AS u")
			  ->where("u.parent_id = 11");
			  
		$rows = $db->setQuery($query)->loadObjectlist();
		
		foreach($rows as $row){
			
			 $schools[] = JHtml::_('select.option', $row->title , $row->title);
			
		}
		
		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $schools);
		
		return $options;
	}
	
}
