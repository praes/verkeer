<?php
 
defined('JPATH_BASE') or die;
 
jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');
 
/**
 * Custom Field class for the Joomla Framework.
 *
 * @package		Joomla.Administrator
 * @subpackage	        com_my
 * @since		1.6
 */
class JFormFieldFilterteacher extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'Filterteacher';
 
	/**
	 * Method to get the field options.
	 *
	 * @return	array	The field option objects.
	 * @since	1.6
	 */
	public function getOptions() {
			
		// Initialize variables.
 
		$db	= JFactory::getDbo();
		$query	= $db->getQuery(true);
 
		$query = $db->getQuery(true);
		
		$query->select("us.*")
			  ->from("#__users AS us")
			  ->join("LEFT", "#__user_usergroup_map AS ug ON us.id = ug.user_id")
			  ->where("ug.group_id = 14");
 
		// Get the options.
		$db->setQuery($query);
 
		$teachers = $db->loadObjectList();
		
		foreach($teachers as $teacher){
			
			$options[] = JHtml::_('select.option', $teacher->id , $teacher->name);
			
		}
		
		return $options;

	}
}