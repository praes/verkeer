<?php
 
defined('JPATH_BASE') or die;
 
jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');
 
/**
 * Custom Field class for the Joomla Framework.
 *
 * @package		Joomla.Administrator
 * @subpackage	        com_my
 * @since		1.6
 */
class JFormFieldFilterschool extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'Filterschool';
 
	/**
	 * Method to get the field options.
	 *
	 * @return	array	The field option objects.
	 * @since	1.6
	 */
	public function getOptions() {
			
		// Initialize variables.
 
		$db	= JFactory::getDbo();
		$query	= $db->getQuery(true);
 
		$query = $db->getQuery(true);
		
		$query->select("gc.company")
			  ->from("#__guru_customer as gc")
			  ->join("LEFT", '#__usergroups AS ug ON gc.company = ug.title')
			  ->where("gc.company = ug.title")
			  ->group("company");
 
		// Get the options.
		$db->setQuery($query);
 
		$schools = $db->loadObjectList();
		
		foreach($schools as $school){
			
			$options[] = JHtml::_('select.option', $school->company, $school->company);
			
		}
		
		return $options;

	}
}