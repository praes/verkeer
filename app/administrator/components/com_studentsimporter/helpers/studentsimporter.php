<?php

/**
 * @version    CVS: 1.0.1
 * @package    Com_Studentsimporter
 * @author     Jelger Visser <jelger@praes.nl>
 * @copyright  2016 Internetbureau Praes
 * @license    GNU General Public License versie 2 of hoger; Zie LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Studentsimporter helper.
 *
 * @since  1.6
 */
class StudentsimporterHelpersStudentsimporter
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param   string  $vName  string
	 *
	 * @return void
	 */
	public static function addSubmenu($vName = '')
	{

		/*JHtmlSidebar::addEntry(
			JText::_('COM_STUDENTSIMPORTER_TITLE_DASHBOARDS'),
			'index.php?option=com_studentsimporter&view=dashboards',
			$vName == 'dashboards'
		);*/
		
		JHtmlSidebar::addEntry(
			JText::_('COM_STUDENTSIMPORTER_TITLE_SCHOOLS'),
			'index.php?option=com_studentsimporter&view=schools',
			$vName == 'schools'
		);

JHtmlSidebar::addEntry(
			JText::_('COM_STUDENTSIMPORTER_TITLE_SUBSCRIPTIONS'),
			'index.php?option=com_studentsimporter&view=subscriptions',
			$vName == 'subscriptions'
		);

JHtmlSidebar::addEntry(
			JText::_('COM_STUDENTSIMPORTER_TITLE_TEACHERS'),
			'index.php?option=com_studentsimporter&view=teachers',
			$vName == 'teachers'
		);

JHtmlSidebar::addEntry(
			JText::_('COM_STUDENTSIMPORTER_TITLE_STUDENTS'),
			'index.php?option=com_studentsimporter&view=students',
			$vName == 'students'
		);
	}

	/**
	 * Gets the files attached to an item
	 *
	 * @param   int     $pk     The item's id
	 *
	 * @param   string  $table  The table's name
	 *
	 * @param   string  $field  The field's name
	 *
	 * @return  array  The files
	 */
	public static function getFiles($pk, $table, $field)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select($field)
			->from($table)
			->where('id = ' . (int) $pk);

		$db->setQuery($query);

		return explode(',', $db->loadResult());
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return    JObject
	 *
	 * @since    1.6
	 */
	public static function getActions()
	{
		$user   = JFactory::getUser();
		$result = new JObject;

		$assetName = 'com_studentsimporter';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action)
		{
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
}


class StudentsimporterHelper extends StudentsimporterHelpersStudentsimporter
{

}
