<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Studentsimporter
 * @author     Jelger Visser <jelger@praes.nl>
 * @copyright  2016 Internetbureau Praes
 * @license    GNU General Public License versie 2 of hoger; Zie LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Teacher controller class.
 *
 * @since  1.6
 */
class StudentsimporterControllerTeacher extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'teachers';
		parent::__construct();
	}
	
	
	public function save()
	{
		
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$app   = JFactory::getApplication();
		$model = $this->getModel('Teacher', 'StudentsimporterModel');

		// Get the user data.
		$data = JFactory::getApplication()->input->get('jform', array(), 'array');
		
		$return = $model->save($data);
		
		if($return){

			// Redirect to the list screen.
			$this->setMessage(JText::_('Docent toegevoegd.'));
			$this->setRedirect('index.php?option=com_studentsimporter&view=teachers');

		}
			
	}
	
	
}
