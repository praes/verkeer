<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Studentsimporter
 * @author     Jelger Visser <jelger@praes.nl>
 * @copyright  2016 Internetbureau Praes
 * @license    GNU General Public License versie 2 of hoger; Zie LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Student controller class.
 *
 * @since  1.6
 */
class StudentsimporterControllerStudent extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'students';
		parent::__construct();
	}
	
	public function remove(){
		
		// Check for request forgeries.
		
		$studentId = JRequest::getVar('id');
		
		// Initialise variables.
		$app   = JFactory::getApplication();
		$model = $this->getModel('Student', 'StudentsimporterModel');
		
		$return = $model->remove($studentId);
		
		if($return){
		
			// Redirect to the list screen.
			$this->setMessage(JText::_('Leerling verwijderd.'));
			$this->setRedirect('index.php?option=com_studentsimporter&view=students');
			
		}
		
	}
	
	
	public function save($key = NULL, $urlVar = NULL)
	{
		
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$app   = JFactory::getApplication();
		$model = $this->getModel('Student', 'StudentsimporterModel');

		// Get the user data.
		$data = JFactory::getApplication()->input->get('jform', array(), 'array');
		
		$return = $model->save($data);
		
		if($return){

			// Redirect to the list screen.
			$this->setMessage(JText::_('Leerling toegevoegd.'));
			$this->setRedirect('index.php?option=com_studentsimporter&view=students');

		}
			
	}
	
	public function import(){
		
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		
		$app   = JFactory::getApplication();
		$model = $this->getModel('Student', 'StudentsimporterModel');
		
		
		$details = JRequest::get('post');
		$password = '$2y$10$ouJWEBwFJfFoXYfvghHN/O9aLHpBrm0bBIRau4Z19uEEMkHNEtwjC';
		$company = $details['jform']["getschools"];
		$order = $details['jform']["getlessons"];
		$author = $details['jform']["getteachers"];
		
		$input = JFactory::getApplication()->input;
		$file = $input->files->get('file_upload'); 
		
		$db = JFactory::getDbo();
 
		$query = $db->getQuery(true);
		
		$tot = 0;
		
		$handle = fopen($_FILES["file_upload"]["tmp_name"], "r");
		
		
		if($handle){
		
			while (($data = fgetcsv($handle, 1000, ";")) == true) {
				
				for ($i=0; $i < 1; $i++) {

					//skip first record
					if($data[0] !='Voornaam'){
						
						$name = $data[0] . " " . $data[1] . " " . $data[2];
						$firstname = $data[0];
						$insertion = $data[1];
						$lastname = $data[2];
						$email = $data[3];
						$group = $data[4];
						
						
						//query into users table

						$query = "INSERT INTO #__users (name, username, email, password) VALUES ('" . $name . "', '" . $email . "', '" . $email . "', '" . $password . "')";
						
						$db->setQuery($query);
						 
						$result = $db->execute();
						
						$lastuser = $db->insertid();
						
						//query into usergroup table 15 = leerlingen
						
						$query = "INSERT INTO #__user_usergroup_map (user_id, group_id)  VALUES ('" . $lastuser ."', '15')";
						
						$db->setQuery($query);
						 
						$result = $db->execute();
						
						//query into usergroup table 2 = registered
						
						$query = "INSERT INTO #__user_usergroup_map (user_id, group_id)  VALUES ('" . $lastuser ."', '2')";
						
						$db->setQuery($query);
						 
						$result = $db->execute();
						
						//query into customer table
						
						$query = "INSERT INTO #__guru_customer (`id`, `company`, `firstname`, `insertion`, `lastname`, `group`)  VALUES ('" . $lastuser . "','" . $company . "','" . $firstname . "','" . $insertion . "','" . $lastname . "','" . $group . "')";
						
						$db->setQuery($query);
						 
						$result = $db->execute();
						
						
						//query into order table
						
						$query = "INSERT INTO #__guru_order (userid, courses, status, amount, amount_paid, published) VALUES ('" . $lastuser . "', '" . $order . "-0-1', 'paid', '0', '-1', '1')";
						
						$db->setQuery($query);
						 
						$result = $db->execute();
						
						$lastorder = $db->insertid();
						
						$query = "INSERT INTO #__guru_buy_courses (userid, author_id, order_id, course_id, plan_id) VALUES ('" . $lastuser . "', '" . $author . "', '" . $lastorder . "', '" . $order . "', '1')";
						
						$db->setQuery($query);
						 
						$result = $db->execute();
						
					}
					
					$tot++;
				}
			}
			
			fclose($handle);
		
		}
		
		$this->setMessage(JText::_('Leerlingen toegevoegd.'));
		$this->setRedirect('index.php?option=com_studentsimporter&view=students');
		
		
	}
	
	
	
	
}
