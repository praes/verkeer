<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Studentsimporter
 * @author     Jelger Visser <jelger@praes.nl>
 * @copyright  2016 Internetbureau Praes
 * @license    GNU General Public License versie 2 of hoger; Zie LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Subscription controller class.
 *
 * @since  1.6
 */
class StudentsimporterControllerSubscription extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'subscriptions';
		parent::__construct();
	}
	
	
	public function license(){
		
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$app   = JFactory::getApplication();
		$model = $this->getModel('Subscription', 'StudentsimporterModel');
		
		$license = JRequest::get('post');
		
		$return = $model->updateLicense($license);
		
		if($return){
			
			// Redirect to the list screen.
			$this->setMessage(JText::_('Licentie opgeslagen.'));
			$this->setRedirect('index.php?option=com_studentsimporter&view=subscriptions');
			
		}
		
	}
	
}
