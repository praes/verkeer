<?php
/**
* @version		1.0.0
* @package		MijoSEF
* @copyright	2009-2014 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @license		2009-2012 GNU/GPL based on AceSEF joomace.net
*/

// No Permission
defined('_JEXEC') or die('Restricted Access');

// View Class
class MijosefViewSefUrls extends MijosefView {

	// View URLs
	function view($tpl = null) {
        $layout = JRequest::getWord('layout');

        if ($layout == 'seorank') {
            $this->viewSeoRank();

            return;
        }
		
		$toolbar = $this->get('ToolbarSelections');
		
	    $this->type = JFactory::getApplication()->getUserStateFromRequest('com_mijosef.urls.type', 'type', 'sef');
		
		// Toolbar
		JToolBarHelper::title(JText::_('COM_MIJOSEF_COMMON_URLS' ), 'mijosef');
		if ($this->type == 'notfound') {
			$this->toolbar->appendButton('Popup', 'new', JText::_('Create 301'), 'index.php?option=com_mijosef&controller=movedurls&task=add&tmpl=component', 600, 340);
		}
		elseif ($this->type != 'trashed') {
			$this->toolbar->appendButton('Popup', 'new', JText::_('New'), 'index.php?option=com_mijosef&controller=sefurls&task=add&tmpl=component', 800, 630);
            //JToolBarHelper::addNew();
            JToolBarHelper::editList();
		}
		
		if ($this->type != 'trashed') {
			if ($this->type == "quickedit") {
				$tpl = "quickedit";
                JToolBarHelper::apply();
			}
			
			if ($this->type != 'notfound') {
				$this->toolbar->appendButton('Popup', 'loop', JText::_('COM_MIJOSEF_TOOLBAR_GENERATE_URLS'), 'index.php?option=com_mijosef&controller=sefurls&task=generate&tmpl=component', 400, 220);
			}
		}

		$this->toolbar->appendButton('Custom', $toolbar->action);
		$this->toolbar->appendButton('Custom', $toolbar->newtags . $toolbar->newilinks . $toolbar->newbookmarks . $toolbar->newtag);
		$this->toolbar->appendButton('Custom', $toolbar->selection);
		$this->toolbar->appendButton('Custom', $toolbar->button);
		$this->toolbar->appendButton('Popup', 'trash', JText::_('COM_MIJOSEF_CACHE_CLEAN'), 'index.php?option=com_mijosef&amp;controller=purgeupdate&amp;task=cache&amp;tmpl=component', 300, 380);
		$this->toolbar->appendButton('Popup', 'help', JText::_('Help'), 'http://miwisoft.com/support/docs/mijosef/user-manual/urls?tmpl=component', 650, 500);
		
		// Get behaviors
		JHTML::_('behavior.tooltip');
		JHTML::_('behavior.modal', 'a.modal', array('onClose'=>'\function(){location.reload(true);}'));
		
		// Footer colspan
		$colspan = 5;
		if ($this->MijosefConfig->ui_sef_published == 1) {
			$colspan = $colspan + 1;
		}
		if ($this->MijosefConfig->ui_sef_used == 1) {
			$colspan = $colspan + 1;
		}
		if ($this->MijosefConfig->ui_sef_locked == 1) {
			$colspan = $colspan + 1;
		}
		if ($this->MijosefConfig->ui_sef_blocked == 1) {
			$colspan = $colspan + 1;
		}
		if ($this->MijosefConfig->ui_sef_cached == 1) {
			$colspan = $colspan + 1;
			$this->cache = $this->get('Cache');
		}
		if ($this->MijosefConfig->ui_sef_date == 1) {
			$colspan = $colspan + 2;
		}
		if ($this->MijosefConfig->ui_sef_hits == 1) {
			$colspan = $colspan + 1;
		}
		if ($this->MijosefConfig->ui_sef_id == 1) {
			$colspan = $colspan + 1;
		}
		
		// Get jQuery
		//if ($this->MijosefConfig->jquery_mode == 1) {
			//$this->document->addScript('components/com_mijosef/assets/js/jquery-1.4.2.min.js');
			$this->document->addScript('components/com_mijosef/assets/js/jquery.bgiframe.min.js');
			$this->document->addScript('components/com_mijosef/assets/js/jquery.autocomplete.js');
		//}
		
		// Get data from the model
		$this->lists 		= $this->get('Lists');
		$this->items 		= $this->get('Items');
		$this->duplicates 	= $this->get('Duplicates');
		$this->pagination 	= $this->get('Pagination');
		$this->colspan 		= $colspan;

		parent::display($tpl);
	}

    public function viewSeoRank() {
        $tpl = 'seorank';

        $id = JRequest::getInt('id');

        $url = Mijosef::getTable('MijosefSefUrls');
        if (!$url->load($id)) {
            return;
        }

        if (empty($url->seo_rank)) {
            $url->seo_rank = '{"total":0,"title":0,"description":0,"h1":0,"title":0,"images_no_alt":1,"social":0,"backlinks":0}';
        }

        $rank = json_decode($url->seo_rank);

        $ranks = array();

        $ranks['title'] = new stdClass();
        $ranks['title']->name = 'Title';
        $ranks['title']->short_desc = $rank->title ? "Page has a title and is smaller than 60 characters." : "Page doesn't have a title or is larger than 60 characters.";
        $ranks['title']->short_desc_color = $rank->title ? "green" : "red";
        $ranks['title']->long_desc = 'The title is an important factor in the on-site search engine optimization. Not only uses the search engines the title for the keywords, the title is also used for display in the SERP. For best practice make use of your main keywords in the title.';

        $ranks['description'] = new stdClass();
        $ranks['description']->name = 'Description';
        $ranks['description']->short_desc = $rank->description ? "Page has a description and is smaller than 160 characters." : "Page doesn't have a description or is larger than 160 characters.";
        $ranks['description']->short_desc_color = $rank->description ? "green" : "red";
        $ranks['description']->long_desc = "The description is an factor in the on-site search engine optimization. Not only uses the search engines the description for the keywords, the description is many times used for display in the SERP. For best practice describe where your web page is about in the description.";

        $ranks['h1'] = new stdClass();
        $ranks['h1']->name = 'H1';
        $ranks['h1']->short_desc = ($rank->h1 and (count($rank->h1) == 1)) ? "Page has a H1 tag." : "Page doesn't have or have more than a H1 tag.";
        $ranks['h1']->short_desc_color = ($rank->h1 and (count($rank->h1) == 1)) ? "green" : "red";
        $ranks['h1']->long_desc = "H1 is the main and the most affective heading for SEO. Search engines evaluate pages similar to humans, in that they scan for headlines, and these are designated in HTML by using the H1 through H6 tags.";

        $ranks['images_no_alt'] = new stdClass();
        $ranks['images_no_alt']->name = 'Alt Attribute';
        $ranks['images_no_alt']->short_desc = $rank->images_no_alt ? $rank->images_no_alt. " image(s) has no alt attribute." : "All the image(s) has an alt attribute.";
        $ranks['images_no_alt']->short_desc_color = $rank->images_no_alt ? "red" : "green";
        $ranks['images_no_alt']->long_desc = "The images alt attributes are used by the search engines. For best practice describe where the images is about in the alt attributes.";

        $ranks['social'] = new stdClass();
        $ranks['social']->name = 'Social Media';
        $ranks['social']->short_desc = ($rank->social > 100) ? "Social media presence is good (" . $rank->social. " shares)." : "Social media presence is not good (" . $rank->social. " shares).";
        $ranks['social']->short_desc_color = ($rank->social > 100) ? "green" : "red";
        $ranks['social']->long_desc = "The importance of social media for certain industries is huge. This data represents social media influences from this specific page.";

        $ranks['backlinks'] = new stdClass();
        $ranks['backlinks']->name = 'Google Backlinks';
        $ranks['backlinks']->short_desc = $rank->backlinks ? "Page has " . $rank->backlinks. " Google backlinks." : "Page doesn't have any Google backlink.";
        $ranks['backlinks']->short_desc_color = $rank->backlinks ? "green" : "red";
        $ranks['backlinks']->long_desc = "The total amount of results for a Google link-search for the page URL.";

        $this->total = $rank->total;
        $this->ranks = $ranks;

        parent::display($tpl);
    }
}