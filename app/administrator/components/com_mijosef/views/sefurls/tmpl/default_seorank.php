<?php
/**
* @version		1.0.0
* @package		MijoSEF
* @subpackage	MijoSEF
* @copyright	2009-2014 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

// No Permission
defined('_JEXEC') or die('Restricted access');

$rank_color = 'green';
if ($this->total >= 0 && $this->total < 33) {
    $rank_color = 'red';
}
else if ($this->total >= 33 && $this->total < 66) {
    $rank_color = 'orange';
}

?>

<div class="current" style="background-color:#ffffff;">
    <table class="adminlist table table-striped" cellspacing="1">
        <thead>
            <tr>
                <th style="width: 20%; padding-bottom: 20px !important;">
                    Overall: <?php echo $this->total; ?> out of 100
                </th>
                <th style="padding-bottom: 20px !important;">
                    <div style="width: 98%;" class="mjsf-progress">
                        <span class="<?php echo $rank_color; ?>" style="width: <?php echo $this->total; ?>%;">
                            <span><?php echo $this->total; ?></span>
                        </span>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php
            $k = 0;
            foreach ($this->ranks as $rank) {
            ?>
            <tr class="<?php echo "row$k"; ?>">
                <td style="width: 20%">
                    <strong><?php echo $rank->name; ?></strong>
                </td>
                <td>
                    <strong><span style="color: <?php echo $rank->short_desc_color; ?>"><?php echo $rank->short_desc; ?></span></strong><br />
                    <?php echo $rank->long_desc; ?>
                </td>
            </tr>
            <?php
                $k = 1 - $k;
            }
            ?>
        </tbody>
    </table>
</div>