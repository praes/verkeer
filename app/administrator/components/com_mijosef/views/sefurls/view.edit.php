<?php
/**
* @version		1.0.0
* @package		MijoSEF
* @subpackage	MijoSEF
* @copyright	2009-2014 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @license		2009-2012 GNU/GPL based on AceSEF joomace.net
*/

// No Permission
defined('_JEXEC') or die('Restricted Access');

// View Class
class MijosefViewSefUrls extends MijosefView {

	// Edit URL
	function edit($tpl = null) {
		$row = $this->getModel()->getEditData('MijosefSefUrls');
		
		// Toolbar
		JToolBarHelper::title(JText::_('COM_MIJOSEF_URL_EDIT_TITLE').' '.$row->url_sef, 'mijosef');
        JToolBarHelper::save('editSave');
        JToolBarHelper::apply('editApply');
        JToolBarHelper::cancel('editCancel');
		JToolBarHelper::divider();
        JToolBarHelper::save2copy('editSaveMoved', JTEXT::_('COM_MIJOSEF_TOOLBAR_SEF_SAVEMOVED'));
		JToolBarHelper::divider();
		$this->toolbar->appendButton('Popup', 'help', JText::_('Help'), 'http://miwisoft.com/support/docs/mijosef/user-manual/urls?tmpl=component', 650, 500);
		
		//
		// Params
		//
		$params = new JRegistry($row->params);
   	   	$url['custom'] = $params->get('custom', '0');
   	   	$url['published'] = $params->get('published', '0');
   	   	$url['locked'] = $params->get('locked', '0');
   	   	$url['blocked'] = $params->get('blocked', '0');
   	   	$url['trashed'] = $params->get('trashed', '0');
   	   	$url['notfound'] = $params->get('notfound', '0');
   	   	$url['tags'] = $params->get('tags', '0');
   	   	$url['ilinks'] = $params->get('ilinks', '0');
   	   	$url['bookmarks'] = $params->get('bookmarks', '0');
		$url['notes'] = $params->get('notes', '');
		
		$cache = $this->get('Cache');
		$url['cached'] = '0';
		if (isset($cache[$row->url_real])) {
			$url['cached'] = '1';
		}
		
		// Get alias
		$url['alias'] = self::getAliases($row->url_sef);

        $metadata = self::getMetadata($row->url_sef);

        $enabled_list[] = JHTML::_('select.option', '0', JText::_('No'));
        $enabled_list[] = JHTML::_('select.option', '1', JText::_('Yes'));
        $enabled_list[] = JHTML::_('select.option', '2', JText::_('Use Global'));
        $lists['og_enabled'] = JHTML::_('select.genericlist', $enabled_list, 'og_enabled', 'class="inputbox" size="1"','value', 'text', $metadata->og_enabled);

        $type_list[] = JHTML::_('select.option', 'article', JText::_('Article'));
        $type_list[] = JHTML::_('select.option', 'blog', JText::_('Blog'));
        $type_list[] = JHTML::_('select.option', 'book', JText::_('Book'));
        $type_list[] = JHTML::_('select.option', 'profile', JText::_('Profile'));
        $type_list[] = JHTML::_('select.option', 'video.movie', JText::_('Movie'));
        $type_list[] = JHTML::_('select.option', 'video.episode', JText::_('TV Episode'));
        $type_list[] = JHTML::_('select.option', 'video.tv_show', JText::_('TV Show'));
        $type_list[] = JHTML::_('select.option', 'video.other', JText::_('Video'));
        $type_list[] = JHTML::_('select.option', 'website', JText::_('Website'));
        $lists['og_type'] = JHTML::_('select.genericlist', $type_list, 'og_type', 'class="inputbox" size="1"','value', 'text', $metadata->og_type);

        $og_title_selected = $metadata->og_title;
        if (($og_title_selected != '0') and ($og_title_selected != '1')) {
            $og_title_selected = 2;
        }

        $og_description_selected = $metadata->og_description;
        if (($og_description_selected != '0') and ($og_description_selected != '1')) {
            $og_description_selected = 2;
        }

        $og_site_name_selected = $metadata->og_site_name;
        if (($og_site_name_selected != '0') and ($og_site_name_selected != '1')) {
            $og_site_name_selected = 2;
        }

        $title_list[] = JHTML::_('select.option', '0', JText::_('Disabled'));
        $title_list[] = JHTML::_('select.option', '1', JText::_('Use Metadata'));
        $title_list[] = JHTML::_('select.option', '2', JText::_('Enter Custom'));
        $lists['og_title'] = JHTML::_('select.genericlist', $title_list, 'og_title', 'class="inputbox" size="1" onchange="changeDisplay(\'og_title_input\', this.value);"','value', 'text', $og_title_selected);

        $description_list[] = JHTML::_('select.option', '0', JText::_('Disabled'));
        $description_list[] = JHTML::_('select.option', '1', JText::_('Use Metadata'));
        $description_list[] = JHTML::_('select.option', '2', JText::_('Enter Custom'));
        $lists['og_description'] = JHTML::_('select.genericlist', $description_list, 'og_description', 'class="inputbox" size="1" onchange="changeDisplay(\'og_description_input\', this.value);"','value', 'text', $og_description_selected);

        $sitename_list[] = JHTML::_('select.option', '0', JText::_('Disabled'));
        $sitename_list[] = JHTML::_('select.option', '1', JText::_('Use Joomla Site Name'));
        $sitename_list[] = JHTML::_('select.option', '2', JText::_('Enter Custom'));
        $lists['og_site_name'] = JHTML::_('select.genericlist', $sitename_list, 'og_site_name', 'class="inputbox" size="1" onchange="changeDisplay(\'og_site_name_input\', this.value);"','value', 'text', $og_site_name_selected);

        $this->og_title_display = $this->og_description_display = $this->og_site_name_display = ' display: none;';

        if (($metadata->og_title == '0') or ($metadata->og_title == '1')) {
            $metadata->og_title = '';
        }
        else {
            $this->og_title_display = '';
        }

        if (($metadata->og_description == '0') or ($metadata->og_description == '1')) {
            $metadata->og_description = '';
        }
        else {
            $this->og_description_display = '';
        }

        if (($metadata->og_site_name == '0') or ($metadata->og_site_name == '1')) {
            $metadata->og_site_name = '';
        }
        else {
            $this->og_site_name_display = '';
        }

		// Assign values
		$this->row          = $row;
		$this->url          = $url;
		$this->metadata     = $metadata;
		$this->sitemap      = self::getSitemap($row->url_sef);
        $this->lists        = $lists;
		
		parent::display($tpl);
	}
	
	function getAliases($url) {
		$aliases = "";
		$urls = MijoDatabase::loadObjectList("SELECT url_old FROM #__mijosef_urls_moved WHERE url_new = '{$url}' ORDER BY url_old");
		
		if (!is_null($urls)) {
			foreach ($urls as $u) {
				$aliases .= $u->url_old."\n";
			}
		}
		
		return $aliases;
	}
	
	function getMetadata($url) {
		$empty = new stdClass();
		$empty->id = "";
		$empty->published = 0;
		$empty->title = "";
		$empty->description = "";
		$empty->keywords = "";
		$empty->lang = "";
		$empty->robots = "";
		$empty->googlebot = "";
		$empty->canonical = "";
		$empty->og_enabled = 2;
		$empty->og_type = "article";
		$empty->og_image = "";
		$empty->og_title = 1;
		$empty->og_description = 1;
		$empty->og_site_name = 1;
		$empty->og_video = "";

		$task = JRequest::getWord('task');
		if ($task == 'add') {
			return $empty;
		}
		
		$metadata = MijoDatabase::loadObject("SELECT * FROM #__mijosef_metadata WHERE url_sef = '{$url}'");
		if (!is_object($metadata)) {
			return $empty;
		}
		
		return $metadata;
	}
	
	function getSitemap($url) {
		$empty = new stdClass();
		$empty->id = "";
		$empty->published = "0";
		$empty->sdate = date('Y-m-d');
		$empty->frequency = $this->MijosefConfig->sm_freq;
		$empty->priority = $this->MijosefConfig->sm_priority;
		$empty->sparent = "";
		$empty->sorder = "";
		
		$task = JRequest::getWord('task');
		if ($task == 'add') {
			return $empty;
		}
		
		$sitemap = MijoDatabase::loadObject("SELECT * FROM #__mijosef_sitemap WHERE url_sef = '{$url}'");
		if (!is_object($sitemap)) {
			return $empty;
		}
		
		return $sitemap;
	}
}
?>