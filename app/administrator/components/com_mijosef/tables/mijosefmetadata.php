<?php
/**
* @package		MijoSEF
* @copyright	2009-2014 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @license		2009-2012 GNU/GPL based on AceSEF joomace.net
*/

// No Permission
defined('JPATH_BASE') or die('Restricted Access');

class TableMijosefMetadata extends JTable {

    public $id 	 		    = null;
    public $url_sef 		= null;
    public $published		= null;
    public $title 			= null;
    public $description	    = null;
    public $keywords		= null;
    public $lang			= null;
    public $robots			= null;
    public $googlebot		= null;
    public $canonical		= null;
    public $og_enabled		= null;
    public $og_type		    = null;
    public $og_image		= null;
    public $og_title		= null;
    public $og_description	= null;
    public $og_site_name	= null;
    public $og_video	    = null;

    public function __construct(&$db) {
		parent::__construct('#__mijosef_metadata', 'id', $db);
	}
}