<?php
/*
* @package		MijoSEF
* @subpackage	Zoo
* @copyright	2012 Mijosoft LLC, www.mijosoft.com
* @license		http://www.mijosoft.com/company/license
* @license		GNU/GPL based on AceSEF www.joomace.net
*/

// No Permission
defined( '_JEXEC' ) or die( 'Restricted access' );

class MijoSEF_com_zoo extends MijosefExtension {

	function beforeBuild(&$uri) {
		self::_a_b_c_mijosef();
		
		// Version 2
		if ($this->params->get('version_inc', '2') == '2') {
			if (!is_null($uri->getVar('task')) && $uri->getVar('task') == 'frontpage') {
				$uri->delVar('task');
				$uri->setVar('view', 'frontpage');
			}
			
			if (!is_null($uri->getVar('task')) && $uri->getVar('task') == 'category') {
				$uri->delVar('task');
				$uri->setVar('view', 'category');
			}
			
			if (!is_null($uri->getVar('task')) && $uri->getVar('task') == 'item') {
				$uri->delVar('task');
				$uri->setVar('view', 'item');
			}
			
			if (!is_null($uri->getVar('task')) && $uri->getVar('task') == 'category' && is_null($uri->getVar('category_id'))) {
				$uri->delVar('task');
			}
			
			if ($uri->getVar('view') == 'item' && !is_null($uri->getVar('category_id')) && !is_null($uri->getVar('item_id')) && $this->params->get('item_categoryid_inc', '2') == '2') {
				$uri->delVar('category_id');
			}
			
			if ($uri->getVar('view') == 'item' && !is_null($uri->getVar('app_id')) && !is_null($uri->getVar('item_id'))) {
				$uri->delVar('app_id');
			}
			
			if ($uri->getVar('task') == 'tag' && !is_null($uri->getVar('tag')) && !is_null($uri->getVar('item_id'))) {
				$uri->delVar('item_id');
			}
		
			if ($uri->getVar('view') == 'frontpage' && (is_null($uri->getVar('category_id')) || $uri->getVar('category_id') == 0)) {
				$uri->delVar('category_id');
			}
		
			if (!is_null($uri->getVar('category_id')) && is_null($uri->getVar('view'))) {
				$uri->setVar('view', 'category');
			}
		
			if (!is_null($uri->getVar('task')) && $uri->getVar('task') == 'tag' && !is_null($uri->getVar('app_id')) && !is_null($uri->getVar('tag')) && $this->params->get('tagsapplication_inc', '1') == '2') {
				$uri->delVar('app_id');
			}
		
			$route_helper = JPATH_ADMINISTRATOR.'/components/com_zoo/config.php';
			if (($this->params->get('smart_itemid', '1') == '2') && file_exists($route_helper) && !is_null($uri->getVar('view'))) {
				require_once($route_helper);
				
				if (!class_exists('RouteHelper')) {
					return;
				}
				
				if (!method_exists('RouteHelper', '_findFrontpage')) {
					return;
				}
				
				if (!method_exists('RouteHelper', '_findCategory')) {
					return;
				}
				
				if (!method_exists('RouteHelper', '_findItem')) {
					return;
				}
				
				$route = new ZoooooRouteHelper();
				
				if ($uri->getVar('view') == 'frontpage' && !is_null($uri->getVar('app_id'))) {
					$app_id = $uri->getVar('app_id');
			
					$item = $route->_findFrontpage($app_id);
					if (!empty($item)) {
						$uri->setVar('Itemid', $item->id);
					}
				}
				
				if ($uri->getVar('view') == 'category' && !is_null($uri->getVar('category_id'))) {
					$category_id = $uri->getVar('category_id');
			
					$item = $route->_findCategory($category_id);
					if (!empty($item)) {
						$uri->setVar('Itemid', $item->id);
					}
				}
				
				if ($uri->getVar('view') == 'item' && !is_null($uri->getVar('item_id'))) {
					$item_id = $uri->getVar('item_id');
			
					$item = $route->_findItem($item_id);
					if (!empty($item)) {
						$uri->setVar('Itemid', $item->id);
					}
				}
			}
			
			// Load application id if it is menu frontpage
			if ($uri->getVar('view') == 'frontpage' && (is_null($uri->getVar('app_id')) || $uri->getVar('app_id') == 0) && $this->params->get('application_inc', '2') == '2'){
				$menu_params = parent::getMenuParams($uri->getVar('Itemid'));
				
				if ($menu_params && is_object($menu_params)){
					$app_id = $menu_params->get('application');
					
					if (!empty($app_id)) {
						$uri->setVar('app_id', $app_id);
					}
				}
			}
			
			// Load cat id if it is menu item
			if ($uri->getVar('view') == 'category' && (is_null($uri->getVar('category_id')) || $uri->getVar('category_id') == 0)) {
				$menu_params = parent::getMenuParams($uri->getVar('Itemid'));
				
				if ($menu_params && is_object($menu_params)){
					$id = $menu_params->get('category');
					
					if (!empty($id)) {
						$uri->setVar('category_id', $id);
					}
				}
			}
		
			// Load item id if it is menu item
			if ($uri->getVar('view') == 'item' && (is_null($uri->getVar('item_id')) || $uri->getVar('item_id') == 0)){
				$menu_params = parent::getMenuParams($uri->getVar('Itemid'));
				
				if ($menu_params && is_object($menu_params)){
					$id = $menu_params->get('item_id');
					
					if (!empty($id)) {
						$uri->setVar('item_id', $id);
					}
				}
			}
		
			// Load submission id if it is menu item
			if ($uri->getVar('view') == 'submission' && $uri->getVar('layout') == 'submission' && is_null($uri->getVar('submission_id')) && !is_null($uri->getVar('type_id'))){
				$menu_params = parent::getMenuParams($uri->getVar('Itemid'));
				
				if ($menu_params && is_object($menu_params)){
					$id = $menu_params->get('submission');
					
					if (!empty($id)) {
						$uri->setVar('submission_id', $id);
					}
				}
			}
		}
		
		// Version 1
		else {
			// Load cat id if it is menu item
			if (is_null($uri->getVar('category_id')) || $uri->getVar('category_id') == 0) {
				$menu_params = parent::getMenuParams($uri->getVar('Itemid'));
				if ($menu_params && is_object($menu_params)){
					$id = $menu_params->get('catalog_category');
					if(!empty($id)) {
						$catid = explode(':', $id);
						if(!empty($catid[1])) {
							$uri->setVar('category_id', $catid[1]);
						} elseif(!empty($catid[0])) {
							$uri->setVar('catalog_id', $catid[0]);
						}
					}
				}
			}
			
			// Load item id if it is menu item
			if (is_null($uri->getVar('item_id')) || $uri->getVar('item_id') == 0){
				$menu_params = parent::getMenuParams($uri->getVar('Itemid'));
				if ($menu_params && is_object($menu_params)){
					$id = $menu_params->get('item_id');
					if (!empty($id)) {
						$uri->setVar('item_id', $id);
					}
				}
			}
		}
		
		if (!is_null($uri->getVar('layout')) && $uri->getVar('view') != 'submission'){
			$uri->delVar('layout');
		}
    }
	
	function catParam($vars, $real_url) {
        extract($vars);
		
		if (isset($view)) {
			switch($view) {
				case 'category':
					if (!empty($category_id)) {
						parent::categoryParam('', 1, $category_id, 1, $real_url);
					}
					break;
				case 'item':
					if (!empty($item_id)){
						$catid = self::_getItemCatId(intval($item_id));
						if (!empty($catid)) {
							parent::categoryParam('', 1, $catid, 0, $real_url);
						}
					}
					break;
			}
		}
	}
	
	function _getItemCatId($id) {
		static $cache = array();
		
		if (!isset($cache[$id])) {
			$cache[$id] = MijoDatabase::loadResult("SELECT category_id FROM #__zoo_category_item WHERE item_id = {$id} AND category_id != '0' LIMIT 1");
		}
		
		return $cache[$id];
    }
	
	function build(&$vars, &$segments, &$do_sef, &$metadata, &$item_limitstart) {
		self::_a_b_c_mijosef();
		
        extract($vars);
		
		$cat_suffix 	= $this->params->get('cat_suffix', '1');
		$default_index	= $this->params->get('default_index', '');
		
		// Version 2
		if ($this->params->get('version_inc', '2') == '2') {
			if (!empty($submission_id)) {
				$segments[] = self::_getSubmission(intval($submission_id));
				unset($vars['submission_id']);
			}
			
			if (!empty($category_id)) {
				$segments = array_merge($segments, self::_getCategory(intval($category_id)));
				if($cat_suffix == '1' && $default_index == '' && empty($item_id) && empty($alpha_char)){
					$segments[] = "/";
				}
				
				if($default_index != ''){
					$segments[] = $default_index;
				}
				unset($vars['category_id']);
			}
			
			if (!empty($item_id)) {
				$add_cat = false;
				if (empty($category_id) && $this->params->get('item_categoryid_inc', '2') == '2'){
					$add_cat = true;
				}
				
				if ($this->params->get('url_structure', 'joomla') == 'joomla') {
					$segments = array_merge($segments, self::_getItem(intval($item_id), $add_cat));
				}
				else {
					$segments = array_merge($segments, self::_getItemWP(intval($item_id), $add_cat));
				}
				
				if ($this->params->get('googlenewsnum', '1') != '1') {
					$i = count($segments) - 1;
					$segments[$i] = self::_googleNews($segments[$i], $item_id);
				}
				unset($vars['item_id']);
			}
			
			if (isset($view)) {
				switch($view) {
					case 'category':
					case 'item':
						break;
					case 'frontpage':
						if (!empty($app_id)) {
							$segments[] = self::_getApplication(intval($app_id));
							unset($vars['app_id']);
						}
						break;
					case 'submission':
						if (isset($layout)){
							if (!empty($item_id)){
								$segments[] = JText::_('Edit');
								unset($vars['item_id']);
							}
							
							if ($layout == 'mysubmissions' && empty($item_id) && empty($submission_id)){
								$segments[] = JText::_('My Submissions');
								unset($vars['layout']);
							}
							
							if ($layout == 'submission' && empty($item_id) && !empty($type_id)){
								$segments[] = $type_id;
								$segments[] = JText::_('Added');
								unset($vars['layout']);
								unset($vars['type_id']);
							}
							
							if ($layout == 'submission' && empty($item_id) && empty($type_id)){
								$segments[] = JText::_('Add');
								unset($vars['layout']);
							}
						}
						unset($vars['submission_hash']);
						break;
					default:
						$segments[] = $view;
						break;
				}
				unset($vars['view']);
			}
		
			if (isset($task)) {
				switch ($task) {
					case 'remove':
					case 'callelement':
					case 'element':
						$do_sef = false;
						break;
					case 'item':
					case 'frontpage':
						break;
					case 'tag':
						$segments = null;
						if (!empty($app_id) && $this->params->get('tagsapplication_inc', '1') == '1' && $this->params->get('application_inc', '2') == '2') {
							$segments[] = self::_getApplication(intval($app_id));
							unset($vars['app_id']);
						}
						break;
					case 'alphaindex':
						if (!empty($app_id)) {
							$segments[] = self::_getApplication(intval($app_id));
							unset($vars['app_id']);
						}
						
						if (isset($alpha_char)){
							$segments[] = JText::_('Index');
							$segments[] = $alpha_char;
							unset($vars['alpha_char']);
						}
						break;
					case 'save':
						$segments[] = JText::_('Save');
						break;
					case 'twitterconnect':
						$segments[] = JText::_('Twitter');
						break;
					default:
						$segments[] = $task;
						break;
				}
				unset($vars['task']);
			}
			
			if (!empty($controller)){
				if ($controller == 'comment') {
					$do_sef = false;
					return;
				}
				
				$segments[] = $controller;
				unset($vars['controller']);
			}
		
			if (!empty($tag)){
				$segments[] = JText::_('Tags');
				if (parent::urlPart($this->params->get('tag_part', 'global')) == 'title'){
					$segments[] = $tag;
				} else {
					$char = strpos($tag, '.html');
					if (isset($char)) {
						$link = explode(".",$tag);
						$tester = self::_getTag($link[0]);
					} else {
						$tester = self::_getTag($tag);
					}
					
					if (empty($tester)){
						$mainframe =& JFactory::getApplication();
						$mainframe->redirect("404.html");
					} else {
						$segments[] = $tag;
					}
				}
				unset($vars['tag']);
			}
			
			if (isset($page)) {
				$segments[] = JText::_('Page').' '.$page;
				unset($vars['page']);
			}
			
		} 
		
		// Version 1
		else {
			if (!empty($catalog_id)) {
				$segments[] = self::_getCatalogOld(intval($catalog_id));
				unset($vars['catalog_id']);
			}
			
			if (!empty($category_id)) {
				$segments = array_merge($segments, self::_getCategoryOld(intval($category_id)));
				if($cat_suffix == '1' && $default_index == '' && empty($item_id) && empty($alpha_char)) {
					$segments[] = "/";
				}
				
				if($default_index != '') {
					$segments[] = $default_index;
				}
				unset($vars['category_id']);
			}
			
			if (!empty($item_id)) {
				$segments[] = self::_getItemOld(intval($item_id));
				if ($this->params->get('googlenewsnum', '1') != '1') {
					$i = count($segments) - 1;
					$segments[$i] = self::_googleNews($segments[$i], $item_id);
				}
				unset($vars['item_id']);
			}
		
			if (isset($view)) {
				switch($view) {
					case 'category':
						if (isset($alpha_char)){
							$segments[] = JText::_('INDEX');
							$segments[] = $alpha_char;
							unset($vars['alpha_char']);
						}
						break;
					case 'item':
						break;
					case 'element':
						if (isset($method) && $method == 'download') {
							$segments[] = JText::_('DOWNLOAD');
							unset($vars['method']);
						}
						break;
					default:
						$segments[] = $view;
						break;
				}
				unset($vars['view']);
			}
			
			if (isset($page)) {
				$segments[] = JText::_('Page').' '.$page;
				unset($vars['page']);
			}
		}
		
		$metadata = parent::getMetaData($vars, $item_limitstart);
		
		unset($vars['limit']);
		unset($vars['limitstart']);
	}

    function _getApplication($id) {
		static $cache = array();
		
		if (!isset($cache[$id])) {
			$joomfish = $this->MijosefConfig->joomfish_trans_url ? ', id' : '';
			$row = MijoDatabase::loadObject("SELECT name, alias, description{$joomfish} FROM #__zoo_application WHERE id = {$id}");
			
			$name = (($this->params->get('applicationid_inc', '1') != '1') ? $id.' ' : '');
			if(parent::urlPart($this->params->get('application_part', 'global')) == 'title') {
				$name .= $row->name;
			} else {
				$name .= $row->alias;
			}
			
			$cache[$id]['name'] = $name;
			$cache[$id]['meta_title'] = $row->name;
			$cache[$id]['meta_desc'] = $row->description;
		}
		
		$this->meta_title[] = $cache[$id]['meta_title'];
		if (!empty($cache[$id]['meta_desc'])) {
			$this->meta_desc = $cache[$id]['meta_desc'];
		}
		
		return $cache[$id]['name'];
    }
	
    function _getCategory($id, $is_item = false) {
		static $cache = array();
		
		if (!isset($cache[$id])) {
			$joomfish = $this->MijosefConfig->joomfish_trans_url ? ', id' : '';
			
			$cats = $this->params->get('category_inc', '2');
			$categories = array();
			$cat_title = array();
			$cat_desc = array();
			$cat_keys = array();
			
			while ($id > 0) {
				$row = MijoDatabase::loadObject("SELECT name, alias, parent, application_id, description, params{$joomfish} FROM #__zoo_category WHERE id = {$id}");
				
				$name = (($this->params->get('categoryid_inc', '1') != '1') ? $id.' ' : '');
				if(parent::urlPart($this->params->get('category_part', 'global')) == 'title') {
					$name .= $row->name;
				} else {
					$name .= $row->alias;
				}
				
				$params = @json_decode($row->params, true);
				array_unshift($categories, $name);
				array_push($cat_title, $row->name);
				
				if($this->params->get('meta_desc_zoo', '1') == '2' && !empty($params['metadata.description'])){
					$cat_desc[] = $params['metadata.description'];
				}
				else {
					$cat_desc[] = $row->description;
				}
				
				if($this->params->get('meta_key_zoo', '1') == '2' && !empty($params['metadata.keywords'])){
					$cat_keys[] = $params['metadata.keywords'];
				}
				
				$id = $row->parent;
				if ($cats == '1'){
					break; //  Only last cat
				}
			}
			
			if (!$is_item && !empty($row->application_id) && $this->params->get('application_inc', '2') == '2'){
				$application = self::_getApplication($row->application_id);
				array_unshift($categories, $application);
			}
			
			$cache[$id]['name'] = $categories;
			$cache[$id]['meta_title'] = $cat_title;
			$cache[$id]['meta_desc'] = $cat_desc;
		}
		
		if (!$is_item && $this->params->get('application_inc', '2') == '2'){
			foreach($cache[$id]['meta_title'] AS $meta_title){
				array_unshift($this->meta_title, $meta_title);
			}
		} else {
			$this->meta_title = $cache[$id]['meta_title'];
		}
		
		if (!empty($cache[$id]['meta_desc'])) {
			$this->meta_desc = $cache[$id]['meta_desc'][0];
		}
		
		if (!empty($cache[$id]['cat_keys'])) {
			$this->key_cat = $cache[$id]['cat_keys'][0];
		}
		
		return $cache[$id]['name'];
    }

    function _getItem($id, $add_cat) {
		static $cache = array();
		
		if (!isset($cache[$id])) {
			list($row, $category) = self::_getItemFromDB($id, $add_cat);
			
			$name = (($this->params->get('itemid_inc', '1') != '1') ? $id.' ' : '');
			if (parent::urlPart($this->params->get('item_part', 'global')) == 'title') {
				$name .= $row->name;
			} else {
				$name .= $row->alias;
			}
			
			array_push($category, $name);
			
			if (!empty($row->application_id) && $this->params->get('application_inc', '2') == '2' && $this->params->get('item_categoryid_inc', '2') == '2'){
				$application = self::_getApplication($row->application_id);
				array_unshift($category, $application);
			}
			
			$desc_id = $this->params->get('description_id', 'fc6f9df9-8604-4ce2-a293-c60089ec6dbc');
			$desc_idss = explode(',', $desc_id);
			$elements = @json_decode($row->elements, true);
			
			foreach($desc_idss AS $desc_ids){
				if(!empty($elements[$desc_ids])){
					$item_desc = $elements[$desc_ids]['0']['value'];
					break;
				}
			}
			
			$params = @json_decode($row->params, true);
			
			if (!empty($params['metadata.description']) && $this->params->get('meta_desc_zoo', '1') == '2') {
				$item_desc = $params['metadata.description'];
			}
			if (!empty($params['metadata.keywords']) && $this->params->get('meta_key_zoo', '1') == '2') {
				$cache[$id]['meta_keys'] = $params['metadata.keywords'];
			}
			
			
			$cache[$id]['name'] = $category;
			$cache[$id]['meta_title'] = $row->name;
			$cache[$id]['meta_desc'] = $item_desc;
		}
		
		array_unshift($this->meta_title, $cache[$id]['meta_title']);
		
		if (!empty($cache[$id]['meta_desc'])) {
			$this->meta_desc = $cache[$id]['meta_desc'];
		}
		
		if (!empty($cache[$id]['meta_keys'])) {
			$this->meta_key = $cache[$id]['meta_keys'];
		}
		
		return $cache[$id]['name'];
    }

    function _getItemWP($id, $add_cat) {
		static $cache = array();
		
		if (!isset($cache[$id])) {
			list($row, $cat) = self::_getItemFromDB($id, $add_cat);
			
			if (!empty($row) && is_object($row)) {
				$name = array();
				$structure = $this->params->get('url_structure', 'daytitle');
				
				$date = explode('-', JFactory::getDate($row->publish_up)->toFormat('%Y-%m-%d'));
				$year = $date[0];
				$month = $date[1];
				$day = $date[2];
				
				$params = @json_decode($row->params, true);
				
				switch($structure) {
					case 'daytitle':
						$name[] = $year;
						$name[] = $month;
						$name[] = $day;
						$name[] = (($this->params->get('itemid_inc', '1') != '1') ? $id.' ' : '') . $row->name;
						
						$meta_title = $row->name;
						break;
					case 'monthtitle':
						$name[] = $year;
						$name[] = $month;
						$name[] = (($this->params->get('itemid_inc', '1') != '1') ? $id.' ' : '') . $row->name;
						
						$meta_title = $row->name;
						break;
					case 'numeric':
						$name[] = 'Archives';
						$name[] = $id;
						
						$meta_title = $row->name;
						break;
					case 'custom':
						$custom_structure = $this->params->get('custom_structure', '{category}/{item}');
						$array_structure = explode('/', $custom_structure);
						
						$category = $author = '';
						$item = (($this->params->get('itemid_inc', '1') != '1') ? $id.' ' : '') . $row->name;
						
						if (in_array('{category}', $array_structure) && !empty($cat)) {
							$category = implode('/', $cat);
						}
						
						if (in_array('{author}', $array_structure) && !empty($row->created_by)) {
							$author = self::_getUser(intval($row->created_by));
						}
						
						$search = array('{category}', '{item}', '{author}', '{year}', '{month}', '{day}');
						$replace = array($category, $item, $author, $year, $month, $day);
						$name[] = str_replace($search, $replace, $custom_structure);
						
						$meta_title = $row->name;
						break;
				}
				
				if (!empty($params['metadata.description']) && $this->params->get('meta_desc_zoo', '1') == '2') {
					$cache[$id]['meta_desc'] = $params['metadata.description'];
				}
				
				if (!empty($params['metadata.keywords']) && $this->params->get('meta_key_zoo', '1') == '2') {
					$cache[$id]['meta_keys'] = $params['metadata.keywords'];
				}
				
				$cache[$id]['name'] = $name;
				$cache[$id]['meta_title'] = $meta_title;
			}
			else {
				$cache[$id]['name'] = array();
				$cache[$id]['meta_title'] = '';
			}
		}
		
		array_unshift($this->meta_title, $cache[$id]['meta_title']);
		if (!empty($cache[$id]['meta_desc'])) {
			$this->meta_desc = $cache[$id]['meta_desc'];
		}
		
		if (!empty($cache[$id]['meta_keys'])) {
			$this->meta_key = $cache[$id]['meta_keys'];
		}
		
		return $cache[$id]['name'];
    }
	
	function _getItemFromDB($id, $add_cat) {
		$category = array();
		$joomfish = $this->MijosefConfig->joomfish_trans_url ? ', id' : '';
		
		if (!$add_cat && ($this->params->get('item_category_inc', '2') == '1')){
			$row = MijoDatabase::loadObject("SELECT name, alias, application_id, publish_up, created_by, params, elements{$joomfish} FROM #__zoo_item WHERE id = {$id}");
		}
		else {
			$row = MijoDatabase::loadObject("SELECT i.name, i.alias, ci.category_id, i.application_id, i.params, i.publish_up, i.created_by, i.elements{$joomfish} FROM #__zoo_item AS i, #__zoo_category_item AS ci WHERE ci.item_id = i.id AND i.id = {$id} AND ci.category_id != '0' LIMIT 1");
			
			if (!is_object($row)) {
				$row = MijoDatabase::loadObject("SELECT name, alias, application_id, params, publish_up, created_by, elements{$joomfish} FROM #__zoo_item WHERE id = {$id} LIMIT 1");
				$params = @json_decode($row->params, true);
				
				if (!empty($params['config.primary_category'])) {
					$category = self::_getItemCategory($params['config.primary_category'], true);
				}
				else {
					//Category Name
					$cat_item = MijoDatabase::loadObject("SELECT category_id, item_id FROM #__zoo_category_item WHERE item_id = {$id} AND category_id != '0' LIMIT 1");
					
					if (!empty($cat_item->category_id)){
						$category = self::_getItemCategory($cat_item->category_id, true);
					}
				}
			}
			else {
				$params = @json_decode($row->params, true);
				
				if (!empty($params['config.primary_category'])) {
					$category = self::_getItemCategory($params['config.primary_category'], true);
				}
				elseif (!empty($row->category_id)){
					$category = self::_getItemCategory($row->category_id, true);
				}
			}
		}
		
		return array($row, $category);
	}
	
    function _getItemCategory($id, $is_item = false) {
		static $cache = array();
		
		if (!isset($cache[$id])) {
			$joomfish = $this->MijosefConfig->joomfish_trans_url ? ', id' : '';
			
			$cats = $this->params->get('item_category_inc', '3');
			$categories = array();
			$cat_title = array();
			$cat_desc = array();
			$cat_keys = array();
			
			if ($cats == '1') {
				$id = 0; // No cat to add
			}
			
			while ($id > 0) {
				$row = MijoDatabase::loadObject("SELECT name, alias, parent, application_id, description, params{$joomfish} FROM #__zoo_category WHERE id = {$id}");
				
				$name = (($this->params->get('categoryid_inc', '1') != '1') ? $id.' ' : '');
				if(parent::urlPart($this->params->get('category_part', 'global')) == 'title') {
					$name .= $row->name;
				} else {
					$name .= $row->alias;
				}
				
				$params = @json_decode($row->params, true);
				array_unshift($categories, $name);
				array_push($cat_title, $row->name);
				
				if($this->params->get('meta_desc_zoo', '1') == '2' && !empty($params['metadata.description'])){
					$cat_desc[] = $params['metadata.description'];
				}
				else {
					$cat_desc[] = $row->description;
				}
				
				if($this->params->get('meta_key_zoo', '1') == '2' && !empty($params['metadata.keywords'])){
					$cat_keys[] = $params['metadata.keywords'];
				}
				
				$id = $row->parent;
				if ($cats == '2'){
					break; //  Only last cat
				}
			}
			
			if (!$is_item && !empty($row->application_id) && $this->params->get('application_inc', '2') == '2'){
				$application = self::_getApplication($row->application_id);
				array_unshift($categories, $application);
			}
			
			$cache[$id]['name'] = $categories;
			$cache[$id]['meta_title'] = $cat_title;
			$cache[$id]['meta_desc'] = $cat_desc;
		}
		
		if (!$is_item && $this->params->get('application_inc', '2') == '2'){
			array_merge($this->meta_title, $cache[$id]['meta_title']);
		} else {
			$this->meta_title = $cache[$id]['meta_title'];
		}
		
		if (!empty($cache[$id]['meta_desc'])) {
			$this->meta_desc = $cache[$id]['meta_desc'][0];
		}
		
		if (!empty($cache[$id]['cat_keys'])) {
			$this->meta_key = $cache[$id]['cat_keys'][0];
		}
		
		return $cache[$id]['name'];
    }

	function _getUser($id) {
		static $cache = array();
		
		if (!isset($cache[$id])) {
			$user = MijoDatabase::loadResult("SELECT name FROM #__users WHERE id = {$id}");

			if (!empty($user)) {
				$cache[$id]['name'] = $user;
			} else {
				$cache[$id]['name'] = $id;
			}
		}
		
		return $cache[$id]['name'];
    }

    function _getSubmission($id) {
		static $cache = array();
		
		if (!isset($cache[$id])) {
			$joomfish = $this->MijosefConfig->joomfish_trans_url ? ', id' : '';
			$row = MijoDatabase::loadRow( "SELECT name, alias$joomfish FROM #__zoo_submission WHERE id =".$id);
			
			$name = (($this->params->get('submissionid_inc', '1') != '1') ? $id.' ' : '');
			if (parent::urlPart($this->params->get('submission_part', 'global')) == 'title') {
				$name .= $row[0];
			} else {
				$name .= $row[1];
			}
			
			$cache[$id]['name'] = $name;
			$cache[$id]['meta_title'] = $row[0];
		}
		
		$this->meta_title[] = $cache[$id]['meta_title'];
		
		return $cache[$id]['name'];
    }
	
    function _getCatalogOld($id) {
		static $cache = array();
		
		if (!isset($cache[$id])) {
			$joomfish = $this->MijosefConfig->joomfish_trans_url ? ', id' : '';
			$row = MijoDatabase::loadRow("SELECT name, alias, description$joomfish FROM #__zoo_core_catalog WHERE id =".$id);
			
			$name = ( ($this->params->get('catalogid_inc', '1') != '1') ? $id.' ' : '' );
			if (parent::urlPart($this->params->get('catalog_part', 'global')) == 'title') {
				$name .= $row[0];
			} else {
				$name .= $row[1];
			}
			
			$cache[$id]['name'] = $name;
			$cache[$id]['meta_title'] = $row[0];
			$cache[$id]['meta_desc'] = $row[2];
		}
		
		$this->meta_title[] = $cache[$id]['meta_title'];
		if (!empty($cache[$id]['meta_desc'])) {
			$this->meta_desc = $cache[$id]['meta_desc'];
		}
		
		return $cache[$id]['name'];
    }

    function _getCategoryOld($id) {
		static $cache = array();
		
		if (!isset($cache[$id])) {
			$joomfish = $this->MijosefConfig->joomfish_trans_url ? ', id' : '';
			
			$cats = $this->params->get('category_inc', '3');
			$categories = array();
			$cat_title = array();
			$cat_desc = array();
			
			if ($cats == '1'){
				$id = 0; // No cat to add
			}
			while ($id > 0) {
				$row = MijoDatabase::loadRow("SELECT name, alias, parent, description$joomfish FROM #__zoo_core_category WHERE id =".$id);
				
				$name = (($this->params->get('categoryid_inc', '1') != '1') ? $id.' ' : '');
				if (parent::urlPart($this->params->get('category_part', 'global')) == 'title') {
					$name .= $row[0];
				} else {
					$name .= $row[1];
				}
				
				array_unshift($categories, $name);
				array_push($cat_title, $row[0]);
				$cat_desc[] = $row[3];
				
				$id = $row[2];
				if ($cats == '2'){
					break; //  Only last cat
				}
			}
			
			$cache[$id]['name'] = $categories;
			$cache[$id]['meta_title'] = $cat_title;
			$cache[$id]['meta_desc'] = $cat_desc;
		}
		
		array_unshift($this->meta_title, $cache[$id]['meta_title']);
		if (!empty($cache[$id]['meta_desc'])) {
			$this->meta_desc = $cache[$id]['meta_desc'][0];
		}
		
		return $cache[$id]['name'];
    }

    function _getItemOld($id) {
		static $cache = array();
		
		if (!isset($cache[$id])) {
			$joomfish = $this->MijosefConfig->joomfish_trans_url ? ', id' : '';
			$row = MijoDatabase::loadRow("SELECT name, alias$joomfish FROM #__zoo_core_item WHERE id =".$id);
			
			$name = ( ($this->params->get('itemid_inc', '1') != '1') ? $id.' ' : '');
			if (parent::urlPart($this->params->get('item_part', 'global')) == 'title') {
				$name .= $row[0];
			} else {
				$name .= $row[1];
			}
			
			$cache[$id]['name'] = $name;
			$cache[$id]['meta_title'] = $row[0];
		}
		
		array_unshift($this->meta_title, $cache[$id]['meta_title']);
		
		return $cache[$id]['name'];
    }

    function _getTag($tag) {
		static $cache = array();
		
		if (!isset($cache[$tag])) {
			$name = MijoDatabase::loadResult("SELECT name FROM #__zoo_tag WHERE name = '$tag'");
			$cache[$tag]['name'] = $name;
		}
		
		return $cache[$tag]['name'];
    }
	
	function _googleNews($segments, $id) {
        $num = '';
        $add = $this->params->get('googlenewsnum', '1');

        if($add == '2') {
            // Article ID
            $digits = trim($this->params->get('digits', '3'));
            if (!is_numeric($digits)) {
                $digits = '3';
            }

            $num = sprintf('%0'.$digits.'d', $id);
        }
        elseif ($add == '3') {
		
			if ($this->params->get('version_inc', '2') == '2'){
				//Version 2 / Publish date 
				$time = MijoDatabase::loadResult("SELECT publish_up FROM #__zoo_item WHERE id =".(int) $id);
			} else {
				//Version 1 / Publish date 
				$time = MijoDatabase::loadResult("SELECT publish_up FROM #__zoo_core_item WHERE id =".(int) $id);
			}
			$time = strtotime($time);

            $date = $this->params->get('dateformat', 'ddmm');

            $search = array('dd', 'd', 'mm', 'm', 'yyyy', 'yy');
            $replace = array(date('d', $time),
            date('j', $time),
            date('m', $time),
            date('n', $time),
            date('Y', $time),
            date('y', $time));
            $num = str_replace($search, $replace, $date);
        }
	
        if (!empty($num)) {
            $sep = $this->MijosefConfig->replacement_character;

            $where = $this->params->get('numberpos', '2');

            if ($where == '2') {
                $segments = $segments.$sep.$num;
            } else {
                $segments = $num.$sep.$segments;
            }
        }

        return $segments;
    }

	function getCategoryList($query) {
		$rows = array();
		
		$db =& JFactory::getDBO();
		$tables	= $db->getTableList();
		$prefix	= $db->getPrefix();
		$zoo_cats = $prefix."zoo_category";
		
		if (in_array($zoo_cats, $tables)){
			$rows = MijoDatabase::loadObjectList("SELECT c.id, CONCAT_WS( ' / ', a.name, c.name) AS name, c.parent FROM #__zoo_category AS c, #__zoo_application AS a WHERE c.application_id = a.id ORDER BY a.name, c.name");
		}
		
		return $rows;
	}

	function _a_b_c_mijosef() {
		jimport('joomla.plugin.helper');
		
		$file_1 = file_exists(JPATH_ADMINISTRATOR.'/components/com_mijosef/mijosef.php');
		$file_2 = file_exists(JPATH_ROOT.'/components/com_mijosef/mijosef.php');
		
		$mijosef_mode = Mijosef::getConfig()->mode;
		$plugin_mode = JPluginHelper::isEnabled('system', 'mijosef');
		$mijosef_pack = defined('MIJOSEF_PACK');
		
		if (!$file_2 || !$file_1 || !$mijosef_mode || !$plugin_mode || !$mijosef_pack) {
			die('a');
		}
	}
}

require_once(JPATH_ADMINISTRATOR.'/components/com_zoo/config.php');
	
if (class_exists('RouteHelper')) {
	class ZoooooRouteHelper extends RouteHelper {
		
		public function _findFrontpage($app_id) {
			return parent::_findFrontpage($app_id);
		}
		
		public function _findCategory($category_id) {
			return parent::_findCategory($category_id);
		}
		
		public function _findItem($item_id) {
			return parent::_findItem($item_id);
		}
	}
}
?>