<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

class paymentsTablePayments extends JTable
{
	function paymentsTablePayments(&$db )
	{
		parent::__construct( '#__ccidealplatform_payments', 'id', $db );
	}
}
?>