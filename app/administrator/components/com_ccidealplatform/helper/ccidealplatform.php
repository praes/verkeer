<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die;

class ccidealplatformHelper
{
	/**
	 * @var		JObject	A cache for the available actions.
	 */
	protected static $actions;

	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($vName)
	{

		// Groups and Levels are restricted to core.admin
		$canDo				= self::getActions();

		$jversion 			= new JVersion();
		$current_version 	= $jversion->getShortVersion();
		$jversion_sht		= substr($current_version,0,1);

		if($jversion_sht == "3"){

			$tabbingName="JHtmlSidebar";

			if ($canDo->get('ccidealplatform.viewpayments')) {
				JHtmlSidebar::addEntry(
					JText::_('ID_GLOBAL_CONFIGURATION'),
					'index.php?option=com_ccidealplatform&view=ccideal',
					$vName == 'ccideal'
				);

				JHtmlSidebar::addEntry(
					JText::_('ID_PAYMENTS'),
					'index.php?option=com_ccidealplatform&view=payments',
					$vName == 'payments'
				);

				if($canDo->get('ccidealplatform.integrations')) {

					JHtmlSidebar::addEntry(
						JText::_('CCIDEAL_INTEGRATIONS'),
						'index.php?option=com_ccidealplatform&view=integrations',
						$vName == 'integrations'
					);
				}

			}else{
				JHtmlSidebar::addEntry(
					JText::_('ID_GLOBAL_CONFIGURATION'),
					'index.php?option=com_ccidealplatform&view=ccideal',
					$vName == 'ccideal'
				);
			}
		}else{
			$tabbingName="JSubMenuHelper";

			if ($canDo->get('ccidealplatform.viewpayments')) {
				JSubMenuHelper::addEntry(
					JText::_('ID_GLOBAL_CONFIGURATION'),
					'index.php?option=com_ccidealplatform&view=ccideal',
					$vName == 'ccideal'
				);

				JSubMenuHelper::addEntry(
					JText::_('ID_PAYMENTS'),
					'index.php?option=com_ccidealplatform&view=payments',
					$vName == 'payments'
				);

				if($canDo->get('ccidealplatform.integrations')) {

					JSubMenuHelper::addEntry(
						JText::_('CCIDEAL_INTEGRATIONS'),
						'index.php?option=com_ccidealplatform&view=integrations',
						$vName == 'integrations'
					);
				}
			}else{
				JSubMenuHelper::addEntry(
					JText::_('ID_GLOBAL_CONFIGURATION'),
					'index.php?option=com_ccidealplatform&view=ccideal',
					$vName == 'ccideal'
				);
			}
		}
	}

	/**
	 * Gets a list of the actions that can be performed.
	 */
	public static function getActions()
	{

		 $user	= JFactory::getUser();
		 $result	= new JObject;

		if (empty($messageId)) {
			$assetName = 'com_ccidealplatform';
		}
		else {
			$assetName = 'com_ccidealplatform.message.'.(int) $messageId;
		}

		$actions = array('core.admin', 'core.manage', 'ccidealplatform.create', 'ccidealplatform.edit', 'ccidealplatform.edit.state', 'ccidealplatform.delete','ccidealplatform.updatestatus','ccidealplatform.config','ccidealplatform.viewpayments','ccidealplatform.integrations');

		foreach ($actions as $action) {
			$result->set($action,	$user->authorise($action, $assetName));
		}

		return $result;
	}
}