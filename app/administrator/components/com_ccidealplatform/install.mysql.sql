CREATE TABLE IF NOT EXISTS `#__ccidealplatform_config` (
`id` int(11) unsigned NOT NULL auto_increment,
`IDEAL_Bank` varchar(255) NOT NULL default '',
`IDEAL_Enable` varchar(255) NOT NULL default '',
`IDEAL_Description` varchar(255) NOT NULL default '',
`IDEAL_Mode` varchar(255) NOT NULL default '',
`IDEAL_MerchantID` varchar(255) NOT NULL default '',
`IDEAL_PrivatekeyPass` varchar(255) NOT NULL default '',
`IDEAL_SubID` varchar(255) NOT NULL default '',
`IDEAL_Privatecert` varchar(255) NOT NULL default '',
`IDEAL_Privatekey` varchar(255) NOT NULL default '',
`IDEAL_PartnerID` varchar(255) NOT NULL default '',
`IDEAL_ABNAMROEASY_ID` varchar(255) NOT NULL default '',
`IDEAL_Targetpay_ID` varchar(255) NOT NULL default '',
`iDEAL_ProVersion` int(11) unsigned NOT NULL,
`IDEAL_PrivatekeyPass_v3` varchar(255) NOT NULL default '',
`IDEAL_Privatecert_v3` varchar(255) NOT NULL default '',
`IDEAL_Privatekey_v3` varchar(255) NOT NULL default '',
`IDEAL_Status_ThankYou` mediumtext NOT NULL,
`IDEAL_Status_Failed` mediumtext NOT NULL,
`IDEAL_Status_Cancelled` mediumtext NOT NULL,
`IDEAL_pay_sucs_value` int(11) unsigned NOT NULL,
`IDEAL_pay_sucs_articleid` int(11) unsigned NOT NULL,
`IDEAL_pay_failed_value` int(11) unsigned NOT NULL,
`IDEAL_pay_fail_articleid` int(11) unsigned NOT NULL,
`IDEAL_pay_cancel_value` int(11) unsigned NOT NULL,
`IDEAL_pay_cancel_articleid` int(11) unsigned NOT NULL,
`IDEAL_mollie_profilekey` varchar(255) NOT NULL default '',
`ordering` int(11) unsigned NOT NULL,
PRIMARY KEY (`id`)
) ;

CREATE TABLE IF NOT EXISTS `#__ccidealplatform_payments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) NOT NULL DEFAULT '',
  `extension_id` varchar(255) NOT NULL DEFAULT '',
  `trans_id` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(11) unsigned NOT NULL,
  `account` varchar(255) NOT NULL DEFAULT '',
  `payment_total` decimal(15,5) NOT NULL,
  `payment_date` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL DEFAULT '',
  `extension` varchar(255) NOT NULL DEFAULT '',
  `article_extra_text` text NOT NULL,
  `cciDEAL_params` varchar(255) NOT NULL DEFAULT '',
  `ordering` int(11) unsigned NOT NULL,
  `pay_method` varchar(255) NOT NULL DEFAULT '',
  `payment_custom` varchar(255) NOT NULL DEFAULT '',  
  PRIMARY KEY (`id`)
);


CREATE TABLE IF NOT EXISTS `#__ccidealplatform_logs` (
  `id` int(255) NOT NULL auto_increment,
  `createDateTimeStamp` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `issuerAuthenticationURL` varchar(255) NOT NULL,
  `transactionID` varchar(255) NOT NULL,
  `purchaseID` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `merchantID` varchar(255) NOT NULL,
  `decription` varchar(255) NOT NULL,
  `entrance_code` varchar(255) NOT NULL,
  `IDEAL_Bank` varchar(255) NOT NULL,
  `IDEAL_Mode` varchar(245) NOT NULL,
  `token` varchar(255) NOT NULL,
  `token_code` varchar(255) NOT NULL,
  `last_tested_date` datetime NOT NULL,
  `after_12hrs` int(11) NOT NULL,
  `after_24hrs` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ;

CREATE TABLE IF NOT EXISTS `#__ccidealplatform_conf` (
`id` int(11) unsigned NOT NULL auto_increment,
`IDEAL_Taal` varchar(255) NOT NULL default '',
`IDEAL_AuthenticationType` varchar(255) NOT NULL default '',
`IDEAL_AcquirerTimeout` varchar(255) NOT NULL default '',
`IDEAL_ExpirationPeriod` varchar(255) NOT NULL default '',
`IDEAL_LogFile` varchar(255) NOT NULL default '',
`IDEAL_MerchantReturnURL` varchar(255) NOT NULL default '',
`IDEAL_Currency` varchar(255) NOT NULL default '',
`IDEAL_Language` varchar(255) NOT NULL default '',
`tmp_orderid` varchar(255) NOT NULL default '',
`ordering` int(11) unsigned NOT NULL,
PRIMARY KEY (`id`)
) ;

CREATE TABLE IF NOT EXISTS `#__ccidealplatform_paymentmethod` (
`id` int(11) unsigned NOT NULL auto_increment,
`method_value` varchar(255) NOT NULL default '',
`account_id` varchar(255) NOT NULL default '',
`method_name` varchar(255) NOT NULL default '',
PRIMARY KEY (`id`)
);