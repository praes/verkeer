<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');

class ccidealplatformModelPayments  extends JModelList
{
	public function __construct( $config = array() )
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 's.id',
				'extension', 's.extension',
				'trans_id', 's.trans_id',
				'order_id', 's.order_id',
				'user_id', 's.user_id',
				'payment_date', 's.payment_date',
				'payment_total', 's.payment_total',
				'article_extra_text', 's.article_extra_text',
				'payment_status', 's.payment_status',

			);
		}
		parent::__construct($config);
	}

	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
			$app		= JFactory::getApplication();

		// Adjust the context to support modal layouts.
			if ($layout = JRequest::getVar('layout')) {
				$this->context .= '.'.$layout;
			}

		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published		= $this->getUserStateFromRequest($this->context.'.filter.state', 'filter_state', '');
		$this->setState('filter.state', $published);

		// List state information.
			parent::populateState('s.payment_date', 'desc');
	}

	protected function getStoreId($id = '')
	{
		// Compile the store id.
			$id	.= ':'.$this->getState('filter.search');
			$id	.= ':'.$this->getState('filter.state');

		return parent::getStoreId($id);
	}

	protected function getListQuery()
	{
		$jversion 			= new JVersion();
		$current_version 	= $jversion->getShortVersion();
		$jversion			= substr($current_version,0,1);

		// Create a new query object.
			$db				= $this->getDbo();
			$query			= $db->getQuery(true);

		// Select the required fields from the table.
			$query->select( $this->getState(
				'list.select',
				's.*'
			) );
			$query->from( '#__ccidealplatform_payments as s' );

			// Filter by search in title.
			$search = $this->getState('filter.search');
			if (!empty($search))
			{
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('s.order_id LIKE '.$search.'OR s.extension_id LIKE '.$search.'OR s.extension LIKE'.$search);
			}

			if($filterstatus = $this->getState('filter.state'))
			{
				if ( $filterstatus == 'p' ) {
					$query->where('s.payment_status LIKE '.$db->Quote( '%pending%', false ));
				}
				else if ($filterstatus == 'c' ) {
					$query->where('s.payment_status LIKE '.$db->Quote( '%paid%', false ));
				}
				else if ($filterstatus == 'x' ) {
					$query->where('s.payment_status LIKE '.$db->Quote( '%cancelled%', false ));
				}
			}

		// Add the list ordering clause.
			$orderCol		= $this->state->get('list.ordering');
			$orderDirn	= $this->state->get('list.direction');

			$query->order($orderCol.' '.$orderDirn);

		return $query;
	}

	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(), 'post', 'array' );
		$row =& $this->getTable();
		if (count( $cids ))
		{
			foreach($cids as $cid)
			{
				if (!$row->delete( $cid ))
				{
					return false;
				}
			}
		}
		return true;
	}

	public function getTable($type = 'payments', $prefix = 'paymentsTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	function getstatusfilter()
	{
		$status1[] = JHTML::_('select.option',  '',  '- Select Status -');
        $status1[] = JHTML::_('select.option',  'p', 'Pending');
        $status1[] = JHTML::_('select.option',  'c', 'Paid');
        $status1[] = JHTML::_('select.option',  'x', 'Cancelled');
		$status1[] = JHTML::_('select.option',  'f', 'Failed');

        return $status1;
	}
	public function getstatusdropdown(){

		$options	= '';
		$options[]	= JHTML::_('select.option',  '',JText::_("- Select Status -"));
		$options[]	= JHTML::_('select.option',  'pending', 'Pending');
		$options[]	= JHTML::_('select.option',  'paid', 'Paid');
		$options[]	= JHTML::_('select.option',  'cancelled', 'Cancelled');
		$options[]	= JHTML::_('select.option',  'failed', 'Failed');

		return $options;
	}
	function getconfiguration(){
		
		$db = JFactory::getDBO();
		$query = "SELECT * FROM #__ccidealplatform_config where id = 1" ;
		$db->setQuery($query);
		$ideal_config_array = $db->loadObject();
		
		return $ideal_config_array;		
	}
}
?>