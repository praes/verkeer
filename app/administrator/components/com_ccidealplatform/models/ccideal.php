<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details. 
* @license	GNU/GPL, see LICENSE.php for full license.
**/


// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
jimport('joomla.html.pagination');

$jversion 			= new JVersion();
$current_version 	= $jversion->getShortVersion();
$jversion			= substr($current_version,0,1);

if(!class_exists('cciDealPlatformBridgeModel')){
	if($jversion != 3){
		class cciDealPlatformBridgeModel extends JModel {}
	} else {
		class cciDealPlatformBridgeModel extends JModelList {}
	}
}

class cciDealPlatformModelccideal extends cciDealPlatformBridgeModel
{

	var $_psclass = null;
	var $_query;
	var $_data;
	var $_total=null;
	var $_pagination=null;

	/**
	 * Constructor that retrieves the ID from the request
	 *
	 * @access	public
	 * @return	void
	 */
	function __construct()
	{
			parent::__construct();

	}

// save

	function save()
	{

	global $mainframe;

	jimport('joomla.filesystem.path');
	jimport('joomla.filesystem.file');

	// Check for request forgeries
	JRequest::checkToken() or jexit( 'Invalid Token' );

	$row =  $this->getTable();
	$row->load("1");

		// Bind the form fields
	$post = JRequest::get( 'post' );

	/* Certificate Uploade Start */
	$cert_file = JRequest::getVar( 'IDEAL_Privatecert', '', 'files', 'array' );
	$key_file = JRequest::getVar( 'IDEAL_Privatekey', '', 'files', 'array' );


	$formatC = strtolower(JFile::getExt($cert_file['name']));
	$formatP = strtolower(JFile::getExt($key_file['name']));

//Check the version and store it in db
	if($post['iDEAL_ProVersion']){
		if($formatC=='cer'){
			$post['IDEAL_Privatecert_v3']	= $cert_file['name'];
		}
		if($formatP=='pem'){
			$post['IDEAL_Privatekey_v3'] 	= $key_file['name'];
		}
	}else{
		$post['IDEAL_Privatecert'] 			= $cert_file['name'];
		$post['IDEAL_Privatekey'] 			= $key_file['name'];
	}

	//Remove Special characters and spaces from Descriptions
 		if(!empty($post['IDEAL_Description'])){
	 	    $post['IDEAL_Description'] =  $this->cciDEALremoveSplChr($post['IDEAL_Description']);;
		}

	//Remove Special characters and spaces from Purchase ID
		if(!empty($post['IDEAL_MerchantID'])){
				$post['IDEAL_MerchantID'] = $this->cciDEALremoveSplChr($post['IDEAL_MerchantID']);
		}
		if(!empty($post['IDEAL_PartnerID'])){
				$post['IDEAL_PartnerID'] = $this->cciDEALremoveSplChr($post['IDEAL_PartnerID']);
		}
		if(!empty($post['IDEAL_ABNAMROEASY_ID'])){
				$post['IDEAL_ABNAMROEASY_ID'] = $this->cciDEALremoveSplChr($post['IDEAL_ABNAMROEASY_ID']);
		}
		if(!empty($post['IDEAL_Targetpay_ID'])){
				$post['IDEAL_Targetpay_ID'] = $this->cciDEALremoveSplChr($post['IDEAL_Targetpay_ID']);
		}
		if(!empty($post['IDEAL_PrivatekeyPass'])){
				$post['IDEAL_PrivatekeyPass'] = $this->cciDEALremoveSplChr($post['IDEAL_PrivatekeyPass']);
		}
		if(!empty($post['IDEAL_SubID'])){
				$post['IDEAL_SubID'] = $this->cciDEALremoveSplChr($post['IDEAL_SubID']);
		}
		if(!empty($post['IDEAL_PrivatekeyPass_v3'])){
				$post['IDEAL_PrivatekeyPass_v3'] = $this->cciDEALremoveSplChr($post['IDEAL_PrivatekeyPass_v3']);
		}


	if(!isset($post['IDEAL_Privatecert_v3']))
	{
		$post['IDEAL_Privatecert_v3']=$row->IDEAL_Privatecert_v3;
	}
	if(!isset($post['IDEAL_Privatekey_v3']))
	{
		$post['IDEAL_Privatekey_v3']=$row->IDEAL_Privatekey_v3;
	}

	if(!isset($post['IDEAL_Privatecert']))
	{
		$post['IDEAL_Privatecert']=$row->IDEAL_Privatecert;
	}

	if(trim(@$post['IDEAL_Privatecert'])=="")
	{
		$post['IDEAL_Privatecert']=$row->IDEAL_Privatecert;
	}

	if(!isset($post['IDEAL_Privatekey']))
	{
		$post['IDEAL_Privatekey']=$row->IDEAL_Privatekey;
	}
	if(trim(@$post['IDEAL_Privatekey'])=="")
	{
		$post['IDEAL_Privatekey']=$row->IDEAL_Privatekey;
	}

	if(trim(@$post['emailnotify'])=="")
	{
		$post['emailnotify']=$row->emailnotify;
	}
	if(trim(@$post['emailsss'])=="")
	{
		$post['emailsss']=$row->emailsss;
	}
	if(trim(@$post['emailsubject'])=="")
	{
		$post['emailsubject']=$row->emailsubject;
	}
	if(trim(@$post['emailbody'])=="")
	{
		$post['emailbody']=$row->emailbody;
	}
	if(trim(@$post['download_id'])=="")
	{
		$post['download_id']=$row->download_id;
	}
	if(trim(@$post['minimum_stability'])=="")
	{
		$post['minimum_stability']=$row->minimum_stability;
	}

	if($post['IDEAL_Bank'] == "Mollie"){
		$ideal_paymentmethod = $post['mollie_method'];
	}elseif($post['IDEAL_Bank'] == "TARGETPAY"){
		$ideal_paymentmethod = $post['targetpay_method'];
	}elseif($post['IDEAL_Bank'] == "RABOOMNIKASSA"){
		$ideal_paymentmethod = $post['raboomni_method'];
	}elseif($post['IDEAL_Bank'] == "SISOW"){
		$ideal_paymentmethod = $post['sisow_method'];
	}

	if(trim(@$ideal_paymentmethod)=="")
	{
		$post['ideal_paymentmethod']='';
	}else{
		$post['ideal_paymentmethod'] = $ideal_paymentmethod;
	}
	if (!$row->bind($post)) {
	return 0;
	}

	// Make sure data is valid
	if (!$row->check())
	{
	return 0;
	}

	// Store it
	if (!$row->store())
	{
	return 0;
	}

	/* Certificate Uploade Start */

/* Certificate Uploade Start */
	$cert_file = JRequest::getVar( 'IDEAL_Privatecert', '', 'files', 'array' );
	$key_file = JRequest::getVar( 'IDEAL_Privatekey', '', 'files', 'array' );

	$bank = JRequest::getVar( 'IDEAL_Bank', '' );


	/*Direct the certificate path based on the bank*/
	$bank_file = "ccideal_".strtolower($bank);
	if($post['iDEAL_ProVersion']){

		if(strtolower($post['IDEAL_Bank'])==strtolower('RABOPROF')){
			$cert_filepath = JPATH_SITE . DS .'components'. DS .'com_ccidealplatform'. DS .'ccideal_adv_pro_idealv3'. DS .'rabo_certificates' . DS.$cert_file['name'];
			$key_filepath = JPATH_SITE . DS .'components'. DS .'com_ccidealplatform'. DS .'ccideal_adv_pro_idealv3'. DS .'rabo_certificates'. DS.$key_file['name'];
		}else{
			$cert_filepath = JPATH_SITE . DS .'components'. DS .'com_ccidealplatform'. DS .'ccideal_adv_pro_idealv3'. DS .'ing_certificates' . DS.$cert_file['name'];
			$key_filepath = JPATH_SITE . DS .'components'. DS .'com_ccidealplatform'. DS .'ccideal_adv_pro_idealv3'. DS .'ing_certificates'. DS.$key_file['name'];
		}

	}

	$cert_file['name'] = JFile::makeSafe($cert_file['name']);

	if (isset($cert_file['name']) && $cert_file['name'] != '') {
		$format = strtolower(JFile::getExt($cert_file['name']));

		if($format != "cer") {
		return -3;
		}
		else if ( JFile::exists($cert_filepath) && !JFile::delete($cert_filepath) ) {
		return -4;
		}
		if (!JFile::upload($cert_file['tmp_name'], $cert_filepath)) {
		return -5;
		}
	}

	$key_file['name'] = JFile::makeSafe($key_file['name']);

	if ( isset($key_file['name']) && $key_file['name'] != '') {
		$format = strtolower(JFile::getExt($key_file['name']));

		if($format != "pem") {
		return -3;
		}
		else if ( JFile::exists($key_filepath) && !JFile::delete($key_filepath) ) {
		return -6;
		}
		if (!JFile::upload($key_file['tmp_name'], $key_filepath)) {
		return -7;
		}
	}


	/* Certificate Uploade End */

	/*Update Logpath & Return URL */
	$log_file = JPATH_SITE.DS.'components'.DS.'com_ccidealplatform'. DS.$bank_file.DS.'thinmpi.log';
	$merchant_retrunurl = JURI :: ROOT()."/index.php?option=com_ccidealplatform&task=checkccidealresult";

	$db = JFactory :: getDBO();
	$query = "UPDATE #__ccidealplatform_conf SET IDEAL_LogFile = '".$log_file."' , IDEAL_MerchantReturnURL = '".$merchant_retrunurl."' WHERE id = '1' ";
	$db->setQuery( $query );
	$db->query();

	/*Update Logpath & Return URL */

	return $row->id;

	}

		/**
	 *
	 * Remove special characters
	 *
	 *
	 */

	 function cciDEALremoveSplChr($postVar){
	 	$stringiDEALDesc =strip_tags($postVar,"");
 		$stringiDEALDesc =trim($postVar);
 		$t=0;
		if (preg_match('/\//', $stringiDEALDesc)) {
			$t=1;
			$date=date("dM");
			$stringiDEALDesc = str_replace("/",$date,$stringiDEALDesc);
		}
 	 	$stringiDEALDesc = preg_replace("/[^A-Za-z0-9\s.+?().,;:_s-]/",'',$stringiDEALDesc);
 	 	$stringiDEALDesc = str_replace(" ","",$stringiDEALDesc);

		if($t=1){
		 	$date=date("dM");
		  	$stringiDEALDesc = str_replace($date,"/",$stringiDEALDesc);
		 	return $stringiDEALDesc;
		}else{
	 	  	return $stringiDEALDesc;
		}
	 }

	function &getBankData(){
		$db = JFactory::getDBO();

		$query = "SELECT * FROM #__ccidealplatform_config" ;
		$db->setQuery($query);
		$ideal_config_array = $db->loadObject();
		return $ideal_config_array;
	}

	function getBankList($mode)
	{

		$test = '';

		$bank_list = array(
			JHTML::_('select.option', '', JText::_( 'ID_SELECT_BANK' )),				
			
			JHTML::_('select.option', 'ABNAMROTEST'.$test, 'ABN AMRO iDEAL Easy' ),
			JHTML::_('select.option', 'ING'.$test, 'ING Advanced'),
			JHTML::_('select.option', 'POSTBASIC'.$test, 'ING Basic'),
			JHTML::_('select.option', 'Mollie'.$test, 'Mollie'),
			JHTML::_('select.option', 'RABOBANKTEST'.$test, 'Rabobank iDEAL Lite' ),
			JHTML::_('select.option', 'RABOPROF'.$test, 'Rabobank iDEAL Professional' ),
			JHTML::_('select.option', 'RABOOMNIKASSA'.$test, 'Rabo OmniKassa' ),
			JHTML::_('select.option', 'SISOW'.$test, 'Sisow' ),
			JHTML::_('select.option', 'TARGETPAY'.$test, 'TargetPay' )

		);
		return $bank_list;
	}

	function iDEALProVersion(){
		$bank_versn = array(
		JHTML::_('select.option', '0', JTEXT::_('CCIDEALPLATFORM_IDEALADVANCE_V2') ),
		JHTML::_('select.option', '1', JTEXT::_('CCIDEALPLATFORM_IDEALADVANCE_V3') ),
		);
		return $bank_versn;
	}


	function getIDealStatusList()
		{
		$ideal_statuslist = array(
		JHTML::_('select.option', 'TEST', JText::_( 'ID_TEST' )),
		JHTML::_('select.option', 'PROD', JText::_( 'ID_PRODUCTION' ) )
		);

		return $ideal_statuslist;
	}

	function getminimum_stability()
	{
		$minimum_stability = array(
			JHTML::_('select.option', 'alpha', JText::_( 'Alpha (unstable)' )),
			JHTML::_('select.option', 'beta', JText::_( 'Beta (testing)' )),
			JHTML::_('select.option', 'rc', JText::_( 'Release Candidate (almost stable)' )),
			JHTML::_('select.option', 'stable', JText::_( 'Stable (use on production sites)' ))
		);

		return $minimum_stability;
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'ccideal', $prefix = 'ccidealTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	/*function gettargetpaylist()
	{
		$db 	= JFactory::getDBO();

		$query 	= "SELECT method_value,method_name FROM #__ccidealplatform_paymentmethod WHERE account_id = 'TARGETPAY'";
		$db->setQuery($query);
		$paymentmethod_array = $db->loadObjectlist();

		$bank_list	 = array();

		$bank_list[] = JHTML::_('select.option', '', JText::_( 'ID_SELECT_BANK' ));

		foreach($paymentmethod_array as $paymethod){
			$bank_list[] = JHTML::_('select.option', $paymethod->method_value, $paymethod->method_name);
		}

		return $bank_list;
	}
	function getmollielist()
	{
		$db 	= JFactory::getDBO();

		$query 	= "SELECT method_value,method_name FROM #__ccidealplatform_paymentmethod WHERE account_id = 'MOLLIE'";
		$db->setQuery($query);
		$paymentmethod_array = $db->loadObjectlist();

		$bank_list	 = array();

		$bank_list[] = JHTML::_('select.option', '', JText::_( 'ID_SELECT_BANK' ));

		foreach($paymentmethod_array as $paymethod){
			$bank_list[] = JHTML::_('select.option', $paymethod->method_value, $paymethod->method_name);
		}

		return $bank_list;
	}
	function getraboomnilist()
	{
		$db 	= JFactory::getDBO();

		$query 	= "SELECT method_value,method_name FROM #__ccidealplatform_paymentmethod WHERE account_id = 'RABOOMNIKASSA'";
		$db->setQuery($query);
		$paymentmethod_array = $db->loadObjectlist();

		$bank_list	 = array();

		$bank_list[] = JHTML::_('select.option', '', JText::_( 'ID_SELECT_BANK' ));

		foreach($paymentmethod_array as $paymethod){
			$bank_list[] = JHTML::_('select.option', $paymethod->method_value, $paymethod->method_name);
		}

		return $bank_list;
	}
	function getsisowlist()
	{
		$db 	= JFactory::getDBO();

		$query 	= "SELECT method_value,method_name FROM #__ccidealplatform_paymentmethod WHERE account_id = 'SISOW'";
		$db->setQuery($query);
		$paymentmethod_array = $db->loadObjectlist();

		$bank_list	 = array();

		$bank_list[] = JHTML::_('select.option', '', JText::_( 'ID_SELECT_BANK' ));

		foreach($paymentmethod_array as $paymethod){
			$bank_list[] = JHTML::_('select.option', $paymethod->method_value, $paymethod->method_name);
		}

		return $bank_list;
	}*/
}
?>