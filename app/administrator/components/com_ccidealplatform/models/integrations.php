<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');

class ccidealplatformModelIntegrations  extends JModelList
{
	public function getItems()
	{

		$plugins		= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'plugins.xml');
		$count			= count($plugins->name);

		$result			= array();


		for($i=0;$i < $count;$i++){

			$name 		= trim($plugins->name[$i]);
			$element 	= trim($plugins->element[$i]);
			$folder 	= trim($plugins->folder[$i]);
			//$dname	 	= trim($plugins->dname[$i]);

			// Check if extension is installed in db

				$db				= $this->getDbo();
				$query			= $db->getQuery(true);

				$query->select('s.element,s.type,s.folder');
				$query->from( '#__extensions as s' );
				$query->where('s.type="plugin" AND (s.element = "'.$element.'" AND s.folder="'.$folder.'" )');

				$db->setQuery($query);

				$extensions_element = $db->loadResult();
				//$extensions_element = "vmpayment";


				$result[$i]['extensions_element'] 	= $extensions_element;

			//enabled & jversion used for sorting
			$result[$i]['enabled'] 		= '0';

			if(($element == "plg_virtuemart20_ideal") || ($element == "rs_payment_ccidealplatform") || ($element == "scideal")){
				$result[$i]['jversion'] 	= '2';
			}else{
				$result[$i]['jversion'] 	= '3';
			}

			if($element == "iDEAL3"){
				$element = "iDEAL";
			}

			$result[$i]['extn_name'] 	= $name;
			$result[$i]['element'] 		= $element;
			$result[$i]['folder'] 		= $folder;

			$result[$i] = (object)$result[$i];
		}

		$this->plugvalue($result);

		//Array sorting
		ksort($result); // was arsort

		//var_dump($result); //jexit();

		$this->manifest_cache($result);

		return $result;
	}

	function plugvalue($tests){

		foreach ($tests as &$test)
		{
			if(strlen($test->element)){

				$db				= $this->getDbo();
				$query			= $db->getQuery(true);

				$query->select('s.extension_id,s.type,s.enabled,s.access,s.manifest_cache');
				$query->from( '#__extensions as s' );
				$query->where('s.type="plugin" AND (s.element = "'.$test->element.'" AND s.folder="'.$test->folder.'" )');

				// Join over the asset groups.
				$query->select('ag.title AS access_level')->join('LEFT', '#__viewlevels AS ag ON ag.id = s.access');

				$db->setQuery($query);

				$data = $db->loadObject();

				if ($data)
				{
					foreach ($data as $key => $value)
					{
						$test->$key = $value;
					}
				}
			}
		}
	}

	function manifest_cache($rows){

		foreach ($rows as &$row)
		{
			if(strlen(@$row->manifest_cache)){
				$data = json_decode($row->manifest_cache);

				if ($data)
				{
					foreach ($data as $key => $value)
					{
						$row->$key = $value;
					}
				}
			}
		}
	}

	public function current_version($element, $folder){

		// David - 2015-07-27
		// Integrations view, implemented better method to get current version of integration plugins, directly from XML file

		// Check if current plugin is installed
		$db				= $this->getDbo();
		$query			= $db->getQuery(true);

		$query->select('s.element,s.type,s.folder');
		$query->from( '#__extensions as s' );
		$query->where('s.type="plugin" AND (s.element = "'.$element.'" AND s.folder="'.$folder.'" )');

		$db->setQuery($query);

		$installed = $db->loadResult();

		// If plugin is not installed, set version to dash
		// If plugin is installed, load XML and get version
		if ( !$installed ) {
			$current_version = "-";
			return $current_version;
		} else {

			$xml = "";

			if ( ( $element == "content_ideal" ) && ( $folder == "content" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'content' . DS . 'content_ideal' . DS . 'content_ideal.xml' );
			} elseif ( ( $element == "plg_ccinvoices_ideal" ) && ( $folder == "ccinvoices_payment" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'ccinvoices_payment' . DS . 'plg_ccinvoices_ideal' . DS . 'plg_ccinvoices_ideal.xml' );
			} elseif ( ( $element == "iDEAL" ) && ( $folder == "akpayment" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'akeeba' . DS . 'plg_akeebasubs_ideal' . DS . 'iDEAL.xml' );
			} elseif ( ( $element == "iDEAL3" ) && ( $folder == "akpayment" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'akeeba3' . DS . 'plg_akeebasubs_ideal' . DS . 'iDEAL.xml' );
			} elseif ( ( $element == "iDEAL" ) && ( $folder == "hikashoppayment" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'hikashoppayment' . DS . 'iDEAL' . DS . 'iDEAL.xml' );
			} elseif ( ( $element == "rdmccidealplatform" ) && ( $folder == "rdmedia" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'rdmedia' . DS . 'rdmccidealplatform' . DS . 'rdmccidealplatform.xml' );
			} elseif ( ( $element == "rsmembership_ccidealplatform" ) && ( $folder == "system" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'rsmembership' . DS . 'plg_rsmembership_ideal' . DS . 'rsmembership_ccidealplatform.xml' );
			} elseif ( ( $element == "rsepro_ideal" ) && ( $folder == "system" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'plg_rseventspro_ideal' . DS . 'rsepro_ideal.xml' );
			} elseif ( ( $element == "rsfp_ideal" ) && ( $folder == "system" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsfp_ideal' . DS . 'rsfp_ideal.xml' );
			} elseif ( ( $element == "plg_joomisp_ideal" ) && ( $folder == "payment" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'joomisp' . DS . 'plg_joomisp_ideal' . DS . 'plg_joomisp_ideal.xml' );
			} elseif ( ( $element == "plg_sobipro_ideal" ) && ( $folder == "system" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'sobipro' . DS . 'plg_sobipro_ideal' . DS . 'plg_sobipro_ideal.xml' );
			} elseif ( ( $element == "plg_virtuemart20_ideal" ) && ( $folder == "vmpayment" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart20_ideal' . DS . 'plg_virtuemart20_ideal.xml' );
			} elseif ( ( $element == "plg_virtuemart3_ideal" ) && ( $folder == "vmpayment" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart3_ideal' . DS . 'plg_virtuemart3_ideal.xml' );
			} elseif ( ( $element == "rs_payment_ccidealplatform" ) && ( $folder == "redshop_payment" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'redshop' . DS . 'plg_redshop_ideal' . DS . 'rs_payment_ccidealplatform.xml' );
			} elseif ( ( $element == "scideal" ) && ( $folder == "content" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'simplecaddy' . DS . 'plg_simplecaddy_ideal' . DS . 'scideal.xml' );
			} elseif ( ( $element == "rsdirectoryideal" ) && ( $folder == "system" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'plg_rsdirectoryideal' . DS . 'rsdirectoryideal.xml' );
			} elseif ( ( $element == "payment_ccideal" ) && ( $folder == "j2store" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'j2store' . DS . 'plg_j2store_payment_ccideal' . DS . 'payment_ccideal.xml' );
			} elseif ( ( $element == "ccideal" ) && ( $folder == "payment" ) ) {
				$xml = JFactory::getXML( JPATH_ROOT . DS . 'plugins' . DS . 'techjoomla' . DS . 'ccideal' . DS . 'ccideal.xml' );
			}

			$current_version = (string) $xml->version;

			return $current_version;
		}
	}

	public function latest_version($element, $folder){

		$xml = "";

		if(($element == "content_ideal")&&($folder == "content")){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'content_ideal'.DS.'content_ideal.xml');
		}elseif(($element == "plg_ccinvoices_ideal")&&($folder == "ccinvoices_payment")){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'ccinvoices'.DS.'plg_ccinvoices_ideal'.DS.'plg_ccinvoices_ideal.xml');
		}elseif(($element == "iDEAL")&&($folder == "akpayment" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'akeeba'.DS.'plg_akeebasubs_ideal'.DS.'iDEAL.xml');
		}elseif(($element == "iDEAL3")&&($folder == "akpayment" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'akeeba3'.DS.'plg_akeebasubs_ideal'.DS.'iDEAL.xml');
		}elseif(($element == "iDEAL")&&($folder == "hikashoppayment" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'hikashop'.DS.'plg_hikashop_ideal'.DS.'iDEAL.xml');
		}elseif(($element == "rdmccidealplatform")&&($folder == "rdmedia" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'ticketmaster'.DS.'rdmccidealplatform'.DS.'rdmccidealplatform.xml');
		}elseif(($element == "rsmembership_ccidealplatform")&&($folder == "system" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'rsmembership'.DS.'plg_rsmembership_ideal'.DS.'rsmembership_ccidealplatform.xml');
		}elseif(($element == "rsepro_ideal")&&($folder == "system" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'rseventspro'.DS.'plg_rseventspro_ideal'.DS.'rsepro_ideal.xml');
		}elseif(($element == "rsfp_ideal")&&($folder == "system" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'rsformpro'.DS.'plg_rsformpro_ideal'.DS.'rsfp_ideal.xml');
		}elseif(($element == "plg_joomisp_ideal")&&($folder == "payment" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'joomisp'.DS.'plg_joomisp_ideal'.DS.'plg_joomisp_ideal.xml');
		}elseif(($element == "plg_sobipro_ideal")&&($folder == "system" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'sobipro'.DS.'plg_sobipro_ideal'.DS.'plg_sobipro_ideal.xml');
		}elseif(($element == "plg_virtuemart20_ideal")&&($folder == "vmpayment" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'virtuemart2'.DS.'plg_virtuemart_20_ideal'.DS.'plg_virtuemart20_ideal.xml');
		}elseif(($element == "plg_virtuemart3_ideal")&&($folder == "vmpayment" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'virtuemart3'.DS.'plg_virtuemart3_ideal'.DS.'plg_virtuemart3_ideal.xml');
		}elseif(($element == "rs_payment_ccidealplatform")&&($folder == "redshop_payment" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'redshop'.DS.'plg_redshop_ideal'.DS.'rs_payment_ccidealplatform.xml');
		}elseif(($element == "scideal")&&($folder == "content" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'simplecaddy'.DS.'plg_simplecaddy_ideal'.DS.'scideal.xml');
		}elseif(($element == "rsdirectoryideal")&&($folder == "system" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'rsdirectory'.DS.'plg_rsdirectoryideal'.DS.'rsdirectoryideal.xml');
		}elseif(($element == "payment_ccideal")&&($folder == "j2store" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'j2store'.DS.'plg_j2store_payment_ccideal'.DS.'payment_ccideal.xml');
		}elseif(($element == "ccideal")&&($folder == "payment" )){
			$xml    	 	= JFactory::getXML(JPATH_COMPONENT.DS.'plugins'.DS.'techjoomla'.DS.'ccideal'.DS.'ccideal.xml');
		}

		$content		= (string)$xml->version;

		return $content;
	}

	public function publish(){

	// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$extension_id		= JRequest::getVar( 'extension_id');
		$extension_id 		= str_replace('cb','',$extension_id);
		$extension_id 		= trim($extension_id);

	// Initialize variables
		$db			= JFactory::getDBO();
		//$user		= JFactory::getUser();
		$publish 	= "";

		$task		= JRequest::getCmd( 'task' );

		if($task == 'unpublish'){
			$publish = 0;
		}

		$publish	= ($task == 'publish');

		$query = 'UPDATE #__extensions'
		. ' SET enabled = ' . (int) $publish
		. ' WHERE extension_id = '.$extension_id
		;


		$db->setQuery( $query );
		if ($db->query()) {
			return true;
		}
	}
	public function component_install($oextension){

		//exit;

		$db =  JFactory::getDBO();
		$db->setQuery("SELECT extension_id FROM #__extensions WHERE `element`='".$oextension."' AND type='component'");
		$extension_id = $db->loadResult();

		return $extension_id;
	}
}
?>