<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class cciDealPlatformViewpayments extends JViewLegacy
{
		function display($tpl = null)
		{
			JToolBarHelper::title( JText::_( 'ID_IDEAL_TITLE' ) . ': ' . JText::_('ID_PAYMENTS'), 'ccideal.png' );

			// Take Current Joomla Version
			$jversion 			= new JVersion();
			$current_version 	= $jversion->getShortVersion();
			$this->jversion_sht	= substr($current_version,0,1);

			$this->items		= $this->get('Items');
			$this->state		= $this->get('State');
			$pagination			= $this->get('pagination');
			$this->statusfilter	= $this->get('statusfilter');
			$this->configuration= $this->get('configuration');	

			$pageNav			= $pagination;

			$where = array();		

			$canDo = ccidealplatformHelper::getActions();

			$this->addToolbar();

			if($this->jversion_sht == "3"){
				$this->addFilter();
				$this->sidebar = JHtmlSidebar::render();
			}

			$xml    	 = JFactory::getXML(JPATH_COMPONENT.DS.'ccidealplatform.xml');
	   		$version_no  = (string)$xml->version;
			$name		 = (string)$xml->name;

	   		$this->statusdropdown 	= $this->get('statusdropdown');

			$this->assignRef('version', 	$version_no);
			$this->assignRef('name', 	$name);
			$this->assignRef('pageNav', 	$pageNav);


			parent::display($tpl);
		}

		protected function addToolbar()
		{
			$canDo = ccidealplatformHelper::getActions();

			if ($canDo->get('ccidealplatform.delete')) {
				JToolBarHelper::divider();
				JToolBarHelper::custom('payments.remove', 'remove','', JText::_('CCIDEALPLATFORM_DELETE'));
			}

			if ($canDo->get('core.admin')) {
				JToolBarHelper::preferences('com_ccidealplatform','550','875',JTEXT::_('JTOOLBAR_OPTIONS_PERMISSIONS'));
			}
		}
		
		function addFilter()
		{
			$filter_state	= JRequest::getVar('filter_state');

			$status1[] = JHTML::_('select.option',  '',  '- Select Status -');
	        $status1[] = JHTML::_('select.option',  'p', 'Pending');
	        $status1[] = JHTML::_('select.option',  'c', 'Paid');
	        $status1[] = JHTML::_('select.option',  'x', 'Cancelled');

			JHtmlSidebar::addFilter(
				'',
				'filter_state',
				JHtml::_('select.options',  $status1, 'value', 'text', $filter_state)
			);
		}

		protected function getSortFields()
		{
			return array(
				's.extension' => JText::_('ID_EXTN'),
				's.trans_id' => JText::_('ID_TRANS_ID'),
				's.order_id' => JText::_('ID_ORDER_NO'),
				's.user_id' => JText::_('USERID'),
				's.payment_date' => JText::_('ID_ORDER_DATE'),
				's.payment_total' => JText::_('ID_TOTAL_AMOUNT'),
				's.article_extra_text' => JText::_('CCIDEALPLATFORM_EXTRA_INFORMATION'),
				's.payment_status' => JText::_('ID_ORDER_STATUS')

			);
		}
	}
?>