<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$jversion_sht	= $this->jversion_sht;

$listOrder		= $this->state->get('list.ordering');
$listDirn		= $this->state->get('list.direction');
$sortFields 	= $this->getSortFields();
$filter_state	= JRequest::getVar('filter_state');

if($jversion_sht == 3){
	JHtml::_('behavior.tooltip');
	JHtml::_('behavior.formvalidation');
	JHtml::_('behavior.keepalive');
	JHtml::_('formbehavior.chosen', 'select');
	JHtml::_('bootstrap.tooltip');
}
?>

<script type="text/javascript">
	Joomla.orderTable = function()
	{
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;

		if (order != '<?php echo $listOrder; ?>')
		{
			dirn = 'asc';
		}
		else
		{
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>
	<form action="<?php echo JRoute::_('index.php?option=com_ccidealplatform&view=payments'); ?>" method="post" name="adminForm" id="adminForm">
		<?php if(!empty( $this->sidebar)): ?>
			<div id="j-sidebar-container" class="span2">
				<?php echo $this->sidebar; ?>
			</div>
			<div id="j-main-container" class="span10">
		<?php else : ?>
			<div id="j-main-container">
		<?php endif;?>

	<div class="chillcreations-bootstrap">
		<table>
			<tr>
				<td align="left" width="100%">

					<div id="filter-bar" class="btn-toolbar">
						<div class="filter-search btn-group pull-left">
							<input type="text" name="filter_search" id="filter_search" class="hasTooltip" placeholder="<?php echo JText::_('FILTER_ORDER_NUMBER'); ?>" value="<?php echo $this->state->get('filter.search'); ?>"/>
						</div>
						<div class="btn-group hidden-phone">
							<button class="btn hasTooltip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
							<button class="btn hasTooltip" type="button" onclick="document.getElementById('filter_search').value='';this.form.submit();" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>"><i class="icon-remove"></i></button>
						</div>
						
						<?php
						
						if($jversion_sht == 3){
						
						?>

							<div class="btn-group pull-right hidden-phone">
								<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
								<?php echo $this->pageNav->getLimitBox(); ?>
							</div>

							<div class="btn-group pull-right hidden-phone">
								<label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></label>
								<select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
									<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></option>
									<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING'); ?></option>
									<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING');  ?></option>
								</select>
							</div>

							<div class="btn-group pull-right">
								<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY'); ?></label>
								<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
									<option value=""><?php echo JText::_('JGLOBAL_SORT_BY');?></option>
									<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder); ?>
								</select>
							</div>
						<?php
						} ?>
					</div>			

				</td>

				<td nowrap="nowrap">

					<?php if($jversion_sht != 3){ ?>

						<select onchange="submitform();" size="1" class="inputbox" id="filter_state" name="filter_state">
							<?php echo JHtml::_('select.options', $this->statusfilter, 'value', 'text', $filter_state); ?>
						</select>
					<?php }	?>
				</td>
			</tr>
		</table>

			<table class="adminlist table table-striped" border="0">
				<thead>
					<tr>
						<th>
							<input type="checkbox" name="checkall-toggle" value="" onclick="Joomla.checkAll(this)" />
						</th>
						<th>
							<?php echo JHtml::_('grid.sort', JText::_( 'ID_EXTN' ), 's.extension', @$listDirn, @$listOrder ); ?>
						</th>
						<th width="14%">
							<?php echo JHtml::_('grid.sort', JText::_( 'ID_TRANS_ID' ), 's.trans_id', @$listDirn, @$listOrder ); ?>
						</th>
						<th>
							<?php echo JHTML::_('grid.sort', JText::_( 'ID_ORDER_NO' ), 's.order_id', @$listDirn, @$listOrder ); ?>
						</th>

						<th>
							<?php echo JHTML::_('grid.sort', JText::_( 'USERID' ), 's.user_id ', @$listDirn, @$listOrder ); ?>
						</th>

						<th>
							<?php echo JHTML::_('grid.sort', JText::_( 'ID_ORDER_DATE' ), 's.payment_date', @$listDirn, @$listOrder ); ?>
						</th>
						<th>
							<?php echo JHTML::_('grid.sort', JText::_( 'ID_TOTAL_AMOUNT' ), 's.payment_total', @$listDirn, @$listOrder ); ?>
						</th>
						<th width="10%">
							<?php echo JHTML::_('grid.sort', JTEXT::_('CCIDEALPLATFORM_EXTRA_INFORMATION') , 's.article_extra_text', @$listDirn, @$listOrder ); ?>

							<?php if($jversion_sht == 3){ ?>

								<span class="disabled">&nbsp;<img class="tip hasTooltip" data-placement="top" data-original-title="<?php echo JTEXT::_("CCIDEALPLATFORM_EXTRA_INFO_MSG"); ?>" src="<?php echo JURI::base(); ?>components/com_ccidealplatform/assets/information.png"></span>

							<?php }else{ ?>

								<a href="#" onMouseOver="ShowPicture('popinfo',1)" onMouseOut="ShowPicture('popinfo',0)">&nbsp;<img src="<?php echo JURI::base(); ?>components/com_ccidealplatform/assets/information.png"></a>
								<div id="popinfo"> <?php echo JTEXT::_("CCIDEALPLATFORM_EXTRA_INFO_MSG"); ?></div>

							<?php }	?>
						</th>

						<th width="20%">
							<?php echo JHTML::_('grid.sort', JText::_( 'ID_ORDER_STATUS' ), 's.payment_status', @$listDirn, @$listOrder ); ?>

							<?php if($jversion_sht == 3){ ?>

								<span class="disabled">&nbsp;<img class="tip hasTooltip" data-placement="top" data-original-title="<?php echo JTEXT::_("POP_MSG"); ?>" src="<?php echo JURI::base(); ?>components/com_ccidealplatform/assets/information.png"></span>

							<?php }else{ ?>

								<a href="#" onMouseOver="ShowPicture('pop',1)" onMouseOut="ShowPicture('pop',0)">&nbsp;<img src="<?php echo JURI::base(); ?>components/com_ccidealplatform/assets/information.png"></a>
								<div id="pop"> <?php echo JTEXT::_("POP_MSG"); ?></div>

							<?php }	?>
						</th>
					</tr>
				</thead>

				<tfoot>
					<tr>
						<td colspan="10">
							<?php echo $this->pageNav->getListFooter();	?>
						</td>
					</tr>
				</tfoot>

			<?php
				$k = 0;
				$p = $c = $x= '';
				$url = JURI::root();
				for ($i=0, $n=count( $this->items ); $i < $n; $i++){
					$row = &$this->items[$i];
					$checked = JHTML::_('grid.id', $i, $row->id );

				if($row->extension == 'caddy'){
					$extension = JTEXT::_('CCIDEALPLATFORM_SCADDY');
				}elseif($row->extension == 'hikashop'){
					$extension = JTEXT::_('CCIDEALPLATFORM_HIKASHOP');
				}elseif($row->extension == 'Content_plugin'){
					$extension = JTEXT::_('CCIDEALPLATFORM_CONTENTPLUGIN');
				}elseif($row->extension == 'rsformpro'){
					$extension = JTEXT::_('CCIDEALPLATFORM_RSFORMPRO');
				}elseif($row->extension == 'rsmembership'){
					$extension = JTEXT::_('CCIDEALPLATFORM_RSMEMBERSHIP');
				}elseif($row->extension == 'redshop'){
					$extension = JTEXT::_('CCIDEALPLATFORM_REDSHOP');
				}elseif($row->extension == 'akeebasubs'){
					$extension = JTEXT::_('CCIDEALPLATFORM_AKEEBASUBS');
				}elseif($row->extension == 'joomisp'){
					$extension = JTEXT::_('CCIDEALPLATFORM_JOOMISP');
				}elseif($row->extension == 'virtuemart'){
					$extension = JTEXT::_('CCIDEALPLATFORM_VIRTUEMART');
				}elseif($row->extension=='testform1' || $row->extension=='testform2' || $row->extension=='testform3' || $row->extension=='testform4' || $row->extension=='testform5' || $row->extension=='testform6' || $row->extension=='testform7'){
					$extension= JTEXT::_('CCIDEALPLATFORM_TESTINGFORM');					
				}elseif($row->extension=='rsepro'){
					$extension= JTEXT::_('CCIDEALPLATFORM_RSEVENTSPRO');
				}elseif($row->extension=='sobipro'){
					$extension= JTEXT::_('CCIDEALPLATFORM_SOBIPRO');
				}elseif($row->extension=='ticketmaster'){
					$extension= JTEXT::_('CCIDEALPLATFORM_TICKETMASTER');
				}elseif($row->extension=='ccinvoices'){
					$extension= JTEXT::_('CCIDEALPLATFORM_CCINVOICES');
				}elseif($row->extension=='rsdirectory'){
					$extension= JTEXT::_('CCIDEALPLATFORM_RSDIRECTORY');
				}elseif($row->extension=='j2store'){
					$extension= JTEXT::_('CCIDEALPLATFORM_J2STORE');
				}elseif($row->extension=='techjoomla'){
					$extension= JTEXT::_('CCIDEALPLATFORM_TECHJOOMLA');
				}elseif($row->extension=='testpayment'){
					$extension= JTEXT::_('CCIDEAL_TEST_PAYMENT');
				}else{
					$extension = JTEXT::_('CCIDEALPLATFORM_UNKNOWN');				
				}

				if($extension==JTEXT::_('CCIDEALPLATFORM_UNKNOWN') && $row->extension!=""){
					  $extension = $row->extension;

				}elseif($extension==JTEXT::_('CCIDEALPLATFORM_UNKNOWN') && $row->extension==""){

					  $extension=JTEXT::_('CCIDEALPLATFORM_UNKNOWN');
				}

			?>

				<tr>
					<td align="center">
						<?php echo $checked; ?>
					</td>

					<td align="center">
						<?php echo $extension; ?>
					</td>

					<td align="center">
						<?php echo $row->trans_id; ?>
					</td>

					<td align="center">
						<?php echo $row->extension_id; ?>
					</td>

					<td align="center">
						<?php if($row->user_id==0){
							echo JText::_('CCIDEALPLATFORM_GUEST');
						}else{
							echo $row->user_id;
						} ?>
					</td>

					<td align="center">
						<?php echo date($row->payment_date); ?>
					</td>

					<td align="center">
						<?php echo "&#8364;".number_format($row->payment_total,2);?>
					</td>

					<td align="center">
						<?php
							// David - 17 March 2015 - 4.3.6 - Removed "hover to view all extra information", and just show all information in the list
							echo $row->article_extra_text;
						 ?>

					</td>

					<td align="center">
						<div id="<?php echo $i;?>">
							<div id="statusdropdown<?php echo $i;?>" class="statusdropdown">
								<?php echo JHTML::_( 'select.genericlist', $this->statusdropdown, 'status'.$i, 'class="ccideal_statusbox input-small"' ,'value','text', $row->payment_status); ?>
							</div>

							<div id="updatebutton<?php echo $i;?>" style="display:block;" class="updatebutton">
								<input type="button" value="Update" name="Submit"
								onclick="if(document.adminForm.status<?php echo $i;?>.value=='<?php echo $row->payment_status ;?>' || document.adminForm.status<?php echo $i;?>.value == '') {
								alert('Please change the Order Status!');
								return false;
								}else{
								update(<?php echo $row->id;?>,'<?php echo $url;?>',<?php echo $i;?>,document.adminForm.status<?php echo $i;?>.value);
								} "
								 class="btn btn-small">
							</div> <?php
						
							
							if($row->payment_status == "pending"){
								if(($row->trans_id != "") && ($row->trans_id != "0")){
									if(($row->account == "ing") && ($this->configuration->IDEAL_Bank == "ING")){
										?>
										<div>
											<a class="btn btn-warning btn-small" href="javascript:void('0');" onclick="request_status('<?php echo $row->extension; ?>', '<?php echo $row->payment_total; ?>', '<?php echo $row->order_id; ?>', '<?php echo $row->trans_id; ?>','<?php echo JURI::root(); ?>');">Get status</a>
										</div><?php
									}elseif(($row->account == "raboprof") && ($this->configuration->IDEAL_Bank == "RABOPROF")){
										?>
										<div>
											<a class="btn btn-warning btn-small" href="javascript:void('0');" onclick="request_status('<?php echo $row->extension; ?>', '<?php echo $row->payment_total; ?>', '<?php echo $row->order_id; ?>', '<?php echo $row->trans_id; ?>','<?php echo JURI::root(); ?>');">Get status</a>
										</div><?php
									}
								}
							}
							?>
						</div>
					</td>
				</tr>

				<?php
						$k = 1 - $k;
					}
				?>
			</table>

			<input type="hidden" name="option" value="com_ccidealplatform" />
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="id" value="0" />
			<input type="hidden" name="oid" value="" />
			<input type="hidden" name="sv" value="" />
			<input type="hidden" name="offsetid" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<input type="hidden" name="controller" value="status" />
			<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
			<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />

	</form>

	<div class="well well-small pagination-centered" style="font-size: 10pt;">	
		

	
		<div class="row-fluid">
			<div class="span4">
				<?php echo $this->name; ?>&nbsp;<?php echo $this->version; ?> by <a href="http://www.chillcreations.com/" target="_blank">Chill Creations</a><br />
				Copyright &copy; 2006-<?php echo $curYear = date('Y'); ?> Chill Creations<br />
			</div>
			
			<div class="span4">
			   <p>Questions, suggestions or problems?</p>
				<a class="btn btn-small" href="http://www.chillcreations.com/manuals/ccideal-platform" target="_blank">Manual</a>&nbsp;&nbsp;&nbsp;
				<a class="btn btn-small btn-success" href="http://www.chillcreations.com/support" target="_blank">Support</a>&nbsp;&nbsp;&nbsp;  
				<a class="btn btn-small" href="http://www.chillcreations.com/joomla-extensions/ccideal-platform-ideal-for-joomla" target="_blank">Information</a>
			</div>
			  
			<div class="span4">
				<p>Please post a review at the: </p>
				<a class="btn btn-small btn-warning" href="http://extensions.joomla.org/extensions/e-commerce/payment-systems/13009" target="_blank">Joomla! Extensions Directory</a><br />
			</div>
		</div>
	</div>
</div>