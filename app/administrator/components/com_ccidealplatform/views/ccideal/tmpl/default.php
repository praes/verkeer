<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license. 
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

$test_id = base64_encode(date('dm'));

$jversion_sht = $this->jversion;

$db = JFactory::getDBO();

if($jversion_sht == 3){

	JHtml::_('behavior.tooltip');
	JHtml::_('behavior.formvalidation');
	JHtml::_('behavior.keepalive');
	JHtml::_('formbehavior.chosen', 'select');

	?>
	<script type="text/javascript">
		Joomla.submitbutton = function(task)
		{
			if (document.formvalidator.isValid(document.id('ccideal-form'))) {
				Joomla.submitform(task, document.getElementById('ccideal-form'));
			} else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
			}
		}
	</script>

	<?php
}
?>
	<form action="index.php?option=com_ccidealplatform" method="post" name="adminForm" enctype="multipart/form-data" id="ccideal-form">
		<?php if(!empty( $this->sidebar)): ?>
			<div id="j-sidebar-container" class="span2">
				<?php echo $this->sidebar; ?>
			</div>
			<div id="j-main-container" class="span10">
		<?php else : ?>
			<div id="j-main-container">
		<?php endif;?>

	<div class="chillcreations-bootstrap">

		<a name="config-top" class="config-top-anchor"></a>
		<div class="well">
			<ul>
				<li><a href="#general-settings"><?php echo JText::_( 'ID_GENERAL_SETTING' ); ?></a>
				<ul>
					<li><?php echo JText::_( 'COM_CCIDEAL_CCIDEAL_SETTINGS' ); ?></li>
					<li><?php echo JText::_( 'COM_CCIDEAL_ACCOUNT_SETTINGS' ); ?></li>
					<li><?php echo JText::_( 'CC_UPDATES' ); ?></a></li>
				</ul>
			</li>
			<li><a href="#communication-settings"><?php echo JText::_( 'COM_CCIDEAL_COMMUNICATION_SETTINGS' ); ?></a>
				<ul>
					<li><?php echo JText::_( 'ID_IDEAL_MSG_AFTER_PAYMENT' ); ?></li>
					<li><?php echo JText::_( 'CCIDEAL_EMAIL_SETTINGS' ); ?></li>
				</ul>
			</li>
			</ul>
		</div>

		<?php if($this->idealconfigarray->download_id == ""){ ?>
				<div class="alert alert-error">
					<a class="close" data-dismiss="alert" href="#"></a>
					<?php echo JText::_( 'CC_DOWNLOADID_WARNING' ); ?>
				</div>
		<?php } elseif (!preg_match('/^([0-9]{1,}:)?[0-9a-f]{32}$/i',$this->idealconfigarray->download_id)) { ?>
			<div class="alert alert-error">
				<a class="close" data-dismiss="alert" href="#"></a>
				<?php echo JText::_( 'CC_DOWNLOADID_INCORRECT' ); ?>
			</div>

		<?php } ?>

		<div class="row-fluid">

			<a name="general-settings" class="config-anchor"></a>
			<legend><?php echo JText::_( 'ID_GENERAL_SETTING' ); ?></legend>

			<div class="span6">

			<fieldset>
				<legend><?php echo JText::_( 'COM_CCIDEAL_CCIDEAL_SETTINGS' ); ?></legend>

				<table cellspacing="1">
					<tbody>
						<tr>
							<td class="cls_setting" >
								<span class="editlinktip hasTip" title="<?php echo JText::_( 'ID_ENABLE' ); ?>::<?php echo JText::_( 'ID_ENABLE_TIPS' ); ?>">
									<?php echo JText::_( 'ID_ENABLE' ); ?>
								</span>
							</td>
							<td class="cls_setting">
								<fieldset class="radio inputbox btn-group" id="ccidealplatform_radio">
									<input <?php if($this->config_iDEAL==-1){ echo "disabled"; }?> type="radio" name="IDEAL_Enable" value="1" id="IDEAL_Enable1"  <?php if(@$this->idealconfigarray->IDEAL_Enable == '1'){ echo "checked='checked'"; } ?> />
									<label for="IDEAL_Enable1" class="radiobtn">Yes</label>
									<input <?php if($this->config_iDEAL==-1){ echo "disabled"; }?>  type="radio" name="IDEAL_Enable" value="0" id="IDEAL_Enable0"  <?php if(@$this->idealconfigarray->IDEAL_Enable == '0'){echo "checked='checked'"; } ?> />
									<label for="IDEAL_Enable0" class="radiobtn">No</label>
									<input <?php if($this->config_iDEAL==-1){ echo "disabled"; }?>  type="radio" name="IDEAL_Enable" value="2" id="IDEAL_Enable2"  <?php if(@$this->idealconfigarray->IDEAL_Enable == '2'){echo "checked='checked'"; } ?> />
									<label for="IDEAL_Enable2" class="radiobtn">Administrator Only</label>
								</fieldset>
							</td>
						</tr>

						<tr>
							<td class="cls_setting">
								<span class="editlinktip hasTip" title="<?php echo JText::_( 'ID_IDEAL_DESCRIPTION' ); ?>::<?php echo JText::_( 'ID_IDEAL_DESCRIPTION_LENGTH' ); ?>">
									<?php echo JText::_( 'ID_IDEAL_DESCRIPTION' ); ?>
								</span>
							</td>
							<td class="cls_setting">
								<div>
									<?php
									if (@$this->idealconfigarray->IDEAL_Description) {
										$description = $this->idealconfigarray->IDEAL_Description;
									} else {
										$description = "iDEAL Betaling";
									}
									?>
									<input <?php if($this->config_iDEAL==-1){ echo "disabled"; }?> class="inputbox" type="text" name="IDEAL_Description"  size="50" maxlength="14" value="<?php echo $description; ?>" />
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</fieldset>


		<?php

		if ($this->idealconfigarray->IDEAL_Bank == 'Mollie') {

			$mollie_style = 'block';
			$bank_style = 'none';
			$abnamroeasy_style = 'none';
			$certificate_style = 'none';
			$target_style = 'none';
			$proversion_style='none';
			$sisow_style = 'none';

		}else if ($this->idealconfigarray->IDEAL_Bank == 'ABNAMROTEST') {

			$abnamroeasy_style = 'block';
			$bank_style = 'none';
			$mollie_style = 'none';
			$certificate_style = 'none';
			$target_style = 'none';
			$proversion_style='none';
			$sisow_style = 'none';

		}else if ($this->idealconfigarray->IDEAL_Bank == 'RABOBANKTEST') {

			$certificate_style = 'none';
			$abnamroeasy_style = 'none';
			$bank_style = 'block';
			$mollie_style = 'none';
			$target_style = 'none';
			$proversion_style='none';
			$sisow_style = 'none';

		}else if ($this->idealconfigarray->IDEAL_Bank == 'POSTBASIC') {

			$certificate_style = 'none';
			$abnamroeasy_style = 'none';
			$bank_style = 'block';
			$mollie_style = 'none';
			$target_style = 'none';
			$proversion_style='none';
			$sisow_style = 'none';
		}else if ($this->idealconfigarray->IDEAL_Bank == 'TARGETPAY') {

			$certificate_style = 'none';
			$abnamroeasy_style = 'none';
			$bank_style = 'none';
			$target_style = 'block';
			$mollie_style = 'none';
			$proversion_style='none';
			$sisow_style = 'none';
		}else if ($this->idealconfigarray->IDEAL_Bank == 'SISOW') {

			$mollie_style = 'none';
			$bank_style = 'none';
			$abnamroeasy_style = 'none';
			$certificate_style = 'none';
			$target_style = 'none';
			$proversion_style='none';
			$sisow_style = 'block';
		}else if ($this->idealconfigarray->IDEAL_Bank == 'RABOOMNIKASSA') {

			$mollie_style = 'none';
			$bank_style = 'block';
			$abnamroeasy_style = 'none';
			$certificate_style = 'none';
			$target_style = 'none';
			$proversion_style='none';
			$sisow_style = 'none';
		}
		 else {

			$certificate_style = 'block';
			$bank_style = 'block';
			$mollie_style = 'none';
			$sisow_style = 'none';
			$abnamroeasy_style = 'none';
			$target_style = 'none';
			$proversion_style='block';

		}

		//validate the iDEAL v3
		if ($this->idealconfigarray->IDEAL_Bank == 'ING' || $this->idealconfigarray->IDEAL_Bank == 'RABOPROF') {
			if($this->idealconfigarray->iDEAL_ProVersion){
					$displayv3="block";
					$displayv2="none";
			}else{
					$displayv2="block";
					$displayv3="none";
			}

		}else{
					$displayv2="block";
					$displayv3="none";
		}

		?>


		<fieldset id="ccidealplatform_radio">
			<legend><?php echo JText::_( 'COM_CCIDEAL_ACCOUNT_SETTINGS' ); ?></legend>

			<!-- Select iDEAL Bank Name -->

			<table cellspacing="1">
				<tbody>
					<tr>
						<td class="cls_setting">
							<span class="editlinktip hasTip" title="<?php echo JText::_( 'ID_BANK' ); ?>::<?php echo JText::_( 'ID_BANK_TIPS' ); ?>">
								<?php echo JText::_( 'ID_BANK' ); ?>
							</span>
						</td>

						<td class="cls_setting_select">
							<?php
								if($this->config_iDEAL==-1){
									$disables= "disabled";
								}else{
									$disables="";
								}
								echo JHTML:: _('select.genericlist', $this->banklist, 'IDEAL_Bank', 'onchange="selectbanks();" '.$disables.'', 'value', 'text', $this->idealconfigarray->IDEAL_Bank);
							?>
							<div class="test_payment">
								<a href="<?php echo JURI::root(); ?>index.php?option=com_ccidealplatform&task=bankform&id=<?php echo $test_id; ?>" target="_blank" class="btn btn-primary btn-small" > <?php echo JText::_('CCIDEAL_TEST_PAYMENT'); ?></a>
							</div>
						</td>
					</tr>

					<!--<tr>
						<td class="cls_setting" >
							<span id="method_label" class="editlinktip hasTip" title="<?php /*echo JText::_( 'ID_BANK' );*/ ?>::<?php /*echo JText::_( 'ID_BANK_TIPS' );*/ ?>">
								<?php /*echo JText::_( 'ID_BANK' );*/ ?>
							</span>
						</td>

						<td class="cls_setting_select">
							<?php
								/*$default_val = "";
								if($this->idealconfigarray->IDEAL_Bank == "TARGETPAY"){
									$tar_enable = "style='display:block'";
									$default_val = $this->idealconfigarray->ideal_paymentmethod;
								}else{
									$tar_enable = "style='display:none'";
								}
								if($this->idealconfigarray->IDEAL_Bank == "Mollie"){
									$mollie_enable = "style='display:block'";
									$default_val = $this->idealconfigarray->ideal_paymentmethod;
								}
								else{
									$mollie_enable = "style='display:none'";
								}
								if($this->idealconfigarray->IDEAL_Bank == "RABOONMIKASSA"){
									$romni_enable = "style='display:block'";
									$default_val = $this->idealconfigarray->ideal_paymentmethod;
								}
								else{
									$romni_enable = "style='display:none'";
								}
								if($this->idealconfigarray->IDEAL_Bank == "SISOW"){
									$sisow_enable = "style='display:block'";
									$default_val = $this->idealconfigarray->ideal_paymentmethod;
								}else{
									$sisow_enable = "style='display:none'";
								}

								echo JHTML:: _('select.genericlist', $this->targetpaylist, 'targetpay_method', $tar_enable, 'value', 'text', $default_val);
								echo JHTML:: _('select.genericlist', $this->mollielist, 'mollie_method', $mollie_enable, 'value', 'text', $default_val);
								echo JHTML:: _('select.genericlist', $this->raboomnilist, 'raboomni_method', $romni_enable, 'value', 'text', $default_val);
								echo JHTML:: _('select.genericlist', $this->sisowlist, 'sisow_method', $sisow_enable, 'value', 'text', $default_val);*/

							?>
						</td>
					</tr>-->
				</tbody>
			</table>

			<!-- Select iDEAL Bank Version -->

			<div id="iDEALProVersion" style="display:<?php echo $proversion_style; ?>"  <?php echo $disables; ?>>
				<table  cellspacing="1" border="0" id="ccidealplatform_img">
					<tbody>
						<tr>
							<td class="cls_setting">
								<div id="iDEALV3_PHPVersion" style="display:none;" class="red_message_box">
									<input type="hidden" name="iDEAL_ProVersion" value="1" />
									<span style="float:left">
										<img src="<?php echo JURI::root(); ?>administrator/components/com_ccidealplatform/assets/cross.png" width="16" height="16" class="ccmargin_RedboxFloat" align="top" />
									</span>
									<span class="ccidealplatform_version_redmessage">
										<?php echo JTEXT::_('CCIDEALPLATFORM_PHPVERSION_MSG'); ?>
									</span>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<!-- Select iDEAL Bank Status -->

			<table cellspacing="1" border="0" id="ccidealplatform_img">
				<tbody>
					<tr>
						<td class="cls_setting">
							<span class="editlinktip hasTip" title="<?php echo JText::_( 'ID_IDEAL_STATUS' ); ?>::<?php echo JText::_( 'ID_IDEAL_STATUS_TIPS' ); ?>">
								<?php echo JText::_( 'ID_IDEAL_STATUS' ); ?>
							</span>
						</td>

						<td class="cls_setting">
							 <fieldset class="radio inputbox btn-group" id="ccidealplatform_radio">
								<input type="radio" name="IDEAL_Mode" value="TEST" id="IDEAL_Mode1"  <?php if(@$this->idealconfigarray->IDEAL_Mode == 'TEST'){ echo "checked='checked'"; } ?> />
								<label for="IDEAL_Mode1" class="radiobtn"><?php echo JText::_( 'ID_TEST' ); ?></label>
								<input type="radio" name="IDEAL_Mode" value="PROD" id="IDEAL_Mode0"  <?php if(@$this->idealconfigarray->IDEAL_Mode == 'PROD'){echo "checked='checked'"; } ?> />
								<label for="IDEAL_Mode0" class="radiobtn"><?php echo JText::_( 'ID_PRODUCTION' ); ?></label>
							</fieldset>
						</td>
					</tr>
				</tbody>
			</table>


			<!-- ING ADVANCE OR RABO PROF bank -->

			<div id="banks" style="display:<?php echo $bank_style; ?>"  <?php echo $disables; ?>>
				<table cellspacing="1" border="0" id="ccidealplatform_img">
					<tbody>
                    <tr>
                        <td colspan="2">
                            <br />
                            <div class="well">
                                <?php echo JText::_( 'ID_TIP_MERCHANTID' ); ?>

                                <br /><br />
                                <a href="http://www.chillcreations.com/manuals/ccideal-platform/" target="_blank" class="btn btn-small">
                                    <?php echo JText::_( 'CCIDEAL_VIEW_MANUAL' ); ?> <i class="icon icon-chevron-right"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
						<tr>
							<td class="key cls_setting">
								<span class="editlinktip hasTip" title="<?php echo JText::_( 'ID_MERCHANTID' ); ?>::<?php echo JText::_( 'ID_TIP_MERCHANTID' ); ?>">
									<?php echo JText::_( 'ID_MERCHANTID' ); ?>
								</span>
							</td>
							<td class="controls cls_setting">
								<input  <?php echo $disables; ?> class="text_area" type="text" name="IDEAL_MerchantID" size="50" value="<?php echo $this->idealconfigarray->IDEAL_MerchantID; ?>" />
							</td>
						</tr>

						<tr>
							<td class="key cls_setting">
								<span class="editlinktip hasTip" title="<?php echo JText::_( 'ID_SUBID' ); ?>::<?php echo JText::_( 'ID_SUBID_TIPS' ); ?>">
									<?php echo JText::_( 'ID_SUBID' ); ?>
								</span>
							</td>
							<td class="controls cls_setting">
								<input  <?php echo $disables; ?> class="text_area" type="text" name="IDEAL_SubID" size="50" value="<?php echo $this->idealconfigarray->IDEAL_SubID; ?>" />
							</td>
						</tr>

						<tr>
							<td class="key cls_setting">
								<span class="editlinktip hasTip" title="<?php echo JText::_( 'ID_PASSWORD' ); ?>::<?php echo JText::_( 'ID_PASSWORD_TIPS' ); ?>">
									<?php echo JText::_( 'ID_PASSWORD' ); ?>
								</span>
							</td>
							<td id="PrivatekeyPass"  style="display:<?php echo $displayv2;?>;" class="cls_setting">
								<input  <?php echo $disables; ?> class="text_area" type="text" name="IDEAL_PrivatekeyPass" size="50" value="<?php echo $this->idealconfigarray->IDEAL_PrivatekeyPass; ?>" />
							</td>

							<td id="PrivatekeyPass_v3" style="display:<?php echo $displayv3;?>;" class="cls_setting">
								<input  <?php echo $disables; ?> class="text_area" type="text" name="IDEAL_PrivatekeyPass_v3" size="50" value="<?php echo $this->idealconfigarray->IDEAL_PrivatekeyPass_v3; ?>" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<table id="certificate" style="display:<?php echo $certificate_style?>" cellspacing="1" border="0" class="well">
				<tr>
					<td colspan="2">
						<legend id="cert_title" style="display:<?php echo $certificate_style?>">Certificates</legend>
					</td>
				</tr>

				<tr>
					<td colspan="2">
						<?php
							if ((JFile::exists($this->certfilepath)&& JFile::exists($this->keyfilepath))){
						?>
								<div class="alert">
								 	<button type="button" class="close" data-dismiss="alert">&times;</button>
									<?php echo JTEXT::_("CCIDEALPLATFOARM_ALL_CERTS"); ?>

									<?php $task_cert = "ccideal.zipFilesAndDownload"; ?>

									<a href="index.php?option=com_ccidealplatform&task=<?php echo $task_cert; ?>">
										<?php echo JTEXT::_('CCIDEALPLATFORM_DWNLD_CERT_ZIP');?>
									</a>

								</div>
								<?php

							}elseif ((!JFile::exists($this->certfilepath)&& !JFile::exists($this->keyfilepath))){ ?>

								<div class="alert">
								  <button type="button" class="close" data-dismiss="alert">&times;</button>
								  <?php echo JTEXT::_("ID_BOTH_CERT_MISSING"); ?>
								</div> <?php

							}elseif(!JFile::exists($this->certfilepath)){ ?>

								<div class="alert">
								  <button type="button" class="close" data-dismiss="alert">&times;</button>
								  <?php echo JTEXT::_("ID_ONLY_PRIV"); ?>
								</div> <?php

							}elseif(!JFile::exists($this->keyfilepath)){ ?>

								<div class="alert">
								  <button type="button" class="close" data-dismiss="alert">&times;</button>
								  <?php echo JTEXT::_("ID_ONLY_CERT"); ?>
								</div> <?php
							} ?>
					</td>
				</tr>

				<tr>
					<td class="cls_setting">
						<?php echo JTEXT::_('CCIDEALPLATFORM_CERT'); ?>
					</td>
					<td class="cert_widt">
						<input <?php echo $disables; ?> class="text_area" type="file" name="IDEAL_Privatecert" size="50" />&nbsp;
					</td>
				</tr>
				<tr>
					<td class="cls_setting">
						<?php echo JTEXT::_('CCIDEALPLATFORM_PERM'); ?>
					</td>
					<td class="cert_widt">
						<input <?php echo $disables; ?> class="text_area" type="file" name="IDEAL_Privatekey" size="50" />&nbsp;
					</td>
				</tr>
			</table>

			<!-- Mollie bank -->

			<div id="mollie" style="display:<?php echo $mollie_style; ?>">
				<table cellspacing="1" border="0">
					<tbody>
                    <tr>
                        <td colspan="2">
                            <br />
                            <div class="well">
                                <?php echo JText::_( 'ID_TIP_MERCHANTID' ); ?>
                                <br /><br />
                                <a href="http://www.chillcreations.com/manuals/ccideal-platform/" target="_blank" class="btn btn-small">
                                    <?php echo JText::_( 'CCIDEAL_VIEW_MANUAL' ); ?> <i class="icon icon-chevron-right"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
						<tr>
							<td class="key cls_setting">
								<span class="editlinktip hasTip" title="<?php echo JText::_( 'ID_MERCHANTID' ); ?>::<?php echo JText::_( 'ID_TIP_MERCHANTID' ); ?>">
									<?php echo JText::_( 'ID_MERCHANTID' ); ?>
								</span>
							</td>
							<td class="cls_setting">
								<input <?php echo $disables; ?> class="text_area" type="text" name="IDEAL_PartnerID" size="50" value="<?php echo $this->idealconfigarray->IDEAL_PartnerID; ?>" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<!-- TargetPay bank -->
			<div id="targetpay" style="display:<?php echo $target_style; ?>">
				<table cellspacing="1" border="0">
					<tbody>
                    <tr>
                        <td colspan="2">
                            <br />
                            <div class="well">
                                <?php echo JText::_( 'ID_TIP_MERCHANTID' ); ?>
                                <br /><br />
                                <a href="http://www.chillcreations.com/manuals/ccideal-platform/" target="_blank" class="btn btn-small">
                                    <?php echo JText::_( 'CCIDEAL_VIEW_MANUAL' ); ?> <i class="icon icon-chevron-right"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
						<tr>
							<td class="key cls_setting">
								<span class="editlinktip hasTip" title="<?php echo JText::_( 'ID_MERCHANTID' ); ?>::<?php echo JText::_( 'ID_TIP_MERCHANTID' ); ?>">
									<?php echo JText::_( 'ID_MERCHANTID' ); ?>
								</span>
							</td>
							<td class="cls_setting">
								<input <?php echo $disables; ?> class="text_area" type="text" name="IDEAL_Targetpay_ID" size="50" value="<?php echo $this->idealconfigarray->IDEAL_Targetpay_ID; ?>" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<!-- ABN AMRO bank -->

			<div id="abnamroeasy" style="display:<?php echo $abnamroeasy_style; ?>">
				<table cellspacing="1" border="0">
					<tbody>
                    <tr>
                        <td colspan="2">
                            <br />
                            <div class="well">
                                <?php echo JText::_( 'ID_TIP_MERCHANTID' ); ?>
                                <br /><br />
                                <a href="http://www.chillcreations.com/manuals/ccideal-platform/" target="_blank" class="btn btn-small">
                                    <?php echo JText::_( 'CCIDEAL_VIEW_MANUAL' ); ?> <i class="icon icon-chevron-right"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
						<tr>
							<td class="key cls_setting">
								<span class="editlinktip hasTip" title="<?php echo JText::_( 'PSPID' ); ?>::<?php echo JText::_( 'ID_TIP_PSPID' ); ?>">
									<?php echo JText::_( 'PSPID' ); ?>
								</span>
							</td>
							<td class="cls_setting">
								<input <?php echo $disables; ?> class="text_area" type="text" name="IDEAL_ABNAMROEASY_ID" size="50" value="<?php echo @$this->idealconfigarray->IDEAL_ABNAMROEASY_ID; ?>" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<!-- SISOW bank -->

			<div id="sisow" style="display:<?php echo $sisow_style; ?>">
				<table cellspacing="1" border="0">
					<tbody>
                    <tr>
                        <td colspan="2">
                            <br />
                            <div class="well">
                                <?php echo JText::_( 'ID_TIP_MERCHANTID' ); ?>
                                <br /><br />
                                <a href="http://www.chillcreations.com/manuals/ccideal-platform/" target="_blank" class="btn btn-small">
                                    <?php echo JText::_( 'CCIDEAL_VIEW_MANUAL' ); ?> <i class="icon icon-chevron-right"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
						<tr>
							<td class="key cls_setting">
								<span class="editlinktip hasTip" title="<?php echo JText::_( 'ID_MERCHANTID' ); ?>::<?php echo JText::_( 'ID_TIP_MERCHANTID' ); ?>">
									<?php echo JText::_( 'ID_MERCHANTID' ); ?>
								</span>
							</td>
							<td class="cls_setting">
								<input <?php echo $disables; ?> class="text_area" type="text" name="sisow_merchantid" size="50" value="<?php echo $this->idealconfigarray->sisow_merchantid; ?>" />
							</td>
						</tr>
						<tr>
							<td class="key cls_setting">
								<span class="editlinktip hasTip" title="<?php echo JText::_( 'SISOW_MERCHANTKEY' ); ?>::<?php echo JText::_( 'ID_KEY_TIPS' ); ?>">
									<?php echo JText::_( 'SISOW_MERCHANTKEY' ); ?>
								</span>
							</td>
							<td class="cls_setting">
								<input <?php echo $disables; ?> class="text_area" type="text" name="sisow_merchantkey" size="50" value="<?php echo $this->idealconfigarray->sisow_merchantkey; ?>" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</fieldset>



			</div>


			<div class="span1">
				</div>

		<div class="span5">
			<fieldset>
				<legend><?php echo JText::_( 'CC_UPDATES' ); ?></legend>

				<div class="well">
					<?php  echo JText::_( 'CC_DOWNLOADID_HELP' ); ?>
					<br /><br />
					<a href="http://www.chillcreations.com/orders/subscriptions" target="_blank" class="btn btn-small">
						<?php  echo JText::_( 'CC_GET_DOWNLOAD_ID' ); ?> <i class="icon icon-chevron-right"></i></a>
				</div>

				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="key cls_setting">
							<?php echo JText::_( 'CC_DOWNLOAD_ID' ); ?>
						</td>
						<td class="dwn_setting">
							<input class="inputbox" name="download_id" type="text" value="<?php echo $this->idealconfigarray->download_id; ?>" />
						</td>
					</tr>
				</table><br/>

			</fieldset>
			</div>
			</div>

		<br />
		<a href="#config-top" class="btn btn-block"><i class="icon-chevron-up"></i> <?php echo JText::_( 'COM_CCIDEAL_CONFIGURATION_BACK_TO_TOP' ); ?> <i class="icon-chevron-up"></i> </a>
		<br />
		<br />
		<br />

		<div class="row-fluid">

			<a name="communication-settings" class="config-anchor"></a>
			<legend><?php echo JText::_( 'COM_CCIDEAL_COMMUNICATION_SETTINGS' ); ?></legend>

			<div class="span6">


				<fieldset>
					<legend><?php echo JText::_( 'ID_IDEAL_MSG_AFTER_PAYMENT' ); ?></legend>

					<div class="well">
						<?php echo JText::_( 'BACKEND_EXPLANATION' ); ?>
					</div>


					<?php
						// Check if Virtuemart is installed on the site
						$db = JFactory::getDbo();
						$db->setQuery("SELECT enabled FROM #__extensions WHERE element = 'com_virtuemart'");
						$virtuemart_installed_enabled = $db->loadResult();

						if ($virtuemart_installed_enabled == 1)
						{
							?>
							<div class="alert alert-error">
								<?php echo JText::_( 'CCIDEAL_MESSAGE_AFTER_PAYMENT_VM_EXCEPTION' ); ?>
							</div>
							<?php
						}
					?>

					<table cellspacing="1" border="0" class="well" style="display:block;">
						<tbody>
						<tr>
							<td colspan="2">
								<legend><?php echo JText::_( 'ID_STATUS_THANKYOU' ); ?></legend>
							</td>
						</tr>

						<tr>
							<td class="key cls_setting">

							<span class="shorttext">
								<?php echo JText::_( 'ID_SHORT_TEXT' ); ?>
								<input <?php echo $disables; ?>  type="radio" class="article_class" name="IDEAL_pay_sucs_value" value="0" <?php $var=$this->idealconfigarray->IDEAL_pay_sucs_value;if($var==0){echo "checked"; } ?> >
							</span>
							</td>
							<td class="cls_setting">

								<?php
									if ($this->idealconfigarray->IDEAL_Status_ThankYou) {
										$Status_ThankYou = $this->idealconfigarray->IDEAL_Status_ThankYou;
									} else {
										$Status_ThankYou = JText :: _('ID_STATUS_ONSUCCESS');
									}
								?>

								<textarea <?php echo $disables; ?>  class="text_area" name="IDEAL_Status_ThankYou" cols="40" rows="2" ><?php echo $Status_ThankYou; ?></textarea>
							</td>
						</tr>

						<tr>
							<td class="key cls_setting">
							<span class="editlinktip hasTip payresult" title="<?php echo JText::_( 'ID_STATUS_THANKYOU' ); ?>::<?php echo JText::_( 'ID_STATUS_THANKYOU_TIPS' ); ?>">
							</span>
							<span class="shorttext">
								<?php echo JText::_( 'ID_ARTICLE' ); ?>
								<input <?php echo $disables; ?>  type="radio" class="article_class" name="IDEAL_pay_sucs_value" value="1" <?php $var=$this->idealconfigarray->IDEAL_pay_sucs_value;if($var==1){echo "checked"; } ?> >
							</span>
							</td>

							<td class="cls_setting_select">
								<?php

									$doc 		=  JFactory::getDocument();

									$article_id1=$this->idealconfigarray->IDEAL_pay_sucs_articleid;

									JHTML::_('behavior.modal', 'a.modal1');
									// Build the script.
									$script1 = array();
									$script1[] = '	function jSelectArticle_1(id, title, catid, object) {';
									$script1[] = '		document.id("1_id").value = id;';
									$script1[] = '		document.id("1_name").value = title;';
									$script1[] = '		SqueezeBox.close();';
									$script1[] = '	}';

									// Setup variables for display.
									$html1	= array();

									// Add the script to the document head.
									JFactory::getDocument()->addScriptDeclaration(implode("\n", $script1));


									// Setup variables for display.
									$html1	= array();
									$link1	= 'index.php?option=com_content&amp;view=articles&amp;layout=modal&amp;tmpl=component&amp;function=jSelectArticle_1';

									$db	= JFactory::getDBO();
									$db->setQuery('SELECT title FROM #__content WHERE id = '.(int) $article_id1);
									$title1 = $db->loadResult();

									if ($error = $db->getErrorMsg()) {
										JError::raiseWarning(500, $error);
									}

									if (empty($title1)) {
										$title1 = JText::_('COM_CONTENT_SELECT_AN_ARTICLE');
									}
									$title1 = htmlspecialchars($title1, ENT_QUOTES, 'UTF-8');

									if($jversion_sht == 2){

										// The current user display field.
										$html1[] = '<div class="fltlft">';
										$html1[] = '  <input type="text" id="1_name" value="'.$title1.'" disabled="disabled" size="35" />';
										$html1[] = '</div>';

										// The user select button.
										$html1[] = '<div class="button2-left">';
										$html1[] = '  <div class="blank">';
										$html1[] = '	<a class="modal1" title="'.JText::_('SELECT_ARTICLE').'"  href="'.$link1.'" rel="{handler: \'iframe\', size: {x: 800, y: 450}}">'.JText::_('SELECT_ARTICLE').'</a>';
										$html1[] = '  </div>';
										$html1[] = '</div>';

									}
									if($jversion_sht == 3){
										// The current user display field.
										$html1[] = '<span class="input-append">';
										$html1[] = '  <input type="text" id="1_name" value="'.$title1.'" disabled="disabled" size="35" />';

										$html1[] = '<a class="modal1 btn" title="'.JText::_('SELECT_ARTICLE').'"  href="'.$link1.'" rel="{handler: \'iframe\', size: {x: 800, y: 450}}">';
										$html1[] = '<i class="icon-file"></i>'. JText::_('SELECT_ARTICLE').'</a> ';
										$html1[] = '</span>';
									}

									// The active article id field.
									if (0 == (int)@$this->value) {
										$value1 = '';
									} else {
										$value1 = (int)$this->value;
									}

									// class='required' for client side validation
									$class1 = '';
									if (@$this->required) {
										$class1 = ' class="required modal-value"';
									}

									$html1[] = '<input type="hidden" id="1_id"'.$class1.' name="IDEAL_pay_sucs_articleid" value="'.(int)$article_id1.'" />';

									echo implode("\n", $html1);

								?>
							</td>
						</tr>

					</table>


					<table  cellspacing="1" border="0"  class="well" style="display:block;">
						<tr>
							<td colspan="2">
								<legend><?php echo JText::_( 'ID_STATUS_FAILED' ); ?></legend>
							</td>
						</tr>
						<tr>
							<td class="key cls_setting" class="">
								<?php JHTML::_('behavior.modal', 'a.modal'); ?>
								<span class="shorttext">
								<?php echo JText::_( 'ID_SHORT_TEXT' ); ?>
									<input <?php echo $disables; ?>  type="radio" class="article_class" name="IDEAL_pay_failed_value" value="0" <?php $var=$this->idealconfigarray->IDEAL_pay_failed_value ;if($var==0){echo "checked"; } ?> />
							</span>
							</td>

							<td class="cls_setting">
								<?php
									if ($this->idealconfigarray->IDEAL_Status_Failed) {
										$Status_Failed = $this->idealconfigarray->IDEAL_Status_Failed;
									} else {
										$Status_Failed = JText :: _('ID_STATUS_FAILURE');
									}
								?>
								<textarea <?php echo $disables; ?>  class="text_area" name="IDEAL_Status_Failed" cols="40" rows="2" ><?php echo $Status_Failed; ?></textarea>
							</td>
						</tr>

						<tr>
							<td class="key cls_setting">
							<span class="shorttext">
								<?php echo JText::_( 'ID_ARTICLE' ); ?>
								<input <?php echo $disables; ?>  type="radio" class="article_class" name="IDEAL_pay_failed_value" value="1" <?php $var=$this->idealconfigarray->IDEAL_pay_failed_value ;if($var==1){echo "checked"; } ?>>
							</span>
							</td>

							<td class="cls_setting_select">
								<?php

									$article_id2=$this->idealconfigarray->IDEAL_pay_fail_articleid;

									JHTML::_('behavior.modal', 'a.modal2');
									// Build the script.
									$script2 = array();
									$script2[] = '	function jSelectArticle_2(id, title, catid, object) {';
									$script2[] = '		document.id("2_id").value = id;';
									$script2[] = '		document.id("2_name").value = title;';
									$script2[] = '		SqueezeBox.close();';
									$script2[] = '	}';

									// Add the script to the document head.
									JFactory::getDocument()->addScriptDeclaration(implode("\n", $script2));

									// Setup variables for display.
									$html2	= array();
									$link2	= 'index.php?option=com_content&amp;view=articles&amp;layout=modal&amp;tmpl=component&amp;function=jSelectArticle_2';

									$db	= JFactory::getDBO();
									$db->setQuery('SELECT title FROM #__content WHERE id = '.(int) $article_id2	);
									$title2 = $db->loadResult();

									if ($error = $db->getErrorMsg()) {
										JError::raiseWarning(500, $error);
									}

									if (empty($title2)) {
										$title2 = JText::_('COM_CONTENT_SELECT_AN_ARTICLE');
									}

									$title2 = htmlspecialchars($title2, ENT_QUOTES, 'UTF-8');

									if($jversion_sht == 2){

										// The current user display field.
										$html2[] = '<div class="fltlft">';
										$html2[] = '  <input type="text" id="2_name" value="'.$title2.'" disabled="disabled" size="35" />';
										$html2[] = '</div>';

										// The user select button.
										$html2[] = '<div class="button2-left">';
										$html2[] = '  <div class="blank">';
										$html2[] = '	<a class="modal2" title="'.JText::_('SELECT_ARTICLE').'"  href="'.$link2.'" rel="{handler: \'iframe\', size: {x: 800, y: 450}}">'.JText::_('SELECT_ARTICLE').'</a>';
										$html2[] = '  </div>';
										$html2[] = '</div>';

									}
									if($jversion_sht == 3){

										// The current user display field.
										$html2[] = '<span class="input-append">';
										$html2[] = '  <input type="text" id="2_name" value="'.$title2.'" disabled="disabled" size="35" />';

										$html2[] = '	<a class="modal2 btn" title="'.JText::_('SELECT_ARTICLE').'"  href="'.$link2.'" rel="{handler: \'iframe\', size: {x: 800, y: 450}}">';
										$html2[] = '<i class="icon-file"></i>'. JText::_('SELECT_ARTICLE').'</a> ';
										$html2[] = '</span>';

									}

									// The active article id field.
									if (0 == (int)@$this->value) {
										$value2 = '';
									} else {
										$value2 = (int)$this->value;
									}

									// class='required' for client side validation
									$class2 = '';
									if (@$this->required) {
										$class2 = ' class="required modal-value"';
									}

									$html2[] = '<input type="hidden" id="2_id"'.$class2.' name="IDEAL_pay_fail_articleid" value="'.(int)$article_id2.'" />';

									echo implode("\n", $html2);

								?>

							</td>
						</tr>

					</table>

					<table  cellspacing="1" border="0"  class="well" style="display:block;">

						<tr>
							<td colspan="2">
								<legend><?php echo JText::_( 'ID_STATUS_CANCELLED' ); ?></legend>
							</td>
						</tr>

						<tr>
							<td class="key cls_setting">
							<span class="shorttext">
								<?php echo JText::_( 'ID_SHORT_TEXT' ); ?>
								<input <?php echo $disables; ?>  type="radio" class="article_class" name="IDEAL_pay_cancel_value" value="0" <?php $var=$this->idealconfigarray->IDEAL_pay_cancel_value ;if($var==0){echo "checked"; } ?>>
							</span>
							</td>

							<td class="cls_setting">
								<?php

									if ($this->idealconfigarray->IDEAL_Status_Cancelled) {
										$Status_Cancelled = $this->idealconfigarray->IDEAL_Status_Cancelled;
									} else {
										$Status_Cancelled = JText :: _('ID_STATUS_CANCELLATION');
									}
								?>
								<textarea <?php echo $disables; ?>  class="text_area" name="IDEAL_Status_Cancelled" cols="40" rows="2" ><?php echo $Status_Cancelled; ?></textarea>
							</td>
						</tr>

						<tr>
							<td class="key cls_setting">
							<span class="shorttext">
								<?php echo JText::_( 'ID_ARTICLE' ); ?>
								<input <?php echo $disables; ?>  type="radio" class="article_class" name="IDEAL_pay_cancel_value" value="1" <?php $var=$this->idealconfigarray->IDEAL_pay_cancel_value ;if($var==1){echo "checked"; } ?>>
							</span>
							</td>

							<td class="cls_setting_select">
								<?php

									$article_id3=$this->idealconfigarray->IDEAL_pay_cancel_articleid ;
									$doc1 = JFactory::getDocument();

									// Build the script.
									$script = array();
									$script[] = '	function jSelectArticle_3(id, title, catid, object) {';
									$script[] = '		document.id("3_id").value = id;';
									$script[] = '		document.id("3_name").value = title;';
									$script[] = '		SqueezeBox.close();';
									$script[] = '	}';

									// Add the script to the document head.
									JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));

									// Setup variables for display.
									$html	= array();
									// Add the script to the document head.
									JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));

									// Setup variables for display.
									$html	= array();
									$link	= 'index.php?option=com_content&amp;view=articles&amp;layout=modal&amp;tmpl=component&amp;function=jSelectArticle_3';

									$db	= JFactory::getDBO();
									$db->setQuery('SELECT title FROM #__content WHERE id = '.(int) $article_id3	);
									$title = $db->loadResult();

									if ($error = $db->getErrorMsg()) {
										JError::raiseWarning(500, $error);
									}

									if (empty($title)) {
										$title = JText::_('COM_CONTENT_SELECT_AN_ARTICLE');
									}
									$title = htmlspecialchars($title, ENT_QUOTES, 'UTF-8');

									if($jversion_sht == 2){

										// The current user display field.
										$html[] = '<div class="fltlft">';
										$html[] = '  <input type="text" id="3_name" value="'.$title.'" disabled="disabled" size="35" />';
										$html[] = '</div>';

										// The user select button.
										$html[] = '<div class="button2-left">';
										$html[] = '  <div class="blank">';
										$html[] = '	<a class="modal" title="'.JText::_('SELECT_ARTICLE').'"  href="'.$link.'" rel="{handler: \'iframe\', size: {x: 800, y: 450}}">'.JText::_('SELECT_ARTICLE').'</a>';
										$html[] = '  </div>';
										$html[] = '</div>';

									}
									if($jversion_sht == 3){

										// The current user display field.
										$html[] = '<span class="input-append">';
										$html[] = '  <input type="text" id="3_name" value="'.$title.'" disabled="disabled" size="35" />';

										// The user select button.
										$html[] = '	<a class="modal btn" title="'.JText::_('SELECT_ARTICLE').'"  href="'.$link.'" rel="{handler: \'iframe\', size: {x: 800, y: 450}}">';
										$html[] = '<i class="icon-file"></i>'. JText::_('SELECT_ARTICLE').'</a> ';
										$html[] = '</span>';

									}

									// The active article id field.
									if (0 == (int)@$this->value) {
										$value3 = '';
									} else {
										$value3 = (int)$this->value;
									}

									// class='required' for client side validation
									$class = '';
									if (@$this->required) {
										$class = ' class="required modal-value"';
									}

									$html[] = '<input type="hidden" id="3_id"'.$class.' name="IDEAL_pay_cancel_articleid" value="'.(int)$article_id3.'" />';

									echo implode("\n", $html);
								?>

							</td>
						</tr>
					</table>

					<table cellspacing="1" border="0"  class="well" style="display:block;">

						<tr>
							<td colspan="2">
								<legend><?php echo JText::_( 'CCIDEALPLATFORM_STATUS_PENDING' ); ?></legend>
							</td>
						</tr>

						<tr>
							<td class="key cls_setting">
								<span class="shorttext">
									<?php echo JText::_( 'ID_SHORT_TEXT' ); ?>
									<input <?php echo $disables; ?>  type="radio" class="article_class" name="IDEAL_pay_pend_value" value="0" <?php $var=$this->idealconfigarray->IDEAL_pay_pend_value;if($var==0){echo "checked"; } ?> >
								</span>
							</td>
							<td class="cls_setting">
								<?php
									if ($this->idealconfigarray->IDEAL_Status_pending) {
										$Status_ThankYou = $this->idealconfigarray->IDEAL_Status_pending;
									} else {
										$Status_ThankYou = JText :: _('ID_STATUS_ONSUCCESS');
									}
								?>
								<textarea <?php echo $disables; ?>  class="text_area" name="IDEAL_Status_pending" cols="40" rows="2" ><?php echo $Status_ThankYou; ?></textarea>
							</td>
						</tr>

						<tr>
							<td class="key cls_setting">
								<span class="editlinktip hasTip payresult" title="<?php echo JText::_( 'ID_STATUS_THANKYOU' ); ?>::<?php echo JText::_( 'ID_STATUS_THANKYOU_TIPS' ); ?>">
								</span>
								<span class="shorttext">
									<?php echo JText::_( 'ID_ARTICLE' ); ?>
									<input <?php echo $disables; ?>  type="radio" class="article_class" name="IDEAL_pay_pend_value" value="1" <?php $var=$this->idealconfigarray->IDEAL_pay_pend_value;if($var==1){echo "checked"; } ?> >
								</span>
							</td>
							<td class="cls_setting_select">
								<?php

									$doc =  JFactory::getDocument();

									$article_id4=$this->idealconfigarray->IDEAL_pay_pend_articleid;

									JHTML::_('behavior.modal', 'a.modal1');
									// Build the script.
									$script = array();
									$script[] = '	function jSelectArticle_4(id, title, catid, object) {';
									$script[] = '		document.id("4_id").value = id;';
									$script[] = '		document.id("4_name").value = title;';
									$script[] = '		SqueezeBox.close();';
									$script[] = '	}';

									// Setup variables for display.
									$html	= array();
									// Add the script to the document head.
									JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));


									// Setup variables for display.
									$html	= array();
									$link	= 'index.php?option=com_content&amp;view=articles&amp;layout=modal&amp;tmpl=component&amp;function=jSelectArticle_4';

									$db	= JFactory::getDBO();
									$db->setQuery('SELECT title FROM #__content WHERE id = '.(int) $article_id4	);
									$title = $db->loadResult();

									if ($error = $db->getErrorMsg()) {
										JError::raiseWarning(500, $error);
									}

									if (empty($title)) {
										$title = JText::_('COM_CONTENT_SELECT_AN_ARTICLE');
									}
									$title = htmlspecialchars($title, ENT_QUOTES, 'UTF-8');

									if($jversion_sht == 2){

										// The current user display field.
										$html[] = '<div class="fltlft">';
										$html[] = '  <input type="text" id="4_name" value="'.$title.'" disabled="disabled" size="35" />';
										$html[] = '</div>';

										// The user select button.
										$html[] = '<div class="button2-left">';
										$html[] = '  <div class="blank">';
										$html[] = '	<a class="modal1" title="'.JText::_('SELECT_ARTICLE').'"  href="'.$link.'" rel="{handler: \'iframe\', size: {x: 800, y: 450}}">'.JText::_('SELECT_ARTICLE').'</a>';
										$html[] = '  </div>';
										$html[] = '</div>';

									}
									if($jversion_sht == 3){

										// The current user display field.
										$html[] = '<span class="input-append">';
										$html[] = '  <input type="text" id="4_name" value="'.$title.'" disabled="disabled" size="35" />';

										// The user select button.
										$html[] = '	<a class="modal1 btn" title="'.JText::_('SELECT_ARTICLE').'"  href="'.$link.'" rel="{handler: \'iframe\', size: {x: 800, y: 450}}">';
										$html[] = '<i class="icon-file"></i>'. JText::_('SELECT_ARTICLE').'</a> ';
										$html[] = '</span>';

									}

									// The active article id field.
									if (0 == (int)@$this->value) {
										$value = '';
									} else {
										$value = (int)$this->value;
									}

									// class='required' for client side validation
									$class1 = '';
									if (@$this->required) {
										$class = ' class="required modal-value"';
									}

									$html[] = '<input type="hidden" id="4_id"'.$class.' name="IDEAL_pay_pend_articleid" value="'.(int)$article_id4.'" />';

									echo implode("\n", $html);

								?>
							</td>
						</tr>
						</tbody>
					</table>
				</fieldset>

			</div>
			<div class="span1">
			</div>
			<div class="span5">

				<fieldset>
				<legend><?php echo JText::_( 'CCIDEAL_EMAIL_SETTINGS' ); ?></legend>

					<div class="well">
						<?php echo JText::_( 'CCIDEAL_EMAIL_SETTINGS_HEADMSG' ); ?>
					</div>

					<div class="control-group well well-small">
						<label class="control-label">
									<?php echo JText::_( 'CCIDEAL_EMAIL_ENABLE_NOTIFY' ); ?>
						</label>
						<div class="controls">

									<?php
										$ideal_EnableEmailYES="";
										$ideal_EnableEmailNO="";
										if($this->idealconfigarray->emailnotify  == "1")
										{
											$ideal_EnableEmailYES = "checked='checked'";
											$ideal_EnableEmailNO = "";
										}
										if($this->idealconfigarray->emailnotify  == "0")
										{
											$ideal_EnableEmailYES = "";
											$ideal_EnableEmailNO = "checked='checked'";
										}
									?>


									<fieldset id="ccidealplatform_radio" class="radio btn-group control-group" >
										<input type="radio" name="emailnotify" value="1" id="emailnotify1" <?php echo $ideal_EnableEmailYES; ?> />
										<label for="emailnotify1" class="radiobtn"><?php echo JText::_('CCIDEAL_EMAIL_NOT_YES');?></label>
										<input type="radio" name="emailnotify" id="emailnotify0" value="0" <?php echo $ideal_EnableEmailNO; ?> />
										<label for="emailnotify0" class="radiobtn"><?php echo JText::_('CCIDEAL_EMAIL_NOT_NO');?></label>
									</fieldset>
						</div>
					</div>

					<div class="alert alert-info">
						<?php echo JText::_( 'CCIDEAL_EMAIL_NOTE' ); ?>
					</div>

					<div class="control-group well well-small form-horizontal">
						<label class="control-label">
									<?php echo JText::_( 'CCIDEAL_NOTIFY_EMAILS' ); ?>
						</label>
						<div class="controls">
							<input type="text" name="emailsss" value="<?php echo $this->idealconfigarray->emailsss; ?>" maxlength="255" size="30"></td>
						</div>
						<p class="text-info">
							<?php echo JText::_('CCIDEAL_NOTIFY_EMAILS_DESC');?>
						</p>
					</div>


			<div class="control-group well well-small form-horizontal">
				<label class="control-label">
									<?php echo JText::_( 'CCIDEAL_NOTIFY_EMAIL_SUBJECT' ); ?>
				</label>
				<div class="controls">
					<input type="text" name="emailsubject" value="<?php echo $this->idealconfigarray->emailsubject; ?>" maxlength="255" size="30">
				</div>
				</div>
					<div class="control-group well well-small">
						<label class="control-label">
									<?php echo JText::_( 'CCIDEAL_NOTIFY_EMAIL_BODY' ); ?>
						</label>
						<div class="controls">
							<textarea name="emailbody" rows="10" style="width:95%;"><?php echo $this->idealconfigarray->emailbody; ?></textarea>
						</div>
					</div>
				</fieldset>
		</div>
	</div>

		<br />
		<a href="#config-top" class="btn btn-block"><i class="icon-chevron-up"></i> <?php echo JText::_( 'COM_CCIDEAL_CONFIGURATION_BACK_TO_TOP' ); ?> <i class="icon-chevron-up"></i> </a>
		<br />
		<br />
		<br />



			<?php echo JHTML::_( 'form.token' ); ?>

			<input type="hidden" name="option" value="com_ccidealplatform" />
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="id" value="<?php echo $this->idealconfigarray->id;?>" />
			<input type="hidden" name="boxchecked" value="0" />
	</form>

	<?php

	if (isset ($this->versionContent)) {
		echo $this->versionContent;
	}
	?>
	<div class="well well-small pagination-centered" style="font-size: 10pt;">



		<div class="row-fluid">
			<div class="span4">
				<?php echo $this->name; ?>&nbsp;<?php echo $this->version; ?> by <a href="http://www.chillcreations.com/" target="_blank">Chill Creations</a><br />
				Copyright &copy; 2006-<?php echo $curYear = date('Y'); ?> Chill Creations<br />
			</div>

			<div class="span4">
			   <p>Questions, suggestions or problems?</p>
				<a class="btn btn-small" href="http://www.chillcreations.com/manuals/ccideal-platform" target="_blank">Manual</a>&nbsp;&nbsp;&nbsp;
				<a class="btn btn-small btn-success" href="http://www.chillcreations.com/support" target="_blank">Support</a>&nbsp;&nbsp;&nbsp;
				<a class="btn btn-small" href="http://www.chillcreations.com/joomla-extensions/ccideal-platform-ideal-for-joomla" target="_blank">Information</a>
			</div>

			<div class="span4">
				<p>Please post a review at the: </p>
				<a class="btn btn-small btn-warning" href="http://extensions.joomla.org/extensions/e-commerce/payment-systems/13009" target="_blank">Joomla! Extensions Directory</a><br />
			</div>
		</div>
	</div>
</div>

