<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license. 
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class cciDealPlatformViewccideal extends JViewLegacy
{
	function display($tpl = null)
	{
		JToolBarHelper::title( JText::_( 'ID_IDEAL_TITLE' ) . ': ' . JText::_('ID_GLOBAL_CONFIGURATION'), 'ccideal.png' );

	 	// Take Current Joomla Version

		$jversion 			= new JVersion();
		$current_version 	= $jversion->getShortVersion();
		$jversion_sht		= substr($current_version,0,1);

		$xml    	 = JFactory::getXML(JPATH_COMPONENT.DS.'ccidealplatform.xml');
   		$version_no  = (string)$xml->version;
		$name		 = (string)$xml->name;

		$canDo = ccidealplatformHelper::getActions();

		if ($canDo->get('ccidealplatform.config')) {
			$config_iDEAL = 1;
		}else{
			$config_iDEAL = -1;
		}

		$model =  $this->getModel();

		$ideal_config_array = $model->getBankData();

		$iDEALProVersion = $model->iDEALProVersion();

		$bank_list = $model->getBankList(@$ideal_config_array->IDEAL_Mode);

		$idealstatus = $model->getIDealStatusList();

		/*Direct the certificate path based on the bank*/
		$bank_file = strtolower(@$ideal_config_array->IDEAL_Bank);

		if($bank_file=='ing'){
			$certificate_path = "ing_certificates";
		}else{
			$certificate_path = "rabo_certificates";
		}
		$cert_filepath = JPATH_SITE . DS .'components'. DS .'com_ccidealplatform'.DS.'ccideal_adv_pro_idealv3'. DS .$certificate_path.DS.'cert.cer';
		$key_filepath = JPATH_SITE . DS .'components'. DS .'com_ccidealplatform'.DS.'ccideal_adv_pro_idealv3'. DS .$certificate_path.DS.'priv.pem';

		if($jversion_sht == "3"){
			$this->sidebar = JHtmlSidebar::render();
		}

		//Check the VM for warning message
		$db = JFactory::getDBO();
		$chk_VM = "select * from #__extensions where `element`='com_virtuemart' and enabled=1";
		$db->setQuery($chk_VM);$getObject = $db->loadObject();
		if(!empty($getObject->manifest_cache) && ($getObject->element == "com_virtuemart") && ($getObject->type == "component")){			
			$vm_assign=1;			
		}else{
			$vm_assign=0;
		}

		/*$this->targetpaylist = $model->gettargetpaylist();
		$this->mollielist	 = $model->getmollielist();
		$this->raboomnilist	 = $model->getraboomnilist();
		$this->sisowlist	 = $model->getsisowlist();*/



		$this->assignRef('jversion', $jversion_sht);
		$this->assignRef('config_iDEAL', $config_iDEAL);
		$this->assignRef('banklist', $bank_list);
		$this->assignRef('idealstatus', $idealstatus);
		$this->assignRef('idealconfigarray', $ideal_config_array);
		$this->assignRef('certfilepath', $cert_filepath);
		$this->assignRef('keyfilepath', $key_filepath);
		$this->assignRef('vm_assign', $vm_assign);

		$this->assignRef('name',		$name);
		$this->assignRef('version',		$version_no);
		$this->assignRef('iDEALProVersion', $iDEALProVersion);

		$this->minimum_stab = $model->getminimum_stability();

		$this->addToolbar();

		parent::display($tpl);
	}

	protected function addToolbar()
	{
		$canDo = ccidealplatformHelper::getActions();

		if ($canDo->get('ccidealplatform.config')) {
			JToolBarHelper::divider();
			JToolBarHelper::apply('ccideal.save');
		}

		if ($canDo->get('core.admin')) {
			JToolBarHelper::preferences('com_ccidealplatform','550','875',JTEXT::_('JTOOLBAR_OPTIONS_PERMISSIONS'));
		}
	}
}
?>