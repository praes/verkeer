<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class cciDealPlatformViewintegrations extends JViewLegacy
{
		function display($tpl = null)
		{
			//Only used for the integrations view
			$document = JFactory::getDocument();
			$document->addScript('components/com_ccidealplatform/assets/js/integrations.js');

			JToolBarHelper::title( JText::_( 'ID_IDEAL_TITLE' ) . ': ' . JText::_('CCIDEAL_INTEGRATIONS_TITLE'), 'ccideal.png' );

			$this->model = $this->getModel();

			$this->addToolbar();

			// Take Current Joomla Version
			$jversion 			= new JVersion();
			$current_version 	= $jversion->getShortVersion();
			$this->jversion_sht	= substr($current_version,0,1);

			$this->items		= $this->get('Items');


			$this->state		= $this->get('State');


			if($this->jversion_sht == "3"){
				$this->sidebar = JHtmlSidebar::render();
			}

			$xml    	 = JFactory::getXML(JPATH_COMPONENT.DS.'ccidealplatform.xml');
	   		$version_no  = (string)$xml->version;
			$name		 = (string)$xml->name;

			$this->assignRef('version', 	$version_no);
			$this->assignRef('name', 	$name);
			
			$canDo = ccidealplatformHelper::getActions();

			if (!$canDo->get('ccidealplatform.integrations')) {
				return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
			}

			parent::display($tpl);
		}

		protected function addToolbar()
		{
			$canDo = ccidealplatformHelper::getActions();

			JToolBarHelper::custom('integrations.refresh', 'refresh', '', JText::_('CCIDEAL_INTEGRATIONS_REFRESH'), false, false);

			if ($canDo->get('core.admin')) {
				JToolBarHelper::preferences('com_ccidealplatform','550','875',JTEXT::_('JTOOLBAR_OPTIONS_PERMISSIONS'));
			}
		}
	}
?>