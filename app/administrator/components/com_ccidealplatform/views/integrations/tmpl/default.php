<?php
/**
* @package	cciDEAL Platform 
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$jversion 			= new JVersion();
$current_version 	= $jversion->getShortVersion();
$jversion_sht		= substr($current_version,0,1);

//Message
$session = JFactory::getSession();
$msg_suc = $session->get('msg_plug');
if($msg_suc){
	$session->clear('msg_plug');
}

?>
	<form action="<?php echo JRoute::_('index.php?option=com_ccidealplatform&view=integrations'); ?>" method="post" name="adminForm" id="adminForm">
		<?php if(!empty( $this->sidebar)): ?>
			<div id="j-sidebar-container" class="span2">
				<?php echo $this->sidebar; ?>
			</div>
			<div id="j-main-container" class="span10">
		<?php else : ?>
			<div id="j-main-container">
		<?php endif;?>

	<div class="chillcreations-bootstrap">
		<div class="well">
			<?php echo JText::_('CCIDEAL_INTEGRATIONS_QUESTIONS');?>
				<a href="<?php echo JRoute::_('index.php?option=com_installer&view=update'); ?>" class="btn btn-warning">
					<i class="icon icon-refresh"></i> <?php echo JText::_('CCIDEAL_INTEGRATIONS_CHECK_UPDATES'); ?>
				</a>
			<a href="http://www.chillcreations.com/manuals/ccideal-platform/680-updating-ccideal" target="_blank" class="btn">
				<i class="icon icon-book"></i> <?php echo JText::_('CCIDEAL_INTEGRATIONS_UPDATE_MANUAL'); ?>
			</a>
	    </div>

	    <?php
	    	if($msg_suc == "plug_sucs"){
	    ?>
	    		<div class="alert alert-info">
		  			<button type="button" class="close" data-dismiss="alert">&times;</button>
		  				<?php echo JText::_("CCIDEAL_INTEGRATIONS_INSTALL_IDEAL_PLUGIN"); ?>
				</div>
		<?php
	    	}
	    ?>

		<table class="adminlist table table-striped" border="0">
			<thead>
				<tr>
					<th width="10%" class="center">
						<?php echo JText::_('JSTATUS'); ?>
					</th>
					<th>
						<?php echo JText::_( 'CCIDEAL_INTEGRATIONS_EXT_VER' ); ?>
					</th>
					<th>
						<?php echo JText::_( 'CCIDEAL_INTEGRATIONS_CURRENT_PLG' ); ?>
					</th>

					<th>
						<?php echo JText::_( 'CCIDEAL_INTEGRATIONS_LATEST_PLG' ); ?>
					</th>

					<th>
						<?php echo JText::_( 'CCIDEAL_INTEGRATIONS_ACTIONS' ); ?>
					</th>
					<th></th>
					<th></th>
					<th>
						<?php echo JText::_( 'CCIDEAL_INTEGRATIONS_ACCESS' ); ?>
					</th>
					<th>
						<?php echo JText::_( 'CCIDEAL_INTEGRATIONS_ID' ); ?>
					</th>
				</tr>
			</thead>

			<?php			
			foreach($this->items as $key => $item){
				if ( @$item->extension_id ) {

					if ( $item->folder == "akpayment" AND $item->extensions_element == "akpayment" ) {
						$akeeba = "";
						$akeeba = JFactory::getXML( JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_akeebasubs' . DS . 'aaa_akeebasubs.xml' );
						$akbs   = (string) $akeeba->version;

						if ( substr( $akbs, 0, 3 ) > 4.0 ) {
							$akeeba_v = 4;
						} else {
							$akeeba_v = 3;
						}
						if ( $item->extn_name == "Akeeba Subscriptions 4.x" ) {
							$item->element = "iDEAL";
						} else {
							$item->element = "iDEAL3";
						}
					}
				}
				if ( $item->folder == "vmpayment" ) {
					if ( file_exists( JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_virtuemart' . DS . 'virtuemart.xml' ) ) {
						$virtuemart = JFactory::getXML( JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_virtuemart' . DS . 'virtuemart.xml' );
						$vm_version = (string) $virtuemart->version;
						$vm_shtver  = substr( $vm_version, 0, 3 );
					}
				}

				@$element = $item->element;
				@$folder = $item->folder;

				// David - 2015-07-27
				// Get current version of plugin from plugin XML file and not via Joomla database
				$current_version = $this->model->current_version( @$element, @$folder );

				// Get version of latest plugin installed with cciDEAL,
				// This is the latest version included in cciDEAL package, can be a newer version than installed on the site
				$latest_version = $this->model->latest_version( @$element, @$folder );

				if ( @$item->extension_id ) {
					$enabled      = @$item->enabled;
					$extension_id = @$item->extension_id;
					$pub_task     = 'integrations.';
					$publish_enab = true;
					$plug_link    = JRoute::_( 'index.php?option=com_plugins&task=plugin.edit&extension_id=' . (int) @$item->extension_id );

				} else {
					$enabled      = "";
					$extension_id = "";
					$pub_task     = "";
					$publish_enab = false;
					$plug_link    = 'javascript:void(0);';
				}

				$oextension  = "";
				$install_plg = "";

				if ( ( $element == "content_ideal" ) && ( $folder == "content" ) ) {
					$link = 'http://www.chillcreations.com/manuals/ccideal-platform/279-adding-ideal-joomla-donations-donaties';

				} elseif ( ( $element == "plg_ccinvoices_ideal" ) && ( $folder == "ccinvoices_payment" ) ) {
					$oextension = "com_ccinvoices";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform/667-adding-ideal-to-ccinvoices';

				} elseif ( ( $element == "iDEAL" ) && ( $folder == "akpayment" ) ) {
					$oextension = "com_akeebasubs";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform/381-adding-ideal-to-akeeba-subscriptions';

				} elseif ( ( $element == "iDEAL3" ) && ( $folder == "akpayment" ) ) {
					$oextension = "com_akeebasubs";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform/381-adding-ideal-to-akeeba-subscriptions';

				} elseif ( ( $element == "iDEAL" ) && ( $folder == "hikashoppayment" ) ) {
					$oextension = "com_hikashop";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform/280-adding-ideal-to-hikashop';

				} elseif ( ( $element == "rdmccidealplatform" ) && ( $folder == "rdmedia" ) ) {
					$oextension = "com_ticketmaster";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform/612-adding-ideal-to-rdmedia-ticketmaster';

				} elseif ( ( $element == "rsmembership_ccidealplatform" ) && ( $folder == "system" ) ) {
					$oextension = "com_rsmembership";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform/309-adding-ideal-to-rsmembership';

				} elseif ( ( $element == "rsepro_ideal" ) && ( $folder == "system" ) ) {
					$oextension = "com_rseventspro";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform/439-adding-ideal-to-rsevents-pro';

				} elseif ( ( $element == "rsfp_ideal" ) && ( $folder == "system" ) ) {
					$oextension = "com_rsform";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform/290-adding-ideal-to-rsform-pro';

				} elseif ( ( $element == "plg_joomisp_ideal" ) && ( $folder == "payment" ) ) {
					$oextension = "com_joomisp";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform/390-adding-ideal-to-joomisp';

				} elseif ( ( $element == "plg_sobipro_ideal" ) && ( $folder == "system" ) ) {
					$oextension = "com_sobipro";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform/391-adding-ideal-to-sobipro';

				} elseif ( ( $element == "plg_virtuemart20_ideal" ) && ( $folder == "vmpayment" ) ) {
					$oextension = "com_virtuemart";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform/385-adding-ideal-to-virtuemart-20';

				} elseif ( ( $element == "plg_virtuemart3_ideal" ) && ( $folder == "vmpayment" ) ) {
					$oextension = "com_virtuemart";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform/385-adding-ideal-to-virtuemart-20';

				} elseif ( ( $element == "rs_payment_ccidealplatform" ) && ( $folder == "redshop_payment" ) ) {
					$oextension = "com_redshop";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform/311-adding-ideal-to-redshop';

				} elseif ( ( $element == "scideal" ) && ( $folder == "content" ) ) {
					$oextension = "com_simplecaddy";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform/235-adding-ideal-to-simple-caddy';

				} elseif ( ( $element == "rsdirectoryideal" ) && ( $folder == "system" ) ) {
					$oextension = "com_rsdirectory";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform/688-adding-ideal-to-rsdirectory';

				} elseif ( ( $element == "payment_ccideal" ) && ( $folder == "j2store" ) ) {
					$oextension = "com_j2store";
					$link       = 'http://www.chillcreations.com/manuals/ccideal-platform';

				} elseif ( ( $element == "ccideal" ) && ( $folder == "payment" ) ) {

					if ( $this->model->component_install( 'com_jticketing' ) ) {
						$oextension = "com_jticketing";
					} elseif ( $this->model->component_install( 'com_jgive' ) ) {
						$oextension = "com_jgive";
					}/*elseif($this->model->component_install('')){
					
					}elseif($this->model->component_install('')){
					
					}*/

					$link = 'http://www.chillcreations.com/manuals/ccideal-platform';
				}

				$install_plg = $this->model->component_install( $oextension );

			?>
			<tr>
				<td class="center">
					<?php echo JHtml::_( 'jgrid.published', $enabled, $extension_id, $pub_task, $publish_enab ); ?>
				</td>
				<td align="center">
					<a href="<?php echo $plug_link; ?>" target="_blank" title="<?php echo $item->extn_name; ?>">
						<?php
							if ($item->extn_name == "Content plugin") {
									echo $item->extn_name . " (cciDEAL Simpel)";
								} else {
									echo $item->extn_name;
								}
						?>
						</a>
					</td>

					<!-- David - 2015-07-27 -->
					<!-- Updated $items->version (from Joomla DB) to $current_version (from XML files) -->
					<td align="center">
						<?php echo @$current_version != '' ? $current_version : '-'; ?>
					</td>

					<td align="center">
						<?php echo $latest_version;	?>
					</td>
					<td align="center">
						<?php							
							if((@$current_version == $latest_version) || (@$current_version > $latest_version)){
								?>
									<a href="javascript:void(0);" onclick="return exten_down('plugininstall','<?php echo $item->element; ?>', '<?php echo $item->folder; ?>');" title="Reinstall">
										<icon class="btn btn-small"><?php echo JText::_( 'CCIDEAL_INTEGRATIONS_REINSTALL' ); ?></icon>
									</a>
								<?php
							}elseif(((@$current_version < $latest_version)) && @$current_version != "-"){
								?>
									<a href="javascript:void(0);" onclick="return exten_down('plugininstall','<?php echo $item->element; ?>', '<?php echo $item->folder; ?>');" title="Update">
										<icon class="btn btn-small btn-warning"><?php echo JText::_( 'CCIDEAL_INTEGRATIONS_UPDATE' ); ?></icon>
									</a>
								<?php
							}else{

								if($install_plg){

									if((($item->element == "plg_virtuemart20_ideal") && ($vm_shtver > 2.6)) || (($item->element == "scideal") && ($jversion_sht == "3")) || (($item->element =="rs_payment_ccidealplatform") && ($jversion_sht == "3")) ){
										echo '<icon class="btn btn-small disabled">'.JText::_( 'CCIDEAL_INTEGRATIONS_INSTALL' ).'</icon>';
									}else if(($item->element == "plg_virtuemart3_ideal") && ($vm_shtver <= 2.6)){
										echo '<icon class="btn btn-small disabled">'.JText::_( 'CCIDEAL_INTEGRATIONS_INSTALL' ).'</icon>';
									}else if(((@$akeeba_v == 4) && ($item->element == "iDEAL3")) || ((@$akeeba_v == 3) && ($item->element == "iDEAL"))){
										echo '<icon class="btn btn-small disabled">'.JText::_( 'CCIDEAL_INTEGRATIONS_INSTALL' ).'</icon>';
									}else{
										?>
										<a href="javascript:void(0);" onclick="return exten_down('plugininstall','<?php echo $item->element; ?>', '<?php echo $item->folder; ?>');" title="Install">
											<icon class="btn btn-small"><?php echo JText::_( 'CCIDEAL_INTEGRATIONS_INSTALL' ); ?></icon>
										</a>
										<?php
									}
								}else{
									echo '<icon class="btn btn-small disabled">'.JText::_( 'CCIDEAL_INTEGRATIONS_INSTALL' ).'</icon>';
								}
							}							
						?>
					</td>
					<td align="center">
						<?php
							/*if((($item->element =="plg_virtuemart20_ideal") || ($item->element =="scideal") || ($item->element =="rs_payment_ccidealplatform")) && ($jversion_sht == "3")){
								echo '<icon class="btn btn-small disabled">'.JText::_( 'CCIDEAL_INTEGRATIONS_DOWNLOAD' ).'</icon>';
							}*/
						?>
						<a href="javascript:void(0);" onclick="return exten_down('downloadextensions','<?php echo $item->element; ?>', '<?php echo $item->folder; ?>');" title="Download">
							<icon class="btn btn-small"><?php echo JText::_( 'CCIDEAL_INTEGRATIONS_DOWNLOAD' ); ?></icon>
						</a>
					</td>
					<td align="center">
						<a href="<?php echo $link; ?>" title="<?php echo @$item->name; ?>" target="_blank">
							<icon class="btn btn-small"><?php echo JText::_( 'CCIDEAL_INTEGRATIONS_VIEWMANUAL' ); ?></icon>
						</a>
					</td>
					<td align="center">
						<?php
							if(@$item->access_level != ''){
								if($this->escape(@$item->access) == 1){
									echo '<span class="badge badge-success">'.$this->escape(@$item->access_level).'</span>';
								}else{
									echo '<span class="badge badge-important">'.$this->escape(@$item->access_level).'</span>';
								}
							}else{
								echo '-';
							}
						?>
					</td>
					<td align="center">
						<?php echo @$item->extension_id != '' ? $item->extension_id : '-'; ?>
					</td>
				</tr>
			<?php
			}
			?>

		</table>
		<input type="hidden" name="delement" value="" />
		<input type="hidden" name="dfolder" value="" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" id="extension_id" name="extension_id" value="" />
		<?php echo JHtml::_('form.token'); ?>

	</form>

	<div class="well well-small pagination-centered" style="font-size: 10pt;">



		<div class="row-fluid">
			<div class="span4">
				<?php echo $this->name; ?>&nbsp;<?php echo $this->version; ?> by <a href="http://www.chillcreations.com/" target="_blank">Chill Creations</a><br />
				Copyright &copy; 2006-<?php echo $curYear = date('Y'); ?> Chill Creations<br />
			</div>

			<div class="span4">
			   <p>Questions, suggestions or problems?</p>
				<a class="btn btn-small" href="http://www.chillcreations.com/manuals/ccideal-platform" target="_blank">Manual</a>&nbsp;&nbsp;&nbsp;
				<a class="btn btn-small btn-success" href="http://www.chillcreations.com/support" target="_blank">Support</a>&nbsp;&nbsp;&nbsp;
				<a class="btn btn-small" href="http://www.chillcreations.com/joomla-extensions/ccideal-platform-ideal-for-joomla" target="_blank">Information</a>
			</div>

			<div class="span4">
				<p>Please post a review at the: </p>
				<a class="btn btn-small btn-warning" href="http://extensions.joomla.org/extensions/e-commerce/payment-systems/13009" target="_blank">Joomla! Extensions Directory</a><br />
			</div>
		</div>
	</div>
</div>