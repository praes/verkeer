INSERT IGNORE INTO `#__rsform_config` (`SettingName`, `SettingValue`) VALUES
('rsfp_mistercash.test', '0'),
('rsfp_mistercash.tax.type', '1'),
('rsfp_mistercash.tax.value', '');

INSERT IGNORE INTO `#__rsform_component_types` (`ComponentTypeId`, `ComponentTypeName`) VALUES (710, 'rsfp_mistercash');

DELETE FROM #__rsform_component_type_fields WHERE ComponentTypeId = 710;
INSERT IGNORE INTO `#__rsform_component_type_fields` (`ComponentTypeId`, `FieldName`, `FieldType`, `FieldValues`, `Ordering`) VALUES
(710, 'NAME', 'textbox', '', 0),
(710, 'LABEL', 'textbox', '', 1),
(710, 'COMPONENTTYPE', 'hidden', '710', 2),
(710, 'LAYOUTHIDDEN', 'hiddenparam', 'YES', 7);