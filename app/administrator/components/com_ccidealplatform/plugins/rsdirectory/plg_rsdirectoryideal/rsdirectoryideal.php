<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// No direct access.
defined('_JEXEC') or die('Restricted access');

// Load the RSDirectory! Plugin helper class file if it exists or stop the execution of the script if it doesn't exist.
if ( file_exists(JPATH_ADMINISTRATOR . '/components/com_rsdirectory/helpers/plugin.php') )
{
    require_once JPATH_ADMINISTRATOR . '/components/com_rsdirectory/helpers/plugin.php';
}
else
{
    return;
}

/**
 * RSDirectory! iDEAL Payment Plugin.
 */
class plgSystemRSDirectoryIdeal extends RSDirectoryPlugin
{
    const PAYMENT_METHOD = 'ideal';
    const PLUGIN = 'rsdirectoryideal';
    const EXTENSION = 'plg_system_rsdirectoryideal';
    const OPTION = 'PLG_SYSTEM_RSDIRECTORYIDEAL_NAME';
        
    /**
     * Class constructor.
     * 
     * @access public
     * 
     * @param object &$subject
     * @param array $config
     */
    public function __construct(&$subject, $config)
    {
		parent::__construct($subject, $config);
			
		$app = JFactory::getApplication();
			
		// Show a warning in the admin section if cURL is not installed or enabled.
		if ( $app->isAdmin() )
		{
			$lang = JFactory::getLanguage();
			$lang->load('com_rsdirectory', JPATH_ADMINISTRATOR);
			$lang->load(self::EXTENSION, JPATH_ADMINISTRATOR);		
		}
    }
        
    /**
     * Can the plugin run?
     *
     * @access public
     * 
     * @return bool
     */
    public function canRun()
    {        
		return parent::canRun() && JPluginHelper::isEnabled('system', self::PLUGIN);
    }
        
    /**
     * On after initialise.
     *
     * @access public
     */
    public function onAfterInitialise()
    {	
		if ( $this->canRun() && JFactory::getApplication()->input->get->get('rsdirectoryideal') )
		{
			//$this->processForm(); 
		}
    }
     
    /**
     * Add the current payment option to the payments list.
     *
     * @access public
     * 
     * @return string
     */
    public function rsdirectory_addPaymentOptions()
    {
		$lang = JFactory::getLanguage();
		$lang->load(self::EXTENSION, JPATH_ADMINISTRATOR);
			
		if ( $this->canRun() )
			return JHTML::_( 'select.option', self::PAYMENT_METHOD, JText::_(self::OPTION) );
    }
        
    /**
     * Show payment form.
     *
     * @access public
     * 
     * @param object $vars
     */
    public function rsdirectory_showForm($vars)
    {	
		// Do a few checks and exit the function if the conditions are not met.
		if ( !$this->canRun() || !isset($vars->method) || $vars->method != self::PAYMENT_METHOD )
			return;
					
			
		$lang = JFactory::getLanguage();
		$lang->load(self::EXTENSION, JPATH_ADMINISTRATOR);
		$lang->load('com_rsdirectory', JPATH_SITE);
			
		// Set the iDEAL url.
		$ideal_url = JRoute::_( JURI::root().'index.php?option=com_ccidealplatform&task=bankform');
			
		self::outputTransactionDetails($vars);			
			
		$price 		= self::formatPrice($vars->price);
		$tax_value 	= self::formatPrice($vars->tax);
		$total 		= $price + $tax_value;		
				
		?>	
			
		<p style="margin: 10px;font-weight: bold;"><?php echo JText::_('PLG_SYSTEM_RSDIRECTORYIDEAL_REDIRECTING'); ?></p>
			
		<form id="iDEALForm" method="post" action="<?php echo $ideal_url; ?>">
			<input type="hidden" name="id" value="<?php echo $vars->user_transaction_id; ?>" />
			<input type="hidden" name="grandtotal" value="<?php echo $total; ?>" />
			<input type="hidden" name="extn" value="rsdirectory" />
			<input type="hidden" name="ordernumber" value="<?php echo date("mdyy").$vars->user_transaction_id; ?>" />
			
		</form>	
			
		<script type="text/javascript">
			function idealFormSubmit()
			{
				document.getElementById('iDEALForm').submit();
			}
				
			try
			{
				window.addEventListener ? window.addEventListener('load', idealFormSubmit, false) : window.attachEvent('onload', idealFormSubmit);
			}
			catch (err)
			{
				idealFormSubmit();
			}
		</script>
			
		<?php
    }  
		
	/**
     * Get taxa data.
     *
     * @access public
     *
     * @param object $vars
     *
     * @return array
     */
    public function rsdirectory_GetTaxData($vars)
    {
        if ( !$this->canRun() || !isset($vars->method) || $vars->method != self::PAYMENT_METHOD )
            return;
            
        // Calculate the tax.
        $tax_type = $this->params->get('tax_type');
		$tax_value = $this->params->get('tax_value');
        $tax = $tax_value && $tax_type ? $vars->price * ($tax_value/100) : $tax_value;
            
        $data = array(
            'tax_type' => $tax_type ? 'percent' : 'fixed',
            'tax_value' => $tax_value,
            'tax' => $tax,
        );
			
		return $data;
    }
}