<?php
/**
* @package	Akeeba Subscriptions Payment - iDEAL plugin for cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

 defined('_JEXEC') or die();

 ?>
<?php
$t1 = JText::_('COM_AKEEBASUBS_LEVEL_REDIRECTING_HEADER');
$t2 = JText::_('COM_AKEEBASUBS_LEVEL_REDIRECTING_BODY');
?>

<h3><?php echo JText::_('COM_AKEEBASUBS_LEVEL_REDIRECTING_HEADER') ?></h3>
<p><?php echo JText::_('COM_AKEEBASUBS_LEVEL_REDIRECTING_BODY') ?></p>
<p align="center">
<form action="<?php echo htmlentities($data->url) ?>"  method="post" id="paymentForm">


	<input type="hidden" name="ordernumber" value="<?php echo date("Ymm").htmlentities($subscription->akeebasubs_subscription_id); ?>" />
	<input type="hidden" name="grandtotal" value="<?php echo htmlentities($subscription->net_amount)+htmlentities($subscription->tax_amount); ?>" />
	<input type="hidden" name="extn" id="extn" value="akeebasubs"/>
	<?php if($cbt = $this->params->get('cbt','')): ?>
	<input type="hidden" name="cbt" value="<?php echo htmlentities($cbt) ?>" />
	<?php endif; ?>
	<?php if($cpp_header_image = $this->params->get('cpp_header_image','')): ?>
	<input type="hidden" name="cpp_header_image" value="<?php echo htmlentities($cpp_header_image)?>" />
	<?php endif; ?>
	<?php if($cpp_headerback_color = $this->params->get('cpp_headerback_color','')): ?>
	<input type="hidden" name="cpp_headerback_color" value="<?php echo htmlentities($cpp_headerback_color)?>" />
	<?php endif; ?>
	<?php if($cpp_headerborder_color = $this->params->get('cpp_headerborder_color','')): ?>
	<input type="hidden" name="cpp_headerborder_color" value="<?php echo htmlentities($cpp_headerborder_color)?>" />
	<?php endif; ?>


</form>
</p>