<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

//defined( 'SOBIPRO' ) || exit( 'Restricted access' );
SPLoader::loadController( 'config', true );
/**
 * @author Radek Suski
 * @version 1.0
 * @created 06-Aug-2010 15:38:15
 */
class SPPaymentideal extends SPConfigAdmCtrl
{
	/**
	 * @var string
	 */
	protected $_defTask = 'config';

	public function execute()
	{
		$this->_task = strlen( $this->_task ) ? $this->_task : $this->_defTask;
		switch ( $this->_task ) {
			case 'config':
				$this->screen();
				Sobi::ReturnPoint();
				break;
			case 'save':
				$this->save();
				break;
			default:
				Sobi::Error( 'SPPaymentBt', 'Task not found', SPC::WARNING, 404, __LINE__, __FILE__ );
				break;
		}
	}

	protected function save()
	{
		if( !( SPFactory::mainframe()->checkToken() ) ) {
			Sobi::Error( 'Token', SPLang::e( 'UNAUTHORIZED_ACCESS_TASK', SPRequest::task() ), SPC::ERROR, 403, __LINE__, __FILE__ );
		}
		SPFactory::registry()->saveDBSection(
			array(
 				array( 'key' => 'idealurl', 'value' => SPRequest::string( 'idealurl' ) ),
 				array( 'key' => 'idealemail', 'value' => SPRequest::string( 'idealemail' ) ),
 				array( 'key' => 'idealcc', 'value' => SPRequest::string( 'idealcc' ) ),

			), 'ideal'
		);
		$data = array(
			'key' => 'idealexpl',
			'value' => SPRequest::string( 'idealexpl', null, true ),
			'type' => 'application',
			'id' => Sobi::Section(),
			'section' => Sobi::Section()
		);
		try {
			SPLang::saveValues( $data );
			$data[ 'key' ] = 'ppsubject';
			$data[ 'value' ] = SPRequest::string( 'ppsubject', true );
			SPLang::saveValues( $data );
		}
		catch ( SPException $x ) {
			$msg = SPLang::e( 'DB_REPORTS_ERR', $x->getMessage() );
			Sobi::Error( 'SPPaymentBt', $msg, SPC::WARNING, 0, __LINE__, __FILE__ );
			Sobi::Redirect( SPMainFrame::getBack(), $msg, SPC::ERROR_MSG, true );
		}
		Sobi::Redirect( SPMainFrame::getBack(), Sobi::Txt( 'MSG.ALL_CHANGES_SAVED' ) );
	}

	private function screen()
	{
		SPFactory::registry()->loadDBSection( 'ideal' );
		$ppexpl = SPLang::getValue( 'idealexpl', 'application' );
		$ppsubj = SPLang::getValue( 'ppsubject', 'application' );
		$view = $this->getView( 'ideal' );
		$tile = Sobi::Txt( 'ideal' );
		$view->assign( Sobi::Reg( 'ideal.idealurl.value' ), 'idealurl' );
		$view->assign( Sobi::Reg( 'ideal.idealemail.value' ), 'idealemail' );
		$view->assign( Sobi::Reg( 'ideal.idealcc.value' ), 'idealcc' );
		$view->assign( $ppexpl, 'idealexpl' );
		$view->assign( $ppsubj, 'idealsubject' );
		$view->loadConfig( 'extensions.ideal' );
		$view->setTemplate( 'extensions.ideal' );
		$view->display();
	}
}
?>