<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

//defined( 'SOBIPRO' ) || exit( 'Restricted access' );
$row = 0;

?>
<?php $this->trigger( 'OnStart' ); ?>
<div style="float: left; width: 20em; margin-left: 3px;">
	<?php $this->menu(); ?>
</div>
<?php $this->trigger( 'AfterDisplayMenu' ); ?>
<div style="margin-left: 20.8em; margin-top: 3px;">
	<fieldset class="adminform">
		<legend>
			<?php $this->txt( 'iDEAL' ); ?>
		</legend>
		<table class="admintable">
			<tr>
				<td>
					 <?php echo "Pay with iDEAL"; ?>
				</td>
			</tr>
		</table>
	</fieldset>
</div>
<?php $this->trigger( 'OnEnd' ); ?>