<?php

/**
* @package	iDEAL for sobiPro Payment for cciDEAL Platform 
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined('_JEXEC') or die('Restricted access');
@define( 'DS', DIRECTORY_SEPARATOR );

jimport('joomla.plugin.plugin');
jimport( 'joomla.html.parameter' );

class plgSystemplg_sobipro_ideal extends JPlugin
{

	public function plgSystemplg_sobipro_ideal( &$subject, $config )
	{

		parent::__construct( $subject, $config );
		$this->file_transfer();

	}

	function file_transfer(){

		jimport('joomla.filesystem.path');
		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.folder' );
		
		// Move Folder

		$exist_Folder 	= JPATH_ROOT.DS.'components'.DS.'com_sobipro'.DS.'opt'.DS.'plugins'.DS.'ideal';
		$copy_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'plg_sobipro_ideal'.DS.'ideal';
		$paste_Folder 	= JPATH_ROOT.DS.'components'.DS.'com_sobipro'.DS.'opt'.DS.'plugins'.DS.'ideal';

		if(!JFolder::exists($exist_Folder)){
			JFolder::move($copy_Folder,$paste_Folder);

		}

		// 	Move Files

		$exist_file = JPATH_ROOT.DS.'components'.DS.'com_sobipro'.DS.'etc'.DS.'ideal.ini';
		$copy_file 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'plg_sobipro_ideal'.DS.'ideal.ini';
		$paste_file = JPATH_ROOT.DS.'components'.DS.'com_sobipro'.DS.'etc'.DS.'ideal.ini';

		if (!JFile::exists($exist_file)){
			JFILE::Move($copy_file, $paste_file);

		}

		$exist_file_adm = JPATH_ROOT.DS.'components'.DS.'com_sobipro'.DS.'lib'.DS.'ctrl'.DS.'adm'.DS.'ideal.php';
		$copy_file_adm 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'plg_sobipro_ideal'.DS.'adm'.DS.'ideal.php';
		$paste_file_adm = JPATH_ROOT.DS.'components'.DS.'com_sobipro'.DS.'lib'.DS.'ctrl'.DS.'adm'.DS.'ideal.php';

		if (!JFile::exists($exist_file_adm)){
			JFILE::Move($copy_file_adm, $paste_file_adm);
		} 

	// In Administrator

		$exist_file_1 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_sobipro'.DS.'extensions'.DS.'ideal.ini';
		$copy_file_1 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'plg_sobipro_ideal'.DS.'extensions'.DS.'ideal.ini';
		$paste_file_1 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_sobipro'.DS.'extensions'.DS.'ideal.ini';

		if (!JFile::exists($exist_file_1)){
				JFILE::Move($copy_file_1, $paste_file_1);

		} 

		$exist_file_2 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_sobipro'.DS.'extensions'.DS.'ideal.php';
		$copy_file_2 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'plg_sobipro_ideal'.DS.'extensions'.DS.'ideal.php';
		$paste_file_2 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_sobipro'.DS.'extensions'.DS.'ideal.php';

		if (!JFile::exists($exist_file_2)){
			JFILE::Move($copy_file_2, $paste_file_2);

		} 

		// Inserting data in tables

		$db = JFactory::getDBO();

		//Table sobipro_plugins
			
		$select = "select pid from #__sobipro_plugins where pid='ideal'";
		$db->setQuery($select);
		$result = $db->loadResult();

		if($result==""){

			$insert_plugin = "INSERT INTO `#__sobipro_plugins` (`pid`, `name`, `version`, `description`, `author`, `authorURL`, `authorMail`, `enabled`, `type`, `depend`) VALUES" .
					"('ideal', 'iDEAL', '1.0', NULL, 'Chill Creations', 'http://www.chillcreations.com', 'info@chillcreations.com', 1, 'payment', '')";
			$db->setQuery($insert_plugin);
			$db->Query();
		}

		$select = "select pid from #__sobipro_plugin_section where pid='ideal' and section='71' ";
		$db->setQuery($select);
		$result = $db->loadResult();
		if($result==""){
			$insert_plugin_section = "INSERT INTO `#__sobipro_plugin_section` (`section`, `pid`, `type`, `enabled`, `position`) VALUES" .
					"(71, 'ideal', 'payment', 1, 3),(1, 'ideal', 'payment', 1, 0)";
			$db->setQuery($insert_plugin_section);
			$db->Query();
		}

	// Table sobipro_plugin_task

		$select = "select pid from #__sobipro_plugin_task where pid='ideal'  ";
		$db->setQuery($select);
		$result = $db->loadResult();
		if($result==""){
			$insert_plugin_task = "INSERT INTO `#__sobipro_plugin_task` (`pid`, `onAction`, `type`) VALUES " .
					"('ideal', 'adm_menu', 'payment'),
					('ideal', 'entry.payment', 'payment'),
					('ideal', 'entry.save', 'payment'),
					('ideal', 'entry.submit', 'payment')";
			$db->setQuery($insert_plugin_task);
			$db->Query();
		}

	//Table sobipro_registry

		$select = "SELECT * FROM `#__sobipro_registry` WHERE section = 'ideal'  ";
		$db->setQuery($select);
		$result = $db->loadResult();
		if($result==""){
			$insert_plugin_registry = "INSERT INTO `#__sobipro_registry` (`section`, `key`, `value`, `params`, `description`, `options`) VALUES" .
					"('ideal', 'idealcc', 'EUR', '', '', ''),
					('ideal', 'idealurl', 'index.php?option=com_ccidealplatform&task=bankform\r\n ', '', '', ''),
					('ideal', 'idealemail', 'change@me.com', '', '', '')";
			$db->setQuery($insert_plugin_registry);
			$db->Query();
		}


	// Delete files and folder after moving

		if(JFile::exists($paste_file_1) && JFile::exists($paste_file_2)){
			$folder_exist1 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'plg_sobipro_ideal'.DS.'extensions';
			if (file_exists($folder_exist1)){
					JFolder::delete($folder_exist1);
			}
		}else{
				JFILE::Move($copy_file_1, $paste_file_1);
				JFILE::Move($copy_file_2, $paste_file_2);
		}


		if(JFile::exists($paste_file_adm)){

			$folder_exist2 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'plg_sobipro_ideal'.DS.'adm';
			if (file_exists($folder_exist2)){
				JFolder::delete($folder_exist2);
			}
		}else{
			JFILE::Move($copy_file_adm, $paste_file_adm);
		}

		if(JFolder::exists($paste_Folder)){
			$Folder_exist 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'plg_sobipro_ideal'.DS.'ideal';
			if (file_exists($Folder_exist)){
						JFolder::delete($Folder_exist);
			}
		}else{
			JFolder::move($copy_Folder,$paste_Folder);
		}


		if(JFile::exists($paste_file)){
			$file_exist 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'plg_sobipro_ideal'.DS.'ideal.ini';
			if (file_exists($file_exist)){
				JFile::delete($file_exist);
			}
		}else{
			JFILE::Move($copy_file, $paste_file);
		}
	}
}
?>