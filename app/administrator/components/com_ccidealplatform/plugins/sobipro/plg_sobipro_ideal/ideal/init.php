<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

//defined( 'SOBIPRO' ) || exit( 'Restricted access' );
/**
 * @author Radek Suski
 * @version 1.0
 * @created 27-Nov-2009 17:10:15
 */
class SPPideal extends SPPlugin
{
	/* (non-PHPdoc)
	 * @see Site/lib/plugins/SPPlugin#provide($action)
	 */
	public function provide( $action )
	{
		return
			$action == 'PaymentMethodView' ||
			$action == 'AppPaymentMessageSend'
		;
	}

	public static function admMenu( &$links )
	{

		$links[ Sobi::Txt( 'iDEAL Payment' ) ] = 'ideal';

	}

	public function AppPaymentMessageSend( &$methods, $entry, &$payment, $html = false )
	{
		return $this->PaymentMethodView( $methods, $entry, $payment, !( $html ) );
	}

	/**
	 * This function have to add own string into the given array
	 * Basically: $methods[ $this->id ] = "Some String To Output";
	 * Optionally the value can be also SobiPro Arr2XML array.
	 * Check the documentation for more information
	 * @param array $methods
	 * @param SPEntry $entry
	 * @param array $payment
	 * @return void
	 */
	public function PaymentMethodView( &$methods, $entry, &$payment, $message = false )
	{
		SPFactory::registry()->loadDBSection( 'ideal' );
		$ppurl = SPLang::replacePlaceHolders( Sobi::Reg( 'ideal.idealurl.value' ), $entry );
		$ppemail = SPLang::replacePlaceHolders( Sobi::Reg( 'ideal.idealemail.value' ), $entry );
		$pprurl = SPLang::replacePlaceHolders( Sobi::Reg( 'ideal.idealurl.value' ), $entry );
		$ppcc = SPLang::replacePlaceHolders( Sobi::Reg( 'ideal.idealcc.value' ), $entry );
		$cfg = SPLoader::loadIniFile( 'etc.ideal' );
		$rp = $cfg[ 'general' ][ 'replace' ];
		$to = ( $cfg[ 'general' ][ 'replace' ] == ',' ) ? '.' : ',';
		$amount = str_replace( $rp, $to, $payment[ 'summary' ][ 'sum_brutto' ] );

		$values = array(
			'entry' => $entry,
			'amount' => preg_replace( '/[^0-9\.,]/', null, $amount ),
			'idealurl' => $ppurl,
			'idealemail' => $ppemail,
			'idealrurl' => $pprurl,
			'idealcc' => $ppcc,
		);

		//$expl = SPLang::replacePlaceHolders( SPLang::getValue( 'ppexpl', 'plugin' ), $values );
		$subject = SPLang::replacePlaceHolders( SPLang::getValue( 'ppsubject', 'plugin' ), $values );
	//	$values[ 'expl' ] = $expl;
		$values[ 'subject' ] = $subject;
		$methods[ $this->id ] = array (
			'content' => ( $message ? $this->raw( $cfg, $values ) : $this->content( $cfg, $values ) ),
			'title' => Sobi::Txt( 'Pay with ideal' )
		);
	}

	/**
	 * @param array $config
	 * @param array $values
	 * @return string
	 */
	private function raw( $config, $values )
	{
		$out = "\n";
		$out .= $values[ 'expl' ];
		$out .= Sobi::Txt( 'Pay with ideal' ).': ';
		$out .= $config[ 'message' ][ 'url' ];
		array_shift( $config[ 'message' ] );
		$v = array();
		foreach ( $config[ 'message' ] as $field => $value ) {
			$v[] = $field.'='.urlencode( SPLang::replacePlaceHolders( $value, $values ) );
		}
		$out .= implode( '&', $v );
		return $out;
	}

	/**
	 * @param array $config
	 * @param array $values
	 * @return string
	 */
	private function content( $config, $values )
	{
		$out = "\n";
		$out .= $values[ 'expl' ];
		$out .= "\n";
		$out .="<br/>";

		$out .= '<form action="index.php?option=com_ccidealplatform&task=bankform" method="post">'."\n";
		foreach ( $config[ 'fields' ] as $field => $value ) {

			if($field=='amount'){

				$out .= '<input name="grandtotal" value="'.htmlentities( SPLang::replacePlaceHolders( $value, $values ) ).'" type="hidden"/>'."\n";
			}
			if($field=='item_number'){

				$out .= '<input name="ordernumber" value="'.date("yddy").htmlentities( SPLang::replacePlaceHolders( $value, $values ) ).'" type="hidden"/>'."\n";
				$out .= '<input type="hidden" name="extn" id="extn" value="sobipro"/>'."\n";
			}

		}
		$img = SPLang::replacePlaceHolders( $config[ 'general' ][ 'image' ] );
		$out .= '<input src="'.JURI::root().'/administrator/components/com_ccidealplatform/assets/select_iDEAL.jpg" name="submit" alt="" type="image"/>'."\n";
		$out .= '</form>'."\n";
		$out .="<br/>";
		return $out;
	}
}
?>