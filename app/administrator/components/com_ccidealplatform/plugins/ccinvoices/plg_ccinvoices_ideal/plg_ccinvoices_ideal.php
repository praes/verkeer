<?php
/**
* @package	cciDEAL Platform - ccInvoices iDEAL payment plugin
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

defined('_JEXEC') or die('Restricted access');
/** Import library dependencies */ 

jimport('joomla.event.plugin');

	// Get language files
	// The cciDEAL integration plugin does not have it's own language file
	// It's language strings are in the ccInvoices site language file
	$language = JFactory::getLanguage();
	$language->load('com_ccinvoices', JPATH_COMPONENT_ADMINISTRATOR, 'en-GB', true);
	$language->load('com_ccinvoices', JPATH_SITE, 'en-GB', true);
	$language->load('com_ccinvoices', JPATH_SITE, null, true);


class plgccinvoices_paymentplg_ccinvoices_ideal extends JPlugin
{
	function plgccinvoices_paymentplg_ccinvoices_ideal( &$subject , $config )
	{
		parent::__construct( $subject , $config);
		$this->_plugin = JPluginHelper::getPlugin( 'ccinvoices_payment', 'plg_ccinvoices_ideal' );		
		
		$params["plugin_name"] = "plg_ccinvoices_ideal";		
		$params["description"] = JText::_("PLG_COM_CCIDEALPLATFORM_PAYMENT_METHOD_DESC");
		$params["payment_method"] = JText::_("PLG_COM_CCIDEALPLATFORM_PAYMENT_METHOD_NAME");
		$params["payment_method_note"] = JText::_("PLG_COM_CCIDEALPLATFORM_PAYMENT_METHOD_NAME");
		$this->params = $params;

	}
	function onPaymentMethodList($val)
	{
		$db = JFactory::getDBO();
		$db->setQuery("SELECT id,total,custom_invoice_number,number  FROM #__ccinvoices_invoices WHERE id =".$val['id']." LIMIT 1");
		$row = $db->loadObject();
		
		$date=date("mddy");
		
		$amount = $row->total;
		$extn = "ccinvoices";
		$ordernumber = $date.$row->id;
	
		$redirect = JRoute :: _("index.php?option=com_ccidealplatform&task=bankform");
	
		echo '<form action="'.$redirect.'" method="post" id="idealForm">';
		
			echo	'<input type="hidden" name="grandtotal" id="grandtotal" value="'.$amount.'"  />';
			echo    '<input type="hidden" name="ordernumber" id="ordernumber" value="'.$ordernumber.'" />';
			echo    '<input type="hidden" name="extn" id="extn" value="'.$extn.'" />';
		
		echo 	'</form>';	
		 	
		$html ='<table class="ccinvoices_paymentmethods" >
			<tr>';				
			$html .='<td width="160" align="center">
					<img src="'.JURI::root().'/components/com_ccidealplatform/assets/images/select_iDEAL.jpg" >
				</td>';
				
				$html .='<td>
					<p>'.$this->params["description"].'</p>
				</td>
				<td width="100" style="padding-right:15px;">
						<a href="javascript:void(0)" class="btn btn-warning" onclick="callpayment()" >'.JText::_("PLG_COM_CCIDEALPLATFORM_BTN_PAYNOW").' <i class="icon-chevron-right"></i></a>
				</td>
			</tr>
		</table>';
		
		?>
		
		<script type="text/javascript">			
			function callpayment(){				
				document.getElementById('idealForm').submit()
			}
		</script>
		<?php
		return $html;
	}
	function onSiteInvoiceOverviewPaymentIcons()
	{
		$paymentIcon["payment_method"] = $this->params["payment_method"];
		$paymentIcon["payment_method_note"] = $this->params["payment_method_note"];
		return $paymentIcon;
	}
}
?>