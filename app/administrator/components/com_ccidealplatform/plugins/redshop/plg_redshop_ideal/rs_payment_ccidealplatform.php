<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.plugin.plugin');
 
class plgRedshop_paymentrs_payment_ccidealplatform extends JPlugin
{
	var $_table_prefix = null;
   /**
    * Constructor
    *
    * For php4 compatability we must not use the __constructor as a constructor for
    * plugins because func_get_args ( void ) returns a copy of all passed arguments
    * NOT references.  This causes problems with cross-referencing necessary for the
    * observer design pattern.
    */
   	function plgRedshop_paymentrs_payment_ccidealplatform($subject)
    {
            // load plugin parameters
            parent::__construct( $subject );
            $this->_table_prefix = '#__redshop_';
            $this->_plugin = JPluginHelper::getPlugin( 'redshop_payment', 'rs_payment_ccidealplatform' );
            //$this->_params = new JParameter( $this->_plugin->params );
			$this->_params = new JRegistry($this->_plugin->params);
    }

    /**
    * Plugin method with the same name as the event will be called automatically.
    */
 	function onPrePayment($element, $data)
    {
    	if($element!='rs_payment_ccidealplatform'){
    		return;
    	}
    	if (empty($plugin))
        {
         	$plugin = $element;
        }

 		$mainframe = JFactory::getApplication();

 		$jversion = new JVersion();
		$current_version = $jversion->getShortVersion();
		$jversion=substr($current_version,0,3);
		if($jversion=="1.5"){
			$paymentpath=JPATH_SITE.DS.'plugins'.DS.'redshop_payment'.DS.$plugin.DS.'rs_payment_ccidealplatform_info.php';
		}else{
			$paymentpath=JPATH_SITE.DS.'plugins'.DS.'redshop_payment'.DS.$plugin.DS.'rs_payment_ccidealplatform'.DS.'rs_payment_ccidealplatform_info.php';
		}
		include($paymentpath);
    }

  function onNotifyPaymentrs_payment_ccidealplatform($element, $request){

    	if($element!='rs_payment_ccidealplatform'){
    		return;
    	}
    	$db = JFactory::getDBO();
		$request=JRequest::get('request');
		$Itemid = $request["Itemid"];

 		$quickpay_parameters=$this->getparameters('rs_payment_ccidealplatform');
		$paymentinfo = $quickpay_parameters[0];
		$paymentparams = new JParameter( $paymentinfo->params );

		$is_test 	= $paymentparams->get('sandbox','');
		$verify_status = $paymentparams->get('verify_status','');
		$invalid_status = $paymentparams->get('invalid_status','');
		$cancel_status = $paymentparams->get('cancel_status','');



		$user=JFactory::getUser();

		$query = "SELECT order_id FROM `#__redshop_orders` WHERE `order_number` = '" . $request['invoice'] . "'";
		$db->SetQuery($query);
	 	$order_id = $db->loadResult();


		$status=$request['payment_status'];
	 	$tid = $request['txn_id'];
		$uri = JURI::getInstance();
		$url= JURI::base();
		$uid=$user->id;
		$db=JFactory::getDBO();

		if($status=='Completed')
		{
		 	$values->order_status_code=$verify_status;
		 	$values->order_payment_status_code='Paid';
		 	$values->log=JTEXT::_('ORDER_PLACED');
 		 	$values->msg=JTEXT::_('ORDER_PLACED');

		}else{
			$values->order_status_code=$invalid_status;
			$values->order_payment_status_code='Unpaid';
		 	$values->log=JTEXT::_('ORDER_NOT_PLACED.');
 		 	$values->msg=JTEXT::_('ORDER_NOT_PLACED');
		}

 		$values->transaction_id=$tid;
		$values->order_id=$order_id;
		return $values;
    }

	function getparameters($payment){
		$db = JFactory::getDBO();
		$sql="SELECT * FROM #__plugins WHERE `element`='".$payment."'";
		$db->setQuery($sql);
		$params=$db->loadObjectList();
		return $params;
	}


	function orderPaymentNotYetUpdated($dbConn, $order_id, $tid){

		$db = JFactory::getDBO();
		$res = false;
		 $query = "SELECT COUNT(*) `qty` FROM `#__redshop_order_payment` WHERE `order_id` = '" . $db->getEscaped($order_id ) . "' and order_payment_trans_id = '" .$db->getEscaped( $tid) . "'";
		$db->SetQuery($query);
	 	$order_payment = $db->loadResult();
		if ($order_payment == 0){
			$res = true;
		}
		return $res;
	}
}