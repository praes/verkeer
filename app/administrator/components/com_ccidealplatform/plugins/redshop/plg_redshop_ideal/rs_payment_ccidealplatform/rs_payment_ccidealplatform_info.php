<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

	require_once( JPATH_COMPONENT.DS.'helpers'.DS.'helper.php' );
	require_once ( JPATH_SITE . DS . 'administrator' . DS . 'components' . DS . 'com_redshop' . DS . 'helpers' . DS . 'redshop.cfg.php');
	$objOrder = new order_functions();

  	$objconfiguration = new Redconfiguration();

   	$user	=	JFactory::getUser();
   	$shipping_address	=	$objOrder->getOrderShippingUserInfo($data['order_id']);
	$Itemid=$_REQUEST['Itemid'];

    $redhelper = new redhelper();
    $db = JFactory::getDBO();
	$user = JFActory::getUser();
	$task=JRequest::getVar('task');
	$layout=JRequest::getVar('layout');
	$mainframe = JFactory::getApplication();

	if($this->_params->get("currency")!="")
	{
		$currency_main = $this->_params->get("currency");
	} else if(CURRENCY_CODE!="") {
		$currency_main = CURRENCY_CODE;
	} else {
		$currency_main = "USD";
	}	
	
	$sql = "SELECT op.*,o.order_total,o.user_id,o.order_tax,o.order_subtotal,o.order_shipping,o.order_number,o.payment_discount FROM " . $this->_table_prefix . "order_payment AS op LEFT JOIN " . $this->_table_prefix . "orders AS o ON op.order_id = o.order_id  WHERE o.order_id='" . $data['order_id'] . "'";
	$db->setQuery($sql);
	$order_details = $db->loadObjectList();


    $idealurl =   "index.php?option=com_ccidealplatform&task=bankform";

    //$currencyClass = new convertPrice( );
	$currencyClass = new CurrencyHelper;	

    @$order->order_subtotal = $currencyClass->convert ( $order_details[0]->order_total, '', $currency_main );
	
    $post_variables = Array(

    "ordernumber" => $data['order_id'].date("Yym"),

	"grandtotal" =>  $order->order_subtotal,

	"extn"=>"redshop",


	);
if(SHIPPING_METHOD_ENABLE){
	$shipping_variables = Array(
	"address1"	=>$shipping_address->address,
	"city"	=>$shipping_address->city,
	"country"	=>$data['billinginfo']->country_2_code,
	"first_name"	=>$shipping_address->firstname,
	"last_name"	=>$shipping_address->lastname,
	"state"	=>$shipping_address->state_code,
	"zip"	=> $shipping_address->zipcode
	);
}

	$payment_price	=	$this->_params->get("payment_price");

	$post_variables['discount_amount_cart'] = round($currencyClass->convert ( $data['odiscount'], '', $currency_main ),2);
	if($this->_params->get("payment_oprand")=='-'){
		$discount_payment_price		=	$payment_price;
		$post_variables['discount_amount_cart']	+=	round($currencyClass->convert ( $order_details[0]->payment_discount, '', $currency_main ),2);

	}else{
		$discount_payment_price		=	$payment_price;
		$post_variables['handling_cart']	=	round($currencyClass->convert ( $order_details[0]->payment_discount, '', $currency_main ),2);
	}



    $db = JFactory::getDBO();
    $q_oi = "SELECT * FROM ".$this->_table_prefix."order_item ";
    $q_oi .= "WHERE ".$this->_table_prefix."order_item.order_id='".$data['order_id']."'";
    $db->setQuery($q_oi);
    $items = $db->loadObjectList();

    $q_oi = "SELECT sum(product_quantity) FROM ".$this->_table_prefix."order_item ";
    $q_oi .= "WHERE ".$this->_table_prefix."order_item.order_id='".$data['order_id']."'";
    $db->setQuery($q_oi);
    $totalq = $db->loadResult();

    $shipping = $order_details[0]->order_shipping/$totalq;
    for($i=0;$i<count($items);$i++) {
      $item = $items[$i];
      $tax =  ($item->product_final_price/$item->product_quantity)-$item->product_item_price ;

	  $supp_var["item_name_".($i+1)] = strip_tags($item->order_item_name);
      $supp_var["quantity_". ($i+1)] = $item->product_quantity;
      $supp_var["amount_". ($i+1)] = round($currencyClass->convert ( $item->product_item_price_excl_vat, '', $currency_main),2 );
     // $supp_var["tax_". ($i+1)] = round($tax,2);
      //  $supp_var["shipping_" . ($i+1)] = round($shipping,2);
      $shipping2 = $item->product_quantity*$shipping;
        $supp_var['shipping_' . ($i+1)] = round($currencyClass->convert ( $shipping2, '', $currency_main) ,2);

     }


		echo "<form action='$idealurl' method='post' name='idealfrm'>";
		echo "<h3>".JTEXT::_('Redirecting to iDEAL Payment')."</h3>";
		foreach( $post_variables as $name => $value )
		{
			echo "<input type='hidden' name='$name' value='$value' />";
		}
		if(is_array($supp_var) && count($supp_var))
		{
		  	foreach($supp_var as $name => $value)
			{
				echo '<input type="hidden" name="'.$name.'" value="'.$value.'" />';
			}
		}
		if(SHIPPING_METHOD_ENABLE)
		{
			if(is_array($shipping_variables) && count($shipping_variables))
			{
	       		 	foreach($shipping_variables as $name => $value)
				{
					echo '<input type="hidden" name="'.$name.'" value="'.$value.'" />';
				}
			}
        }
		echo '<INPUT TYPE="hidden" name="charset" value="utf-8">';
		echo "</form>";
?>
<script type='text/javascript'>document.idealfrm.submit();</script>