<?php
/**
* @package	Akeeba Subscriptions Payment - iDEAL plugin for cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

defined('_JEXEC') or die();

jimport('joomla.plugin.plugin');

//Akeeba mail conf
if(@!AKEEBASUBS_PRO){
	@define('AKEEBASUBS_PRO', '1');
}

$akpaymentinclude = include_once JPATH_ADMINISTRATOR.'/components/com_akeebasubs/assets/akpayment.php';
if(!$akpaymentinclude) { unset($akpaymentinclude); return; } else { unset($akpaymentinclude); }

class plgAkpaymentiDEAL extends plgAkpaymentAbstract
{
	protected $ppName = 'iDEAL';
	protected $ppKey = 'PLG_AKPAYMENT_IDEAL_TITLE';

	public function __construct(&$subject, $config = array())
	{
		$config = array_merge($config, array(
			'ppName'		=> 'iDEAL',
			'ppKey'			=> 'PLG_AKPAYMENT_IDEAL_TITLE',
			'ppImage'		=> JURI::root().'components/com_ccidealplatform/assets/images/ideal_medium.jpg'
		));

		parent::__construct($subject, $config);

		require_once JPATH_ADMINISTRATOR.'/components/com_akeebasubs/helpers/cparams.php';

		// Load the language files
		$jlang = JFactory::getLanguage();
		$jlang->load('plg_akpayment_ideal', JPATH_ADMINISTRATOR, 'en-GB', true);
		$jlang->load('plg_akpayment_ideal', JPATH_ADMINISTRATOR, $jlang->getDefault(), true);
		$jlang->load('plg_akpayment_ideal', JPATH_ADMINISTRATOR, null, true);
	}	

	/**
	 * Returns the payment form to be submitted by the user's browser. The form must have an ID of
	 * "paymentForm" and a visible submit button.
	 *
	 * @param string $paymentmethod
	 * @param JUser $user
	 * @param AkeebasubsTableLevel $level
	 * @param AkeebasubsTableSubscription $subscription
	 * @return string
	 */	
	public function onAKPaymentNew($paymentmethod, $user, $level, $subscription)
	{
		if($paymentmethod != $this->ppName) return false;		

		$slug = F0FModel::getTmpInstance('Levels','AkeebasubsModel')
				->setId($subscription->akeebasubs_level_id)
				->getItem()
				->slug;

		$rootURL = rtrim(JURI::base(),'/');
		$subpathURL = JURI::base(true);
		if(!empty($subpathURL) && ($subpathURL != '/')) {
			$rootURL = substr($rootURL, 0, -1 * strlen($subpathURL));
		}

		$data = (object)array(
			'url'			=> $this->getPaymentURL()

		);

		$kuser = F0FModel::getTmpInstance('Users','AkeebasubsModel')
			->user_id($user->id)
			->getFirstItem();

		@ob_start();
		include dirname(__FILE__).'/iDEAL/form.php';
		$html = @ob_get_clean();

		return $html;	
	}
 
	public function onAKPaymentCallback($paymentmethod,$data1)
	{
		 jimport('joomla.utilities.date'); 
		$data1=$data1['0'];
		// Check if we're supposed to handle this
		
		 if($data1['payment_method'] != $this->ppName) return false;
		
		//The necessary code to update a subscription is this:
		if(!defined('FOF_INCLUDED')) {
			include_once JPATH_LIBRARIES.'/fof/include.php';
		}		
		
		$date=date("Ymm");
		$order_id=str_replace("$date","",$data1['trx_ids']);		
		
		$subscription = F0FModel::getTmpInstance('Subscriptions','AkeebasubsModel')->setId($order_id)->getItem();
		
		$updates = array(
			'akeebasubs_subscription_id'				=> $order_id,
			'processor_key'		=> $data1['processor_key'],
			'state'				=> $data1['state'],
			'enabled'			=> 0
		);
		JLoader::import('joomla.utilities.date');
		if($data1['state'] == 'C') {
			$this->fixDates($subscription, $updates);
		}
		 
		$subscription->save($updates);
		 
	}
	
	/**
	 * Gets the form action URL for the payment
	 */
	private function getPaymentURL()
	{
			return JURI::root().'index.php?option=com_ccidealplatform&task=bankform';
	}

	private function _toPPDuration($days)
	{
		$ret = (object)array(
			'unit'		=> 'D',
			'value'		=> $days
		);

		// 0-90 => return days
		if($days < 90) return $ret;

		// Translate to weeks, months and years
		$weeks = (int)($days / 7);
		$months = (int)($days / 30);
		$years = (int)($days / 365);

		// Find which one is the closest match
		$deltaW = abs($days - $weeks*7);
		$deltaM = abs($days - $months*30);
		$deltaY = abs($days - $years*365);
		$minDelta = min($deltaW, $deltaM, $deltaY);

		// Counting weeks gives a better approximation
		if($minDelta == $deltaW) {
			$ret->unit = 'W';
			$ret->value = $weeks;

			// Make sure we have 1-52 weeks, otherwise go for a months or years
			if(($ret->value > 0) && ($ret->value <= 52)) {
				return $ret;
			} else {
				$minDelta = min($deltaM, $deltaY);
			}
		}

		// Counting months gives a better approximation
		if($minDelta == $deltaM) {
			$ret->unit = 'M';
			$ret->value = $months;

			// Make sure we have 1-24 month, otherwise go for years
			if(($ret->value > 0) && ($ret->value <= 24)) {
				return $ret;
			} else {
				$minDelta = min($deltaM, $deltaY);
			}
		}

		// If we're here, we're better off translating to years
		$ret->unit = 'Y';
		$ret->value = $years;

		if($ret->value < 0) {
			// Too short? Make it 1 (should never happen)
			$ret->value = 1;
		} elseif($ret->value > 5) {
			// One major pitfall. You can't have renewal periods over 5 years.
			$ret->value = 5;
		}

		return $ret;
	}
	 
}