<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

defined('_JEXEC') or die('Direct Access to ' . basename(__FILE__) . ' is not allowed.');

if (!class_exists('vmPSPlugin'))
require(JPATH_VM_PLUGINS . DS . 'vmpsplugin.php');

$lang = JFactory::getLanguage();

$extension = 'plg_vmpayment20_iDEAL';
$base_dir = JPATH_ADMINISTRATOR;
$language_tag = $lang->getTag();
$lang->load($extension, $base_dir, $language_tag, true);

class plgVMPaymentplg_virtuemart20_ideal extends vmPSPlugin {

	// instance of class
	public static $_this = false;

	function __construct(& $subject, $config) {
		
		parent::__construct($subject, $config);

		$this->_loggable = true;
		$this->tableFields = array_keys($this->getTableSQLFields());
		$this->_tablepkey = 'id'; //virtuemart_ideal_id';
		$this->_tableId = 'id'; //'virtuemart_ideal_id';
		$varsToPush = array('ideal_merchant_email' => array('', 'char'),
	    'ideal_verified_only' => array('', 'int'),
	    'payment_currency' => array('', 'int'),
	    'payment_logos' => array('', 'char'),
	    'debug' => array(0, 'int'),
	    'status_pending' => array('', 'char'),
	    'status_success' => array('', 'char'),
	    'status_canceled' => array('', 'char'),
	    'countries' => array('', 'char'),
	    'min_amount' => array('', 'int'),
	    'max_amount' => array('', 'int'),
	    'secure_post' => array('', 'int'),
	    'ipn_test' => array('', 'int'),
	    'no_shipping' => array('', 'int'),
	    'address_override' => array('', 'int'),
	    'cost_per_transaction' => array('', 'int'),
	    'cost_percent_total' => array('', 'int'),
	    'tax_id' => array(0, 'int'),
		'ideal_paymentmethod' => array('', 'char')
		);

		$this->setConfigParameterable($this->_configTableFieldName, $varsToPush);	
	}

	public function getVmPluginCreateTableSQL() {

		return $this->createTableSQL('Payment iDEAL Table');
	}

	function getTableSQLFields() {

		$SQLfields = array(
	    'id' => ' INT(11) unsigned NOT NULL AUTO_INCREMENT ',
	    'virtuemart_order_id' => ' int(1) UNSIGNED DEFAULT NULL',
	    'order_number' => ' char(32) DEFAULT NULL',
	    'virtuemart_paymentmethod_id' => ' mediumint(1) UNSIGNED DEFAULT NULL',
	    'payment_name' => 'varchar(5000)',
	    'payment_order_total' => 'decimal(15,5) NOT NULL DEFAULT \'0.00000\' ',
	    'payment_currency' => 'char(3) ',
	    'cost_per_transaction' => ' decimal(10,2) DEFAULT NULL ',
	    'cost_percent_total' => ' decimal(10,2) DEFAULT NULL ',
	    'tax_id' => ' smallint(1) DEFAULT NULL'
		);
		return $SQLfields;
	}

	function plgVmConfirmedOrder($cart, $order) {

		if (!($method = $this->getVmPluginMethod($order['details']['BT']->virtuemart_paymentmethod_id))) {
			return null; // Another method was selected, do nothing
		}
		if (!$this->selectedThisElement($method->payment_element)) {
			return false;
		}
		$session = JFactory::getSession();
		$return_context = $session->getId();
		$this->_debug = $method->debug;
		$this->logInfo('plgVmConfirmedOrder order number: ' . $order['details']['BT']->order_number, 'message');

		if (!class_exists('VirtueMartModelOrders'))
		require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );
		if (!class_exists('VirtueMartModelCurrency'))
		require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'currency.php');
		
		$new_status = '';

		$usrBT = $order['details']['BT'];
		$address = ((isset($order['details']['ST'])) ? $order['details']['ST'] : $order['details']['BT']);

		if (!class_exists('TableVendors'))
		require(JPATH_VM_ADMINISTRATOR . DS . 'table' . DS . 'vendors.php');
		$vendorModel = VmModel::getModel('Vendor');
		$vendorModel->setId(1);
		$vendor = $vendorModel->getVendor();
		$vendorModel->addImages($vendor, 1);
		$this->getPaymentCurrency($method);
		$q = 'SELECT `currency_code_3` FROM `#__virtuemart_currencies` WHERE `virtuemart_currency_id`="' . $method->payment_currency . '" ';
		$db = &JFactory::getDBO();
		$db->setQuery($q);
		$currency_code_3 = $db->loadResult();

		$paymentCurrency = CurrencyDisplay::getInstance($method->payment_currency);
		$totalInPaymentCurrency = round($paymentCurrency->convertCurrencyTo($method->payment_currency, $order['details']['BT']->order_total, false), 2);
		$cd = CurrencyDisplay::getInstance($cart->pricesCurrency);

		$testReq = $method->debug == 1 ? 'YES' : 'NO';
		$post_variables = Array(
	    'cmd' => '_ext-enter',
	    'redirect_cmd' => '_xclick',
	    'upload' => '1', //Indicates the use of third-party shopping cart
	    'order_number' => $order['details']['BT']->order_number,
	    "invoice" => $order['details']['BT']->order_number,
	    'custom' => $return_context,
	    'item_name' => JText::_('VMPAYMENT_IDEAL_ORDER_NUMBER') . ': ' . $order['details']['BT']->order_number,
	    "amount" => $totalInPaymentCurrency,
	    "currency_code" => $currency_code_3,
		/*
		 * 1 â€“ L'adresse spÃ©cifiÃ©e dans les variables prÃ©-remplies remplace l'adresse de livraison enregistrÃ©e auprÃ¨s de PayPal.
		* Le payeur voit l'adresse qui est transmise mais ne peut pas la modifier.
		* Aucune adresse n'est affichÃ©e si l'adresse n'est pas valable
		* (par exemple si des champs requis, tel que le pays, sont manquants) ou pas incluse.
		* Valeurs autorisÃ©es : 0, 1. Valeur par dÃ©faut : 0
		*/
	    "address_override" => isset($method->address_override) ? $method->address_override : 0, // 0 ??   iDEAL does not allow your country of residence to ship to the country you wish to
	    "first_name" => $address->first_name,
	    "last_name" => $address->last_name,
	    "address1" => $address->address_1,
	    "address2" => isset($address->address_2) ? $address->address_2 : '',
	    "zip" => $address->zip,
	    "city" => $address->city,
	    "state" => isset($address->virtuemart_state_id) ? ShopFunctions::getStateByID($address->virtuemart_state_id) : '',
	    "country" => ShopFunctions::getCountryByID($address->virtuemart_country_id, 'country_3_code'),
	    "email" => $order['details']['BT']->email,
	    "night_phone_b" => $address->phone_1,
	    "return" => JROUTE::_(JURI::root() . 'index.php?option=com_virtuemart&view=pluginresponse&task=pluginresponsereceived&on=' . $order['details']['BT']->order_number . '&pm=' . $order['details']['BT']->virtuemart_paymentmethod_id),		
	    "notify_url" => JROUTE::_(JURI::root() . 'index.php?option=com_virtuemart&view=pluginresponse&task=pluginnotification&tmpl=component'),
	    "cancel_return" => JROUTE::_(JURI::root() . 'index.php?option=com_virtuemart&view=pluginresponse&task=pluginUserPaymentCancel&on=' . $order['details']['BT']->order_number . '&pm=' . $order['details']['BT']->virtuemart_paymentmethod_id),		
	    "image_url" => JURI::root() . $vendor->images[0]->file_url,
	    "no_shipping" => isset($method->no_shipping) ? $method->no_shipping : 0,
	    "no_note" => "1");

		// Prepare data that should be stored in the database
		$dbValues['order_number'] = $order['details']['BT']->order_number;
		$dbValues['payment_name'] = $this->renderPluginName($method, $order);
		$dbValues['virtuemart_paymentmethod_id'] = $cart->virtuemart_paymentmethod_id;
		$dbValues['ideal_custom'] = $return_context;
		$dbValues['cost_per_transaction'] = $method->cost_per_transaction;
		$dbValues['cost_percent_total'] = $method->cost_percent_total;
		$dbValues['payment_currency'] = $method->payment_currency;
		$dbValues['payment_order_total'] = $totalInPaymentCurrency;
		$dbValues['tax_id'] = $method->tax_id;
		$this->storePSPluginInternalData($dbValues);

		$url = $this->_getiDEALUrlHttps($method);

		 $post_variables = Array(
				    'ordernumber' => $order['details']['BT']->order_number,
				    'extn' => JText::_('virtuemart') ,
				    "grandtotal" => $totalInPaymentCurrency,
					"ideal_paymentmethod" => $method->ideal_paymentmethod,
			);

		// add spin image
		$html = '<html><head><title>Redirection</title></head><body><div style="margin: auto; text-align: center;">';
		$html .= '<form action="' . $url . '" method="post" name="vm_ideal_form" >';
		$html.= '<input type="submit"  value="'.JTEXT::_('VMPAYMENT_IDEAL_PLEASE_WAIT').'" />';
		foreach ($post_variables as $name => $value) {
			$html.= '<input type="hidden" name="' . $name . '" value="' . htmlspecialchars($value) . '" />';
		}
		//Created by Gopi
		if(empty($_SESSION['iDEALVMPayment_SESSION'])){
			$_SESSION['iDEALVMPayment_SESSION']=date("yidmYi");
			$html.= '<input type="hidden" name="iDEALVMPayment_SESSION" value="' . $_SESSION['iDEALVMPayment_SESSION'] . '" />';
		}else{
			$_SESSION['iDEALVMPayment_SESSION']="";
			$_SESSION['iDEALVMPayment_SESSION']=date("yidmYi");
			$html.= '<input type="hidden" name="iDEALVMPayment_SESSION" value="' . $_SESSION['iDEALVMPayment_SESSION'] . '" />';
		}
		$html.= '<input type="hidden" name="payment_custom" value="' . $cart->virtuemart_paymentmethod_id . '" />';
		
		$html.= '</form></div>';
		$html.= ' <script type="text/javascript">';
		$html.= ' document.vm_ideal_form.submit();';
		$html.= ' </script></body></html>';		
		
		$db = JFactory::getDBO();
		$query = "SELECT IDEAL_Bank FROM #__ccidealplatform_config" ;
		$db->setQuery($query);
		$ideal_config = $db->loadResult();
		
		$modelOrder = VmModel::getModel('orders');		
		$method = $this->getVmPluginMethod ($cart->virtuemart_paymentmethod_id);	
		
		if(($ideal_config == "ABNAMROTEST") || ($ideal_config == "POSTBASIC") || ($ideal_config == "RABOBANKTEST")){
			
			$modelOrder = VmModel::getModel ('orders');
			$order['order_status'] = $method->status_pending;
			$order['customer_notified'] = 0;
			//$order['customer_notified'] = 1;
			$order['comments'] = '';
			$modelOrder->updateStatusForOneOrder ($order['details']['BT']->virtuemart_order_id, $order, TRUE);
		}


		// 	2 = don't delete the cart, don't send email and don't redirect
		//return $this->processConfirmedOrderPaymentResponse(2, $cart, $order, $html, $dbValues['payment_name'], $new_status);

		// 	2 = don't delete the cart, don't send email and don't redirect
		$cart->_confirmDone = false;
		$cart->_dataValidated = false;
		$cart->setCartIntoSession();
		JRequest::setVar('html', $html);
		
	}

	function plgVmgetPaymentCurrency($virtuemart_paymentmethod_id, &$paymentCurrencyId) {

		if (!($method = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
			return null; // Another method was selected, do nothing
		}
		if (!$this->selectedThisElement($method->payment_element)) {
			return false;
		}
		$this->getPaymentCurrency($method);
		$paymentCurrencyId = $method->payment_currency;
	}

	function plgVmOnPaymentResponseReceived(&$html) {
		
		if (!class_exists('VirtueMartCart'))
		require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');
		if (!class_exists('shopFunctionsF'))
		require(JPATH_VM_SITE . DS . 'helpers' . DS . 'shopfunctionsf.php');
		if (!class_exists('VirtueMartModelOrders'))
		require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );

		// the payment itself should send the parameter needed.
		$lang = JFactory::getLanguage();
		$lite = JRequest::getVar('lite');
		$virtuemart_paymentmethod_id = JRequest::getInt('pm', 0);
		$order_number = JRequest::getVar('on', 0);
		$vendorId = 0;
		if (!($method = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
			return null; // Another method was selected, do nothing
		}
		if (!$this->selectedThisElement($method->payment_element)) {
			return false;
		}


		$ideal_data = JRequest::get('post');
		$modelOrder = VmModel::getModel('orders');
		$payment_name = $this->renderPluginName($method);

		if($method->payment_element!="plg_virtuemart20_ideal"){
			return false;
		}
		
		//validation for LITE BANK by Gopi.A
			if($lite){
				$ideal_data['payment_status'] = 'Pending';
			}else{
				$ideal_data['payment_status'] = 'Completed';
			}

		if(empty($ideal_data) || $ideal_data==""){

			$ideal_data = array();
			$ideal_data['invoice'] = $order_number;
			$ideal_data['custom']  =  $ideal_data['custom'];

			//validation for LITE BANK by Gopi.A
			if($lite){
				$ideal_data['payment_status'] = 'Pending';
			}else{
				$ideal_data['payment_status'] = 'Completed';
			}
		}

		vmdebug('iDEAL plgVmOnPaymentResponseReceived', $ideal_data);				
		
		if (!($virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($order_number) )) {
				return null;
			}
			if (!($paymentTable = $this->getDataByOrderId($virtuemart_order_id) )) {
				// JError::raiseWarning(500, $db->getErrorMsg());
				return '';
			}
			$payment_name = $this->renderPluginName($method);
			$html = $this->_getPaymentResponseHtml($paymentTable, $payment_name);

		if (strcmp($ideal_data['payment_status'], 'Completed') == 0) {
			$order['order_status'] = $method->status_success;
			$order['comments'] = JText::sprintf('VMPAYMENT_IDEAL_PAYMENT_STATUS_CONFIRMED', $order_number);
		} elseif (strcmp($ideal_data['payment_status'], 'Pending') == 0) {
			$key = 'VMPAYMENT_IDEAL_PENDING_REASON_FE_' . strtoupper(@$ideal_data['pending_reason']);
			if (!$lang->hasKey($key)) {
			$key = 'VMPAYMENT_IDEAL_PENDING_REASON_FE_DEFAULT';
			}
			$order['comments'] = JText::sprintf('VMPAYMENT_IDEAL_PAYMENT_STATUS_PENDING', $order_number) . JText::_($key);
			$order['order_status'] = $method->status_pending;
		} else {
			$order['order_status'] = $method->status_canceled;
		}
		$order['customer_notified'] = 1;
		$order['details']['BT']->virtuemart_order_id = $order_number;	
		
		if($_SESSION['iDEALVMStatus_SESSION']){
			$db = JFactory::getDBO();
			$query = "SELECT IDEAL_Bank FROM #__ccidealplatform_config" ;
			$db->setQuery($query);
			$ideal_config = $db->loadResult();
			
			if(($ideal_config != "ABNAMROTEST") && ($ideal_config != "POSTBASIC") && ($ideal_config != "RABOBANKTEST")){
				$modelOrder->updateStatusForOneOrder($virtuemart_order_id, $order, true);
			}

			//We delete the old stuff
			// get the correct cart / session
			unset($_SESSION['iDEALVMStatus_SESSION']);
			$cart = VirtueMartCart::getCart();
			$cart->emptyCart();			
					
		}else{		 
			$order_number = JRequest::getVar('on', 0);
			$vm_paymentmethod_id = JRequest::getInt('pm', 0);			
			$db = &JFactory::getDBO();
			
			$payName = "select `payment_element` from #__virtuemart_paymentmethods  where `virtuemart_paymentmethod_id`='$vm_paymentmethod_id'";
			$db->setQuery($payName);$getName = $db->loadResult();
			
			if($getName=="plg_virtuemart20_ideal"){			
				$status = "select `payment_status` from #__ccidealplatform_payments where order_id='$order_number'";
				$db->setQuery($status);
				$getState = $db->loadResult(); if($getState=='paid'){ $statuses = 'Completed'; } else { $statuses = $getState; }
				echo JTEXT::_("VMPAYMENT_STATUS_1").$order_number.JTEXT::_("VMPAYMENT_STATUS_2").$statuses;
			}
		}		
	}

	function plgVmOnUserPaymentCancel() {	
	
		$db = JFactory::getDBO();
		$query = "SELECT IDEAL_Bank FROM #__ccidealplatform_config" ;
		$db->setQuery($query);
		$ideal_config = $db->loadResult();

		if (!class_exists('VirtueMartModelOrders'))
		require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );

		$order_number = JRequest::getString('on', '');
		$virtuemart_paymentmethod_id = JRequest::getInt('pm', '');
		if (empty($order_number) or empty($virtuemart_paymentmethod_id) or !$this->selectedThisByMethodId($virtuemart_paymentmethod_id)) {
		    return null;
		}
		if (!($virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($order_number))) {
			return null;
		}
		if (!($paymentTable = $this->getDataByOrderId($virtuemart_order_id))) {
		    return null;
		}

		VmInfo(Jtext::_('VMPAYMENT_IDEAL_PAYMENT_CANCELLED'));
		
		$session = JFactory::getSession ();
		$return_context = $session->getId ();
		
		if(($ideal_config != "ABNAMROTEST") && ($ideal_config != "POSTBASIC") && ($ideal_config != "RABOBANKTEST")){	

			if (($return_context) && ($paymentTable)) {
				$this->handlePaymentUserCancel ($virtuemart_order_id); 
			}		

			$modelOrder = VmModel::getModel('orders');
			
			$payment = $this->getDataByOrderId($virtuemart_order_id);

			$method = $this->getVmPluginMethod($payment->virtuemart_paymentmethod_id);
			
			$order['order_status'] = $method->status_canceled;
			
			$order['customer_notified'] = 1;
			$order['comments'] = '';
			$order['details']['BT']->virtuemart_order_id = $order_number;	
		
			$modelOrder->updateStatusForOneOrder($virtuemart_order_id, $order, true);			
		}
		
		return true;
	}

	/*
	 *   plgVmOnPaymentNotification() - This event is fired by Offline Payment. It can be used to validate the payment data as entered by the user.
	* Return:
	* Parameters:
	*  None
	*  @author Valerie Isaksen
	*/

	function plgVmOnPaymentNotification() {

		if (!class_exists('VirtueMartModelOrders'))
		require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );
		$ideal_data = JRequest::get('post');
		if (!isset($ideal_data['invoice'])) {
			return;
		}
		$order_number = $ideal_data['invoice'];
		$virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($ideal_data['invoice']);		

		if (!$virtuemart_order_id) {
			return;
		}
		$vendorId = 0;
		$payment = $this->getDataByOrderId($virtuemart_order_id);

		$method = $this->getVmPluginMethod($payment->virtuemart_paymentmethod_id);
		if (!$this->selectedThisElement($method->payment_element)) {
			return false;
		}

		$this->_debug = $method->debug;
		if (!$payment) {
			$this->logInfo('getDataByOrderId payment not found: exit ', 'ERROR');
			return null;
		}
		$this->logInfo('ideal_data ' . implode('   ', $ideal_data), 'message');

		$this->_storeiDEALInternalData($method, $ideal_data, $virtuemart_order_id);

		$error_msg = $this->_processIPN($ideal_data, $method, $virtuemart_order_id);
		$this->logInfo('process IPN ' . $error_msg, 'message');
		if (!(empty($error_msg) )) {
			$new_status = $method->status_canceled;
			$this->logInfo('process IPN ' . $error_msg . ' ' . $new_status, 'ERROR');
		} else {
			$this->logInfo('process IPN OK', 'message');
		}
		/*
		 * The status of the payment:
		 * Canceled_Reversal: A reversal has been canceled. For example, you won a dispute with the customer, and the funds for the transaction that was reversed have been returned to you.
		 * Completed: The payment has been completed, and the funds have been added successfully to your account balance.
		 * Created: A German ELV payment is made using Express Checkout.
		 * Denied: You denied the payment. This happens only if the payment was previously pending because of possible reasons described for the pending_reason variable or the Fraud_Management_Filters_x variable.
		 * Expired: This authorization has expired and cannot be captured.
		 * Failed: The payment has failed. This happens only if the payment was made from your customerâ€™s bank account.
		 * Pending: The payment is pending. See pending_reason for more information.
		 * Refunded: You refunded the payment.
		 * Reversed: A payment was reversed due to a chargeback or other type of reversal. The funds have been removed from your account balance and returned to the buyer. The reason for the reversal is specified in the ReasonCode element.
		 * Processed: A payment has been accepted.
		 * Voided: This authorization has been voided.
		 *
		 */	 
		

		$new_status = $this->_getPaymentStatus($method, $ideal_data['payment_status']);

		$this->logInfo('plgVmOnPaymentNotification return new_status:' . $new_status, 'message');

		$modelOrder = VmModel::getModel('orders');

		$order = array();
		$order['order_status'] = $new_status;
		$order['customer_notified'] =1;
		$order['comments'] = JText::sprintf('VMPAYMENT_IDEAL_PAYMENT_STATUS_CONFIRMED', $order_number);

		 if (strcmp($ideal_data['payment_status'], 'Completed') == 0) {
			    $order['order_status'] = $method->status_success;
			    $order['comments'] = JText::sprintf('VMPAYMENT_IDEAL_PAYMENT_STATUS_CONFIRMED', $order_number);
			} elseif (strcmp($ideal_data['payment_status'], 'Pending') == 0) {
			    $key = 'VMPAYMENT_PAYPAL_PENDING_REASON_FE_' . strtoupper($ideal_data['pending_reason']);
			    if (!$lang->hasKey($key)) {
				$key = 'VMPAYMENT_PAYPAL_PENDING_REASON_FE_DEFAULT';
			    }
			    $order['comments'] = JText::sprintf('VMPAYMENT_PAYPAL_PAYMENT_STATUS_PENDING', $order_number) . JText::_($key);
			    $order['order_status'] = $method->status_pending;
			} else {
			    $order['order_status'] = $method->status_canceled;
			}

		$modelOrder->updateStatusForOneOrder($virtuemart_order_id, $order, true);		

		//// remove vmcart
		$this->emptyCart($return_context);		
	}

	function _storeiDEALInternalData($method, $ideal_data, $virtuemart_order_id) {

		// get all know columns of the table
		$db = JFactory::getDBO();
		$query = 'SHOW COLUMNS FROM `' . $this->_tablename . '` ';
		$db->setQuery($query);
		$columns = $db->loadResultArray(0);
		$post_msg = '';
		foreach ($ideal_data as $key => $value) {
			$post_msg .= $key . "=" . $value . "<br />";
			$table_key = 'ideal_response_' . $key;
			if (in_array($table_key, $columns)) {
				$response_fields[$table_key] = $value;
			}
		}
		
		$response_fields['payment_name'] = $this->renderPluginName($method);
		$response_fields['order_number'] = $ideal_data['invoice'];
		$response_fields['virtuemart_order_id'] = $virtuemart_order_id;		
		$this->storePSPluginInternalData($response_fields, 'virtuemart_order_id', true);
	}

	function _getTablepkeyValue($virtuemart_order_id) {
		$db = JFactory::getDBO();
		$q = 'SELECT ' . $this->_tablepkey . ' FROM `' . $this->_tablename . '` '
		. 'WHERE `virtuemart_order_id` = ' . $virtuemart_order_id;
		$db->setQuery($q);

		if (!($pkey = $db->loadResult())) {
			JError::raiseWarning(500, $db->getErrorMsg());
			return '';
		}
		return $pkey;
	}

	function _getPaymentStatus($method, $ideal_status) {
		$new_status = '';
		if (strcmp($ideal_status, 'Completed') == 0) {
			$new_status = $method->status_success;
		} elseif (strcmp($ideal_status, 'Pending') == 0) {
			$new_status = $method->status_pending;
		} else {
			$new_status = $method->status_canceled;
		}
		return $new_status;
	}

	/**
	 * Display stored payment data for an order
	 * @see components/com_virtuemart/helpers/vmPSPlugin::plgVmOnShowOrderBEPayment()
	 */
	function plgVmOnShowOrderBEPayment($virtuemart_order_id, $payment_method_id) {

		if (!$this->selectedThisByMethodId($payment_method_id)) {
			return null; // Another method was selected, do nothing
		}


		if (!($paymentTable = $this->_getiDEALInternalData($virtuemart_order_id) )) {			
			return '';
		}
		$this->getPaymentCurrency($paymentTable);
		$q = 'SELECT `currency_code_3` FROM `#__virtuemart_currencies` WHERE `virtuemart_currency_id`="' . $paymentTable->payment_currency . '" ';
		$db = &JFactory::getDBO();
		$db->setQuery($q);
		$currency_code_3 = $db->loadResult();
		$html = '<table class="adminlist">' . "\n";
		$html .=$this->getHtmlHeaderBE();
		$html .= $this->getHtmlRowBE('IDEAL_PAYMENT_NAME', $paymentTable->payment_name);		
		$code = "ideal_response_";
		foreach ($paymentTable as $key => $value) {
			if (substr($key, 0, strlen($code)) == $code) {
				$html .= $this->getHtmlRowBE($key, $value);
			}
		}
		$html .= '</table>' . "\n";
		return $html;
	}

	function _getiDEALInternalData($virtuemart_order_id, $order_number='') {
		$db = JFactory::getDBO();
		$q = 'SELECT * FROM `' . $this->_tablename . '` WHERE ';
		if ($order_number) {
			$q .= " `order_number` = '" . $order_number . "'";
		} else {
			$q .= ' `virtuemart_order_id` = ' . $virtuemart_order_id;
		}

		$db->setQuery($q);
		if (!($paymentTable = $db->loadObject())) {			
			return '';
		}
		return $paymentTable;
	}
	
	function _getiDEALUrl($method) {

		$url = JURI::root().'index.php?option=com_ccidealplatform&task=bankform';

		return $url;
	}

	function _getiDEALUrlHttps($method) {
		$url = $this->_getiDEALUrl($method);

		return $url;
	}

	function _getPaymentResponseHtml($idealTable, $payment_name) {

		$html = '<table>' . "\n";
		$html .= $this->getHtmlRow('IDEAL_PAYMENT_NAME', $payment_name);
		if (!empty($idealTable)) {
			$html .= $this->getHtmlRow('IDEAL_ORDER_NUMBER', $idealTable->order_number);			
		}
		$html .= '</table>' . "\n";

		return $html;
	}

	function getCosts(VirtueMartCart $cart, $method, $cart_prices) {
		if (preg_match('/%$/', $method->cost_percent_total)) {
			$cost_percent_total = substr($method->cost_percent_total, 0, -1);
		} else {
			$cost_percent_total = $method->cost_percent_total;
		}
		return ($method->cost_per_transaction + ($cart_prices['salesPrice'] * $cost_percent_total * 0.01));
	}

	/**
	 * Check if the payment conditions are fulfilled for this payment method
	 * @author: Valerie Isaksen
	 *
	 * @param $cart_prices: cart prices
	 * @param $payment
	 * @return true: if the conditions are fulfilled, false otherwise
	 *
	 */
	protected function checkConditions($cart, $method, $cart_prices) {

		$this->convert_condition_amount($method);
		$amount = $this->getCartAmount($cart_prices);
		$address = (($cart->ST == 0) ? $cart->BT : $cart->ST);
		
 		$amount_cond = ($amount >= $method->min_amount AND $amount <= $method->max_amount
			OR
			($method->min_amount <= $amount AND ($method->max_amount == 0)));
		if (!$amount_cond) {
			return FALSE;
		}
		$countries = array();
		if (!empty($method->countries)) {
			if (!is_array ($method->countries)) {
				$countries[0] = $method->countries;
			} else {
				$countries = $method->countries;
			}
		}

		// probably did not gave his BT:ST address
		if (!is_array ($address)) {
			$address = array();
			$address['virtuemart_country_id'] = 0;
		}

		if (!isset($address['virtuemart_country_id'])) {
			$address['virtuemart_country_id'] = 0;
		}
		if (count ($countries) == 0 || in_array ($address['virtuemart_country_id'], $countries) ) {
			return TRUE;
		}

		return FALSE;	
	}

	/**
	 * We must reimplement this triggers for joomla 1.7
	 */

	/**
	 * Create the table for this plugin if it does not yet exist.
	 * This functions checks if the called plugin is active one.
	 * When yes it is calling the standard method to create the tables
	 * @author ValÃ©rie Isaksen
	 *
	 */
	function plgVmOnStoreInstallPaymentPluginTable($jplugin_id) {

		return $this->onStoreInstallPluginTable($jplugin_id);
	}

	/**
	 * This event is fired after the payment method has been selected. It can be used to store
	 * additional payment info in the cart.
	 *
	 * @author Max Milbers
	 * @author ValÃ©rie isaksen
	 *
	 * @param VirtueMartCart $cart: the actual cart
	 * @return null if the payment was not selected, true if the data is valid, error message if the data is not vlaid
	 *
	 */
	public function plgVmOnSelectCheckPayment(VirtueMartCart $cart) {
		return $this->OnSelectCheck($cart);
	}

	/**
	 * plgVmDisplayListFEPayment
	 * This event is fired to display the pluginmethods in the cart (edit shipment/payment) for exampel
	 *
	 * @param object $cart Cart object
	 * @param integer $selected ID of the method selected
	 * @return boolean True on succes, false on failures, null when this plugin was not selected.
	 * On errors, JError::raiseWarning (or JError::raiseError) must be used to set a message.
	 *
	 * @author Valerie Isaksen
	 * @author Max Milbers
	 */
	public function plgVmDisplayListFEPayment(VirtueMartCart $cart, $selected = 0, &$htmlIn) {
		return $this->displayListFE($cart, $selected, $htmlIn);
	}

	/*
	 * plgVmonSelectedCalculatePricePayment
	* Calculate the price (value, tax_id) of the selected method
	* It is called by the calculator
	* This function does NOT to be reimplemented. If not reimplemented, then the default values from this function are taken.
	* @author Valerie Isaksen
	* @cart: VirtueMartCart the current cart
	* @cart_prices: array the new cart prices
	* @return null if the method was not selected, false if the shiiping rate is not valid any more, true otherwise
	*
	*
	*/

	public function plgVmonSelectedCalculatePricePayment(VirtueMartCart $cart, array &$cart_prices, &$cart_prices_name) {
		return $this->onSelectedCalculatePrice($cart, $cart_prices, $cart_prices_name);
	}

	/**
	 * plgVmOnCheckAutomaticSelectedPayment
	 * Checks how many plugins are available. If only one, the user will not have the choice. Enter edit_xxx page
	 * The plugin must check first if it is the correct type
	 * @author Valerie Isaksen
	 * @param VirtueMartCart cart: the cart object
	 * @return null if no plugin was found, 0 if more then one plugin was found,  virtuemart_xxx_id if only one plugin is found
	 *
	 */
	function plgVmOnCheckAutomaticSelectedPayment(VirtueMartCart $cart, array $cart_prices = array()) {
		return $this->onCheckAutomaticSelected($cart, $cart_prices);
	}

	/**
	 * This method is fired when showing the order details in the frontend.
	 * It displays the method-specific data.
	 *
	 * @param integer $order_id The order ID
	 * @return mixed Null for methods that aren't active, text (HTML) otherwise
	 * @author Max Milbers
	 * @author Valerie Isaksen
	 */
	public function plgVmOnShowOrderFEPayment($virtuemart_order_id, $virtuemart_paymentmethod_id, &$payment_name) {
		$this->onShowOrderFE($virtuemart_order_id, $virtuemart_paymentmethod_id, $payment_name);
	}

	/**
	 * This event is fired during the checkout process. It can be used to validate the
	 * method data as entered by the user.
	 *
	 * @return boolean True when the data was valid, false otherwise. If the plugin is not activated, it should return null.
	 * @author Max Milbers

	 public function plgVmOnCheckoutCheckDataPayment($psType, VirtueMartCart $cart) {
	 return null;
	 }
	 */

	/**
	 * This method is fired when showing when priting an Order
	 * It displays the the payment method-specific data.
	 *
	 * @param integer $_virtuemart_order_id The order ID
	 * @param integer $method_id  method used for this order
	 * @return mixed Null when for payment methods that were not selected, text (HTML) otherwise
	 * @author Valerie Isaksen
	 */
	function plgVmonShowOrderPrintPayment($order_number, $method_id) {
		return $this->onShowOrderPrint($order_number, $method_id);
	}
	
	function plgVmDeclarePluginParamsPayment($name, $id, &$data) {
		return $this->declarePluginParams('payment', $name, $id, $data);
	}

	function plgVmSetOnTablePluginParamsPayment($name, $id, &$table) {
		return $this->setOnTablePluginParams($name, $id, $table);
	}

}

// No closing tag