<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>  
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.  
* @license	GNU/GPL, see LICENSE.php for full license. 
**/

// No direct access allowed to this file
defined( '_JEXEC' ) or die( 'Restricted access' );

// Import Joomla! Plugin library file
jimport('joomla.plugin.plugin');
require_once (JPATH_ROOT.DS.'components'.DS.'com_simplecaddy'.DS.'simplecaddy.class.php'); // mandatory

//The Content plugin Loadmodule
class plgContentScideal extends JPlugin {
	var $debugshow="hidden"; // just for testing purposes
	var $_plugin_number	= 0;

	function plgContentScideal( &$subject, $config ) {
        parent::__construct( $subject, $config );
        $this->loadLanguage(); // necessary or not, let's make sure we get the language file
	}

	public function _setPluginNumber() {
		$this->_plugin_number = (int)$this->_plugin_number + 1; // only the first occurrence of the plugin should load css
	}

	function onContentPrepare($context, &$article, &$params, $page = 0 ) {
		if (!JComponentHelper::isEnabled('com_simplecaddy', true)) { // check for the component install
			echo "<div style='color:red;'>The SimpleCaddy component is not installed or is not enabled</div>";
			return;
		}

        $regex = '/{(scideal)\s*(.*?)}/i'; // the plugin code to get from content

        $parms=array();
        $matches = array();
        preg_match_all( $regex, $article->text, $matches, PREG_SET_ORDER );
        foreach ($matches as $elm) {
 			$this->_setPluginNumber();
 			if ($this->_plugin_number==1) { // get the stylesheet only ONCE per page
				JHTML::stylesheet('components/com_simplecaddy/css/simplecaddy.css' );				
 			}
 			if ($this->_plugin_number>1 ) { // there should be only ONE payment plugin per page...
	            $article->text = preg_replace($regex, JText::_("SC_EXTRA_PLUGINS_REMOVED"), $article->text, 1);
		 		return true;
 			}
			$line=str_replace("&nbsp;", " ", $elm[2]);
            $line=str_replace(" ", "&", $line);
            $line=strtolower($line);
            parse_str( $line, $parms );

            if (!isset($parms['type'])) { // no type provided, or just forgot...
            	$parms["type"]="checkout";
            }
        	// get all different types of display here, define manually to avoid hacking of the code
        	echo strtolower($parms['type']);
        	switch (strtolower($parms['type']) ) {

        		case "ipn":
        		case "paysuccess":
        			$html=$this->showIdealSuccess($parms);
        			break;
        		case "payfail":
        			$html=$this->showIdealFail($parms);
        			break;
       			case "checkout": // the default if nothing has been provided
                	$html=$this->showIdealscreen($parms);
                	break;
        		default: // anything else provides an error message
                	$html=JText::_("SC_THIS_PLUGIN_TYPE_NOT_SUPPORTED"). "({$parms['type']})";
        	}
            $article->text = preg_replace($regex, $html, $article->text, 1);
        }
        return true;
	}

	function showIdealScreen($parms) {

		$ordercode=JRequest::getVar("data"); // the data contains the ordercode when you finish the details page

		$orders=new orders();
		$orderid=$orders->getOrderIdFromCart($ordercode);
		$order=new order();
		$order->load($orderid);

		// add the details to the order
		$gtotal=0; //define the grand total
		$pptax=0; // define the tax for ideal
		$taxrate=0;

		$path=JURI::root()."components/com_ccidealplatform/assets/images/select_iDEAL.jpg";
		
		$html='<form id="idealform" action="index.php?option=com_ccidealplatform&task=bankform" method="post" target="_self" style="cursor: pointer;">';
			$html.='<img src="'.$path.'" onclick="sendToCCiDEal();" />';
			$html.='<input type="hidden" name="grandtotal" id="grandtotal" value="'.$order->total.'" />';
			$html.='<input type="hidden" name="ordernumber" id="ordernumber" value="'.$order->id.'"/>';
			$html.='<input type="hidden" name="extn" id="extn" value="caddy"/>';
		$html.='</form>';
		
		$html.='<script language="JavaScript" type="text/javascript">';
  		$html.='function sendToCCiDEal()';
  		$html.='{';
  			$html.='document.getElementById("idealform").submit();';
  		$html.='}';
		$html.='</script>';

		return $html;
	}

	function showIdealSuccess($parms) {

		// now send success email
		$em=new email();
		$em->mailorder($parms['order_number']);
		
		//Empty Cart
		$cart2=new cart2();
		$cart2->destroyCart();
	}

	function showIdealFail() {
		$html = "Payment failed";
		return $html;
	}
}