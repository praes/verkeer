<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

/**
 * Editor iDEAL buton
 *
 * @package Editors-xtd
 * @since 1.5
 */
class plgButtonIdeal_editor extends JPlugin
{
	/**
	 * Constructor
	 *
	 * For php4 compatability we must not use the __constructor as a constructor for plugins
	 * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
	 * This causes problems with cross-referencing necessary for the observer design pattern.
	 *
	 * @param 	object $subject The object to observe
	 * @param 	array  $config  An array that holds the plugin configuration
	 * @since 1.5
	 */
	function plgButtonIdeal_editor(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}

	/**
	 * readmore button
	 * @return array A two element array of ( imageName, textToInsert )
	 */
	function onDisplay($name)
	{
	
		$jversion = new JVersion();
		$current_version = $jversion->getShortVersion();		
		$jversion_sht=substr($current_version,0,1);

		global $mainframe;

		$app = JFactory::getApplication();
		$doc 		= JFactory::getDocument();
		$template 	= $app->getTemplate();

		// button is not active in specific content components

		$getContent = $this->_subject->getContent($name);
		$present = JText::_('ALREADY EXISTS', true) ;
		 $get_component = JRequest::getVar('option');
		 if($get_component=='com_content'){
		$js = "
			function insertcode(editor) {
				var content = $getContent
				if (content.match(/<hr\s+{=(\"|')ccidealplatform(\"|')\s*\/*>/i)) {
					alert('$present');
					return false;
				} else {
					jInsertEditorText('{ccidealplatform amount=\"15\"} <br />Dit is de meest minimale tag, met alleen maar een bedrag, super simpel he? Bekijk de handleiding voor inspiratie over andere mogelijkheden: <a href=\"http://www.chillcreations.com/super-simpel-ideal\">http://www.chillcreations.com/super-simpel-ideal</a>', editor);
				}
			}

			";

		$doc->addScriptDeclaration($js);
		
		if($jversion_sht == 2){		
			$css	= " .button2-left .Ideal_editor { background: url(".JURI::root()."components/com_ccidealplatform/assets/images/editor_button_ideal.jpg) 100% 0 no-repeat; } ";		
		}
		
		if($jversion_sht == 3){
			$css	= " .icon-Ideal_editor { background: url(".JURI::root()."components/com_ccidealplatform/assets/images/editor_button_ideal.jpg) 70% 40% no-repeat;width: 16px !important; } ";
		}
		
		
		
		
		$doc->addStyleDeclaration($css);
		$button = new JObject();
		$button->set('modal', false);
		$button->class = 'btn';
		$button->set('onclick', 'insertcode(\''.$name.'\');return false;');
		$button->set('text', JText::_('iDEAL'));
		$button->set('name', 'Ideal_editor');
		// TODO: The button writer needs to take into account the javascript directive
		//$button->set('link', 'javascript:void(0)');
		$button->set('link', '#');

		return $button;
		 }
	}
}