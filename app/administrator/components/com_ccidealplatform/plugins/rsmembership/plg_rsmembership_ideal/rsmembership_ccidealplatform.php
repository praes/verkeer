<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.plugin.plugin' );

if (file_exists(JPATH_ADMINISTRATOR.'/components/com_rsmembership/helpers/rsmembership.php')){
	require_once JPATH_ADMINISTRATOR.'/components/com_rsmembership/helpers/rsmembership.php';
}


class plgSystemRSMembership_cciDEALPlatform extends JPlugin
{
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);	
		
		if ($this->canRun() && $this->params) {
			$name = $this->params->get('payment_name','iDEAL');
			RSMembership::addPlugin($name, 'rsmembership_ccidealplatform'); 			
		}	
	}
	
	function canRun()
	{
		return file_exists(JPATH_ADMINISTRATOR.'/components/com_rsmembership/helpers/rsmembership.php');
	}

	function onMembershipPayment($plugin, $data, $extra, $membership, $transaction, $html)
	{
	
		if (!$this->canRun()) return;
		if ($plugin != 'rsmembership_ccidealplatform') return false;

		$db 	= JFactory::getDBO();
		$query	= $db->getQuery(true);
		
		$this->loadLanguage('plg_system_rsmembership_ccidealplatform', JPATH_ADMINISTRATOR);		

		$html 	= '';
		$html .= $this->params->get('details');
		$html .= '<br /><br /><br /><br />';
		$db = JFactory::getDBO();
		$select = "SELECT id FROM #__rsmembership_transactions ORDER BY id DESC ";
		$db->setQuery($select); 
		$id=$db->loadResult();
		$db->query();
		$ordernumber = $id+1;
		$date = date("Yd");
		$transaction->custom = md5($transaction->params.' '.time());		
		
			// No hash for this
		$transaction->hash = '';
 
		$tax_value = $this->params->get('tax_value');
		if (!empty($tax_value))
		{
			$tax_type = $this->params->get('tax_type');

			// percent ?
			if ($tax_type == 0)
			$tax_value = $transaction->price * ($tax_value / 100);

			$transaction->price = $transaction->price + $tax_value;
		}
		
		$html .= '
				<form action="index.php?option=com_ccidealplatform&task=bankform" id="idealform" method="post" target="_self">
				<input type="hidden" name="custom" id="custom" value="'.htmlentities($transaction->custom).'"/>
				<input type="hidden" name="grandtotal" id="grandtotal" value="'.$transaction->price.'" />
				<input type="hidden" name="ordernumber" id="ordernumber" value="'.$date.$ordernumber.'"/>
				<input type="hidden" name="extn" id="extn" value="rsmembership"/>
				</form>';
		$html .= '<script type="text/javascript">'."\n";
		$html .= 'var prodid = document.getElementById("ordernumber").value ;'."\n";
		$html .= 'if ( prodid) {'."\n";
		$html .= 'document.getElementById("idealform").submit(); }'."\n";
		$html .= '</script>'."\n";

		$html .= '<br /><br /><br /><br />'; 

		if ($membership->activation == 2)
			$transaction->status = 'completed';
			return $html;
	}
}