<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

defined('_JEXEC') or die();

class JFormFieldipaymethod extends JFormField{
	protected $type = 'ipaymethod';

	protected function getInput() { 
	
		$plugin 	= JPluginHelper::getPlugin('system', 'rsepro_ideal');
		$params 	= new JRegistry($plugin->params);
		$ipaymethod =  $params->get('ipaymethod','ideal');		

        $db =  JFactory::getDBO();
		
		$query = 'SELECT `IDEAL_Bank` FROM `#__ccidealplatform_config` WHERE `id` = 1';
		$db->setQuery($query);
        $IDEAL_Bank = $db->loadResult();		

        $query = 'SELECT `method_value` AS value, `method_name` AS text FROM `#__ccidealplatform_paymentmethod`
               		WHERE `account_id` = "'.$IDEAL_Bank.'" ORDER BY `id` ASC ';

        $db->setQuery($query);
        $fields = $db->loadObjectList();
        
        return JHTML::_('select.genericlist', $fields, 'jform[params][ipaymethod]', '', 'value', 'text', $ipaymethod);
    }

}