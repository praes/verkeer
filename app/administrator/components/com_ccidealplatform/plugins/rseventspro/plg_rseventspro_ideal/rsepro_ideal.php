<?php
/**
* @package	RSEvents!Pro iDEAL payment plugin for cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.plugin.plugin' );

class plgSystemRsepro_ideal extends JPlugin
{
	//set the value of the payment option
	var $rsprooption = 'iDeal';
	
	public function __construct( &$subject, $config ) {	
	
		parent::__construct( $subject, $config );
	}
	
	/*
	*	Is RSEvents!Pro installed
	*/
	
	protected function canRun() {
		$helper = JPATH_SITE.'/components/com_rseventspro/helpers/rseventspro.php';
		if (file_exists($helper)) {
			require_once $helper;
			JFactory::getLanguage()->load('plg_system_rsepro_ideal',JPATH_ADMINISTRATOR);
			
			return true;
		}
		
		return false;
	}
	
	/*
	*	Add the current payment option to the Payments List
	*/

	public function rsepro_addOptions() {
		$lang = JFactory::getLanguage();
		$lang->load('plg_system_rsepro_ideal',JPATH_ADMINISTRATOR);
	
		if ($this->canRun())
			return JHTML::_('select.option', $this->rsprooption, JText::_('RSEPRO_PLG_PLUGIN_IDEAL_NAME'));
		else return JHTML::_('select.option', '', '');
	}
	
	/*
	*	Add optional fields for the payment plugin. Example: Credit Card Number, etc.
	*	Please use the syntax <form method="post" action="index.php?option=com_rseventspro&task=process" name="paymentForm">
	*	The action provided in the form will actually run the rsepro_processForm() of your payment plugin.
	*/
	
	public function rsepro_showForm($vars) {
		$app 		= JFactory::getApplication();		
		$Itemid 	= JRequest::getVar('Itemid');
		$ipaymethod = $this->params->get('ipaymethod','ideal');
		if($app->getName() != 'site') return;
		
		//check to see if we can show something
		if (!$this->canRun()) return;
		
		if (isset($vars['method']) && $vars['method'] == $this->rsprooption) {
			JFactory::getLanguage()->load('com_rseventspro',JPATH_SITE);
		
			jimport('joomla.mail.helper');
			$db		= JFactory::getDbo();
			$query	= $db->getQuery(true);
			
			//is the plugin enabled ?
			$enable = JPluginHelper::isEnabled('system', 'rsepro_ideal');
			if (!$enable) return;
			
			$details = $vars['details'];			
			$tickets = $vars['tickets'];
			
			//check to see if its a payment request
			if (empty($details->verification) && empty($details->ide) && empty($details->email) && empty($tickets)) 
				return;
			
			//get the currency
			$currency = $vars['currency'];			
			
			$query->clear()
				->select($db->qn('name'))
				->from($db->qn('#__rseventspro_events'))
				->where($db->qn('id').' = '.(int) $details->ide);
			
			$db->setQuery($query);
			$event = $db->loadObject();			
			
			if (count($tickets) == 1) {
				$ticket			= $tickets[0];
				$paypal_item	= htmlentities($event->name.' - '.$ticket->name,ENT_QUOTES,'UTF-8');
				$paypal_total	= isset($ticket->price) ? $ticket->price : 0;
				$paypal_number	= isset($ticket->quantity) ? $ticket->quantity : 1;
			} else {
				$paypal_item	= htmlentities($event->name.' - '.JText::_('RSEPRO_PLG_PLUGIN_IDEAL_MULTIPLE'),ENT_QUOTES,'UTF-8');
				$paypal_total	= 0;
				$paypal_number	= 1;
				
				foreach ($tickets as $ticket) {
					if ($ticket->price > 0)
						$paypal_total += ($ticket->price * $ticket->quantity);
				}
			}
			
			
			if ($paypal_total == 0) return;
			
			$thetax = 0;
			$thediscount = 0;
			
			if ($details->early_fee)
				$thediscount += $details->early_fee;
			
			if ($details->late_fee)
				$thetax += $details->late_fee;
			
			$html = '';		
			$html .= '<fieldset>'."\n";
			$html .= '<legend>'.JText::_('RSEPRO_PLG_PLUGIN_IDEAL_TICKETS_INFO').'</legend>'."\n";
			$html .= '<table cellspacing="10" cellpadding="0" class="table table-bordered rs_table">'."\n";
			$html .= '<tr>'."\n";
			$html .= '<td>'.JText::_('RSEPRO_PLG_PLUGIN_IDEAL_TICKETS').'</td>'."\n";
			$html .= '<td>'."\n";
			
			$discount = $details->discount;
			$total = 0;
			if (!empty($tickets)) { 
				foreach ($tickets as $ticket) {
					if (empty($ticket->price))
						$html .= $ticket->quantity. ' x '.$ticket->name.' ('.JText::_('COM_RSEVENTSPRO_GLOBAL_FREE'). ')<br />';
					else
						$html .= $ticket->quantity. ' x '.$ticket->name.' ('.rseventsproHelper::currency($ticket->price). ')<br />';
					
					if ($ticket->price > 0)
						$total += ($ticket->quantity * $ticket->price);
				}
			} 
			if (!empty($discount)) $total = $total - $discount;
			
			$html .= '</td>'."\n";
			$html .= '</tr>'."\n";
			
			if (!empty($discount)) {
				$html .= '<tr>'."\n";
				$html .= '<td>'.JText::_('RSEPRO_PLG_PLUGIN_IDEAL_TICKETS_DISCOUNT').'</td>'."\n";
				$html .= '<td>'.rseventsproHelper::currency($discount).'</td>'."\n";
				$html .= '</tr>'."\n";
			}
			
			if ($details->early_fee) {
				$total = $total - $details->early_fee;
				$html .= '<tr>'."\n";
				$html .= '<td>'.JText::_('RSEPRO_PLG_PLUGIN_IDEAL_EARLY_FEE').'</td>'."\n";
				$html .= '<td>'."\n";
				$html .= rseventsproHelper::currency($details->early_fee);
				$html .= '</td>'."\n";
				$html .= '</tr>'."\n";
			}
			
			if ($details->late_fee) {
				$total = $total + $details->late_fee;
				$html .= '<tr>'."\n";
				$html .= '<td>'.JText::_('RSEPRO_PLG_PLUGIN_IDEAL_LATE_FEE').'</td>'."\n";
				$html .= '<td>'."\n";
				$html .= rseventsproHelper::currency($details->late_fee);
				$html .= '</td>'."\n";
				$html .= '</tr>'."\n";
			}
			
			if (!empty($details->tax)) {
				$total = $total + $details->tax;
				$html .= '<tr>'."\n";
				$html .= '<td>'.JText::_('RSEPRO_PLG_PLUGIN_IDEAL_TICKETS_TAX').'</td>'."\n";
				$html .= '<td>'.rseventsproHelper::currency($details->tax).'</td>'."\n";
				$html .= '</tr>'."\n";
			}
			
			//$paypal_total = $paypal_number * $paypal_total;
			
			$html .= '<tr>'."\n";
			$html .= '<td>'.JText::_('RSEPRO_PLG_PLUGIN_IDEAL_TICKETS_TOTAL').'</td>'."\n";
			$html .= '<td>'.rseventsproHelper::currency($total).'</td>'."\n";
			$html .= '</tr>'."\n";
			
			$html .= '</table>'."\n";
			$html .= '</fieldset>'."\n";
			
			$html .= '<p style="margin: 10px;font-weight: bold;">'.JText::_('RSEPRO_PLG_PLUGIN_IDEAL_REDIRECTION').'</p>'."\n";			
			
			$html .= '<form method="post" action="index.php?option=com_ccidealplatform&task=bankform&Itemid='.$Itemid.'" id="idealForm">'."\n";			
			
			$html .= '<input type="hidden" name="ids" value="'.$details->id.'" />'."\n";
			$html .= '<input type="hidden" name="grandtotal" value="'.$total.'" />'."\n";
			$html .= '<input type="hidden" name="extn" value="rsepro" />'."\n";
			$html .= '<input type="hidden" name="ordernumber" value="'.date("ddy").$details->id.'" />'."\n";
			$html .= '<input type="hidden" name="ideal_paymentmethod" value="'.$ipaymethod.'" />'."\n";
			
			$html .= '</form>'."\n";			
			
			$html .= '<script type="text/javascript">'."\n";
			$html .= 'function idealFormSubmit() { document.getElementById(\'idealForm\').submit() }'."\n";
			$html .= 'try { window.addEventListener ? window.addEventListener("load",idealFormSubmit,false) : window.attachEvent("onload",idealFormSubmit); }'."\n";
			$html .= 'catch (err) { idealFormSubmit(); }'."\n";
			$html .= '</script>'."\n";
			
			echo $html;
		}	
	}
	
	/*
	*	Process the form
	*/
	
	public function rsepro_processForm() {			

        // David - 15 May 2015 - cciDEAL 4.4.5 TEST
        // NOT USED ANYMORE, SEE
        // /components/com_ccidealplatform/models/ccideal.php >  rsepro_notify
        //

        //check to see if we can show something
		if (!$this->canRun()) 
			return;				
		
		$log	= array();		
		
		$order_number 	= JRequest :: getVar('orderID');
		
		if(!$order_number){
			$order_number 	= JRequest :: getVar('order_id');
		}
		
		if(!$order_number){
            $order_number     = JRequest::getVar('orderid');
        }
		
		$date=date("ddy");
		$subscriber=str_replace("$date","",$order_number);								
							
		$db = JFactory::getDBO();
        $query = "SELECT payment_status FROM #__ccidealplatform_payments WHERE order_id='" . $order_number . "'";
        $db->setQuery($query);
        $result = $db->loadResult();		
		
		//send the activation email
		
		require_once JPATH_SITE.'/components/com_rseventspro/helpers/emails.php';
		
		if($result == "paid") {
            //send the activation email
            $status = 1;
            rseventsproHelper::confirm($subscriber);
            $log[] = "Successfully added the payment to the database.";

        } else if ($result == "pending") {
            $status = 0;
            $log[] = "Payment processed and placed in the log.";

        } else if ($result == "cancelled") {
            $status = 2;
            rseventsproHelper::denied($subscriber);
            $log[] = "Payment cancelled and placed in the log.";
        }
		
		$query1 = "UPDATE #__rseventspro_users SET `state` = '" . $status . "' WHERE id='$order_number'";
        $db->setQuery($query1);
        $db->Query();
		
		//rseventsproHelper::confirm($subscriber);	
		
		rseventsproHelper::savelog($log,$subscriber);
		
	}		
	
	protected function escape($string) {
		return htmlentities($string, ENT_COMPAT, 'UTF-8');
	}
	
	protected function convertprice($price) {
		return number_format($price, 2, '.', '');
	}
	
	public function rsepro_tax($vars) {
		if (!$this->canRun()) return;	
		
		if (isset($vars['method']) && $vars['method'] == $this->rsprooption) {
			$total		= isset($vars['total']) ? $vars['total'] : 0;
			$tax_value	= $this->params->get('tax_value',0);
			$tax_type	= $this->params->get('tax_type',0);
			
			return rseventsproHelper::setTax($total,$tax_type,$tax_value);
		}
	}
}