INSERT IGNORE INTO `#__rsform_config` (`SettingName`, `SettingValue`) VALUES
('ideal.test', '0'),
('ideal.tax.type', '1'),
('ideal.tax.value', '');

INSERT IGNORE INTO `#__rsform_component_types` (`ComponentTypeId`, `ComponentTypeName`) VALUES (700, 'ideal');

DELETE FROM #__rsform_component_type_fields WHERE ComponentTypeId = 700;
INSERT IGNORE INTO `#__rsform_component_type_fields` (`ComponentTypeId`, `FieldName`, `FieldType`, `FieldValues`, `Ordering`) VALUES
(700, 'NAME', 'textbox', '', 0),
(700, 'LABEL', 'textbox', '', 1),
(700, 'COMPONENTTYPE', 'hidden', '700', 2),
(700, 'LAYOUTHIDDEN', 'hiddenparam', 'YES', 7);