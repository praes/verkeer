<?php
/**
* @package	cciDEAL Platform 
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * RSForm! Pro system plugin
 */
class plgSystemRSFP_iDEAL extends JPlugin
{
	var $componentId 	= 700;
	var $componentValue = 'ideal';
	
	function plgSystemRSFP_iDEAL( &$subject, $config )
	{	
		parent::__construct( $subject, $config );
		$this->newComponents = array(700);
	}

	function rsfp_bk_onAfterShowComponents()
	{
		$lang = JFactory::getLanguage();
		$lang->load( 'plg_system_rsfp_ideal' );

		$mainframe 	=  JFactory::getApplication();
		$db 		=  JFactory::getDBO();
		$formId 	= JRequest::getInt('formId');

		$link = "displayTemplate('".$this->componentId."')";
		if ($components = RSFormProHelper::componentExists($formId, $this->componentId))
			$link = "displayTemplate('".$this->componentId."', '".$components[0]."')";
		?>
		<li><a href="javascript: void(0);" onclick="<?php echo $link;?>;return false;" id="rsfpc<?php echo $this->componentId; ?>"><span id="ideal"><img style="margin-left: -20px;" src="<?php echo JURI::root() ?>administrator/components/com_ccidealplatform/assets/ideal.png"> <?php echo JText::_('(Payment) iDEAL'); ?></span></a></li>
		<?php
	}

	function rsfp_getPayment(&$items, $formId)
	{  
		if ($components = RSFormProHelper::componentExists($formId, $this->componentId))
		{
			$data = RSFormProHelper::getComponentProperties($components[0]);

			$item 			= new stdClass();
			$item->value 	= $this->componentValue;
			$item->text 	= $data['LABEL'];

			// add to array
			$items[] = $item;
		}
	}

	function rsfp_doPayment($payValue, $formId, $SubmissionId, $price, $products, $code)
	{	
		// execute only for our plugin
		if ($payValue != $this->componentValue) return;

		$mainframe   = JFactory::getApplication();
		$ideal_link = RSFormProHelper::getConfig('ideal.test') ?  JURI::root()."plugins/system/rsfp_ideal/rsfp_redirect_ideal.php?" : JURI::root()."plugins/system/rsfp_ideal/rsfp_redirect_ideal.php?" ;
		$cancel_link = RSFormProHelper::getConfig('ideal.cancel');
		$cancel_link = !empty($cancel_link) ? '&cancel_return='.urlencode($cancel_link) : '';
		$currency	 = RSFormProHelper::getConfig('payment.currency');
		$language	 = RSFormProHelper::getConfig('ideal.language');
		$language	 = !empty($language) ? '&lc='.urlencode($language) : '&lc=US';
		$tax		 = RSFormProHelper::getConfig('ideal.tax.value');
		$tax		 = !empty($tax) ? $tax : 0;
		$taxtype	 = RSFormProHelper::getConfig('ideal.tax.type');
		$products	 = implode(', ', $products);

		if ($tax)
			$tax_code = $taxtype ? '&tax='.urlencode($tax) : '&tax_rate='.urlencode($tax);
		else
			$tax_code = '';

		if ($price > 0)
		{	
			if($taxtype){
				$priceTax = ($price * $tax )/100;
				$price 	= $price+$priceTax;
				
			}else{
				$price 	= $price+$tax;	
			}

			// Find and and get value of ccideal_description if field is used in the form
			$db 	= JFactory::getDBO();
			$db->setQuery("SELECT FieldValue FROM #__rsform_submission_values as sv WHERE sv.FieldName='ccideal_description' AND sv.SubmissionId = '".$SubmissionId."'");
			$ccideal_description = $db->loadResult();

            $ordernumber	= date("Ymdd").$SubmissionId;
		 	$price = number_format($price, 2, '.', '');
		 
			$price=$price+
			$link = $ideal_link . '&item_name=' . urlencode($products) . '&currency_code=' . urlencode($currency). '&grandtotal=' . urlencode($price) . '&extn=rsformpro'.'&ordernumber='.$ordernumber.'&formId='.$formId.'&charset=utf-8'.$language.$tax_code;
			 
			$total			= $price;
			$extn			= 'rsformpro'; 
			$redirect 		= "index.php?option=com_ccidealplatform&task=bankform";

		?>
			<form id="idealform" action="<?php echo $redirect; ?>" method="post">
				<input type="hidden" name="grandtotal" id="grandtotal" value="<?php echo $total; ?>" />
				<input type="hidden" name="ordernumber" id="ordernumber" value="<?php echo $ordernumber; ?>"/>
				<input type="hidden" name="extn" id="extn" value="<?php echo $extn; ?>"/>

				<!-- David - 02 May 2015 - cciDEAL 4.4.5 -->
				<!-- Send and store more information in cciDEAL DB -->
                <?php
                    if (!empty($ccideal_description)) {
                ?>
                    <input type = "hidden" name = "ccideal_description" id = "ccideal_description" value = "<?php echo $ccideal_description; ?>" />
                <?php
                    }
                ?>

                <!-- David - 27 May 2015 - cciDEAL 4.4.5 -->
                <!-- Set a non-default payment method for this plugin/payment method -->
                <input type="hidden" name="ideal_paymentmethod" id="ideal_paymentmethod" value="ALL"/>

                <input type="hidden" name="payment_custom" id="payment_custom" value="<?php echo $SubmissionId; ?>"/>
				<input type="hidden" name="extra_textfield" id="extra_textfield" value="<?php echo $SubmissionId; ?>"/>
			</form>

			<script type="text/javascript">

				var prodid = document.getElementById('ordernumber').value ;
				if ( prodid) {
					document.getElementById('idealform').submit();
				}
			</script><?php
			
			exit;
		}
	}

	function rsfp_bk_onAfterCreateComponentPreview($args = array())
	{   
		if ($args['ComponentTypeName'] == 'ideal')
		{
			$args['out'] = '<td>&nbsp;</td>';
			$args['out'].= '<td><img src="'.JURI::root(true).'/administrator/components/com_ccidealplatform/assets/ideal.png" /> '.$args['data']['LABEL'].'</td>';
		}
	}

	function rsfp_bk_onAfterShowConfigurationTabs($tabs)
	{
		$lang =  JFactory::getLanguage();
		$lang->load( 'plg_system_rsfp_ideal' );

		$tabs->addTitle(JText::_('iDEAL'), 'form-ideal');
		$tabs->addContent($this->idealConfigurationScreen());
	}
	function rsfp_afterConfirmPayment($SubmissionId)
    {        
        $date     = date("Ymdd");
        $sid    = str_replace("$date","",$SubmissionId);
        
        $db      = JFactory::getDBO();    
        $query ="SELECT id FROM #__ccidealplatform_payments WHERE extension_id='".$sid."'";
        $db->setQuery( $query );
        $result     = $db->loadResult();
        
		//Commented in 12/6/14
        /*if($result){
            RSFormProHelper::sendSubmissionEmails($SubmissionId);
        } */       
    }
	
	function idealConfigurationScreen()
	{
		ob_start();

		?>
		<div id="page-ideal" class="com-rsform-css-fix">
			<table  class="admintable">

				<tr>
					<td width="200" style="width: 200px;" align="right" class="key"><label for="tax.type"><?php echo JText::_( 'Belasting berekening' ); ?></label></td>
					<td><?php echo JHTML::_('select.booleanlist', 'rsformConfig[ideal.tax.type]' , '' , RSFormProHelper::htmlEscape(RSFormProHelper::getConfig('ideal.tax.type')), JText::_('Percentage'), JText::_('Vast bedrag'));?></td>
				</tr>
				<tr>
					<td width="200" style="width: 200px;" align="right" class="key"><label for="tax.value"><?php echo JText::_( 'Belasting bedrag/percentage' ); ?></label></td>
					<td><input type="text" name="rsformConfig[ideal.tax.value]" value="<?php echo RSFormProHelper::htmlEscape(RSFormProHelper::getConfig('ideal.tax.value'));  ?>" size="4" maxlength="5"></td>
				</tr>
			 
			</table>
		</div>
		<?php

		$contents = ob_get_contents();
		ob_end_clean();
		return $contents;
	}
}