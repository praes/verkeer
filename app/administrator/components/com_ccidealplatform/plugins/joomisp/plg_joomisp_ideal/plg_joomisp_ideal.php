<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

class JoomISPPluginPaymentplg_joomisp_ideal
{
	function getForm() {
		$params = JoomISPHelperPayment::getParams();

		$subject = $_SESSION['paymentsubject'];
		$order_number = str_replace('Order No. ','',$subject);
		$order_number = date("mY").$order_number;
		$currency = $params->get('currency','EUR');
		$address = $params->get('address');
		$amount = $_SESSION['amount'];

		$lang = JFactory::getLanguage();
		$langtag = JString::str_ireplace('-','_', $lang->getTag());

		return '
			<form action="index.php?option=com_ccidealplatform&task=bankform" method="post" target="_self">

				<input type="hidden" name="extn" id="extn" value="joomisp"/>
				<input type="hidden" name="ordernumber" value="'.$order_number.'" />
				<input type="hidden" name="grandtotal" value="'.$amount.'" />
				<b><img alt="Betaal met iDEAL" border="0" src=" " width="1" height="1"></b><br/><br/>
				<input style="border:2px solid #D6E3D2;" type="image" src="'.JURI::root().'/administrator/components/com_ccidealplatform/assets/ccideal_thanks.jpg" border="0" name="submit" alt="iDEAL Payment" />

			</form>';

	}

	function getLink() {
		$params = JoomISPHelperPayment::getParams();

		$subject = $_SESSION['paymentsubject'];
		$order_number = str_replace('Order No. ','',$subject);
		$order_number = $order_number.date("mY");
		$currency = $params->get('currency','EUR');
		$address = $params->get('address');
		$amount = $_SESSION['amount'];

		return JURI::root().'index.php?option=com_ccidealplatform&view=ccideal&layout=content_redirect&extn=joomisp&ordernumber='.$order_number.'&grandtotal='.$amount;

	}
}
