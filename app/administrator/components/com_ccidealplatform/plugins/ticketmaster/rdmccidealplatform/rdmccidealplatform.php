<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
## Import library dependencies
jimport('joomla.plugin.plugin');
 
class plgRDmediaRDMccidealplatform extends JPlugin
{
/**
 * Constructor
 *
 * For php4 compatability we must not use the __constructor as a constructor for
 * plugins because func_get_args ( void ) returns a copy of all passed arguments
 * NOT references.  This causes problems with cross-referencing necessary for the
 * observer design pattern.
 */
 function plgRDMediaRDMccidealplatform( &$subject, $params  ) {
 
    parent::__construct( $subject , $params  );
	
	## Loading language:	
	$lang = JFactory::getLanguage();
	$lang->load('plg_rdmedia_ccidealplatform', JPATH_ADMINISTRATOR);	

	## load plugin params info
 	$plugin = JPluginHelper::getPlugin('rdmedia', 'rdmccidealplatform');	
	$this->currency = $this->params->def( 'currency', 'EUR' );	
	$this->success_tpl = $this->params->def( 'success_tpl', 1 );
	$this->failure_tpl = $this->params->def( 'failure_tpl', 1 );
	$this->itemid = $this->params->def( 'itemid', 1 );
	$this->infobox = $this->params->def( 'infobox', 'enter a message in the backend.' );
	$this->layout = $this->params->def( 'layout', 0 );	
	
	## Including required paths to calculator.
	$path_include = JPATH_SITE.DS.'components'.DS.'com_ticketmaster'.DS.'assets'.DS.'helpers'.DS.'get.amount.php';
	include_once( $path_include );

	## Getting the global DB session
	$session =& JFactory::getSession();
	## Gettig the orderid if there is one.
	$this->ordercode = $session->get('ordercode');
	
	## Getting the amounts for this order.
	$this->amount = _getAmount($this->ordercode);
	$this->fees	  = _getFees($this->ordercode); 
	
 }
 
/**
 * Plugin method with the same name as the event will be called automatically.
 * You have to get at least a function called display, and the name of the processor (in this case iDEAL)
 * Now you should be able to display and process transactions.
 * 
*/

	 function display()
	 {
		$app = JFactory::getApplication();
		
		## Loading the CSS file for iDEAL plugin.
		$document = JFactory::getDocument();
		$document->addStyleSheet( JURI::root(true).'/plugins/rdmedia/rdmccidealplatform/rdmedia_ccidealplatform/css/rdmccidealplatform.css' );	
		
		$user = JFactory::getUser();
		
		## Making sure iDEAL getting the right amount (23.00)
		$ordertotal = number_format($this->amount, 2, '.', '');		
		
		$ordernumber	= date("yddm").$this->ordercode;
		
		$extn = "ticketmaster";
		
		## Check the amount, if higher then 0.00 then show the plugin data.	
		if ($ordertotal > '0.00') {
			
			## Check if this is Joomla 2.5 or 3.0.+
			$isJ30 = version_compare(JVERSION, '3.0.0', 'ge');
			
			## This will only be used if you use Joomla 2.5 with bootstrap enabled.
			## Please do not change!
			
			if(!$isJ30){
				if($config->load_bootstrap == 1){
					$isJ30 = true;
				}
			}

				$redirect = "index.php?option=com_ccidealplatform&task=bankform";	

				echo '<img src="plugins/rdmedia/rdmccidealplatform/rdmedia_ccidealplatform/images/rdmccidealplatform_vertical_view.jpg" />';
				
				echo '<form action="'.$redirect.'" method="post" name="idealForm">';
				
				## Low let's get some information about your payment.	
					echo    '<input type="hidden" name="grandtotal" id="grandtotal" value="'.$ordertotal.'"  />';
					echo    '<input type="hidden" name="ordernumber" id="ordernumber" value="'.$ordernumber.'" />';
					echo    '<input type="hidden" name="extn" id="extn" value="'.$extn.'" />';	
				
				echo    '<button class="btn btn-block btn-success" style="margin-top: 8px;" type="submit">'.JText::_( 'Make Payment' ).'</button>';			
				echo 	'</form>';

		}
		
		return true;
	 }

	function _showmsg($msgid, $msg){
		
		$db = JFactory::getDBO();
		
		## Getting the desired info from the configuration table
		$sql = "SELECT * FROM #__ticketmaster_emails WHERE emailid = ".(int)$msgid."";
		$db->setQuery($sql);
		$config = $db->loadObject();
	
		echo '<h1>'.$config->mailsubject.'</h1>';
		$message = str_replace('%%MSG%%', $msg, $config->mailbody);
		echo $message;
		echo '<br/><br/><br/><br/><br/><br/>';

		## Removing the session, it's not needed anymore.
		$session =& JFactory::getSession();
		$session->clear('ordercode');
				
	}


	function rdmccidealplatform() {

		// Load user_profile plugin language
		$lang = JFactory::getLanguage();
		$lang->load('plg_rdmedia_ccidealplatform', JPATH_ADMINISTRATOR);
		
		$order_id = JRequest::getVar( 'order_id' );	
		
		
		$db = JFactory :: getDBO();
		$query = "SELECT payment_status,trans_id,payment_total FROM #__ccidealplatform_payments WHERE order_id='".$order_id."'";
		$db->setQuery($query);
		$status = $db->loadObject();
		
	
		## Include the confirmation class to sent the tickets. 
		$path = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ticketmaster'.DS.'classes'.DS.'createtickets.class.php';
		$override = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ticketmaster'.DS.'classes'.DS.'override'.DS.'createtickets.class.php';		
		
		if($status->payment_status == "paid") {			
				
				## Connecting the database
				$db = JFactory::getDBO();
				## Current date for database.
				$trans_date = date("d-m-Y H:i");
				## Getting the transaction info from PP.
				$payment_id = $status->trans_id;
				
				## Check that txn_id has not been previously processed
				$sql = 'SELECT COUNT(pid) AS total 
						FROM #__ticketmaster_transactions 
						WHERE transid = "'.$payment_id.'" ';
						
				$db->setQuery($sql);
				$results = $db->loadObject();
				
				if($results->total > 0){				
				
					## Show error on failed - Transaction may not exsist in DB.
					$msg = JText::_( 'COM_TICKETMSTER_PP_TRANSACTION_PROCESSED' );
					$this->_showmsg($this->failure_tpl, $msg);		
				
				}else{				
				
					## Paid amount to iDEAL
					$payment_amount = $status->payment_total;
					## Get the email address from the buyer.
					
					$user 	 = &JFactory::getUser();
					$user_id = ($user->id);
					$sql1 = 'SELECT emailaddress FROM #__ticketmaster_clients WHERE userid = "'.$user->id.'" ';						
					$db->setQuery($sql1);
					$user_email = $db->loadResult();
					
					$payer_email 	= $user_email;
					## Get the order information sent by PP.
					//$orderid 		= $response['custom'];
					
					$date			= date("yddm");
					$orderid		= str_replace("$date","",$order_id);
					
					## Including required paths to calculator.
					$path_include = JPATH_SITE.DS.'components'.DS.'com_ticketmaster'.DS.'assets'.DS.'helpers'.DS.'get.amount.php';
					include_once( $path_include );
					
					## Getting the amounts for this order.
					$amount = _getAmount($orderid, 1);
					
					##Requested amount for this order.
					$amount_req = number_format($amount, 2, '', '');
					## Sent amount by PP (needs the same notation as ours)							
					$amount_pp = number_format($payment_amount, 2, '', '');
					
					## Check if the amount is the same as the paid amount.
					if ($amount_req != $amount_pp) {
						
						## Amounts are not the same. Show the message to the client.
						$msg = JText::_( 'COM_TICKETMSTER_PP_AMOUNT_IS_NOT_CORRECTLY' );
						$this->_showmsg($this->failure_tpl, $msg);						
					
					}else{
						
						## Getting the latest logged in user.
						$user = & JFactory::getUser();
			
						JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ticketmaster'.DS.'tables');
						$row =& JTable::getInstance('transaction', 'Table');	
					
						## Pickup All Details and create foo=bar&baz=boom&cow=milk&php=hypertext+processor
						//$payment_details = http_build_query($response);
						$payment_type = 'iDEAL';
						//$orderid = $response['custom'];
						
						## Now store all data in the transactions table
						$row->transid = $payment_id;
						$row->userid = $user->id;
						@$row->details = $payment_details;
						$row->amount = $payment_amount;
						$row->type = 'iDEAL';
						$row->email_paypal = $payer_email;
						$row->orderid = $orderid;
						
						## Store data
						$row->store();		
						
						$query = 'UPDATE #__ticketmaster_orders'
							. ' SET paid = 1, published = 1'
							. ' WHERE ordercode = '.(int)$orderid.'';
						
						## Do the query now	
						$db->setQuery( $query );
						
						## When query goes wrong.. Show message with error.
						if (!$db->query()) {
							$this->setError($db->getErrorMsg());
							return false;
						}
						
						$query = 'SELECT * FROM #__ticketmaster_orders WHERE ordercode = '.(int)$orderid.'';
	
						## Do the query now	
						$db->setQuery($query);
						$data = $db->loadObjectList();
			
						
						$k = 0;
						for ($i = 0, $n = count($data); $i < $n; $i++ ){
							
							$row  = &$data[$i];
						
							## Check if the override is there.
							if (file_exists($override)) {
								## Yes, now we use it.
								require_once($override);
							} else {
								## No, use the standard			
								
								require_once($path);
							}
							
							if(isset($row->orderid)) {  
							
								$creator = new ticketcreator( (int)$row->orderid );  
								$creator->doPDF();
							
							}  				
							
							$k=1 - $k;
							
						}
						
						## Include the confirmation class to sent the tickets. 
						$path_include = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ticketmaster'.DS.'classes'.DS.'sendonpayment.class.php';
						include_once( $path_include );
						
						## Sending the ticket immediatly to the client.
						$creator = new sendonpayment( (int)$orderid );  
						$creator->send();
						
						## Removing the session, it's not needed anymore.
						$session =& JFactory::getSession();
						$session->clear($orderid);
						$session->clear('ordercode');
						$session->clear('coupon');						

						## Getting the desired info from the configuration table
						$sql = "SELECT * FROM #__ticketmaster_emails WHERE emailid = ".(int)$this->success_tpl."";
						$db->setQuery($sql);
						$config = $db->loadObject();

						## Getting the desired info from the configuration table
						$sql = "SELECT * FROM #__users WHERE id = ".(int)$user->id."";
						$db->setQuery($sql);
						$user = $db->loadObject();							
						
						echo '<h1>'.$config->mailsubject.'</h1>';
						
						$message = str_replace('%%TID%%', $payment_id, $config->mailbody);
						$message = str_replace('%%OID%%', $orderid, $message);
						$message = str_replace('%%AMOUNT%%', $payment_amount, $message);
						$message = str_replace('%%DATE%%', $trans_date, $message);
						$message = str_replace('%%NAME%%', $user->name, $message);
						$message = str_replace('%%EMAIL%%', $payer_email, $message);					
						
										
						## Imaport mail functions:
						jimport( 'joomla.mail.mail' );
											
						## Set the sender of the email:
						$sender[0] = $config->from_email;
						$sender[1] = $config->from_name;					
						## Compile mailer function:			
						$obj = JFactory::getMailer();
						$obj->setSender( $sender );
						$obj->isHTML( true );
						$obj->setBody ( $message );				
						$obj->addRecipient($user->email);
						## Send blind copy to site admin?
						if ($config->receive_bcc == 1){
							if ($config->reply_to_email != ''){
								$obj->addRecipient($obj->reply_to_email);
							}	
						}					
						## Add reply to and subject:					
						$obj->addReplyTo($config->reply_to_email);
						$obj->setSubject($config->mailsubject);
						
						if (@$mail->published == 1){						
							
							$sent = $obj->Send();						
						}								
						
						echo $message;										
																								
					
					}

				}							
			
			
		
		} elseif($status->payment_status == "pending"){			
			
			$date			= date("yddm");
			$ordercode		= str_replace("$date","",$order_id);		

			$db = JFactory::getDBO();
			$user = JFactory::getUser();
			
			$sql = "SELECT * FROM #__users WHERE id = ".(int)$user->id."";
			$db->setQuery($sql);
			$user = $db->loadObject();		

		
			## Getting the desired info from the configuration table
			$sql = "SELECT * FROM #__ticketmaster_emails WHERE emailid = ".(int)$this->success_tpl."";
			$db->setQuery($sql);
			$config = $db->loadObject();
		
			## Making the query for showing all the cars in list function
			$sql = 'SELECT priceformat, valuta, dateformat FROM #__ticketmaster_config WHERE configid = 1';
	 
			$db->setQuery($sql);
			$configuration = $db->loadObject();		
		
			## Getting the amount of tickets.
			$query = 'SELECT COUNT(orderid) AS tickets FROM #__ticketmaster_orders WHERE ordercode = '.(int)$ordercode.'';
			$db->setQuery($query);
			$t = $db->loadObject();	
		
			##### SHOWING A MESSAGE IN SCREEN ######

			## Including required paths to calculator.
			$path_include = JPATH_SITE.DS.'components'.DS.'com_ticketmaster'.DS.'assets'.DS.'functions.php';
			include_once( $path_include );	
			$price = $this->amount/100;
			
			$amount =  showprice($configuration->priceformat, $price, $configuration->valuta);	

			$path_include = JPATH_SITE.DS.'components'.DS.'com_ticketmaster'.DS.'assets'.DS.'functions.php';
			include_once( $path_include );

			$db = JFactory::getDBO();		
			
			## Loading the configuration table.
			$sql = 'SELECT * FROM #__ticketmaster_config WHERE configid = 1';
			 
			$db->setQuery($sql);
			$configuration = $db->loadObject();
						
			## Getting the desired info from the configuration table
			$sql = 'SELECT * FROM #__ticketmaster_emails WHERE emailid = 3';
			 
			$db->setQuery($sql);
			$config = $db->loadObject();
		
			$sql='SELECT  a.*, t.*, e.eventname, c.*, 
				  t.ticketdate, t.starttime, t.location, t.locationinfo, a.paid, e.groupname, t.eventcode
				  FROM #__ticketmaster_orders AS a, #__ticketmaster_clients AS c,
				  #__ticketmaster_events AS e, #__ticketmaster_tickets AS t
				  WHERE a.userid = c.userid
				  AND a.eventid = e.eventid
				  AND a.ticketid = t.ticketid
				  AND ordercode = '.(int)$ordercode.'';
					
			$db->setQuery($sql);
			$data = $db->loadObjectList();	
		
			$sql='SELECT COUNT(orderid) AS total 
				  FROM #__ticketmaster_orders 
				  WHERE ordercode = '.(int)$ordercode.'
				  AND paid = 0';
				  
			$db->setQuery($sql);
			$status = $db->loadObject();				  		
		
			if ($status->total > 0) {
				$paymentstatus = '<font color="#FF0000">'.JText::_( 'COM_TICKETMASTER_ORDERSTATUS_UNPAID' ).'</font>';
			}else{
				$paymentstatus = '<font color="#006600">'.JText::_( 'COM_TICKETMASTER_ORDERSTATUS_PAID' ).'</font>';
			}
		
			$orders = '<ul>';
			
			$k = 0;
			for ($i = 0, $n = count($data); $i < $n; $i++ ){
		
				$row        = &$data[$i];
				
				$price = showprice($configuration->priceformat ,$row->ticketprice , $configuration->valuta);
				$ticketdate = date ($configuration->dateformat, strtotime($row->ticketdate));
				
				$customer = $row->name;
				$email = $row->emailaddress;
				
				$orders .= '<li>[ '.$row->orderid.' ] - [ '.$ticketdate.' ] - <strong>'.$row->ticketname.'</strong> [ '.$price.' ]</li>';	
		
				
			$k=1 - $k;
			}
		
			$orders .= '</ul>';	

			## Including required paths to calculator.
			$path_include = JPATH_SITE.DS.'components'.DS.'com_ticketmaster'.DS.'assets'.DS.'helpers'.DS.'get.amount.php';
			include_once( $path_include );	
			
			$date  = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));
			
			$expired  = mktime(0, 0, 0, date("m")  , date("d")+$configuration->removal_days, date("Y"));
			$releasedate = date($configuration->dateformat, $expired);		
			
			## Get the paid amount:
			$to_be_paid = _getAmount($ordercode);	
			$price = showprice($configuration->priceformat ,$to_be_paid , $configuration->valuta);
			$total_tickets = count($data);	
			
			## Getting the date	
			$today = date($configuration->dateformat);  
		
			echo '<h1>'.$config->mailsubject.'</h1>';
			
			$message = str_replace('%%NAME%%', $customer, $config->mailbody);
			
			$message = str_replace('%%ORDERCODE%%', $ordercode, $message);	
			$message = str_replace('%%ORDERDATE%%', $date, $message);
			$message = str_replace('%%TICKETS%%', $total_tickets, $message);	
			$message = str_replace('%%PRICE%%', $price, $message);
			$message = str_replace('%%ORDERLIST%%', $orders, $message);
			$message = str_replace('%%PAYMENTSTATUS%%', $paymentstatus, $message);
			$message = str_replace('%%RELEASEDATE%%', $releasedate, $message);
			$message = str_replace('%%COMPANYNAME%%', $configuration->companyname, $message);
			$message = str_replace('%%COMPANYADDRESS%%', $configuration->address1, $message);
			$message = str_replace('%%COMPANYCITY%%', $configuration->city, $message);
			$message = str_replace('%%PHONENUMBER%%', $configuration->phone, $message);
			
			echo $message;

			$query = 'SELECT * FROM #__ticketmaster_orders WHERE ordercode = '.(int)$ordercode.'';

			## Do the query now	
			$db->setQuery($query);
			$data = $db->loadObjectList();
		
			$k = 0;
			for ($i = 0, $n = count($data); $i < $n; $i++ ){
				
				$row  = &$data[$i];
			
				## Check if the override is there.
				if (file_exists($override)) {
					## Yes, now we use it.
					require_once($override);
				} else {
					## No, use the standard
					require_once($path);
				}	
				
				if(isset($row->orderid)) {  

					$creator = new ticketcreator( (int)$row->orderid );  
					$creator->doPDF();
				
				}  				
				
				$k=1 - $k;
				
			}

			$query = 'UPDATE #__ticketmaster_orders'
				. ' SET paid = 0, published = 1'
				. ' WHERE ordercode = '.(int)$ordercode.'';
			
			## Do the query now	
			$db->setQuery( $query );
			
			## When query goes wrong.. Show message with error.
			if (!$db->query()) {
				$this->setError($db->getErrorMsg());
				return false;
			}
			
			$path_include = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ticketmaster'.DS.'classes'.DS.'confirmation.php';
			include_once( $path_include );
			
			
			$sendconfirmation = new confirmation( (int)$ordercode );  
			$sendconfirmation->doConfirm();
			$sendconfirmation->doSend();

			## Removing the session, it's not needed anymore.
			$session =& JFactory::getSession();
			$session->clear('ordercode');
			$session->clear('coupon');	
		
		} else {
		
				## Amounts are not the same. Show the message to the client.
				$msg = JText::_( 'COM_TICKETMSTER_PP_TRANSACTION_FAILED' );
				$this->_showmsg($this->failure_tpl, $msg);		
		
		}		
	
	}	
}	 
?>