<?php
/**
* @package	Hikashop - cciDEAL Platform plugin for cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

defined('_JEXEC') or die('Restricted access');

class plgHikashoppaymentIDEAL extends hikashopPaymentPlugin
{
	var $accepted_currencies = array('EUR');
	var $multiple = true;
	var $name = 'iDEAL';
	var $doc_form = 'iDEAL';	
	
	function __construct(&$subject, $config) {
		parent::__construct($subject, $config);
	}	
	function onAfterOrderConfirm(&$order,&$methods,$method_id){
		parent::onAfterOrderConfirm($order, $methods, $method_id);	

		$db = JFactory::getDBO();

        // David - 28 May 2015 - cciDEAL 4.4.6
        // Removed search for payment method name via payment method name,
        // searching via payment method ID is a lot smarter :)
		$select = "SELECT payment_params FROM #__hikashop_payment WHERE payment_id = '".$method_id."'";
		$db->setQuery($select);
		$params = $db->loadResult();
		$payment_params = unserialize($params);

		$ideal_paymentmethod = $payment_params->ideal_paymentmethod;

		$db = JFactory::getDBO();
		$query = "SELECT IDEAL_Enable,IDEAL_Bank FROM #__ccidealplatform_config";
		$db->setQuery($query);
		$bank = $db->loadObject();

		if ($bank->IDEAL_Enable) {

            $this->writeToLog("cciDEAL plugin: onAfterOrderConfirm start redirect to cciDEAL for order " .
                $order->order_id . "\r\n");

			$redirect_ccideal = JURI::base();
			?>
			<form id="idealform" action="<?php echo $redirect_ccideal.'index.php?option=com_ccidealplatform&task=bankform'; ?>" method="post">
				<input type="hidden" name="grandtotal" id="grandtotal" value="<?php echo number_format($order->order_full_price,2,'.',''); ?>" />
				<input type="hidden" name="ordernumber" id="ordernumber" value="<?php echo $order->order_id; ?>"/>
				<input type="hidden" name="extra_textfield" id="extra_textfield" value="<?php echo $order->order_number; ?>"/>
				<input type="hidden" name="ideal_paymentmethod" id="ideal_paymentmethod" value="<?php echo @$ideal_paymentmethod; ?>"/>				
				<input type="hidden" name="extn" id="extn" value="hikashop"/> 
			</form> 

			<script type="text/javascript"> 
				var prodid = document.getElementById('ordernumber').value; 
				
				if(prodid) {
					document.getElementById('idealform').submit();
				}
			</script> 
			<?php
		}

        if ($bank->IDEAL_Enable==0) {
            $this->writeToLog("cciDEAL plugin: onAfterOrderConfirm, cciDEAL disabled in Components > cciDEAL for order
            " . $order->order_id . "\r\n");
	}

        if ($bank->IDEAL_Enable==2) {
            $this->writeToLog("cciDEAL plugin: onAfterOrderConfirm, cciDEAL disabled for all users but
            Super Users in Components > cciDEAL for order
            " . $order->order_id . "\r\n");
        }
	}

	function onPaymentNotification(&$statuses) {

        $orderID 	= $statuses['orderID'];
        $statuses	= $statuses['statuses'];

        $this->writeToLog("cciDEAL plugin: onPaymentNotification start process for order " . $orderID . "\r\n");

        $db = JFactory :: getDBO();
		$vars = array();

		$select = "SELECT payment_params FROM #__hikashop_payment WHERE payment_type='iDEAL'";
		$db->setQuery($select);
		$currencyCode1 = $db->loadResult();
		$stateArry=unserialize($currencyCode1);

		if(!$stateArry->notification){
            $this->writeToLog("ciDEAL plugin: onPaymentNotification process stopped for order " . $orderID . ",
                notifications are disabled in HikaShop payment method!". "\r\n");
			return false;
		}

		$select = "SELECT * FROM #__hikashop_order WHERE order_id=".$orderID."";
		$db->setQuery($select);
		$dbOrder = $db->loadObject();		

        // If order not found, stop process and warn user
		if(empty($dbOrder)){ echo "Could not load any order for your notification ".@$vars['invoice']; return false; }

        // Get order ID
		@$order = null;
        @$order->order_id = $dbOrder->order_id;
		@$order->old_status->order_status=$dbOrder->order_status;

		@require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_hikashop'.DS.'helpers'.DS.'helper.php');

		if($statuses=="$stateArry->verified_status"){
			$order->order_status = $stateArry->verified_status;
		}else{
			$order->order_status = $stateArry->pending_status;
		}

        // START - payment notification mail
		@$order->mail_status=$statuses[$order->order_status];
        $order_text = "\r\n".JText::sprintf('NOTIFICATION_OF_ORDER_ON_WEBSITE',$dbOrder->order_number,HIKASHOP_LIVE);

        // Get payment method from the HikaShop payments with the payment method ID from orders
        $hikashop_payment_id = $dbOrder->order_payment_id;
        $select = "SELECT payment_name FROM #__hikashop_payment WHERE payment_id=".$hikashop_payment_id."";
        $db->setQuery($select);
        $mail_payment_method = $db->loadResult();

        $email = new stdClass();
        $email->subject = JText::sprintf('PAYMENT_NOTIFICATION_FOR_ORDER',$mail_payment_method,$order->order_status,$dbOrder->order_number);
        $email->body = str_replace('<br/>',"\r\n",JText::sprintf('PAYMENT_NOTIFICATION_STATUS',$mail_payment_method,
                $order->order_status)).' '.JText::sprintf('ORDER_STATUS_CHANGED',$order->order_status)."\r\n".$order_text;
        // END - payment notification mail

        $order_status = $dbOrder->order_status;
        $order_id = $orderID;

        $this->writeToLog("cciDEAL plugin: onPaymentNotification finished process for order " . $order_id . "\r\n");

		// Set history->notified to 1, otherwise customers don't get confirmation emails
		$history = new stdClass();
		$history->notified = 1;
        $this->modifyOrder($order_id, $order->order_status, $history, $email);

		$app = JFactory::getApplication();

        $configs = hikashop_config();
		if($configs->get('clean_cart') == "order_confirmed"){		
			if($configs->get('clean_cart') == 'order_'.$order->order_status) {
				$cart_id = $app->getUserState( HIKASHOP_COMPONENT.'.cart_id');
				if($cart_id) {
					$class = hikashop_get('class.cart');
					$class->delete($cart_id);
					$app->setUserState( HIKASHOP_COMPONENT.'.cart_id',0);
				}
			}
		}
		return true; 
	}
	 
	function onPaymentConfiguration(&$element) {
		$subtask = JRequest::getCmd('subtask', '');		

		parent::onPaymentConfiguration($element);
		$this->address = hikashop_get('type.address');		
	}

	function onPaymentConfigurationSave(&$element) {
		return true;
	}
	
	function getPaymentDefaultValues(&$element) {
		$element->payment_name = 'iDEAL';
		$element->payment_description='You can pay with Dutch payment method iDEAL';
		$element->payment_images = 'select_iDEAL';

		$element->payment_params->url = JURI::root();'index.php?option=com_ccidealplatform&task=bankform';
		$element->payment_params->notification = 1;
		$element->payment_params->invalid_status = 'cancelled';
		$element->payment_params->pending_status = 'created';
		$element->payment_params->verified_status = 'confirmed';		
	}	
 }