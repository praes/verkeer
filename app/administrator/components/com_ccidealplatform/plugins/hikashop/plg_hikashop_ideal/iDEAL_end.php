<?php
/**
* @package	Hikashop - cciDEAL Platform plugin for cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/


defined('_JEXEC') or die('Restricted access');
?>
<div class="hikashop_iDEAL_end" id="hikashop_iDEAL_end">
	<span id="hikashop_iDEAL_end_message" class="hikashop_iDEAL_end_message">
		<?php echo JText::sprintf('PLEASE_WAIT_BEFORE_REDIRECTION_TO_X',$method->payment_name).'<br/>'. JText::_('CLICK_ON_BUTTON_IF_NOT_REDIRECTED');?>
	</span>
	<span id="hikashop_iDEAL_end_spinner" class="hikashop_iDEAL_end_spinner">
		<img src="<?php echo HIKASHOP_IMAGES.'spinner.gif';?>" />
	</span>
	<br/>
	<form id="hikashop_iDEAL_form" name="hikashop_iDEAL_form" action="<?php echo $method->payment_params->url;?>" method="post">
		<div id="hikashop_iDEAL_end_image" class="hikashop_iDEAL_end_image">
			<input id="hikashop_iDEAL_button" type="submit" value="" name="" alt="<?php echo JText::_('PAY_NOW');?>" />
		</div>
		<?php
			foreach( $vars as $name => $value ) {
				echo '<input type="hidden" name="'.$name.'" value="'.htmlspecialchars((string)$value).'" />';
			}
			$doc =& JFactory::getDocument();
			$doc->addScriptDeclaration("window.addEvent('domready', function() {document.getElementById('hikashop_iDEAL_form').submit();});");
			JRequest::setVar('noform',1);
		?>
	</form>
</div>