<?php
/**
* @package	Hikashop - cciDEAL Platform plugin for cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

defined('_JEXEC') or die('Restricted access');

?>
<tr>
	<td class="key">
		<label for="data[payment][payment_params][ideal_paymentmethod]">
			<?php echo JText::_( 'Select payment method' ); ?>
		</label>
	</td>
	<td>
		<?php
		$db =  JFactory::getDBO();
		
		$query = 'SELECT `IDEAL_Bank` FROM `#__ccidealplatform_config` WHERE `id` = 1';
		$db->setQuery($query);
        $IDEAL_Bank = $db->loadResult();		

        $query = 'SELECT `method_value` AS value, `method_name` AS text FROM `#__ccidealplatform_paymentmethod`
               		WHERE `account_id` = "'.$IDEAL_Bank.'" ORDER BY `id` ASC ';

        $db->setQuery($query);
        $fields = $db->loadObjectList();	
		
        echo JHTML::_('select.genericlist', $fields, 'data[payment][payment_params][ideal_paymentmethod]', '', 'value', 'text', @$this->element->payment_params->ideal_paymentmethod);

		?>
	</td>
</tr>

<tr>
	<td class="key">
		<label for="data[payment][payment_params][notification]">
			<?php echo JText::sprintf( 'ALLOW_NOTIFICATIONS_FROM_X', @$this->element->payment_params->payment_type);  ?>
		</label>
	</td>
	<td>
		<?php echo JHTML::_('select.booleanlist', "data[payment][payment_params][notification]" , '',@$this->element->payment_params->notification	); ?>
	</td>
</tr>

<tr>
	<td class="key">
		<label for="data[payment][payment_params][invalid_status]"><?php
			echo JText::_('INVALID_STATUS');
		?></label>
	</td>
	<td><?php
		echo $this->data['order_statuses']->display("data[payment][payment_params][invalid_status]", @$this->element->payment_params->invalid_status);
	?></td>
</tr>

<tr>
	<td class="key">
		<label for="data[payment][payment_params][pending_status]"><?php
			echo JText::_('PENDING_STATUS');
		?></label>
	</td>
	<td><?php
		echo $this->data['order_statuses']->display("data[payment][payment_params][pending_status]", @$this->element->payment_params->pending_status);
	?></td>
</tr>
<tr>
	<td class="key">
		<label for="data[payment][payment_params][verified_status]"><?php
			echo JText::_('VERIFIED_STATUS');
		?></label>
	</td>
	<td><?php
		echo $this->data['order_statuses']->display("data[payment][payment_params][verified_status]", @$this->element->payment_params->verified_status);
	?></td>
</tr>