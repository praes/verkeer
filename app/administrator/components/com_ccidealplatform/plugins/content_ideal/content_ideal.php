<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license. 
**/ 

// no direct access
defined('_JEXEC') or die;

class plgContentContent_ideal extends JPlugin
{

    // David - 2 April 2015 - cciDEAL 4.4.4
    // Removing Akeeba Strapper and FOF, adding ccBootstrap
    public function ccVersionCompare()
    {
        $jversion = new JVersion();
        $current_version =  $jversion->getShortVersion();
        return substr($current_version,0,1);
    }

	public function onContentPrepare($context, &$article, &$params, $page = 0)
	{

        if($this->ccVersionCompare() == 3){
            JHtml::_('behavior.tooltip');
            JHtml::_('behavior.formvalidation');
            JHtml::_('behavior.keepalive');
        }

        if($this->ccVersionCompare() < 3)
        {
            require_once JPATH_ROOT . '/media/chillcreations_bootstrap/ccbootstrap.php';
            if(class_exists('ccbootstrap')){
                $chillcreations_bootsrap = new ccbootstrap();
            }
        }

        $document = JFactory :: getDocument();
        $document->addStyleSheet('components/com_ccidealplatform/assets/css/style.css');

		//Initializing the database
			$regex = trim('/{(ccidealplatform)\s*(.*?)}/i');
			$plugin = JPluginHelper::getPlugin('content', 'content_ideal');
			// to get any params for the plugin
			
			$pluginParams = "";			

			//get global params from config
			$parms=array();
			$matches = array();
			preg_match_all( $regex, $article->text, $matches, PREG_SET_ORDER );
			$i=1;
				foreach ($matches as $elm) {

				//replacing the string using str_replace
				$line=str_replace("&nbsp;", " ", $elm[2]);
				$line=str_replace(" ", "&", $line);
				$line=str_replace('"', " ",$line);

				parse_str( $line,$parms );

				//call the getiDEALSingle function, here we call the $html variable
				$html=$this->getiDEALSingle($parms,$pluginParams,1,$i);
				$i++;

				//matching the filtering the article text using $regex variable,searching the value {ccidealplatform}
				$article->text = preg_replace($regex, $html, $article->text, 1);
					}

				return true;
	}

	public function random_generator($digits){
		srand ((double) microtime() * 10000000);
		//Array of number
		$input = array ("1","2","3","4","5","6","7","8","9","0");

		$random_generator="";// Initialize the string to store random numbers
			for($i=1;$i<$digits+1;$i++){ // Loop the number of times of required digits

				if(rand(1,2) == 1){// to decide the digit should be numeric or alphabet
					// Add one random alphabet
					$rand_index = array_rand($input);
					$random_generator .=$input[$rand_index]; // One char is added

				}else{

					// Add one numeric digit between 1 and 10
					$random_generator .=rand(1,10); // one number is added
				} // end of if else

			} // end of for loop

			return $random_generator;
		}

	function getiDEALSingle($params, $pluginParams, $debugmode=0,$count) {

			global $Itemid, $mainframe;

			if($Itemid==""){
				$Itemid = JRequest::getVar('Itemid');
			}
			$path = JURI::base();

			// printf("<pre>%s</pre>", print_r($pluginParams,1));
			$classsuffix=isset($params['classsfx']) ? $params['classsfx'] : "";

			//print_r($params);
			$refer_id= $this->random_generator(3);
			$date=date("Ymy");
			$articleid = JRequest::getVar('id');

			//Getting all the values using params

			$amount=@$params['amount'];

			$title=@$params['title'];
			$title= str_replace("_"," ",$title);

			$amount_text=@$params['amount_text'];
			$amount_text=str_replace("_"," ",$amount_text);

			$payid_text=@$params['paymentid_text'];
			$payid_text=str_replace("_"," ",$payid_text);

			$payment_id=$refer_id.$date;

			$extra_text =@$params['extra_text'];
	 		$extra_text	=str_replace("_"," ",$extra_text);

			$submit_button=@$params['submit_button'];
			$submit_button=str_replace("_"," ",$submit_button);

			// David: default labels if not defined in article
			if(empty($title)){$title = "Betaal met iDEAL";}
			if(empty($amount_text)){$amount_text = "Bedrag:";}
			if(empty($payid_text)){$payid_text = "Kenmerk:";}
			if(empty($extra_text)){$extra_text = "Informatie:";}
			if(empty($submit_button)){$submit_button = "Verder";}

			//Fetching image from administrator folder
			$img = JURI::root().'components/com_ccidealplatform/assets/images/select_iDEAL.jpg';

			//Fetching the css from administrator folder
			$document = JFactory::getDocument();
			$document->addStyleSheet('components/com_ccidealplatform/assets/css/style.css');			

			?>
			<script src="<?php echo JURI::root();?>components/com_ccidealplatform/assets/js/content_validate.js" type="text/javascript"></script>
			<?php		

			// From using $html variable,we are displaying the whole process

			@$html .="<div class='akeeba-bootstrap'><div class='ccidealplatform_content_plugin_box'>";

			@$html .="\n<form title='$title' action='index.php?option=com_ccidealplatform&amp;view=ccideal&amp;layout=content_redirect' method='post' id='content_ideal' name='content_ideal' style=\"display:visible;\">";

			@$html .="<div class='ccidealplatform_content_plugin_title' style='text-align: center;'>";
			@$html .=$title;
			@$html .="</div>";

			// iDEAL image
			@$html .="<div class='ccidealplatform_content_plugin_image'>";
			@$html .="<img src='$img' alt='' />";
			@$html .="</div>";

			@$html .="<div class='ccidealplatform_content_plugin_fieldset'>";
			@$html .="<table class='idealcontent'>";

				// Matching the variable using preg_match for amount_by_user

					if (@$params['amount_by_user']=="" && @$params['amount']==""){
						@$html .="\n<td class='ccidealplatform_content_plugin_labels'>$amount_text &#8364;</td><td class='ccidealplatform_content_plugin_labels'><input type='text' name='total' onkeyup=\"ajaxFunction(this.form,'amount_by_user',$count)\" onblur=\"ajaxFunction(this.form,'amount_by_user',$count)\" id='amount_by_user' class='ccidealplatform_content_plugin_fields' value=''/>\n";
						@$html .="<span id='ajaxDivname$count' style='display:none;float:right;'><img src='$path"."components/com_ccidealplatform/assets/images/cross.png' width='16' height='16' alt='' /></span><span id='ajaxDivname1$count' style='display:none;float:right;'><img src='$path"."components/com_ccidealplatform/assets/images/tick.png' width='16' height='16' alt='' /></span></td></tr>";
						$temp_value = 1;
						$a = 1;
					}
					else{

				// Condition for single amount or multiple amount
						
						$c=explode(",",$amount);

						//If count of value is 'one' then it will single amount
						if(count($c)==1){
							@$html .="<tr>";
							@$html .="\n<td class='ccidealplatform_content_plugin_labels'>$amount_text</td><td class='ccidealplatform_content_plugin_labels'>&#8364;$c[0]</td>";
							$date=date("mYmd");
							$ars= base64_encode($c[0].$date);
							@$html .="\n<input type='hidden' name='grandtotal' id=\"single_amt\" value='$ars' />\n</tr>";
					}
					else{

						//Displaying multiple amount in drop down list
						@$html .="<tr>";

						@$html .="<td class='ccidealplatform_content_plugin_labels'>$amount_text</td><td class='ccidealplatform_content_plugin_labels'>";
						@$html .= '<select name="grandtotal" id="multiple_amt">';
					for($i=0;$i<count($c);$i++){
						$ar=$c[$i];
						$date=date("mYmd");

						//coverting the amount into base64_encode
						$ars= base64_encode($ar.$date);
						@$html .="<option value='$ars'>&#8364; $ar</option>";
					}
						@$html .= '</select>';
						@$html .="</td></tr>";
				}

			}

                // Payment ID is not yet set, change to 1 if set
                $paymentid_set=0;

                //No longer needed to define paymentid_none, value for payment_id added automatically
//				//Matching the variable using preg_match for paymentid_none
//                $paymentnone=0;
//				$keys = array_keys(@$params,@$params['paymentid_none']);
//				 for($i=0;$i<=count($keys);$i++){
//					if (@preg_match("/\bpaymentid_none\b/i",trim($keys[$i]))){
//                        $paymentnone=1;
//					@$html .="\n<tr><td><input type='hidden' name='ordernumber' value='$payment_id' /></td></tr>\n";
//                        $paymentid_set=1;
//					}
//				 }

				 //Matching the variable using preg_match for paymentid_by_user
				$paymentuser=0;
				 $keys = array_keys(@$params,@$params['paymentid_by_user']);
				  for($i=0;$i<=count($keys);$i++){
					 if (@preg_match("/\bpaymentid_by_user\b/i",trim($keys[$i])) && !@preg_match("/\bpaymentid\b/i",trim($keys[$i]))){ //Matching the variable using preg_match for paymentid_by_user
						$paymentuser=1;
						@$html .="<tr><td class='ccidealplatform_content_plugin_labels'>$payid_text</td>";
						@$html .="\n<td class='ccidealplatform_content_plugin_labels'><input type='text' name='cont_extn' onkeyup=\"ajaxFunction(this.form,'paymentid_by_user',$count)\" onblur=\"ajaxFunction(this.form,'paymentid_by_user',$count)\" id='paymentid_by_user' value='' maxlength='14' class=\"inputbox required ccidealplatform_content_plugin_fields\"/>\n";
						@$html .="<span id='ajaxDivpayuser$count' style='display:none;float:right;'><img src='$path"."components/com_ccidealplatform/assets/images/cross.png' width='16' height='16' alt='' /></span><span id='ajaxDivpayuser1$count' style='display:none;float:right;'><img src='$path"."components/com_ccidealplatform/assets/images/tick.png' width='16' height='16' alt='' /></span></td>";
						@$html .="\n<td><input type='hidden' name='hidden_order_number' style='width:85px;' value='$payment_id' />\n</td></tr>";
						$temp_value = 2;
						$b = 1;
                         $paymentid_set=1;
					}
				  }

				 //Matching the variable using preg_match for paymentid
				  $paymentid=0;
				  $keys = array_keys(@$params,@$params['paymentid']);
				  for($i=0;$i<=count($keys);$i++){

					if (@preg_match("/\bpaymentid\b/i",trim($keys[$i])) && !@preg_match("/\bpaymentid_by_user\b/i",
                            trim($keys[$i])) && $paymentuser!=1){ // else condition for default payment id
						$paymentid=1;
						@$html .="<td class='ccidealplatform_content_plugin_labels'>$payid_text</td>";
						@$paymentid=$params['paymentid'];
						@$paymentid=str_replace("_"," ",$paymentid);
						@$html .="<td class='ccidealplatform_content_plugin_labels'>$paymentid</td>";
						@$html .="\n<input type='hidden' name='cont_extn' value='$paymentid' />\n";
						@$html .="\n<input type='hidden' name='hidden_order_number' value='$payment_id' />\n";
                        $paymentid_set=1;
					}
				  }

            // David: automatically set a payment id if none is set in article tag
            if ($paymentid_set==0)
            {
                @$html .= "\n<tr><td><input type='hidden' name='ordernumber' value='$payment_id' /></td></tr>\n";
            }

			 //Matching the variable using preg_match for extra_text

			$extra_area=0;

			   $keys = array_keys(@$params,@$params['extra_textfield']);
			   for($i=0;$i<=count($keys);$i++){

                   // condition for default extra_textfield
					if (@preg_match("/\bextra_textfield.*\b/i",trim($keys[$i])) ){
 					 	 $extra_area=1;
 						@$html 				.="<tr><td class='ccidealplatform_content_plugin_labels'>$extra_text</td>";
 						@$extra_textfield	=$params['extra_textfield'];
 						@$html 				.="$extra_textfield";
 						@$html 				.="\n<td class='ccidealplatform_content_plugin_labels'><input type='text' name='extra_textfield'  onkeyup=\"ajaxFunction(this.form,'extra_textfield',$count)\" onblur=\"ajaxFunction(this.form,'extra_textfield',$count)\" id='extra_textfield' value='' maxlength='100' />\n";
						@$html 				.="<span id='ajaxDiv_textfield$count' style='display:none;float:right;'><img src='$path"."components/com_ccidealplatform/assets/images/cross.png' width='16' height='16' alt='' /></span><span id='ajaxDiv_textfield1$count' style='display:none;float:right;'><img src='$path"."components/com_ccidealplatform/assets/images/tick.png' width='16' height='16' alt='' /></span></td></tr>";

                    }
				  }


			 //Matching the variable using preg_match for extra_textfield_required

			   $keys = array_keys(@$params,@$params['extra_textfield_required']);
			   for($i=0;$i<=count($keys);$i++){

					if ((@preg_match("/\bextra_textfield_required.*\b/i",trim($keys[$i])) /*&& !@preg_match
                            ("/\bextra_textarea_required\b/i",trim($keys[$i])) && !(@preg_match
 ("/\bextra_textfield\b/i",trim($keys[$i]))) && !(@preg_match("/\bextra_textarea\b/i",trim($keys[$i]))))&&
 $extra_area !=1*/ ))
                    { // else condtion for extra_textfield_required
						$extra_area =2;
 						@$html 				.="<tr><td class='ccidealplatform_content_plugin_labels'>$extra_text</td>";
 						@$extra_textfield	=$params['extra_textfield_required'];
 						@$html 				.="$extra_textfield";
 						@$html 				.="\n<td class='ccidealplatform_content_plugin_labels'><input type='text' name='extra_textfield' onkeyup=\"ajaxFunction(this.form,'extra_textfield_required',$count)\" onblur=\"ajaxFunction(this.form,'extra_textfield_required',$count)\" id='extra_textfield_required' value='' maxlength='100' />*\n";
						@$html 				.="<span id='ajaxDiv_textfield_req$count' style='display:none;float:right;'><img src='$path"."components/com_ccidealplatform/assets/images/cross.png' width='16' height='16' alt='' /></span><span id='ajaxDiv_textfield_req1$count' style='display:none;float:right;'><img src='$path"."components/com_ccidealplatform/assets/images/tick.png' width='16' height='16' alt='' /></span></td></tr>";
						$temp_value = 4;
						$c=1;
					}
				  }

			//Matching the variable using preg_match for extra_textarea

			   $keys = array_keys(@$params,@$params['extra_textarea']);
			   for($i=0;$i<=count($keys);$i++){

					if ((@preg_match("/\bextra_textarea.*\b/i",trim($keys[$i])) /*&& !@preg_match
                        ("/\bextra_textarea_required\b/i",trim($keys[$i])) && !(@preg_match("/\bextra_textfield\b/i",
                            trim($keys[$i]))) && !(@preg_match("/\bextra_textfield_required\b/i",trim($keys[$i]))) &&
                        $extra_area!=2 && $extra_area!=1*/) ){ // else condtion for extra_textarea
						$extra_area =3;
 						@$html 				.="<tr><td class='ccidealplatform_content_plugin_labels'>$extra_text</td>";
 						@$extra_textfield	=$params['extra_textarea'];
 						@$html 				.="$extra_textfield";
 						@$html 				.="\n<td class='ccidealplatform_content_plugin_labels'><textarea name='extra_textfield' onkeyup=\"ajaxFunction(this.form,'extra_textarea',$count)\" onblur=\"ajaxFunction(this.form,'extra_textarea',$count)\" id='extra_textarea' class='ccidealplatform_textareacontent'></textarea>\n";
						@$html 				.="<span id='ajaxDiv_textarea$count' style='display:none;float:right;'><img src='$path"."components/com_ccidealplatform/assets/images/cross.png' width='16' height='16' alt='' /></span><span id='ajaxDiv_textarea1$count' style='display:none;float:right;'><img src='$path"."components/com_ccidealplatform/assets/images/tick.png' width='16' height='16' alt='' /></span></td></tr>";
					}
				  }

		  	//Matching the variable using preg_match for extra_textarea_required

			   $keys = array_keys(@$params,@$params['extra_textarea_required']);
			   for($i=0;$i<=count($keys);$i++){

					if ((@preg_match("/\bextra_textarea_required.*\b/i",trim($keys[$i])) /*&& !@preg_match
                            ("/\bextra_textarea \b/i",trim($keys[$i])) && !(@preg_match("/\bextra_textfield\b/i",trim
 ($keys[$i]))) && !(@preg_match("/\bextra_textfield_required\b/i",trim($keys[$i]))))&&$extra_area!=2 &&
 $extra_area!=1 &&$extra_area!=3 */)){ // else else condtion for extra_textarea_required
						$extra_area = 4;
 						@$html 				.="<tr><td class='ccidealplatform_content_plugin_labels'>$extra_text</td>";
 						@$extra_textfield	=$params['extra_textarea_required '];
 						@$html 				.="$extra_textfield";
 						@$html 				.="\n<td class='ccidealplatform_content_plugin_labels'><textarea name='extra_textfield' onkeyup=\"ajaxFunction(this.form,'extra_textarea_required',$count)\" onblur=\"ajaxFunction(this.form,'extra_textarea_required',$count)\" id='extra_textarea_required' class='ccidealplatform_textareacontent'></textarea>*\n";
						@$html 				.="<span id='ajaxDiv_textarea_req$count' style='display:none;float:right;'><img src='$path"."components/com_ccidealplatform/assets/images/cross.png' width='16' height='16' alt='' /></span><span id='ajaxDiv_textarea_req1$count' style='display:none;float:right;'><img src='$path"."components/com_ccidealplatform/assets/images/tick.png' width='16' height='16' alt='' /></span></td></tr>";
						$temp_value = 5;
						$d=1;
					}
				  }

			// Identification of form

				if(@$temp_value == 1){
					@$temp_value = 1;

				}elseif(@$temp_value == 2){

					@$temp_value = 2;
					if(@$a == 1 && @$b ==1 &&$c=1 && $d=1){
							@$temp_value = 3;
					}
				}else if(@$temp_value == 4){
					@$temp_value = 4;

				}else if(@$temp_value == 5){
					@$temp_value = 5;

				}else{
					@$temp_value = 0;
				}			

				//For menu Item ID
				$app = JFactory::getApplication();
	   			$menu = $app->getMenu();
				$menuItem = $menu->getActive();
				
				//Passing the variable as hidden

				@$html .="<tr ><td colspan='2'>\n<input type='submit' name='submit' onclick='return submitbutton(this.form,$count, $temp_value)' id='content_plugin_submit' class='btn btn-success' value='$submit_button' /></td></tr>";
				@$html .="</table></div>";

				@$html .="\n<input type='hidden' name='itemid' value='$Itemid'/>\n";
				@$html .="<input type=\"hidden\" name=\"path\" id=\"path\" value='$path' />";
				@$html .="\n<input type='hidden' name='id' value='$articleid'/>\n";
				

				@$html .="\n<input type='hidden' name='extn' value='Content_plugin' />";
				@$html .="\n<input type='hidden' name='task' value='idealRedirect'/>";
				@$html .="<input id=\"hidden_org\" type=\"hidden\" name=\"hidden_org\" class=\"inputbox required\" value=\"\"/>";
				@$html .="\n<input type='hidden' name='myMenuItemID' value='$menuItem->id'/>";
				@$html .="\n</form>";

				@$html .="</div></div><br style='clear:both;' />";

			return $html;
	}

}
