<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license. 
**/

defined('_JEXEC') or die('Restricted access'); ?>

<?php echo JText::_($vars->display_name); ?>
<br />
<?php echo JText::_($vars->onbeforepayment_text); 

$date	= date("md");
$total 	= $date.$vars->orderpayment_id;

?>
<form action='<?php echo $vars->post_url; ?>' method='post'>
	<input type="hidden" name="grandtotal" id="grandtotal" value="<?php echo $vars->orderpayment_amount;?>"  />
	<input type="hidden" name="ordernumber" id="ordernumber" value="<?php echo $total; ?>" />
	<input type="hidden" name="extn" id="extn" value="j2store" />
	
	<input type="submit" class="btn btn-primary button" value="<?php echo JText::_($vars->button_text); ?>" />
</form>