<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license. 
**/

/** ensure this file is being included by a parent file */
defined('_JEXEC') or die('Restricted access');

require_once (JPATH_ADMINISTRATOR.'/components/com_j2store/library/plugins/payment.php');
require_once (JPATH_ADMINISTRATOR.'/components/com_j2store/library/prices.php');
require_once (JPATH_SITE.'/components/com_j2store/helpers/utilities.php');

class plgJ2StorePayment_ccideal extends J2StorePaymentPlugin
{
	/**
	 * @var $_element  string  Should always correspond with the plugin's filename,
	 *                         forcing it to be unique
	 */
    var $_element    = 'payment_ccideal';
    var $_isLog      = false;
    var $_j2version = null;

	
	function plgJ2StorePayment_ccideal(& $subject, $config) {
		parent::__construct($subject, $config);
		$this->loadLanguage( '', JPATH_ADMINISTRATOR );
		$this->_j2version = $this->getVersion();
	}	

	function onJ2StoreGetPaymentOptions($element, $order){
		// Check if this is the right plugin
		if (!$this->_isMe($element)){
			return null;
		}
		
		$status = true;
		// if this payment method should be available for this order, return true
		// if not, return false.
		// by default, all enabled payment methods are valid, so return true here,
		// but plugins may override this
		if( version_compare( $this->_j2version, '2.6.7', 'ge' ) ) {
			//$order = JTable::getInstance('Orders', 'Table');
			$order->setAddress();
			$address = $order->getBillingAddress();
			$geozone_id = $this->params->get('geozone_id', '0');
			//get the geozones
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('gz.*,gzr.*')->from('#__j2store_geozones AS gz')
			->innerJoin('#__j2store_geozonerules AS gzr ON gzr.geozone_id = gz.geozone_id')
			->where('gz.geozone_id='.$geozone_id )
			->where('gzr.country_id='.$db->q($address['country_id']).' AND (gzr.zone_id=0 OR gzr.zone_id='.$db->q($address['zone_id']).')');
			$db->setQuery($query);
			$grows = $db->loadObjectList();
			if (!$geozone_id ) {
				$status = true;
			} elseif ($grows) {
				$status = true;
			} else {
				$status = false;
			}
		}
		return $status;
	}
	
	function _beforePayment($order) {
		//get surcharge if any
		$surcharge = 0;

		$surcharge_percent = $this->params->get('surcharge_percent', 0);
		$surcharge_fixed = $this->params->get('surcharge_fixed', 0);
		if((float) $surcharge_percent > 0 || (float) $surcharge_fixed > 0) {

			//percentage
			if((float) $surcharge_percent > 0) {
				$surcharge += ($order->order_total * (float) $surcharge_percent) / 100;
			}

			if((float) $surcharge_fixed > 0) {
				$surcharge += (float) $surcharge_fixed;
			}
			//make sure it is formated to 2 decimals

			$order->order_surcharge = round($surcharge, 2);
			$order->calculateTotals();
		}
	}

    /**
     * @param $data     array       form post data
     * @return string   HTML to display
     */
    function _prePayment( $data )
    {
		// get component params
		$params = JComponentHelper::getParams('com_j2store');

        // prepare the payment form

        $vars = new JObject();
        $vars->order_id = $data['order_id'];
        $vars->orderpayment_id = $data['orderpayment_id'];
        JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_j2store/tables');
        $order = JTable::getInstance('Orders', 'Table');
        $order->load($data['orderpayment_id']);
        $currency_values= $this->getCurrency($order);

        $vars->currency_code =$currency_values['currency_code'];
        $vars->orderpayment_amount = $this->getAmount($order->orderpayment_amount, $currency_values['currency_code'], $currency_values['currency_value'], $currency_values['convert']);

        $vars->orderpayment_type = $this->_element;

        $vars->cart_session_id = JFactory::getSession()->getId();

        $vars->display_name = $this->params->get('display_name', 'PAYMENT_CCIDEAL');
        $vars->onbeforepayment_text = $this->params->get('onbeforepayment', '');
        $vars->button_text = $this->params->get('button_text', 'J2STORE_PLACE_ORDER');

        $vars->post_url = $this->_getPostUrl();        

        if(isset($order->invoice_number) && $order->invoice_number > 0) {
        	$invoice_number = $order->invoice_prefix.$order->invoice_number;
        }else {
        	$invoice_number = $order->id;
        }
        $vars->invoice = $invoice_number;

        $html = $this->_getLayout('prepayment', $vars);
        return $html;
    }

    /**
     * Processes the payment form
     * and returns HTML to be displayed to the user
     * generally with a success/failed message
     *
     * @param $data     array       form post data
     * @return string   HTML to display
     */
	 
	//Not removed, because of later implemtataion 
    /*function _postPayment( $data )
    {	
	
        // Process the payment
        $paction = JRequest::getVar('paction');	

        $vars = new JObject();

        switch ($paction)
        {
            case "display_message":
            	$session = JFactory::getSession();
            	$session->set('j2store_cart', array());
                $vars->message = JText::_($this->params->get('onafterpayment', ''));
                $html = $this->_getLayout('message', $vars);
                $html .= $this->_displayArticle();

              break;
            case "process":
                $vars->message = $this->_process();
                $html = $this->_getLayout('message', $vars);
                echo $html; // TODO Remove this
                $app = JFactory::getApplication();
                $app->close();
              break;
            case "cancel":
                $vars->message = JText::_($this->params->get('oncancelpayment', ''));
                $html = $this->_getLayout('message', $vars);
              break;
            default:
                $vars->message = JText::_($this->params->get('onerrorpayment', ''));
                $html = $this->_getLayout('message', $vars);
              break;
        }

        return $html;
    }*/

    /**
     * Prepares variables for the payment form
     *
     * @return unknown_type
     */
    function _renderForm( $data )
    {
        $user = JFactory::getUser();
        $vars = new JObject();
        $vars->onselection_text = $this->params->get('onselection', '');
        $html = $this->_getLayout('form', $vars);

        return $html;
    }

    /************************************
     * Note to 3pd:
     *
     * The methods between here
     * and the next comment block are
     * specific to this payment plugin
     *
     ************************************/

    /**
     * Gets the Paypal gateway URL
     *
     * @param boolean $full
     * @return string
     * @access protected
     */
    function _getPostUrl($full = true)
    {
        $url = JURI::root().'index.php?option=com_ccidealplatform&task=bankform';      
		
        return $url;
    }   

    /**
     * Sends error messages to site administrators
     *
     * @param string $message
     * @param string $paymentData
     * @return boolean
     * @access protected
     */
    function _sendErrorEmails($message, $paymentData)
    {
        $mainframe = JFactory::getApplication();

        // grab config settings for sender name and email
        $config     = JComponentHelper::getParams('com_j2store');
        $mailfrom   = $config->get( 'emails_defaultemail', $mainframe->getCfg('mailfrom') );
        $fromname   = $config->get( 'emails_defaultname', $mainframe->getCfg('fromname') );
        $sitename   = $config->get( 'sitename', $mainframe->getCfg('sitename') );
        $siteurl    = $config->get( 'siteurl', JURI::root() );

        $recipients = $this->_getAdmins();
        $mailer = JFactory::getMailer();

        $subject = JText::sprintf('J2STORE_CCIDEAL_EMAIL_PAYMENT_NOT_VALIDATED_SUBJECT', $sitename);

        foreach ($recipients as $recipient)
        {
            $mailer = JFactory::getMailer();
            $mailer->addRecipient( $recipient->email );

            $mailer->setSubject( $subject );
            $mailer->setBody( JText::sprintf('J2STORE_CCIDEAL_EMAIL_PAYMENT_FAILED_BODY', $recipient->name, $sitename, $siteurl, $message, $paymentData) );
            $mailer->setSender(array( $mailfrom, $fromname ));
            $sent = $mailer->send();
        }

        return true;
    }

    /**
     * Gets admins data
     *
     * @return array|boolean
     * @access protected
     */
    function _getAdmins()
    {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('u.name,u.email');
        $query->from('#__users AS u');
        $query->join('LEFT', '#__user_usergroup_map AS ug ON u.id=ug.user_id');
        $query->where('u.sendEmail = 1');
        $query->where('ug.group_id = 8');

        $db->setQuery($query);
        $admins = $db->loadObjectList();
        if ($error = $db->getErrorMsg()) {
            JError::raiseError(500, $error);
            return false;
        }

        return $admins;
    }

    function getCurrency($order) {
		$results = array();
		$convert = false;
		$params = JComponentHelper::getParams('com_j2store');

    	if( version_compare( $this->_j2version, '2.6.7', 'lt' ) ) {
    		$currency_code = $params->get('currency_code', 'USD');
    		$currency_value = 1;
    	} else {

    		include_once (JPATH_ADMINISTRATOR.'/components/com_j2store/library/base.php');
    		$currencyObject = J2StoreFactory::getCurrencyObject();

    		$currency_code = $order->currency_code;
    		$currency_value = $order->currency_value;

    		//accepted currencies
    		$currencies = $this->getAcceptedCurrencies();
    		if(!in_array($order->currency_code, $currencies)) {
    			$default_currency = 'USD';
    			if($currencyObject->has($default_currency)) {
    				$currencyObject->set($default_currency);
    				$currency_code = $default_currency;
    				$currency_value = $currencyObject->getValue($default_currency);
    				$convert = true;
    			}
    		}
    	}
    	$results['currency_code'] = $currency_code;
    	$results['currency_value'] = $currency_value;
    	$results['convert'] = $convert;

    	return $results;
    }

    function getAmount($value, $currency_code, $currency_value, $convert=false) {

    	if( version_compare( $this->_j2version, '2.6.7', 'lt' ) ) {
    		return J2StoreUtilities::number( $value, array( 'thousands'=>'', 'num_decimals'=>'2', 'decimal'=>'.') );
    	} else {
    		include_once (JPATH_ADMINISTRATOR.'/components/com_j2store/library/base.php');
    		$currencyObject = J2StoreFactory::getCurrencyObject();
    		$amount = $currencyObject->format($value, $currency_code, $currency_value, false);
    		return $amount;
    	}
    }

    function getVersion() {

    	if(is_null($this->_j2version)) {
    		$xmlfile = JPATH_ADMINISTRATOR.'/components/com_j2store/manifest.xml';
    		$xml = JFactory::getXML($xmlfile);
    		$this->_j2version=(string)$xml->version;
    	}
    	return $this->_j2version;
    }

    function getAcceptedCurrencies() {
    	$currencies = array('EUR');
		
    	return $currencies;
    }

    function getOrderPaymentID($order_id) {

    	$db = JFactory::getDbo();
    	$query = $db->getQuery(true)->select('id')->from('#__j2store_orders')->where('order_id='.$db->q($order_id));
    	$db->setQuery($query);
    	return $db->loadResult();

    }	
}