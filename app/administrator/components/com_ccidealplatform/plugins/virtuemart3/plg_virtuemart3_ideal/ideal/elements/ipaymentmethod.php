<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

defined('_JEXEC') or die();


jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

class JFormFieldipaymentmethod extends JFormField {
   
	protected $type = 'ipaymentmethod';
	
	protected function getInput() {
		
		$db =  JFactory::getDBO();
		
		$paymethod = "";
		
		$query = 'SELECT `IDEAL_Bank` FROM `#__ccidealplatform_config` WHERE `id` = 1';
		$db->setQuery($query);
        $IDEAL_Bank = $db->loadResult();		

        $query = 'SELECT `method_value` AS value, `method_name` AS text FROM `#__ccidealplatform_paymentmethod`
               		WHERE `account_id` = "'.$IDEAL_Bank.'" ORDER BY `id` ASC ';

        $db->setQuery($query);
        $fields = $db->loadObjectList();
		
		$cid = JRequest::getVar('cid'); 
		$paymentmethod = $cid['0'];
		
		$query = 'SELECT `payment_params` FROM `#__virtuemart_paymentmethods` WHERE `virtuemart_paymentmethod_id` = "'.$paymentmethod.'"';
		$db->setQuery($query);
        $pay_current_params = $db->loadResult();
		
		$data = explode('|',$pay_current_params);		
		
		foreach($data as $value){			
			$arr = explode('=',$value);
			if($arr['0'] == "ideal_paymentmethod"){
				$paymethod = trim(str_replace('"','',$arr['1']));
			}			
		}
        
        return JHTML::_('select.genericlist', $fields, 'params[ideal_paymentmethod]', 'id="paramsideal_paymentmethod"', 'value', 'text',$paymethod);
	}
}