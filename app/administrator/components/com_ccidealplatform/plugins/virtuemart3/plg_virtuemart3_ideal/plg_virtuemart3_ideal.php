<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

defined('_JEXEC') or die('Restricted access');
if (!class_exists('vmPSPlugin')) {
	require(JPATH_VM_PLUGINS . DS . 'vmpsplugin.php');
}

$lang = JFactory::getLanguage();

$extension = 'plg_vmpayment3_iDEAL';
$base_dir = JPATH_ADMINISTRATOR;
$language_tag = $lang->getTag();
$lang->load($extension, $base_dir, $language_tag, true);

class plgVMPaymentplg_virtuemart3_ideal extends vmPSPlugin {	

	function __construct(& $subject, $config) {
		
		parent::__construct($subject, $config);

		$this->_loggable = true;
		$this->tableFields = array_keys($this->getTableSQLFields());
		$this->_tablepkey = 'id'; //virtuemart_ideal_id';
		$this->_tableId = 'id'; //'virtuemart_ideal_id';
		$varsToPush = array('ideal_merchant_email' => array('', 'char'),
			'ideal_verified_only' => array('', 'int'),
			'payment_currency' => array('', 'int'),
			'payment_logos' => array('', 'char'),
			'debug' => array(0, 'int'),
			'status_pending' => array('', 'char'),
			'status_success' => array('', 'char'),
			'status_canceled' => array('', 'char'),

			//Restrictions
			'countries' => array('', 'char'),
			'min_amount' => array('', 'float'),
			'max_amount' => array('', 'float'),

			'secure_post' => array('', 'int'),
			'no_shipping' => array('', 'int'),
			'address_override' => array('', 'int'),

			//discount
			'cost_per_transaction' => array('', 'float'),
			'cost_percent_total' => array('', 'char'),
			'tax_id' => array(0, 'int'),

			'ideal_paymentmethod' => array('', 'char')
		);

		$this->setConfigParameterable($this->_configTableFieldName, $varsToPush);	
	}

	public function getVmPluginCreateTableSQL() {

		return $this->createTableSQL('Payment iDEAL Table');
	}

	function getTableSQLFields() {

		$SQLfields = array(
			'id' => ' INT(11) unsigned NOT NULL AUTO_INCREMENT ',
			'virtuemart_order_id' => 'int(1) UNSIGNED',
			'order_number' => 'char(64)',
			'virtuemart_paymentmethod_id' => 'mediumint(1) UNSIGNED',
			'payment_name' => 'varchar(5000)',
			'payment_order_total' => 'decimal(15,5) NOT NULL',
			'payment_currency' => 'smallint(1)',
			'cost_per_transaction' => 'decimal(10,2)',
			'cost_percent_total' => 'decimal(10,2)',
			'tax_id' => 'smallint(1)'
		);
		return $SQLfields;
	}

	function plgVmConfirmedOrder($cart, $order) {

        if(!$_SESSION['vmlangu']){
            $_SESSION['vmlangu'] = VmConfig::get('vmlang');
        }
		VmConfig::loadJLang('com_virtuemart_orders', TRUE);

		if (!($this->_currentMethod = $this->getVmPluginMethod($order['details']['BT']->virtuemart_paymentmethod_id))) {
			return null;
		}
		if (!$this->selectedThisElement($this->_currentMethod->payment_element)) {
			return FALSE;
		}
		if (!class_exists('VirtueMartModelOrders')) {
			require(VMPATH_ADMIN . DS . 'models' . DS . 'orders.php');
		}
		if (!class_exists('VirtueMartModelCurrency')) {
			require(VMPATH_ADMIN . DS . 'models' . DS . 'currency.php');
		}

		// Log information to Virtuemart log
		$session = JFactory::getSession();
		$return_context = $session->getId();
		$this->_debug = $this->_currentMethod->debug;
		$this->logInfo('plgVmConfirmedOrder order number: ' . $order['details']['BT']->order_number, 'message');

		// Get data that we need to store
		$this->getPaymentCurrency($this->_currentMethod);
		$payment_name = $this->renderPluginName($this->_currentMethod, $order);
		$payment_method = $this->getVmPluginMethod ($cart->virtuemart_paymentmethod_id);
		$paymentCurrency = CurrencyDisplay::getInstance($this->_currentMethod->payment_currency);
		$totalInPaymentCurrency = round($paymentCurrency->convertCurrencyTo($this->_currentMethod->payment_currency, $order['details']['BT']->order_total, false), 2);

		// Prepare data that should be stored in the database
		$dbValues['order_number'] = $order['details']['BT']->order_number;
		$dbValues['payment_name'] = $payment_name;
		$dbValues['virtuemart_paymentmethod_id'] = $cart->virtuemart_paymentmethod_id;
		$dbValues['ideal_custom'] = $return_context;
		$dbValues['cost_per_transaction'] = $this->_currentMethod->cost_per_transaction;
		$dbValues['cost_percent_total'] = $this->_currentMethod->cost_percent_total;
		$dbValues['payment_currency'] = $this->_currentMethod->payment_currency;
		$dbValues['payment_order_total'] = $totalInPaymentCurrency;
		$dbValues['tax_id'] = $this->_currentMethod->tax_id;
		$this->storePSPluginInternalData($dbValues);
		VmConfig::loadJLang('com_virtuemart_orders', TRUE);

		$url = $this->_getiDEALUrlHttps($this->_currentMethod);

		$post_variables = Array(
			'ordernumber' => $order['details']['BT']->order_number,
			'extn' => JText::_('virtuemart') ,
			"grandtotal" => $totalInPaymentCurrency,
			"ideal_paymentmethod" => $this->_currentMethod->ideal_paymentmethod,
		);

		$html 	 = '<html><head><title>Redirection</title></head><body><div style="margin: auto; text-align: center;">';
		$html	.= '<form action="' . $url . '" method="post" name="vm_ideal_form" >';
		$html	.= '<input type="submit"  value="'.JTEXT::_('VMPAYMENT_IDEAL_PLEASE_WAIT').'" />';
		foreach ($post_variables as $name => $value) {
			$html	.= '<input type="hidden" name="' . $name . '" value="' . htmlspecialchars($value) . '" />';
		}

		if(empty($_SESSION['iDEALVMPayment_SESSION'])){
			$_SESSION['iDEALVMPayment_SESSION']=date("yidmYi");
		}
		if(!empty($_SESSION['iDEALVMPayment_SESSION'])){
			$_SESSION['iDEALVMPayment_SESSION']="";
			$_SESSION['iDEALVMPayment_SESSION']=date("yidmYi");
		}
		$html	.= '<input type="hidden" name="iDEALVMPayment_SESSION" value="' . $_SESSION['iDEALVMPayment_SESSION'] . '" />';
		$html	.= '<input type="hidden" name="payment_custom" value="' . $cart->virtuemart_paymentmethod_id . '" />';

		$html	.= '</form></div>';
		$html	.= '<script type="text/javascript">document.vm_ideal_form.submit();</script>';		
		$html	.= '</body></html>';		

        $modelOrder = VmModel::getModel ('orders');
        $order['order_status'] = $payment_method->status_pending;
		$order['customer_notified'] = 1;
		$order['comments'] = JText::sprintf('VMPAYMENT_IDEAL_ORDER_PLACED_START_PAYMENT', $payment_name);
        $modelOrder->updateStatusForOneOrder ($order['details']['BT']->virtuemart_order_id, $order, TRUE);

		$cart->_confirmDone = false;
		$cart->_dataValidated = false;
		$cart->setCartIntoSession();
		vRequest::setVar('html', $html);
	}

	function plgVmgetPaymentCurrency($virtuemart_paymentmethod_id, &$paymentCurrencyId) {

		if (!($this->_currentMethod = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
			return NULL; // Another method was selected, do nothing
		}
		if (!$this->selectedThisElement($this->_currentMethod->payment_element)) {
			return FALSE;
		}
		$this->getPaymentCurrency($this->_currentMethod);
		$paymentCurrencyId = $this->_currentMethod->payment_currency;
	}

	function plgVmOnPaymentResponseReceived(&$html) {

		if (!class_exists('VirtueMartCart')) {
			require(VMPATH_SITE . DS . 'helpers' . DS . 'cart.php');
		}
		if (!class_exists('shopFunctionsF')) {
			require(VMPATH_SITE . DS . 'helpers' . DS . 'shopfunctionsf.php');
		}
		if (!class_exists('VirtueMartModelOrders')) {
			require(VMPATH_ADMIN . DS . 'models' . DS . 'orders.php');
		}

		// Get the Joomla! or Virtuemart language
		VmConfig::loadJLang('com_virtuemart');
		VmConfig::loadJLang('com_virtuemart_orders', TRUE);
		if(!VmConfig::get('vmlang')){$vml = $_SESSION['vmlangu'];}else{$vml = VmConfig::get('vmlang');}
		VmConfig::set('vmlang',$vml);

		// Get the payment parameters from the URL
		$getparamsfromurl = JFactory::getApplication()->input;
		$lite = $getparamsfromurl->get('lite');
		$virtuemart_paymentmethod_id = $getparamsfromurl->get('pm', 0);
		$order_number = $getparamsfromurl->get('on', 0);

		// Verify the request is for our payment plugin
		if (!($this->_currentMethod = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
			return NULL; // Another method was selected, do nothing
		}		
		if (!$this->selectedThisElement($this->_currentMethod->payment_element)) {
			return NULL;
		}		
		if (!($virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($order_number))) {
			return NULL;
		}
		if($this->_currentMethod->payment_element != "plg_virtuemart3_ideal"){
			return NULL;
		}
		if(empty($_SESSION['iDEALVMPayment_SESSION'])){
			return NULL;
		}

		$modelOrder = VmModel::getModel ('orders');
		$order = $modelOrder->getOrder($virtuemart_order_id);
		$payment_name = $this->renderPluginName($this->_currentMethod);


		$app = JFactory::getApplication();
		$ideal_data = $app->input->getArray(array());

		// Is it a lite bank: ING Basic, Rabobank Lite, ABN AMRO Easy?
		// Make sure the status is never completed!
		// These banks do not support verified automatic status update!
		if($lite){
			$ideal_data['payment_status'] = 'Pending';
		}else{
			$ideal_data['payment_status'] = 'Completed';
		}

		if(empty($ideal_data) || $ideal_data==""){
			$ideal_data['invoice'] = 	$order_number;
			$ideal_data['custom']  =  	$virtuemart_paymentmethod_id;
		}

		if(!($paymentTable = $this->getDataByOrderId($virtuemart_order_id) )) {
			return '';
		}

		$html = $this->_getPaymentResponseHtml($paymentTable, $payment_name);

		if ($ideal_data['payment_status'] == 'Completed') {
			$order['order_status'] = $this->_currentMethod->status_success;
			$order['comments'] = JText::sprintf('VMPAYMENT_IDEAL_PAYMENT_STATUS_CONFIRMED', $order_number) . $payment_name;
		} else {
			// There should be no other statuses than completed here, because:
			// pending payments are not redirected to Virtuemart,
			// canceled payments are redirected to pluginUserPaymentCancel
			return null;
		}

		$order['customer_notified'] = 1;
		$order['details']['BT']->virtuemart_order_id = $order_number;

		$db = JFactory::getDBO();
		$query = "SELECT IDEAL_Bank FROM #__ccidealplatform_config" ;
		$db->setQuery($query);
		$ideal_config = $db->loadResult();

		// Only update order to completed if bank supports verified automatic status update
		// Ignore stupid ABN AMRO Easy, Rabobank Lite and ING Basic
		if(($ideal_config != "ABNAMROTEST") && ($ideal_config != "POSTBASIC") && ($ideal_config != "RABOBANKTEST")){

            // David - 01 May 2015 - cciDEAL 4.4.5
            // Added extra condition to make sure status is not updated for the same status twice
            // This makes sure there are no duplicate order comments or order confirmation emails
            $order_history =  array_pop($order['history']);
            if($order_history->order_status_code != $this->_currentMethod->status_success) {
                $modelOrder->updateStatusForOneOrder($virtuemart_order_id, $order);
            }
		}

		// We delete the previous session for VM cart and cciDEAL
		// Get the correct cart / session
		unset($_SESSION['iDEALVMPayment_SESSION']);
		$cart = VirtueMartCart::getCart();
		$cart->emptyCart();
        return $html;

	}

	function plgVmOnUserPaymentCancel() {	
	
		$db = JFactory::getDBO();
		$query = "SELECT IDEAL_Bank,IDEAL_pay_cancel_value,IDEAL_Status_Cancelled FROM #__ccidealplatform_config" ;
		$db->setQuery($query);
		$ideal_config = $db->loadObject();

		if (!class_exists('VirtueMartModelOrders'))
		require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );

		//$order_number = JRequest::getString('on', 0);

		$jinput = JFactory::getApplication()->input;
		$order_number = $jinput->get('on', 0);

		//echo $order_number;
		//exit;

		$virtuemart_paymentmethod_id = JRequest::getInt('pm', '');
		if (empty($order_number) or empty($virtuemart_paymentmethod_id) or !$this->selectedThisByMethodId($virtuemart_paymentmethod_id)) {
		    return null;
		}
		if (!($virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($order_number))) {
			return null;
		}
		if (!($paymentTable = $this->getDataByOrderId($virtuemart_order_id))) {
		    return null;
		}

		if($ideal_config->IDEAL_pay_cancel_value){
		VmInfo(Jtext::_('VMPAYMENT_IDEAL_PAYMENT_CANCELLED'));
		}else{
			VmInfo(Jtext::_($ideal_config->IDEAL_Status_Cancelled));
		}

		$session = JFactory::getSession ();
		$return_context = $session->getId ();
		
		if(($ideal_config->IDEAL_Bank != "ABNAMROTEST") && ($ideal_config->IDEAL_Bank != "POSTBASIC") && ($ideal_config->IDEAL_Bank != "RABOBANKTEST")){

			if (($return_context) && ($paymentTable)) {
				$this->handlePaymentUserCancel ($virtuemart_order_id); 
			}		

			$modelOrder = VmModel::getModel('orders');
			
			$payment = $this->getDataByOrderId($virtuemart_order_id);

			$method = $this->getVmPluginMethod($payment->virtuemart_paymentmethod_id);
			
			$order['order_status'] = $method->status_canceled;
			
			$order['customer_notified'] = 1;
			$order['comments'] = '';

			// When enabled creates "Creating default object from empty value" error,
			// disabled seems to have no affect
			//$order['details']['BT']->virtuemart_order_id = $order_number;
		
			$modelOrder->updateStatusForOneOrder($virtuemart_order_id, $order, true);			
		}
		
		return true;
	}

	function plgVmOnPaymentNotification() {

		if (!class_exists('VirtueMartModelOrders'))
		require( JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php' );
		$ideal_data = JRequest::get('post');
		if (!isset($ideal_data['invoice'])) {
			return;
		}
		$order_number = $ideal_data['invoice'];
		$virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($ideal_data['invoice']);		

		if (!$virtuemart_order_id) {
			return;
		}
		$vendorId = 0;
		$payment = $this->getDataByOrderId($virtuemart_order_id);

		$method = $this->getVmPluginMethod($payment->virtuemart_paymentmethod_id);
		if (!$this->selectedThisElement($method->payment_element)) {
			return false;
		}

		$this->_debug = $method->debug;
		if (!$payment) {
			$this->logInfo('getDataByOrderId payment not found: exit ', 'ERROR');
			return null;
		}
		$this->logInfo('ideal_data ' . implode('   ', $ideal_data), 'message');

		$this->_storeiDEALInternalData($method, $ideal_data, $virtuemart_order_id);

		$error_msg = $this->_processIPN($ideal_data, $method, $virtuemart_order_id);
		$this->logInfo('process IPN ' . $error_msg, 'message');
		if (!(empty($error_msg) )) {
			$new_status = $method->status_canceled;
			$this->logInfo('process IPN ' . $error_msg . ' ' . $new_status, 'ERROR');
		} else {
			$this->logInfo('process IPN OK', 'message');
		}

		$new_status = $this->_getPaymentStatus($method, $ideal_data['payment_status']);

		$this->logInfo('plgVmOnPaymentNotification return new_status:' . $new_status, 'message');

		$modelOrder = VmModel::getModel('orders');

		$order = array();
		$order['order_status'] = $new_status;
		$order['customer_notified'] =1;
		$order['comments'] = JText::sprintf('VMPAYMENT_IDEAL_PAYMENT_STATUS_CONFIRMED', $order_number);

		 if (strcmp($ideal_data['payment_status'], 'Completed') == 0) {
			    $order['order_status'] = $method->status_success;
			    $order['comments'] = JText::sprintf('VMPAYMENT_IDEAL_PAYMENT_STATUS_CONFIRMED', $order_number);
			} elseif (strcmp($ideal_data['payment_status'], 'Pending') == 0) {
			    $key = 'VMPAYMENT_PAYPAL_PENDING_REASON_FE_' . strtoupper($ideal_data['pending_reason']);
			    if (!$lang->hasKey($key)) {
				$key = 'VMPAYMENT_PAYPAL_PENDING_REASON_FE_DEFAULT';
			    }
			    $order['comments'] = JText::sprintf('VMPAYMENT_PAYPAL_PAYMENT_STATUS_PENDING', $order_number) . JText::_($key);
			    $order['order_status'] = $method->status_pending;
			} else {
			    $order['order_status'] = $method->status_canceled;
			}

		$modelOrder->updateStatusForOneOrder($virtuemart_order_id, $order, true);		

		//// remove vmcart
		$this->emptyCart($return_context);		
	}

	function _storeiDEALInternalData($method, $ideal_data, $virtuemart_order_id) {

		// get all know columns of the table
		$db = JFactory::getDBO();
		$query = 'SHOW COLUMNS FROM `' . $this->_tablename . '` ';
		$db->setQuery($query);
		$columns = $db->loadResultArray(0);
		$post_msg = '';
		foreach ($ideal_data as $key => $value) {
			$post_msg .= $key . "=" . $value . "<br />";
			$table_key = 'ideal_response_' . $key;
			if (in_array($table_key, $columns)) {
				$response_fields[$table_key] = $value;
			}
		}
		
		$response_fields['payment_name'] = $this->renderPluginName($method);
		$response_fields['order_number'] = $ideal_data['invoice'];
		$response_fields['virtuemart_order_id'] = $virtuemart_order_id;		
		$this->storePSPluginInternalData($response_fields, 'virtuemart_order_id', true);
	}

	function _getTablepkeyValue($virtuemart_order_id) {
		$db = JFactory::getDBO();
		$q = 'SELECT ' . $this->_tablepkey . ' FROM `' . $this->_tablename . '` '
		. 'WHERE `virtuemart_order_id` = ' . $virtuemart_order_id;
		$db->setQuery($q);

		if (!($pkey = $db->loadResult())) {
			JError::raiseWarning(500, $db->getErrorMsg());
			return '';
		}
		return $pkey;
	}

	function _getPaymentStatus($method, $ideal_status) {
		$new_status = '';
		if (strcmp($ideal_status, 'Completed') == 0) {
			$new_status = $method->status_success;
		} elseif (strcmp($ideal_status, 'Pending') == 0) {
			$new_status = $method->status_pending;
		} else {
			$new_status = $method->status_canceled;
		}
		return $new_status;
	}

	/**
	 * Display stored payment data for an order
	 * @see components/com_virtuemart/helpers/vmPSPlugin::plgVmOnShowOrderBEPayment()
	 */
	function plgVmOnShowOrderBEPayment($virtuemart_order_id, $payment_method_id) {

		if (!$this->selectedThisByMethodId($payment_method_id)) {
			return null; // Another method was selected, do nothing
		}


		if (!($paymentTable = $this->_getiDEALInternalData($virtuemart_order_id) )) {			
			return '';
		}
		$this->getPaymentCurrency($paymentTable);
		$q = 'SELECT `currency_code_3` FROM `#__virtuemart_currencies` WHERE `virtuemart_currency_id`="' . $paymentTable->payment_currency . '" ';
		$db = &JFactory::getDBO();
		$db->setQuery($q);
		$currency_code_3 = $db->loadResult();
		$html = '<table class="adminlist">' . "\n";
		$html .=$this->getHtmlHeaderBE();
		$html .= $this->getHtmlRowBE('IDEAL_PAYMENT_NAME', $paymentTable->payment_name);		
		$code = "ideal_response_";
		foreach ($paymentTable as $key => $value) {
			if (substr($key, 0, strlen($code)) == $code) {
				$html .= $this->getHtmlRowBE($key, $value);
			}
		}
		$html .= '</table>' . "\n";
		return $html;
	}

	function _getiDEALInternalData($virtuemart_order_id, $order_number='') {
		$db = JFactory::getDBO();
		$q = 'SELECT * FROM `' . $this->_tablename . '` WHERE ';
		if ($order_number) {
			$q .= " `order_number` = '" . $order_number . "'";
		} else {
			$q .= ' `virtuemart_order_id` = ' . $virtuemart_order_id;
		}

		$db->setQuery($q);
		if (!($paymentTable = $db->loadObject())) {			
			return '';
		}
		return $paymentTable;
	}
	
	function _getiDEALUrl($method) {

		$url = JURI::root().'index.php?option=com_ccidealplatform&task=bankform';

		return $url;
	}

	function _getiDEALUrlHttps($method) {
		$url = $this->_getiDEALUrl($method);

		return $url;
	}

	function _getPaymentResponseHtml($idealTable, $payment_name) {

        $link = JRoute::_('index.php?option=com_virtuemart&view=orders&layout=details&order_number='.$idealTable->order_number, false);

		$html = '<table>' . "\n";
		$html .= $this->getHtmlRow('IDEAL_PAYMENT_NAME', $payment_name);
		if (!empty($idealTable)) {
			$html .= $this->getHtmlRow('IDEAL_ORDER_NUMBER', $idealTable->order_number);			
		}
		$html .= '</table>' . "\n";

        // Don't show "View order" button is user is a guest (has no account)
        // VM will not show the order unless user is logged in...
        $user = JFactory::getUser();
        if($user->id!=0){
            $html .= '<a class="vm-button-correct" href="' . $link . '">' . vmText::_('COM_VIRTUEMART_ORDER_VIEW_ORDER').'</a>';
        }

		return $html;
	}

	function getCosts(VirtueMartCart $cart, $method, $cart_prices) {
		if (preg_match('/%$/', $method->cost_percent_total)) {
			$cost_percent_total = substr($method->cost_percent_total, 0, -1);
		} else {
			$cost_percent_total = $method->cost_percent_total;
		}
		return ($method->cost_per_transaction + ($cart_prices['salesPrice'] * $cost_percent_total * 0.01));
	}

	/**
	 * Check if the payment conditions are fulfilled for this payment method
	 * @author: Valerie Isaksen
	 *
	 * @param $cart_prices: cart prices
	 * @param $payment
	 * @return true: if the conditions are fulfilled, false otherwise
	 *
	 */
	protected function checkConditions($cart, $method, $cart_prices) {

		$this->convert_condition_amount($method);
		$amount = $this->getCartAmount($cart_prices);
		$address = (($cart->ST == 0) ? $cart->BT : $cart->ST);
		
 		$amount_cond = ($amount >= $method->min_amount AND $amount <= $method->max_amount
			OR
			($method->min_amount <= $amount AND ($method->max_amount == 0)));
		if (!$amount_cond) {
			return FALSE;
		}
		$countries = array();
		if (!empty($method->countries)) {
			if (!is_array ($method->countries)) {
				$countries[0] = $method->countries;
			} else {
				$countries = $method->countries;
			}
		}

		// probably did not gave his BT:ST address
		if (!is_array ($address)) {
			$address = array();
			$address['virtuemart_country_id'] = 0;
		}

		if (!isset($address['virtuemart_country_id'])) {
			$address['virtuemart_country_id'] = 0;
		}
		if (count ($countries) == 0 || in_array ($address['virtuemart_country_id'], $countries) ) {
			return TRUE;
		}

		return FALSE;	
	}

	/**
	 * We must reimplement this triggers for joomla 1.7
	 */

	/**
	 * Create the table for this plugin if it does not yet exist.
	 * This functions checks if the called plugin is active one.
	 * When yes it is calling the standard method to create the tables
	 * @author ValÃ©rie Isaksen
	 *
	 */
	function plgVmOnStoreInstallPaymentPluginTable($jplugin_id) {

		return $this->onStoreInstallPluginTable($jplugin_id);
	}

	/**
	 * This event is fired after the payment method has been selected. It can be used to store
	 * additional payment info in the cart.
	 *
	 * @author Max Milbers
	 * @author ValÃ©rie isaksen
	 *
	 * @param VirtueMartCart $cart: the actual cart
	 * @return null if the payment was not selected, true if the data is valid, error message if the data is not vlaid
	 *
	 */
	public function plgVmOnSelectCheckPayment(VirtueMartCart $cart) {
		return $this->OnSelectCheck($cart);
	}

	/**
	 * plgVmDisplayListFEPayment
	 * This event is fired to display the pluginmethods in the cart (edit shipment/payment) for exampel
	 *
	 * @param object $cart Cart object
	 * @param integer $selected ID of the method selected
	 * @return boolean True on succes, false on failures, null when this plugin was not selected.
	 * On errors, JError::raiseWarning (or JError::raiseError) must be used to set a message.
	 *
	 * @author Valerie Isaksen
	 * @author Max Milbers
	 */
	public function plgVmDisplayListFEPayment(VirtueMartCart $cart, $selected = 0, &$htmlIn) {
		return $this->displayListFE($cart, $selected, $htmlIn);
	}

	/*
	 * plgVmonSelectedCalculatePricePayment
	* Calculate the price (value, tax_id) of the selected method
	* It is called by the calculator
	* This function does NOT to be reimplemented. If not reimplemented, then the default values from this function are taken.
	* @author Valerie Isaksen
	* @cart: VirtueMartCart the current cart
	* @cart_prices: array the new cart prices
	* @return null if the method was not selected, false if the shiiping rate is not valid any more, true otherwise
	*
	*
	*/

	public function plgVmonSelectedCalculatePricePayment(VirtueMartCart $cart, array &$cart_prices, &$cart_prices_name) {
		return $this->onSelectedCalculatePrice($cart, $cart_prices, $cart_prices_name);
	}

	/**
	 * plgVmOnCheckAutomaticSelectedPayment
	 * Checks how many plugins are available. If only one, the user will not have the choice. Enter edit_xxx page
	 * The plugin must check first if it is the correct type
	 * @author Valerie Isaksen
	 * @param VirtueMartCart cart: the cart object
	 * @return null if no plugin was found, 0 if more then one plugin was found,  virtuemart_xxx_id if only one plugin is found
	 *
	 */
	function plgVmOnCheckAutomaticSelectedPayment(VirtueMartCart $cart, array $cart_prices = array()) {
		return $this->onCheckAutomaticSelected($cart, $cart_prices);
	}

	/**
	 * This method is fired when showing the order details in the frontend.
	 * It displays the method-specific data.
	 *
	 * @param integer $order_id The order ID
	 * @return mixed Null for methods that aren't active, text (HTML) otherwise
	 * @author Max Milbers
	 * @author Valerie Isaksen
	 */
	public function plgVmOnShowOrderFEPayment($virtuemart_order_id, $virtuemart_paymentmethod_id, &$payment_name) {
		$this->onShowOrderFE($virtuemart_order_id, $virtuemart_paymentmethod_id, $payment_name);
	}

	/**
	 * This event is fired during the checkout process. It can be used to validate the
	 * method data as entered by the user.
	 *
	 * @return boolean True when the data was valid, false otherwise. If the plugin is not activated, it should return null.
	 * @author Max Milbers

	 public function plgVmOnCheckoutCheckDataPayment($psType, VirtueMartCart $cart) {
	 return null;
	 }
	 */

	/**
	 * This method is fired when showing when priting an Order
	 * It displays the the payment method-specific data.
	 *
	 * @param integer $_virtuemart_order_id The order ID
	 * @param integer $method_id  method used for this order
	 * @return mixed Null when for payment methods that were not selected, text (HTML) otherwise
	 * @author Valerie Isaksen
	 */
	function plgVmonShowOrderPrintPayment($order_number, $method_id) {
		return $this->onShowOrderPrint($order_number, $method_id);
	}
	
	function plgVmDeclarePluginParamsPaymentVM3( &$data) {
		return $this->declarePluginParams('payment', $data);
	}

	function plgVmSetOnTablePluginParamsPayment($name, $id, &$table) {
		return $this->setOnTablePluginParams($name, $id, $table);
	}

	/*
     * Keep backwards compatibility
     * a new parameter has been added in the xml file
     */
	function getNewStatus ($method) {

		if (isset($method->status_pending) and $method->status_pending!="") {
			return $method->status_pending;
		} else {
			return 'P';
		}
	}

}

// No closing tag