<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined('_JEXEC') or die;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

$language = JFactory::getLanguage();
$language->load('com_ccidealplatform', JPATH_ADMINISTRATOR, 'en-GB', true);
$language->load('com_ccidealplatform', JPATH_ADMINISTRATOR, null, true);

$jversion 			= new JVersion();
$current_version 	= $jversion->getShortVersion();
$jversion_sht		= substr($current_version,0,1);

	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return substr($current_version,0,1);
	}

	// Access check.
	if (!JFactory::getUser()->authorise('core.manage', 'com_ccidealplatform')) {
		return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
	}

	// Joomla redefiend function for tooltip
	JHtml::_('behavior.tooltip');

	// Include dependancies
	jimport('joomla.application.component.controller');

	// For styles & scripts
	$document = JFactory::getDocument();
	$document->addStyleSheet('components/com_ccidealplatform/assets/css/style.css');
	$document->addScript('components/com_ccidealplatform/assets/js/ccvalidate.js');

	// David - 2 April 2015 - cciDEAL 4.4.4
	// Removing Akeeba Strapper and FOF, adding ccBootstrap
	if(versionCompare()<3)
	{
		require_once JPATH_ROOT . '/media/chillcreations_bootstrap/ccbootstrap.php';
		if(class_exists('ccbootstrap')){
			$chillcreations_bootsrap = new ccbootstrap();
		}
	}

// Execute the task.
	$controller	= JControllerLegacy::getInstance('ccidealplatform');
	$controller->execute(JFactory::getApplication()->input->get('task'));
	$controller->redirect();