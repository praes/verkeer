/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

//Only used for the integrations view

function listItemTask(a,b){
	document.getElementById('extension_id').value=a;
	Joomla.submitbutton(b);
}
function exten_down(task,delement, dfolder){
	document.adminForm.delement.value = delement;
	document.adminForm.dfolder.value = dfolder;
	document.adminForm.task.value = 'integrations.'+task;
	document.adminForm.submit();
	document.adminForm.task.value = '';
}