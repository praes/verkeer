/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

function submitData()
{
	if(document.getElementById('grandtotal').value == '') {
		alert('Total amount is missing');
		return false;
	}
	else if(document.getElementById('ordernumber').value == '') {
		alert('order number is missing');
		return false;
	}
	else if(document.getElementById('extn').value == '') {
		alert('Extension name is missing');
		return false;
	}
	else{
	  return true;
	}
}

function submitBank()
{
	var form = document.idealform;
	if(form.elements[4].value == 0)
	{
		alert('Selecteer uw bank en klik op betalen');
		return false;
	}
	else
	{
		form.submit();
	}
}

function update(id,urls,divid,sv){

	document.adminForm.oid.value = id;
	document.adminForm.sv.value = sv;
	document.adminForm.offsetid.value = divid;

	document.adminForm.task.value = 'payments.statusupdate';
	document.adminForm.submit();
	document.adminForm.task.value = '';
}

function selectbanks(){
	if(document.adminForm.IDEAL_Bank.value == 'Mollie'){
		document.getElementById('mollie').style.display = 'block';
		document.getElementById('banks').style.display = 'none';
		document.getElementById('abnamroeasy').style.display = 'none';
		document.getElementById('certificate').style.display = 'none';
		document.getElementById('cert_title').style.display = 'none';
		document.getElementById('targetpay').style.display = 'none';
		document.getElementById('iDEALProVersion').style.display = 'none';
		document.getElementById('sisow').style.display = 'none';

		/*document.getElementById('mollie_method').style.display = 'block';
		document.getElementById('targetpay_method').style.display = 'none';
		document.getElementById('raboomni_method').style.display = 'none';
		document.getElementById('sisow_method').style.display = 'none';
		document.getElementById('method_label').style.display = 'block';*/
	}
	else if(document.adminForm.IDEAL_Bank.value == 'ABNAMROTEST'){
		document.getElementById('abnamroeasy').style.display = 'block';
		document.getElementById('banks').style.display = 'none';
		document.getElementById('mollie').style.display = 'none';
		document.getElementById('certificate').style.display = 'none';
		document.getElementById('cert_title').style.display = 'none';
		document.getElementById('targetpay').style.display = 'none';
		document.getElementById('iDEALProVersion').style.display = 'none';
		document.getElementById('sisow').style.display = 'none';

		/*document.getElementById('mollie_method').style.display = 'none';
		document.getElementById('targetpay_method').style.display = 'none';
		document.getElementById('raboomni_method').style.display = 'none';
		document.getElementById('sisow_method').style.display = 'none';
		document.getElementById('method_label').style.display = 'none';*/

	}
	else if(document.adminForm.IDEAL_Bank.value == 'RABOBANKTEST'){
		document.getElementById('certificate').style.display = 'none';
		document.getElementById('cert_title').style.display = 'none';
		document.getElementById('abnamroeasy').style.display = 'none';
		document.getElementById('banks').style.display = 'block';
		document.getElementById('mollie').style.display = 'none';
		document.getElementById('targetpay').style.display = 'none';
		document.getElementById('iDEALProVersion').style.display = 'none';
		document.getElementById('PrivatekeyPass_v3').style.display = 'none';
		document.getElementById('PrivatekeyPass').style.display = 'block';
		document.getElementById('sisow').style.display = 'none';

		/*document.getElementById('mollie_method').style.display = 'none';
		document.getElementById('targetpay_method').style.display = 'none';
		document.getElementById('raboomni_method').style.display = 'none';
		document.getElementById('sisow_method').style.display = 'none';
		document.getElementById('method_label').style.display = 'none';*/

	}else if(document.adminForm.IDEAL_Bank.value == 'ING'){
		document.getElementById('certificate').style.display = 'block';
		document.getElementById('cert_title').style.display = 'block';
		document.getElementById('abnamroeasy').style.display = 'none';
		document.getElementById('banks').style.display = 'block';
		document.getElementById('mollie').style.display = 'none';
		document.getElementById('targetpay').style.display = 'none';
		document.getElementById('iDEALProVersion').style.display = 'block';
		document.getElementById('PrivatekeyPass_v3').style.display = 'none';
		document.getElementById('PrivatekeyPass').style.display = 'block';
		document.getElementById('sisow').style.display = 'none';

		/*document.getElementById('mollie_method').style.display = 'none';
		document.getElementById('targetpay_method').style.display = 'none';
		document.getElementById('raboomni_method').style.display = 'none';
		document.getElementById('sisow_method').style.display = 'none';
		document.getElementById('method_label').style.display = 'none';*/

	}else if(document.adminForm.IDEAL_Bank.value == 'RABOPROF'){
		document.getElementById('certificate').style.display = 'block';
		document.getElementById('cert_title').style.display = 'block';
		document.getElementById('abnamroeasy').style.display = 'none';
		document.getElementById('banks').style.display = 'block';
		document.getElementById('mollie').style.display = 'none';
		document.getElementById('targetpay').style.display = 'none';
		document.getElementById('iDEALProVersion').style.display = 'block';
		document.getElementById('PrivatekeyPass_v3').style.display = 'none';
		document.getElementById('PrivatekeyPass').style.display = 'block';
		document.getElementById('sisow').style.display = 'none';

		/*document.getElementById('mollie_method').style.display = 'none';
		document.getElementById('targetpay_method').style.display = 'none';
		document.getElementById('raboomni_method').style.display = 'none';
		document.getElementById('sisow_method').style.display = 'none';
		document.getElementById('method_label').style.display = 'none';*/

	}
	else if(document.adminForm.IDEAL_Bank.value == 'POSTBASIC'){

		document.getElementById('certificate').style.display = 'none';
		document.getElementById('cert_title').style.display = 'none';
		document.getElementById('abnamroeasy').style.display = 'none';
		document.getElementById('banks').style.display = 'block';
		document.getElementById('mollie').style.display = 'none';
		document.getElementById('targetpay').style.display = 'none';
		document.getElementById('iDEALProVersion').style.display = 'none';
		document.getElementById('PrivatekeyPass_v3').style.display = 'none';
		document.getElementById('PrivatekeyPass').style.display = 'block';
		document.getElementById('sisow').style.display = 'none';

		/*document.getElementById('mollie_method').style.display = 'none';
		document.getElementById('targetpay_method').style.display = 'none';
		document.getElementById('raboomni_method').style.display = 'none';
		document.getElementById('sisow_method').style.display = 'none';
		document.getElementById('method_label').style.display = 'none';*/

	}
	else if(document.adminForm.IDEAL_Bank.value == 'TARGETPAY'){

		document.getElementById('certificate').style.display = 'none';
		document.getElementById('cert_title').style.display = 'none';
		document.getElementById('abnamroeasy').style.display = 'none';
		document.getElementById('banks').style.display = 'none';
		document.getElementById('mollie').style.display = 'none';
		document.getElementById('targetpay').style.display = 'block';
		document.getElementById('iDEALProVersion').style.display = 'none';
		document.getElementById('sisow').style.display = 'none';

		/*document.getElementById('mollie_method').style.display = 'none';
		document.getElementById('targetpay_method').style.display = 'block';
		document.getElementById('raboomni_method').style.display = 'none';
		document.getElementById('sisow_method').style.display = 'none';
		document.getElementById('method_label').style.display = 'block';*/

	}
	else if(document.adminForm.IDEAL_Bank.value == 'RABOOMNIKASSA'){

		document.getElementById('certificate').style.display = 'none';
		document.getElementById('cert_title').style.display = 'none';
		document.getElementById('abnamroeasy').style.display = 'none';
		document.getElementById('banks').style.display = 'block';
		document.getElementById('mollie').style.display = 'none';
		document.getElementById('targetpay').style.display = 'none';
		document.getElementById('iDEALProVersion').style.display = 'none';
		document.getElementById('PrivatekeyPass_v3').style.display = 'none';
		document.getElementById('PrivatekeyPass').style.display = 'block';
		document.getElementById('sisow').style.display = 'none';

		/*document.getElementById('mollie_method').style.display = 'none';
		document.getElementById('targetpay_method').style.display = 'none';
		document.getElementById('raboomni_method').style.display = 'block';
		document.getElementById('sisow_method').style.display = 'none';
		document.getElementById('method_label').style.display = 'block';*/

	}else if(document.adminForm.IDEAL_Bank.value == 'SISOW'){

		document.getElementById('certificate').style.display = 'none';
		document.getElementById('cert_title').style.display = 'none';
		document.getElementById('abnamroeasy').style.display = 'none';
		document.getElementById('banks').style.display = 'none';
		document.getElementById('mollie').style.display = 'none';
		document.getElementById('targetpay').style.display = 'none';
		document.getElementById('iDEALProVersion').style.display = 'none';
		document.getElementById('PrivatekeyPass_v3').style.display = 'none';
		document.getElementById('PrivatekeyPass').style.display = 'none';
		document.getElementById('sisow').style.display = 'block';

		/*document.getElementById('mollie_method').style.display = 'none';
		document.getElementById('targetpay_method').style.display = 'none';
		document.getElementById('raboomni_method').style.display = 'block';
		document.getElementById('sisow_method').style.display = 'none';
		document.getElementById('method_label').style.display = 'block';*/

	}
	else{
		document.getElementById('abnamroeasy').style.display = 'none';
		document.getElementById('mollie').style.display = 'none';
		document.getElementById('banks').style.display = 'block';
		document.getElementById('certificate').style.display = 'block';
		document.getElementById('cert_title').style.display = 'block';
		document.getElementById('targetpay').style.display = 'none';
		document.getElementById('sisow').style.display = 'none';

	}
}

function ShowPicture(id,Source) {

	if (Source=="1"){
		if (document.layers) document.layers[''+id+''].visibility = "show"
		else if (document.all) document.all[''+id+''].style.visibility = "visible"
		else if (document.getElementById) document.getElementById(''+id+'').style.visibility = "visible"
	}
	else
		if (Source=="0"){
			if (document.layers) document.layers[''+id+''].visibility = "hide"
			else if (document.all) document.all[''+id+''].style.visibility = "hidden"
			else if (document.getElementById) document.getElementById(''+id+'').style.visibility = "hidden"
	}
}

function showExtrainfo(id,Source,count){

	if (Source=="1"){
	 	if (document.layers) document.layers[''+id+count+''].visibility = "show"
		else if (document.all) document.all[''+id+count+''].style.visibility = "visible"
		else if (document.getElementById) document.getElementById(''+id+count+'').style.visibility = "visible"
		else if (document.getElementById) document.getElementById(''+id+count+'').style.display = "block";

	}else{
		if (Source=="0"){

			if (document.layers) document.layers[''+id+count+''].visibility = "hide"
			else if (document.all) document.all[''+id+count+''].style.visibility = "hidden"
			else if (document.getElementById) document.getElementById(''+id+count+'').style.visibility = "hidden"
			else if (document.getElementById) document.getElementById(''+id+count+'').style.display = "none"
		}
	}
}
function request_status(com,amt,order_id,trxid,path){

	if(trxid == ""){
		alert("Transaction ID empty");
		return false;
	}

	var ajaxRequest;  // The variable that makes Ajax possible!

	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}

	// Create a function that will receive data sent from the server

	ajaxRequest.onreadystatechange = function(){
		if(ajaxRequest.readyState == 4){
			window.location.href = path +'administrator/index.php?option=com_ccidealplatform&view=payments';
		}
	}

	var url= path+'index.php?option=com_ccidealplatform&task=checkccidealresult&com='+com+'&amt='+amt+'&order_id='+order_id+'&trxid='+trxid;

	ajaxRequest.open("GET", url, true);
	ajaxRequest.send(null);

}