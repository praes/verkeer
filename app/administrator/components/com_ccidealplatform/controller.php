<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die;

class ccidealplatformController extends JControllerLegacy
{
    /**
     * @var        string    The default view.
     */
    protected $default_view = 'ccideal';


    public function display($cachable = false, $urlparams = false)
    {

        if (JFactory::getApplication()->input->get('view') == "liveupdate") {
            JFactory::getApplication()->redirect('index.php?option=com_installer&view=update');
        }

            require_once(JPATH_COMPONENT . DS . 'helper' . DS . 'ccidealplatform.php');

            ccidealplatformHelper::addSubmenu(JRequest::getCmd('view', 'ccideal'));

            parent::display();

            return $this;
        }
    }
