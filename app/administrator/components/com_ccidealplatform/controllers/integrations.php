<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

defined('_JEXEC') or die;

jimport('joomla.filesystem.path');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder' );
jimport('joomla.filesystem.archive');

jimport( 'joomla.application.component.controller' );

class cciDealPlatformControllerIntegrations extends JControllerLegacy
{
	public function publish()
	{
		$model = $this->getModel('integrations');
		if($model->publish()){
			$this->setRedirect('index.php?option=com_ccidealplatform&view=integrations','Plugin successfully enabled');
		}
	}
	public function unpublish()
	{
		$model = $this->getModel('integrations');
		if($model->publish()){
			$this->setRedirect('index.php?option=com_ccidealplatform&view=integrations','Plugin successfully disabled');
		}
	}
	public function downloadextensions(){

		$delement = JRequest::getVar('delement');
		$dfolder  = JRequest::getVar('dfolder');

		$data			= $this->direct_read($delement,$dfolder);
		$name			= $data['name'].'.zip';
	    $files 			= $data['files'];
	    $dir 			= $data['folder'];

		$zip_adapter 	= JArchive::getAdapter('zip'); // compression type
	    $filesToZip 	= array();

	    $zip_name 		= JPATH_COMPONENT.DS.'assets'.DS.'files'.DS.$name;

		foreach ($files as $file) {
			$data = JFile::read(JPATH_COMPONENT.DS.'plugins'.DS.$dir.DS.$file);
			$filesToZip[] = array('name'=> $file, 'data' => $data);
		}

		if(!$zip_adapter->create($zip_name, $filesToZip,'zip',array())) {
			global $mainframe;
			$mainframe->enqueueMessage('Error creating zip file.', 'message');
		} else {
			header("Content-Disposition: attachment; filename=$name");
			header("Pragma: public");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: public");
			header("Content-Description: File Transfer");
			header("Content-type: application/octet-stream");
			header("Content-Transfer-Encoding: binary");
			header("Expires: 0");
			ob_end_flush();
			readfile("$zip_name");
	 	}

	 	@JFile::delete($zip_name);
	}

	public function direct_read($element,$folder){

		if(($element == "content_ideal")&&($folder == "content")){

			$name 			= "content_ideal";
			$files 			= array('content_ideal.php','content_ideal.xml');
			$dir 			= 'content_ideal';

		}elseif(($element == "plg_ccinvoices_ideal")&&($folder == "ccinvoices_payment")){

			$name			= 'plg_ccinvoices_ideal';
		    $files 			= array('plg_ccinvoices_ideal.php','plg_ccinvoices_ideal.xml');
		    $dir 			= 'ccinvoices'.DS.'plg_ccinvoices_ideal';

		}elseif(($element == "iDEAL")&&($folder == "akpayment" )){

			$name			= 'plg_akeebasubs_ideal';
		    $files 			= array('iDEAL/form.php','iDEAL/Thumbs.db','en-GB.plg_akpayment_iDEAL.ini' ,'iDEAL.php', 'iDEAL.xml');
		    $dir 			= 'akeeba'.DS.'plg_akeebasubs_ideal';

		}elseif(($element == "iDEAL3")&&($folder == "akpayment" )){

			$name			= 'plg_akeebasubs_ideal';
		    $files 			= array('iDEAL/form.php','iDEAL/Thumbs.db','en-GB.plg_akpayment_iDEAL.ini' ,'iDEAL.php', 'iDEAL.xml');
		    $dir 			= 'akeeba3'.DS.'plg_akeebasubs_ideal';

		}elseif(($element == "iDEAL")&&($folder == "hikashoppayment" )){

			$name			= 'plg_hikashop_ideal';
		    $files 			= array('iDEAL.php', 'iDEAL.xml','iDEAL_configuration.php' ,'iDEAL_end.php');
		    $dir 			= 'hikashop'.DS.'plg_hikashop_ideal';

		}elseif(($element == "rdmccidealplatform")&&($folder == "rdmedia" )){

			$name			= 'rdmccidealplatform';
		    $files 			= array('rdmccidealplatform.xml','rdmccidealplatform.php','rdmedia_ccidealplatform/css/rdmccidealplatform.css' ,'rdmedia_ccidealplatform/images/rdmccidealplatform_button.png','rdmedia_ccidealplatform/images/rdmccidealplatform_logo.jpg','rdmedia_ccidealplatform/images/rdmccidealplatform_vertical_view.jpg');
		    $dir 			= 'ticketmaster'.DS.'rdmccidealplatform';

		}elseif(($element == "rsmembership_ccidealplatform")&&($folder == "system" )){

			$name			= 'plg_rsmembership_ideal';
		    $files 			= array('rsmembership_ccidealplatform.xml', 'rsmembership_ccidealplatform.php','en-GB.plg_system_rsmembership_ccidealplatform.ini');
		    $dir 			= 'rsmembership'.DS.'plg_rsmembership_ideal';

		}elseif(($element == "rsepro_ideal")&&($folder == "system" )){

			$name			= 'plg_rseventspro_ideal';
		    $files 			= array('rsepro_ideal.xml', 'rsepro_ideal.php','en-GB.plg_system_rsepro_ideal.ini','fields/ipaymethod.php');
		    $dir 			= 'rseventspro'.DS.'plg_rseventspro_ideal';

		}elseif(($element == "rsfp_ideal")&&($folder == "system" )){

            $name			= 'plg_rsformpro_ideal';
            $files 			= array('rsfp_ideal.xml', 'rsfp_ideal.php','sql/mysql/install.sql','language/en-GB/en-GB.plg_system_rsfp_ideal.ini','language/en-GB/en-GB.plg_system_rsfp_ideal.sys.ini');
            $dir 			= 'rsformpro'.DS.'plg_rsformpro_ideal';

        }elseif(($element == "rsfp_mistercash")&&($folder == "system" )){

            $name			= 'plg_rsformpro_mistercash';
            $files 			= array('rsfp_mistercash.xml', 'rsfp_mistercash.php','sql/mysql/install.sql');
            $dir 			= 'rsformpro'.DS.'plg_rsformpro_mistercash';

        }elseif(($element == "plg_joomisp_ideal")&&($folder == "payment" )){

			$name			= 'plg_joomisp_ideal';
		    $files 			= array('plg_joomisp_ideal.xml', 'plg_joomisp_ideal.php');
		    $dir 			= 'joomisp'.DS.'plg_joomisp_ideal';

		}elseif(($element == "plg_sobipro_ideal")&&($folder == "system" )){

			$name			= 'plg_sobipro_ideal';
		    $files 			= array('plg_sobipro_ideal.xml', 'plg_sobipro_ideal.php','ideal.ini','adm/ideal.php','extensions/ideal.ini','extensions/ideal.php','ideal/index.html','ideal/init.php');
		    $dir 			= 'sobipro'.DS.'plg_sobipro_ideal';

		}elseif(($element == "plg_virtuemart20_ideal")&&($folder == "vmpayment" )){

			$name			= 'plg_virtuemart_20_ideal';
		    $files 			= array('plg_virtuemart20_ideal.xml', 'plg_virtuemart20_ideal.php','en-GB.plg_vmpayment20_iDEAL.ini','en-GB.plg_vmpayment20_iDEAL.sys.ini','nl-NL.plg_vmpayment20_iDEAL.ini','nl-NL.plg_vmpayment20_iDEAL.sys.ini','ipaymethod.php');
		    $dir 			= 'virtuemart2'.DS.'plg_virtuemart_20_ideal';

		}elseif(($element == "plg_virtuemart3_ideal")&&($folder == "vmpayment" )){

			$name			= 'plg_virtuemart3_ideal';
		    $files 			= array('plg_virtuemart3_ideal.xml', 'plg_virtuemart3_ideal.php','en-GB.plg_vmpayment3_iDEAL.ini','en-GB.plg_vmpayment3_iDEAL.sys.ini','nl-NL.plg_vmpayment3_iDEAL.ini','nl-NL.plg_vmpayment3_iDEAL.sys.ini','ideal/elements/ipaymentmethod.php');
		    $dir 			= 'virtuemart3'.DS.'plg_virtuemart3_ideal';

		}elseif(($element == "rs_payment_ccidealplatform")&&($folder == "redshop_payment" )){

			$name			= 'plg_redshop_ideal';
		    $files 			= array('rs_payment_ccidealplatform.xml', 'rs_payment_ccidealplatform.php','rs_payment_ccidealplatform/index.html','rs_payment_ccidealplatform/rs_payment_ccidealplatform_info.php','language/en-GB.plg_redshop_payment_rs_payment_ideal.ini','language/en-GB.plg_redshop_payment_rs_payment_ideal.sys.ini','language/index.html');
		    $dir 			= 'redshop'.DS.'plg_redshop_ideal';

		}elseif(($element == "scideal")&&($folder == "content" )){

			$name			= 'plg_simplecaddy_ideal';
		    $files 			= array('scideal.xml', 'scideal.php');
		    $dir 			= 'simplecaddy'.DS.'plg_simplecaddy_ideal';

		}elseif(($element == "rsdirectoryideal")&&($folder == "system" )){

			$name			= 'plg_rsdirectoryideal';
		    $files 			= array('rsdirectoryideal.xml', 'rsdirectoryideal.php','language/en-GB/en-GB.plg_system_rsdirectoryideal.ini','language/en-GB/en-GB.plg_system_rsdirectoryideal.sys.ini');
		    $dir 			= 'rsdirectory'.DS.'plg_rsdirectoryideal';
		}elseif(($element == "payment_ccideal")&&($folder == "j2store" )){

			$name			= 'plg_j2store_payment_ccideal';
		    $files 			= array('payment_ccideal.xml', 'payment_ccideal.php','index.html','payment_ccideal/index.html','payment_ccideal/tmpl/index.html','payment_ccideal/tmpl/form.php','payment_ccideal/tmpl/message.php','payment_ccideal/tmpl/postpayment.php','payment_ccideal/tmpl/prepayment.php','payment_ccideal/tmpl/view.php','languages/en-GB.plg_j2store_payment_ccideal.ini','languages/en-GB.plg_j2store_payment_ccideal.sys.ini');
		    $dir 			= 'j2store'.DS.'plg_j2store_payment_ccideal';
		}elseif(($element == "ccideal")&&($folder == "payment" )){

			$name			= 'ccideal';
		    $files 			= array('ccideal.xml','ccideal.php','ccideal/helper.php','ccideal/tmpl/default.php','en-GB/en-GB.plg_payment_ccideal.ini','nl-NL/nl-NL.plg_payment_ccideal.ini');
		    $dir 			= 'techjoomla'.DS.'ccideal';
		}

	    $result         = array('name'=> $name, 'files'=> $files, 'folder'=>$dir);

	    return $result;
	}

	public function plugininstall(){

		$element = JRequest::getVar('delement');
		$folder  = JRequest::getVar('dfolder');

		$jversion 			= new JVersion();
		$current_version 	= $jversion->getShortVersion();
		$jversion_sht		= substr($current_version,0,1);

		$db =  JFactory::getDBO();

	/* Plugin: ccInvoices */
		if(($element == "plg_ccinvoices_ideal")&&($folder == "ccinvoices_payment")){

			$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'ccinvoices'.DS.'plg_ccinvoices_ideal';
			$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'ccinvoices_payment'.DS.'plg_ccinvoices_ideal';

			if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'ccinvoices_payment'.DS.'plg_ccinvoices_ideal')){

				JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'ccinvoices_payment'.DS.'plg_ccinvoices_ideal');
				JFolder::copy($copy_Folder,$paste_Folder);
			}else{
				JFolder::copy($copy_Folder,$paste_Folder);
			}

			$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='plg_ccinvoices_ideal' AND `folder`='ccinvoices_payment' LIMIT 1");
			$plg_id_ccinvoices = $db->loadObject();

			$plg_extid_ccinvoices = ""; $enab_ccinvoices = 0; $params_ccinvoices = "{}";

			@$plg_extid_ccinvoices  = $plg_id_ccinvoices->extension_id;
			@$enab_ccinvoices 		= $plg_id_ccinvoices->enabled;
			@$params_ccinvoices 	= $plg_id_ccinvoices->params;

			if ($plg_extid_ccinvoices){
			   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='plg_ccinvoices_ideal'");
			   $db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='plg_ccinvoices_ideal' ";
			$db->setQuery($select);
			$result = $db->loadResult();

			if($result!="plg_ccinvoices_ideal"){

				$ccinvoices = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('ccInvoices - Payment - cciDEAL Platform', 'plugin', 'plg_ccinvoices_ideal', 'ccinvoices_payment', '0', '".$enab_ccinvoices."', '1', '0', '{\"legacy\":true,\"name\":\"ccInvoices - Payment - cciDEAL Platform\",\"type\":\"plugin\",\"creationDate\":\"REFRESH CACHE\",\"author\":\"Chill Creations\",\"copyright\":\"2008 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"REFRESH CACHE\",\"description\":\"cciDEAL Platform plugin for ccInvoices\",\"group\":\"\"}', '".$params_ccinvoices."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery($ccinvoices);
				$db->query();
			}
		}

	/* Plugin: Akeeba subscription */
		if(($element == "iDEAL")&&($folder == "akpayment" )){

			$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'akeeba'.DS.'plg_akeebasubs_ideal';
			$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'akpayment'.DS.'iDEAL';

			if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'akpayment'.DS.'iDEAL')){

				JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'akpayment'.DS.'iDEAL');
				JFolder::copy($copy_Folder,$paste_Folder);
			}else{
				JFolder::copy($copy_Folder,$paste_Folder);
			}

			$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='iDEAL' AND `folder`='akpayment' LIMIT 1");
			$plg_id_akeeba = $db->loadObject();

			$plg_extid_akeeba = ""; $enab_akeeba = 0; $params_akeeba = "{}";

			@$plg_extid_akeeba  = $plg_id_akeeba->extension_id;
			@$enab_akeeba 		= $plg_id_akeeba->enabled;
			@$params_akeeba		= $plg_id_akeeba->params;

			if ($plg_extid_akeeba){
			   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='iDEAL' AND `folder`='akpayment'");
			   $db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='iDEAL' AND `folder`='akpayment'";
			$db->setQuery($select);
			$result = $db->loadResult();

			if($result!="iDEAL"){

				$akeeba = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('Akeeba Subscriptions Payment - iDEAL', 'plugin', 'iDEAL', 'akpayment', '0', '".$enab_akeeba."', '1', '0', '{\"legacy\":true,\"name\":\"Akeeba Subscriptions Payment - iDEAL\",\"type\":\"plugin\",\"creationDate\":\"June 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.1.0\",\"description\":\"cciDEAL Platform plugin for Akeeba Subscriptions\",\"group\":\"\"}', '".$params_akeeba."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery($akeeba);
				$db->query();
			}

			//language

			$copy_File 	= JPATH_ROOT.DS.'plugins'.DS.'akpayment'.DS.'iDEAL'.DS.'en-GB.plg_akpayment_iDEAL.ini';
			$paste_File = JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_akpayment_iDEAL.ini';

			if(JFile::exists($paste_File)){

				JFile::delete($paste_File);
				JFile::move($copy_File,$paste_File);
			}else{
				JFile::move($copy_File,$paste_File);
			}
		}

	/* Plugin: Akeeba subscription 3.x */
		if(($element == "iDEAL3")&&($folder == "akpayment" )){

			$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'akeeba3'.DS.'plg_akeebasubs_ideal';
			$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'akpayment'.DS.'iDEAL';

			if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'akpayment'.DS.'iDEAL')){

				JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'akpayment'.DS.'iDEAL');
				JFolder::copy($copy_Folder,$paste_Folder);
			}else{
				JFolder::copy($copy_Folder,$paste_Folder);
			}

			$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='iDEAL' AND `folder`='akpayment' LIMIT 1");
			$plg_id_akeeba = $db->loadObject();

			$plg_extid_akeeba = ""; $enab_akeeba = 0; $params_akeeba = "{}";

			@$plg_extid_akeeba  = $plg_id_akeeba->extension_id;
			@$enab_akeeba 		= $plg_id_akeeba->enabled;
			@$params_akeeba		= $plg_id_akeeba->params;

			if ($plg_extid_akeeba){
			   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='iDEAL' AND `folder`='akpayment'");
			   $db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='iDEAL' AND `folder`='akpayment'";
			$db->setQuery($select);
			$result = $db->loadResult();

			if($result!="iDEAL"){

				$akeeba = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('Akeeba Subscriptions Payment - iDEAL', 'plugin', 'iDEAL', 'akpayment', '0', '".$enab_akeeba."', '1', '0', '{\"legacy\":true,\"name\":\"Akeeba Subscriptions Payment - iDEAL\",\"type\":\"plugin\",\"creationDate\":\"June 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.0.0\",\"description\":\"cciDEAL Platform plugin for Akeeba Subscriptions\",\"group\":\"\"}', '".$params_akeeba."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery($akeeba);
				$db->query();
			}

			//language

			$copy_File 	= JPATH_ROOT.DS.'plugins'.DS.'akpayment'.DS.'iDEAL'.DS.'en-GB.plg_akpayment_iDEAL.ini';
			$paste_File = JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_akpayment_iDEAL.ini';

			if(JFile::exists($paste_File)){

				JFile::delete($paste_File);
				JFile::move($copy_File,$paste_File);
			}else{
				JFile::move($copy_File,$paste_File);
			}
		}

	/* Plugin: Hikashop */
		if(($element == "iDEAL")&&($folder == "hikashoppayment" )){

			$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'hikashop'.DS.'plg_hikashop_ideal';
			$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'hikashoppayment'.DS.'iDEAL';

			if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'hikashoppayment'.DS.'iDEAL')){

				JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'hikashoppayment'.DS.'iDEAL');
				JFolder::copy($copy_Folder,$paste_Folder);
			}else{
				JFolder::copy($copy_Folder,$paste_Folder);
			}

			$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='iDEAL' AND `folder`='hikashoppayment' LIMIT 1");
			$plg_id_hikashop = $db->loadObject();

			$plg_extid_hikashop = ""; $enab_hikashop = 0; $params_hikashop = "{}";

			@$plg_extid_hikashop  	= $plg_id_hikashop->extension_id;
			@$enab_hikashop 		= $plg_id_hikashop->enabled;
			@$params_hikashop		= $plg_id_hikashop->params;


			if ($plg_extid_hikashop){
			   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='iDEAL' AND `folder`='hikashoppayment'");
			   $db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='iDEAL' AND `folder`='hikashoppayment'";
			$db->setQuery($select);
			$result = $db->loadResult();

			if($result!="iDEAL"){

				$hikashop = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('HikaShop iDEAL payment plugin', 'plugin', 'iDEAL', 'hikashoppayment', '0', '".$enab_hikashop."', '1', '0', '{\"legacy\":true,\"name\":\"HikaShop iDEAL payment plugin\",\"type\":\"plugin\",\"creationDate\":\"REFRESH CACHE\",\"author\":\"Chill Creations\",\"copyright\":\"2008 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"REFRESH CACHE\",\"description\":\"Hikashop iDEAL plugin for cciDEAL Platform\",\"group\":\"\"}', '".$params_hikashop."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery($hikashop);
				$db->query();
			}
		}

	/* Plugin: TicketMaster */
		if(($element == "rdmccidealplatform")&&($folder == "rdmedia" )){

			$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'ticketmaster'.DS.'rdmccidealplatform';
			$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'rdmedia'.DS.'rdmccidealplatform';

			if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'rdmedia'.DS.'rdmccidealplatform')){

				JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'rdmedia'.DS.'rdmccidealplatform');
				JFolder::copy($copy_Folder,$paste_Folder);
			}else{
				JFolder::copy($copy_Folder,$paste_Folder);
			}

			$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='rdmccidealplatform' AND `folder`='rdmedia' LIMIT 1");
			$plg_id_ticketmaster = $db->loadObject();

			$plg_extid_ticketmaster = ""; $enab_ticketmaster = 0; $params_ticketmaster = "{}";

			@$plg_extid_ticketmaster  	= $plg_id_ticketmaster->extension_id;
			@$enab_ticketmaster 		= $plg_id_ticketmaster->enabled;
			@$params_ticketmaster		= $plg_id_ticketmaster->params;

			if ($plg_extid_ticketmaster){
			   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='rdmccidealplatform' AND `folder`='rdmedia'");
			   $db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='rdmccidealplatform' AND `folder`='rdmedia'";
			$db->setQuery($select);
			$result = $db->loadResult();

			if($result!="rdmccidealplatform"){

				$ticketmaster = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('RD-Media iDEAL by cciDEAL', 'plugin', 'rdmccidealplatform', 'rdmedia', '0', '".$enab_ticketmaster."', '1', '0', '{\"legacy\":true,\"name\":\"RD-Media iDEAL by cciDEAL\",\"type\":\"plugin\",\"creationDate\":\"November 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.3.0\",\"description\":\"iDEAL Payment Processor for RD-Media\",\"group\":\"\"}', '".$params_ticketmaster."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery($ticketmaster);
				$db->query();
			}
		}

	/* Plugin: RSMembership */
		if(($element == "rsmembership_ccidealplatform")&&($folder == "system" )){

			$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'rsmembership'.DS.'plg_rsmembership_ideal';
			$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsmembership_ccidealplatform';

			if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsmembership_ccidealplatform')){

				JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsmembership_ccidealplatform');
				JFolder::copy($copy_Folder,$paste_Folder);
			}else{
				JFolder::copy($copy_Folder,$paste_Folder);
			}

			$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='rsmembership_ccidealplatform' AND `folder`='system' LIMIT 1");
			$plg_id_rsmembership = $db->loadObject();

			$plg_extid_rsmembership = ""; $enab_rsmembership = 0; $params_rsmembership = "{}";

			@$plg_extid_rsmembership  	= $plg_id_rsmembership->extension_id;
			@$enab_rsmembership 		= $plg_id_rsmembership->enabled;
			@$params_rsmembership		= $plg_id_rsmembership->params;

			if ($plg_extid_rsmembership){
			   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='rsmembership_ccidealplatform'");
			   $db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='rsmembership_ccidealplatform' ";
			$db->setQuery($select);
			$result = $db->loadResult();

			if($result!="rsmembership_ccidealplatform"){

				$rsmembership = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('System - RSMembership - cciDEAL Platform', 'plugin', 'rsmembership_ccidealplatform', 'system', '0', '".$enab_rsmembership."', '1', '0', '{\"legacy\":true,\"name\":\"System - RSMembership - cciDEAL Platform\",\"type\":\"plugin\",\"creationDate\":\"October 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.2.0\",\"description\":\"RSMembership iDEAL plugin for cciDEAL Platform\",\"group\":\"\"}', '".$params_rsmembership."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery($rsmembership);
				$db->query();
			}

			//language

			$copy_File 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsmembership_ccidealplatform'.DS.'en-GB.plg_system_rsmembership_ccidealplatform.ini';
			$paste_File = JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_system_rsmembership_ccidealplatform.ini';

			if(JFile::exists($paste_File)){

				JFile::delete($paste_File);
				JFile::move($copy_File,$paste_File);
			}else{
				JFile::move($copy_File,$paste_File);
			}
		}

	/* Plugin: RSEvents Pro */

		if(($element == "rsepro_ideal")&&($folder == "system" )){

			$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'rseventspro'.DS.'plg_rseventspro_ideal';
			$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsepro_ideal';

			if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsepro_ideal')){

				JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsepro_ideal');
				JFolder::copy($copy_Folder,$paste_Folder);
			}else{
				JFolder::copy($copy_Folder,$paste_Folder);
			}

			$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='rsepro_ideal' AND `folder`='system' LIMIT 1");
			$plg_id_rseventspro = $db->loadObject();

			$plg_extid_rseventspro = ""; $enab_rseventspro = 0; $params_rseventspro = "{}";

			@$plg_extid_rseventspro = $plg_id_rseventspro->extension_id;
			@$enab_rseventspro 		= $plg_id_rseventspro->enabled;
			@$params_rseventspro	= $plg_id_rseventspro->params;

			if ($plg_extid_rseventspro){
			   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='rsepro_ideal'");
			   $db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='rsepro_ideal' ";
			$db->setQuery($select);
			$result = $db->loadResult();

			if($result!="rsepro_ideal"){

				$rseventspro = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('RSEvents!Pro iDEAL payment', 'plugin', 'rsepro_ideal', 'system', '0', '".$enab_rseventspro."', '1', '0', '{\"legacy\":true,\"name\":\"RSEvents!Pro iDEAL payment\",\"type\":\"plugin\",\"creationDate\":\"October 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.2.0\",\"description\":\"RSEvents!Pro iDEAL plugin. Make sure you publish the plugin\",\"group\":\"\"}', '".$params_rseventspro."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery($rseventspro);
				$db->query();
			}

			//language

			$copy_File 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsepro_ideal'.DS.'en-GB.plg_system_rsepro_ideal.ini';
			$paste_File = JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_system_rsepro_ideal.ini';

			if(JFile::exists($paste_File)){

				JFile::delete($paste_File);
				JFile::move($copy_File,$paste_File);
			}else{
				JFile::move($copy_File,$paste_File);
			}
		}

        /* Plugin: RSForm Pro */
        if(($element == "rsfp_ideal")&&($folder == "system" )){

            $copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'rsformpro'.DS.'plg_rsformpro_ideal';
            $paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsfp_ideal';

            if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsfp_ideal')){

                JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsfp_ideal');
                JFolder::copy($copy_Folder,$paste_Folder);
            }else{
                JFolder::copy($copy_Folder,$paste_Folder);
            }

            $db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='rsfp_ideal' AND `folder`='system' LIMIT 1");
            $plg_id_rsformpro = $db->loadObject();

            $plg_extid_rsformpro = ""; $enab_rsformpro = 0; $params_rsformpro = "{}";

            @$plg_extid_rsformpro  	= $plg_id_rsformpro->extension_id;
            @$enab_rsformpro 		= $plg_id_rsformpro->enabled;
            @$params_rsformpro	 	= $plg_id_rsformpro->params;

            if ($plg_extid_rsformpro){
                $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='rsfp_ideal'");
                $db->query();
            }

            $select = "SELECT element FROM #__extensions WHERE element='rsfp_ideal' ";
            $db->setQuery($select);
            $result = $db->loadResult();

            if($result!="rsfp_ideal"){

                $rsformpro = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('System - RSFormPro - cciDEAL Platform', 'plugin', 'rsfp_ideal', 'system', '0', '".$enab_rsformpro."', '1', '0', '{\"legacy\":true,\"name\":\"System - RSFormPro - cciDEAL Platform\",\"type\":\"plugin\",\"creationDate\":\"November 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.3.0\",\"description\":\"RSFormPro iDEAL plugin for cciDEAL Platform\",\"group\":\"\"}', '".$params_rsformpro."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
                $db->setQuery($rsformpro);
                $db->query();

                $rsformpro_config = "INSERT IGNORE INTO `#__rsform_config` (`SettingName`, `SettingValue`) VALUES ('ideal.test', '0'),('ideal.tax.type', '1'),('ideal.tax.value', '')";
                $db->setQuery($rsformpro_config);
                $db->query();

                $rsformpro_ctypes = "INSERT IGNORE INTO `#__rsform_component_types` (`ComponentTypeId`, `ComponentTypeName`) VALUES (700, 'ideal')";
                $db->setQuery($rsformpro_ctypes);
                $db->query();

                $rsformpro_ctfields = "DELETE FROM #__rsform_component_type_fields WHERE ComponentTypeId = 700";
                $db->setQuery($rsformpro_ctfields);
                $db->query();

                $rsformpro_ctfields = "INSERT IGNORE INTO `#__rsform_component_type_fields` (`ComponentTypeId`, `FieldName`, `FieldType`, `FieldValues`, `Ordering`) VALUES (700, 'NAME', 'textbox', '', 0),(700, 'LABEL', 'textbox', '', 1),(700, 'COMPONENTTYPE', 'hidden', '700', 2),(700, 'LAYOUTHIDDEN', 'hiddenparam', 'YES', 7)";
                $db->setQuery($rsformpro_ctfields);
                $db->query();
            }

            //language

            $copy_File 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsfp_ideal'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_system_rsfp_ideal.ini';
            $paste_File = JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_system_rsfp_ideal.ini';

            if(JFile::exists($paste_File)){

                JFile::delete($paste_File);
                JFile::move($copy_File,$paste_File);
            }else{
                JFile::move($copy_File,$paste_File);
            }

            $copy_File1 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsfp_ideal'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_system_rsfp_ideal.sys.ini';
            $paste_File1	= JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_system_rsfp_ideal.sys.ini';

            if(JFile::exists($paste_File1)){

                JFile::delete($paste_File1);
                JFile::move($copy_File1,$paste_File1);
            }else{
                JFile::move($copy_File1,$paste_File1);
            }

            $del_File 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsfp_ideal'.DS.'language';

            if(JFolder::exists($del_File)){
                JFolder::delete($del_File);
            }
        }


        // David - 26 May 2015 - cciDEAL 4.4.5
        // Added support for installtion of
        // RSForm Pro - MisterCash plugin
    if(($element == "rsfp_mistercash")&&($folder == "system" )){

        $copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'rsformpro_mistercash'.DS.'plg_rsformpro_mistercash';
        $paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsfp_mistercash';

        if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsfp_mistercash')){

            JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsfp_mistercash');
            JFolder::copy($copy_Folder,$paste_Folder);
        }else{
            JFolder::copy($copy_Folder,$paste_Folder);
        }

        $db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='rsfp_mistercash' AND `folder`='system' LIMIT 1");
        $plg_id_rsformpro = $db->loadObject();

        $plg_extid_rsformpro = ""; $enab_rsformpro = 0; $params_rsformpro = "{}";

        @$plg_extid_rsformpro  	= $plg_id_rsformpro->extension_id;
        @$enab_rsformpro 		= $plg_id_rsformpro->enabled;
        @$params_rsformpro	 	= $plg_id_rsformpro->params;

        if ($plg_extid_rsformpro){
            $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='rsfp_mistercash'");
            $db->query();
        }

        $select = "SELECT element FROM #__extensions WHERE element='rsfp_mistercash' ";
        $db->setQuery($select);
        $result = $db->loadResult();

        if($result!="rsfp_mistercash"){

            $rsformpro = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('System - RSFormPro - ccmistercash Platform', 'plugin', 'rsfp_mistercash', 'system', '0', '".$enab_rsformpro."', '1', '0', '{\"legacy\":true,\"name\":\"System - RSFormPro - ccmistercash Platform\",\"type\":\"plugin\",\"creationDate\":\"November 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.3.0\",\"description\":\"RSFormPro mistercash plugin for ccmistercash Platform\",\"group\":\"\"}', '".$params_rsformpro."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
            $db->setQuery($rsformpro);
            $db->query();

            $rsformpro_config = "INSERT IGNORE INTO `#__rsform_config` (`SettingName`, `SettingValue`) VALUES ('mistercash.test', '0'),('mistercash.tax.type', '1'),('mistercash.tax.value', '')";
            $db->setQuery($rsformpro_config);
            $db->query();

            $rsformpro_ctypes = "INSERT IGNORE INTO `#__rsform_component_types` (`ComponentTypeId`, `ComponentTypeName`) VALUES (710, 'mistercash')";
            $db->setQuery($rsformpro_ctypes);
            $db->query();

            $rsformpro_ctfields = "DELETE FROM #__rsform_component_type_fields WHERE ComponentTypeId = 710";
            $db->setQuery($rsformpro_ctfields);
            $db->query();

            $rsformpro_ctfields = "INSERT IGNORE INTO `#__rsform_component_type_fields` (`ComponentTypeId`, `FieldName`, `FieldType`, `FieldValues`, `Ordering`) VALUES (710, 'NAME', 'textbox', '', 0),(710, 'LABEL', 'textbox', '', 1),(710, 'COMPONENTTYPE', 'hidden', '710', 2),(710, 'LAYOUTHIDDEN', 'hiddenparam', 'YES', 7)";
            $db->setQuery($rsformpro_ctfields);
            $db->query();
        }
    }
        // END support for RSForm PRO - MisterCash plugin


	/* Plugin: JoomISP */
		if(($element == "plg_joomisp_ideal")&&($folder == "payment" )){

			$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'joomisp'.DS.'plg_joomisp_ideal';
			$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'payment'.DS.'plg_joomisp_ideal';

			if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'payment'.DS.'plg_joomisp_ideal')){

				JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'payment'.DS.'plg_joomisp_ideal');
				JFolder::copy($copy_Folder,$paste_Folder);
			}else{
				JFolder::copy($copy_Folder,$paste_Folder);
			}

			$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='plg_joomisp_ideal' AND `folder`='payment' LIMIT 1");
			$plg_id_joomisp = $db->loadObject();

			$plg_extid_joomisp = ""; $enab_joomisp = 0; $params_joomisp = "{}";

			@$plg_extid_joomisp = $plg_id_joomisp->extension_id;
			@$enab_joomisp 		= $plg_id_joomisp->enabled;
			@$params_joomisp	= $plg_id_joomisp->params;

			if ($plg_extid_joomisp){
			   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='plg_joomisp_ideal'");
			   $db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='plg_joomisp_ideal' ";
			$db->setQuery($select);
			$result = $db->loadResult();

			if($result!="plg_joomisp_ideal"){

				$joomisp = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('JoomISP - iDEAL', 'plugin', 'plg_joomisp_ideal', 'payment', '0', '".$enab_joomisp."', '1', '0', '{\"legacy\":true,\"name\":\"JoomISP - iDEAL\",\"type\":\"plugin\",\"creationDate\":\"November 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.3.0\",\"description\":\"JoomISP iDEAL plugin for cciDEAL Platform\",\"group\":\"\"}', '".$params_joomisp."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery($joomisp);
				$db->query();
			}
		}

	/* Plugin: Sobi Pro */
		if(($element == "plg_sobipro_ideal")&&($folder == "system" )){

			$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'sobipro'.DS.'plg_sobipro_ideal';
			$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'plg_sobipro_ideal';

			if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'plg_sobipro_ideal')){

				JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'plg_sobipro_ideal');
				JFolder::copy($copy_Folder,$paste_Folder);
			}else{
				JFolder::copy($copy_Folder,$paste_Folder);
			}

			$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='plg_sobipro_ideal' AND `folder`='system' LIMIT 1");
			$plg_id_sobipro = $db->loadObject();

			$plg_extid_sobipro = ""; $enab_sobipro = 0; $params_sobipro = "{}";

			@$plg_extid_sobipro = $plg_id_sobipro->extension_id;
			@$enab_sobipro 		= $plg_id_sobipro->enabled;
			@$params_sobipro	= $plg_id_sobipro->params;

			if ($plg_extid_sobipro){
			   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='plg_sobipro_ideal'");
			   $db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='plg_sobipro_ideal' ";
			$db->setQuery($select);
			$result = $db->loadResult();

			if($result!="plg_sobipro_ideal"){

				$sobipro = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('System - SobiPro - cciDEAL Platform', 'plugin', 'plg_sobipro_ideal', 'system', '0', '".$enab_sobipro."', '1', '0', '{\"legacy\":true,\"name\":\"System - SobiPro - cciDEAL Platform\",\"type\":\"plugin\",\"creationDate\":\"January 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.0.0\",\"description\":\"iDEAL Payment for SobiPro\",\"group\":\"\"}', '".$params_sobipro."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery($sobipro);
				$db->query();
			}
		}

	/* Plugin: Virtuemart2 */
		if(($element == "plg_virtuemart20_ideal")&&($folder == "vmpayment" )){

			if($jversion_sht == 2){

				$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'virtuemart2'.DS.'plg_virtuemart_20_ideal';
				$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'vmpayment'.DS.'plg_virtuemart20_ideal';

				if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'vmpayment'.DS.'plg_virtuemart20_ideal')){

					JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'vmpayment'.DS.'plg_virtuemart20_ideal');
					JFolder::copy($copy_Folder,$paste_Folder);
				}else{
					JFolder::copy($copy_Folder,$paste_Folder);
				}

				$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='plg_virtuemart20_ideal' AND `folder`='vmpayment' LIMIT 1");
				$plg_id_virtuemart = $db->loadObject();

				$plg_extid_virtuemart = ""; $enab_virtuemart = 0; $params_virtuemart = "{}";

				@$plg_extid_virtuemart  = $plg_id_virtuemart->extension_id;
				@$enab_virtuemart 		= $plg_id_virtuemart->enabled;
				@$params_virtuemart 	= $plg_id_virtuemart->params;

				if ($plg_extid_virtuemart){
				   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='plg_virtuemart20_ideal'");
				   $db->query();
				}

				$select = "SELECT element FROM #__extensions WHERE element='plg_virtuemart20_ideal' ";
				$db->setQuery($select);
				$result = $db->loadResult();

				if($result!="plg_virtuemart20_ideal"){

					$virtuemart2 = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('VM Payment - cciDEAL integration', 'plugin', 'plg_virtuemart20_ideal', 'vmpayment', '0', '".$enab_virtuemart."', '1', '0', '{\"legacy\":true,\"name\":\"VM Payment - cciDEAL integration\",\"type\":\"plugin\",\"creationDate\":\"November 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.3.0\",\"description\":\"Virtuemart 2.0 iDEAL plugin for cciDEAL Platform\",\"group\":\"\"}', '".$params_virtuemart."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
					$db->setQuery($virtuemart2);
					$db->query();
				}

				//After install
				$alr_paymentm = "SELECT extension_id FROM #__extensions WHERE element='plg_virtuemart20_ideal' ";
				$db->setQuery($alr_paymentm);
				$apaymentm = $db->loadResult();

				$query = "UPDATE #__virtuemart_paymentmethods SET payment_jplugin_id = '".$apaymentm."' WHERE payment_element='plg_virtuemart20_ideal'";
				$db->setQuery($query);
				$db->Query();

				//Multiple payment parameter

				$copy_mpayment  = JPATH_ROOT.DS.'plugins'.DS.'vmpayment'.DS.'plg_virtuemart20_ideal'.DS.'ipaymethod.php';
				$paste_mpayment = JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_virtuemart'.DS.'elements'.DS.'ipaymethod.php';

				if(JFile::exists($copy_mpayment)){
					if(JFile::exists($paste_mpayment)){
						JFile::delete($paste_mpayment);
					}
					JFile::move($copy_mpayment,$paste_mpayment);
				}


				$copy_File1 	= JPATH_ROOT.DS.'plugins'.DS.'vmpayment'.DS.'plg_virtuemart20_ideal'.DS.'en-GB.plg_vmpayment20_iDEAL.ini';
				$paste_File1	= JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_vmpayment20_iDEAL.ini';

				$copy_File2 	= JPATH_ROOT.DS.'plugins'.DS.'vmpayment'.DS.'plg_virtuemart20_ideal'.DS.'en-GB.plg_vmpayment20_iDEAL.sys.ini';
				$paste_File2	= JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_vmpayment20_iDEAL.sys.ini';

				$copy_File3 	= JPATH_ROOT.DS.'plugins'.DS.'vmpayment'.DS.'plg_virtuemart20_ideal'.DS.'nl-NL.plg_vmpayment20_iDEAL.ini';
				$paste_File3	= JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'nl-NL'.DS.'nl-NL.plg_vmpayment20_iDEAL.ini';

				$copy_File4 	= JPATH_ROOT.DS.'plugins'.DS.'vmpayment'.DS.'plg_virtuemart20_ideal'.DS.'nl-NL.plg_vmpayment20_iDEAL.sys.ini';
				$paste_File4	= JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'nl-NL'.DS.'nl-NL.plg_vmpayment20_iDEAL.sys.ini';


				$paste_folder1	= JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'nl-NL';

				if(!JFolder::exists($paste_folder1)){
					JFolder::create($paste_folder1);
				}

				if(JFile::exists($copy_File1)){
					if(JFile::exists($paste_File1)){
						JFile::delete($paste_File1);
						JFile::delete($paste_File2);
						JFile::delete($paste_File3);
						JFile::delete($paste_File4);
					}
				}
				JFile::move($copy_File1,$paste_File1);
				JFile::move($copy_File2,$paste_File2);
				JFile::move($copy_File3,$paste_File3);
				JFile::move($copy_File4,$paste_File4);
			}
		}

		/* Plugin: Virtuemart3 */
		if(($element == "plg_virtuemart3_ideal")&&($folder == "vmpayment" )){			

			$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'virtuemart3'.DS.'plg_virtuemart3_ideal';
			$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'vmpayment'.DS.'plg_virtuemart3_ideal';

			if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'vmpayment'.DS.'plg_virtuemart3_ideal')){

				JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'vmpayment'.DS.'plg_virtuemart3_ideal');
				JFolder::copy($copy_Folder,$paste_Folder);
			}else{
				JFolder::copy($copy_Folder,$paste_Folder);
			}

			$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='plg_virtuemart3_ideal' AND `folder`='vmpayment' LIMIT 1");
			$plg_id_virtuemart = $db->loadObject();

			$plg_extid_virtuemart = ""; $enab_virtuemart = 0; $params_virtuemart = "{}";

			@$plg_extid_virtuemart  = $plg_id_virtuemart->extension_id;
			@$enab_virtuemart 		= $plg_id_virtuemart->enabled;
			@$params_virtuemart 	= $plg_id_virtuemart->params;

			if ($plg_extid_virtuemart){
			   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='plg_virtuemart3_ideal'");
			   $db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='plg_virtuemart3_ideal' ";
			$db->setQuery($select);
			$result = $db->loadResult();

			if($result!="plg_virtuemart3_ideal"){

				$virtuemart3 = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('VM Payment - cciDEAL integration', 'plugin', 'plg_virtuemart3_ideal', 'vmpayment', '0', '".$enab_virtuemart."', '1', '0', '{\"legacy\":true,\"name\":\"VM Payment - cciDEAL integration\",\"type\":\"plugin\",\"creationDate\":\"REFRESH CACHE\",\"author\":\"Chill Creations\",\"copyright\":\"2008 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"REFRESH CACHE\",\"description\":\"Virtuemart 3.x iDEAL plugin for cciDEAL Platform\",\"group\":\"\"}', '".$params_virtuemart."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery($virtuemart3);
				$db->query();
			}

			//After install
			$alr_paymentm = "SELECT extension_id FROM #__extensions WHERE element='plg_virtuemart3_ideal' ";
			$db->setQuery($alr_paymentm);
			$apaymentm = $db->loadResult();

			$query = "UPDATE #__virtuemart_paymentmethods SET payment_jplugin_id = '".$apaymentm."' WHERE payment_element='plg_virtuemart3_ideal'";
			$db->setQuery($query);
			$db->Query();

			$copy_File1 	= JPATH_ROOT.DS.'plugins'.DS.'vmpayment'.DS.'plg_virtuemart3_ideal'.DS.'en-GB.plg_vmpayment3_iDEAL.ini';
			$paste_File1	= JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_vmpayment3_iDEAL.ini';

			$copy_File2 	= JPATH_ROOT.DS.'plugins'.DS.'vmpayment'.DS.'plg_virtuemart3_ideal'.DS.'en-GB.plg_vmpayment3_iDEAL.sys.ini';
			$paste_File2	= JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_vmpayment3_iDEAL.sys.ini';

			$copy_File3 	= JPATH_ROOT.DS.'plugins'.DS.'vmpayment'.DS.'plg_virtuemart3_ideal'.DS.'nl-NL.plg_vmpayment3_iDEAL.ini';
			$paste_File3	= JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'nl-NL'.DS.'nl-NL.plg_vmpayment3_iDEAL.ini';

			$copy_File4 	= JPATH_ROOT.DS.'plugins'.DS.'vmpayment'.DS.'plg_virtuemart3_ideal'.DS.'nl-NL.plg_vmpayment3_iDEAL.sys.ini';
			$paste_File4	= JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'nl-NL'.DS.'nl-NL.plg_vmpayment3_iDEAL.sys.ini';


			$paste_folder1	= JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'nl-NL';

			if(!JFolder::exists($paste_folder1)){
				JFolder::create($paste_folder1);
			}

			if(JFile::exists($copy_File1)){
				if(JFile::exists($paste_File1)){
					JFile::delete($paste_File1);
					JFile::delete($paste_File2);
					JFile::delete($paste_File3);
					JFile::delete($paste_File4);
				}
			}
			JFile::move($copy_File1,$paste_File1);
			JFile::move($copy_File2,$paste_File2);
			JFile::move($copy_File3,$paste_File3);
			JFile::move($copy_File4,$paste_File4);
			
		}

	/* Plugin: redShop */
		if(($element == "rs_payment_ccidealplatform")&&($folder == "redshop_payment" )){

			if($jversion_sht == 2){

				$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'redshop'.DS.'plg_redshop_ideal';
				$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'redshop_payment'.DS.'rs_payment_ccidealplatform';

				if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'redshop_payment'.DS.'rs_payment_ccidealplatform')){

					JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'redshop_payment'.DS.'rs_payment_ccidealplatform');
					JFolder::copy($copy_Folder,$paste_Folder);
				}else{
					JFolder::copy($copy_Folder,$paste_Folder);
				}

				$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='rs_payment_ccidealplatform' AND `folder`='redshop_payment' LIMIT 1");
				$plg_id_redshop = $db->loadObject();

				$plg_extid_redshop = ""; $enab_redshop = 0; $params_redshop = "{}";

				@$plg_extid_redshop  = $plg_id_redshop->extension_id;
				@$enab_redshop 		 = $plg_id_redshop->enabled;
				@$params_redshop 	 = $plg_id_redshop->params;

				if ($plg_extid_redshop){
				   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='rs_payment_ccidealplatform'");
				   $db->query();
				}

				$select = "SELECT element FROM #__extensions WHERE element='rs_payment_ccidealplatform' ";
				$db->setQuery($select);
				$result = $db->loadResult();

				if($result!="rs_payment_ccidealplatform"){

					$redshop = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('redSHOP - cciDEAL Platform', 'plugin', 'rs_payment_ccidealplatform', 'redshop_payment', '0', '".$enab_redshop."', '1', '0', '{\"legacy\":true,\"name\":\"redSHOP - cciDEAL Platform\",\"type\":\"plugin\",\"creationDate\":\"January 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.0.0\",\"description\":\"redSHOP iDEAL plugin for cciDEAL Platform\",\"group\":\"\"}', '".$params_redshop."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
					$db->setQuery($redshop);
					$db->query();
				}

				//language

				$copy_File 	= JPATH_ROOT.DS.'plugins'.DS.'redshop_payment'.DS.'rs_payment_ccidealplatform'.DS.'language'.DS.'en-GB.plg_redshop_payment_rs_payment_ideal.ini';
				$paste_File = JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_redshop_payment_rs_payment_ideal.ini';

				if(JFile::exists($paste_File)){

					JFile::delete($paste_File);
					JFile::move($copy_File,$paste_File);
				}else{
					JFile::move($copy_File,$paste_File);
				}

				$copy_File1 	= JPATH_ROOT.DS.'plugins'.DS.'redshop_payment'.DS.'rs_payment_ccidealplatform'.DS.'language'.DS.'en-GB.plg_redshop_payment_rs_payment_ideal.sys.ini';
				$paste_File1	= JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_system_rsfp_ideal.sys.ini';

				if(JFile::exists($paste_File1)){

					JFile::delete($paste_File1);
					JFile::move($copy_File1,$paste_File1);
				}else{
					JFile::move($copy_File1,$paste_File1);
				}

				$del_File 	= JPATH_ROOT.DS.'plugins'.DS.'redshop_payment'.DS.'rs_payment_ccidealplatform'.DS.'language';

				if(JFolder::exists($del_File)){
					JFolder::delete($del_File);
				}
			}
		}

	/* Plugin: Simple Caddy */
		if(($element == "scideal")&&($folder == "content" )){

			if($jversion_sht == 2){

				$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'simplecaddy'.DS.'plg_simplecaddy_ideal';
				$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'content'.DS.'scideal';

				if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'content'.DS.'scideal')){

					JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'content'.DS.'scideal');
					JFolder::copy($copy_Folder,$paste_Folder);
				}else{
					JFolder::copy($copy_Folder,$paste_Folder);
				}

				$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='scideal' AND `folder`='content' LIMIT 1");
				$plg_id_simplecaddy = $db->loadObject();

				$plg_extid_simplecaddy = ""; $enab_simplecaddy = 0; $params_simplecaddy = "{}";

				@$plg_extid_simplecaddy  = $plg_id_simplecaddy->extension_id;
				@$enab_simplecaddy 		 = $plg_id_simplecaddy->enabled;
				@$params_simplecaddy 	 = $plg_id_simplecaddy->params;

				if ($plg_extid_simplecaddy){
				   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='scideal'");
				   $db->query();
				}

				$select = "SELECT element FROM #__extensions WHERE element='scideal' ";
				$db->setQuery($select);
				$result = $db->loadResult();

				if($result!="scideal"){

					$simplecaddy = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('Content - SimpleCaddy - iDEAL', 'plugin', 'scideal', 'content', '0', '".$enab_simplecaddy."', '1', '0', '{\"legacy\":true,\"name\":\"Content - SimpleCaddy - iDEAL\",\"type\":\"plugin\",\"creationDate\":\"January 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.0.0\",\"description\":\"iDEAL processor for SimpleCaddy 2.0\",\"group\":\"\"}', '".$params_simplecaddy."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
					$db->setQuery($simplecaddy);
					$db->query();
				}
			}
		}

		/* Plugin: RSDirectory */
		if(($element == "rsdirectoryideal")&&($folder == "system" )){

			$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'rsdirectory'.DS.'plg_rsdirectoryideal';
			$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsdirectoryideal';

			if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsdirectoryideal')){

				JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsdirectoryideal');
				JFolder::copy($copy_Folder,$paste_Folder);
			}else{
				JFolder::copy($copy_Folder,$paste_Folder);
			}

			$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='rsdirectoryideal' AND `folder`='system' LIMIT 1");
			$plg_id_rsdirectory = $db->loadObject();

			$plg_extid_rsdirectory = ""; $enab_rsdirectory = 0; $params_rsdirectory = "{}";

			@$plg_extid_rsdirectory  	= $plg_id_rsdirectory->extension_id;
			@$enab_rsdirectory 			= $plg_id_rsdirectory->enabled;
			@$params_rsdirectory	 	= $plg_id_rsdirectory->params;

			if ($plg_extid_rsdirectory){
			   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='rsdirectoryideal'");
			   $db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='rsdirectoryideal' ";
			$db->setQuery($select);
			$result = $db->loadResult();

			if($result!="rsdirectoryideal"){

				$rsdirectory = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('RSDirectory iDEAL by Chill Creations', 'plugin', 'rsdirectoryideal', 'system', '0', '".$enab_rsdirectory."', '1', '0', '{\"legacy\":true,\"name\":\"RSDirectory iDEAL by Chill Creations\",\"type\":\"plugin\",\"creationDate\":\"May 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.1.0\",\"description\":\"RSDirectory! iDEAL Payment Plugin. Make sure you publish the plugin\",\"group\":\"\"}', '".$params_rsdirectory."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery($rsdirectory);
				$db->query();
			}

			//language

			$copy_File 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsdirectoryideal'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_system_rsdirectoryideal.ini';
			$paste_File = JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_system_rsdirectoryideal.ini';

			if(JFile::exists($paste_File)){

				JFile::delete($paste_File);
				JFile::move($copy_File,$paste_File);
			}else{
				JFile::move($copy_File,$paste_File);
			}

			$copy_File1 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsdirectoryideal'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_system_rsdirectoryideal.sys.ini';
			$paste_File1	= JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_system_rsdirectoryideal.sys.ini';

			if(JFile::exists($paste_File1)){

				JFile::delete($paste_File1);
				JFile::move($copy_File1,$paste_File1);
			}else{
				JFile::move($copy_File1,$paste_File1);
			}

			$del_File 	= JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'rsdirectoryideal'.DS.'language';

			if(JFolder::exists($del_File)){
				JFolder::delete($del_File);
			}
		}
		
		/* Plugin: J2Store */
		if(($element == "payment_ccideal")&&($folder == "j2store" )){

			$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'j2store'.DS.'plg_j2store_payment_ccideal';
			$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'j2store'.DS.'payment_ccideal';

			if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'j2store'.DS.'payment_ccideal')){

				JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'j2store'.DS.'payment_ccideal');
				JFolder::copy($copy_Folder,$paste_Folder);
			}else{
				JFolder::copy($copy_Folder,$paste_Folder);
			}

			$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='payment_ccideal' AND `folder`='j2store' LIMIT 1");
			$plg_id_j2store = $db->loadObject();

			$plg_extid_j2store = ""; $enab_j2store = 0; $params_j2store = "{}";

			@$plg_extid_j2store  	= $plg_id_j2store->extension_id;
			@$enab_j2store 			= $plg_id_j2store->enabled;
			@$params_j2store	 	= $plg_id_j2store->params;

			if ($plg_extid_j2store){
			   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='payment_ccideal'");
			   $db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='payment_ccideal' ";
			$db->setQuery($select);
			$result = $db->loadResult();

			if($result!="payment_ccideal"){

				$j2store = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('J2Store - cciDEAL integration', 'plugin', 'payment_ccideal', 'j2store', '0', '".$enab_j2store."', '1', '0', '{\"legacy\":true,\"name\":\"J2Store - cciDEAL integration\",\"type\":\"plugin\",\"creationDate\":\"October 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"1.0.0\",\"description\":\"J2Store! iDEAL Payment Plugin. Make sure you publish the plugin\",\"group\":\"\"}', '".$params_j2store."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery($j2store);
				$db->query();
			}

			//language

			$copy_File 	= JPATH_ROOT.DS.'plugins'.DS.'j2store'.DS.'payment_ccideal'.DS.'languages'.DS.'en-GB.plg_j2store_payment_ccideal.ini';
			$paste_File = JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_j2store_payment_ccideal.ini';

			if(JFile::exists($paste_File)){

				JFile::delete($paste_File);
				JFile::move($copy_File,$paste_File);
			}else{
				JFile::move($copy_File,$paste_File);
			}

			$copy_File1 	= JPATH_ROOT.DS.'plugins'.DS.'j2store'.DS.'payment_ccideal'.DS.'languages'.DS.'en-GB.plg_j2store_payment_ccideal.sys.ini';
			$paste_File1	= JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_j2store_payment_ccideal.sys.ini';

			if(JFile::exists($paste_File1)){

				JFile::delete($paste_File1);
				JFile::move($copy_File1,$paste_File1);
			}else{
				JFile::move($copy_File1,$paste_File1);
			}

			$del_File 	= JPATH_ROOT.DS.'plugins'.DS.'j2store'.DS.'payment_ccideal'.DS.'languages';

			if(JFolder::exists($del_File)){
				JFolder::delete($del_File);
			}
		}
		
		/* Plugin: TechJoomla */
		if(($element == "ccideal")&&($folder == "payment" )){

			$copy_Folder 	= JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccidealplatform'.DS.'plugins'.DS.'techjoomla'.DS.'ccideal';
			$paste_Folder 	= JPATH_ROOT.DS.'plugins'.DS.'payment'.DS.'ccideal';

			if(JFolder::exists( JPATH_ROOT.DS.'plugins'.DS.'payment'.DS.'ccideal')){

				JFolder::delete(JPATH_ROOT.DS.'plugins'.DS.'payment'.DS.'ccideal');
				JFolder::copy($copy_Folder,$paste_Folder);
			}else{
				JFolder::copy($copy_Folder,$paste_Folder);
			}

			$db->setQuery("SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='ccideal' AND `folder`='payment' LIMIT 1");
			$plg_id_techjoomla = $db->loadObject();

			$plg_extid_techjoomla = ""; $enab_techjoomla = 0; $params_techjoomla = "{}";

			@$plg_extid_techjoomla  	= $plg_id_techjoomla->extension_id;
			@$enab_techjoomla 			= $plg_id_techjoomla->enabled;
			@$params_techjoomla	 		= $plg_id_techjoomla->params;

			if ($plg_extid_techjoomla){
			   $db->setQuery("DELETE FROM `#__extensions` WHERE `element`='ccideal'");
			   $db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='ccideal' ";
			$db->setQuery($select);
			$result = $db->loadResult();

			if($result!="ccideal"){

				$payment = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('TechJoomla extensions - cciDEAL integration', 'plugin', 'ccideal', 'payment', '0', '".$enab_techjoomla."', '1', '0', '{\"legacy\":true,\"name\":\"TechJoomla extensions - cciDEAL integration\",\"type\":\"plugin\",\"creationDate\":\"November 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"1.0.0\",\"description\":\"TechJoomla extensions - cciDEAL integration Payment Plugin. Make sure you publish the plugin\",\"group\":\"\"}', '".$params_techjoomla."', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery($payment);
				$db->query();
			}

			//language

			$copy_File 	= JPATH_ROOT.DS.'plugins'.DS.'payment'.DS.'ccideal'.DS.'en-GB'.DS.'en-GB.plg_payment_ccideal.ini';
			$paste_File = JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'en-GB'.DS.'en-GB.plg_payment_ccideal.ini';

			if(JFile::exists($paste_File)){

				JFile::delete($paste_File);
				JFile::move($copy_File,$paste_File);
			}else{
				JFile::move($copy_File,$paste_File);
			}

			$copy_File1 	= JPATH_ROOT.DS.'plugins'.DS.'payment'.DS.'ccideal'.DS.'nl-NL'.DS.'nl-NL.plg_payment_ccideal.sys.ini';
			$paste_File1	= JPATH_ROOT.DS.'administrator'.DS.'language'.DS.'nl-NL'.DS.'nl-NL.plg_payment_ccideal.sys.ini';

			if(JFile::exists($paste_File1)){

				JFile::delete($paste_File1);
				JFile::move($copy_File1,$paste_File1);
			}else{
				JFile::move($copy_File1,$paste_File1);
			}

			$del_File 	= JPATH_ROOT.DS.'plugins'.DS.'payment'.DS.'ccideal'.DS.'en-GB';

			if(JFolder::exists($del_File)){
				JFolder::delete($del_File);
			}
			$del_File2 	= JPATH_ROOT.DS.'plugins'.DS.'payment'.DS.'ccideal'.DS.'nl-NL';

			if(JFolder::exists($del_File2)){
				JFolder::delete($del_File2);
			}
		}
		
		$session = JFactory::getSession();
		$session->set('msg_plug', 'plug_sucs');

		$this->setRedirect('index.php?option=com_ccidealplatform&view=integrations');
	}
	public function refresh(){
		return $this->setRedirect('index.php?option=com_ccidealplatform&view=integrations');
	}
}
