<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );

class cciDealPlatformControllerccideal extends JControllerLegacy
{
	function __construct( $config = array() )
	{
		parent::__construct( $config );
	}

	function display($cachable = false, $urlparams = false)
	{
		JRequest::setVar( 'view', 'ccideal');
		JRequest::setVar( 'views', 'email_sucs');
		JRequest::setVar( 'views', 'email_fail');
		JRequest::setVar( 'views', 'email_cancel');
		parent::display();
	}

	function save()
	{
		$model = & $this->getModel('ccideal');

		$save_result =$model->save();

		if( $save_result == 1) {
			$msg = JText::_( 'ID_CONFIG_SAVED_SUCCESS' );
			$this->setRedirect('index.php?option=com_ccidealplatform', $msg);
		}
		else if($save_result == 0 || $save_result == -1) {
			JError::raiseWarning(100, JText::_('ID_CONFIG_SAVED_FAILURE'));
		}
		else if($save_result == -3) {
			JError::raiseWarning(100, JText::_('ID_ERROR_NOT_CERT_FILE'));
		}
		else if($save_result == -4) {
			JError::raiseWarning(100, JText::_('ID_ERROR_FILE_KEY_EXIST_NOT_DELETED'));
		}
		else if($save_result == -5) {
			JError::raiseWarning(100, JText::_('ID_WARN_NO_FILES_UPLOAD'));
		}
		else if($save_result == -6) {
			JError::raiseWarning(100, JText::_('ID_ERROR_FILE_KEY_EXIST_NOT_DELETED'));
		}
		else if($save_result == -7) {
			JError::raiseWarning(100, JText::_('ID_WARN_NO_FILES_UPLOAD'));
		}
		$this->setRedirect('index.php?option=com_ccidealplatform');
	}

	function zipFilesAndDownload()
	{
		jimport( 'joomla.filesystem.folder' );
		jimport( 'joomla.filesystem.archive' );
		jimport('joomla.filesystem.path');
		jimport('joomla.filesystem.file');
		$path=JURI::root();

		$payment_class 	= 	$this->getBankData();

		$filesArray = array();
		$archivename="certificates.zip";

		$model 			= $this->getModel('ccideal');
		$payment_class 	= $model->getBankData();

		$payment_bank 	= $payment_class->IDEAL_Bank;

		//For iDEAL v3 certificates
		if($payment_class->iDEAL_ProVersion){
			$certs = $payment_class->IDEAL_Privatecert_v3;
			$priv  = $payment_class->IDEAL_Privatekey_v3;

			$files=array($certs,$priv);

			foreach($files as $file)
			{
				if($payment_bank=='ING'){
						$data =  JFile::read(JPATH_SITE . DS .'components'. DS .'com_ccidealplatform'. DS .'ccideal_adv_pro_idealv3'. DS .'ing_certificates'. DS.$certs);
						$data =  JFile::read(JPATH_SITE . DS .'components'. DS .'com_ccidealplatform'. DS .'ccideal_adv_pro_idealv3'. DS .'ing_certificates'. DS.'priv.pem');
				}elseif($payment_bank=="RABOPROF"){
						$data =  JFile::read(JPATH_SITE. DS .'components'. DS .'com_ccidealplatform'. DS .'ccideal_adv_pro_idealv3'. DS .'rabo_certificates'. DS.$certs);
						$data =  JFile::read(JPATH_SITE. DS .'components'. DS .'com_ccidealplatform'. DS .'ccideal_adv_pro_idealv3'. DS .'rabo_certificates'. DS.'priv.pem');
				}
				 $filesArray[] = array('name' => $file, 'data' => $data);
			}
		}
		
		$zip =  JArchive::getAdapter('zip');
		$zip->create($archivename, $filesArray,array(),array());

		header("Content-Disposition: attachment; filename=$archivename");
		header("Pragma: no-cache");
		header("Expires: 0");
		readfile("$archivename");
		$this->setRedirect('index.php?option=com_ccidealplatform');
		exit;
	}

	function  getBankData(){
		$db = JFactory::getDBO();
		$query = "SELECT * FROM #__ccidealplatform_config" ;
		$db->setQuery($query);
		$ideal_config_array = $db->loadObject();
		return $ideal_config_array;
	}
}
?>