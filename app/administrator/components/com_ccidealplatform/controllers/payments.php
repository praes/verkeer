<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.controller' );

class cciDealPlatformControllerpayments extends JControllerLegacy
{
	function __construct( $config = array() )
	{
		parent::__construct( $config );
	}

	function display($cachable = false, $urlparams = false)
	{
		JRequest::setVar( 'view', 'payments');
		parent::display();
	}
	function statusupdate(){

		$pid = JRequest::getVar('oid');
		$status = JRequest::getVar('sv');
		$divid = JRequest::getVar('offsetid');

		$db = JFactory :: getDBO();

		$query = "UPDATE #__ccidealplatform_payments SET payment_status = '".$status."' WHERE id = '" . $pid . "'";
		$db->setQuery($query);
		if($db->Query()){
			$select = "SELECT `order_id` FROM #__ccidealplatform_payments WHERE `id`='$pid'";
			$db->setQuery($select);
			$takeOid = $db->loadResult();

			$queryLog = "UPDATE #__ccidealplatform_logs SET payment_status = '$status' WHERE purchaseID = '" . $takeOid . "'";
			$db->setQuery($queryLog);
			$db->Query();

			$msg = JText::_( 'CCIDEAL_PAYMENTS_UPDATESTATUS' );
			$this->setRedirect( 'index.php?option=com_ccidealplatform&view=payments' ,$msg);
		}
	}

	function remove()
	{
		// get the model
		$model = & $this->getModel('payments');
		if($model->delete())
		{
			$msg = JText::_( 'CC_IDEAL_DELETED_SUCCESS' );
		}
		else
		{
			$msg = JText::_( 'CC_ERROR_IDEAL_DELET' );
		}

		$this->setRedirect( 'index.php?option=com_ccidealplatform&view=payments' ,$msg);
	}
}
?>