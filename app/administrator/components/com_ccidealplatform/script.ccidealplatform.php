<?php

/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/


defined('_JEXEC') or die('Restricted access');
@define( 'DS', DIRECTORY_SEPARATOR );

jimport('joomla.filesystem.path');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder' );

class com_ccidealplatformInstallerScript {
	function install( $parent ) {
		$this->installitems();

		//enqueueMessage for install success
		$this->alertmsg( 1 );
	}

	function update( $parent ) {
		$this->installitems();

		//enqueueMessage for update success
		$this->alertmsg( 2 );
	}

	function uninstall( $parent ) {
		$this->uninstallitems();

		// Only uninstall Chill Creations update plugin if
		// there are no other Chill Creations extensions installed on this website

		if ( JFolder::exists( JPATH_SITE . DS . 'plugins' . DS . 'installer' . DS . 'chillcreations_update' ) ) {

			// This code is loaded before the extension is uninstalled, and therefor counts in the db query,
			// so the plugin should be uninstalled if there are one or less Chill Creations extensions
			$db = JFactory::getDBO();
			$db->setQuery( "SELECT count(*) FROM  #__extensions WHERE element = 'com_ccinvoices' OR element = 'com_ccidealplatform' OR element = 'com_ccnewsletter'" );
			$other_extensions_installed = $db->loadResult();
			if ( $other_extensions_installed <= 1 ) {

				$destinationfile = JPATH_SITE . DS . 'plugins' . DS . 'installer' . DS . 'chillcreations_update';
				JFolder::delete( $destinationfile );

				$db->setQuery( "SELECT count(*) FROM  #__extensions WHERE element= 'chillcreations_update'" );
				if ( $db->loadResult() ) {
					$sql = "DELETE FROM `#__extensions` WHERE element='chillcreations_update'";
					$db->setQuery( $sql );
					$db->execute();
				}
			}
		}

		//enqueueMessage for uninstall success
		$this->alertmsg( 3 );
	}

	function alertmsg( $code ) {

		$jversion        = new JVersion();
		$current_version = $jversion->getShortVersion();
		$current_version = substr( $current_version, 0, 1 );

		?>
		<style>
			div.alert-success p:last-child, dd.message ul li:last-child {
				display: none;
			}
		</style>
		<?php

		if ( $current_version == 3 ) {
			if ( $code == 1 ) {
				$msg = '<p>Installing cciDEAL was successful.</p><a href="index.php?option=com_ccidealplatform" class="btn btn-medium btn-success">Go to cciDEAL</a>';
			} else if ( $code == 2 ) {
				$msg = '<p>Updating cciDEAL was successful.</p><a href="index.php?option=com_ccidealplatform" class="btn btn-medium btn-success">Go to cciDEAL</a>';
			} else if ( $code == 3 ) {
				$msg = '<p>Uninstalling cciDEAL was successful.</p>';
			}
		} else {
			if ( $code == 1 ) {
				$msg = 'Installing cciDEAL was successful.';
			} else if ( $code == 2 ) {
				$msg = 'Updating cciDEAL was successful.';
			} else if ( $code == 3 ) {
				$msg = 'Uninstalling cciDEAL was successful.';
			}
		}
		JFactory::getApplication()->enqueueMessage( $msg );
	}

	function preflight( $type, $parent ) {
		return true;
	}

	function postflight( $type, $parent ) {

		// David - 21 April 2015 - cciDEAL 4.4.5
		// Removed "add" + removed stability column
		// Now using Joomla! updater

		if ( ! $this->checkFieldExist( "#__ccidealplatform_config", "minimum_stability" ) ) {
			$query = "ALTER TABLE `#__ccidealplatform_config`  DROP `minimum_stability`";
			$db    = JFactory::getDBO();
			$db->setQuery( $query );
			$db->execute();
		}

		return true;
	}

	function uninstallitems() {
		$plg_installer = new JInstaller();

		$db = JFactory::getDBO();

		$db->setQuery( "SELECT extension_id FROM #__extensions WHERE `element`='content_ideal' AND `folder`='content' LIMIT 1" );
		$plg_id = $db->loadResult();

		if ( $plg_id ) {
			$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='content_ideal'" );
			$db->query();
		}
		$db->setQuery( "SELECT extension_id FROM #__extensions WHERE `element`='ideal_editor' AND `folder`='editors-xtd' LIMIT 1" );
		$plg_id = $db->loadResult();

		if ( $plg_id ) {
			$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='ideal_editor'" );
			$db->query();
		}


		if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'editors-xtd' . DS . 'ideal_editor' ) ) {
			JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'editors-xtd' . DS . 'ideal_editor' );
		}
		if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'content' . DS . 'content_ideal' ) ) {
			JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'content' . DS . 'content_ideal' );
		}
	}

	function installChillCreationsBootstrap() {
		// Install Chill Creations Bootstrap
		JLoader::import( 'joomla.filesystem.folder' );
		JLoader::import( 'joomla.filesystem.file' );

		$target = JPATH_ROOT . '/media/chillcreations_bootstrap';

		if ( JFolder::exists( $target ) ) {
			JFolder::delete( $target );
		}
		JFolder::move( JPATH_ROOT . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'chillcreations_bootstrap', $target );
	}

	function removeOldLanguageFiles() {

		// David - 02 April 2015 - cciDEAL 4.4.4
		// Search all core language folders and remove old cciDEAL files

		// Use for debugging
		// echo "<pre>"; print_r(JLanguage::getKnownLanguages());

		if ( $languages = JLanguage::getKnownLanguages() ) {

			foreach ( $languages as $tag => $language ) {

				// Use for debugging
				// echo JPATH_ROOT.DS.'language'.DS.$tag.DS.$tag.$file;

				$language_files = array (
					".com_ccidealplatform.ini",
					".com_ccidealplatform.menu.ini",
					".com_ccidealplatform.sys.ini",
					".plg_content_ideal.ini",
					".plg_editors-xtd_ideal_editor.ini"
				);

				foreach ( $language_files as $file ) {
					//Admin files
					if ( JFile::exists( JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . $tag . DS . $tag . $file ) ) {
						JFile::delete( JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . $tag . DS . $tag . $file );
					}
					// Site files
					if ( JFile::exists( JPATH_ROOT . DS . 'language' . DS . $tag . DS . $tag . $file ) ) {
						JFile::delete( JPATH_ROOT . DS . 'language' . DS . $tag . DS . $tag . $file );
					}
				}
			}

		}

	}

	function installChillCreationsUpdatePlugin() {

		$db = JFactory::getDBO();

		if ( ! JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'installer' . DS . 'chillcreations_update' ) ) {
			JFolder::create( JPATH_ROOT . DS . 'plugins' . DS . 'installer' );
			JFolder::move( JPATH_ROOT . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'chillcreations_update',
				JPATH_ROOT . DS . 'plugins' . DS . 'installer' . DS . 'chillcreations_update' );


			$db->setQuery( "SELECT count(*) FROM  #__extensions WHERE `element`='chillcreations_update' AND `folder`='installer'" );
			if ( ! $db->loadResult() ) {

				$manifest_cache_chillcreations_updates = '{\"name\":\"Chill Creations update - ccNewsletter, cciDEAL, ccInvoices\",\"type\":\"plugin\",\"creationDate\":\"REFRESH CACHE\",\"author\":\"Chill Creations\",\"copyright\":\"Copyright 2008 (c) Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\/\/www.chillcreations.com\",\"version\":\"REFRESH CACHE\",\"description\":\"Handle the updates for cciDEAL, ccNewsletter, ccInvoices\",\"group\":\"\"}';

				$sql = "INSERT INTO  #__extensions (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`,
`manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`)
VALUES('Chill Creations update - ccNewsletter, cciDEAL, ccInvoices', 'plugin', 'chillcreations_update', 'installer', 0,
1, 1, 0,'" . $manifest_cache_chillcreations_updates . "', '', '', '', 0, '0000-00-00 00:00:00', 0, 0)";

				$db->setQuery( $sql );
				$db->query();
			}
		} else {
			if ( JFolder::exists( JPATH_ROOT . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'chillcreations_update' ) ) {
				if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'installer' . DS . 'chillcreations_update' ) ) {
					JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'installer' . DS . 'chillcreations_update' );
					JFolder::move( JPATH_ROOT . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS
					               . 'chillcreations_update', JPATH_ROOT . DS . 'plugins' . DS . 'installer' . DS
					                                          . 'chillcreations_update' );
				}
			}
		}

	}

	function integrationplugin() {

		$jversion        = new JVersion();
		$current_version = $jversion->getShortVersion();
		$jversion_sht    = substr( $current_version, 0, 1 );

		$db = JFactory::getDBO();

		/* Plugin: content_ideal */

		$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'content_ideal';
		$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'content' . DS . 'content_ideal';

		if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'content' . DS . 'content_ideal' ) ) {
			JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'content' . DS . 'content_ideal' );
			JFolder::copy( $copy_Folder, $paste_Folder );
		} else {
			JFolder::copy( $copy_Folder, $paste_Folder );
		}

		$db->setQuery( "SELECT extension_id,enabled FROM #__extensions WHERE `element`='content_ideal' AND `folder`='content' LIMIT 1" );
		$plg_id_content = $db->loadObject();

		$plg_extid_content = "";
		$enab_content      = 0;

		@$plg_extid = $plg_id_content->extension_id;
		@$enab_content = $plg_id_content->enabled;

		if ( $plg_extid_content ) {
			$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='content_ideal'" );
			$db->query();
		}

		$selects = "SELECT element FROM #__extensions WHERE element='content_ideal' ";
		$db->setQuery( $selects );
		$results = $db->loadResult();

		if ( $results != "content_ideal" ) {

			$content = "INSERT INTO `#__extensions` (`name`,`type`,`element`,`folder`,`client_id`,`enabled`,`access`,`protected`,`manifest_cache`,`params`,`custom_data`,`system_data`,`checked_out`,`checked_out_time`,`ordering`,`state`) Values('Content - iDEAL','plugin','content_ideal','content','0','" . $enab_content . "','1','0','{\"legacy\":true,\"name\":\"Content Plugin for cciDEAL Platform\",\"type\":\"plugin\",\"creationDate\":\"October 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\/\/www.chillcreations.com\",\"version\":\"4.3.0\",\"description\":\"Pay with iDEAL from a Joomla! article\",\"group\":\"\"}','{}','','','0','','3','0')";
			$db->setQuery( $content );
			$db->query();

		}

		$selectname = "SELECT `name` FROM #__extensions WHERE element='content_ideal'";
		$db->setQuery( $selectname );
		$resultName = $db->loadResult();

		if ( $resultName != "Content - iDEAL" ) {
			$update = "UPDATE #__extensions SET `name`='Content - iDEAL' WHERE element='content_ideal'";
			$db->setQuery( $update );
			$db->Query();
		}

		$update1 = "UPDATE #__extensions SET `manifest_cache`='{\"legacy\":true,\"name\":\"Content Plugin for cciDEAL Platform\",\"type\":\"plugin\",\"creationDate\":\"May 2014\",\"author\":\"Chill Creations\",\"copyright\":\"Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\/\/www.chillcreations.com\",\"version\":\"4.1.0\",\"description\":\"Pay with iDEAL from a Joomla! article\",\"group\":\"\"}' WHERE element='content_ideal'";
		$db->setQuery( $update1 );
		$db->Query();

		/* Plugin: ideal_editor */

		$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'ideal_editor';
		$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'editors-xtd' . DS . 'ideal_editor';

		if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'editors-xtd' . DS . 'ideal_editor' ) ) {

			JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'editors-xtd' . DS . 'ideal_editor' );
			JFolder::copy( $copy_Folder, $paste_Folder );
		} else {
			JFolder::copy( $copy_Folder, $paste_Folder );
		}

		$db->setQuery( "SELECT extension_id,enabled FROM #__extensions WHERE `element`='ideal_editor' AND `folder`='editors-xtd' LIMIT 1" );
		$plg_id_editor = $db->loadObject();

		$plg_extid_editor = "";
		$enab_editor      = 1;

		@$plg_extid_editor = $plg_id_editor->extension_id;
		@$enab_editor = $plg_id_editor->enabled;

		if ( @$plg_extid_editor == "" ) {
			$enab_editor = 1;
		}

		if ( $plg_extid_editor ) {
			$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='ideal_editor'" );
			$db->query();
		}

		$select = "SELECT element FROM #__extensions WHERE element='ideal_editor' ";
		$db->setQuery( $select );
		$result = $db->loadResult();

		if ( $result != "ideal_editor" ) {

			$editor = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('Editor button - iDEAL', 'plugin', 'ideal_editor', 'editors-xtd', '0', '" . $enab_editor . "', '1', '0', '{\"legacy\":true,\"name\":\"Editor button - iDEAL\",\"type\":\"plugin\",\"creationDate\":\"October 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.3.0\",\"description\":\" iDEAL Editors\",\"group\":\"\"}', '{}', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
			$db->setQuery( $editor );
			$db->query();
		}

		/* Plugin: ccInvoices */

		$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='plg_ccinvoices_ideal' AND `folder`='ccinvoices_payment' LIMIT 1" );
		$plg_id_ccinvoices = $db->loadObject();

		if ( @$plg_id_ccinvoices->extension_id ) {

			$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'ccinvoices' . DS . 'plg_ccinvoices_ideal';
			$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'ccinvoices_payment' . DS . 'plg_ccinvoices_ideal';

			if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'ccinvoices_payment' . DS . 'plg_ccinvoices_ideal' ) ) {

				JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'ccinvoices_payment' . DS . 'plg_ccinvoices_ideal' );
				JFolder::copy( $copy_Folder, $paste_Folder );
			} else {
				JFolder::copy( $copy_Folder, $paste_Folder );
			}

			$plg_extid_ccinvoices = "";
			$enab_ccinvoices      = 0;
			$params_ccinvoices    = "{}";

			@$plg_extid_ccinvoices = $plg_id_ccinvoices->extension_id;
			@$enab_ccinvoices = $plg_id_ccinvoices->enabled;
			@$params_ccinvoices = $plg_id_ccinvoices->params;

			if ( $plg_extid_ccinvoices ) {
				$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='plg_ccinvoices_ideal'" );
				$db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='plg_ccinvoices_ideal' ";
			$db->setQuery( $select );
			$result = $db->loadResult();

			if ( $result != "plg_ccinvoices_ideal" ) {

				$ccinvoices = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('ccInvoices - Payment - cciDEAL Platform', 'plugin', 'plg_ccinvoices_ideal', 'ccinvoices_payment', '0', '" . $enab_ccinvoices . "', '1', '0', '{\"legacy\":true,\"name\":\"ccInvoices - Payment - cciDEAL Platform\",\"type\":\"plugin\",\"creationDate\":\"REFRESH CACHE\",\"author\":\"Chill Creations\",\"copyright\":\"2008 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"REFRESH CACHE\",\"description\":\"cciDEAL Platform plugin for ccInvoices\",\"group\":\"\"}', '" . $params_ccinvoices . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery( $ccinvoices );
				$db->query();
			}
		}

		/* Plugin: Akeeba subscription */

		$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='iDEAL' AND `folder`='akpayment' LIMIT 1" );
		$plg_id_akeeba = $db->loadObject();

		if ( @$plg_id_akeeba->extension_id ) {

			$akeeba = "";
			$akeeba = JFactory::getXML( JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_akeebasubs' . DS . 'aaa_akeebasubs.xml' );
			$akbs   = (string) $akeeba->version;

			if ( substr( $akbs, 0, 3 ) > 4.0 ) {
				$akeeba_t = "akeeba";
				$akeeba_v = "4.1.0";
			} else {
				$akeeba_t = "akeeba3";
				$akeeba_v = "4.0.0";
			}

			$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . $akeeba_t . DS . 'plg_akeebasubs_ideal';
			$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'akpayment' . DS . 'iDEAL';


			if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'akpayment' . DS . 'iDEAL' ) ) {

				JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'akpayment' . DS . 'iDEAL' );
				JFolder::copy( $copy_Folder, $paste_Folder );
			} else {
				JFolder::copy( $copy_Folder, $paste_Folder );
			}

			$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='iDEAL' AND `folder`='akpayment' LIMIT 1" );
			$plg_id_akeeba = $db->loadObject();

			$plg_extid_akeeba = "";
			$enab_akeeba      = 0;
			$params_akeeba    = "{}";

			@$plg_extid_akeeba = $plg_id_akeeba->extension_id;
			@$enab_akeeba = $plg_id_akeeba->enabled;
			@$params_akeeba = $plg_id_akeeba->params;

			if ( $plg_extid_akeeba ) {
				$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='iDEAL' AND `folder`='akpayment'" );
				$db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='iDEAL' AND `folder`='akpayment'";
			$db->setQuery( $select );
			$result = $db->loadResult();

			if ( $result != "iDEAL" ) {

				$akeeba = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('Akeeba Subscriptions Payment - iDEAL', 'plugin', 'iDEAL', 'akpayment', '0', '" . $enab_akeeba . "', '1', '0', '{\"legacy\":true,\"name\":\"Akeeba Subscriptions Payment - iDEAL\",\"type\":\"plugin\",\"creationDate\":\"June 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"$akeeba_v\",\"description\":\"cciDEAL Platform plugin for Akeeba Subscriptions\",\"group\":\"\"}', '" . $params_akeeba . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery( $akeeba );
				$db->query();
			}

			//language

			$copy_File  = JPATH_ROOT . DS . 'plugins' . DS . 'akpayment' . DS . 'iDEAL' . DS . 'en-GB.plg_akpayment_iDEAL.ini';
			$paste_File = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_akpayment_iDEAL.ini';

			if ( JFile::exists( $copy_File ) ) {

				JFile::delete( $paste_File );
				JFile::move( $copy_File, $paste_File );
			} else {
				JFile::move( $copy_File, $paste_File );
			}
		}

		/* Plugin: Hikashop */

		$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='iDEAL' AND `folder`='hikashoppayment' LIMIT 1" );
		$plg_id_hikashop = $db->loadObject();

		if ( @$plg_id_hikashop->extension_id ) {

			$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'hikashop' . DS . 'plg_hikashop_ideal';
			$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'hikashoppayment' . DS . 'iDEAL';

			if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'hikashoppayment' . DS . 'iDEAL' ) ) {

				JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'hikashoppayment' . DS . 'iDEAL' );
				JFolder::copy( $copy_Folder, $paste_Folder );
			} else {
				JFolder::copy( $copy_Folder, $paste_Folder );
			}

			$plg_extid_hikashop = "";
			$enab_hikashop      = 0;
			$params_hikashop    = "{}";

			@$plg_extid_hikashop = $plg_id_hikashop->extension_id;
			@$enab_hikashop = $plg_id_hikashop->enabled;
			@$params_hikashop = $plg_id_hikashop->params;

			if ( $plg_extid_hikashop ) {
				$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='iDEAL' AND `folder`='hikashoppayment'" );
				$db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='iDEAL' AND `folder`='hikashoppayment'";
			$db->setQuery( $select );
			$result = $db->loadResult();

			if ( $result != "iDEAL" ) {

				$hikashop = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('HikaShop iDEAL payment plugin', 'plugin', 'iDEAL', 'hikashoppayment', '0', '" . $enab_hikashop . "', '1', '0', '{\"legacy\":true,\"name\":\"HikaShop iDEAL payment plugin\",\"type\":\"plugin\",\"creationDate\":\"REFRESH CACHE\",\"author\":\"Chill Creations\",\"copyright\":\"2008 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"REFRESH CACHE\",\"description\":\"Hikashop iDEAL plugin for cciDEAL Platform\",\"group\":\"\"}', '" . $params_hikashop . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery( $hikashop );
				$db->query();
			}
		}

		/* Plugin: TicketMaster */

		$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='rdmccidealplatform' AND `folder`='rdmedia' LIMIT 1" );
		$plg_id_ticketmaster = $db->loadObject();

		if ( @$plg_id_ticketmaster->extension_id ) {

			$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'ticketmaster' . DS . 'rdmccidealplatform';
			$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'rdmedia' . DS . 'rdmccidealplatform';

			if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'rdmedia' . DS . 'rdmccidealplatform' ) ) {

				JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'rdmedia' . DS . 'rdmccidealplatform' );
				JFolder::copy( $copy_Folder, $paste_Folder );
			} else {
				JFolder::copy( $copy_Folder, $paste_Folder );
			}

			$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='rdmccidealplatform' AND `folder`='rdmedia' LIMIT 1" );
			$plg_id_ticketmaster = $db->loadObject();

			$plg_extid_ticketmaster = "";
			$enab_ticketmaster      = 0;
			$params_ticketmaster    = "{}";

			@$plg_extid_ticketmaster = $plg_id_ticketmaster->extension_id;
			@$enab_ticketmaster = $plg_id_ticketmaster->enabled;
			@$params_ticketmaster = $plg_id_ticketmaster->params;

			if ( $plg_extid_ticketmaster ) {
				$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='rdmccidealplatform' AND `folder`='rdmedia'" );
				$db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='rdmccidealplatform' AND `folder`='rdmedia'";
			$db->setQuery( $select );
			$result = $db->loadResult();

			if ( $result != "rdmccidealplatform" ) {

				$ticketmaster = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('RD-Media iDEAL by cciDEAL', 'plugin', 'rdmccidealplatform', 'rdmedia', '0', '" . $enab_ticketmaster . "', '1', '0', '{\"legacy\":true,\"name\":\"RD-Media iDEAL by cciDEAL\",\"type\":\"plugin\",\"creationDate\":\"November 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.3.0\",\"description\":\"iDEAL Payment Processor for RD-Media\",\"group\":\"\"}', '" . $params_ticketmaster . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery( $ticketmaster );
				$db->query();
			}
		}

		/* Plugin: RSMembership */

		$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='rsmembership_ccidealplatform' AND `folder`='system' LIMIT 1" );
		$plg_id_rsmembership = $db->loadObject();

		if ( @$plg_id_rsmembership->extension_id ) {

			$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'rsmembership' . DS . 'plg_rsmembership_ideal';
			$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsmembership_ccidealplatform';

			if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsmembership_ccidealplatform' ) ) {

				JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsmembership_ccidealplatform' );
				JFolder::copy( $copy_Folder, $paste_Folder );
			} else {
				JFolder::copy( $copy_Folder, $paste_Folder );
			}

			$plg_extid_rsmembership = "";
			$enab_rsmembership      = 0;
			$params_rsmembership    = "{}";

			@$plg_extid_rsmembership = $plg_id_rsmembership->extension_id;
			@$enab_rsmembership = $plg_id_rsmembership->enabled;
			@$params_rsmembership = $plg_id_rsmembership->params;

			if ( $plg_extid_rsmembership ) {
				$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='rsmembership_ccidealplatform'" );
				$db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='rsmembership_ccidealplatform' ";
			$db->setQuery( $select );
			$result = $db->loadResult();

			if ( $result != "rsmembership_ccidealplatform" ) {

				$rsmembership = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('System - RSMembership - cciDEAL Platform', 'plugin', 'rsmembership_ccidealplatform', 'system', '0', '" . $enab_rsmembership . "', '1', '0', '{\"legacy\":true,\"name\":\"System - RSMembership - cciDEAL Platform\",\"type\":\"plugin\",\"creationDate\":\"October 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.2.0\",\"description\":\"RSMembership iDEAL plugin for cciDEAL Platform\",\"group\":\"\"}', '" . $params_rsmembership . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery( $rsmembership );
				$db->query();
			}

			//language

			$copy_File  = JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsmembership_ccidealplatform' . DS . 'en-GB.plg_system_rsmembership_ccidealplatform.ini';
			$paste_File = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_system_rsmembership_ccidealplatform.ini';

			if ( JFile::exists( $copy_File ) ) {

				JFile::delete( $paste_File );
				JFile::move( $copy_File, $paste_File );
			} else {
				JFile::move( $copy_File, $paste_File );
			}
		}

		/* Plugin: RSEvents Pro */

		$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='rsepro_ideal' AND `folder`='system' LIMIT 1" );
		$plg_id_rseventspro = $db->loadObject();

		if ( @$plg_id_rseventspro->extension_id ) {

			$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'rseventspro' . DS . 'plg_rseventspro_ideal';
			$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsepro_ideal';

			if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsepro_ideal' ) ) {

				JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsepro_ideal' );
				JFolder::copy( $copy_Folder, $paste_Folder );
			} else {
				JFolder::copy( $copy_Folder, $paste_Folder );
			}

			$plg_extid_rseventspro = "";
			$enab_rseventspro      = 0;
			$params_rseventspro    = "{}";

			@$plg_extid_rseventspro = $plg_id_rseventspro->extension_id;
			@$enab_rseventspro = $plg_id_rseventspro->enabled;
			@$params_rseventspro = $plg_id_rseventspro->params;

			if ( $plg_extid_rseventspro ) {
				$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='rsepro_ideal'" );
				$db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='rsepro_ideal' ";
			$db->setQuery( $select );
			$result = $db->loadResult();

			if ( $result != "rsepro_ideal" ) {

				$rseventspro = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('RSEvents!Pro iDEAL payment', 'plugin', 'rsepro_ideal', 'system', '0', '" . $enab_rseventspro . "', '1', '0', '{\"legacy\":true,\"name\":\"RSEvents!Pro iDEAL payment\",\"type\":\"plugin\",\"creationDate\":\"October 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.2.0\",\"description\":\"RSEvents!Pro iDEAL plugin. Make sure you publish the plugin\",\"group\":\"\"}', '" . $params_rseventspro . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery( $rseventspro );
				$db->query();
			}

			//language

			$copy_File  = JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsepro_ideal' . DS . 'en-GB.plg_system_rsepro_ideal.ini';
			$paste_File = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_system_rsepro_ideal.ini';

			if ( JFile::exists( $copy_File ) ) {

				JFile::delete( $paste_File );
				JFile::move( $copy_File, $paste_File );
			} else {
				JFile::move( $copy_File, $paste_File );
			}
		}

		/* Plugin: RSForm Pro */

		$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='rsfp_ideal' AND `folder`='system' LIMIT 1" );
		$plg_id_rsformpro = $db->loadObject();

		if ( @$plg_id_rsformpro->extension_id ) {

			$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'rsformpro' . DS . 'plg_rsformpro_ideal';
			$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsfp_ideal';

			if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsfp_ideal' ) ) {

				JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsfp_ideal' );
				JFolder::copy( $copy_Folder, $paste_Folder );
			} else {
				JFolder::copy( $copy_Folder, $paste_Folder );
			}

			$plg_extid_rsformpro = "";
			$enab_rsformpro      = 0;
			$params_rsformpro    = "{}";

			@$plg_extid_rsformpro = $plg_id_rsformpro->extension_id;
			@$enab_rsformpro = $plg_id_rsformpro->enabled;
			@$params_rsformpro = $plg_id_rsformpro->params;

			if ( $plg_extid_rsformpro ) {
				$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='rsfp_ideal'" );
				$db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='rsfp_ideal' ";
			$db->setQuery( $select );
			$result = $db->loadResult();

			if ( $result != "rsfp_ideal" ) {

				$rsformpro = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('System - RSFormPro - cciDEAL Platform', 'plugin', 'rsfp_ideal', 'system', '0', '" . $enab_rsformpro . "', '1', '0', '{\"legacy\":true,\"name\":\"System - RSFormPro - cciDEAL Platform\",\"type\":\"plugin\",\"creationDate\":\"November 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.3.0\",\"description\":\"RSFormPro iDEAL plugin for cciDEAL Platform\",\"group\":\"\"}', '" . $params_rsformpro . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery( $rsformpro );
				$db->query();

				$rsformpro_config = "INSERT IGNORE INTO `#__rsform_config` (`SettingName`, `SettingValue`) VALUES ('ideal.test', '0'),('ideal.tax.type', '1'),('ideal.tax.value', '')";
				$db->setQuery( $rsformpro_config );
				$db->query();

				$rsformpro_ctypes = "INSERT IGNORE INTO `#__rsform_component_types` (`ComponentTypeId`, `ComponentTypeName`) VALUES (700, 'ideal')";
				$db->setQuery( $rsformpro_ctypes );
				$db->query();

				$rsformpro_ctfields = "DELETE FROM #__rsform_component_type_fields WHERE ComponentTypeId = 700";
				$db->setQuery( $rsformpro_ctfields );
				$db->query();

				$rsformpro_ctfields = "INSERT IGNORE INTO `#__rsform_component_type_fields` (`ComponentTypeId`, `FieldName`, `FieldType`, `FieldValues`, `Ordering`) VALUES (700, 'NAME', 'textbox', '', 0),(700, 'LABEL', 'textbox', '', 1),(700, 'COMPONENTTYPE', 'hidden', '700', 2),(700, 'LAYOUTHIDDEN', 'hiddenparam', 'YES', 7)";
				$db->setQuery( $rsformpro_ctfields );
				$db->query();
			}

			//language

			$copy_File  = JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsfp_ideal' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_system_rsfp_ideal.ini';
			$paste_File = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_system_rsfp_ideal.ini';

			if ( JFile::exists( $copy_File ) ) {

				JFile::delete( $paste_File );
				JFile::move( $copy_File, $paste_File );
			} else {
				JFile::move( $copy_File, $paste_File );
			}

			$copy_File1  = JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsfp_ideal' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_system_rsfp_ideal.sys.ini';
			$paste_File1 = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_system_rsfp_ideal.sys.ini';

			if ( JFile::exists( $copy_File1 ) ) {

				JFile::delete( $paste_File1 );
				JFile::move( $copy_File1, $paste_File1 );
			} else {
				JFile::move( $copy_File1, $paste_File1 );
			}

			$del_File = JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsfp_ideal' . DS . 'language';

			if ( JFolder::exists( $del_File ) ) {
				JFolder::delete( $del_File );
			}
		}

		/* Plugin: JoomISP */

		$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='plg_joomisp_ideal' AND `folder`='payment' LIMIT 1" );
		$plg_id_joomisp = $db->loadObject();

		if ( @$plg_id_joomisp->extension_id ) {

			$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'joomisp' . DS . 'plg_joomisp_ideal';
			$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'payment' . DS . 'plg_joomisp_ideal';

			if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'payment' . DS . 'plg_joomisp_ideal' ) ) {

				JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'payment' . DS . 'plg_joomisp_ideal' );
				JFolder::copy( $copy_Folder, $paste_Folder );
			} else {
				JFolder::copy( $copy_Folder, $paste_Folder );
			}

			$plg_extid_joomisp = "";
			$enab_joomisp      = 0;
			$params_joomisp    = "{}";

			@$plg_extid_joomisp = $plg_id_joomisp->extension_id;
			@$enab_joomisp = $plg_id_joomisp->enabled;
			@$params_joomisp = $plg_id_joomisp->params;

			if ( $plg_extid_joomisp ) {
				$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='plg_joomisp_ideal'" );
				$db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='plg_joomisp_ideal' ";
			$db->setQuery( $select );
			$result = $db->loadResult();

			if ( $result != "plg_joomisp_ideal" ) {

				$joomisp = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('JoomISP - iDEAL', 'plugin', 'plg_joomisp_ideal', 'payment', '0', '" . $enab_joomisp . "', '1', '0', '{\"legacy\":true,\"name\":\"JoomISP - iDEAL\",\"type\":\"plugin\",\"creationDate\":\"November 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.3.0\",\"description\":\"JoomISP iDEAL plugin for cciDEAL Platform\",\"group\":\"\"}', '" . $params_joomisp . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery( $joomisp );
				$db->query();
			}
		}

		/* Plugin: Sobi Pro */

		$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='plg_sobipro_ideal' AND `folder`='system' LIMIT 1" );
		$plg_id_sobipro = $db->loadObject();

		if ( @$plg_id_sobipro->extension_id ) {

			$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'sobipro' . DS . 'plg_sobipro_ideal';
			$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'plg_sobipro_ideal';

			if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'plg_sobipro_ideal' ) ) {

				JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'plg_sobipro_ideal' );
				JFolder::copy( $copy_Folder, $paste_Folder );
			} else {
				JFolder::copy( $copy_Folder, $paste_Folder );
			}

			$plg_extid_sobipro = "";
			$enab_sobipro      = 0;
			$params_sobipro    = "{}";

			@$plg_extid_sobipro = $plg_id_sobipro->extension_id;
			@$enab_sobipro = $plg_id_sobipro->enabled;
			@$params_sobipro = $plg_id_sobipro->params;

			if ( $plg_extid_sobipro ) {
				$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='plg_sobipro_ideal'" );
				$db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='plg_sobipro_ideal' ";
			$db->setQuery( $select );
			$result = $db->loadResult();

			if ( $result != "plg_sobipro_ideal" ) {

				$sobipro = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('System - SobiPro - cciDEAL Platform', 'plugin', 'plg_sobipro_ideal', 'system', '0', '" . $enab_sobipro . "', '1', '0', '{\"legacy\":true,\"name\":\"System - SobiPro - cciDEAL Platform\",\"type\":\"plugin\",\"creationDate\":\"January 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.0.0\",\"description\":\"iDEAL Payment for SobiPro\",\"group\":\"\"}', '" . $params_sobipro . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery( $sobipro );
				$db->query();
			}
		}

		/* Plugin: Virtuemart2 */

		$db = JFactory::getDbo();
		$db->setQuery( "SELECT element FROM #__extensions WHERE element = 'plg_virtuemart20_ideal'  AND enabled = 1" );
		$virtuemart_plugin_installed = $db->loadResult();

		// Only run the Virtuemart 2 database updates if cciDEAL integration plugin is installed
		if ( $virtuemart_plugin_installed === 'plg_virtuemart20_ideal' ) {

			$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='plg_virtuemart20_ideal' AND `folder`='vmpayment' LIMIT 1" );
			$plg_id_virtuemart = $db->loadObject();

			if ( ( @$plg_id_virtuemart->extension_id ) && ( $jversion_sht == 2 ) ) {

				$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'virtuemart2' . DS . 'plg_virtuemart_20_ideal';
				$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart20_ideal';

				if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart20_ideal' ) ) {

					JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart20_ideal' );
					JFolder::copy( $copy_Folder, $paste_Folder );
				} else {
					JFolder::copy( $copy_Folder, $paste_Folder );
				}

				$plg_extid_virtuemart = "";
				$enab_virtuemart      = 0;
				$params_virtuemart    = "{}";

				@$plg_extid_virtuemart = $plg_id_virtuemart->extension_id;
				@$enab_virtuemart = $plg_id_virtuemart->enabled;
				@$params_virtuemart = $plg_id_virtuemart->params;


				if ( $plg_extid_virtuemart ) {
					$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='plg_virtuemart20_ideal'" );
					$db->query();
				}

				$select = "SELECT element FROM #__extensions WHERE element='plg_virtuemart20_ideal' ";
				$db->setQuery( $select );
				$result = $db->loadResult();

				if ( $result != "plg_virtuemart20_ideal" ) {

					$virtuemart2 = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('VM Payment - cciDEAL integration', 'plugin', 'plg_virtuemart20_ideal', 'vmpayment', '0', '" . $enab_virtuemart . "', '1', '0', '{\"legacy\":true,\"name\":\"VM Payment - cciDEAL integration\",\"type\":\"plugin\",\"creationDate\":\"November 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.3.0\",\"description\":\"Virtuemart 2.0 iDEAL plugin for cciDEAL Platform\",\"group\":\"\"}', '" . $params_virtuemart . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
					$db->setQuery( $virtuemart2 );
					$db->query();
				}

				//After install
				$alr_paymentm = "SELECT extension_id FROM #__extensions WHERE element='plg_virtuemart20_ideal' ";
				$db->setQuery( $alr_paymentm );
				$apaymentm = $db->loadResult();

				$query = "UPDATE #__virtuemart_paymentmethods SET payment_jplugin_id = '" . $apaymentm . "' WHERE payment_element='plg_virtuemart20_ideal'";
				$db->setQuery( $query );
				$db->Query();

				//Multiple payment parameter

				$copy_mpayment  = JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart20_ideal' . DS . 'ipaymethod.php';
				$paste_mpayment = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_virtuemart' . DS . 'elements' . DS . 'ipaymethod.php';

				if ( JFile::exists( $copy_mpayment ) ) {
					if ( JFile::exists( $paste_mpayment ) ) {
						JFile::delete( $paste_mpayment );
					}
					JFile::move( $copy_mpayment, $paste_mpayment );
				}

				$copy_File1  = JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart20_ideal' . DS . 'en-GB.plg_vmpayment20_iDEAL.ini';
				$paste_File1 = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_vmpayment20_iDEAL.ini';

				$copy_File2  = JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart20_ideal' . DS . 'en-GB.plg_vmpayment20_iDEAL.sys.ini';
				$paste_File2 = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_vmpayment20_iDEAL.sys.ini';

				$copy_File3  = JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart20_ideal' . DS . 'nl-NL.plg_vmpayment20_iDEAL.ini';
				$paste_File3 = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'nl-NL' . DS . 'nl-NL.plg_vmpayment20_iDEAL.ini';

				$copy_File4  = JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart20_ideal' . DS . 'nl-NL.plg_vmpayment20_iDEAL.sys.ini';
				$paste_File4 = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'nl-NL' . DS . 'nl-NL.plg_vmpayment20_iDEAL.sys.ini';


				$paste_folder1 = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'nl-NL';

				if ( ! JFolder::exists( $paste_folder1 ) ) {
					JFolder::create( $paste_folder1 );
				}

				if ( JFile::exists( $copy_File1 ) ) {
					JFile::delete( $paste_File1 );
					JFile::delete( $paste_File2 );
					JFile::delete( $paste_File3 );
					JFile::delete( $paste_File4 );
				}
				JFile::move( $copy_File1, $paste_File1 );
				JFile::move( $copy_File2, $paste_File2 );
				JFile::move( $copy_File3, $paste_File3 );
				JFile::move( $copy_File4, $paste_File4 );

			}
		}

		/* Plugin: Virtuemart3 */

		$db = JFactory::getDbo();
		$db->setQuery( "SELECT element FROM #__extensions WHERE element = 'plg_virtuemart3_ideal' AND enabled = 1" );
		$virtuemart_plugin_installed = $db->loadResult();

		// Only run the Virtuemart 3 database updates if cciDEAL integration plugin is installed
		if ( $virtuemart_plugin_installed === 'plg_virtuemart3_ideal' ) {

			$plg_id_virtuemart = "";

			$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='plg_virtuemart3_ideal' AND `folder`='vmpayment' LIMIT 1" );
			$plg_id_virtuemart = $db->loadObject();

			if ( ( @$plg_id_virtuemart->extension_id ) && ( $jversion_sht == 3 ) ) {

				$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'virtuemart3' . DS . 'plg_virtuemart3_ideal';
				$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart3_ideal';

				if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart3_ideal' ) ) {

					JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart3_ideal' );
					JFolder::copy( $copy_Folder, $paste_Folder );
				} else {
					JFolder::copy( $copy_Folder, $paste_Folder );
				}

				$plg_extid_virtuemart = "";
				$enab_virtuemart      = 0;
				$params_virtuemart    = "{}";

				@$plg_extid_virtuemart = $plg_id_virtuemart->extension_id;
				@$enab_virtuemart = $plg_id_virtuemart->enabled;
				@$params_virtuemart = $plg_id_virtuemart->params;

				if ( $plg_extid_virtuemart ) {
					$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='plg_virtuemart3_ideal'" );
					$db->query();
				}

				$select = "SELECT element FROM #__extensions WHERE element='plg_virtuemart3_ideal' ";
				$db->setQuery( $select );
				$result = $db->loadResult();

				if ( $result != "plg_virtuemart3_ideal" ) {

					$virtuemart2 = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('VM Payment - cciDEAL integration', 'plugin', 'plg_virtuemart3_ideal', 'vmpayment', '0', '" . $enab_virtuemart . "', '1', '0', '{\"legacy\":true,\"name\":\"VM Payment - cciDEAL integration\",\"type\":\"plugin\",\"creationDate\":\"REFRESH CACHE\",\"author\":\"Chill Creations\",\"copyright\":\"2008 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"REFRESH CACHE\",\"description\":\"Virtuemart 3.x iDEAL plugin for cciDEAL Platform\",\"group\":\"\"}', '" . $params_virtuemart . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
					$db->setQuery( $virtuemart2 );
					$db->query();
				}

				//After install
				$alr_paymentm = "SELECT extension_id FROM #__extensions WHERE element='plg_virtuemart3_ideal' ";
				$db->setQuery( $alr_paymentm );
				$apaymentm = $db->loadResult();

				$query = "UPDATE #__virtuemart_paymentmethods SET payment_jplugin_id = '" . $apaymentm . "' WHERE payment_element='plg_virtuemart3_ideal'";
				$db->setQuery( $query );
				$db->Query();

				$copy_File1  = JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart3_ideal' . DS . 'en-GB.plg_vmpayment3_iDEAL.ini';
				$paste_File1 = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_vmpayment3_iDEAL.ini';

				$copy_File2  = JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart3_ideal' . DS . 'en-GB.plg_vmpayment3_iDEAL.sys.ini';
				$paste_File2 = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_vmpayment3_iDEAL.sys.ini';

				$copy_File3  = JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart3_ideal' . DS . 'nl-NL.plg_vmpayment3_iDEAL.ini';
				$paste_File3 = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'nl-NL' . DS . 'nl-NL.plg_vmpayment3_iDEAL.ini';

				$copy_File4  = JPATH_ROOT . DS . 'plugins' . DS . 'vmpayment' . DS . 'plg_virtuemart3_ideal' . DS . 'nl-NL.plg_vmpayment3_iDEAL.sys.ini';
				$paste_File4 = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'nl-NL' . DS . 'nl-NL.plg_vmpayment3_iDEAL.sys.ini';


				$paste_folder1 = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'nl-NL';

				if ( ! JFolder::exists( $paste_folder1 ) ) {
					JFolder::create( $paste_folder1 );
				}

				if ( JFile::exists( $copy_File1 ) ) {
					JFile::delete( $paste_File1 );
					JFile::delete( $paste_File2 );
					JFile::delete( $paste_File3 );
					JFile::delete( $paste_File4 );
				}
				JFile::move( $copy_File1, $paste_File1 );
				JFile::move( $copy_File2, $paste_File2 );
				JFile::move( $copy_File3, $paste_File3 );
				JFile::move( $copy_File4, $paste_File4 );

			}
		}

		/* Plugin: redShop */

		$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='rs_payment_ccidealplatform' AND `folder`='redshop_payment' LIMIT 1" );
		$plg_id_redshop = $db->loadObject();

		if ( ( @$plg_id_redshop->extension_id ) && ( $jversion_sht == 2 ) ) {

			$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'redshop' . DS . 'plg_redshop_ideal';
			$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'redshop_payment' . DS . 'rs_payment_ccidealplatform';

			if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'redshop_payment' . DS . 'rs_payment_ccidealplatform' ) ) {

				JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'redshop_payment' . DS . 'rs_payment_ccidealplatform' );
				JFolder::copy( $copy_Folder, $paste_Folder );
			} else {
				JFolder::copy( $copy_Folder, $paste_Folder );
			}

			$plg_extid_redshop = "";
			$enab_redshop      = 0;
			$params_redshop    = "{}";

			@$plg_extid_redshop = $plg_id_redshop->extension_id;
			@$enab_redshop = $plg_id_redshop->enabled;
			@$params_redshop = $plg_id_redshop->params;

			if ( $plg_extid_redshop ) {
				$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='rs_payment_ccidealplatform'" );
				$db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='rs_payment_ccidealplatform' ";
			$db->setQuery( $select );
			$result = $db->loadResult();

			if ( $result != "rs_payment_ccidealplatform" ) {

				$redshop = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('redSHOP - cciDEAL Platform', 'plugin', 'rs_payment_ccidealplatform', 'redshop_payment', '0', '" . $enab_redshop . "', '1', '0', '{\"legacy\":true,\"name\":\"redSHOP - cciDEAL Platform\",\"type\":\"plugin\",\"creationDate\":\"January 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.0.0\",\"description\":\"redSHOP iDEAL plugin for cciDEAL Platform\",\"group\":\"\"}', '" . $params_redshop . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery( $redshop );
				$db->query();
			}

			//language

			$copy_File  = JPATH_ROOT . DS . 'plugins' . DS . 'redshop_payment' . DS . 'rs_payment_ccidealplatform' . DS . 'language' . DS . 'en-GB.plg_redshop_payment_rs_payment_ideal.ini';
			$paste_File = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_redshop_payment_rs_payment_ideal.ini';

			if ( JFile::exists( $copy_File ) ) {

				JFile::delete( $paste_File );
				JFile::move( $copy_File, $paste_File );
			} else {
				JFile::move( $copy_File, $paste_File );
			}

			$copy_File1  = JPATH_ROOT . DS . 'plugins' . DS . 'redshop_payment' . DS . 'rs_payment_ccidealplatform' . DS . 'language' . DS . 'en-GB.plg_redshop_payment_rs_payment_ideal.sys.ini';
			$paste_File1 = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_system_rsfp_ideal.sys.ini';

			if ( JFile::exists( $copy_File1 ) ) {

				JFile::delete( $paste_File1 );
				JFile::move( $copy_File1, $paste_File1 );
			} else {
				JFile::move( $copy_File1, $paste_File1 );
			}

			$del_File = JPATH_ROOT . DS . 'plugins' . DS . 'redshop_payment' . DS . 'rs_payment_ccidealplatform' . DS . 'language';

			if ( JFolder::exists( $del_File ) ) {
				JFolder::delete( $del_File );
			}
		}

		/* Plugin: Simple Caddy */

		$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='scideal' AND `folder`='content' LIMIT 1" );
		$plg_id_simplecaddy = $db->loadObject();

		if ( ( @$plg_id_simplecaddy->extension_id ) && ( @$jversion_sht == 2 ) ) {

			$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'simplecaddy' . DS . 'plg_simplecaddy_ideal';
			$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'content' . DS . 'scideal';

			if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'content' . DS . 'scideal' ) ) {

				JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'content' . DS . 'scideal' );
				JFolder::copy( $copy_Folder, $paste_Folder );
			} else {
				JFolder::copy( $copy_Folder, $paste_Folder );
			}

			$plg_extid_simplecaddy = "";
			$enab_simplecaddy      = 0;
			$params_simplecaddy    = "{}";

			@$plg_extid_simplecaddy = $plg_id_simplecaddy->extension_id;
			@$enab_simplecaddy = $plg_id_simplecaddy->enabled;
			@$params_simplecaddy = $plg_id_simplecaddy->params;

			if ( $plg_extid_simplecaddy ) {
				$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='scideal'" );
				$db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='scideal' ";
			$db->setQuery( $select );
			$result = $db->loadResult();

			if ( $result != "scideal" ) {

				$simplecaddy = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('Content - SimpleCaddy - iDEAL', 'plugin', 'scideal', 'content', '0', '" . $enab_simplecaddy . "', '1', '0', '{\"legacy\":true,\"name\":\"Content - SimpleCaddy - iDEAL\",\"type\":\"plugin\",\"creationDate\":\"January 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.0.0\",\"description\":\"iDEAL processor for SimpleCaddy 2.0\",\"group\":\"\"}', '" . $params_simplecaddy . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery( $simplecaddy );
				$db->query();
			}
		}

		/* Plugin: RSDirectory */

		$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='rsdirectoryideal' AND `folder`='system' LIMIT 1" );
		$plg_id_rsdirectory = $db->loadObject();

		if ( @$plg_id_rsdirectory->extension_id ) {

			$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'rsdirectory' . DS . 'plg_rsdirectoryideal';
			$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsdirectoryideal';

			if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsdirectoryideal' ) ) {

				JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsdirectoryideal' );
				JFolder::copy( $copy_Folder, $paste_Folder );
			} else {
				JFolder::copy( $copy_Folder, $paste_Folder );
			}

			$plg_extid_rsdirectory = "";
			$enab_rsdirectory      = 0;
			$params_rsdirectory    = "{}";

			@$plg_extid_rsdirectory = $plg_id_rsdirectory->extension_id;
			@$enab_rsdirectory = $plg_id_rsdirectory->enabled;
			@$params_rsdirectory = $plg_id_rsdirectory->params;

			if ( $plg_extid_rsdirectory ) {
				$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='rsdirectoryideal'" );
				$db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='rsdirectoryideal' ";
			$db->setQuery( $select );
			$result = $db->loadResult();

			if ( $result != "rsdirectoryideal" ) {

				$rsdirectory = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('RSDirectory iDEAL by Chill Creations', 'plugin', 'rsdirectoryideal', 'system', '0', '" . $enab_rsdirectory . "', '1', '0', '{\"legacy\":true,\"name\":\"RSDirectory iDEAL by Chill Creations\",\"type\":\"plugin\",\"creationDate\":\"May 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"4.1.0\",\"description\":\"RSDirectory! iDEAL Payment Plugin. Make sure you publish the plugin\",\"group\":\"\"}', '" . $params_rsdirectory . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery( $rsdirectory );
				$db->query();
			}

			//language

			$copy_File  = JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsdirectoryideal' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_system_rsdirectoryideal.ini';
			$paste_File = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_system_rsdirectoryideal.ini';

			if ( JFile::exists( $paste_File ) ) {

				JFile::delete( $paste_File );
				JFile::move( $copy_File, $paste_File );
			} else {
				JFile::move( $copy_File, $paste_File );
			}

			$copy_File1  = JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsdirectoryideal' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_system_rsdirectoryideal.sys.ini';
			$paste_File1 = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_system_rsdirectoryideal.sys.ini';

			if ( JFile::exists( $paste_File1 ) ) {

				JFile::delete( $paste_File1 );
				JFile::move( $copy_File1, $paste_File1 );
			} else {
				JFile::move( $copy_File1, $paste_File1 );
			}

			$del_File = JPATH_ROOT . DS . 'plugins' . DS . 'system' . DS . 'rsdirectoryideal' . DS . 'language';

			if ( JFolder::exists( $del_File ) ) {
				JFolder::delete( $del_File );
			}
		}

		/* Plugin: J2Store */

		$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='payment_ccideal' AND `folder`='j2store' LIMIT 1" );
		$plg_id_j2store = $db->loadObject();

		if ( @$plg_id_j2store->extension_id ) {

			$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'j2store' . DS . 'plg_j2store_payment_ccideal';
			$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'j2store' . DS . 'payment_ccideal';

			if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'j2store' . DS . 'payment_ccideal' ) ) {

				JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'j2store' . DS . 'payment_ccideal' );
				JFolder::copy( $copy_Folder, $paste_Folder );
			} else {
				JFolder::copy( $copy_Folder, $paste_Folder );
			}

			$plg_extid_j2store = "";
			$enab_j2store      = 0;
			$params_j2store    = "{}";

			@$plg_extid_j2store = $plg_id_j2store->extension_id;
			@$enab_j2store = $plg_id_j2store->enabled;
			@$params_j2store = $plg_id_j2store->params;

			if ( $plg_extid_j2store ) {
				$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='payment_ccideal'" );
				$db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='payment_ccideal' ";
			$db->setQuery( $select );
			$result = $db->loadResult();

			if ( $result != "payment_ccideal" ) {

				$j2store = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('J2Store - cciDEAL integration', 'plugin', 'payment_ccideal', 'j2store', '0', '" . $enab_j2store . "', '1', '0', '{\"legacy\":true,\"name\":\"J2Store - cciDEAL integration\",\"type\":\"plugin\",\"creationDate\":\"October 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"1.0.0\",\"description\":\"J2Store! iDEAL Payment Plugin. Make sure you publish the plugin\",\"group\":\"\"}', '" . $params_j2store . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery( $j2store );
				$db->query();
			}

			//language

			$copy_File  = JPATH_ROOT . DS . 'plugins' . DS . 'j2store' . DS . 'payment_ccideal' . DS . 'languages' . DS . 'en-GB.plg_j2store_payment_ccideal.ini';
			$paste_File = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_j2store_payment_ccideal.ini';

			if ( JFile::exists( $paste_File ) ) {

				JFile::delete( $paste_File );
				JFile::move( $copy_File, $paste_File );
			} else {
				JFile::move( $copy_File, $paste_File );
			}

			$copy_File1  = JPATH_ROOT . DS . 'plugins' . DS . 'j2store' . DS . 'payment_ccideal' . DS . 'languages' . DS . 'en-GB.plg_j2store_payment_ccideal.sys.ini';
			$paste_File1 = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_j2store_payment_ccideal.sys.ini';

			if ( JFile::exists( $paste_File1 ) ) {

				JFile::delete( $paste_File1 );
				JFile::move( $copy_File1, $paste_File1 );
			} else {
				JFile::move( $copy_File1, $paste_File1 );
			}

			$del_File = JPATH_ROOT . DS . 'plugins' . DS . 'j2store' . DS . 'payment_ccideal' . DS . 'languages';

			if ( JFolder::exists( $del_File ) ) {
				JFolder::delete( $del_File );
			}
		}
		/* Plugin: TechJoomla */

		$db->setQuery( "SELECT extension_id,enabled,params FROM #__extensions WHERE `element`='ccideal' AND `folder`='payment' LIMIT 1" );
		$plg_id_techjoomla = $db->loadObject();

		if ( @$plg_id_j2store->extension_id ) {

			$copy_Folder  = JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'plugins' . DS . 'techjoomla' . DS . 'ccideal';
			$paste_Folder = JPATH_ROOT . DS . 'plugins' . DS . 'payment' . DS . 'ccideal';

			if ( JFolder::exists( JPATH_ROOT . DS . 'plugins' . DS . 'payment' . DS . 'ccideal' ) ) {

				JFolder::delete( JPATH_ROOT . DS . 'plugins' . DS . 'payment' . DS . 'ccideal' );
				JFolder::copy( $copy_Folder, $paste_Folder );
			} else {
				JFolder::copy( $copy_Folder, $paste_Folder );
			}

			$plg_extid_techjoomla = "";
			$enab_techjoomla      = 0;
			$params_techjoomla    = "{}";

			@$plg_extid_techjoomla = $plg_id_techjoomla->extension_id;
			@$enab_techjoomla = $plg_id_techjoomla->enabled;
			@$params_techjoomla = $plg_id_techjoomla->params;

			if ( $plg_extid_techjoomla ) {
				$db->setQuery( "DELETE FROM `#__extensions` WHERE `element`='ccideal'" );
				$db->query();
			}

			$select = "SELECT element FROM #__extensions WHERE element='ccideal' ";
			$db->setQuery( $select );
			$result = $db->loadResult();

			if ( $result != "ccideal" ) {

				$payment = "INSERT INTO `#__extensions` (`name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('TechJoomla extensions - cciDEAL integration', 'plugin', 'ccideal', 'payment', '0', '" . $enab_techjoomla . "', '1', '0', '{\"legacy\":true,\"name\":\"TechJoomla extensions - cciDEAL integration\",\"type\":\"plugin\",\"creationDate\":\"November 2014\",\"author\":\"Chill Creations\",\"copyright\":\"2014 Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"1.0.0\",\"description\":\"TechJoomla extensions - cciDEAL integration Payment Plugin. Make sure you publish the plugin\",\"group\":\"\"}', '" . $params_techjoomla . "', '', '', '0', '0000-00-00 00:00:00', '0', '0')";
				$db->setQuery( $payment );
				$db->query();
			}

			//language

			$copy_File  = JPATH_ROOT . DS . 'plugins' . DS . 'payment' . DS . 'ccideal' . DS . 'en-GB' . DS . 'en-GB.plg_payment_ccideal.ini';
			$paste_File = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'en-GB' . DS . 'en-GB.plg_payment_ccideal.ini';

			if ( JFile::exists( $paste_File ) ) {

				JFile::delete( $paste_File );
				JFile::move( $copy_File, $paste_File );
			} else {
				JFile::move( $copy_File, $paste_File );
			}

			$copy_File1  = JPATH_ROOT . DS . 'plugins' . DS . 'payment' . DS . 'ccideal' . DS . 'nl-NL' . DS . 'nl-NL.plg_payment_ccideal.sys.ini';
			$paste_File1 = JPATH_ROOT . DS . 'administrator' . DS . 'language' . DS . 'nl-NL' . DS . 'nl-NL.plg_payment_ccideal.sys.ini';

			if ( JFile::exists( $paste_File1 ) ) {

				JFile::delete( $paste_File1 );
				JFile::move( $copy_File1, $paste_File1 );
			} else {
				JFile::move( $copy_File1, $paste_File1 );
			}

			$del_File = JPATH_ROOT . DS . 'plugins' . DS . 'payment' . DS . 'ccideal' . DS . 'en-GB';

			if ( JFolder::exists( $del_File ) ) {
				JFolder::delete( $del_File );
			}
			$del_File2 = JPATH_ROOT . DS . 'plugins' . DS . 'payment' . DS . 'ccideal' . DS . 'nl-NL';

			if ( JFolder::exists( $del_File2 ) ) {
				JFolder::delete( $del_File2 );
			}
		}
	}

	function installitems() {
		jimport( 'joomla.filesystem.path' );
		jimport( 'joomla.filesystem.file' );
		jimport( 'joomla.filesystem.folder' );

		if ( JFile::exists( JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'uninstall.ccidealplatform.php' ) ) {
			JFile::delete( JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'uninstall.ccidealplatform.php' );
		}

		$db = JFactory::getDBO();

		// Process iDEAL integration plugins

		$this->integrationplugin();

		// Added by David - 23 March 2015 - cciDEAL 4.4.2
		// Remove old language files in core language folders, because I moved language files into extension itself (Joomla! best practice)

		$this->removeOldLanguageFiles();

		// Install Chill Creations update plugin

		$this->installChillCreationsUpdatePlugin();

		// Added by David - 02 April 2015 - cciDEAL 4.4.4
		// Install Chill Creations Bootstrap

		$this->installChillCreationsBootstrap();

		/* Function for Update Query */

		$check_comp = "SELECT `params` FROM #__extensions WHERE `element` = 'com_ccidealplatform' ";
		$db->setQuery( $check_comp );
		$check_comp_result = $db->loadResult();

		if ( empty( $check_comp_result ) ) {

			$insert_params = "UPDATE #__extensions SET `params`=1 WHERE `element` = 'com_ccidealplatform'";
			$db->setQuery( $insert_params );
			$db->Query();
		} else {
			$check_comp_result = $check_comp_result + 1;
			$insert_params     = "UPDATE #__extensions SET `params`='$check_comp_result' WHERE `element` = 'com_ccidealplatform'";
			$db->setQuery( $insert_params );
			$db->Query();
		}


		//End checking

		// Added by David March 14th 2015 - 4.3.6 - for sudden and unexplained mysql issues during install
		$create_config_table = "CREATE TABLE IF NOT EXISTS `#__ccidealplatform_config` (
						`id` int(11) unsigned NOT NULL auto_increment,
						`IDEAL_Bank` varchar(255) NOT NULL default '',
						`IDEAL_Enable` varchar(255) NOT NULL default '',
						`IDEAL_Description` varchar(255) NOT NULL default '',
						`IDEAL_Mode` varchar(255) NOT NULL default '',
						`IDEAL_MerchantID` varchar(255) NOT NULL default '',
						`IDEAL_PrivatekeyPass` varchar(255) NOT NULL default '',
						`IDEAL_SubID` varchar(255) NOT NULL default '',
						`IDEAL_Privatecert` varchar(255) NOT NULL default '',
						`IDEAL_Privatekey` varchar(255) NOT NULL default '',
						`IDEAL_PartnerID` varchar(255) NOT NULL default '',
						`IDEAL_ABNAMROEASY_ID` varchar(255) NOT NULL default '',
						`IDEAL_Targetpay_ID` varchar(255) NOT NULL default '',
						`iDEAL_ProVersion` VARCHAR( 255 ) NOT NULL,
						`IDEAL_Status_ThankYou` mediumtext NOT NULL,
						`IDEAL_Status_Failed` mediumtext NOT NULL,
						`IDEAL_Status_Cancelled` mediumtext NOT NULL,
						`IDEAL_pay_sucs_value` int(11) unsigned NOT NULL,
						`IDEAL_pay_sucs_articleid` int(11) unsigned NOT NULL,
						`IDEAL_pay_failed_value` int(11) unsigned NOT NULL,
						`IDEAL_pay_fail_articleid` int(11) unsigned NOT NULL,
						`IDEAL_pay_cancel_value` int(11) unsigned NOT NULL,
						`IDEAL_pay_cancel_articleid` int(11) unsigned NOT NULL,
						`ordering` int(11) unsigned NOT NULL,
						PRIMARY KEY (`id`)
						)";

		$db->setQuery( $create_config_table );
		$db->execute();

		$create_conf_table = "CREATE TABLE IF NOT EXISTS `#__ccidealplatform_conf` (
  								`id` int(11) unsigned NOT NULL,
								  `IDEAL_Taal` varchar(255) NOT NULL DEFAULT '',
								  `IDEAL_AuthenticationType` varchar(255) NOT NULL DEFAULT '',
								  `IDEAL_AcquirerTimeout` varchar(255) NOT NULL DEFAULT '',
								  `IDEAL_ExpirationPeriod` varchar(255) NOT NULL DEFAULT '',
								  `IDEAL_LogFile` varchar(255) NOT NULL DEFAULT '',
								  `IDEAL_MerchantReturnURL` varchar(255) NOT NULL DEFAULT '',
								  `IDEAL_Currency` varchar(255) NOT NULL DEFAULT '',
								  `IDEAL_Language` varchar(255) NOT NULL DEFAULT '',
								  `tmp_orderid` varchar(255) NOT NULL DEFAULT '',
								  `ordering` int(11) unsigned NOT NULL
								)";

		$db->setQuery( $create_conf_table );
		$db->execute();

		$create_logs_table = "CREATE TABLE IF NOT EXISTS `#__ccidealplatform_logs` (
								  `id` int(255) NOT NULL,
								  `createDateTimeStamp` varchar(255) NOT NULL,
								  `amount` varchar(255) NOT NULL,
								  `issuerAuthenticationURL` varchar(255) NOT NULL,
								  `transactionID` varchar(255) NOT NULL,
								  `purchaseID` varchar(255) NOT NULL,
								  `payment_status` varchar(255) NOT NULL,
								  `merchantID` varchar(255) NOT NULL,
								  `decription` varchar(255) NOT NULL,
								  `entrance_code` varchar(255) NOT NULL,
								  `IDEAL_Bank` varchar(255) NOT NULL,
								  `IDEAL_Mode` varchar(245) NOT NULL,
								  `token` varchar(255) NOT NULL,
								  `token_code` varchar(255) NOT NULL,
								  `last_tested_date` datetime NOT NULL,
								  `after_12hrs` int(11) NOT NULL,
								  `after_24hrs` int(11) NOT NULL,
								  `ordering` int(11) NOT NULL
								)";

		$db->setQuery( $create_logs_table );
		$db->execute();

		$create_paymentmethod_table = "CREATE TABLE IF NOT EXISTS `#__ccidealplatform_paymentmethod` (
								  `id` int(11) unsigned NOT NULL,
								  `method_value` varchar(255) NOT NULL DEFAULT '',
								  `account_id` varchar(255) NOT NULL DEFAULT '',
								  `method_name` varchar(255) NOT NULL DEFAULT ''
								)";

		$db->setQuery( $create_paymentmethod_table );
		$db->execute();

		$create_payments_table = "CREATE TABLE IF NOT EXISTS `#__ccidealplatform_payments` (
								  `id` int(11) unsigned NOT NULL,
								  `order_id` varchar(255) NOT NULL DEFAULT '',
								  `extension_id` varchar(255) NOT NULL DEFAULT '',
								  `trans_id` varchar(255) NOT NULL DEFAULT '',
								  `user_id` int(11) unsigned NOT NULL,
								  `account` varchar(255) NOT NULL,
								  `payment_total` decimal(15,5) NOT NULL,
								  `payment_date` varchar(255) NOT NULL,
								  `payment_status` varchar(255) NOT NULL DEFAULT '',
								  `extension` varchar(255) NOT NULL DEFAULT '',
								  `article_extra_text` text NOT NULL,
								  `cciDEAL_params` varchar(255) NOT NULL DEFAULT '',
								  `ordering` int(11) unsigned NOT NULL,
								  `pay_method` varchar(255) NOT NULL,
								  `payment_custom` varchar(255) NOT NULL
								)";

		$db->setQuery( $create_payments_table );
		$db->execute();

		// END

		// Added by David March 14th 2015 - 4.3.6
		// Seems like this code is used to drop and update the cciDEAL config table
		// Disable for now, don't trust it as its not accurate for current db structure

		//Check the configuration is already stored or not (Mandatory)
		//		$query="SELECT IDEAL_Bank FROM #__ccidealplatform_config";
		//		$db->setQuery( $query );
		//		$bank_config = $db->loadResult();
		//
		//		$query="SELECT IDEAL_Description  FROM #__ccidealplatform_config";
		//		$db->setQuery( $query );
		//		$IDEAL_Description = $db->loadResult();


		//		if($bank_config=="" && $IDEAL_Description=="" ){
		//
		//			$query = "SELECT COUNT(*) FROM #__ccidealplatform_config";
		//			$db->setQuery( $query );
		//			$total_methods = $db->loadResult();
		//
		//			if($total_methods<=0)
		//			{
		//				//Added by David March 14th 2015 - 4.3.6 -  disabled default value insert, causing issues in some DB's, need to refactor script
		////				$query = "INSERT INTO #__ccidealplatform_config (id,IDEAL_Bank,IDEAL_Enable,IDEAL_Description,IDEAL_Mode,IDEAL_MerchantID,IDEAL_PrivatekeyPass,IDEAL_SubID,IDEAL_Privatecert,IDEAL_Privatekey,IDEAL_PartnerID,IDEAL_ABNAMROEASY_ID,IDEAL_Targetpay_ID,iDEAL_ProVersion,IDEAL_PrivatekeyPass_v3,IDEAL_Privatecert_v3,IDEAL_Privatekey_v3,IDEAL_Status_ThankYou,IDEAL_Status_Failed,IDEAL_Status_Cancelled,IDEAL_pay_sucs_value,IDEAL_pay_sucs_articleid,IDEAL_pay_failed_value) VALUES ('1', 'ING', '0', 'Betaling', 'TEST', '', '', '0','', '', '', '', '','1','', '', '0', '0', '0', '0', '0', '0','0')";
		////				$db->setQuery( $query );
		////				$db->query();
		//
		//			}else{
		//
		//				$query = "SELECT * FROM #__ccidealplatform_config";
		//				$db->setQuery( $query );
		//				$config = $db->loadObject();
		//
		//				$query = " DROP TABLE #__ccidealplatform_config ";
		//				$db->setQuery( $query );
		//
		//				if($db->query()){
		//					$query = "CREATE TABLE IF NOT EXISTS `#__ccidealplatform_config` (
		//						`id` int(11) unsigned NOT NULL auto_increment,
		//						`IDEAL_Bank` varchar(255) NOT NULL default '',
		//						`IDEAL_Enable` varchar(255) NOT NULL default '',
		//						`IDEAL_Description` varchar(255) NOT NULL default '',
		//						`IDEAL_Mode` varchar(255) NOT NULL default '',
		//						`IDEAL_MerchantID` varchar(255) NOT NULL default '',
		//						`IDEAL_PrivatekeyPass` varchar(255) NOT NULL default '',
		//						`IDEAL_SubID` varchar(255) NOT NULL default '',
		//						`IDEAL_Privatecert` varchar(255) NOT NULL default '',
		//						`IDEAL_Privatekey` varchar(255) NOT NULL default '',
		//						`IDEAL_PartnerID` varchar(255) NOT NULL default '',
		//						`IDEAL_ABNAMROEASY_ID` varchar(255) NOT NULL default '',
		//						`IDEAL_Targetpay_ID` varchar(255) NOT NULL default '',
		//						`iDEAL_ProVersion` VARCHAR( 255 ) NOT NULL,
		//						`IDEAL_Status_ThankYou` mediumtext NOT NULL,
		//						`IDEAL_Status_Failed` mediumtext NOT NULL,
		//						`IDEAL_Status_Cancelled` mediumtext NOT NULL,
		//						`IDEAL_pay_sucs_value` int(11) unsigned NOT NULL,
		//						`IDEAL_pay_sucs_articleid` int(11) unsigned NOT NULL,
		//						`IDEAL_pay_failed_value` int(11) unsigned NOT NULL,
		//						`IDEAL_pay_fail_articleid` int(11) unsigned NOT NULL,
		//						`IDEAL_pay_cancel_value` int(11) unsigned NOT NULL,
		//						`IDEAL_pay_cancel_articleid` int(11) unsigned NOT NULL,
		//						`ordering` int(11) unsigned NOT NULL,
		//						PRIMARY KEY (`id`)
		//						)";
		//
		//					$db->setQuery( $query );
		//
		////					if($db->query())
		////					{
		////						$query = "INSERT INTO #__ccidealplatform_config VALUES (
		////						1,
		////						'".$config->IDEAL_Bank."',
		////						'".$config->IDEAL_Description."',
		////						'".$config->IDEAL_Description."',
		////						'".$config->IDEAL_Mode."',
		////						'".$config->IDEAL_MerchantID."',
		////						'".$config->IDEAL_PrivatekeyPass."',
		////						'".$config->IDEAL_SubID."',
		////						'".$config->IDEAL_Privatecert."',
		////						'".$config->IDEAL_Privatekey."',
		////						'".$config->IDEAL_PartnerID."',
		////						'".$config->IDEAL_ABNAMROEASY_ID."',
		////						'".$config->IDEAL_Targetpay_ID."',
		////						'".$config->IDEAL_Status_ThankYou."',
		////						'".$config->IDEAL_Status_Failed."',
		////						'".$config->IDEAL_Status_Cancelled."',
		////						'".$config->IDEAL_pay_sucs_value."',
		////						'".$config->IDEAL_pay_sucs_articleid."',
		////						'".$config->IDEAL_pay_failed_value."',
		////						'".$config->IDEAL_pay_fail_articleid."',
		////						'".$config->IDEAL_pay_cancel_value."',
		////						'".$config->IDEAL_pay_cancel_articleid."',
		////						'".$config->ordering."'
		////						)";
		////						$db->setQuery( $query );
		////						$db->query();
		////					}
		//
		//				}
		//
		//			}	/*else*/
		//
		//		}	//condition End

		if ( JFolder::exists( JPATH_SITE . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'ccideal_ing' ) ) {
			JFolder::delete( JPATH_SITE . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'ccideal_ing' );
		}

		$log_file           = JPATH_SITE . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'ccideal' . DS . 'thinmpi.log';
		$merchant_retrunurl = JURI:: ROOT() . "/index.php?option=com_ccidealplatform&task=checkccidealresult";

		$query = "SELECT COUNT(*) FROM #__ccidealplatform_conf";
		$db->setQuery( $query );
		$total_methods = $db->loadResult();

		if ( $total_methods <= 0 ) {
			$query = "INSERT INTO #__ccidealplatform_conf VALUES (1, 'nl', 'SHA1_RSA', '10', 'PT10M','" . $log_file . "','" . $merchant_retrunurl . "', 'EUR', 'nl','','')";
			$db->setQuery( $query );
			$db->query();
		}

		// Update Function

		@com_ccidealplatformInstallerScript::updateitems();

		if ( JFile::exists( JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'ccidealplatform_j3.xml' ) ) {
			JFile::delete( JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'ccidealplatform_j3.xml' );
		}


		?>

		<style type="text/css">
			#system-message dd.error ul, .alert {
				display: none !important;
			}

			.alert-success {
				display: block !important;
			}
		</style><?php

	}

	function updateitems() {
		jimport( 'joomla.filesystem.path' );
		jimport( 'joomla.filesystem.file' );
		jimport( 'joomla.filesystem.folder' );

		$db = JFactory::getDBO();

		/* Add new field for Upgrade version
		 * Add new Ordering field for Joomla 1.5 + versions */

		// Create Table ccidealplatform logs

		$select_logs_table = "SELECT * FROM #__ccidealplatform_logs";
		$db->setQuery( $select_logs_table );

		if ( ! $db->Query() ) {

			$create_table_logs = "CREATE TABLE IF NOT EXISTS `#__ccidealplatform_logs` (
							  `id` int(255) NOT NULL auto_increment,
							  `createDateTimeStamp` varchar(255) NOT NULL,
							  `amount` varchar(255) NOT NULL,
							  `issuerAuthenticationURL` varchar(255) NOT NULL,
							  `transactionID` varchar(255) NOT NULL,
							  `purchaseID` varchar(255) NOT NULL,
							  `payment_status` varchar(255) NOT NULL,
							  `merchantID` varchar(255) NOT NULL,
							  `decription` varchar(255) NOT NULL,
							  `entrance_code` varchar(255) NOT NULL,
							  `IDEAL_Bank` varchar(255) NOT NULL,
							  `IDEAL_Mode` varchar(245) NOT NULL,
							  `token` varchar(255) NOT NULL,
							  `token_code` varchar(255) NOT NULL,
							  `last_tested_date` datetime NOT NULL,
							  `after_12hrs` int(11) NOT NULL,
							  `after_24hrs` int(11) NOT NULL,
							  `ordering` int(11) NOT NULL,
			) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=87";
			$db->setQuery( $create_table_logs );
			$db->Query();
		}

		//Delete Table ccidealplatform transactions

		$delete_table_trans = "DROP TABLE IF EXISTS `#__ccidealplatform_transactions`";
		$db->setQuery( $delete_table_trans );
		$db->Query();

		//Alter table for ccidealplatform_config

		// Alter the `IDEAL_pay_sucs_value` field

		$select_field1 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field = 'IDEAL_pay_sucs_value'";
		$db->setQuery( $select_field1 );
		$selectfield1 = $db->loadResult();

		if ( ! $selectfield1 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `IDEAL_pay_sucs_value` INT( 11 ) NOT NULL AFTER `IDEAL_Status_Cancelled`";
			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the `IDEAL_pay_sucs_articleid` field

		$select_field2 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field = 'IDEAL_pay_sucs_articleid'";
		$db->setQuery( $select_field2 );
		$selectfield2 = $db->loadResult();

		if ( ! $selectfield2 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `IDEAL_pay_sucs_articleid` INT( 11 ) NOT NULL AFTER `IDEAL_pay_sucs_value`";
			$db->setQuery( $alter_table );
			$db->query();
		}


		// Alter the `IDEAL_pay_failed_value` field

		$select_field3 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field = 'IDEAL_pay_failed_value'";
		$db->setQuery( $select_field3 );
		$selectfield3 = $db->loadResult();

		if ( ! $selectfield3 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `IDEAL_pay_failed_value` INT( 11 ) NOT NULL AFTER `IDEAL_pay_sucs_articleid`";
			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the `IDEAL_pay_fail_articleid` field

		$select_field4 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field = 'IDEAL_pay_fail_articleid'";
		$db->setQuery( $select_field4 );
		$selectfield4 = $db->loadResult();

		if ( ! $selectfield4 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `IDEAL_pay_fail_articleid` INT( 11 ) NOT NULL AFTER `IDEAL_pay_failed_value`";
			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the `IDEAL_pay_cancel_value` field

		$select_field5 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field = 'IDEAL_pay_cancel_value'";
		$db->setQuery( $select_field5 );
		$selectfield5 = $db->loadResult();

		if ( ! $selectfield5 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `IDEAL_pay_cancel_value` INT( 11 ) NOT NULL AFTER `IDEAL_pay_fail_articleid`";
			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the `IDEAL_pay_cancel_articleid` field

		$select_field6 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field = 'IDEAL_pay_cancel_articleid'";
		$db->setQuery( $select_field6 );
		$selectfield6 = $db->loadResult();

		if ( ! $selectfield6 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `IDEAL_pay_cancel_articleid` INT( 11 ) NOT NULL AFTER `IDEAL_pay_cancel_value`";
			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the `ordering` field

		$select_field7 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field = 'ordering'";
		$db->setQuery( $select_field7 );
		$selectfield7 = $db->loadResult();

		if ( ! $selectfield7 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `ordering` INT( 11 ) NOT NULL AFTER `IDEAL_pay_cancel_articleid`";
			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the `IDEAL_ABNAMROEASY_ID` field

		$select_field8 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field= 'IDEAL_ABNAMROEASY_ID'";
		$db->setQuery( $select_field8 );
		$selectfield8 = $db->loadResult();

		if ( ! $selectfield8 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `IDEAL_ABNAMROEASY_ID` VARCHAR( 255 ) NOT NULL AFTER `IDEAL_PartnerID`";
			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the `IDEAL_Targetpay_ID` field

		$select_field9 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field= 'IDEAL_Targetpay_ID'";
		$db->setQuery( $select_field9 );
		$selectfield9 = $db->loadResult();

		if ( ! $selectfield9 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `IDEAL_Targetpay_ID` VARCHAR( 255 ) NOT NULL AFTER `IDEAL_ABNAMROEASY_ID` ";
			$db->setQuery( $alter_table );
			$db->Query();
		}

		// Alter the `iDEAL_ProVersion` field

		$select_field10 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field= 'iDEAL_ProVersion'";
		$db->setQuery( $select_field10 );
		$selectfield10 = $db->loadResult();

		if ( ! $selectfield10 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `iDEAL_ProVersion` VARCHAR( 255 ) NOT NULL AFTER `IDEAL_Targetpay_ID` ";
			$db->setQuery( $alter_table );
			$db->Query();
		}

		// Alter the `IDEAL_PrivatekeyPass_v3` field

		$select_field11 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field= 'IDEAL_PrivatekeyPass_v3'";
		$db->setQuery( $select_field11 );
		$selectfield11 = $db->loadResult();

		if ( ! $selectfield11 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `IDEAL_PrivatekeyPass_v3` VARCHAR( 255 ) NOT NULL AFTER `iDEAL_ProVersion` ";
			$db->setQuery( $alter_table );
			$db->Query();
		}

		// Alter the `IDEAL_Privatecert_v3` field

		$select_field12 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field= 'IDEAL_Privatecert_v3'";
		$db->setQuery( $select_field12 );
		$selectfield12 = $db->loadResult();

		if ( ! $selectfield12 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `IDEAL_Privatecert_v3` VARCHAR( 255 ) NOT NULL AFTER `IDEAL_PrivatekeyPass_v3` ";
			$db->setQuery( $alter_table );
			$db->Query();
		}

		// Alter the `IDEAL_Privatekey_v3` field

		$select_field13 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field= 'IDEAL_Privatekey_v3'";
		$db->setQuery( $select_field13 );
		$selectfield13 = $db->loadResult();

		if ( ! $selectfield13 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `IDEAL_Privatekey_v3` VARCHAR( 255 ) NOT NULL AFTER `IDEAL_Privatecert_v3` ";
			$db->setQuery( $alter_table );
			$db->Query();
		}

		// Alter the `IDEAL_Status_pending` field(since cciDEALPlatform 2.8.5)

		$select_field14 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field = 'IDEAL_Status_pending'";
		$db->setQuery( $select_field14 );
		$selectfield14 = $db->loadResult();

		if ( ! $selectfield14 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `IDEAL_Status_pending` MEDIUMTEXT NOT NULL ";
			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the `IDEAL_pay_pend_value` field(since cciDEALPlatform 2.8.5)

		$select_field15 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field = 'IDEAL_pay_pend_value'";
		$db->setQuery( $select_field15 );
		$selectfield15 = $db->loadResult();

		if ( ! $selectfield15 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `IDEAL_pay_pend_value` INT( 11 ) NOT NULL AFTER `IDEAL_Status_pending`";
			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the `IDEAL_pay_pend_articleid` field(since cciDEALPlatform 2.8.5)

		$select_field16 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field = 'IDEAL_pay_pend_articleid'";
		$db->setQuery( $select_field16 );
		$selectfield16 = $db->loadResult();

		if ( ! $selectfield16 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `IDEAL_pay_pend_articleid` INT( 11 ) NOT NULL AFTER `IDEAL_pay_pend_value`";
			$db->setQuery( $alter_table );
			$db->query();
		}


		// Update the `IDEAL_Targetpay_ID` field

		$IDescription = "select `IDEAL_Description` from #__ccidealplatform_config";
		$db->setQuery( $IDescription );
		$IDEAL_Description = $db->loadResult();

		if ( $IDEAL_Description != "Betaling" ) {
			$betaling = "UPDATE `#__ccidealplatform_config` SET `IDEAL_Description`='Betaling'";
			$db->setQuery( $betaling );
			$db->Query();
		}


		//Table for ccidealplatform_payments

		// Alter the `article_extra_text` field

		$select_field17 = "SHOW COLUMNS FROM #__ccidealplatform_payments WHERE Field= 'article_extra_text'";
		$db->setQuery( $select_field17 );
		$selectfield17 = $db->loadResult();

		if ( ! $selectfield17 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_payments` ADD `article_extra_text` text NOT NULL AFTER extension ";
			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the `extension_id` field

		$select_field18 = "SHOW COLUMNS FROM #__ccidealplatform_payments WHERE Field= 'extension_id'";
		$db->setQuery( $select_field18 );
		$selectfield18 = $db->loadResult();

		if ( ! $selectfield18 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_payments` ADD `extension_id` VARCHAR( 255 ) NOT NULL AFTER `order_id`";
			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the `trans_id` field

		$select_field19 = "SHOW COLUMNS FROM #__ccidealplatform_payments WHERE Field= 'trans_id'";
		$db->setQuery( $select_field19 );
		$selectfield19 = $db->loadResult();

		if ( ! $selectfield19 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_payments` ADD `trans_id` VARCHAR( 255 ) NOT NULL AFTER extension_id ";
			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the `ordering` field

		$select_field20 = "SHOW COLUMNS FROM #__ccidealplatform_payments WHERE Field= 'ordering'";
		$db->setQuery( $select_field20 );
		$selectfield20 = $db->loadResult();

		if ( ! $selectfield20 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_payments` ADD `ordering` text NOT NULL AFTER trans_id ";
			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the `cciDEAL_params` field

		$select_field21 = "SHOW COLUMNS FROM #__ccidealplatform_payments WHERE Field= 'cciDEAL_params'";
		$db->setQuery( $select_field21 );
		$selectfield21 = $db->loadResult();

		if ( ! $selectfield21 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_payments` ADD `cciDEAL_params` VARCHAR( 255 ) NOT NULL AFTER `article_extra_text` ";
			$db->setQuery( $alter_table );
			$db->Query();
		}

		// Alter the `article_extra_text` field

		$alter_payment_date = "ALTER TABLE `#__ccidealplatform_payments` CHANGE `payment_date` `payment_date` VARCHAR( 255 ) NOT NULL";
		$db->setQuery( $alter_payment_date );
		$db->Query();

		//n-checked	

		//Alter the table for #__ccidealplatform_logs

		// Alter the `IDEAL_Bank` field

		$select_field22 = "SHOW COLUMNS FROM #__ccidealplatform_logs WHERE Field= 'IDEAL_Bank'";
		$db->setQuery( $select_field22 );
		$selectfield22 = $db->loadResult();

		if ( ! $selectfield22 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_logs` ADD `IDEAL_Bank` VARCHAR( 255 ) NOT NULL AFTER `entrance_code` ";
			$db->setQuery( $alter_table );
			$db->Query();
		}

		// Alter the `IDEAL_Mode` field

		$select_field23 = "SHOW COLUMNS FROM #__ccidealplatform_logs WHERE Field= 'IDEAL_Mode'";
		$db->setQuery( $select_field23 );
		$selectfield23 = $db->loadResult();

		if ( ! $selectfield23 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_logs` ADD `IDEAL_Mode` VARCHAR( 255 ) NOT NULL AFTER `IDEAL_Bank` ";
			$db->setQuery( $alter_table );
			$db->Query();
		}

		// Alter the `token` field(since cciDEALPlatform 2.7.0)

		$select_field24 = "SHOW COLUMNS FROM #__ccidealplatform_logs WHERE Field= 'token'";
		$db->setQuery( $select_field24 );
		$selectfield24 = $db->loadResult();

		if ( ! $selectfield24 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_logs` ADD `token` VARCHAR( 255 ) NOT NULL AFTER `IDEAL_Mode`";
			$db->setQuery( $alter_table );
			$db->Query();
		}

		// Alter the `IDEAL_mollie_profilekey` field(since cciDEALPlatform 3.0.5)

		$select_field25 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field = 'IDEAL_mollie_profilekeys'";
		$db->setQuery( $select_field25 );
		$selectfield25 = $db->loadResult();

		if ( ! $selectfield25 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` ADD `IDEAL_mollie_profilekeys` VARCHAR( 255 ) NOT NULL AFTER `IDEAL_pay_cancel_articleid`";
			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the `emailnotify`,emailsss,emailsubject,emailbody field(since cciDEALPlatform 3.1.0)

		$select_field26 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field = 'emailnotify'";
		$db->setQuery( $select_field26 );
		$selectfield26 = $db->loadResult();

		if ( ! $selectfield26 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` 
							ADD `emailnotify` TINYINT( 1 ) NOT NULL, 
							ADD `emailsss` VARCHAR( 255 ) NOT NULL, 
							ADD `emailsubject` VARCHAR(255) NOT NULL, 
							ADD `emailbody` TEXT NOT NULL";
			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the `download_id`, minimum_stability field (since cciDEALPlatform 4.0.0)

		$select_field27 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field = 'download_id'";
		$db->setQuery( $select_field27 );
		$selectfield27 = $db->loadResult();

		if ( ! $selectfield27 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` 
							ADD `download_id` VARCHAR( 255 ) NOT NULL, 
							ADD `minimum_stability` VARCHAR( 255 ) NOT NULL";

			$db->setQuery( $alter_table );
			$db->query();
		}

		//11 Mar 14		

		$select_field28 = "SHOW COLUMNS FROM #__ccidealplatform_payments WHERE Field = 'payment_method'";
		$db->setQuery( $select_field28 );
		$selectfield28 = $db->loadResult();

		if ( $selectfield28 ) {
			$alter_account = "ALTER TABLE `#__ccidealplatform_payments` CHANGE `payment_method` `account` VARCHAR( 255 ) NOT NULL";
			$db->setQuery( $alter_account );
			$db->Query();
		}

		//20 Mar 14		

		$select_field29 = "SHOW COLUMNS FROM #__ccidealplatform_payments WHERE Field = 'pay_method'";
		$db->setQuery( $select_field29 );
		$selectfield29 = $db->loadResult();

		if ( ! $selectfield29 ) {
			$alter_accounts = "ALTER TABLE `#__ccidealplatform_payments` ADD `pay_method` VARCHAR( 255 ) NOT NULL";
			$db->setQuery( $alter_accounts );
			$db->Query();
		}

		// Alter the ideal_paymentmethod field(since cciDEALPlatform 4.1.0)

		$select_field30 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field = 'ideal_paymentmethod'";
		$db->setQuery( $select_field30 );
		$selectfield30 = $db->loadResult();

		if ( ! $selectfield30 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` 
							ADD `ideal_paymentmethod` VARCHAR( 255 ) NOT NULL";

			$db->setQuery( $alter_table );
			$db->query();
		}

		// Alter the custom field(since cciDEALPlatform 4.3.0)

		$select_field31 = "SHOW COLUMNS FROM #__ccidealplatform_payments WHERE Field = 'payment_custom'";
		$db->setQuery( $select_field31 );
		$selectfield31 = $db->loadResult();

		if ( ! $selectfield31 ) {
			$alter_accounts = "ALTER TABLE `#__ccidealplatform_payments` ADD `payment_custom` VARCHAR( 255 ) NOT NULL";
			$db->setQuery( $alter_accounts );
			$db->Query();
		}

		$create_table_paymentmethod = "CREATE TABLE IF NOT EXISTS `#__ccidealplatform_paymentmethod` (
							`id` int(11) unsigned NOT NULL auto_increment,
							`method_value` varchar(255) NOT NULL default '',
							`account_id` varchar(255) NOT NULL default '',
							`method_name` varchar(255) NOT NULL default '',
							PRIMARY KEY (`id`)
							);";
		$db->setQuery( $create_table_paymentmethod );
		$db->Query();


		$query = "SELECT COUNT(*) FROM #__ccidealplatform_paymentmethod";
		$db->setQuery( $query );
		$pay_methods = $db->loadResult();

		if ( $pay_methods <= 0 ) {
			$query = "INSERT INTO `#__ccidealplatform_paymentmethod` (`id`, `method_value`, `account_id`, `method_name`) VALUES						
						(1, 'ideal', 'TARGETPAY', 'iDEAL'),
						(2, 'mistercash', 'TARGETPAY', 'Bancontact/MisterCash'),
						(3, 'sofort', 'TARGETPAY', 'SOFORT banking'),
						(4, 'ideal', 'MOLLIE', 'iDEAL'),
						(5, 'mistercash', 'MOLLIE', 'Bancontact/MisterCash'),
						(6, 'creditcard', 'MOLLIE', 'MasterCard & Visa'),
						(7, 'paypal', 'MOLLIE', 'Paypal'),
						(8, 'banktransfer', 'MOLLIE', 'Bank transfer/Overboeking'),
						(9, 'ideal', 'RABOOMNIKASSA', 'iDEAL'),
						(10, 'creditcards', 'RABOOMNIKASSA', 'MasterCard & Visa'),
						(11, 'maestro', 'RABOOMNIKASSA', 'Maestro'),
						(12, 'incasso', 'RABOOMNIKASSA', 'Automatische incasso'),
						(13, 'acceptgiro', 'RABOOMNIKASSA', 'Acceptgiro'),
						(14, 'rembours', 'RABOOMNIKASSA', 'Rembours'),
						(15, 'minitix', 'RABOOMNIKASSA', 'Minitix'),
						(16, 'ideal', 'SISOW', 'iDEAL'),
						(17, 'mistercash', 'SISOW', 'Bancontact/MisterCash'),
						(18, 'sofort', 'SISOW', 'SOFORT banking'),
						(19, 'sofort', 'MOLLIE', 'SOFORT banking'),
						(20, 'bitcoin', 'MOLLIE', 'Bitcoin'),
						(21, 'BCMC', 'RABOOMNIKASSA', 'Bancontact/MisterCash'),
						(22, 'VPAY', 'RABOOMNIKASSA', 'V PAY'),
						(23, 'ALL', 'MOLLIE', 'All payment methods'),
						(24, 'ALL', 'RABOOMNIKASSA', 'All payment methods')";

			$db->setQuery( $query );
			$db->query();

		} else {
			$query = "UPDATE #__ccidealplatform_paymentmethod SET method_name='MasterCard & Visa' WHERE id = 6 OR id = 10";
			$db->setQuery( $query );
			$db->query();

			//updated on cciDEAL 4.3.0

			if ( $pay_methods < 19 ) {
				$query = "INSERT INTO `#__ccidealplatform_paymentmethod` (`id`, `method_value`, `account_id`, `method_name`) VALUES
						(19, 'sofort', 'MOLLIE', 'SOFORT banking'),
						(20, 'bitcoin', 'MOLLIE', 'Bitcoin'),
						(21, 'BCMC', 'RABOOMNIKASSA', 'Bancontact/MisterCash'),
						(22, 'VPAY', 'RABOOMNIKASSA', 'V PAY'),
						(23, 'ALL', 'MOLLIE', 'All payment methods'),
						(24, 'ALL', 'RABOOMNIKASSA', 'All payment methods')";
			} else if ( $pay_methods < 23 ) {
				$query = "INSERT INTO `#__ccidealplatform_paymentmethod` (`id`, `method_value`, `account_id`, `method_name`) VALUES						
						(23, 'ALL', 'MOLLIE', 'All payment methods'),
						(24, 'ALL', 'RABOOMNIKASSA', 'All payment methods')";
			}


			$db->setQuery( $query );
			$db->query();
		}

		$select_field31 = "SHOW COLUMNS FROM #__ccidealplatform_config WHERE Field = 'sisow_merchantid'";
		$db->setQuery( $select_field31 );
		$selectfield31 = $db->loadResult();

		if ( ! $selectfield31 ) {
			$alter_table = "ALTER TABLE `#__ccidealplatform_config` 
							ADD `sisow_merchantid` VARCHAR( 255 ) NOT NULL, 
							ADD `sisow_merchantkey` VARCHAR( 255 ) NOT NULL";

			$db->setQuery( $alter_table );
			$db->query();
		}


		//Change date format	

		$query = $db->getQuery( true );
		$query->select( 'id,payment_date' );
		$query->from( '#__ccidealplatform_payments' );
		$query->where( 'payment_date LIKE "%/%"' );
		$db->setQuery( $query );
		$originaldate = $db->loadObjectlist();

		if ( count( $originaldate ) ) {
			foreach ( $originaldate as $orgdate ) {

				$timestamp = date_create_from_format( 'd/m/Y H:i', $orgdate->payment_date );
				$date_new  = date_format( $timestamp, 'Y-m-d H:i:s' );

				$query = "UPDATE #__ccidealplatform_payments SET payment_date='" . $date_new . "' WHERE id =" . $orgdate->id;
				$db->setQuery( $query );
				$db->query();
			}
		}

		//End Change date format

	}

	function checkFieldExist( $tableName, $fieldName ) {
		$db    = JFactory::getDBO();
		$query = "SHOW COLUMNS FROM " . $tableName . "";
		$db->setQuery( $query );
		$rows = $db->loadObjectList();

		$FieldFlag = true;
		if ( count( $rows ) ) {
			foreach ( $rows as $fields ) {
				if ( trim( $fields->Field ) == trim( $fieldName ) ) {
					$FieldFlag = false;
				}
			}
		}

		return $FieldFlag;
	}

}
?>