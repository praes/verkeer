<?php
/**
 * @package   AdminTools
 * @copyright Copyright (c)2010-2016 Nicholas K. Dionysopoulos
 * @license   GNU General Public License version 3, or later
 * @version   $Id$
 */

// Protect from unauthorized access
defined('_JEXEC') or die;

class AdmintoolsControllerQuickstart extends F0FController
{
	public function __construct($config = array())
	{
		parent::__construct($config);

		$this->modelName = 'Quickstart';
	}

	public function execute($task)
	{
		if (!in_array($task, array('commit', 'cancel')))
		{
			$task = 'browse';
		}

		$this->getThisModel()->setState('task', $task);

		parent::execute($task);
	}

	public function commit()
	{
		// CSRF prevention
		$this->_csrfProtection();

		/** @var AdmintoolsModelQuickstart $model */
		$model = $this->getThisModel();

		$stateVariables = array(
			'adminpw', 'admin_username', 'admin_password', 'emailonadminlogin', 'ipwl', 'detectedip', 'nonewadmins',
			'nofesalogin', 'enablewaf', 'ipworkarounds', 'autoban', 'autoblacklist', 'emailbreaches', 'bbhttpblkey',
			'htmaker'
		);

		foreach ($stateVariables as $k)
		{
			$v = $this->input->get($k, null, 'raw', 2);
			$model->setState($k, $v);
		}

		$model->applyPreferences();

		$message = JText::_('COM_ADMINTOOLS_QUICKSTART_MSG_DONE');
		$this->setRedirect('index.php?option=com_admintools&view=cpanel', $message);
	}

	public function onBeforeBrowse()
	{
		$path = $this->input->get('path', '', 'none', 2);

		/** @var AdmintoolsModelWafconfig $wafConfigModel */
		$wafConfigModel = $this->getModel('wafconfig');
		$this->getThisView()->setModel($wafConfigModel, false, 'wafconfig');

		// We need both privileges to complete the setup
		return $this->checkACL('admintools.maintenance') && $this->checkACL('admintools.security');
	}

}
