<?php
/**
 * @package   AdminTools
 * @copyright Copyright (c)2010-2016 Nicholas K. Dionysopoulos
 * @license   GNU General Public License version 3, or later
 */

// Protect from unauthorized access
defined('_JEXEC') or die();

class AdmintoolsViewSchedule extends F0FViewHtml
{
	public function onAdd($tpl = null)
	{
		// Get the CRON paths
		$this->croninfo  = $this->getModel()->getPaths();
        $this->checkinfo = $this->getModel()->getCheckPaths();
	}
}