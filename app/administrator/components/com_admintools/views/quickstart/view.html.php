<?php
/**
 * @package   AdminTools
 * @copyright Copyright (c)2010-2016 Nicholas K. Dionysopoulos
 * @license   GNU General Public License version 3, or later
 * @version   $Id$
 */

// Protect from unauthorized access
defined('_JEXEC') or die;

class AdmintoolsViewQuickstart extends F0FViewHtml
{
	/** @var string The detect IP of the current visitor */
	public $myIp = '';

	/** @var array  */
	public $wafconfig = null;

	public $isFirstRun = true;

	protected function onBrowse($tpl = null)
	{
		// Get the reported IP
		$myIp = F0FUtilsIp::getIp();

		if (array_key_exists('FOF_REMOTE_ADDR', $_SERVER))
		{
			$myIp = $_SERVER['FOF_REMOTE_ADDR'];
		}
		elseif (function_exists('getenv'))
		{
			if (getenv('FOF_REMOTE_ADDR'))
			{
				$myIp = getenv('FOF_REMOTE_ADDR');
			}
		}

		$this->myIp = $myIp;

		// Get the WAF configuration
		/** @var AdmintoolsModelWafconfig $wafConfigModel */
		$wafConfigModel = $this->getModel('Wafconfig');
		$this->wafconfig = $wafConfigModel->getConfig();

		// Create an admin password if necessary
		if (empty($this->wafconfig['adminpw']))
		{
			$this->wafconfig['adminpw'] = JUserHelper::genRandomPassword(8);
		}

		// Populate email addresses if necessary
		if (empty($this->wafconfig['emailonadminlogin']))
		{
			$this->wafconfig['emailonadminlogin'] = JFactory::getUser()->email;
		}

		if (empty($this->wafconfig['emailbreaches']))
		{
			$this->wafconfig['emailbreaches'] = JFactory::getUser()->email;
		}

		// Get the administrator username/password
		$this->admin_username = '';
		$this->admin_password = '';

		/** @var AdmintoolsModelQuickstart $model */
		$model = $this->getModel();
		$this->isFirstRun = $model->isFirstRun();

		return true;
	}
}