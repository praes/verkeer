<?php
/**
 * @package   AdminTools
 * @copyright Copyright (c)2010-2016 Nicholas K. Dionysopoulos
 * @license   GNU General Public License version 3, or later
 */

// Protect from unauthorized access
defined('_JEXEC') or die;

// Workaround for IDIOT HOSTS. If you are one of these hosts, YOUR "ANTIVIRUS" IS CRAP!
$idiotHostWorkaround = 'h' . str_repeat('t', 2) . 'ps' . ':';
$idiotHostWorkaround .= str_repeat('/', 2) . substr('WTF api IDIOT HOST', 4, 3) . '.';
$idiotHostWorkaround .= strtolower('IP') . substr('signify', -3) .  '.' . substr('organisation', 0, 3);

JHtml::_('formbehavior.chosen');
$this->loadHelper('select');

$jNo = JText::_('JNO');
$jYes = JText::_('JYES');

$js = <<<JS
akeeba.jQuery(document).ready(function(){
    // Enable popovers
	akeeba.jQuery('[rel="popover"]').popover({
		trigger: 'manual',
		animate: false,
		html: true,
		placement: 'bottom',
		template: '<div class="popover akeeba-bootstrap-popover" onmouseover="akeeba.jQuery(this).mouseleave(function() {akeeba.jQuery(this).hide(); });"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'
	})
	.click(function(e) {
		e.preventDefault();
	})
	.mouseenter(function(e) {
		akeeba.jQuery('div.popover').remove();
		akeeba.jQuery(this).popover('show');
	});

	akeeba.jQuery.ajax('{$idiotHostWorkaround}/?format=jsonp&callback=adminToolsIpWorkaroundsDetect');
});

function adminToolsIpWorkaroundsDetect(data)
{
	akeeba.jQuery('#ipWorkaroundsRecommendedSetting').removeClass('label-default');
	akeeba.jQuery('#detectedip').val(data.ip);

	if (data.ip == '{$this->myIp}')
	{
		akeeba.jQuery('#ipWorkaroundsRecommendedSetting').addClass('label label-important');
		akeeba.jQuery('#ipWorkaroundsRecommendedSetting').text('$jNo');

		return;
	}

	akeeba.jQuery('#ipWorkaroundsRecommendedSetting').addClass('label label-success');
	akeeba.jQuery('#ipWorkaroundsRecommendedSetting').text('$jYes');
	akeeba.jQuery('#ipworkarounds').val(1);
}

function admintools_youwanttobreakyoursite()
{
	akeeba.jQuery('#youhavebeenwarnednottodothat').hide();
	akeeba.jQuery('#adminForm').show();
}

JS;
AkeebaStrapper::addJSdef($js);

$formStyle = $this->isFirstRun ? '' : 'display: none';
$warningStyle = $this->isFirstRun ? 'display: none' : '';
?>

<div class="alert alert-error" style="<?php echo $warningStyle; ?>" id="youhavebeenwarnednottodothat">
	<h4>
		<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ALREADYCONFIGURED_HEAD'); ?>
	</h4>
	<p></p>
	<p>
		<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ALREADYCONFIGURED_BODY'); ?>
	</p>
	<p></p>
	<p>
		<a href="index.php?option=com_admintools"
		   class="btn btn-large btn-success">
			<span class="icon icon-home"></span>
			<strong>
				<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ALREADYCONFIGURED_BTN_NO'); ?>
			</strong>
		</a>
		&nbsp;&nbsp;&nbsp;
		<a onclick="admintools_youwanttobreakyoursite(); return false;"
			class="btn btn-mini btn-danger"
		>
			<span class="icon icon-white icon-fire"></span>
			<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ALREADYCONFIGURED_BTN_YES'); ?>
		</a>
	</p>
</div>

<form name="adminForm" id="adminForm" action="index.php" method="post"
	  class="form form-horizontal form-horizontal-wide"
	  style="<?php echo $formStyle; ?>"
>
	<div class="alert alert-info" style="<?php echo $formStyle; ?>">
		<p>
			<?php echo JText::sprintf('COM_ADMINTOOLS_QUICKSTART_INTRO', 'https://www.akeebabackup.com/documentation/admin-tools.html'); ?>
		</p>
	</div>

	<div class="alert alert-error" style="<?php echo $warningStyle; ?>">
		<h1>
			<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ALREADYCONFIGURED_NOSUPPORT_HEAD') ?>
		</h1>
		<p>
			<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ALREADYCONFIGURED_NOSUPPORT_BODY') ?>
		</p>
	</div>

	<input type="hidden" name="option" value="com_admintools"/>
	<input type="hidden" name="view" value="quickstart"/>
	<input type="hidden" name="task" value="commit"/>
	<input type="hidden" name="<?php echo JFactory::getSession()->getFormToken(); ?>" value="1"/>
	<input type="hidden" name="detectedip" id="detectedip" value=""/>

	<h2><?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_HEAD_ADMINSECURITY') ?></h2>

	<div class="control-group">
		<label class="control-label" for="adminpw"
			   rel="popover"
			   data-original-title="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_ADMINPW'); ?>"
			   data-content="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_ADMINPW_TIP'); ?>">
			<?php echo JText::_('ATOOLS_LBL_WAF_OPT_ADMINPW'); ?>
		</label>

		<div class="controls">
			<input type="text" size="20" name="adminpw" value="<?php echo $this->wafconfig['adminpw'] ?>"/>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="admin_username"
			   rel="popover"
			   data-original-title="<?php echo JText::_('ADMINTOOLS_TITLE_ADMINPW'); ?>"
			   data-content="<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ADMINISTRATORPASSORD_INFO'); ?>">
			<?php echo JText::_('ADMINTOOLS_TITLE_ADMINPW'); ?>
		</label>

		<div class="controls">
			<input type="text" name="admin_username" id="admin_username" value="<?php echo $this->admin_username ?>" autocomplete="off"
				placeholder="<?php echo JText::_('ATOOLS_LBL_ADMINPW_USERNAME') ?>"
				/>
			<input type="text" name="admin_password" id="admin_password" value="<?php echo $this->admin_password ?>" autocomplete="off"
				   placeholder="<?php echo JText::_('ATOOLS_LBL_ADMINPW_PASSWORD') ?>"
				/>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="emailonadminlogin"
			   rel="popover"
			   data-original-title="<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ADMINLOGINEMAIL_LBL'); ?>"
			   data-content="<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ADMINLOGINEMAIL_DESC'); ?>">
			<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ADMINLOGINEMAIL_LBL'); ?>
		</label>

		<div class="controls">
			<input type="text" size="20" name="emailonadminlogin"
				   value="<?php echo $this->wafconfig['emailonadminlogin'] ?>" >
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="ipwl"
			   rel="popover"
			   data-original-title="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_IPWL'); ?>"
			   data-content="<?php echo JText::sprintf('COM_ADMINTOOLS_QUICKSTART_WHITELIST_DESC', $this->myIp) ?>"
			>
			<?php echo JText::_('ATOOLS_LBL_WAF_OPT_IPWL'); ?>
		</label>

		<div class="controls">
			<?php echo AdmintoolsHelperSelect::booleanlist('ipwl', array(), $this->wafconfig['ipwl']) ?>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="nonewadmins"
			   rel="popover"
			   data-original-title="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_NONEWADMINS'); ?>"
			   data-content="<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_NONEWADMINS_DESC'); ?>">
			<?php echo JText::_('ATOOLS_LBL_WAF_OPT_NONEWADMINS'); ?>
		</label>

		<div class="controls">
			<?php echo AdmintoolsHelperSelect::booleanlist('nonewadmins', array(), $this->wafconfig['nonewadmins']) ?>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="nofesalogin"
			   rel="popover"
			   data-original-title="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_NOFESALOGIN'); ?>"
			   data-content="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_NOFESALOGIN_TIP'); ?>">
			<?php echo JText::_('ATOOLS_LBL_WAF_OPT_NOFESALOGIN'); ?>
		</label>

		<div class="controls">
			<?php echo AdmintoolsHelperSelect::booleanlist('nofesalogin', array(), $this->wafconfig['nofesalogin']) ?>
		</div>
	</div>

	<h2><?php echo JText::_('ATOOLS_LBL_WAF_OPTGROUP_BASIC') ?></h2>


	<div class="control-group">
		<label class="control-label" for="enablewaf"
			   rel="popover"
			   data-original-title="<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ENABLEWAF_LBL'); ?>"
			   data-content="<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ENABLEWAF_DESC') ?>"
			>
			<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ENABLEWAF_LBL'); ?>
		</label>

		<div class="controls">
			<?php echo AdmintoolsHelperSelect::booleanlist('enablewaf', array(), 1) ?>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="ipworkarounds"
			   rel="popover"
			   data-original-title="<?php echo JText::_('COM_ADMINTOOLS_WAFCONFIG_OPT_IPWORKAROUNDS'); ?>"
			   data-content="<?php echo JText::_('COM_ADMINTOOLS_WAFCONFIG_IPWORKAROUNDS_TIP') ?>"
			>
			<?php echo JText::_('COM_ADMINTOOLS_WAFCONFIG_OPT_IPWORKAROUNDS'); ?>
		</label>

		<div class="controls">
			<?php echo AdmintoolsHelperSelect::booleanlist('ipworkarounds', array(), 1) ?>
			<div class="help-block">
				<?php echo JText::_('COM_ADMINTOOLS_WAFCONFIG_IPWORKAROUNDS_RECOMMENDED') ?>
				<span class="label label-default" id="ipWorkaroundsRecommendedSetting">
					<?php echo JText::_('COM_ADMINTOOLS_WAFCONFIG_IPWORKAROUNDS_RECOMMENDED_WAIT') ?>
				</span>
			</div>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="autoban"
			   rel="popover"
			   data-original-title="<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_AUTOBAN_LBL'); ?>"
			   data-content="<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_AUTOBAN_DESC') ?>"
			>
			<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_AUTOBAN_LBL'); ?>
		</label>

		<div class="controls">
			<?php echo AdmintoolsHelperSelect::booleanlist('autoban', array(), 1) ?>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="autoblacklist"
			   rel="popover"
			   data-original-title="<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_AUTOBLACKLIST_LBL'); ?>"
			   data-content="<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_AUTOBLACKLIST_DESC') ?>"
			>
			<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_AUTOBLACKLIST_LBL'); ?>
		</label>

		<div class="controls">
			<?php echo AdmintoolsHelperSelect::booleanlist('autoblacklist', array(), 1) ?>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="emailbreaches"
			   rel="popover"
			   data-original-title="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_EMAILBREACHES'); ?>"
			   data-content="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_EMAILBREACHES_TIP'); ?>">
			<?php echo JText::_('ATOOLS_LBL_WAF_OPT_EMAILBREACHES'); ?>
		</label>

		<div class="controls">
			<input type="text" size="20" name="emailbreaches" value="<?php echo $this->wafconfig['emailbreaches'] ?>">
		</div>
	</div>

	<h2><?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_HEAD_ADVANCEDSECURITY') ?></h2>

	<div class="control-group">
		<label class="control-label" for="bbhttpblkey"
			   rel="popover"
			   data-original-title="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_BBHTTPBLKEY'); ?>"
			   data-content="<?php echo JText::_('ATOOLS_LBL_WAF_OPT_BBHTTPBLKEY_TIP'); ?>">
			<?php echo JText::_('ATOOLS_LBL_WAF_OPT_BBHTTPBLKEY'); ?>
		</label>

		<div class="controls">
			<input type="text" size="45" name="bbhttpblkey" value="<?php echo $this->wafconfig['bbhttpblkey'] ?>"/>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="htmaker"
			   rel="popover"
			   data-original-title="<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_HTMAKER_LBL'); ?>"
			   data-content="<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_HTMAKER_DESC') ?>"
			>
			<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_HTMAKER_LBL'); ?>
		</label>

		<div class="controls">
			<?php echo AdmintoolsHelperSelect::booleanlist('htmaker', array(), 1) ?>
		</div>
	</div>

	<h2><?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_HEAD_ALMOSTTHERE') ?></h2>

	<p>
		<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ALMOSTTHERE_INTRO'); ?>
	</p>
	<ul>
		<li>
			<a href="http://akee.ba/lockedout">http://akee.ba/lockedout</a>
		</li>
		<li>
			<a href="http://akee.ba/500htaccess">http://akee.ba/500htaccess</a>
		</li>
		<li>
			<a href="http://akee.ba/adminpassword">http://akee.ba/adminpassword</a>
		</li>
		<li>
			<a href="http://akee.ba/lockedout">http://akee.ba/403edituser</a>
		</li>
	</ul>
	<p>
		<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ALMOSTTHERE_OUTRO'); ?>
	</p>

	<div class="alert alert-error" style="<?php echo $warningStyle; ?>">
		<h1>
			<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ALREADYCONFIGURED_NOSUPPORT_HEAD') ?>
		</h1>
		<p>
			<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ALREADYCONFIGURED_NOSUPPORT_BODY') ?>
		</p>
	</div>

	<div class="form-actions" style="<?php echo $formStyle ?>">
		<button type="submit" class="btn btn-primary">
			<?php echo JText::_('JSAVE'); ?>
		</button>
	</div>

	<div class="form-actions" style="<?php echo $warningStyle ?>">
		<button type="submit" class="btn btn-danger">
			<span class="icon icon-white icon-fire"></span>
			<?php echo JText::_('JSAVE'); ?>
		</button>

		<a href="index.php?option=com_admintools"
		   class="btn btn-large btn-success">
			<span class="icon icon-home"></span>
			<strong>
				<?php echo JText::_('COM_ADMINTOOLS_QUICKSTART_ALREADYCONFIGURED_BTN_NO'); ?>
			</strong>
		</a>
	</div>
</form>