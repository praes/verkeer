<?php
/**
 * @package   AdminTools
 * @copyright Copyright (c)2010-2016 Nicholas K. Dionysopoulos
 * @license   GNU General Public License version 3, or later
 */

defined('_JEXEC') or die;

class AdmintoolsViewIpbls extends FOFViewCsv
{
    public function __construct(array $config)
    {
        $config['csv_filename'] = 'ip_blacklist.csv';
        $config['csv_fields']   = array('ip', 'description');

        parent::__construct($config);
    }

    protected function onDisplay($tpl = null)
    {
        $model = $this->getModel();

        // Let's save the current values for pagination
        $limit = $model->getState('limit', 0);
        $limitstart = $model->getState('limitstart', 0);

        // Let's force the model to retrieve the whole list of IPs
        $model->setState('limit', 0);
        $model->setState('limitstart', 0);

        $result = parent::onDisplay($tpl);

        // Let's revert the old values back
        $model->setState('limit', $limit);
        $model->setState('limitstart', $limitstart);

        return $result;
    }
}