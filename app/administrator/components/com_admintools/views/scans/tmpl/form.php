<?php

defined('_JEXEC') or die;

$conf   = JFactory::getConfig();

$editor = JEditor::getInstance($conf->get('editor'));

?>
<form name="adminForm" id="adminForm" action="index.php" class="form-horizontal">
    <input type="hidden" name="option" value="com_admintools" />
    <input type="hidden" name="view" value="scans" />
    <input type="hidden" name="id" value="<?php echo $this->item->id ?>" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="<?php echo JSession::getFormToken()?>" value="1" />

    <div class="control-group">
        <label class="control-label"><?php echo JText::_('COM_ADMINTOOLS_SCANS_EDIT_COMMENT')?></label>
        <div class="controls">
            <textarea class="input-xxlarge" style="height:100px" name="comment"><?php echo $this->item->comment?></textarea>
        </div>
    </div>
</form>
