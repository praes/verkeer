<?php
/**
* @package RSForm!Pro
* @copyright (C) 2007-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<table class="admintable">
<tr>
	<td valign="top" align="left" width="60%">
		<table>
			<tr>
				<td colspan="2"><div class="rsform_error"><?php echo JText::_('RSFP_RSEPRO_DESC'); ?></div></td>
			</tr>
			<tr>
				<td width="80" align="right" nowrap="nowrap" class="key"><?php echo JText::_('RSFP_RSEPRO_USE_INTEGRATION'); ?></td>
				<td><?php echo $lists['published']; ?></td>
			</tr>
		</table>
	</td>
	<td valign="top">
		&nbsp;
	</td>
</tr>
</table>

<br />

<?php 
	foreach ($emails as $type => $email) { 
		$tabs->addTitle('RSFP_RSEPRO_'.strtoupper($type).'_EMAIL','rsepro-'.$type); 
		$content = '<table class="admintable" width="80%" cellpadding="5">';
		
		foreach ($email as $field) {
			$content .= '<tr>';
			$content .= '<td width="200px;">';
			$content .= '<label>'.$field['label'].'</label>';
			$content .= '</td>';
			$content .= '<td>';
			$content .= $field['input'];
			$content .= '</td>';
			$content .= '</tr>';
		}
		
		$content .= '</table>';
		
		$tabs->addContent($content);
	}
	
	$tabs->render();
?>