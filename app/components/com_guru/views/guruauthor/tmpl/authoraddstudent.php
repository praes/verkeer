<?php

/*------------------------------------------------------------------------
# com_guru
# ------------------------------------------------------------------------
# author    iJoomla
# copyright Copyright (C) 2013 ijoomla.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.ijoomla.com
# Technical Support:  Forum - http://www.ijoomla.com/forum/index/
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
JHTML::_('behavior.tooltip');
$db = JFactory::getDBO();
$div_menu = $this->authorGuruMenuBar();
$my_courses = $this->mycoursesth;
$config = $this->config;
$allow_teacher_action = json_decode($config->st_authorpage);//take all the allowed action from administator settings
@$teacher_add_courses = $allow_teacher_action->teacher_add_courses; //allow or not action Add courses
@$teacher_edit_courses = $allow_teacher_action->teacher_edit_courses; //allow or not action Add courses
$Itemid = JRequest::getVar("Itemid", "0");
$doc = JFactory::getDocument();
$doc->setTitle(trim(JText::_('GURU_AUTHOR'))." ".trim(JText::_('GURU_AUTHOR_MY_COURSE')));

// get courses of the current teacher





?>

<script type="text/javascript" language="javascript">
	document.body.className = document.body.className.replace("modal", "");
</script>

<script language="javascript" type="application/javascript">
	function deleteAuthorCourse(){
		if (document.adminForm['boxchecked'].value == 0) {
			alert( "<?php echo JText::_("GURU_MAKE_SELECTION_FIRT");?>" );
		} 
		else{
			if(confirm("<?php echo JText::_("GURU_REMOVE_AUTHOR_COURSES"); ?>")){
				document.adminForm.task.value='removeCourse';
				document.adminForm.submit();
			}
		}	
	}
	
	function newAuthorCourse(){
		document.adminForm.task.value='addCourse';
		document.adminForm.action = '<?php echo JRoute::_("index.php?option=com_guru&view=guruauthor&task=addCourse&layout=addCourse&Itemid=".intval($Itemid)); ?>';
		document.adminForm.submit();	
	}
	
	function duplicateCourse(){
		if (document.adminForm['boxchecked'].value == 0) {
			alert( "<?php echo JText::_("GURU_MAKE_SELECTION_FIRT");?>" );
		} 
		else{
			document.adminForm.task.value='duplicateCourse';
			document.adminForm.submit();
		}	
	}
	
	function unpublishCourse(){
		if (document.adminForm['boxchecked'].value == 0) {
			alert( "<?php echo JText::_("GURU_MAKE_SELECTION_FIRT");?>" );
		} 
		else{
			document.adminForm.task.value='unpublishCourse';
			document.adminForm.submit();
		}	
	}
	
	function publishCourse(){
		if (document.adminForm['boxchecked'].value == 0) {
			alert( "<?php echo JText::_("GURU_MAKE_SELECTION_FIRT");?>" );
		} 
		else{
			document.adminForm.task.value='publishCourse';
			document.adminForm.submit();
		}	
	}
</script>	


<div class="uk-grid">

	<div class="uk-width-1-1">
		
		<div class="pr-mod-spacing-large pr-background-white-transparent">
		
			<h1 class="pr-font-black pr-title-primary-border uk-display-inline-block">Leerling toevoegen</h1>
			
			<form class="uk-form uk-form-horizontal uk-grid" action="<?php echo JRoute::_("index.php?option=com_guru&view=guruauthor&task=savestudent&layout=addCourse&Itemid=".intval($Itemid)); ?>" id="adminForm" method="post" name="adminForm" enctype="multipart/form-data">
				<div class="uk-width-medium-2-3">
					<div class="uk-panel">

						<div class="uk-form-row">
							<label class="uk-form-label" for="first_name">Voornaam</label>
							<div class="uk-form-controls">
								<input type="text" name="first_name" class="uk-width-1-1" required>
							</div>
						</div>	

						<div class="uk-form-row">
							<label class="uk-form-label" for="insertion">Tussenvoegsel</label>
							<div class="uk-form-controls">
								<input type="text" name="insertion" class="uk-width-1-1">
							</div>
						</div>	

						<div class="uk-form-row">
							<label class="uk-form-label" for="last_name">Achternaam</label>
							<div class="uk-form-controls">
								<input type="text" name="last_name" class="uk-width-1-1" required>
							</div>
						</div>	

						<div class="uk-form-row">
							<label class="uk-form-label" for="email">E-mailadres</label>
							<div class="uk-form-controls">
								<input type="text" name="email" class="uk-width-1-1" required>
							</div>
						</div>	

						<div class="uk-form-row">
							<label class="uk-form-label" for="group">Klas / groep</label>
							<div class="uk-form-controls">
								<input type="text" name="group" class="uk-width-1-1" required>
							</div>
						</div>	

						<div class="uk-form-row">
							<label class="uk-form-label" for="course">Aan welke lesbrief wilt u de leerling toevoegen?</label>
							<div class="uk-form-controls">
								<select name="course" class="uk-width-1-1">
									<option value="0">(selecteer een lesbrief)</option>
									<?php foreach($this->courses as $course) : ?>
									<option value="<?php echo $course->id; ?>"  required><?php echo $course->name; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>

						<div class="uk-form-row">
							<div class="uk-form-controls">
								<button type="submit" class="uk-button uk-button-success">Leerling toevoegen</button>
								<a href="<?php echo JRoute::_("index.php?option=com_guru&view=guruauthor&layout=mystudents"); ?>" class="uk-button uk-button-link">Annuleren</a>
							</div>
						</div>
						
					</div>
				</div>
				
				<div class="uk-width-medium-1-3">
				</div>
               
				<input type="hidden" name="option" value="com_guru" />
				<input type="hidden" name="controller" value="guruAuthor" />
<!-- 				<input type="hidden" name="view" value="guruauthor" /> -->
				<input type="hidden" name="task" value="savestudent" />
				<input type="hidden" name="qid" value="<?php echo JRequest::getVar("qid", ""); ?>" />
				<input type="hidden" name="cid" value="<?php echo JRequest::getVar("cid", ""); ?>" />
				
			</form>
				
		</div>
		
	</div>
	
</div>





