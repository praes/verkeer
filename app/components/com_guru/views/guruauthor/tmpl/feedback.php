

<?php $pid = JRequest::getVar('pid'); $programs = $this->studentPrograms();?>

<script type="text/javascript" language="javascript">
	document.body.className = document.body.className.replace("modal", "");
	
	

	
	
	function saveEssayScore(key, pid){
		
		var user_id = document.getElementById("user-"+key).value;
		var grade = document.getElementById("grade-"+key).value;
		var teacher_answer = document.getElementById("feedback-"+key).value;
		var question_id = document.getElementById("question-"+key).value;
		var quiz_id = document.getElementById("quiz-"+key).value;

		// "<?php JRoute::_('index.php?option=com_guru&task=guruAuthor.saveFeedback');?>",
		var req = new Request.HTML({ 
			method: 'post',
			url: "<?php echo JURI::base();?>index.php?option=com_guru&controller=guruAuthor&task=saveFeedback&tmpl=component&format=raw",
			data: { 'pid' : pid, 'user_id' : user_id, 'grade' : grade, 'teacher_answer' : teacher_answer, 'question_id' : question_id, 'quiz_id' : quiz_id},
			onSuccess: function(response, responseElements, responseHTML){
				if(responseHTML == "1"){
					
				}
			}
		}).send(); 
	}

	//clicking "a" sends a variable to the url, so that we can set an active "li" after saving feedback.
	
	function addUrlHash(id){
		
		//get id from anchor tag
		
		var urlHash = id;
		
		//place id into url
		window.location.hash = urlHash;
		
		
		
	}
	
	UIkit.on('beforeready.uk.dom', function() {
	  var hash = document.location.hash;
	  
	  
	  
	  if (hash) {
		  
		  
		UIkit.$(hash).addClass('uk-active');
	  }
	});
	
	
</script>



<div class="pr-mod-spacing-large pr-background-white-transparent">

	<?php foreach($programs as $program) : ?>

		<h1 class="pr-font-black pr-title-primary-border uk-display-inline-block"><?php echo $program->name; ?></h1>
	
		<?php break; ?>
	
	<?php endforeach; ?>
	
	<ul class="pr-feedback-list" data-uk-switcher="{connect:'#my-results'}">
	
		<?php foreach($this->studentsWithAnswersGroup($pid) as $studentGroup) : ?>  
			
				<?php $userGrouped = $this->getStudentName($studentGroup->user_id); ?>
				
				<?php if($userGrouped != null) : ?> 
					
					<li id="<?php echo $userGrouped->id; ?>" onclick="addUrlHash(id)" class="uk-display-inline-block uk-margin-bottom">
					
						<a id="" onclick="" class="uk-badge pr-badge uk-display-inline-block uk-margin-right pr-background-primary pr-font-white" href="#">				
						
							<?php echo $userGrouped->firstname; ?> <?php echo $userGrouped->insertion; ?> <?php echo $userGrouped->lastname; ?>
							
						</a>
						
					</li>
				<?php endif; ?>
		<?php endforeach; ?>
		
	</ul>
	
	<ul id="my-results" class="uk-switcher">
	
		<?php foreach($this->studentsWithAnswersGroup($pid) as $studentGroup) : ?> 
		
			<?php $userGrouped = $this->getStudentName($studentGroup->user_id); ?>
			
			<?php //var_dump($studentGroup); ?>
			<?php //$userGrouped = $this->getStudentName($studentGroup->user_id); ?>
			
			
			<?php if($userGrouped != null) : ?> 
	
			<li>
			
				<table class="uk-table uk-table-striped pr-feedback-table">
	
					<thead>
					
						<tr class="pr-background-primary pr-font-white">
						
							<th>Leerling</th>
							
							<th>Opdracht</th>
							
							<th>Antwoord</th>
							
							<th>Feedback</th>
							
							<th> </th>
							
						</tr>
						
					</thead>
				
					<tbody>
			
						<?php foreach($this->studentsWithAnswers($pid) as $key => $student) : ?>
						
							<?php $user = $this->getStudentName($student->user_id); ?>
						
						
							<?php //var_dump( $user) ;?>
						
							<?php if($student->user_id == $studentGroup->user_id ) : ?>
						
							<?php //if(isset($student) and $student->user_id == $userGrouped->id ) : ?>
						
								<form class="uk-form" action="" method="post" name="feedbackForm">
								
									<tr>
									
										<td>
											
											<?php echo ($user) ? $user->firstname . " " . $user->insertion . " " . $user->lastname : $student->user_id; ?>
											
										</td>
														
										<td><?php echo $this->getQuizById($student->quiz_id); ?></td>
										
										<td><?php echo $student->answers_given; ?></td>
										
										<td>
											
											<?php $feedback = $this->getFeedback($student->user_id, $student->question_id); ?>
											
											<textarea id="feedback-<?php echo intval($key); ?>" value="feedback" placeholder="Geef de leerling feedback..." class="feedback pr-feedback"><?php echo ($feedback) ? $feedback->feedback_quiz_results : ""; ?></textarea>
											
											<select id="grade-<?php echo intval($key); ?>" >

												<option value="0"> <?php echo JText::_("GURU_SELECT_GRADE"); ?> </option>
												<?php
													for($i=1; $i<=10; $i++){
														$selected = '';
														 
														if(intval($feedback->grade) == $i){
															$selected = 'selected="selected"';
														}
													
														echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
													}
												?>
												
											 </select>
											 
										</td>
										
										<td><button class="uk-button uk-button uk-button-success" type="submit" onclick="javascript:saveEssayScore(<?php echo $key . "," . $pid; ?>);">Opslaan</button><td>
												
												
									</tr>
									
									<input type="hidden" id="user-<?php echo $key; ?>" value="<?php echo $student->user_id; ?>" />
									<input type="hidden" id="question-<?php echo $key; ?>" value="<?php echo $student->question_id; ?>" />
									<input type="hidden" id="quiz-<?php echo $key; ?>" value="<?php echo $student->quiz_id; ?>" />
									
								</form>
								
							<?php endif; ?>
						
						<?php endforeach; ?>
				
					</tbody>
					
				</table>
				
				
			<?php endif; ?>
				
			</li>
		
		<?php endforeach; ?>
					
	</ul>

</div>

<script>



	//if hash means dat er iemand al een record heeft opgeslagen.
	/*if(window.location.hash){
	
		//hash van een student is bijvoorbeeld #659. De "li" van een switcher heeft ook deze id.
	
		var hash = window.location.hash;
	
	
		//split hash om duplicate id te voorkomen
		var hashSplit = hash.split("#");
		
		
		//add #student-prefix voor het id
		var newId = "student-" + hashSplit[1];
		
		
		console.log(newId);


		
		//try to add class van uk-active
		$("#" + newId).addClass("uk-active");
					
	}*/

</script>
