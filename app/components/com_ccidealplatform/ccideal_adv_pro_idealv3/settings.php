<?php

/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

/*
 *
* load the config file and save the data into an array
* @return array (with configfile data)
*
*/

defined('_JEXEC') or die('Restricted access');

	$db = JFactory::getDBO();
	$query = "SELECT * FROM #__ccidealplatform_config LIMIT 0,1";
	$db->setQuery($query);
	$config = $db->loadObject();

	//set the Bank
	if(strtolower($config->IDEAL_Bank)==strtolower("ING")){
		$configBank = 'ING Bank';
		$IDEAL_PrivatekeyPass =  $config->IDEAL_PrivatekeyPass_v3;
		$IDEAL_Privatekey	  =  $config->IDEAL_Privatekey_v3;
		$IDEAL_Privatecert 	  =  $config->IDEAL_Privatecert_v3;
		$certificatesPath	  =  'ing_certificates';
	}else if(strtolower($config->IDEAL_Bank)==strtolower("RABOPROF")){
		$configBank = 'Rabobank';
		$IDEAL_PrivatekeyPass =  $config->IDEAL_PrivatekeyPass_v3;
		$IDEAL_Privatekey	  =  $config->IDEAL_Privatekey_v3;
		$IDEAL_Privatecert 	  =  $config->IDEAL_Privatecert_v3;
		$certificatesPath	  =  'rabo_certificates';
	}

	if(strtolower($config->IDEAL_Mode)==strtolower("TEST")){
		$iDEALPayment=true;
	}else{
		$iDEALPayment=false;
	}

	$aSettings = array();

	$aSettings['ACQUIRER'] = $configBank; // Use: 'ABN Amro', 'ING Bank' or 'Rabobank'
	$aSettings['MERCHANT_ID'] = $config->IDEAL_MerchantID; // Your merchant ID
	$aSettings['SUB_ID'] = $config->IDEAL_SubID;
	$aSettings['TEST_MODE'] = $iDEALPayment; // true=TEST dashboard, false=LIVE dashboard

	$aSettings['CERTIFICATE_PATH'] = dirname(__FILE__) . '/'.$certificatesPath.'/'; // Folder to store keys & certificates
	$aSettings['TEMP_PATH'] = dirname(__FILE__) . '/cache/'; // Folder to store cached issuer list, should be writable (CHMOD 0777)

	$aSettings['PRIVATE_KEY_PASS'] =  $IDEAL_PrivatekeyPass; // Password used to generate private key file
	$aSettings['PRIVATE_KEY_FILE'] = $IDEAL_Privatekey ; // Private key file name
	$aSettings['PRIVATE_CERTIFICATE_FILE'] = $IDEAL_Privatecert; // Private certificate file name


?>