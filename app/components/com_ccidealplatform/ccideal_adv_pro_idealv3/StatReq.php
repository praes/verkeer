<?php

/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.  
**/
 
defined('_JEXEC') or die('Restricted access');

$document = JFactory :: getDocument();
$document->addStyleSheet('components/com_ccidealplatform/assets/css/style.css');

// Set default timezone for DATE/TIME functions
if(function_exists('date_default_timezone_set'))
{
	date_default_timezone_set('Europe/Amsterdam');
}

// Include settings & library
require_once(dirname(__FILE__) . '/settings.php');
require_once(dirname(__FILE__) . '/idealprofessional.cls.php');

$app = JFactory::getApplication('site');
jimport('joomla.application.component.model'); 

require_once(JPATH_BASE . DS . 'components' . DS . 'com_ccidealplatform' . DS . 'models'. DS .'ccideal.php');

$component = JRequest :: getVar('com');
$amount = JRequest :: getVar('amt');
$custom = '';
$getMyItemID = '';

if(empty($amount)){
	$amount = JRequest :: getVar('grandtotal');
}

$order_number 	= JRequest::getVar('order_id');

$app =  JFactory::getApplication();
	
$jversion = new JVersion();
$db = JFactory::getDBO();

// Find TransactieID and EntranceCode in $_GET
$sTransactionId = $_GET['trxid'];

if($sTransactionId){	
	$trans_query = "UPDATE #__ccidealplatform_payments SET `trans_id`='$sTransactionId' WHERE `order_id` ='$order_number' ";
	$db->setQuery($trans_query);
	$db->Query();
}

// Validate TransactieID/EntranceCode-combination in database
// TransactieID/EntranceCode-combination is valid
if(true){

	// Setup status request
	$oStatusRequest = new StatusRequest();
	$oStatusRequest->setSecurePath($aSettings['CERTIFICATE_PATH']);
	$oStatusRequest->setCachePath($aSettings['TEMP_PATH']);
	$oStatusRequest->setPrivateKey($aSettings['PRIVATE_KEY_PASS'], $aSettings['PRIVATE_KEY_FILE'], $aSettings['PRIVATE_CERTIFICATE_FILE']);
	$oStatusRequest->setMerchant($aSettings['MERCHANT_ID'], $aSettings['SUB_ID']);
	$oStatusRequest->setAcquirer($aSettings['ACQUIRER'], $aSettings['TEST_MODE']);

	// Set TransactionID you want to verify
	$oStatusRequest->setTransactionId($sTransactionId);

	// Request transaction status TransactionID (SUCCESS, CANCELLED, FAILURE, OPEN, EXPIRED)
	$sStatus = $oStatusRequest->doRequest();

	// Any errors in request?
	if($oStatusRequest->hasErrors())
	{
		// Je kunt $oStatusRequest->getErrors() gebruiken om de lijst met opgetreden fouten op te vragen.
		$getErrorReport 	= $oStatusRequest->getErrors();			 
		 
		if($jversion == 3){ ?>
			
			<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button><?php			
		
		}else{
			echo '<div class="red_message_box">';
			echo '<img src="'.JURI::root().'/components/com_ccidealplatform/assets/images/cross.png" width="16" height="16" align="top" />';
		}			 
			foreach($getErrorReport as $showError){
				echo "<span style='padding:0px 0px 0px 7px;'>".$showError['desc']."</span>";
				echo "<br/>";
				echo JTEXT::_('CCIDEALPLATFORM_ERROCODE').$showError['code'];
				$errorCode = 1;
			}
		echo '</div>';

	}
	else{
			// Report status on screen
		if($sStatus=="SUCCESS"){
			
			$db = JFactory::getDBO();
		
			//Same order status can be break using this code
			
			$query = "SELECT order_id FROM #__ccidealplatform_payments WHERE payment_status='paid' AND `trans_id`='$sTransactionId'";
			$db->setQuery($query);
			$status_check = $db->loadResult();
			
			if($status_check){
				return false;
			}

			$pay_status = "Success";
			$paid_status = "1";		
			
			$query = "UPDATE #__ccidealplatform_payments SET payment_status = 'paid' WHERE order_id = '" . $order_number . "'";
			$db->setQuery($query);
			$db->Query();

			$queryLog = "UPDATE #__ccidealplatform_logs SET payment_status = 'paid' WHERE purchaseID = '" . $order_number . "'";
			$db->setQuery($queryLog);
			$db->Query();

		}
		elseif($sStatus=="CANCELLED" OR $sStatus=="EXPIRED" OR $sStatus=="OPEN" OR $sStatus=="FAILURE"){
			
			$pay_status = "Cancelled";			
			$paid_status = "0";
			
			$query = "UPDATE #__ccidealplatform_payments SET payment_status = 'cancelled' WHERE order_id = '" . $order_number . "'";
			$db->setQuery($query);
			$db->Query();			

			$queryLog = "UPDATE #__ccidealplatform_logs SET payment_status = 'cancelled' WHERE purchaseID = '" . $order_number . "'";
			$db->setQuery($queryLog);
			$db->Query();

		}
		
		$trans = "UPDATE #__ccidealplatform_payments SET `trans_id`='$sTransactionId' WHERE `order_id` ='$order_number' ";
		$db->setQuery($trans);
		$db->Query();
		
		$query = "SELECT payment_status FROM #__ccidealplatform_payments WHERE order_id='" . $order_number . "'";
		$db->setQuery($query);
		$result = $db->loadResult();
		
		//Support for TEST FORM
		if ($component == 'testform1' || $component == 'testform2' || $component == 'testform3' || $component == 'testform4' || $component == 'testform5' || $component == 'testform6' || $component == 'testform7' || $component == 'testpayment') {						
			$statuslayout = ccIdealplatformController::statuslayout($result);
			
			return $statuslayout;
		}
		
		//Support for Content_plugin
		if ($component == 'Content_plugin') {			
			$statuslayout = ccIdealplatformController::statuslayout($result);
			
			return $statuslayout;
		}
		
		//3rd party integration
		
		//Support for Akeeba subscriptions
		if ($component == 'akeebasubs') {
			ccIdealplatformModelccideal::akeeba_notify($order_number,$pay_status,$sTransactionId);	
			ccIdealplatformModelccideal::akeeba_return($order_number,$result);
		}
		
		//Support for hikashop
		if ($component == 'hikashop') {		
			ccIdealplatformModelccideal::hikashop_notify($order_number,$pay_status,$paid_status);
			ccIdealplatformModelccideal::hikashop_return($order_number,$result);
		}
		
		//Support for rsformpro
		if ($component == 'rsformpro') {		
			ccIdealplatformModelccideal::rsformpro_notify($order_number,$paid_status);
			ccIdealplatformModelccideal::rsformpro_return($order_number,$result);
		}
		
		//Support for RSEvents Pro
		if($component=="rsepro"){		
			ccIdealplatformModelccideal::rsepro_notify($order_number,$paid_status);
			ccIdealplatformModelccideal::rsepro_return($order_number,$result);
		}
		
		//Support for ticketmaster	
		if($component=="ticketmaster"){	
			ccIdealplatformModelccideal::ticketmaster_notify($order_number,$pay_status,$sTransactionId);	
			ccIdealplatformModelccideal::ticketmaster_return($order_number,$result);
		}
		
		//Support for rsmembership
		if ($component == 'rsmembership') {
			ccIdealplatformModelccideal::rsmembership_notify($order_number,$paid_status,$sTransactionId);
			ccIdealplatformModelccideal::rsmembership_return($order_number,$result);	
		}
		
		//Support for JoomISP
		if ($component == 'joomisp') {		
			ccIdealplatformModelccideal::joomisp_notify($order_number,$paid_status);
			ccIdealplatformModelccideal::joomisp_return($order_number,$result);
		}
		
		//Support for Virtuemart
		if ($component == 'virtuemart') {		
			ccIdealplatformModelccideal::virtuemart_notify($order_number,$paid_status,$sTransactionId);
			ccIdealplatformModelccideal::virtuemart_return($order_number,$result);
		}
		
		//Support for redshop
		if ($component == 'redshop') {		
			ccIdealplatformModelccideal::redshop_notify($order_number,$paid_status);
			ccIdealplatformModelccideal::redshop_return($order_number,$result);
		}
		
		//Support for SobiPro
		if($component=="sobipro"){
			ccIdealplatformModelccideal::sobipro_notify($order_number,$paid_status);
			ccIdealplatformModelccideal::sobipro_return($order_number,$result);
		}

		//Support for caddy
		if ($component == 'caddy') {			
			ccIdealplatformModelccideal::caddy_notify($order_number,$paid_status);
			ccIdealplatformModelccideal::caddy_return($order_number,$result);
		}
		
		//Support for ccInvoices
		if ($component == 'ccinvoices') {			
			ccIdealplatformModelccideal::ccinvoices_notify($order_number,$paid_status,$sTransactionId);
			ccIdealplatformModelccideal::ccinvoices_return($order_number,$result);
		}
		
		//Support for RSDirectory
		if ($component == 'rsdirectory') {			
			ccIdealplatformModelccideal::rsdirectory_notify($order_number,$paid_status,$sTransactionId);
			ccIdealplatformModelccideal::rsdirectory_return($order_number,$result);			
		}
		
		//Support for J2store
		if ($component == 'j2store') {			
			ccIdealplatformModelccideal::j2store_notify($order_number,$paid_status,$sTransactionId);
			ccIdealplatformModelccideal::j2store_return($order_number,$result);			
		}
		
		//Support for techjoomla
		if ($component == 'techjoomla') {			
			ccIdealplatformModelccideal::techjoomla_notify($order_number,$paid_status,$sTransactionId);
			ccIdealplatformModelccideal::techjoomla_return($order_number,$result);			
		}
	}
}
?>