<?php

/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

defined('_JEXEC') or die('Restricted access');

	// Set default timezone for DATE/TIME functions
	if(function_exists('date_default_timezone_set'))
	{
		date_default_timezone_set('Europe/Amsterdam');
	}

	// Include settings & library
	require_once(dirname(__FILE__) . '/settings.php');
	require_once(dirname(__FILE__) . '/idealprofessional.cls.php');

	// Setup transaction request
	$oTransactionRequest = new TransactionRequest();
	$oTransactionRequest->setSecurePath($aSettings['CERTIFICATE_PATH']);
	$oTransactionRequest->setCachePath($aSettings['TEMP_PATH']);
	$oTransactionRequest->setPrivateKey($aSettings['PRIVATE_KEY_PASS'], $aSettings['PRIVATE_KEY_FILE'], $aSettings['PRIVATE_CERTIFICATE_FILE']);
	$oTransactionRequest->setMerchant($aSettings['MERCHANT_ID'], $aSettings['SUB_ID']);
	$oTransactionRequest->setAcquirer($aSettings['ACQUIRER'], $aSettings['TEST_MODE']);

	$db = JFactory::getDBO();

	$query = "SELECT * FROM #__ccidealplatform_config";
	$db->setQuery($query);
	$ideal_config = $db->loadObject();

	$order_id  = JRequest::getVar('ordernumber'); // Entered order ID
	//For content description
	$chk_extn="SELECT * FROM #__ccidealplatform_payments WHERE `order_id` = '$order_id'";
	$db->setQuery($chk_extn);
	$content_desc=$db->loadObject();
	
	//Payment total security checking for sisow payment	
	$payment_total = $content_desc->payment_total;

	if($amount != $payment_total)
	{
		$amount = $payment_total;
	}

	$ideal_desc_orderid = trim($ideal_config->IDEAL_Description.": ".$content_desc->extension_id);

	// Find order information (this demo uses POST data)
	$sOrderId = JRequest::getVar('ordernumber'); // Entered order ID
	$sOrderDescription = substr($ideal_desc_orderid, 0, 32); // Entered order description
	$fOrderAmount = round(floatval(str_replace(',', '.', $amount)), 2); // Entered order amount
	$sIssuerId = JRequest::getVar('issuerID'); // Selected issuer

	//Extension name
	  $ExtnName 			= $content_desc->extension;
	  $ItemID				=  ccPaymentItemID($ExtnName);

	// Generate random EntranceCode (upto 40 characters)
	$sEntranceCode = sha1($sOrderId . '_' . date('YmdHis'));


	// Add order settings
	$oTransactionRequest->setOrderId($sOrderId);
	$oTransactionRequest->setOrderDescription($sOrderDescription);
	$oTransactionRequest->setOrderAmount($fOrderAmount);

	$oTransactionRequest->setIssuerId($sIssuerId);
	$oTransactionRequest->setEntranceCode($sEntranceCode);


	// Auto detect return URL
	$merchant_retrunurl = "index.php?option=com_ccidealplatform&task=checkccidealresult&com=$ExtnName&amt=$fOrderAmount&order_id=$sOrderId&ItemID=$ItemID";
	$sCurrentUrl = strtolower(substr($_SERVER['SERVER_PROTOCOL'], 0, strpos($_SERVER['SERVER_PROTOCOL'], '/')) . '://' . $_SERVER['SERVER_NAME'] . '/') . substr($_SERVER['SCRIPT_NAME'], 1);
	$sReturnUrl = substr($sCurrentUrl, 0, strrpos($sCurrentUrl, '/') + 1) . $merchant_retrunurl;
	//$sReturnUrl = $merchant_retrunurl;
	$oTransactionRequest->setReturnUrl($sReturnUrl);


	// Request a transaction ID from the iDEAL server.
	$sTransactionId = $oTransactionRequest->doRequest();



	// Any errors in request?
	if($oTransactionRequest->hasErrors())
	{
		// Je kunt $oStatusRequest->getErrors() gebruiken om de lijst met opgetreden fouten op te vragen.
		$getErrorReport 	= $oTransactionRequest->getErrors();
		 echo '<div class="red_message_box">';
		 echo '<img src="'.JURI::root().'/components/com_ccidealplatform/assets/images/cross.png" width="16" height="16" align="top" />';
			foreach($getErrorReport as $showError){
				echo "<span style='padding:0px 0px 0px 7px;'>".$showError['desc']."</span>";
				echo "<br/>";
				echo JTEXT::_('CCIDEALPLATFORM_ERROCODE').$showError['code'];
				$errorCode = 1;
			}
		echo '</div>';
		//print_r($oTransactionRequest->getErrors());

	}
	else
	{
		// Save order, TransactionID and EntranceCode in database.
		// ...


		// echo '<a href="' . htmlentities($oTransactionRequest->getTransactionUrl()) . '">' . htmlentities($oTransactionRequest->getTransactionUrl()) . '</a>';
		// exit;


		// Start tranaction (user is redirected to iDEAL server, make sure no output was send or header() will fail).
		$oTransactionRequest->doTransaction();
	}


function  ccPaymentItemID($component){

		if($component=='sobi'){
			$cmpElement='com_sobi';
		}
		if($component=='sobipro'){
			$cmpElement='com_sobipro';
		}
		if($component=='hikashop'){
			$cmpElement='com_hikashop';
		}
		if($component=='rokquickcart'){
			$cmpElement='com_rokquickcart';
		}
		if($component=='rsformpro'){
			$cmpElement='com_rsform';
		}
		if($component=='rsevents'){
			$cmpElement='com_rsevents';
		}
		if($component=='rsmembership'){
			$cmpElement='com_rsmembership';
		}
		if($component=='redshop'){
			$cmpElement='com_redshop';
		}
		if($component=='akeebasubs'){
			$cmpElement='com_akeebasubs';
		}
		if($component=='joomisp'){
			$cmpElement='com_joomisp';
		}
		if($component=='rsepro'){
			$cmpElement='com_rseventspro';
		}
		if($component=='virtuemart'){
			$cmpElement='com_virtuemart';
		}
		//Tick Mast
		if($component=='ticketmaster'){
			$cmpElement='com_ticketmaster';
		}

		$db = &JFactory::getDBO();
		if(!empty($cmpElement)){
			$s="select `extension_id` from #__extensions where `element`='$cmpElement'";
			$db->setQuery($s); $extnID=  $db->loadResult();

			$s1="select `id` from #__menu where `component_id`='$extnID' and `menutype`='mainmenu' and `published`=1";
			$db->setQuery($s1); $ItemID=  $db->loadResult();

			if($component=='Content_plugin'){
				$ccItemID	=  $_SESSION['CC_MenuItemID'];
				return $ccItemID;
			}
			else if($component=='caddy'){
				$ccItemID	=  $_SESSION['CC_MenuItemID'];
				return $ccItemID;
			} else{
				return $ItemID;
			}
		}
	}
?>