<?php

/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

defined('_JEXEC') or die('Restricted access');

	// Set default timezone for DATE/TIME functions
	if(function_exists('date_default_timezone_set'))
	{
		date_default_timezone_set('Europe/Amsterdam');
	}


	// Include settings & library
	require_once(dirname(__FILE__) . '/settings.php');
	require_once(dirname(__FILE__) . '/idealprofessional.cls.php');





	// Setup issuer request
	$oIssuerRequest = new IssuerRequest();
	$oIssuerRequest->setSecurePath($aSettings['CERTIFICATE_PATH']);
	$oIssuerRequest->setCachePath($aSettings['TEMP_PATH']);
	$oIssuerRequest->setPrivateKey($aSettings['PRIVATE_KEY_PASS'], $aSettings['PRIVATE_KEY_FILE'], $aSettings['PRIVATE_CERTIFICATE_FILE']);
	$oIssuerRequest->setMerchant($aSettings['MERCHANT_ID'], $aSettings['SUB_ID']);
	$oIssuerRequest->setAcquirer($aSettings['ACQUIRER'], $aSettings['TEST_MODE']);





	// Request issuer list (List of banks)
	$sIssuerOptions = '';
	$aIssuerList = $oIssuerRequest->doRequest();





	// Any errors in request?
	if($oIssuerRequest->hasErrors()) 
	{ $errorCode = 1;
		// Je kunt $oStatusRequest->getErrors() gebruiken om de lijst met opgetreden fouten op te vragen.
		/*	$getErrorReport 	= $oIssuerRequest->getErrors();
			 echo '<div class="red_message_box">';
			 echo '<img src="'.JURI::root().'/components/com_ccidealplatform/assets/images/cross.png" width="16" height="16" align="top" />';	
				foreach($getErrorReport as $showError){
					echo "<span style='padding:0px 0px 0px 7px;'>".$showError['desc']."</span>";
					echo "<br/>";
					echo JTEXT::_('CCIDEALPLATFORM_ERROCODE').$showError['code'];	
					$errorCode = 1;
				}
			echo '</div>';*/
		//print_r($oIssuerRequest->getErrors());
		 
	}
	else
	{
		$sIssuerOptions .= '<select name="issuerID" class="inputbox bform_select"  size="1">';
		$sIssuerOptions .= "<option value=\"0\">" . JText::_('ID_MESSAGE4') . "</option>" ;
		foreach($aIssuerList as $k => $v)
		{
			$sIssuerOptions .= '<option value="' . $k . '">' . htmlentities($v) . '</option>';
		}
		$sIssuerOptions .="</select>"; 
		/*List the issuer ID*/
		echo $sIssuerOptions;
	}
 
	// Create random order data
	$sOrderId = 'WEB-' . rand(1000000, 9999999); // Random order ID
	$sOrderDescription = 'My Order Description'; // Upto 32 characters
	$fOrderAmount = round(rand(100, 25000) / 100, 2); // Random amount, 2 decimals (1.00 to 250.00)


 ?>
 
 
 