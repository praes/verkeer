/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license. 
**/

function submitData()
{

	if(document.getElementById('grandtotal').value == '') {
		alert('Total amount is missing');
		return false;
	}
	else if(document.getElementById('ordernumber').value == '') {
		alert('order number is missing');
		return false;
	}
	else if(document.getElementById('extn').value == '') {
		alert('Extension name is missing');
		return false;
	}
	else{
	  return true;
	}
}

function submitBank()
{
	var form = document.idealform;
	if(form.elements[4].value == 0)
	{
		alert('Selecteer uw bank en klik op betalen');
		return false;
	}
	else
	{
		form.submit();
	}
}

function submitsofortBank()
{
	var form = document.idealform;
	if(form.sofort_country.value == 0)
	{
		alert('Selecteer uw land en klik op betalen');
		return false;
	}
	else
	{
		form.submit();
	}
}

function submitbtransfer()
{
	var form = document.idealform;
	if(form.btransfer_email.value == "")
	{
		alert('Voer het e-mailadres');
		return false;
	}
	else
	{
		form.submit();
	}
}


function submitBanktest(test){

	if(test == "test1"){
		if( document.test_form1.id.value= "idealform1" ){
			document.test_form1.submit();
		}else{
			return false;
		}
	}

	if(test == "test2"){
		if( document.test_form2.id.value= "idealform2" ){
			document.test_form2.submit();
		}else{
			return false;
		}
	}

	if(test == "test3"){
		if( document.test_form3.id.value= "idealform3" ){
			document.test_form3.submit();
		}else{
		 	return false;
		}
	}

	if(test == "test4"){
		if( document.test_form4.id.value= "idealform4" ){
			document.test_form4.submit();
		}else{
			return false;
		}
	}

	if(test == "test5"){
		if( document.test_form5.id.value= "idealform5" ){
			document.test_form5.submit();
		}else{
			return false;
		}
	}

	if(test == "test6"){
		if( document.test_form6.id.value= "idealform6" ){
			document.test_form6.submit();
		}else{
			return false;
		}
	}

	if(test == "test7"){
		if( document.test_form7.id.value= "idealform7" ){
			document.test_form7.submit();
		}else{
			return false;
		}
	}
}