/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

<!--
//Browser Support Code
function ajaxFunction(form,str,count){

	var ajaxRequest;  // The variable that makes Ajax possible!

	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
	// Create a function that will receive data sent from the server


	var path 		= document.getElementById("path").value;

    if(str == 'amount_by_user') {


			var amount_by_user = form.amount_by_user.value;

			if(amount_by_user == '') { // Checking for amount_by_user

				var div1 = "ajaxDivname"+count;
				var div2  = "ajaxDivname1"+count;

				form.amount_by_user.style.borderColor = 'red';
				document.getElementById(div1).style.display="block";
				document.getElementById(div2).style.display="none";
				form.content_plugin_submit.disabled = true;

				return false;
			}else{

				var url=path+'index.php?option=com_ccidealplatform&task=checkval&amtbyuser='+amount_by_user;
			}

	   }else if(str == 'paymentid_by_user') { // Checking for paymentid_by_user

		var paymentid_by_user = form.paymentid_by_user.value;
		if(paymentid_by_user == '') {

				var div3 = "ajaxDivpayuser1"+count;
				var div4  = "ajaxDivpayuser"+count;

			form.paymentid_by_user.style.borderColor = 'red';
			document.getElementById(div3).style.display='none';
			document.getElementById(div4).style.display='block';
			form.content_plugin_submit.disabled = true;

			return false;
		}else{
				var url=path+'index.php?option=com_ccidealplatform&task=checkval&paybyuser='+paymentid_by_user;
			}
	 }else if(str =='extra_textfield'){ // Checking for extra_textfield

	 	var extra_textfield = form.extra_textfield.value;
	 	var url=path+'index.php?option=com_ccidealplatform&task=checkval&extra_textfield='+extra_textfield;

	 }else if(str == 'extra_textfield_required'){ // Checking for extra_textfield_required

		var extra_textfield_required = form.extra_textfield_required.value;

			if(extra_textfield_required ==""){

				var div6 = "ajaxDiv_textfield_req"+count;
				var div7  = "ajaxDiv_textfield_req1"+count;

			form.extra_textfield_required.style.borderColor = 'red';
			document.getElementById(div7).style.display='none';
			document.getElementById(div6).style.display='block';
			form.content_plugin_submit.disabled = true;

			}else{
	 			var url=path+'index.php?option=com_ccidealplatform&task=checkval&extra_textfield_required='+extra_textfield_required;
	 	}
	 }else if(str =='extra_textarea'){ // Checking for extra_textarea

	 	var extra_textarea = form.extra_textarea.value;
	 	var url=path+'index.php?option=com_ccidealplatform&task=checkval&extra_textarea='+extra_textarea;
				if(extra_textarea ==""){
					form.content_plugin_submit.disabled = false;
					form.extra_textarea_required.style.borderColor = 'green';
					form.content_plugin_submit.disabled = false;

				}

	 }else if(str =='extra_textarea_required'){ // Checking for extra_textarea_required

		var extra_textarea_required = form.extra_textarea_required.value;

			if(extra_textarea_required ==""){

				var div9 = "ajaxDiv_textarea_req"+count;
				var div10  = "ajaxDiv_textarea_req1"+count;

			form.extra_textarea_required.style.borderColor = 'red';
			document.getElementById(div10).style.display='none';
			document.getElementById(div9).style.display='block';
			form.content_plugin_submit.disabled = true;

			}else{

	 			var url=path+'index.php?option=com_ccidealplatform&task=checkval&extra_textarea_required='+extra_textarea_required;
	 	}

	 }


		ajaxRequest.onreadystatechange = function(){
					if(ajaxRequest.readyState == 4){
						var ajaxDisplay = ajaxRequest.responseText;
					    var update = new Array();

			       		if(ajaxDisplay.indexOf('|') != -1) {
			            	update = ajaxDisplay.split('|');
			           		changeText(update[0],update[1],count,form);
			        	}
					}
				}

	ajaxRequest.open("GET", url, true);
	ajaxRequest.send(null);
}

function submitbutton(form,count,temp_value){

	var div1 = "ajaxDivname"+count;
	var div2  = "ajaxDivname1"+count;

	var div3 = "ajaxDivpayuser1"+count;
	var div4  = "ajaxDivpayuser"+count;

	var div6 = "ajaxDiv_textfield_req"+count;
	var div7  = "ajaxDiv_textfield_req1"+count;

	var div9 = "ajaxDiv_textarea_req"+count;
	var div10  = "ajaxDiv_textarea_req1"+count;

	if(temp_value == 3 ){
			var amount_by_user = form.amount_by_user.value;
		if(amount_by_user =="" ){

		      			form.amount_by_user.style.borderColor = 'red';
		      			document.getElementById(div1).style.display="block";
		     			document.getElementById(div2).style.display="none";
		    			form.content_plugin_submit.disabled=true;
						form.amount_by_user.focus();

				return false;
		      }else{

	      			ajaxFunction(form,'amount_by_user',count);
	      }
		var paymentid_by_user = form.paymentid_by_user.value;
	    if(paymentid_by_user == "" ){


	      			form.paymentid_by_user.style.borderColor = 'red';
	      			document.getElementById(div4).style.display="block";
	     			document.getElementById(div3).style.display="none";
	    			form.content_plugin_submit.disabled=true;
					form.paymentid_by_user.focus();

				return false;
	      }else{

	      		ajaxFunction(form,'paymentid_by_user',count);


	      }

   	    if(extra_textfield_required == "" ){

				var extra_textfield_required = form.extra_textfield_required.value;
	      			form.extra_textfield_required.style.borderColor = 'red';
	      			document.getElementById(div6).style.display="block";
	     			document.getElementById(div7).style.display="none";
	    			form.content_plugin_submit.disabled=true;
					form.extra_textfield_required.focus();

				return false;
	      }else{

	      		ajaxFunction(form,'extra_textfield_required',count);


	      }
	}

	if(temp_value == 2 ){
			var paymentid_by_user = form.paymentid_by_user.value;

	    if(paymentid_by_user == "" ){

	      			form.paymentid_by_user.style.borderColor = 'red';
	      			document.getElementById(div4).style.display="block";
	     			document.getElementById(div3).style.display="none";
	    			form.content_plugin_submit.disabled=true;
					form.paymentid_by_user.focus();

				return false;
	      }else{

	      		ajaxFunction(form,'paymentid_by_user',count);


	      }
	}


	if(temp_value == 1 ){
		var amount_by_user = form.amount_by_user.value;

	    if(amount_by_user == "" ){
		      			form.amount_by_user.style.borderColor = 'red';
		      			document.getElementById(div1).style.display="block";
		     			document.getElementById(div2).style.display="none";
		    			form.content_plugin_submit.disabled=true;
						form.amount_by_user.focus();

				return false;
		      }else{
				 ajaxFunction(form,'amount_by_user',count);
	      }
	}

	if(temp_value == 4 ){
		var extra_textfield_required = form.extra_textfield_required.value;
		var myRegxp = /^[A-Za-z0-9_\s@.,#!?]+$/;

	    if(extra_textfield_required == "" ){
		      			form.extra_textfield_required.style.borderColor = 'red';
		      			document.getElementById(div6).style.display="block";
		     			document.getElementById(div7).style.display="none";
		    			form.content_plugin_submit.disabled=true;
						form.extra_textfield_required.focus();

				return false;
		      }else if(myRegxp.test(extra_textfield_required)==false){

		      	  		form.content_plugin_submit.disabled = true;
		      	  		form.extra_textfield_required.style.borderColor = 'red';
		      			document.getElementById(div6).style.display="block";
		     			document.getElementById(div7).style.display="none";
		    			form.content_plugin_submit.disabled=true;
						form.extra_textfield_required.focus();
						return false;

		      	  }else{
				 ajaxFunction(form,'extra_textfield_required',count);
	      }
	}

	if(temp_value == 5 ){
		var extra_textarea_required = form.extra_textarea_required.value;
		var myRegxp = /^[A-Za-z0-9_\s@.,#!?]+$/;
	    if(extra_textarea_required == "" ){
		      			form.extra_textarea_required.style.borderColor = 'red';
		      			document.getElementById(div9).style.display="block";
		     			document.getElementById(div10).style.display="none";
		    			form.content_plugin_submit.disabled=true;
						form.extra_textarea_required.focus();

				return false;
		      }else if(myRegxp.test(extra_textarea_required)==false){

		      	  		form.content_plugin_submit.disabled = true;
		      	  		form.extra_textarea_required.style.borderColor = 'red';
		      			document.getElementById(div9).style.display="block";
		     			document.getElementById(div10).style.display="none";
		    			form.content_plugin_submit.disabled=true;
						form.extra_textarea_required.focus();
						return false;

		      	  }else{
				 	ajaxFunction(form,'extra_textarea_required',count);
				 }

	}

}

function changeText(div_id,value_temp,count,form)
{
	var value_temp = value_temp.substring(0, 5);

	var div1 = "ajaxDivname"+count;
	var div2  = "ajaxDivname1"+count;

	var div3 = "ajaxDivpayuser1"+count;
	var div4  = "ajaxDivpayuser"+count;

	var div5	="ajaxDiv_textfield"+count;
	var div5_1	="ajaxDiv_textfield1"+count;

	var div6 = "ajaxDiv_textfield_req"+count;
	var div7 = "ajaxDiv_textfield_req1"+count;

	var div8 = "ajaxDiv_textarea"+count;
	var div8_1 = "ajaxDiv_textarea1"+count;

	var div9 = "ajaxDiv_textarea_req"+count;
	var div10 = "ajaxDiv_textarea_req1"+count;

	if(div_id == 1) {

		if(value_temp == -1) {

			document.getElementById(div1).style.display="block";
			document.getElementById(div2).style.display="none";

			form.amount_by_user.style.borderColor = 'red';
			form.content_plugin_submit.disabled = true;
			form.amount_by_user.focus();
			document.josForm.hidden_org.value= '';

		}else if(value_temp == 3){

			form.amount_by_user.style.borderColor = 'green';
			document.getElementById(div1).style.display='none';
			document.getElementById(div2).style.display='block';

			form.content_plugin_submit.disabled = false;

		}
	}

	else if(div_id == 2){

		if(value_temp == -1) {

			form.paymentid_by_user.style.borderColor = 'red';
			document.getElementById(div4).style.display='block';
			document.getElementById(div3).style.display='none';

			form.paymentid_by_user.focus();
			form.content_plugin_submit.disabled = true;

		} else if(value_temp == 5){

			form.paymentid_by_user.style.borderColor = 'green';
			document.getElementById(div4).style.display='none';
			document.getElementById(div3).style.display='block';
			form.content_plugin_submit.disabled = false;

		}

	}

	else if(div_id == 3){

		var myRegxp = /^[A-Za-z0-9-_\s@.,#!?]*$/;
		var extra_textfield = form.extra_textfield.value;

		if(value_temp == 3) {

				if(myRegxp.test(extra_textfield)==false){

		      	  		form.content_plugin_submit.disabled = true;
		      	  		form.extra_textfield.style.borderColor = 'red';
		      			document.getElementById(div5).style.display="block";
		     			document.getElementById(div5_1).style.display="none";
		    			form.content_plugin_submit.style.display=true;
						form.extra_textfield.focus();
						return false;

		      	  }else{

						form.extra_textfield.style.borderColor = 'red';
						document.getElementById(div5).style.display='block';
						document.getElementById(div5_1).style.display='none';
						form.content_plugin_submit.disabled = true;
						form.extra_textfield.focus();
			}

		}else if(value_temp == 2){


						form.content_plugin_submit.disabled = false;
						form.extra_textfield.style.borderColor = 'green';
						document.getElementById(div5).style.display='none';

		}
		else{
					if(myRegxp.test(extra_textfield)==false){

		      	  		form.content_plugin_submit.disabled = true;
		      	  		form.extra_textfield.style.borderColor = 'red';
		      			document.getElementById(div5).style.display="block";
		     			document.getElementById(div5_1).style.display="none";
		    			form.content_plugin_submit.style.display=true;
						form.extra_textfield.focus();
						return false;

		      	  }else{
						form.content_plugin_submit.disabled = false;
						form.extra_textfield.style.borderColor = 'green';
						document.getElementById(div5).style.display='none';
						document.getElementById(div5_1).style.display='block';
			}

		}

	}

	else if(div_id == 4){

		var myRegxp = /^[A-Za-z0-9_\s@.,#!?]+$/;
		var extra_textfield_required = form.extra_textfield_required.value;

		if(value_temp == 1) {

					if(myRegxp.test(extra_textfield_required)==false){

		      	  		form.content_plugin_submit.disabled = true;
		      	  		form.extra_textfield_required.style.borderColor = 'red';
		      			document.getElementById(div6).style.display="block";
		     			document.getElementById(div7).style.display="none";
		    			form.content_plugin_submit.style.display=true;
						form.extra_textfield_required.focus();
						return false;

		      	  }else{
						form.content_plugin_submit.disabled = false;
						form.extra_textfield_required.style.borderColor = 'green';
						document.getElementById(div7).style.display='block';
						document.getElementById(div6).style.display='none';
			}

		}else{

			form.extra_textfield_required.style.borderColor = 'red';
			document.getElementById(div6).style.display='block';
			document.getElementById(div7).style.display='none';
			form.content_plugin_submit.disabled = true;
			form.extra_textfield_required.focus();

 		}

	}

	else if(div_id == 5){
		var myRegxp = /^[A-Za-z0-9_\s@.,#!?]+$/;
		var extra_textarea = form.extra_textarea.value;
		var dd = document.getElementById("extra_textarea").value;
		if(value_temp == 1) {

					if(myRegxp.test(extra_textarea)==false){

		      	  		form.content_plugin_submit.disabled = true;
		      	  		form.extra_textarea.style.borderColor = 'red';
		      			document.getElementById(div8).style.display="block";
		     			document.getElementById(div8_1).style.display="none";
		    			form.content_plugin_submit.style.display=true;
						form.extra_textarea.focus();
						return false;

		      	  }else{
						form.content_plugin_submit.disabled = false;
						form.extra_textarea.style.borderColor = 'green';
						document.getElementById(div8_1).style.display='block';
						document.getElementById(div8).style.display='none';
			}

		}else if(value_temp == 2){

			if(myRegxp.test(extra_textarea)==false){
					if(extra_textarea==""){
						form.content_plugin_submit.disabled = false;
		      	  		form.extra_textarea.style.borderColor = 'green';
		     			document.getElementById(div8_1).style.display="none";

					}else{
		      	  		form.content_plugin_submit.disabled = true;
		      	  		form.extra_textarea.style.borderColor = 'red';
		      			document.getElementById(div8).style.display="block";
		     			document.getElementById(div8_1).style.display="none";
		    			form.content_plugin_submit.style.display=false;
						form.extra_textarea.focus();
						return false;
					}
		      	  }else{
						form.content_plugin_submit.disabled = false;
						form.extra_textarea.style.borderColor = 'green';
						document.getElementById(div8_1).style.display='none';
				}

		}else if(value_temp == 3){
					if(myRegxp.test(extra_textarea)==false){
						form.content_plugin_submit.disabled = true;
		      	  		form.extra_textarea.style.borderColor = 'red';
		      			document.getElementById(div8).style.display="block";
		     			document.getElementById(div8_1).style.display="none";
		    			form.content_plugin_submit.style.display=false;
						form.extra_textarea.focus();
						return false;
					}
		}
		else{

			form.extra_textarea.style.borderColor = 'green';
			document.getElementById(div8).style.display='block';
			document.getElementById(div8_1).style.display='none';
			form.content_plugin_submit.disabled = false;
 		}

	}

	else if(div_id == 6){

	var myRegxp = /^[A-Za-z0-9_\s@.,#!?]+$/;

		if(value_temp == 1) {

			var extra_textarea_required = form.extra_textarea_required.value;

			if(myRegxp.test(extra_textarea_required)==false){

		      	  		form.content_plugin_submit.disabled = true;
		      	  		form.extra_textarea_required.style.borderColor = 'red';
		      			document.getElementById(div9).style.display="block";
		     			document.getElementById(div10).style.display="none";
		    			form.content_plugin_submit.style.display=true;
						form.extra_textarea_required.focus();
						return false;

		      	  }else{

					form.content_plugin_submit.disabled = false;
					form.extra_textarea_required.style.borderColor = 'green';
					document.getElementById(div10).style.display='block';
					document.getElementById(div9).style.display='none';
			}

		}else{
			form.extra_textarea_required.style.borderColor = 'red';
			document.getElementById(div9).style.display='block';
			document.getElementById(div10).style.display='none';
			form.content_plugin_submit.disabled = true;
			form.extra_textarea_required.focus();

 		}

	}
}

//-->


