<?php
    /**
     * @package	cciDEAL Platform
     * @author 	Chill Creations <info@chillcreations.com>
     * @link 	http://www.chillcreations.com
     * @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
     * @license	GNU/GPL, see LICENSE.php for full license.
     **/
    // no direct access
    defined('_JEXEC') or die('Restricted access');

    $disabled = "";
    $class = 'class="btn btn-small"';

?>
<div class="chillcreations-bootstrap">
    <h1>cciDEAL test form</h1>
    <div class="well"><?php echo JTEXT::_('TEST_INFO'); ?></div>
    <?php
        $count = count($this->test1_paid);
    ?>

        <table width="100%" class="table table-striped table-hovered">
            <thead>
            <tr>
            <th>Test #</th>
            <th>Result</th>
            <th>Action</th>
                <th>Status</th>
            </tr>
            </thead>

            <!-- TEST 1 NEW -->
            <tr <?php if($this->test1_paid) { echo "class='success'";} ?>>
                <td>
                    <?php echo JTEXT::_('TESTCASE1'); ?>
                </td>
                <td>
                    <?php echo JTEXT::_('TESTRESULT1'); ?>
                </td>
                <td>
                    <form action="index.php?option=com_ccidealplatform&task=bankform" id="idealform1" method="post" target="_self" name="test_form1" title="test_form1">
                        <input type="button" id="submit1" value="<?php echo JText::_( 'TEST' ); ?>" onclick="submitBanktest('test1')" <?php echo $class ?> />
                        <input type="hidden" name="grandtotal" id="grandtotal" value="1" />
                        <input type="hidden" name="ordernumber" id="ordernumber" value="test1"/>
                        <input type="hidden" name="extn" id="extn" value="testform1"/>
                    </form>
                </td>
                <td>
                    <?php
                        if(!empty($this->test1_paid)) {
                            foreach($this->test1_paid as $paid1){
                                if($paid1->extension=="testform1"){
                                    
                                    echo "&nbsp;<i class='icon icon-checkmark text-success'></i>&nbsp;";
                                }else{
                                    
                                    echo "&nbsp;<i class='icon icon-remove text-error'></i>&nbsp;";
                                }

                                if($paid1->payment_date!=""){
                                    echo "&nbsp;".JTEXT::_('TESTED')."&nbsp;". (gmdate("d-m-Y H:i",strtotime($paid1->payment_date)));
                                }else{
                                    echo "&nbsp;".JTEXT::_('NOT_TESTED');
                                }
                            }
                        }else{
                            
                            echo "&nbsp;<i class='icon icon-remove text-error'></i>&nbsp;";
                            echo " &nbsp;".JTEXT::_('NOT_TESTED');
                        }
                    ?>
                </td>
            </tr>

            <!-- TEST 2 NEW -->
            <tr <?php if($this->test2_paid) { echo "class='success'";} ?>>
                <td>
                    <?php echo JTEXT::_('TESTCASE2'); ?>
                </td>
                <td>
                    <?php echo JTEXT::_('TESTRESULT2'); ?>
                </td>
                <td>
                    <form action="index.php?option=com_ccidealplatform&task=bankform" id="idealform2" method="post" target="_self" name="test_form2" title="test_form2">
                        <input id="submit2" type="button" value="<?php echo JText::_( 'TEST' ); ?>" onclick="submitBanktest('test2')" <?php echo $class ?> />
                        <input type="hidden" name="grandtotal" id="grandtotal" value="2" />
                        <input type="hidden" name="ordernumber" id="ordernumber" value="test2"/>
                        <input type="hidden" name="extn" id="extn" value="testform2"/>
                    </form>
                </td>
                <td>
                    <?php

                        if(!empty($this->test2_paid)){
                            foreach($this->test2_paid as $paid2){
                                if($paid2->extension=="testform2")
                                {
                                    echo "&nbsp;<i class='icon icon-checkmark text-success'></i>&nbsp;";
                                }else{
                                    echo "&nbsp;<i class='icon icon-remove text-error'></i>&nbsp;";
                                }

                                if($paid2->payment_date!=""){
                                    echo "&nbsp;" . JTEXT::_('TESTED') . "&nbsp;" . (gmdate("d-m-Y H:i",strtotime($paid2->payment_date)));
                                }else{
                                    echo "&nbsp;".JTEXT::_('NOT_TESTED');
                                }
                            }
                        }else{
                            echo "&nbsp;<i class='icon icon-remove text-error'></i>>&nbsp;";
                            echo "&nbsp;".JTEXT::_('NOT_TESTED');
                        }
                    ?>
                </td>
            </tr>

            <!-- TEST 3 NEW -->
            <tr <?php if($this->test3_paid) { echo "class='success'";} ?>>
                <td>
                    <?php echo JTEXT::_('TESTCASE3'); ?>
                </td>
                <td>
                    <?php echo JTEXT::_('TESTRESULT3'); ?>
                </td>
                <td>
                    <form action="index.php?option=com_ccidealplatform&task=bankform" id="idealform3" method="post" target="_self" name="test_form3" title="test_form3">
                        <input id="submit3" type="button" value="<?php echo JText::_( 'TEST' ); ?>" onclick="submitBanktest('test3')" <?php echo $class ?> />
                        <input type="hidden" name="grandtotal" id="grandtotal" value="3" />
                        <input type="hidden" name="ordernumber" id="ordernumber" value="test3"/>
                        <input type="hidden" name="extn" id="extn" value="testform3"/>
                    </form>
                </td>
                <td>
                    <?php

                        if(!empty($this->test3_paid)){
                            foreach($this->test3_paid as $paid3){

                                if($paid3->extension=="testform3"){
                                    echo "&nbsp;<i class='icon icon-checkmark text-success'></i>&nbsp;";
                                }else{
                                    echo "&nbsp;<i class='icon icon-remove text-error'></i>&nbsp;";
                                }


                                if($paid3->payment_date!=""){
                                    echo "&nbsp;".JTEXT::_('TESTED')."&nbsp;".(gmdate("d-m-Y H:i",strtotime($paid3->payment_date)));
                                }else{
                                    echo "&nbsp;".JTEXT::_('NOT_TESTED');
                                }
                            }
                        }else{
                            echo "&nbsp;<i class='icon icon-remove text-error'></i>&nbsp;";
                            echo "&nbsp;".JTEXT::_('NOT_TESTED');
                        }
                    ?>
                </td>
            </tr>

            <!-- TEST 4 NEW -->
            <tr <?php if($this->test4_paid) { echo "class='success'";} ?>>
                <td>
                    <?php echo JTEXT::_('TESTCASE4'); ?>
                </td>
                <td>
                    <?php echo JTEXT::_('TESTRESULT4'); ?>
                </td>
                <td>
                    <form action="index.php?option=com_ccidealplatform&task=bankform" id="idealform4" method="post" target="_self" name="test_form4" title="test_form4">
                        <input id="submit4" type="button" value="<?php echo JText::_( 'TEST' ); ?>" onclick="submitBanktest('test4')" <?php echo $class ?> />
                        <input type="hidden" name="grandtotal" id="grandtotal" value="4" />
                        <input type="hidden" name="ordernumber" id="ordernumber" value="test4"/>
                        <input type="hidden" name="extn" id="extn" value="testform4"/>
                    </form>
                </td>
                <td>
                    <?php

                        if(!empty($this->test4_paid)){
                            foreach($this->test4_paid as $paid4){
                                if($paid4->extension=="testform4"){
                                    echo "&nbsp;<i class='icon icon-checkmark text-success'></i>&nbsp;";
                                }else{
                                    echo "&nbsp;<i class='icon icon-remove text-error'></i>&nbsp;";
                                }

                                if($paid4->payment_date!=""){
                                    echo "&nbsp;".JTEXT::_('TESTED')."&nbsp;".(gmdate("d-m-Y H:i",strtotime($paid4->payment_date)));
                                }else{
                                    echo "&nbsp;".JTEXT::_('NOT_TESTED');
                                }
                            }
                        }else{
                            echo "&nbsp;<i class='icon icon-remove text-error'></i>&nbsp;";
                            echo "&nbsp;".JTEXT::_('NOT_TESTED');
                        }
                    ?>
                </td>
            </tr>

            <!-- TEST 5 NEW -->
            <tr <?php if($this->test5_paid) { echo "class='success'";} ?>>
                <td>
                    <?php echo JTEXT::_('TESTCASE5'); ?>
                </td>
                <td>
                    <?php echo JTEXT::_('TESTRESULT5'); ?>
                </td>
                <td>
                    <form action="index.php?option=com_ccidealplatform&task=bankform" id="idealform5" method="post" target="_self" name="test_form5" title="test_form5">
                        <input id="submit5" type="button" value="<?php echo JText::_( 'TEST' ); ?>" onclick="submitBanktest('test5')" <?php echo $class ?> />
                        <input type="hidden" name="grandtotal" id="grandtotal" value="5" />
                        <input type="hidden" name="ordernumber" id="ordernumber" value="test5"/>
                        <input type="hidden" name="extn" id="extn" value="testform5"/>
                    </form>
                </td>
                <td>
                    <?php

                        if(!empty($this->test5_paid)){
                            foreach($this->test5_paid as $paid5){
                                if($paid5->extension=="testform5"){
                                    echo "&nbsp;<i class='icon icon-checkmark text-success'></i>&nbsp;";
                                }else{
                                    echo "&nbsp;<i class='icon icon-remove text-error'></i>&nbsp;";
                                }


                                if($paid5->payment_date!=""){
                                    echo "&nbsp;".JTEXT::_('TESTED')."&nbsp;".(gmdate("d-m-Y H:i",strtotime($paid5->payment_date)));
                                }else{
                                    echo "&nbsp;".JTEXT::_('NOT_TESTED');
                                }
                            }
                        }else{
                            echo "&nbsp;<i class='icon icon-remove text-error'></i>&nbsp;";
                            echo "&nbsp;".JTEXT::_('NOT_TESTED');
                        }
                    ?>
                </td>
            </tr>

            <!-- TEST 6 NEW -->
            <tr <?php if($this->test6_paid) { echo "class='success'";} ?>>
                <td>
                    <?php echo JTEXT::_('TESTCASE6'); ?>
                </td>
                <td>
                    <?php echo JTEXT::_('TESTRESULT6'); ?>
                </td>
                <td>
                    <form action="index.php?option=com_ccidealplatform&task=bankform" id="idealform6" method="post" target="_self" name="test_form6" title="test_form6">
                        <input id="submit6" type="button" value="<?php echo JText::_( 'TEST' ); ?>" onclick="submitBanktest('test6')" <?php echo $class ?> />
                        <input type="hidden" name="grandtotal" id="grandtotal" value="6" />
                        <input type="hidden" name="ordernumber" id="ordernumber" value="test6"/>
                        <input type="hidden" name="extn" id="extn" value="testform6"/>
                    </form>
                </td>
                <td>
                    <?php

                        if(!empty($this->test6_paid)){
                            foreach($this->test6_paid as $paid6){
                                if($paid6->extension=="testform6"){
                                    echo "&nbsp;<i class='icon icon-checkmark text-success'></i>&nbsp;";
                                }else{
                                    echo "&nbsp;<i class='icon icon-remove text-error'></i>&nbsp;";
                                }

                                if($paid6->payment_date!=""){
                                    echo "&nbsp;".JTEXT::_('TESTED')."&nbsp;".(gmdate("d-m-Y H:i",strtotime($paid6->payment_date)));
                                }else{
                                    echo "&nbsp;".JTEXT::_('NOT_TESTED');
                                }
                            }
                        }else{
                            
                            echo "&nbsp;<i class='icon icon-remove text-error'></i>&nbsp;";
                            echo "&nbsp;".JTEXT::_('NOT_TESTED');
                        }
                    ?>
                </td>
            </tr>

            <!-- TEST 7 NEW -->
            <tr <?php if($this->test7_paid) { echo "class='success'";} ?>>
                <td>
                    <?php echo JTEXT::_('TESTCASE7'); ?>
                </td>
                <td>
                    <?php echo JTEXT::_('TESTRESULT7'); ?>
                </td>
                <td>
                    <form action="index.php?option=com_ccidealplatform&task=bankform" id="idealform7" method="post" target="_self" name="test_form7" title="test_form7">
                        <input id="submit7" type="button" value="<?php echo JText::_( 'TEST' ); ?>" onclick="submitBanktest('test7')" <?php echo $class ?> />
                        <input type="hidden" name="grandtotal" id="grandtotal" value="7" />
                        <input type="hidden" name="ordernumber" id="ordernumber" value="test7"/>
                        <input type="hidden" name="extn" id="extn" value="testform7"/>
                    </form>
                </td>
                <td>
                    <?php

                        if(!empty($this->test7_paid)){
                            foreach($this->test7_paid as $paid7){
                                if($paid7->extension=="testform7"){
                                    echo "&nbsp;<i class='icon icon-checkmark text-success'></i>&nbsp;";
                                }else{
                                    echo "&nbsp;<i class='icon icon-remove text-error'></i>&nbsp;";
                                }


                                if($paid7->payment_date!=""){
                                    echo "&nbsp;".JTEXT::_('TESTED')."&nbsp;".(gmdate("d-m-Y H:i",strtotime($paid7->payment_date)));
                                }else{
                                    echo "&nbsp;".JTEXT::_('NOT_TESTED');
                                }
                            }
                        }else{
                            echo "&nbsp;<i class='icon icon-remove text-error'></i>&nbsp;";
                            echo "&nbsp;".JTEXT::_('NOT_TESTED');
                        }
                    ?>
                </td>
            </tr>

        </table>

    <div class="well"><?php echo JTEXT::_('CHECK_STATUS'); ?></div>
</div>