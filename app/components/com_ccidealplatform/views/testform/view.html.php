<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class cciDealPlatformViewtestform extends JViewLegacy {

	function display($tpl = null) {

		$db = JFactory::getDBO();

		$post = JRequest::get('post');

			$query1 = "SELECT IDEAL_Bank FROM `#__ccidealplatform_config` WHERE `id` = 1";
			$db->setQuery( $query1);
 			$IDEAL_Bank = $db->loadResult();		
			
			if($IDEAL_Bank == "POSTBASIC"){
				$status1_test = $status6_test = 'pending';
				$status3_test = $status4_test = $status5_test = 'pending';
			}else{
				$status1_test = $status6_test = 'paid';
				$status3_test = $status4_test = $status5_test = 'cancelled';
			}

			//test 1
			$query1 = "SELECT * FROM `#__ccidealplatform_payments` WHERE `extension` = 'testform1' AND `payment_status`='".$status1_test."'" ;
			$db->setQuery( $query1);
 			$test1_paid = $db->loadObjectlist();
			
 			$bgcolor  = "#F3F3F3";
 			$this->assignRef('bgcolor',		$bgcolor);
	        $this->assignRef('test1_paid',		$test1_paid);

			//test 2
	        $query2 = "SELECT *	FROM `#__ccidealplatform_payments` WHERE `extension` = 'testform2' AND `payment_status`='cancelled'" ;
			$db->setQuery( $query2);
 			$test2_paid = $db->loadObjectlist();
	        $this->assignRef('test2_paid',		$test2_paid);

			//test 3
	        $query3 = "SELECT * FROM `#__ccidealplatform_payments` WHERE `extension` = 'testform3' AND `payment_status`='".$status3_test."'";
			$db->setQuery( $query3);
 			$test3_paid = $db->loadObjectlist();
	        $this->assignRef('test3_paid',		$test3_paid);

			//test 4
	        $query4 = "SELECT *	FROM `#__ccidealplatform_payments` WHERE `extension` = 'testform4' AND `payment_status`='".$status4_test."'";
			$db->setQuery( $query4);
 			$test4_paid = $db->loadObjectlist();
	        $this->assignRef('test4_paid',		$test4_paid);

			//test 5
	        $query5 = "SELECT * FROM `#__ccidealplatform_payments` WHERE `extension` = 'testform5' AND `payment_status`='".$status5_test."'";
			$db->setQuery( $query5);
 			$test5_paid = $db->loadObjectlist();
	        $this->assignRef('test5_paid',		$test5_paid);

			//test 6
	        $query6 = "SELECT * FROM `#__ccidealplatform_payments` WHERE `extension` = 'testform6' AND `payment_status`='".$status6_test."'";
			$db->setQuery( $query6);
 			$test6_paid = $db->loadObjectlist();
	        $this->assignRef('test6_paid',		$test6_paid);

			//test 7
	        $query7 = "SELECT *	FROM `#__ccidealplatform_payments` WHERE `extension` = 'testform7' AND `payment_status`='pending'";
			$db->setQuery( $query7);
 			$test7_paid = $db->loadObjectlist();
	        $this->assignRef('test7_paid',		$test7_paid);

		$this->assignRef('data', $post);
		parent::display($tpl);

	}	
}
?>