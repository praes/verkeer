<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

$jversion 			= new JVersion();
$current_version 	= $jversion->getShortVersion();
$jversion			= substr($current_version,0,1);

	$document =  JFactory::getDocument();
	$js = JURI::base().'components/com_ccidealplatform/assets/js/ccvalidate.js';
	$document->addScript($js);
	$user = JFactory::getUser();
	$user_type = $user->get( 'usertype' );
	$user_id   = $user->get('id');
	
	if($this->ideal_enable == 1 || $this->ideal_enable == 2){

	?>
	<center>
		<form name="idealform" action="index.php" method="post" target="_self">

			<input type="hidden" name="grandtotal" value="<?php echo $this->data['grandtotal']; ?>" />
			<input type="hidden" name="ordernumber" value="<?php echo $this->data['ordernumber']; ?>"/>
			<input type="hidden" name="issuerID" value="<?php if (empty ($_SESSION["issuerID"])) { echo @$_SESSION["issuerID"]; }?>"/>
			<input type="hidden" name="ec" value="<?php echo $this->data['ordernumber']; ?>"/>
			<?php require_once (JPATH_SITE . '/components/com_ccidealplatform/'.$this->bankfile.'/DirReqIdeal.php');
			if(!$result->isOK() || count($issuerArray) == 0) {
				require_once (JPATH_SITE . '/components/com_ccidealplatform/'.$this->bankfile.'/DirReqIdeal.php');
			}else{
			?>
				<div class="ccfull row-fluid">
					<div class="ccleft">
						<div class="ccll span4" >
							<img src="components/com_ccidealplatform/assets/images/iDEAL.gif" width="120" height="106" align="center" />
						</div>

							<div class="cclr span4" >
								<?php
									echo $issuerDropdowns;
								?>
							</div>
					</div>

					<div class="ccright span4" >
						<input type="button" value="<?php echo JText::_( 'ID_PAY_NOW' ); ?>" onclick="submitBank()" class="<?php if($jversion != "3") echo "ccmlimage"; ?> btn btn-success" />
					</div>

				</div> <?php
							}
								//For Custom rsmembership
							if($this->data['extn']=="rsmembership"){

								?><input type="hidden" name="custom" value="<?php echo  $this->data['custom'];  ?>"/><?php
							}

							//For virtuemart
							if(@$this->data['extn']=="virtuemart"){
								$virtue_sesn=@ JRequest::getVar('iDEALVMPayment_SESSION');
								?><input type="hidden" name="iDEALVMPayment_SESSION" value="<?php echo  $virtue_sesn;  ?>"/><?php
							}

								//For Custom payment

								if(@$this->custom_payment){
									echo "<input type='hidden' name='custom_payment' id='custom_payment' value='custom_payment' />";
								}

						?>

			<input type="hidden" name="extn" value="<?php echo $this->data['extn']; ?>"/>
			<input type="hidden" name="option" value="com_ccidealplatform" />
			<input type="hidden" name="task" value="checkccideal" />
		</form>
	</center>
	<?php
	}
?>