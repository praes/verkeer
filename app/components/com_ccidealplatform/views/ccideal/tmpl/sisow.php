<?php
/**
* @package	cciDEAL Platform 
* @author 	Chill Creations <info@chillcreations.com> 
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details. 
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined('_JEXEC') or die('Restricted access');
	
$jversion 			= new JVersion();
$current_version 	= $jversion->getShortVersion();
$jversion			= substr($current_version,0,1);

$user 				= JFactory::getUser();
@$user_type 		= $user->get( 'usertype' );
@$user_id   		= $user->get('id');
$amt				= @JRequest::getVar('amt');
$cont_extn			= @JRequest::getVar('cont_extn');

$extn				= @JRequest::getVar('com');
$orderid			= @JRequest::getVar('orderid');	
	
$ideal_paymentmethod = JRequest::getVar( 'ideal_paymentmethod', 'ideal');

if(@$this->data['grandtotal']!=""){
	$min_trans= $this->data['grandtotal'];
}else{
	$min_trans= $amt;
}

$sisow_merchantid = $this->ideal_config->sisow_merchantid;
$sisow_merchantkey = $this->ideal_config->sisow_merchantkey;
	
	?>
	<div class="akeeba-bootstrap">
	<?php		
		if($this->ideal_enable == 1 || $this->ideal_enable == 2){
			$transaction_id=JRequest::getVar('transaction_id');

			if($min_trans <0.84 && $transaction_id==""){ ?>
				
				<div class="red_message_box">
					<img src="<?php echo JURI::root(); ?>/components/com_ccidealplatform/assets/images/cross.png" width="16" height="16" class="ccmargin_RedboxFloat" align="top" /><?php
					echo JTEXT::_('MIN_TRANS2')."</div>";
			} ?>	
			
			<center>
				<form name="idealform" action="index.php" method="post" target="_self" class="idealform">
					<?php if($min_trans <0.84){ ?>
						<input type="hidden" name="amount" value="0.84" />
					<?php }else{?>
						<input type="hidden" name="amount" value="<?php echo $min_trans; ?>" />
					<?php } ?>

					<input type="hidden" name="item_number" value="<?php if(@$this->data['ordernumber']!=""){echo @$this->data['ordernumber'];}else{echo $orderid; } ?>"/>
					<input type='hidden' name='description' value="<?php echo $this->payment_desc_orderid; ?>"/>
					<input type='hidden' name='title' value="<?php echo $this->payment_desc; ?>"/>

					<?php
						if($ideal_paymentmethod == "ideal"){
							
							require_once(JPATH_COMPONENT . '/ccideal_sisow/sisow.php');						
							
							$sisow = new Sisow($sisow_merchantid, $sisow_merchantkey);
							$select = "";
							$mode = false;
							if($this->ideal_config->IDEAL_Mode == "TEST"){
								$mode = true;
							}
							
							$sisow->DirectoryRequest($select, true, $mode);
					?>					
					
							<div class="row-fluid">
								<div class="span2 text-center">
									<img src="components/com_ccidealplatform/assets/images/iDEAL.gif" style="height: 80px;">
								</div>
								<div class="span10 text-center">
									<h1><?php echo sprintf(JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_TITLE' ), $ideal_paymentmethod); ?></h1>
								</div>
							</div><br>
							<p>
								<?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_DESC' ); ?>
							</p><br>
							
							<div class="well well-small">
								<b><?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_TOTAL' ); ?></b> <?php echo "&#8364;".$min_trans; ?><br>
								<b><?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_ORDERNUMBER' ); ?></b> <?php if(@$this->data['ordernumber']!=""){echo @$this->data['ordernumber'];}else{echo $orderid; } ?>
							</div>
							
							<div class="row-fluid">
								<div class="span6">
									<?php echo $select ?>
								</div>
								<div class="span6">
									<a href="javascript:void(0);" class="btn btn-danger btn-large btn-block" onclick="submitBank()">
										<?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_CONTINUE' ); ?>&nbsp;&nbsp;<em class="icon-chevron-right icon-white"></em>
									</a>
								</div>
							</div>
							<br class="clear" />							
					<?php
						}			
					
						//Retry payment
						if(@$this->retry){
							?><input type="hidden" name="retry" value="<?php echo  "1";  ?>"/><?php
						}

						//For extension name
						if(@$this->data['extn']!=""){$extn= $this->data['extn'];}else{ $extn; }
						
						//For virtuemart
						if(@$extn=="virtuemart"){
							$virtue_sesn=@ JRequest::getVar('iDEALVMPayment_SESSION');
							?><input type="hidden" name="iDEALVMPayment_SESSION" value="<?php echo  $virtue_sesn;  ?>"/><?php
						}
						
					?>

					<input type="hidden" name="extn" value="<?php if(@$this->data['extn']!=""){echo @$this->data['extn'];}else{ echo $extn; } ?>"/>
					<input type="hidden" name="option" value="com_ccidealplatform" />
					<input type="hidden" name="task" value="sisow_process" />
					<input type="hidden" name="ideal_paymentmethod" value="<?php echo $ideal_paymentmethod; ?>" />
					
				</form>
			</center>
			<br style="clear:both;">	
			
			<?php
			
			if(($ideal_paymentmethod == "mistercash") || ($ideal_paymentmethod == "sofort")){
				?>
					<script>										
						this.idealform.submit();
					</script>
				<?php
			}			
		}
		?>
	</div>