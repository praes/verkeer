<?php
/**
* @package	cciDEAL Platform 
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details. 
* @license	GNU/GPL, see LICENSE.php for full license. 
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

$jversion 			= new JVersion();
$current_version 	= $jversion->getShortVersion();
$jversion			= substr($current_version,0,1);


$user = JFactory::getUser();
@$user_type = $user->get( 'usertype' );
@$user_id   = $user->get('id');
$amt=@ JRequest::getVar('amt');

if(@$this->data['grandtotal']!=""){

	$min_trans= $this->data['grandtotal'];

}else{

	$min_trans= $amt;
}
$min_trans = str_replace(',','.',$min_trans);

$extn=@ JRequest::getVar('com');
$orderid=@ JRequest::getVar('orderid');
?>
	
	<div class="akeeba-bootstrap"><?php

		if($this->ideal_enable == 1 || $this->ideal_enable == 2){
			$transaction_id=JRequest::getVar('transaction_id');

			if($min_trans <1.20 && $transaction_id==""){ ?>
			
					<div class="red_message_box">
					<img src="<?php echo JURI::root(); ?>/components/com_ccidealplatform/assets/images/cross.png" width="16" height="16" class="ccmargin_RedboxFloat" align="top" alt="" /><?php
					echo JTEXT::_('MIN_TRANS1')."</div>";
			} ?>
			
			<center>
				<form name="idealform" action="index.php" method="post" target="_self" class="idealform">
					<?php if($min_trans <1.20){ ?>
						<input type="hidden" name="amount" value="1.20" />
					<?php }else{?>
						<input type="hidden" name="amount" value="<?php echo $min_trans; ?>" />
					<?php } ?>

					<input type="hidden" name="item_number" value="<?php if(@$this->data['ordernumber']!=""){echo @$this->data['ordernumber'];}else{echo $orderid; } ?>"/>
					<input type='hidden' name='description' value="<?php echo $this->payment_desc_orderid; ?>"/>
					<input type='hidden' name='title' value="<?php echo $this->payment_desc; ?>"/>


                    <?php

						// Extension name
							if(@$this->data['extn']!=""){$extn= $this->data['extn'];}else{ $extn; }

                        // RSForm Pro
                        if(@$extn=="rsformpro" AND empty($ccideal_description)){
                            $ccideal_description = JRequest::getVar('ccideal_description');
                            ?><input type='hidden' name='ccideal_description' value="<?php echo $ccideal_description; ?>"/><?php
                        }

                        // RSMembership
							if(@$extn=="rsmembership"){
								$custom=@ JRequest::getVar('custom');
								?><input type="hidden" name="custom" value="<?php echo  $custom;  ?>"/><?php
							}
						// Virtuemart
							if(@$extn=="virtuemart"){
								$virtue_sesn=@ JRequest::getVar('iDEALVMPayment_SESSION');
								?><input type="hidden" name="iDEALVMPayment_SESSION" value="<?php echo  $virtue_sesn;  ?>"/><?php
							}

						// Custom payment
							if(@$this->custom_payment){
								echo "<input type='hidden' name='custom_payment' id='custom_payment' value='custom_payment' />";
							}

						?>

					<input type='hidden' name='profile_key' value="<?php echo $this->profile_key; ?>"/>
					<input type="hidden" name="extn" value="<?php if(@$this->data['extn']!=""){echo @$this->data['extn'];}else{ echo $extn; } ?>"/>
					<input type="hidden" name="option" value="com_ccidealplatform" />
					<input type="hidden" name="task" value="mollie_process" />

				</form>
			</center>
			
			<script>										
				this.idealform.submit();
			</script>
		<?php
		}		
		?>
	</div>