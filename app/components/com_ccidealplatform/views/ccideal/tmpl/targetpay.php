<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

$jversion 			= new JVersion();
$current_version 	= $jversion->getShortVersion();
$jversion			= substr($current_version,0,1);

$user =  JFactory::getUser();
@$user_type = $user->get( 'usertype' );
@$user_id   = $user->get('id');
$amt=@ JRequest::getVar('amt');
$cont_extn=@ JRequest::getVar('cont_extn');

//Check the language type
$lang = JFactory::getLanguage();
$extension = 'com_ccidealplatform';
$base_dir = JPATH_SITE;
$language_tag = $lang->getTag();
$tag = explode("-",$language_tag);

$extn=@ JRequest::getVar('com');
$orderid=@ JRequest::getVar('orderid');

	if($tag[0]=='nl'){
		$targetLang = 'nl';
	}else{
		$targetLang = 'en';
	}

	if(@$this->data['grandtotal']!=""){
		$min_trans= $this->data['grandtotal'];
	}else{
		$min_trans= $amt;
	}

	if($cont_extn){
		$orderid = $cont_extn;
	}else{
		$orderid = $orderid;
	}

	if(@$this->retry){
		echo $this->fail;
	}

	if(empty($this->payment_desc_orderid)){
		$this->payment_desc_orderid = $this->targetpay_desc;
	}
	$count_desc = explode(" ",$this->payment_desc_orderid );
	if(count($count_desc)==2){
		$this->payment_desc_orderid = $this->payment_desc_orderid.$orderid;
	}

	$ideal_paymentmethod = JRequest::getVar( 'ideal_paymentmethod', 'ideal');

?>
<div class="akeeba-bootstrap">
<?php	
	if($this->ideal_enable == 1 || $this->ideal_enable == 2){
		$transaction_id=JRequest::getVar('transaction_id');

		if($min_trans <0.84 && $transaction_id==""){ ?>

			<div class="red_message_box">
				<img src="<?php echo JURI::root(); ?>/components/com_ccidealplatform/assets/images/cross.png" width="16" height="16" class="ccmargin_RedboxFloat" align="top" /><?php
				echo JTEXT::_('MIN_TRANS2')."</div>";
		} ?>

		<center>
			<form name="idealform" action="index.php" method="post" target="_self" class="idealform">
				<?php if($min_trans <0.84){ ?>
					<input type="hidden" name="amount" value="0.84" />
				<?php }else{?>
					<input type="hidden" name="amount" value="<?php echo $min_trans; ?>" />
				<?php } ?>

				<input type="hidden" name="item_number" value="<?php if(@$this->data['ordernumber']!=""){echo @$this->data['ordernumber'];}else{echo $orderid; } ?>"/>
				<input type='hidden' name='description' value="<?php echo $this->payment_desc_orderid; ?>"/>
				<input type='hidden' name='title' value="<?php echo $this->payment_desc; ?>"/>

				<?php
					if($ideal_paymentmethod == "ideal"){
				?>
						<br>
						<div class="row-fluid">
							<div class="span2 text-center">
								<img src="components/com_ccidealplatform/assets/images/iDEAL.gif" style="height: 80px;">
							</div>
							<div class="span10 text-center">
								<h1><?php echo sprintf(JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_TITLE' ), $ideal_paymentmethod); ?></h1>
							</div>
						</div><br>
						<p>
							<?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_DESC' ); ?>
						</p><br>

						<div class="well well-small">
							<b><?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_TOTAL' ); ?></b> <?php echo "&#8364;".$min_trans; ?><br>
							<b><?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_ORDERNUMBER' ); ?></b> <?php if(@$this->data['ordernumber']!=""){echo @$this->data['ordernumber'];}else{echo $orderid; } ?>
						</div>

						<div class="row-fluid">
							<div class="span6">
								<select name="bank" class="bform_select">
									<?php
										$issuers    	 = JFactory::getXML("http://www.targetpay.com/ideal/getissuers.php?format=xml");

										echo "<option value=''>".JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_OPTION1' )."</option>";

										foreach ($issuers as $issuer) {
										   echo "<option value='".$issuer["id"]."'>".$issuer[0]."</option>";
										}
									?>
								</select>
							</div>
							<div class="span6">
								<a href="javascript:void(0);" class="btn btn-danger btn-large btn-block" onclick="submitBank()">
									<?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_CONTINUE' ); ?>&nbsp;&nbsp;<em class="icon-chevron-right icon-white"></em>
								</a>
							</div>
						</div>
						<br class="clear" />
				<?php
					}elseif($ideal_paymentmethod == "sofort"){

				?>
						<br>
						<div class="row-fluid">
							<div class="span2 text-center">
								<img src="components/com_ccidealplatform/assets/images/sofort.png" style="height: 80px;">
							</div>
							<div class="span10 text-center">
								<h1><?php echo sprintf(JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_TITLE' ), $ideal_paymentmethod); ?></h1>
							</div>
						</div><br>
						<p>
							<?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_DESC' ); ?>
						</p><br>

						<div class="well well-small">
							<b><?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_TOTAL' ); ?></b> <?php echo "&#8364;".$min_trans; ?><br>
							<b><?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_ORDERNUMBER' ); ?></b> <?php if(@$this->data['ordernumber']!=""){echo @$this->data['ordernumber'];}else{echo $orderid; } ?>
						</div>

						<div class="row-fluid">
							<div class="span12">
								<select name="sofort_country" class="span6" style="height: 45px;float:left;">
									<?php
										if($targetLang == 'nl'){
											echo '<option value="">Selecteer land</option>';
											echo '<option value="32">BelgiÃ«</option>';
											echo '<option value="41">Zwitserland</option>';
											echo '<option value="43">Oostenrijk</option>';
											echo '<option value="49">Duitsland</option>';

										}else{
											echo '<option value="">Select country</option>';
											echo '<option value="32">Belgium</option>';
											echo '<option value="41">Switzerland</option>';
											echo '<option value="43">Austria</option>';
											echo '<option value="49">Germany</option>';
										}
									?>
								</select>
								<div class="span6">
									<a href="javascript:void(0);" class="btn btn-danger btn-large btn-block" onclick="submitsofortBank()">
										<?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_CONTINUE' ); ?>&nbsp;&nbsp;<em class="icon-chevron-right icon-white"></em>
									</a>
								</div>
							</div>							
						</div>
						<br class="clear" />
				<?php
					}

					//Retry payment
					if(@$this->retry){
						?><input type="hidden" name="retry" value="<?php echo  "1";  ?>"/><?php
					}

					//For extension name
					if(@$this->data['extn']!=""){$extn= $this->data['extn'];}else{ $extn; }

					//For Custom rsmembership
					if(@$extn=="rsmembership"){
						$custom=@ JRequest::getVar('custom');
						?><input type="hidden" name="custom" value="<?php echo  $custom;  ?>"/><?php
					}
					//For virtuemart
					if(@$extn=="virtuemart"){
						$virtue_sesn=@ JRequest::getVar('iDEALVMPayment_SESSION');
						?><input type="hidden" name="iDEALVMPayment_SESSION" value="<?php echo  $virtue_sesn;  ?>"/><?php
					}
					//For Custom payment
					if(@$this->custom_payment){
						echo "<input type='hidden' name='custom_payment' id='custom_payment' value='custom_payment' />";
					}
				?>

				<input type="hidden" name="extn" value="<?php if(@$this->data['extn']!=""){echo @$this->data['extn'];}else{ echo $extn; } ?>"/>
				<input type="hidden" name="option" value="com_ccidealplatform" />
				<input type="hidden" name="task" value="target_process" />
				<input type="hidden" name="ideal_paymentmethod" value="<?php echo $ideal_paymentmethod; ?>" />
			</form>
		</center>
		<br style="clear:both;">

		<?php

		if($ideal_paymentmethod == "mistercash"){
			?>
				<script>
					this.idealform.submit();
				</script>
			<?php
		}
	}
	?>
</div>