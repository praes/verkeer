<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>  
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license. 
**/
// no direct access
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );

$statusURL	= @$_GET['url'];
$order_id	= @$_GET['orderID'];
$component 	= JRequest::getVar('extn');
$ccItemID	= $_SESSION['CC_MenuItemID'];

$transaction_id = "";
$custom="0";
$db = JFactory :: getDBO();

	if(empty($getMyItemID)){
		$cciItemID = $ccItemID;
	}

	if(empty($statusURL)){
		$statusURL = JRequest::getVar('url');
	}

	if(empty($order_id)){
		$order_id  = JRequest::getVar('orderID');
	}
	
	$order_number = $order_id;	
	
	if($statusURL == 'urlCancel'){
		$result = "cancelled";
		
		$query = "UPDATE #__ccidealplatform_payments SET payment_status = 'cancelled' WHERE order_id = '" . $order_number . "'";
		$db->setQuery($query);
		$db->Query();

		$queryLog = "UPDATE #__ccidealplatform_logs SET payment_status = 'cancelled' WHERE purchaseID = '" . $order_number . "'";
		$db->setQuery($queryLog);
		$db->Query();
		
	}elseif($statusURL == 'urlSuccess'){
		$result = "pending";
	}elseif($statusURL == 'urlError'){
		$result = "failed";
	}	
	
	//Support for TEST FORM
		if ($component == 'testform1' || $component == 'testform2' || $component == 'testform3' || $component == 'testform4' || $component == 'testform5' || $component == 'testform6' || $component == 'testform7' || $component == 'testpayment') {						
			$statuslayout = ccIdealplatformController::statuslayout($result);
			
			return $statuslayout;
		}
		
	//Support for Content_plugin
		if ($component == 'Content_plugin') {			
			$statuslayout = ccIdealplatformController::statuslayout($result);
			
			return $statuslayout;
		}
		
	//Support for Akeeba subscriptions
		if ($component == 'akeebasubs') {		
			ccIdealplatformModelccideal::akeeba_return($order_number,$result);
		}
		
	//Support for hikashop	
		if ($component == 'hikashop') {		
			ccIdealplatformModelccideal::hikashop_return($order_number,$result);
		}

	//Support for rsformpro
		if ($component == 'rsformpro') {
			ccIdealplatformModelccideal::rsformpro_return($order_number,$result);			
		}
		
	//Support for RSEvents Pro
		if($component=="rsepro"){			
			ccIdealplatformModelccideal::rsepro_return($order_number,$result);
		}
	
	//Support for ticketmaster	
		if($component=="ticketmaster"){	
			ccIdealplatformModelccideal::ticketmaster_return($order_number,$result);			
		}
		
	//Support for rsmembership
		if ($component == 'rsmembership') {		
			ccIdealplatformModelccideal::rsmembership_return($order_number,$result,$custom);			
		}
		
	//Support for JoomISP
		if ($component == 'joomisp') {		
			ccIdealplatformModelccideal::joomisp_return($order_number,$result);
		}
		
	//Support for Virtuemart
		if ($component == 'virtuemart') {			
			ccIdealplatformModelccideal::virtuemart_return($order_number,$result);
		}
		
	//Support for redshop
		if ($component == 'redshop') {
			@ccIdealplatformModelccideal::redshop_return($order_number,$result);			
		}
		
	//Support for SobiPro
		if($component=="sobipro"){		
			ccIdealplatformModelccideal::sobipro_return($order_number,$result);
		}
		
	//Support for caddy
		if ($component == 'caddy') {
			ccIdealplatformModelccideal::caddy_return($order_number,$result);
		}
		
	//Support for ccInvoices
		if ($component == 'ccinvoices') {
			ccIdealplatformModelccideal::ccinvoices_return($order_number,$result);
		}
	//Support for RSDirectory
		if ($component == 'rsdirectory') {		
			ccIdealplatformModelccideal::rsdirectory_return($order_number,$result);			
		}
	//Support for J2Store
		if ($component == 'j2store') {		
			ccIdealplatformModelccideal::j2store_return($order_number,$result);			
		}
	//Support for techjoomla
		if ($component == 'techjoomla') {		
			ccIdealplatformModelccideal::techjoomla_return($order_number,$result);			
		}
	
?>