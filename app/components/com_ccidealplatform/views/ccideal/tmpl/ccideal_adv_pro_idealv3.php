<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.  
**/

defined('_JEXEC') or die('Restricted access');

$jversion 			= new JVersion();
$current_version 	= $jversion->getShortVersion();
$jversion			= substr($current_version,0,1);

?>
<div class="chillcreations-bootstrap">
<?php

	$errorCode="";
	// Set default timezone for DATE/TIME functions
	if(function_exists('date_default_timezone_set'))
	{
		date_default_timezone_set('Europe/Amsterdam');
	}

	// Include settings & library
	require_once (JPATH_SITE . '/components/com_ccidealplatform/ccideal_adv_pro_idealv3/settings.php');
	require_once (JPATH_SITE . '/components/com_ccidealplatform/ccideal_adv_pro_idealv3/idealprofessional.cls.php');



	// Setup issuer request
	$oIssuerRequest = new IssuerRequest();
	$oIssuerRequest->setSecurePath($aSettings['CERTIFICATE_PATH']);
	$oIssuerRequest->setCachePath($aSettings['TEMP_PATH']);
	$oIssuerRequest->setPrivateKey($aSettings['PRIVATE_KEY_PASS'], $aSettings['PRIVATE_KEY_FILE'], $aSettings['PRIVATE_CERTIFICATE_FILE']);
	$oIssuerRequest->setMerchant($aSettings['MERCHANT_ID'], $aSettings['SUB_ID']);
	$oIssuerRequest->setAcquirer($aSettings['ACQUIRER'], $aSettings['TEST_MODE']);

	// Request issuer list (List of banks)
	$sIssuerOptions = '';
	$aIssuerList = $oIssuerRequest->doRequest();

	// Any errors in request?
	if($oIssuerRequest->hasErrors())
	{
		$getErrorReport 	= $oIssuerRequest->getErrors();
		 echo '<div class="red_message_box">';
			 echo '<img src="'.JURI::root().'/components/com_ccidealplatform/assets/images/cross.png" width="16" height="16" align="top" />';
				foreach($getErrorReport as $showError){
					echo "<span style='padding:0px 0px 0px 7px;'>".JTEXT::_('CCIDEALPLATFORM_IDEALERROR').$showError['desc']."</span>";
					echo "<br/>";
					echo JTEXT::_('CCIDEALPLATFORM_ERROCODE').$showError['code'];
					$errorCode = 1;
				}
			echo '</div>';
		//print_r($oIssuerRequest->getErrors());

	}
	else
	{
		foreach($aIssuerList as $k => $v)
		{
			$sIssuerOptions .= '<option value="' . $k . '">' . htmlentities($v) . '</option>';
		}
	}

	// Create random order data
	$sOrderId = 'WEB-' . rand(1000000, 9999999); // Random order ID
	$sOrderDescription = 'My Order Description'; // Upto 32 characters
	$fOrderAmount = round(rand(100, 25000) / 100, 2); // Random amount, 2 decimals (1.00 to 250.00)

	$document =  JFactory::getDocument();
	$js = JURI::base().'components/com_ccidealplatform/assets/js/ccvalidate.js';
	$document->addScript($js);
	$user = JFactory::getUser();
	$user_type = $user->get( 'usertype' );
	$user_id   = $user->get('id');

?>
	<center>
		<form name="idealform" action="index.php" method="post" target="_self" class="idealform">
			<input type="hidden" name="grandtotal" value="<?php echo $this->data['grandtotal']; ?>" />
			<input type="hidden" name="ordernumber" value="<?php echo $this->data['ordernumber']; ?>"/>
			<input type="hidden" id="issuerID" name="issuerID" value="<?php if (empty ($_SESSION["issuerID"])) { echo @$_SESSION["issuerID"]; }?>"/>
			<input type="hidden" name="ec" value="<?php echo $this->data['ordernumber']; ?>"/>
			
			<?php
				if($errorCode==1){
					 require_once (JPATH_SITE . '/components/com_ccidealplatform/ccideal_adv_pro_idealv3/DirReqIdeal.php');
				}else{
			?>			
					<div class="row-fluid">
						<div class="span2 text-center">
							<img src="components/com_ccidealplatform/assets/images/iDEAL.gif" style="height: 80px;">
						</div>
						<div class="span10 text-center">
							<h1><?php echo sprintf(JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_TITLE' ), 'iDEAL'); ?></h1>
						</div>
					</div><br>
					<p>
						<?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_DESC' ); ?>
					</p><br>

					<div class="well well-small">
						<b><?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_TOTAL' ); ?></b> <?php echo "&#8364;".$this->data['grandtotal'];; ?><br>
						<b><?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_ORDERNUMBER' ); ?></b> <?php if(@$this->data['ordernumber']!=""){echo @$this->data['ordernumber'];} ?>
					</div>
					
					<div class="row-fluid">
						<div class="span6">
							<?php 
								if (empty ($_SESSION["issuerID"])) {
									require_once (JPATH_SITE . '/components/com_ccidealplatform/ccideal_adv_pro_idealv3/DirReqIdeal.php');
								}								
							?>
						</div>
						<div class="span6">
							<a href="javascript:void(0);" class="btn btn-danger btn-large btn-block" onclick="submitBank()">
								<?php echo JText::_( 'CCIDEAL_BANKFORM_SELECT_BANK_CONTINUE' ); ?>&nbsp;&nbsp;<em class="icon-chevron-right icon-white"></em>
							</a>
						</div>
					</div>
					<br class="clear" />					
			<?php 							
				} 
			?>
			
			<input type="hidden" name="extn" value="<?php echo $this->data['extn']; ?>"/>
			<input type="hidden" name="option" value="com_ccidealplatform" />
			<input type="hidden" name="task" value="checkccideal" />

		</form>
	</center>
</div>
<br class="clear" />	