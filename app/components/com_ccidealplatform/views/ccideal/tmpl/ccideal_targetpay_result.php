<?php

/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details. 
* @license	GNU/GPL, see LICENSE.php for full license.  
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

require_once( JPATH_COMPONENT .DS.'ccideal_targetpay'.DS.'targetpayideal.class.php' );

$mainframe =  JFactory::getApplication('administrator');
$app =  JFactory::getApplication();
$db = JFactory :: getDBO();

$order_number 	= JRequest :: getVar('orderid');
$tmp_orderID	= $order_number;
$transaction_id	= JRequest :: getVar('trxid');
$ec				= JRequest :: getVar('ec');
@$custom_payment = JRequest :: getVar('custom_payment');
$ccItemID		=  $_SESSION['CC_MenuItemID'];

//Get the Payment details
$query1 = "SELECT * FROM #__ccidealplatform_payments WHERE `order_id`='$order_number'";
$db->setQuery($query1);
$payment_data = $db->loadObject();

$component = $payment_data->extension;

	if(!empty($custom_payment)){
		$custom_payment = JRequest :: getVar('custom_payment');
	}
	
	$query = "SELECT * FROM #__ccidealplatform_config LIMIT 0,1";
	$db->setQuery($query);
	$conf_data = $db->loadAssoc();
	
	$trans = "UPDATE #__ccidealplatform_payments SET trans_id='$transaction_id' WHERE order_id ='$order_number' ";
	$db->setQuery($trans);
	$db->Query();
	
	$query = "SELECT payment_status FROM #__ccidealplatform_payments WHERE order_id='" . $order_number . "'";
	$db->setQuery($query);
	$result = $db->loadResult();
	
//Support for TEST FORM
	if ($component == 'testform1' || $component == 'testform2' || $component == 'testform3' || $component == 'testform4' || $component == 'testform5' || $component == 'testform6' || $component == 'testform7' || $component == 'testpayment') {						
		$statuslayout = ccIdealplatformController::statuslayout($result);
		
		return $statuslayout;
	}
	
//Support for Content_plugin
	if ($component == 'Content_plugin') {			
		$statuslayout = ccIdealplatformController::statuslayout($result);
		
		return $statuslayout;
	}
	
//Support for Akeeba subscriptions
	if ($component == 'akeebasubs') {		
		ccIdealplatformModelccideal::akeeba_return($order_number,$result);
	}
	
//Support for hikashop	
	if ($component == 'hikashop') {		
		ccIdealplatformModelccideal::hikashop_return($order_number,$result);
	}

//Support for rsformpro
	if ($component == 'rsformpro') {		
		ccIdealplatformModelccideal::rsformpro_return($order_number,$result);			
	}
	
//Support for RSEvents Pro
	if($component=="rsepro"){			
		ccIdealplatformModelccideal::rsepro_return($order_number,$result);
	}

//Support for ticketmaster	
	if($component=="ticketmaster"){	
		ccIdealplatformModelccideal::ticketmaster_return($order_number,$result);			
	}
	
//Support for rsmembership
	if ($component == 'rsmembership') {		
		ccIdealplatformModelccideal::rsmembership_return($order_number,$result);			
	}
	
//Support for JoomISP
	if ($component == 'joomisp') {		
		ccIdealplatformModelccideal::joomisp_return($order_number,$result);
	}
	
//Support for Virtuemart
	if ($component == 'virtuemart') {			
		ccIdealplatformModelccideal::virtuemart_return($order_number,$result);	
	}
	
//Support for redshop
	if ($component == 'redshop') {
		ccIdealplatformModelccideal::redshop_return($order_number,$result);			
	}
	
//Support for SobiPro
	if($component=="sobipro"){		
		ccIdealplatformModelccideal::sobipro_return($order_number,$result);
	}
	
//Support for caddy
	if ($component == 'caddy') {
		ccIdealplatformModelccideal::caddy_return($order_number,$result);
	}
	
//Support for ccInvoices
	if ($component == 'ccinvoices') {
		ccIdealplatformModelccideal::ccinvoices_return($order_number,$result);
	}
//Support for RSDirectory
	if ($component == 'rsdirectory') {		
		ccIdealplatformModelccideal::rsdirectory_return($order_number,$result);			
	}
?>