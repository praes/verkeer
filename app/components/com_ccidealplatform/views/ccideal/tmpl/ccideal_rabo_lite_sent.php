<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license. 
**/


// no direct access
defined('_JEXEC') or die('Restricted access');

@$getPost = $this->getPostData;
@$articleDesc = $getPost['cont_extn'];

	if($this->ideal_enable == 1 || $this->ideal_enable == 2){

		/* Resultaten verwerken */
		$order_total = (100 * $this->order_total);
		$order_currency = $this->order_currency;

		/* Configuratie laden */
		### is gelijk aan 'Sleutel', zie uw dashboard onder tabblad "Certificaat uploaden"
		$key = $this->IDEAL_PrivatekeyPass;

		$merchantID =   $this->IDEAL_MerchantID ;

		### subID niet wijzigen ###
		$subID = "0";

		### bedrag moet geconverteerd worden naar eurocenten ###		
		$paymentType =$this->IDEAL_Description;

		### De geldigheid van de aanbieding zetten we in dit voorbeeld op een week ###
		$validUntil = date('Y-m-d\TH-i-s\Z', time()+3600);

		### Toon de bestellijst ###
		$orderlist = '';
		$partOfSha = '';

		$i=1;
		
		if(isset($order_id) && $statusURL == 'urlCancel'){
			$itemNumber=1;
			$itemDescription='Order_details';
			$orderlist .=
			'<INPUT type="hidden" NAME="itemNumber'.$i.'" VALUE="'.$order_id.'">
			<INPUT type="hidden" NAME="itemDescription'.$i.'" VALUE="'.htmlspecialchars($this->payment_desc_orderid ).'">
			<INPUT type="hidden" NAME="itemQuantity'.$i.'" VALUE="1">
			<INPUT type="hidden" NAME="itemPrice'.$i.'" VALUE="'.$this->order_total * 100 .'">
			';

			### bereken alvast een deel van de SHA string ###
 			$partOfSha = $partOfSha . $order_id . htmlspecialchars($this->payment_desc_orderid) .'1' . ($this->order_total * 100 );
		}
		$i++;

		/*********************************************************************************/

		if(!isset($statusURL)  && $statusURL != 'urlCancel'){
			$itemNumber=1;
			$itemDescription='Order_details';
			$post_variables["itemNumber1"] = '1';
			$post_variables["itemDescription1"] = $itemDescription;
			$post_variables["itemQuantity1"] = 1;
			$post_variables["itemPrice1"] = $order_total ;
			$partOfSha = $partOfSha . $itemNumber . $itemDescription . '1' . $order_total;
		}

		### bouw de String op waarover een SHA1 moet worden berekend ###
		$shastring = "$key" . "$merchantID" . "$subID" . "$order_total" . "$this->orderid" . $paymentType . "$validUntil" . $partOfSha;

		###speciale HTML entiteiten verwijderen:
		$clean_shaString = HTML_entity_decode($shastring);

		### De tekens "\t", "\n", "\r", " " (spaties) mogen niet voorkomen in de string

		$not_allowed = array("\t", "\n", "\r", " ");
		$clean_shaString = str_replace($not_allowed, "",$clean_shaString);

		$clean_shaString = str_replace("&amp;", "&",$clean_shaString);
		$clean_shaString = str_replace("&gt;", ">",$clean_shaString);
		$clean_shaString = str_replace("&lt;", "<",$clean_shaString);
		$clean_shaString = str_replace("&quot;", "\"",$clean_shaString);

		$shasign = sha1($clean_shaString);

		if(isset($statusURL)  && $statusURL == 'urlCancel') {
			?>
			<!-- Formulier voor naar de Rabobank Server -->

			<FORM METHOD="post" ACTION="<?php echo  $this->IDEAL_AcquirerURL; ?>" name="betalen">

				<INPUT type="hidden" NAME="merchantID" value="<?php echo $this->IDEAL_MerchantID; ?>" />
				<INPUT type="hidden" NAME="subID" value="<?php print $subID ?>" />
				<INPUT type="hidden" NAME="amount" VALUE="<?php echo $order_total; ?>" />
				<INPUT type="hidden" NAME="purchaseID" VALUE="<?php echo $this->orderid; ?>" />
				<INPUT type="hidden" NAME="language" VALUE="<?php echo 'nl'; ?>" />
				<INPUT type="hidden" NAME="currency" VALUE="<?php echo $order_currency; ?>" />
				<INPUT type="hidden" NAME="description" VALUE="<?php echo $this->payment_desc_orderid; ?>" />
				<INPUT type="hidden" NAME="hash" size="50" VALUE="<?php print $shasign ?>" />
				<INPUT type="hidden" NAME="paymentType" VALUE="<?php echo $paymentType; ?>" />
				<INPUT type="hidden" NAME="validUntil" VALUE="<?php print $validUntil ?>" />

			<?php
				echo $orderlist;
			?>

			<INPUT type="hidden" NAME="urlCancel" VALUE="<?php echo $this->IDEAL_MerchantReturnURL. '&amp;url=urlCancel&amp;orderID=' . $this->orderid.'&extn='.$getPost['extn'].'&amount='.$getPost['grandtotal']; ?>" />
			<INPUT type="hidden" NAME="urlSuccess" VALUE="<?php echo $this->IDEAL_MerchantReturnURL. '&amp;url=urlSuccess&amp;orderID=' . $this->orderid.'&extn='.$getPost['extn'].'&amount='.$getPost['grandtotal']; ?>" />
			<INPUT type="hidden" NAME="urlError" VALUE="<?php echo $this->IDEAL_MerchantReturnURL. '&amp;url=urlError&amp;orderID=' . $this->orderid.'&extn='.$getPost['extn'].'&amount='.$getPost['grandtotal']; ?>" />

			<?php
				echo JText::_('ID_CLICK_PAYMENT');
				echo '<br /><br />';
				echo '<a href="javascript:document.betalen.submit()"><img src="' . JURI::root() .'/components/com_ccidealplatform/assets/images/ccideal_thanks.jpg" border="0" /></a>';
				echo '<br/>';
			?>
			</form>
			<?php
		
		}else {

			$url = $this->IDEAL_AcquirerURL;
			$post_variables["merchantID"] = $this->IDEAL_MerchantID;
			$post_variables["subID"] = $subID;
			$post_variables["amount"] = $this->order_total*100;
			$post_variables["purchaseID"] = $this->orderid;
			$post_variables["language"] = 'nl';
			$post_variables["currency"] = $order_currency;
			$post_variables["description"] = $this->payment_desc_orderid;
			$post_variables["hash"] = $shasign;
			$post_variables["paymentType"] = $paymentType;
			$post_variables["validUntil"] = $validUntil;
			$post_variables["urlCancel"] = $this->IDEAL_MerchantReturnURL. '&amp;url=urlCancel&amp;orderID=' . $this->orderid.'&extn='.$getPost['extn'].'&amount='.$getPost['grandtotal'];
			$post_variables["urlSuccess"] = $this->IDEAL_MerchantReturnURL. '&amp;url=urlSuccess&amp;orderID=' . $this->orderid.'&extn='.$getPost['extn'].'&amount='.$getPost['grandtotal'];
			$post_variables["urlError"] = $this->IDEAL_MerchantReturnURL. '&amp;url=urlError&amp;orderID=' . $this->orderid.'&extn='.$getPost['extn'].'&amount='.$getPost['grandtotal'];

			$query_string = "?";

			foreach( $post_variables as $name => $value ) {
				$query_string .= $name. "=" . urlencode($value) ."&";
			}			
				
			$app = JFactory::getApplication();
			$app->redirect( $url . $query_string );
		}	
	}
?>