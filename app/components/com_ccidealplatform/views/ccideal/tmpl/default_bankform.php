<?php

/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details. 
* @license	GNU/GPL, see LICENSE.php for full license.     
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

if($this->ideal_enable == 2 || $this->ideal_enable == 0){
	echo '<div class="akeeba-bootstrap"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>'.JTEXT::_("NOT_POSSIBLE").'</div></div>';	
}
