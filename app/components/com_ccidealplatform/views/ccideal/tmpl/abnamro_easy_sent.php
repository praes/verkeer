<?php
/**
* @package	cciDEAL Platform 
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

@$getPost = $this->getPostData;	
	
	if($this->ideal_enable == 1 || $this->ideal_enable == 2){
		if(isset($order_id) && $statusURL == 'urlCancel'){

			$orderID = $order_id;
			$time =  time();
			$tv = date('y', $time) % 10;
			$orderID = $orderID + $tv;
			$tv = (date('m', $time) * 31) + date('d', $time);
			$orderID = $orderID + (($tv < 10) ? '0' : '') + (($tv < 100) ? '0' : '') + $tv;
			$tv = (date('G', $time) * 3600) + (date('i', $time) * 60) + date('s', $time);
			$orderID = $orderID + (($tv < 10) ? '0' : '') + (($tv < 100) ? '0' : '') + (($tv < 1000) ? '0' : '') + (($tv < 10000) ? '0' : '') + $tv;
			$tvplus = round(rand(0,9));

			$new_orderid = $orderID + (($tvplus + 1) % 10); ?>

			<FORM METHOD="post" ACTION="<?php echo $this->IDEAL_AcquirerURL; ?>" name="betalen">

				<INPUT type="hidden" NAME="PSPID" value="<?php echo $this->pspID; ?>" />
				<INPUT type="hidden" NAME="amount" VALUE="<?php  $amount = $this->order_total * 100; echo $amount; ?>" />
				<INPUT type="hidden" NAME="orderID" value="<?php echo $new_orderid; ?>" />
				<INPUT type="hidden" NAME="language" VALUE="NL_NL" />
				<INPUT type="hidden" NAME="currency" VALUE="<?php echo $this->order_currency; ?>" />
				<INPUT type="hidden" NAME="PM" VALUE="iDEAL" />
				<INPUT type="hidden" NAME="COM" VALUE="<?php echo $this->payment_desc_orderid; ?>" />
				<INPUT type="hidden" NAME="accepturl" VALUE="<?php echo $this->IDEAL_MerchantReturnURL. '&amp;url=urlSuccess&orderID='.$this->new_orderid.'&extn='.$getPost['extn'].'&amount='.$getPost['grandtotal'];?>">
				<INPUT TYPE="hidden" NAME="declineurl" VALUE="<?php echo $this->IDEAL_MerchantReturnURL. '&amp;url=urlCancel&amp;orderID=' . $this->new_orderid.'&extn='.$getPost['extn'].'&amount='.$getPost['grandtotal']; ?>">
				<INPUT TYPE="hidden" NAME="exceptionurl" VALUE="<?php echo $this->IDEAL_MerchantReturnURL. '&amp;url=urlError';?>">
				<INPUT TYPE="hidden" NAME="cancelurl" VALUE="<?php echo $this->IDEAL_MerchantReturnURL. '&amp;url=urlCancel&amp;orderID=' . $this->new_orderid.'&extn='.$getPost['extn'].'&amount='.$getPost['grandtotal']; ?>">

				<?php
					echo JText::_('ID_CLICK_PAYMENT');
					echo '<br /><br />';
					echo '<a href="javascript:document.betalen.submit()"><img src="' . JURI::root() .'/components/com_ccidealplatform/assets/images/ccideal_thanks.jpg" border="0" /></a>';
					echo '<br/>';
				?>
			</form>
			<?php
		}else{		
		?>

			<form METHOD="post" ACTION="<?php echo $this->IDEAL_AcquirerURL; ?>" name="betalen" id="abnamro">

				<input type="hidden" NAME="PSPID" value="<?php echo $this->pspID; ?>" />
				<input type="hidden" NAME="amount" VALUE="<?php $amount = $this->order_total*100; echo $amount; ?>" />
				<input type="hidden" NAME="orderID" value="<?php echo $this->new_orderid; ?>" />
				<input type="hidden" NAME="language" VALUE="NL_NL" />
				<input type="hidden" NAME="currency" VALUE="<?php echo $this->order_currency; ?>" />
				<input type="hidden" NAME="PM" VALUE="iDEAL" />
				<input type="hidden" NAME="COM" VALUE="<?php echo $this->payment_desc_orderid; ?>" />
				<input type="hidden" NAME="accepturl" VALUE="<?php echo $this->IDEAL_MerchantReturnURL. '&amp;url=urlSuccess&orderID='.$this->new_orderid.'&extn='.$getPost['extn'].'&amount='.$getPost['grandtotal'];?>">
				<input TYPE="hidden" NAME="declineurl" VALUE="<?php echo $this->IDEAL_MerchantReturnURL. '&amp;url=urlCancel&amp;orderID=' . $this->new_orderid.'&extn='.$getPost['extn'].'&amount='.$getPost['grandtotal']; ?>">
				<input TYPE="hidden" NAME="exceptionurl" VALUE="<?php echo $this->IDEAL_MerchantReturnURL. '&amp;url=urlError';?>">
				<input TYPE="hidden" NAME="cancelurl" VALUE="<?php echo $this->IDEAL_MerchantReturnURL. '&amp;url=urlCancel&amp;orderID=' . $this->new_orderid.'&extn='.$getPost['extn'].'&amount='.$getPost['grandtotal']; ?>">

			</form>
			
			<script type="text/javascript">
				callpayment()
				function callpayment(){
					document.getElementById('abnamro').submit();					
				}
			</script>
			<?php
		}
	}
?>