<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined('_JEXEC') or die;

$task = JRequest::getVar("task");
$extn = JRequest::getVar('extn');

if($task == "idealRedirect"){
	$post=$_POST;
	if(@$post['amt_usr']){
		$post['total'] = str_replace(',','.',$post['total']);
	}
	@$amt=$post['total'];
	$article_id = $post['id'];
	$item_id = $post['itemid'];
	@$cont_extn=$post['cont_extn'];

	@$extra_textfield = JRequest::getVar('extra_textfield');
	@$extra_textfield = strip_tags(trim($extra_textfield));

	// Again checking for Secure process
	if($extra_textfield){
			if(!preg_match('/^[A-Za-z0-9_\s@ .,#!?]+$/', $extra_textfield)){
				?><script>alert("Not a valid data in extra fields!"); window.location="index.php?option=com_content&view=article&id=<?php echo $article_id; ?>&Itemid=<?php echo $item_id; ?>";</script><?php
			}
	}

	if($amt!=""){
		$total=$post['total'];

		$total= iconv("UTF-8", "ISO-8859-1//IGNORE", $total);
	}else{
		$grandtotal=$post['grandtotal'];
		$get_tot=base64_decode($grandtotal);
		$date=date("mYmd");
		$total=str_replace("$date","",$get_tot);
	}
		
	$total = str_replace(',','.',$total);

	if(@$post['ordernumber']==""){
		$ordernumber = $post['hidden_order_number'];

		// Again checking for Secure process
		if($cont_extn==""){
			?><script>alert("You need to fill in a payment identification number!"); window.location="index.php?option=com_content&view=article&id=<?php echo $article_id; ?>&Itemid=<?php echo $item_id; ?>";</script><?php
		}

	}else{

		$ordernumber=$post['ordernumber'];
	}

	// Again checking for Secure process
	if($total==""){

		?><script>alert("You need to fill in an amount to be payed!"); window.location="index.php?option=com_content&view=article&id=<?php echo $article_id; ?>&Itemid=<?php echo $item_id; ?>";</script><?php

	}
	$extn=$post['extn'];
?>

	<form id="idealform" action="index.php?option=com_ccidealplatform&task=bankform" method="post">

		<input type="hidden" name="grandtotal" id="grandtotal" value="<?php echo $total; ?>" />
		<input type="hidden" name="ordernumber" id="ordernumber" value="<?php echo $ordernumber; ?>"/>
		<input type="hidden" name="extn" id="extn" value="<?php echo $extn; ?>"/>
		<?php if($cont_extn!="") { ?>
			<input type="hidden" name="cont_extn" id="cont_extn" value="<?php echo $cont_extn; ?>"/>
			<input type="hidden" name="extra_textfield" id="extra_textfield" value="<?php echo $extra_textfield; ?>"/>
		<?php } ?>

	</form>

	<script type="text/javascript">

		var prodid = document.getElementById('ordernumber').value ;
			if ( prodid) {
				document.getElementById('idealform').submit();
			}
	</script>
	<?php
}

// Redirection for JoomISP
if($extn=="joomisp"){
	
	$db = JFactory::getDBO();

	$total = JRequest::getVar('grandtotal');
	$ordernumber = JRequest::getVar('ordernumber');

	$date=date("mY");
	$ordernumber=str_replace("$date","",$ordernumber);	

	$query = "SELECT * FROM #__isp_orders WHERE id='" . $ordernumber . "'";
	$db->setQuery($query);
	if($db->Query()){
		$result = $db->loadObject();	

		$plan_id 		 = $result->plan_id;
		$my_order_number = $result->id;
		$my_order_total	 = $result->price;			

		if(($my_order_number == $ordernumber) && ($my_order_total==$total)){

			$form ="";
			$form .='<form id="idealform" action="index.php?option=com_ccidealplatform&task=bankform" method="post">';
			$form .='<input type="hidden" name="grandtotal" id="grandtotal" value='.$total.' " />';
			$form .='<input type="hidden" name="ordernumber" id="ordernumber" value='.$ordernumber.' "/>';
			$form .='<input type="hidden" name="extn" id="extn" value="'.$extn.'" />';
			$form .='</form>';

			$form .='<script type="text/javascript">';
			$form .='var prodid = document.getElementById("grandtotal").value ;';
			$form .='if ( prodid) {';
			$form .='document.getElementById("idealform").submit();';
			$form .='}';
			$form .='</script>';

			echo $form;

		}else{
			echo "Sorry..,mismatch account details";
		}
	}
}
?>