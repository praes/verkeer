<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

$user 		= JFactory::getUser();
@$user_type = $user->get( 'usertype' );
@$user_id   = $user->get('id');
$amt		= @JRequest::getVar('amt');

if(@$this->data['grandtotal']!=""){
	$min_trans= $this->data['grandtotal'];
}else{
	$min_trans= $amt;
}

$min_trans 	= str_replace(',','',$min_trans);

$newdts				= JRequest::getVar('newDts','','post');
$languageCode		= $this->getModel()->getLanguageCode();
//$extn				= @JRequest::getVar('com');
$orderid			= @$this->data['ordernumber'];
$min_trans			= $min_trans*100;
$returnURL			= JURI::root().'index.php?option=com_ccidealplatform&task=raboomnikassa_return';
$AutoMaticReturnURL = JURI::root().'index.php?option=com_ccidealplatform&task=raboomnikassa_notify';

$ideal_paymentmethod				= @JRequest::getVar('ideal_paymentmethod');

//IDEAL, MINITIX, VISA, MASTERCARD, MAESTRO, INCASSO, ACCEPTGIRO, REMBOURS

if($ideal_paymentmethod == "creditcards"){
	$paymentmethod = "VISA, MASTERCARD";
}else if($ideal_paymentmethod == "ALL"){
	$paymentmethod = '';
}else{
	$paymentmethod = $ideal_paymentmethod;
}

//default Euro
$currencyCode		= '978';
?>
	<div class="akeeba-bootstrap"><?php

		if((trim($this->conf_data['IDEAL_MerchantID'])=="" OR trim($this->conf_data['IDEAL_SubID'])=="" OR trim($this->conf_data['IDEAL_PrivatekeyPass'])=="") && (trim($this->conf_data['IDEAL_Mode'])!="TEST"))
		{
			?>
			<div class="alert alert-error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo JText::_('NOT_POSSIBLE_PAYMENT'); ?>
			</div>
			<?php
		}else{

			if(trim($this->conf_data['IDEAL_Mode'])=="TEST"){
				
				//cciDEAL 4.2.1(Default test accounts)
				$this->conf_data['IDEAL_MerchantID']		= "002020000000001";
				$this->conf_data['IDEAL_SubID']				= "1";
				$this->conf_data['IDEAL_PrivatekeyPass'] 	= "002020000000001_KEY1";
				
				$action_url = "https://payment-webinit.simu.omnikassa.rabobank.nl/paymentServlet";
			}else{
				$action_url = "https://payment-webinit.omnikassa.rabobank.nl/paymentServlet";
			}

			$Data = 'transactionReference='.$this->transactionReference.'|amount='.$min_trans.'|currencyCode='.$currencyCode.'|merchantId='.$this->conf_data['IDEAL_MerchantID'].'|keyVersion='.$this->conf_data['IDEAL_SubID'].'|normalReturnUrl='.$returnURL.'|customerLanguage='.$languageCode.'|automaticResponseUrl='.$AutoMaticReturnURL.'|orderId='.$orderid.'|paymentMeanBrandList='.$paymentmethod;		

			if($this->ideal_enable == 1 || $this->ideal_enable == 2){
			?>
				<form name="raboomniKassa" method="post" action="<?php echo $action_url; ?>">
					<input type="hidden" name="Data" value="<?php echo $Data; ?>">
					<input type="hidden" name="InterfaceVersion" value="HP_1.0">
					<input type="hidden" name="Seal" value="<?php echo hash('sha256', $Data.$this->conf_data['IDEAL_PrivatekeyPass']); ?>">

					<table width="100%" cellspacing="0" cellpadding="0" id="tableMain" style="<?php if(!isset($newdts['responseCode'])){ echo "display:none;"; }?>">
						<tr>
							<td style="padding:5px;" align="center">
								<table cellspacing="0" cellpadding="0" id="tableMain" border="0">
									<tr>
										<td>
										&nbsp;
											<?php echo JText::_('CCOMNIKASSA_PAYMENTDESC_OMNIKASABANK');?>
											&nbsp;
										</td>
										<td align="right">
											<button type="submit" name="pay_rabobank" id="pay_rabobank" class="ccmlimage"><?php echo JText::_('CCOMNIKASSA_PAYNOW_DESC');?></button>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</form>

				<script type="text/javascript">
					document.raboomniKassa.submit();
				</script>
				<?php
			}			
		}
	?>
	</div>