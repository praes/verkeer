<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class cciDealPlatformViewccideal extends JViewLegacy {

		function display($tpl = null) {

			$db = JFactory :: getDBO();
			@$post = JRequest ::get('post');

			if(empty($post)){
				//In case,the variable is not fetch,then we can use $_GET.
 	 		    @$order_id 				= $_GET['ordernumber'];
		 		@$total					= $_GET['grandtotal'];
		 		@$extn					= $_GET['extn'];
		 		@$custom_payment		= $_GET['custom_payment'];
                @$payment_custom		= $_GET['payment_custom'];
                @$ccideal_description	= $_GET['ccideal_description'];
			}

			$data = array();

				$jversion = new JVersion();
				$current_version = $jversion->getShortVersion();
				$jversion=substr($current_version,0,3);

			if($post){

				if(@$post['ordernumber']){
					$order_id = $post['ordernumber'];
				}else{
					$order_id = JRequest ::getVar('orderid','');
					$data['ordernumber'] = $order_id;
				}

				@$order_id				= $post['ordernumber'];
                @$extn		            = $post['extn'];
				@$custom_payment		= $post['custom_payment'];
                @$payment_custom		= $post['payment_custom'];
                @$ccideal_description	= $post['ccideal_description'];
				$data = $post;

			}else{

				$order_id = JRequest ::getVar('orderid','');

				if(!$order_id){
					$order_id = JRequest ::getVar('orderID','');
				}

				$extn = JRequest ::getVar('com','');

				if(!$extn){
					$extn = JRequest ::getVar('extn','');
				}
				@$custom_payment		= JRequest ::getVar('custom_payment');
                @$payment_custom		= JRequest ::getVar('payment_custom');
                @$ccideal_description	= JRequest ::getVar('ccideal_description');

				if($order_id =="" || $extn==""){
					//In case,the variable is not fetch,then we can use $_GET.
					@$order_id 	= $_GET['ordernumber'];
					@$extn		= $_GET['extn'];
				}

				$data['ordernumber'] = $order_id;
				$data['custom_payment'] = @$custom_payment;
                $data['payment_custom'] = @$payment_custom;
                $data['ccideal_description'] = @$ccideal_description;
				$data['extn'] = $extn;
				$data['custom'] ="0";
				$data['custom']=@ JRequest::getVar('custom');

  				$query = "SELECT payment_total FROM #__ccidealplatform_payments WHERE order_id ='".$order_id."' AND extension = '".$data['extn']."'";
				$db->setQuery($query);
	 			$data['grandtotal'] = $db->loadResult();

			}

			$this->assignRef('custom_payment', $custom_payment);
            $this->assignRef('payment_custom', $payment_custom);
            $this->assignRef('ccideal_description', $ccideal_description);

			$query = "SELECT * FROM #__ccidealplatform_config";
			$db->setQuery($query);
			$ideal_config = $db->loadObject();

			if(@$ideal_config){

				$ideal_bank = $ideal_config->IDEAL_Bank;
				$ideal_desc = $ideal_config->IDEAL_Description;
				$ideal_enable=$ideal_config->IDEAL_Enable ;
				$bank_file = "ccideal_" . strtolower($ideal_bank);
			}

			//For content description
			$chk_extn="SELECT * FROM #__ccidealplatform_payments WHERE `order_id` = '$order_id'";
			$db->setQuery($chk_extn);
			$content_desc=$db->loadObject();

			@$ideal_desc_orderid = $ideal_config->IDEAL_Description.": ".$content_desc->extension_id;
			@$ideal_profile_key	 = $ideal_config->IDEAL_mollie_profilekey;

			if($this->_layout == trim('mollie')){

			}			
			if($this->_layout == trim('sisow')){
			
				$post = JRequest::get('post');
				
				
				$this->assignRef('ideal_config', 	$ideal_config);
				$this->assignRef('data', 	$post);
			}

			if($this->_layout == trim('rabo_omnikassa')){
				$query = "SELECT * FROM #__ccidealplatform_config LIMIT 0,1";
				$db->setQuery($query);
				$conf_data = $db->loadAssoc();
				$this->assignRef('conf_data', $conf_data);

				$model = $this->getModel();
				$this->transactionReference = $model->random_generator(20);

					$query = "UPDATE #__ccidealplatform_payments SET trans_id='" . $this->transactionReference . "' WHERE order_id = '" . $order_id . "'";
					$db->setQuery($query);
					$db->query();

			}


			if(empty($post)){
				//In case,the variable is not fetch,then we can use $_GET.
				@$post['extn'] 		= $_GET['extn'];
				@$post['ordernumber']= $_GET['ordernumber'];
				@$post['grandtotal'] = $_GET['grandtotal'];
			}


			//Layout for targetpay
			if($this->_layout == trim('targetpay')){

					$post = JRequest::get('post');
					$retry = JRequest::getVar('retry');
					$orderID = JRequest::getVar('orderID');

						if($retry){

							//Get the configuration details
							$query = "SELECT * FROM #__ccidealplatform_config ";
							$db->setQuery($query);
							$conf_data = $db->loadObject();

							$fail ="";
							$fail .='<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>';
							//$fail .='<img src="'.JURI::root().'components/com_ccidealplatform/assets/images/cross.png" width="16" height="16" class="cross" align="top" />';
							$fail .= $conf_data->IDEAL_Status_Failed ."</div>";

							$this->assignRef('retry', 	$retry);
							$this->assignRef('fail', 	$fail);

						}
					$this->assignRef('data', 	$post);
			}

			//Layout for targetpay result
			if($this->_layout == trim('ccideal_targetpay_result')){
					$post = JRequest::get('post');

					$jversion 			= new JVersion();
					$current_version 	= $jversion->getShortVersion();
					$sht_jversion			= substr($current_version,0,1);

					if($sht_jversion == 3){
						$sucs = "";
						$sucs .="<div class=\"alert alert-success\">";
						$sucs .="<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>";

						$fail ="";
						$fail .="<div class=\"alert alert-error\">";
						$fail .="<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>";

					}else{
						$sucs = "";
						$sucs .="<div class=\"green_message_box\" style=\"text-align:center;\">";
						$sucs .="<img src=\"".JURI::root()."components/com_ccidealplatform/assets/images/tick.png\" alt=\"tick_sssuccess\" align=\"top\"/>&nbsp;";

						$fail ="";
						$fail .='<div class="red_message_box">';
						$fail .='<img src="'.JURI::root().'components/com_ccidealplatform/assets/images/cross.png" width="16" height="16" class="cross" align="top" />';
					}

					$this->assignRef('data', 	$post);
					$this->assignRef('success', $sucs);
					$this->assignRef('fail', 	$fail);
			}

			//Layout for abnamro easy sent
			if($this->_layout == trim('abnamro_easy_sent')){

				$model = & $this->getModel();
			    $Abnamroeasy_bank = $model->getBank_LIST();
			    $Abnamroeasy_orderList = $model->getBank_ORDERLIST();

				if ($Abnamroeasy_bank->IDEAL_Mode == 'TEST') {

					$test_mode = 1;
					$IDEAL_AcquirerURL='https://internetkassa.abnamro.nl/ncol/test/orderstandard.asp';
				}else{

					$IDEAL_AcquirerURL= 'https://internetkassa.abnamro.nl/ncol/prod/orderstandard.asp';
				}

		 		$pspID 			= $Abnamroeasy_bank->IDEAL_ABNAMROEASY_ID;
		 		//$order_total	= $Abnamroeasy_orderList->payment_total;
		 		//$order_id		= $Abnamroeasy_orderList->order_id;
		 		$order_currency	= "EUR";
				$order_total	= $_POST['grandtotal'];
		 		$order_id		= $_POST['ordernumber'];

			 	$merchant_return_url =  JURI::root().'index.php?option=com_ccidealplatform&view=ccideal&layout=abn_amro_easy_result';

				$this->assignRef('IDEAL_AcquirerURL', $IDEAL_AcquirerURL);
				$this->assignRef('pspID', $pspID);
				$this->assignRef('order_total', $order_total);
				$this->assignRef('new_orderid', $order_id);
				$this->assignRef('order_currency', $order_currency);
				$this->assignRef('IDEAL_MerchantReturnURL', $merchant_return_url);
				$this->assignRef('getPostData', $post);

			}

			//Layout for abnamro easy result
			if($this->_layout == trim('abn_amro_easy_result')){

						$query = "SELECT * FROM #__ccidealplatform_config LIMIT 0,1";
						$db->setQuery($query);
						$conf_data = $db->loadAssoc();
						$this->assignRef('conf_data', $conf_data);

						$model = & $this->getModel();
						$Abnamroeasy_bank = $model->getBank_LIST();
						$Abnamroeasy_orderList = $model->getBank_ORDERLIST();

						if($extn == 'virtuemart'){
						
							$jversion 			= new JVersion();
							$current_version 	= $jversion->getShortVersion();
							$jversion_sht		= substr($current_version,0,1);
							
							if($jversion_sht == "3"){
								$plg_vm = "plg_virtuemart3_ideal";
							}else{
								$plg_vm = "plg_virtuemart20_ideal";
							}

							//fetching VM Payment id
							$q = "select virtuemart_paymentmethod_id from #__virtuemart_paymentmethods where payment_element = '".$plg_vm."'" ;
							$db->setQuery($q);
							$redir = $db->loadResult();
						}else{
							$redir = "iDEAL";
						}


						if ($Abnamroeasy_bank->IDEAL_Mode == 'TEST') {

						$test_mode = 1;
						$IDEAL_AcquirerURL='https://internetkassa.abnamro.nl/ncol/test/orderstandard.asp';
						}else{

						$IDEAL_AcquirerURL= 'https://internetkassa.abnamro.nl/ncol/prod/orderstandard.asp';
						}

						$pspID = $Abnamroeasy_bank->IDEAL_ABNAMROEASY_ID;
						$order_total = $Abnamroeasy_orderList->payment_total;
						$order_id = $Abnamroeasy_orderList->order_id;
						$order_currency = "EUR";

						$merchant_return_url = JURI::root().'index.php?option=com_ccidealplatform&view=ccideal&layout=abn_amro_easy_result';

						$this->assignRef('IDEAL_AcquirerURL', $IDEAL_AcquirerURL);
						$this->assignRef('pspID', $pspID);
						$this->assignRef('order_total', $order_total);
						$this->assignRef('new_orderid', $order_id);
						$this->assignRef('order_currency', $order_currency);
						$this->assignRef('IDEAL_MerchantReturnURL', $merchant_return_url);
						$this->assignRef('getPaymentName', $redir);
						}

			//Layout for ccideal rabo lite  sent
			if($this->_layout == trim('ccideal_rabo_lite_sent')){

				$model = & $this->getModel();
			    $rabolite_bank = $model->getBank_LIST();
			   // $rabolite_orderList = $model->getBank_ORDERLIST();


				if ($rabolite_bank->IDEAL_Mode == 'TEST') {

					$test_mode = 1;
					$IDEAL_AcquirerURL= 'https://idealtest.rabobank.nl/ideal/mpiPayInitRabo.do';

				}else{
					$IDEAL_AcquirerURL='https://ideal.rabobank.nl/ideal/mpiPayInitRabo.do';

				}
				$query = "SELECT * FROM #__ccidealplatform_payments where order_id = '".$_POST['ordernumber']."'";
				$db->setQuery($query);
				$rabolite_orderList = $db->loadObject();

			    $IDEAL_MerchantID 		= $rabolite_bank->IDEAL_MerchantID;
	 			//$order_total			= $rabolite_orderList->payment_total;
		 		//$order_id				= $rabolite_orderList->order_id;
		 		$IDEAL_PrivatekeyPass 	= $rabolite_bank->IDEAL_PrivatekeyPass;
		 		$IDEAL_Description 		= $rabolite_bank->IDEAL_Description;

		 		$order_currency	= "EUR";
				$order_total			= $_POST['grandtotal'];
		 		$order_id				= $_POST['ordernumber'];

			 	$merchant_return_url =  JURI::root().'index.php?option=com_ccidealplatform&view=ccideal&layout=checkout.ccideal_rabo_lite_result';

				$this->assignRef('IDEAL_MerchantID', $IDEAL_MerchantID);
				$this->assignRef('IDEAL_AcquirerURL', $IDEAL_AcquirerURL);
				$this->assignRef('pspID', $pspID);
				$this->assignRef('order_total', $order_total);
				$this->assignRef('orderid', $order_id);
				$this->assignRef('order_currency', $order_currency);
				$this->assignRef('IDEAL_MerchantReturnURL', $merchant_return_url);
				$this->assignRef('IDEAL_PrivatekeyPass', $IDEAL_PrivatekeyPass);
				$this->assignRef('IDEAL_Description', $IDEAL_Description);
				$this->assignRef('getPostData', $post);


			}

			//Layout for ccideal rabo lite result
			if($this->_layout == trim('checkout.ccideal_rabo_lite_result')){

						$query = "SELECT * FROM #__ccidealplatform_config LIMIT 0,1";
						$db->setQuery($query);
						$conf_data = $db->loadAssoc();
						$this->assignRef('conf_data', $conf_data);

						if($extn == 'virtuemart'){
						
							$jversion 			= new JVersion();
							$current_version 	= $jversion->getShortVersion();
							$jversion_sht		= substr($current_version,0,1);
							
							if($jversion_sht == "3"){
								$plg_vm = "plg_virtuemart3_ideal";
							}else{
								$plg_vm = "plg_virtuemart20_ideal";
							}

							//fetching VM Payment id
							$q = "select virtuemart_paymentmethod_id from #__virtuemart_paymentmethods where payment_element = '".$plg_vm."'" ;
							$db->setQuery($q);
							$redir = $db->loadResult();

						}else{
							$redir = "iDEAL";
						}

						$model = & $this->getModel();
						$rabolite_bank = $model->getBank_LIST();
						//$rabolite_orderList = $model->getBank_ORDERLIST();

							if ($rabolite_bank->IDEAL_Mode == 'TEST') {

								$test_mode = 1;
								$IDEAL_AcquirerURL= 'https://idealtest.rabobank.nl/ideal/mpiPayInitRabo.do';

							}else{
								$IDEAL_AcquirerURL='https://ideal.rabobank.nl/ideal/mpiPayInitRabo.do';

							}



							$query = "SELECT * FROM #__ccidealplatform_payments where order_id = '".$order_id."'";
							$db->setQuery($query);
							$rabolite_orderList = $db->loadObject();


							$IDEAL_MerchantID 		= $rabolite_bank->IDEAL_MerchantID;
							$order_total			= $rabolite_orderList->payment_total;
							$order_id				= $rabolite_orderList->order_id;
							$IDEAL_PrivatekeyPass 	= $rabolite_bank->IDEAL_PrivatekeyPass;
							$IDEAL_Description 		= $rabolite_bank->IDEAL_Description;

							$order_currency = "EUR";

							$merchant_return_url = JURI::root().'index.php?option=com_ccidealplatform&view=ccideal&layout=checkout.ccideal_rabo_lite_result';

							$this->assignRef('IDEAL_AcquirerURL', $IDEAL_AcquirerURL);
							$this->assignRef('IDEAL_PrivatekeyPass', $IDEAL_PrivatekeyPass);
							$this->assignRef('IDEAL_Description', $IDEAL_Description);
							$this->assignRef('IDEAL_MerchantID', $IDEAL_MerchantID);
							$this->assignRef('order_total', $order_total);
							$this->assignRef('orderid', $order_id);
							$this->assignRef('order_currency', $order_currency);
							$this->assignRef('IDEAL_MerchantReturnURL', $merchant_return_url);
							$this->assignRef('getPaymentName', $redir);
						}

			//Layout for ccideal post basic sent
			if($this->_layout == trim('ccideal_post_basic_sent')){

				$model = & $this->getModel();
				$post_bank = $model->getBank_LIST();

				$query = "SELECT * FROM #__ccidealplatform_payments where order_id = '".$_POST['ordernumber']."'";
				$db->setQuery($query);
				$post_bank_orderList = $db->loadObject();

				if ($post_bank->IDEAL_Mode == 'TEST') {

						$test_mode = 1;
						$IDEAL_AcquirerURL= 'https://idealtest.secure-ing.com/ideal/mpiPayInitIng.do';

					}else{
						$IDEAL_AcquirerURL='https://ideal.secure-ing.com/ideal/mpiPayInitIng.do';

				}

				$IDEAL_MerchantID 		= $post_bank->IDEAL_MerchantID;
				//$order_total			= $post_bank_orderList->payment_total;
				//$order_id				= $post_bank_orderList->order_id;
				$IDEAL_PrivatekeyPass 	= $post_bank->IDEAL_PrivatekeyPass;
				$IDEAL_Description 		= $post_bank->IDEAL_Description;
				$IDEAL_SubID			= $post_bank->IDEAL_SubID;

				$order_currency	= "EUR";
				$order_total			= $_POST['grandtotal'];
		 		$order_id				= $_POST['ordernumber'];

				$merchant_return_url =  JURI::root().'index.php?option=com_ccidealplatform&view=ccideal&layout=checkout.ccideal_post_basic_result';

				$this->assignRef('IDEAL_MerchantID', $IDEAL_MerchantID);
				$this->assignRef('IDEAL_AcquirerURL', $IDEAL_AcquirerURL);
				$this->assignRef('pspID', $pspID);
				$this->assignRef('order_total', $order_total);
				$this->assignRef('orderid', $order_id);
				$this->assignRef('order_currency', $order_currency);
				$this->assignRef('IDEAL_MerchantReturnURL', $merchant_return_url);
				$this->assignRef('IDEAL_PrivatekeyPass', $IDEAL_PrivatekeyPass);
				$this->assignRef('IDEAL_Description', $IDEAL_Description);
				$this->assignRef('IDEAL_SubID', $IDEAL_SubID);
				$this->assignRef('getPostData', $post);

			}

			//Layout for ccideal post basic result
			if($this->_layout == trim('checkout.ccideal_post_basic_result')){

						$query = "SELECT * FROM #__ccidealplatform_config LIMIT 0,1";
						$db->setQuery($query);
						$conf_data = $db->loadAssoc();
						$this->assignRef('conf_data', $conf_data);

						$model = $this->getModel();
						$post_bank = $model->getBank_LIST();
						$post_orderList = $model->getBank_ORDERLIST();

						if($extn == 'virtuemart'){
						
							$jversion 			= new JVersion();
							$current_version 	= $jversion->getShortVersion();
							$jversion_sht		= substr($current_version,0,1);
							
							if($jversion_sht == "3"){
								$plg_vm = "plg_virtuemart3_ideal";
							}else{
								$plg_vm = "plg_virtuemart20_ideal";
							}

							//fetching VM Payment id
							$q = "select virtuemart_paymentmethod_id from #__virtuemart_paymentmethods where payment_element = '".$plg_vm."'";
							$db->setQuery($q);
							$redir = $db->loadResult();

						}else{
							$redir = "iDEAL";
						}

							if ($post_bank->IDEAL_Mode == 'TEST') {

								$test_mode = 1;
								$IDEAL_AcquirerURL= 'https://idealtest.secure-ing.com/ideal/mpiPayInitIng.do';

							}else{
								$IDEAL_AcquirerURL='https://ideal.secure-ing.com/ideal/mpiPayInitIng.do';

							}


						$IDEAL_MerchantID 		= $post_bank->IDEAL_MerchantID;
						$order_total			= $post_orderList->payment_total;
						$order_id				= $post_orderList->order_id;
						$IDEAL_PrivatekeyPass 	= $post_bank->IDEAL_PrivatekeyPass;
						$IDEAL_Description 		= $post_bank->IDEAL_Description;
						$IDEAL_SubID			= $post_bank->IDEAL_SubID;

						$order_currency = "EUR";

						$merchant_return_url = JURI::root().'index.php?option=com_ccidealplatform&view=ccideal&layout=checkout.ccideal_post_basic_result';

						$this->assignRef('IDEAL_AcquirerURL', $IDEAL_AcquirerURL);
						$this->assignRef('IDEAL_PrivatekeyPass', $IDEAL_PrivatekeyPass);
						$this->assignRef('IDEAL_Description', $IDEAL_Description);
						$this->assignRef('IDEAL_MerchantID', $IDEAL_MerchantID);
						$this->assignRef('order_total', $order_total);
						$this->assignRef('orderid', $order_id);
						$this->assignRef('order_currency', $order_currency);
						$this->assignRef('IDEAL_MerchantReturnURL', $merchant_return_url);
						$this->assignRef('IDEAL_SubID', $IDEAL_SubID);
						$this->assignRef('getPaymentName', $redir);
						}
						
				if($this->_layout == trim('testpayment')){

				}

				if(empty($post)){
					//In case,the variable is not fetch,then we can use $_GET.
					@$post['extn'] 		= $_GET['extn'];
					@$post['ordernumber']= $_GET['ordernumber'];
					@$post['grandtotal'] = $_GET['grandtotal'];
				}
				$ItemID = $this->getccPaymentItemID(@$extn);

				$this->assignRef('data', $post);
				$this->assignRef('ccPaymentItemID', $ItemID);
				$this->assignRef('bankfile', $bank_file);
				$this->assignRef('payment_desc', $ideal_desc);
				$this->assignRef('payment_desc_orderid', $ideal_desc_orderid);
				$this->assignRef('ideal_enable',$ideal_enable);
				//$this->assignRef('options', $option);
				$this->assignRef('jversion', $jversion);
				$this->assignRef('profile_key',$ideal_profile_key);


			parent :: display($tpl);

			}

		function getccPaymentItemID($component){

			if($component=='sobi'){
				$cmpElement='com_sobi';
			}
			if($component=='sobipro'){
				$cmpElement='com_sobipro';
			}
			if($component=='hikashop'){
				$cmpElement='com_hikashop';
			}
			if($component=='rokquickcart'){
				$cmpElement='com_rokquickcart';
			}
			if($component=='rsformpro'){
				$cmpElement='com_rsform';
			}
			if($component=='rsevents'){
				$cmpElement='com_rsevents';
			}
			if($component=='rsmembership'){
				$cmpElement='com_rsmembership';
			}
			if($component=='redshop'){
				$cmpElement='com_redshop';
			}
			if($component=='akeebasubs'){
				$cmpElement='com_akeebasubs';
			}
			if($component=='joomisp'){
				$cmpElement='com_joomisp';
			}
			if($component=='rsepro'){
				$cmpElement='com_rseventspro';
			}

		$db = JFactory::getDBO();
			if(!empty($cmpElement)){
				$s="select `extension_id` from #__extensions where `element`='$cmpElement'";
				$db->setQuery($s); $extnID=  $db->loadResult();

				$s1="select `id` from #__menu where `component_id`='$extnID'";
				$db->setQuery($s1); $ItemID=  $db->loadResult();

				return $ItemID;
			}
		}
	}
?>