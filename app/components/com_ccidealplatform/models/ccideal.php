<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details. 
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

$document = JFactory :: getDocument();
$document->addStyleSheet('components/com_ccidealplatform/assets/css/style.css');

class ccIdealplatformModelccideal extends JModelLegacy {

	const MIN_TRANS_AMOUNT = 1.20;

	function __construct()
	{
		parent::__construct();
	}

    // David - 2015-06-10 - cciDEAL 4.4.7
    // Simple logging of payment process (to emails at the moment)
    public function ccideal_logging($log_subject, $log_message = null) {

        // Logging details
        $logging_status = true;
        $logging_status = null; // disabled by default
        $log_email = 'chillcreations+test@gmail.com';

        if ($logging_status == null) {
            return;
        }

        // Send log messages to file
        // TODO Add logging to file

        // Send log messages to email
        mail($log_email, $log_subject, $log_message);

        return;

    }

    // Start Mollie payment process
	function mollie_process() {

		$amount 	 = JRequest::getVar('amount');
		$description = JRequest::getVar('description');
		$userid 	 = JRequest::getVar('userid');
		$component 	 = JRequest::getVar('extn');
		$order_id 	 = JRequest::getVar('item_number');
        $ccideal_description 	 = JRequest::getVar('ccideal_description');
		//$order_id 	 = $this->cciDEALremoveSplChr($order_id);
		$ItemID 	 = $this->getccPaymentItemID($component);

		// Virtuemart
		@$getiDEALSession = JRequest::getVar('iDEALVMPayment_SESSION');

        // URLs
		$return_url  = JURI::ROOT()."index.php?option=com_ccidealplatform&task=mollie_return&com=$component&orderid=$order_id&amt=$amount&getiDEALSession=$getiDEALSession&Itemid=$ItemID";
		$report_url  = JURI::ROOT()."index.php?option=com_ccidealplatform&task=mollie_notify";		

		$db = JFactory::getDBO();

		$query = "SELECT IDEAL_PartnerID FROM #__ccidealplatform_config WHERE id=1";
		$db->setQuery($query);
		$mollie_api = $db->loadResult();

		$query = "SELECT pay_method,payment_total FROM #__ccidealplatform_payments WHERE order_id = '$order_id' AND extension = '".$component."'";
		$db->setQuery($query);
		$order_payments = $db->loadObject();

		$pay_method		= "";
		$pay_method		= $order_payments->pay_method;
		$order_amount 	= $order_payments->payment_total;

		if($order_amount != $amount){
			$amount = $amount;
		}

		$amount = round($amount,2);

		if(!$this->setDescription($description)){
			echo "desc";
		}else if( !$this->setAmount($amount)){
			echo "amount";
		}else if(!$this->setReturnUrl($return_url)){
			echo "rurl";
		}else if(!$report_url) {
			echo "reurl";
		}

        // RSForm Pro: get custom ccideal_description if set
        if (!empty($ccideal_description) AND $component = "rsformpro") {
            $this->setDescription($ccideal_description);
        }

		// Mollie Integration
		require_once(JPATH_COMPONENT . '/Mollie/API/Autoloader.php');
		$mollie = new Mollie_API_Client;
		$mollie->setApiKey($mollie_api);

		//$order_id = time();

		$payment = $mollie->payments->create(array(
			"amount"      => $this->getAmount(),
			"description" => $this->getDescription(),
			"webhookUrl"  => $report_url,
			"redirectUrl" => $this->getReturnURL(),			
			"method" 	  => $pay_method,
			"metadata"    => array(
							"order_id" 			=> $order_id,
							"component" 		=> $component,
							"getiDEALSession" 	=> $getiDEALSession,
							"Itemid" 			=> $ItemID
							
			),
		));
		
		$Authen_URL 			= $payment->getPaymentUrl();
		$amount 				= $this->getAmount();
		$getPartnerID_bankid 	= urlencode($mollie_api);
		$timestamp = gmdate('Y') . "-" . gmdate('m') . "-" . gmdate('d') . "T" . gmdate('H') . ":" . gmdate('i') . ":" . gmdate('s') . ".000Z";
		$transaction_id = "#BT";

		$this->save_logs($transaction_id,$amount,$Authen_URL,$order_id,$timestamp,$getPartnerID_bankid,$this->getDescription(),'','','');

		header("Location: " . $payment->getPaymentUrl());
		exit;
	}

	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return substr($current_version,0,3);
	}

// Remove special characters

	function cciDEALremoveSplChr($postVar){
	 	$stringiDEALDesc =strip_tags($postVar,"");
 		$stringiDEALDesc =trim($postVar);
 		$t=0;
		if (preg_match('/\//', $stringiDEALDesc)) {
			$t=1;
			$date=date("dM");
			$stringiDEALDesc = str_replace("/",$date,$stringiDEALDesc);
		}
 	 	$stringiDEALDesc = preg_replace("/[^A-Za-z0-9\s.+?().,';:s-]/",'',$stringiDEALDesc);
			$stringiDEALDesc = str_replace(" ", "", $stringiDEALDesc);

		if($t=1){
		 	$date=date("dM");
		  	$stringiDEALDesc = str_replace($date,"/",$stringiDEALDesc);
		 	return $stringiDEALDesc;
		}else{
	 	  	return $stringiDEALDesc;
		}
	}

	function getccPaymentItemID($component){

		if($component=='sobi'){
			$cmpElement='com_sobi';
		}
		if($component=='sobipro'){
			$cmpElement='com_sobipro';
		}
		if($component=='hikashop'){
			$cmpElement='com_hikashop';
		}
		if($component=='rokquickcart'){
			$cmpElement='com_rokquickcart';
		}
		if($component=='rsformpro'){
			$cmpElement='com_rsform';
		}
		if($component=='rsevents'){
			$cmpElement='com_rsevents';
		}
		if($component=='rsmembership'){
			$cmpElement='com_rsmembership';
		}
		if($component=='redshop'){
			$cmpElement='com_redshop';
		}
		if($component=='akeebasubs'){
			$cmpElement='com_akeebasubs';
		}
		if($component=='joomisp'){
			$cmpElement='com_joomisp';
		}
		if($component=='rsepro'){
			$cmpElement='com_rseventspro';
		}
		if($component=='virtuemart'){
			$cmpElement='com_virtuemart';
		}

		$db = JFactory::getDBO();
		if(!empty($cmpElement)){
			$s="select `extension_id` from #__extensions where `element`='$cmpElement'";
			$db->setQuery($s); $extnID=  $db->loadResult();

			$s1="select `id` from #__menu where `component_id`='$extnID' and `published`=1 AND `access`=1";
			$db->setQuery($s1); $ItemID=  $db->loadResult();

			return $ItemID;
		}
	}

// mollie_notify function for updating the status

	function mollie_notify() {
	
		$db = JFactory::getDBO();

		$query = "SELECT IDEAL_PartnerID FROM #__ccidealplatform_config WHERE id=1";
		$db->setQuery($query);
		$mollie_api = $db->loadResult();

		require_once(JPATH_COMPONENT . '/Mollie/API/Autoloader.php');

		$mollie = new Mollie_API_Client;
		$mollie->setApiKey($mollie_api);
		
		$getiDEALSession	= "";
		$ItemID				= "";
		$state				= "";

		$transaction_id		= $_POST["id"];
		$payment		 	= $mollie->payments->get($_POST["id"]);
		$order_number		= $payment->metadata->order_id;
		$component		 	= $payment->metadata->component;
		$getiDEALSession 	= $payment->metadata->getiDEALSession;
		$ItemID			 	= $payment->metadata->ItemID;
		$mode				= $payment->mode;
		$createdDatetime 	= $payment->createdDatetime;
		$state 				= $payment->state;
		$description 		= $payment->description;
		$method 			= $payment->method;

		if ($payment->isPaid() == TRUE)
		{
			$pay_status = "Success";
		}elseif ($payment->isOpen() == FALSE)
		{
			$pay_status = "Cancelled";
		}

		$this->ccideal_notify($pay_status,$transaction_id,$method,$order_number,$component);
	}

	function mollie_return() {

		$component = JRequest::getVar('com');
		$order_number = JRequest::getVar('orderid');
		//$ccItemID	=  $_SESSION['CC_MenuItemID'];

		$this->ccideal_return($component,$order_number);
	}

	public function ccideal_notify($pay_status,$transaction_id,$method,$order_number,$component){

		$db = JFactory :: getDBO();

        if ($pay_status == "Success") {

			$paid_status 	= "1";

			//Same order status can be break using this code

			$query = "SELECT order_id FROM #__ccidealplatform_payments WHERE payment_status='paid' AND `trans_id`='$transaction_id'";
			$db->setQuery($query);
			$status_check = $db->loadResult();

			if($status_check){
				return false;
			}

			$query = "UPDATE #__ccidealplatform_payments SET `payment_status`= 'paid', `pay_method` = '".$method."', `trans_id` = '".$transaction_id."' WHERE order_id = '" . $order_number . "'";
			$db->setQuery($query);
			$db->Query();

			$queryLog = "UPDATE #__ccidealplatform_logs SET payment_status = 'paid' WHERE purchaseID = '" . $order_number . "'";
			$db->setQuery($queryLog);
			$db->Query();

		}else if($pay_status == "Cancelled"){

			$paid_status 	= "0";

			$query = "UPDATE #__ccidealplatform_payments SET payment_status = 'cancelled', pay_method = '".$method."', `trans_id` = '".$transaction_id."' WHERE order_id = '" . $order_number . "'";
			$db->setQuery($query);
			$db->Query();

			$queryLog = "UPDATE #__ccidealplatform_logs SET payment_status = 'cancelled' WHERE purchaseID = '" . $order_number . "'";
			$db->setQuery($queryLog);
			$db->Query();
			
		}else if($pay_status == "Pending"){

			$paid_status 	= "2";

			$query = "UPDATE #__ccidealplatform_payments SET payment_status = 'pending', pay_method = '".$method."', `trans_id` = '".$transaction_id."' WHERE order_id = '" . $order_number . "'";
			$db->setQuery($query);
			$db->Query();

			$queryLog = "UPDATE #__ccidealplatform_logs SET payment_status = 'pending' WHERE purchaseID = '" . $order_number . "'";
			$db->setQuery($queryLog);
			$db->Query();
			
		}else if($pay_status == "Failed"){

			$paid_status 	= "3";

			$query = "UPDATE #__ccidealplatform_payments SET payment_status = 'failed', pay_method = '".$method."', `trans_id` = '".$transaction_id."' WHERE order_id = '" . $order_number . "'";
			$db->setQuery($query);
			$db->Query();

			$queryLog = "UPDATE #__ccidealplatform_logs SET payment_status = 'failed' WHERE purchaseID = '" . $order_number . "'";
			$db->setQuery($queryLog);
			$db->Query();
		}

	//Support for Akeeba subscriptions
		if ($component == 'akeebasubs') {
			$this->akeeba_notify($order_number,$pay_status,$transaction_id);
		}

	//Support for hikashop
		if ($component == 'hikashop') {
			$this->hikashop_notify($order_number,$pay_status,$paid_status);
		}

	//Support for rsformpro
		if ($component == 'rsformpro') {
            $this->rsformpro_notify($order_number,$paid_status);
		}

	//Support for RSEvents Pro
		if($component=="rsepro"){
			$this->rsepro_notify($order_number,$paid_status);
		}

	//Support for rsmembership
		if ($component == 'rsmembership') {
			$this->rsmembership_notify($order_number,$paid_status,$transaction_id);
		}

	//Support for JoomISP
		if ($component == 'joomisp') {
			$this->joomisp_notify($order_number,$paid_status);
		}

	//Support for Virtuemart
		if ($component == 'virtuemart') {
			$this->virtuemart_notify($order_number,$paid_status,$transaction_id);
		}

	//Support for redshop
		if ($component == 'redshop') {
			$this->redshop_notify($order_number,$paid_status);
		}

	//Support for SobiPro
		if($component=="sobipro"){
			$this->sobipro_notify($order_number,$paid_status);
		}

	//Support for caddy
		if ($component == 'caddy') {
			$this->caddy_notify($order_number,$paid_status);
		}

	//Support for ccInvoices
		if ($component == 'ccinvoices') {
			$this->ccinvoices_notify($order_number,$paid_status,$transaction_id);
		}
		
	//Support for TicketMaster
		if ($component == 'ticketmaster') {
			$this->ticketmaster_notify($order_number,$paid_status,$transaction_id,$method);
		}
	//Support for RSDirectory
		if ($component == 'rsdirectory') {
			$this->rsdirectory_notify($order_number,$paid_status,$transaction_id,$method);
		}
	//Support for J2Store
		if ($component == 'j2store') {
			$this->j2store_notify($order_number,$paid_status,$transaction_id,$method);
		}
		
	//Support for techjoomla
		if ($component == 'techjoomla') {
			$this->techjoomla_notify($order_number,$paid_status,$transaction_id,$method);
		}
	}

	public function ccideal_return($component,$order_number){

		$db = JFactory :: getDBO();

		$getMyItemID = $this->getccPaymentItemID($component);

		$query = "SELECT * FROM #__ccidealplatform_config LIMIT 0,1";
		$db->setQuery($query);
		$conf_data = $db->loadAssoc();	

		$query = "SELECT payment_status FROM #__ccidealplatform_payments WHERE order_id='" . $order_number . "'";
		$db->setQuery($query);
		$result = $db->loadResult();		
		
		$slmsg = new ccIdealplatformController();

	//Support for TEST FORM
		if ($component == 'testform1' || $component == 'testform2' || $component == 'testform3' || $component == 'testform4' || $component == 'testform5' || $component == 'testform6' || $component == 'testform7' || $component == 'testpayment') {
			$statuslayout = $slmsg->statuslayout($result);			
		}

	//Support for Content_plugin
		if ($component == 'Content_plugin') {
			$statuslayout = $slmsg->statuslayout($result);
		}

	//Support for Akeeba subscriptions
		if ($component == 'akeebasubs') {
			$this->akeeba_return($order_number,$result);
		}

	//Support for hikashop
		if ($component == 'hikashop') {
			$this->hikashop_return($order_number,$result);
		}

	//Support for RSForm Pro
		if ($component == 'rsformpro') {
			$this->rsformpro_return($order_number,$result);
		}

	//Support for RSEvents Pro
		if($component=="rsepro"){
			$this->rsepro_return($order_number,$result);
		}

	//Support for ticketmaster
		if($component=="ticketmaster"){
			$this->ticketmaster_return($order_number,$result);
		}

	//Support for rsmembership
		if ($component == 'rsmembership') {
			$this->rsmembership_return($order_number,$result);
		}

	//Support for JoomISP
		if ($component == 'joomisp') {
			$this->joomisp_return($order_number,$result);
		}

	//Support for Virtuemart
		if ($component == 'virtuemart') {
			$this->virtuemart_return($order_number,$result);
		}

	//Support for redshop
		if ($component == 'redshop') {
			$this->redshop_return($order_number,$result);
		}

	//Support for SobiPro
		if($component=="sobipro"){
			$this->sobipro_return($order_number,$result);
		}

	//Support for caddy
		if ($component == 'caddy') {
			$this->caddy_return($order_number,$result);
		}

	//Support for ccInvoices
		if ($component == 'ccinvoices') {
			$this->ccinvoices_return($order_number,$result);
		}
		
	//Support for RSDirectory
		if ($component == 'rsdirectory') {
			$this->rsdirectory_return($order_number,$result);
		}
		
	//Support for J2Store
		if ($component == 'j2store') {
			$this->j2store_return($order_number,$result);
		}
	//Support for techjoomla
		if ($component == 'techjoomla') {
			$this->techjoomla_return($order_number,$result);
		}
	}

// Getting  BANK DETSILS

	function getBank_LIST(){

		$db = JFactory :: getDBO();
		$query = "SELECT * FROM #__ccidealplatform_config";
		$db->setQuery($query);
		$ideal_bank = $db->loadObject();

		return $ideal_bank;
	}

	function getBank_ORDERLIST(){

		$db = JFactory :: getDBO();

		$query = "SELECT * FROM #__ccidealplatform_conf";
		$db->setQuery($query);
		$tmp_orderid =$db->loadObject();


		$query = "SELECT * FROM #__ccidealplatform_payments where order_id = '$tmp_orderid->tmp_orderid'";
		$db->setQuery($query);
		$ideal_bank = $db->loadObject();

		return $ideal_bank;
	}

//save it in logs table

	function save_logs($transactionID,$amount,$Authen_URL,$purchaseID,$timestamp,$merchantID,$description,$entrance_code,$token,$tokenCode){

		$db = JFactory::getDBO();
		//$amount = $amount/100;

		$select = "SELECT transactionID FROM #__ccidealplatform_logs WHERE purchaseID='$purchaseID'";
		$db->setQuery($select); $result = $db->loadResult();

		$select_bank = "select IDEAL_Bank,IDEAL_Mode  from #__ccidealplatform_config";
		$db->setQuery($select_bank);
		$Bank_details = $db->loadObject();

		$Bank_name = $Bank_details->IDEAL_Bank;
		$Bank_mode = $Bank_details->IDEAL_Mode;
		$IDEAL_PartnerID = $Bank_details->IDEAL_PartnerID;

		if(empty($merchantID)){
			$merchantID = $IDEAL_PartnerID;
		}

		if($result==""){

			$insert_log = "INSERT INTO #__ccidealplatform_logs (`createDateTimeStamp`,`amount`,`issuerAuthenticationURL`,`transactionID`,`purchaseID`,`payment_status`,`merchantID`,`decription`,`entrance_code`,`IDEAL_Bank`,`IDEAL_Mode`,`token`,`token_code`,`last_tested_date`,`ordering`) " .
			"VALUES('$timestamp','$amount','$Authen_URL','$transactionID','$purchaseID','pending','$merchantID','$description','$entrance_code','$Bank_name','$Bank_mode','$token','$tokenCode','','')";
			$db->setQuery($insert_log);
			$db->Query();
		}else{

			$update_log = "UPDATE #__ccidealplatform_logs SET `createDateTimeStamp`='$timestamp',`amount`='$amount',`issuerAuthenticationURL`='$Authen_URL',`transactionID`='$transactionID',`merchantID`='$merchantID',`decription`='$description',`entrance_code`='$entrance_code',`IDEAL_Bank`='$Bank_name',`IDEAL_Mode`='$Bank_mode' WHERE `purchaseID`='$purchaseID'";
			$db->setQuery($update_log);
			$db->Query();
		}
	}

//Added 23 Oct 2013

//Akeeba Subscription

	public function akeeba_notify($order_number,$pay_status,$transaction_id){

		$db = JFactory :: getDBO();

		$date=date("Ymm");

		$order_number=str_replace("$date","",$order_number);
		$query = "SELECT * FROM #__akeebasubs_subscriptions WHERE akeebasubs_subscription_id='" . $order_number . "'";
		$db->setQuery($query);
		$result = $db->loadAssoc();

		$order_total = 0;
		$order_id = $result['akeebasubs_subscription_id'];

		$my_order_number = $result['akeebasubs_subscription_id'];

		$my_order_total = $result['gross_amount'];

		$d['order_id'] = $order_id;

		$query_id = "SELECT `user_id` FROM #__akeebasubs_subscriptions WHERE akeebasubs_subscription_id='" . $order_number . "'";
		$db->setQuery($query_id);
		$akeeba_uid = $db->loadResult();

		$query_titleid = "SELECT `akeebasubs_level_id` FROM #__akeebasubs_subscriptions WHERE akeebasubs_subscription_id='" . $order_number . "'";
		$db->setQuery($query_titleid);
		$status_titleid = $db->loadResult();

		$query_title = "SELECT `title` FROM #__akeebasubs_levels WHERE akeebasubs_level_id ='" . $status_titleid . "'";
		$db->setQuery($query_title);
		$status_title = $db->loadResult();

		$query = "SELECT state FROM #__akeebasubs_subscriptions WHERE akeebasubs_subscription_id='" . $order_number . "'";
		$db->setQuery($query);
		$status = $db->loadResult();

		$statuses = explode("\n", $status);

		if (!in_array('C', $statuses)) {

			if($akeeba_uid){

				$query1 = "SELECT `block`,`activation` FROM #__users WHERE id='" . $akeeba_uid . "'";
				$db->setQuery($query1);
				$uid_status = $db->loadAssoc();

				if($uid_status['block'] && $uid_status['activation']!=""){
					$queryuser = "UPDATE #__users SET block = '0',activation='' WHERE id='$akeeba_uid'";
					$db->setQuery($queryuser);
					$db->Query();
				}
			}
			$status = $status;
		}

		$order_id = $order_number;
		if ($pay_status == "Success") {
			$status = 'C';
		}else if($pay_status == "Cancelled"){
			$status = 'X';
		}else{
			$status = 'P';
		}

		$val['state']=$status;
		$val['trx_ids']=$order_number;
		$val['processor_key']=$transaction_id;
		$val['payment_method']="iDEAL";

		$options = array( $val);

		jimport('joomla.plugin.helper');
		JPluginHelper::importPlugin('akpayment','iDEAL');
		$app = JFactory::getApplication();
		$jResponse = $app->triggerEvent('onAKPaymentCallback',array("0",$options));
	}

	public function akeeba_return($order_number, $result){

		$db = JFactory::getDBO();

		$date=date("Ymm");

		$order_number=str_replace("$date","",$order_number);

		$statuslayout = ccIdealplatformController::statuslayout($result);

		// Get akeebasubs_level_id
			$query = "SELECT akeebasubs_level_id FROM #__akeebasubs_subscriptions WHERE akeebasubs_subscription_id ='$order_number'";
			$db->setQuery($query);
			$akeebasubs_level_id = $db->loadResult();

		// Get slug
			$query = "SELECT slug FROM #__akeebasubs_levels WHERE akeebasubs_level_id ='$akeebasubs_level_id'";
			$db->setQuery($query);
			$slug = $db->loadResult();

		// Auto redirect
		if (($result == "paid") || ($result == "pending")) {
			echo "<meta http-equiv='refresh' content='3;url=index.php?option=com_akeebasubs&view=message&slug=$slug&layout=order&subid=$order_number'>";
		}else{
			echo "<meta http-equiv='refresh' content='3;url=index.php?option=com_akeebasubs&view=message&slug=$slug&layout=cancel&subid=$order_number'>";
		}
	}

//Hikashop

	public function hikashop_notify($order_number,$pay_status,$paid_status){

		$db = JFactory::getDBO();
		
		$query = "SELECT config_value FROM #__hikashop_config WHERE config_namekey='cancelled_order_status'";
		$db->setQuery($query);
		$status = $db->loadResult();
		$statuses = explode(",", $status);

		$select = "SELECT payment_params FROM #__hikashop_payment WHERE payment_type='iDEAL'";
		$db->setQuery($select);
		$currencyCode1 = $db->loadResult();
		$stateArry=unserialize($currencyCode1);

		$status = $stateArry->verified_status;

		if (!in_array($status, $statuses)) {
			$status = $status . ",".$status;
			$query = "UPDATE #__hikashop_config SET config_value = '" . $paid_status . "' WHERE config_namekey='cancelled_order_status'";
			$db->setQuery($query);
			$db->Query();
		}

		if ($paid_status==1) {

			$select = "SELECT payment_params FROM #__hikashop_payment WHERE payment_type='iDEAL'";
			$db->setQuery($select);
			$currencyCode1 = $db->loadResult();
			$stateArry=unserialize($currencyCode1);
			$status = $stateArry->verified_status;

			@require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_hikashop'.DS.'helpers'.DS.'helper.php');

			require_once(JPATH_ROOT . '/plugins/hikashoppayment/iDEAL/iDEAL.php');

			jimport('joomla.plugin.helper');
			jimport('joomla.mail.helper');
			jimport( 'joomla.utilities.utility' );
			$configs =JFactory::getConfig();

			$options = array("statuses" => $status, "orderID" => $order_number);
			$dispatcher = JDispatcher::getInstance();
			$type = preg_replace('#[^A-Z0-9_\.-]#i', '', 'iDEAL');
			$name = preg_replace('#[^A-Z0-9_\.-]#i', '', 'hikashoppayment');
			$instance = new plgHikashoppaymentIDEAL($dispatcher, array('name'=>$name,'type'=>$type));
			$instance = $instance->onPaymentNotification($options);

		}else{
			$status = $stateArry->invalid_status;

            // TODO - Run HikaShop iDEAL plugin > onPaymentNotification after other statuses also

		}

		$query = "UPDATE #__hikashop_order SET order_status = '" . $status . "' WHERE order_id='" . $order_number . "'";
		$db->setQuery($query);
		$db->Query();
	}

	public function hikashop_return($order_number, $result){
		
		$slmsg 			= new ccIdealplatformController();
		$statuslayout 	= $slmsg->statuslayout($result);
	}

//RSForm Pro

	public function rsformpro_notify($order_number,$paid_status){

		$db = JFactory::getDBO();

		$date=date("Ymdd");

		$order_number=str_replace("$date","",$order_number);

		$query = "SELECT FieldValue FROM #__rsform_submission_values WHERE SubmissionId='" . $order_number . "' AND FieldName='_STATUS'";
		$db->setQuery($query);
		$status = $db->loadResult();
		$statuses = explode(",", $status);

		if (!in_array('1', $statuses)) {

			$query = "UPDATE #__rsform_submission_values SET FieldValue = '" . $paid_status . "' WHERE SubmissionId='" . $order_number . "' AND FieldName='_STATUS'";
			$db->setQuery($query);
			$db->Query();
		}

		if($status != 1){

			$query = "UPDATE #__rsform_submission_values SET FieldValue = '" . $paid_status . "' WHERE SubmissionId='" . $order_number . "' AND FieldName='_STATUS'";
			$db->setQuery($query);
			$db->Query();
			if ($paid_status == 1) {

				$path=JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_rsform'.DS.'helpers'.DS.'rsform.php';
				require_once ($path);

				jimport('joomla.plugin.helper');
				JPluginHelper::importPlugin('system','rsfppayment');

				$app = JFactory::getApplication();
				$app->triggerEvent('rsfp_afterConfirmPayment', array($order_number));

			}
		}
	}

	public function rsformpro_return($order_number,$result){
		
		$slmsg 			= new ccIdealplatformController();
		$statuslayout 	= $slmsg->statuslayout($result);
	}

//RSEvents Pro
	public function rsepro_notify($order_number,$paid_status){


        $db = JFactory::getDBO();

        $date=date("ddy");
        $order_number=str_replace("$date","",$order_number);

        $query = "SELECT * FROM #__rseventspro_users WHERE id='" . $order_number . "'";
        $db->setQuery($query);
        $result = $db->loadAssoc();

        $order_total = 0;
        $order_id = $result['id'];
        $my_order_number = $result['id'];

        if($paid_status) {
            $status = 1;
        }else{
            $status = 0;
        }

        $query1 = "UPDATE #__rseventspro_users SET `state` = '" . $status . "' WHERE id='$order_number'";
        $db->setQuery($query1);
        $db->Query();

        $db = JFactory::getDBO();
        $query = "SELECT payment_status FROM #__ccidealplatform_payments WHERE extension_id='" . $order_number . "'";
        $db->setQuery($query);
        $result = $db->loadResult();

        // David - 15 May 2015 - cciDEAL 4.4.5
        // Added RSEvents processing (from plugin)

        // Note: PDFs are only attached when RSEvents PDF plugin
        // is enabled, not controllable by call to rseventsproHelper

        // Confirm and send mails
        require_once JPATH_SITE.'/components/com_rseventspro/helpers/rseventspro.php';
        require_once JPATH_SITE.'/components/com_rseventspro/helpers/emails.php';

        if($result == "paid") {
            //send the activation email
            $status = 1;
            rseventsproHelper::confirm($order_number);
            $log[] = "Successfully added the payment to the database.";

        } else if ($result == "pending") {
            $status = 0;
            $log[] = "Payment processed and placed in the log.";

        } else if ($result == "cancelled") {
            $status = 2;
            rseventsproHelper::denied($order_number);
            $log[] = "Payment cancelled and placed in the log.";
        }

        $query1 = "UPDATE #__rseventspro_users SET `state` = '" . $status . "' WHERE id='$order_number'";
        $db->setQuery($query1);
        $db->Query();

        rseventsproHelper::savelog($log,$order_number);


        // David - 15 May 2015 - cciDEAL 4.4.5 TEST
        // Disabled calling RSEvents plugin, now updated in cciDEAL itself
        //jimport('joomla.plugin.helper');
        //JPluginHelper::importPlugin('system','rsepro_ideal');

        //$app = JFactory::getApplication();
        //$app->triggerEvent('rsepro_processForm');
	}

	public function rsepro_return($order_number,$result){
		
		$slmsg 			= new ccIdealplatformController();
		$statuslayout 	= $slmsg->statuslayout($result);
	}
	
	//Ticket Master
	public function ticketmaster_notify($order_number,$paid_status,$transaction_id, $method = "ideal"){

        $this->ccideal_logging ('Reached ticketmaster_notify, starting to process: ' . $order_number);

		$date		= date("yddm");
		$ordercode	= str_replace("$date","",$order_number);

		$db 		= JFactory::getDBO();
		$user 		= JFactory::getUser();

		## Getting the global DB session
		$session 	= JFactory::getSession();

        // Get all details belonging to this payment
        $query = "SELECT * FROM #__ccidealplatform_payments WHERE `order_id`='".$order_number."'";
        $db->setQuery($query);
        $ccideal_payment = $db->loadObject();

		if(!$session->get('ordercode')){
			$session->set('ordercode',$ordercode);
		}

		## Gettig the orderid if there is one.
		$this->ordercode = $session->get('ordercode');		
		
		## SHOWING THE FORM TO THE CUSTOMER ##
		$path_include = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ticketmaster'.DS.'classes'.DS.'payment.api.class.php';			
		 
		## include the API file:
		include($path_include);
		
		## Stop if the class is not present:
		if (!class_exists('paymentAPI')) {
			exit("Class is not available to process transactions.");
		}
		
		// Load the language files
		$jlang = JFactory::getLanguage();
		$jlang->load('com_ticketmaster', JPATH_SITE, 'en-GB', true);
		$jlang->load('com_ticketmaster', JPATH_SITE, $jlang->getDefault(), true);

		## $newPayment is starting the class and sending the ordercode.
		$newPayment = new paymentAPI((int)$this->ordercode);		
		
		if(!$transaction_id){
			$transaction_id = md5($this->ordercode);
		}

		$temp_transaction = $newPayment->insertTempTransaction($ccideal_payment->user_id, $transaction_id);
		
		$newPayment->updateTempTransaction( $transaction_id , '1', '1') ;
		
		$temp_transactions = $newPayment->checkTempTransactionAmount($transaction_id);

		if($temp_transactions > 1){
		  exit('script needs to stop now - show message');
		}
		
		//$transaction = $newPayment->getTempTransactionResult($transaction_id);

		$details = http_build_query($_POST);
        $this->ccideal_logging ('Post details, trans. ID: ' . print_r($details, true));

		$result = $newPayment->saveTransaction($transaction_id, $ccideal_payment->user_id, $details, $ccideal_payment->payment_total, $method);

        $this->ccideal_logging ('Result of $newPayment->saveTransaction: ' . $result);

		if($paid_status == 1){

            $this->ccideal_logging ('$paid_status == 1, ready to process updateOrder+createTickets ');

			$result 		= $newPayment->updateOrder();
			$ticket_creator = $newPayment->createTickets();

            // David - 2015-06-10 - cciDEAL 4.4.7
            // Disabling this fixed all email messages from not being sent, no idea why.
			//$newPayment->sendConfirmation();
		}

        $this->ccideal_logging ('After updateOrder+createTickets, before sendTickets');

		## if tickets has been created:
		if($ticket_creator == true){
            $this->ccideal_logging ('$ticket_creator is true, prepare sendTickets');
			$newPayment->sendTickets();
		}

		## Clearing Ticketmaster session.
		$newPayment->clearSession();
	}

	public function ticketmaster_return($order_number,$result){

        // David - 2015-06-13 - cciDEAL 4.4.7
        // Clear RD-Ticketmaster cache/cart on return by user
        ## Include the confirmation class to sent the tickets.
        $path_include = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ticketmaster'.DS.'classes'.DS.'payment.api.class.php';
        include_once( $path_include );

        ## Stop if the class is not present:
        if (!class_exists('paymentAPI')) {
            exit("Class is not available to process transactions.");
        }

        ## Start the API to process everything.
        $newPayment = new paymentAPI();
        ## Clearing the session:
        $newPayment->clearSession();

        $slmsg 			= new ccIdealplatformController();
		$statuslayout 	= $slmsg->statuslayout($result);

		//echo "<meta http-equiv='refresh' content='3;url=index.php?option=com_ticketmaster&view=transaction&payment_type=rdmccidealplatform&order_id=$order_number'>";
	}

//RS Membership
	public function rsmembership_notify($order_number,$paid_status,$transaction_id){
	
		if (file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_rsmembership'.DS.'helpers'.DS.'rsmembership.php'))
			require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_rsmembership'.DS.'helpers'.DS.'rsmembership.php');
			
		$date=date("Yd");
		$order_number=str_replace("$date","",$order_number);
		$log = array();
		
		$db = JFactory::getDBO();	
		
		$query = "SELECT status,params FROM #__rsmembership_transactions WHERE id=" . $order_number . "";
		$db->setQuery($query);
		$status_trns = $db->loadObject();

		$status = $status_trns->status;
		$params = $status_trns->params;			
		$mem_id = str_replace('membership_id=','',$params);
		
		$query = "SELECT activation FROM #__rsmembership_memberships WHERE id=" . $mem_id . "";
		$db->setQuery($query);
		$activation = $db->loadResult();	

		if(trim($activation) == "1"){
		
			if ($paid_status == "1") {
				$log[] = "iDEAL reported a valid transaction.";
				$log[] = "Payment status is ".(!empty($paid_status) ? 'Completed' : 'empty').".";
				
				// process payment
				RSMembership::approve($order_number);

				$log[] = "Successfully added the payment to the database.";						
			}
		}
		
		if ($transaction_id)
		{	
			//Update transaction ID			
			$query = "UPDATE #__rsmembership_transactions SET hash = '".$transaction_id."' WHERE id='" . $order_number . "'";
			$db->setQuery($query);
			$db->Query();
			
			RSMembership::saveTransactionLog($log, $order_number);
		}
	}

	public function rsmembership_return($order_number,$result){

		$db = JFactory::getDBO();

		$date=date("Yd");
		$order_number=str_replace("$date","",$order_number);

		if ($result == "paid") {

			$select_rsmember = "SELECT * FROM #__rsmembership_transactions WHERE id = '$order_number'";
			$db->setQuery($select_rsmember);
			$action_list = $db->loadAssoc();

			$get_membershipid = $action_list['params'];
			$get_membershipid = str_replace("membership_id=","",$get_membershipid);

			$select = "SELECT * FROM #__rsmembership_memberships WHERE id='$get_membershipid'";
			$db->setQuery($select);
			$action = $db->loadAssoc();
		}

		$slmsg 			= new ccIdealplatformController();
		$statuslayout 	= $slmsg->statuslayout($result);	

		if ($result == "paid") {
			if($action['action']!=0){
				$redirect = $action['redirect'];
				echo "<meta http-equiv='refresh' content='3;url=$redirect'>";

			}else{
				echo "<meta http-equiv='refresh' content='3;url=index.php?option=com_rsmembership&task=thankyou'>";
			}
		}
	}

//JoomISP
	

	public function joomisp_notify($order_number,$paid_status){

		$db = JFactory::getDBO();

		$date=date("mY");
		$order_number=str_replace("$date","",$order_number);
		$orderid=preg_replace("/[^0-9,]/", "", $order_number);	
		
		if( $orderid ) {
			$db = JFactory::getDBO();

			$db->setQuery('SELECT u.email, c.lastname, c.firstname, o.id, o.plan_id, c.user_id FROM #__users AS u'.
						  ' LEFT JOIN #__isp_orders AS o ON o.id = '.$orderid.
						  ' LEFT JOIN #__isp_customers AS c ON c.id = o.customer_id'.
						  ' WHERE u.id = c.user_id');
			$customer = $db->loadObject();		
			
			$db->setQuery('SELECT state FROM #__isp_plans WHERE id = '.$customer->plan_id);
			$state = $db->loadResult();			

			$db->setQuery('UPDATE #__isp_orders SET state='.$state.' WHERE id='.$orderid);
			$db->query();

			$db->setQuery('SELECT title, description, params FROM #__isp_states WHERE id='.$state);
			$state = $db->loadObject();

			$params = new JRegistry( $state->params );
			
			require_once(JPATH_ROOT.DS.'components'.DS.'com_joomisp'.DS.'helpers'.DS.'joomisp.php');		

			if($params->get('sendmail','no')=='yes') {
			
				$replaces['state'] = $state->title;
				$replaces['description'] = $state->description;
				$replaces['lastname'] = $customer->lastname;
				$replaces['firstname'] = $customer->firstname;
				$replaces['orderid'] = $orderid;
				$template = JoomISPHelperSite::getTemplate('orderstateupdate',$replaces);
				$mailer = JFactory::getMailer();
				$mailer->addRecipient($customer->email);
				$mailer->setSubject($template->caption);
				$mailer->setBody($template->content);
				$mailer->send();
				$statemsg = JText::_('Customer notified');
			}
			$authorize	= JFactory::getACL();

			$user = JFactory::getUser($customer->user_id);
			if( $params->get('changeusergroup','no') == 'yes' && !in_array($params->get('usergroup','2'), $user->groups) ) {
				$gid = $params->get('usergroup','2');
				$user->groups = array($gid=>$gid);
				if( $user->save() )
					$groupmsg = JText::_('Usergroup_changed');
			}

			// loading component configuration
			/*$comparams = JComponentHelper::getParams('com_joomisp');
			$adminmail = $comparams->get( 'adminmail');		

			$mailer = JFactory::getMailer();
			$mailer->addRecipient($adminmail);
			$mailer->setSubject('IPN: Incoming Payment');
			$mailer->setBody("Order No. ".$orderid."\n".$groupmsg."\nAssigned Status: ".$state->title);
			$mailer->send();*/
		}	
	}
	

	public function joomisp_return($order_number,$result){
		
		$slmsg 			= new ccIdealplatformController();
		$statuslayout 	= $slmsg->statuslayout($result);
	}

//Virtuemart
	public function virtuemart_notify($order_number,$paid_status,$transaction_id){	

		$db 	= JFactory::getDBO();

		$query 				= "SELECT payment_custom FROM #__ccidealplatform_payments WHERE order_id = '".$order_number."'";
		$db->setQuery($query);
		$virtuemart_paymentmethod_id = $db->loadResult();
		
		$q = "select payment_params from #__virtuemart_paymentmethods where virtuemart_paymentmethod_id = ".$virtuemart_paymentmethod_id;
		$db->setQuery($q);
		$params = $db->loadResult();
		$line=str_replace("|", "&", $params);
		$line=str_replace('"', ' ' ,$line);
		$parms = array();
		parse_str( $line,$parms );

		$order_id = $order_number;

		if ($paid_status == 1) {
			$status=trim($parms['status_success']);
		}else if($paid_status == 2){
			$status=trim($parms['status_pending']);
		}else{
			$status=trim($parms['status_canceled']);
		}
		
		$query1 = "UPDATE #__virtuemart_orders SET `order_status`='".trim($status)."' WHERE `order_number`='$order_number'";
		$db->setQuery($query1);
		$db->Query();

		JRequest::setVar( 'getiDEALSession', '');

		if(empty($_SESSION['iDEALVMPayment_SESSION'])){
			$_SESSION['iDEALVMPayment_SESSION'] = date("yidmYi");
		}else{
			$_SESSION['iDEALVMPayment_SESSION'] = "";
			$_SESSION['iDEALVMPayment_SESSION'] = date("yidmYi");
		}
		
		if(!empty($_SESSION['iDEALVMPayment_SESSION'])){
		
			$trans = "UPDATE #__ccidealplatform_payments SET trans_id='$transaction_id' WHERE order_id ='$order_number' ";
			$db->setQuery($trans);$db->Query();

			$query = "SELECT order_status FROM #__virtuemart_orders  WHERE order_number='$order_number'";
			$db->setQuery($query);
			$result = $db->loadResult();
			
			$status = trim($parms['status_success']);			

			if ($result == $status) {

				if(!empty($_SESSION['iDEALVMStatus_SESSION'])){
					$_SESSION['iDEALVMStatus_SESSION'] = "";
					$_SESSION['iDEALVMStatus_SESSION'] = "1";
				}else{
					$_SESSION['iDEALVMStatus_SESSION'] = "1";
				}				
				
				$ItemID = self::getccPaymentItemID('virtuemart');
				
				JRequest::setVar('on',$order_number);				
				JRequest::setVar('pm',$virtuemart_paymentmethod_id);
				JRequest::setVar('Itemid',$ItemID);				
				
				require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'config.php');				
				
				if (!class_exists('vmPSPlugin')){
					require(JPATH_VM_PLUGINS . DS . 'vmpsplugin.php'); JPluginHelper::importPlugin('vmpayment');
				}
				
				$dispatcher = JDispatcher::getInstance();
				$html = "";
				$paymentResponse = Jtext::_('COM_VIRTUEMART_CART_THANKYOU');				
				$returnValues = $dispatcher->trigger('plgVmOnPaymentResponseReceived', array( 'html' => &$html,&$paymentResponse));	
			}
		}
	}

	public function virtuemart_return($order_number,$result)
	{

		$db = JFactory::getDBO();
		$query = "SELECT payment_custom FROM #__ccidealplatform_payments WHERE order_id = '" . $order_number . "'";
		$db->setQuery($query);
		$virtuemart_paymentmethod_id = $db->loadResult();

		if(empty($_SESSION['iDEALVMPayment_SESSION'])){
			$_SESSION['iDEALVMPayment_SESSION'] = date("yidmYi");
		}else{
			$_SESSION['iDEALVMPayment_SESSION'] = "";
			$_SESSION['iDEALVMPayment_SESSION'] = date("yidmYi");
		}

		if (!empty($_SESSION['iDEALVMPayment_SESSION'])) {

			$query = "SELECT order_status FROM #__virtuemart_orders  WHERE order_number='" . $order_number . "'";
			$db->setQuery($query);
			$result = $db->loadResult();

			// Get payment method status preferences from Virtuemart plugin
			$q = "select payment_params from #__virtuemart_paymentmethods where virtuemart_paymentmethod_id = '" . $virtuemart_paymentmethod_id . "'";
			$db->setQuery($q);
			$params = $db->loadResult();
			$line = str_replace("|", "&", $params);
			$line = str_replace('"', ' ', $line);
			$parms = array ();
			parse_str($line, $parms);

			$status_success = trim($parms['status_success']);
			$status_pending = trim($parms['status_pending']);
			$status_canceled = trim($parms['status_canceled']);

			// Process pending orders
			if ($result == $status_pending) {
				// Empty the cart? Wait for status update? Send to VM pending page?
				unset($_SESSION['iDEALVMStatus_SESSION']);
				$statuslayout = ccIdealplatformController::statuslayout($result);
			}

			// Process canceled orders
			if ($result == $status_canceled) {
				$redirect = JFactory::getApplication();
				$redirect->redirect("index.php?option=com_virtuemart&view=pluginresponse&task=pluginUserPaymentCancel&on=$order_number&pm=$virtuemart_paymentmethod_id&Itemid=$getMyItemID");
			}

			// Process successful orders
			if ($result == $status_success) {
				$redirect = JFactory::getApplication();
				$redirect->redirect("index.php?option=com_virtuemart&view=pluginresponse&task=pluginresponsereceived&on=$order_number&pm=$virtuemart_paymentmethod_id&Itemid=$getMyItemID");
			}

		}
	}

//redShop
	public function redshop_notify($order_number,$paid_status){

		$db 	= JFactory::getDBO();

		$date=date("Yym");
		$order_number=str_replace("$date","",$order_number);

		$query = "SELECT * FROM #__redshop_orders WHERE order_id='" . $order_number . "'";
		$db->setQuery($query);
		$result = $db->loadAssoc();

		$order_total = 0;
		$order_id = $result['order_id'];

		$query = "SELECT order_payment_status FROM #__redshop_orders WHERE order_id='" . $order_number . "'";
		$db->setQuery($query);
		$status = $db->loadResult();

		$statuses = explode("\n", $status);

		if (!in_array('Paid', $statuses)) {
			$status = $status;
			$query = "UPDATE #__redshop_orders SET order_payment_status = '" . $paid_status . "',order_status='C' WHERE order_id='" . $order_number . "'";
			$db->setQuery($query);
			$db->Query();

				$query1 = "UPDATE #__redshop_order_item SET order_status = '" . $status . "' WHERE order_id='$order_id'";
				$db->setQuery($query1);
				$db->Query();

		}
		$order_id = $order_number;
		if ($paid_status) {
			$status = 'Paid';
		}else{
			$status = 'Unpaid';
		}
		$query = "UPDATE #__redshop_orders SET order_payment_status = '" . $status . "',order_status='C' WHERE order_id='" . $order_number . "'";
		$db->setQuery($query);
		$db->Query();

		$query1 = "UPDATE #__redshop_order_item SET order_status = '" . $status . "' WHERE order_id='$order_id'";
		$db->setQuery($query1);
		$db->Query();
	}

	public function redshop_return($order_number,$result){

		$date=date("Yym");
		$order_number=str_replace("$date","",$order_number);

		$slmsg 			= new ccIdealplatformController();
		$statuslayout 	= $slmsg->statuslayout($result);		

		if($result == "paid"){
			echo "<meta http-equiv='refresh' content='3;url=index.php?option=com_redshop&view=order_detail&Itemid=10&oid=$order_number'>";
		}
	}

//SobiPro
	public function sobipro_notify($order_number,$paid_status){

		$db 	= JFactory::getDBO();

		$date=date("yddy");
		$order_number=str_replace("$date","",$order_number);

		if ($paid_status) {
			$status = 1;
		}else{
			$status = 0;
		}

		$query1 = "UPDATE #__sobipro_payments SET `paid` = '" . $status . "',`datePaid`='".date("Y-m-d H:i:s")."' WHERE sid='$order_number'";
		$db->setQuery($query1);
		$db->Query();
	}

	public function sobipro_return($order_number,$result){
		
		$slmsg 			= new ccIdealplatformController();
		$statuslayout 	= $slmsg->statuslayout($result);		
	}

//Simple Caddy
	public function caddy_notify($order_number,$paid_status){

		$db 	= JFactory::getDBO();

		$status="";
		if ($paid_status) {
			$status = 'Paid';
		}else{
			$status = 'Cancelled';
		}

		$query = "UPDATE #__sc_orders SET status = '".$status."' WHERE id=".$order_number."";
		$db->setQuery($query);
		$db->Query();

		$val['order_number']=$order_number;
		$options = array( $val);

		jimport('joomla.plugin.helper');
		JPluginHelper::importPlugin('content','scideal');
		$app = JFactory::getApplication();
		$jResponse = $app->triggerEvent('showIdealSuccess',$options);
	}

	public function caddy_return($order_number,$result){
		
		$slmsg 			= new ccIdealplatformController();
		$statuslayout 	= $slmsg->statuslayout($result);	
	}

	//ccInvoices
	public function ccinvoices_notify($order_number,$paid_status,$transaction_id){

		$payment_method = "iDEAL";
		$pdate = gmdate("Y-m-d H:i:s");
		$date	= date("mddy");
		$order_number=str_replace("$date","",$order_number);

		$db 	= JFactory::getDBO();

		$query = 'SELECT inv_id FROM #__ccinvoices_payment where inv_id = "'.$order_number.'"';
		$db->setQuery( $query );
		$inv_id = $db->loadResult();

		if($paid_status)
		{
			$sql = "UPDATE #__ccinvoices_invoices SET status = 4 WHERE id = '".$order_number."'";
			$db->setQuery($sql);
			$db->query();

			// search query for id
			if($inv_id == '')
			{
				$query = 'INSERT INTO #__ccinvoices_payment ( inv_id, method , transaction_id , pdate, 	status)' .
						' VALUES ( "'.$order_number.'" ,"'.$payment_method.'","'.$transaction_id.'" ,"'.$pdate.'" ,"1" )'
						;
				$db->setQuery($query);
				$db->query();
			}
		}
	}

	public function ccinvoices_return($order_number,$result){
		
		$slmsg 			= new ccIdealplatformController();
		$statuslayout 	= $slmsg->statuslayout($result);
	}
	
	public function rsdirectory_notify($order_number,$paid_status,$transaction_id,$method = "ideal"){
	
		$g_order_number = $order_number;
		$date	= date("mdyy");
		$order_number=str_replace("$date","",$order_number);
		
		$db 	= JFactory::getDBO();

		$query = 'SELECT hash FROM #__rsdirectory_users_transactions where id = "'.$order_number.'"';
		$db->setQuery( $query );
		$hash = $db->loadResult();	
	
		// Load the RSDirectory! helper.
		require_once JPATH_ADMINISTRATOR . '/components/com_rsdirectory/helpers/rsdirectory.php';
			
		// Exit the function if no transaction was found or if the transaction was already finalized.
		$user_transaction = RSDirectoryHelper::getUserTransaction($hash);
		
		// Exit the function if no transaction was found or if the transaction was already finalized.
		if (!$user_transaction || $user_transaction->status == 'finalized')
			return;
			
			
		$query = 'SELECT IDEAL_Mode FROM #__ccidealplatform_config where id = 1';
		$db->setQuery( $query );
		$payment_mode = $db->loadResult();	
			
		// Initialize the transaction log.
		$log = array();
			
		$log[] = 'Receiving a new transaction from iDEAL.';
			
		if($payment_mode == "TEST" )
		{
			$log[] = 'Demo mode is on.';
		}		
		
		if($paid_status)
		{	
		
			$log[] = 'iDEAL reported a valid transaction.';
			$log[] = 'Payment status is Completed';		
						
			// Format the credits package price.
			$price = $user_transaction->total;				
			
			// Set the transaction params.
			$params = array();
						
			$params = "transaction_id = $transaction_id";
				
			$new_user_transaction = (object)array(
				'id' => $user_transaction->id,
				'gateway_order_number' => $g_order_number,
				'gateway_order_type' => $method,
				'gateway_params' => $params ? @implode("\n", $params) : '',
				'date_finalized' => JFactory::getDate()->toSql(),
				'finalized' => 1,
				'status' => 'finalized',
			);
				
			JFactory::getDbo()->updateObject('#__rsdirectory_users_transactions', $new_user_transaction, 'id');
			
			RSDirectoryHelper::addUserCredits($user_transaction->user_id, $user_transaction->credits);			
				
			$log[] = 'Successfully added the payment to the database.';
							
		}
		else
		{
			// Log for manual investigation.
			$log[] = "Could not verify transaction authencity. iDEAL said it's invalid.";
			$log[] = "String sent to iDEAL is Cancelled";
		}
			
		RSDirectoryHelper::saveTransactionLog($user_transaction->id, $log);	
	}
	
	public function rsdirectory_return($order_number,$result){

		$slmsg 			= new ccIdealplatformController();
		$statuslayout 	= $slmsg->statuslayout($result);
	}

	public function j2store_notify($order_number,$paid_status,$transaction_id,$method = "ideal"){	
	
		$app 				= JFactory::getApplication();
		$db 				= JFactory::getDBO();
		$orderpayment_type 	= "payment_ccideal";
		
		$date				= date("md");
		$orderpayment_id	= str_replace("$date","",$order_number);	

		$dispatcher = JDispatcher::getInstance();
		JPluginHelper::importPlugin ('j2store');

		$html = "";		

		$query 		= 'SELECT order_id FROM #__j2store_orderinfo where orderpayment_id = '.$orderpayment_id;
		$db->setQuery( $query );
		$orderinfo_id = $db->loadResult();		
		
		JTable::addIncludePath( JPATH_ADMINISTRATOR.'/components/com_j2store/tables' );
		
		$order = JTable::getInstance('Orders', 'Table');
		
		$order->load( array('id'=>$orderpayment_id));	
		
		if($order->order_id != $orderinfo_id){
			return false;
		}		

		// free product? set the state to confirmed and save the order.
		if ( (!empty($orderpayment_id)) && (float) $order->order_total == (float)'0.00' )
		{	
			$order->order_state = trim(JText::_('CONFIRMED'));
			$order->order_state_id = '1'; // PAYMENT RECEIVED.
			if($order->save()) {
				// remove items from cart
				J2StoreHelperCart::removeOrderItems( $order->id );
			}
			//send email
			require_once (JPATH_SITE.'/components/com_j2store/helpers/orders.php');
			J2StoreOrdersHelper::sendUserEmail($order->user_id, $order->order_id, $order->transaction_status, $order->order_state, $order->order_state_id);

		}
		else
		{			
			$errors = array();
			$payment_status = JText::_('J2STORE_INCOMPLETE');
			$user = JFactory::getUser();
			
			switch($paid_status)
            {
                case '1':
                  // Approved
                   $payment_status = JText::_('J2STORE_COMPLETED');

                break;

                case '0':                 
				{
                     // Declined
                    $payment_status = JText::_('J2STORE_DECLINED');
                    $errors[] 		= "Cancelled";
                }
                break;

				default:
				   // Error
					$payment_status = JText::_('J2STORE_ERROR');
					$order_status 	= JText::_('J2STORE_INCOMPLETE');
					$errors[] 		= JText::_( "J2STORE_CCIDEAL_ERROR_PROCESSING_PAYMENT" );
				  break;
			}           
			
			//$order->transaction_details  = $this->_getFormattedTransactionDetails($resp);
            $order->transaction_id       = $transaction_id;
            $order->transaction_status   = $payment_status;

            //set a default status to it
			$order->order_state = JText::_('J2STORE_PENDING'); // PENDING
			$order->order_state_id = 4; // PENDING

            // set the order's new status and update quantities if necessary
            if(count($errors))
            {
                // if an error occurred
                $order->order_state  = trim(JText::_('J2STORE_FAILED')); // FAILED
                $order->order_state_id = 3; // FAILED
            }else{			
				$order->order_state  = trim(JText::_('J2STORE_CONFIRMED')); // Payment received and CONFIRMED
				$order->order_state_id = 1; // CONFIRMED			
            }

            // save the order
            if (!$order->save())
            {
                $errors[] = $order->getError();
            }

            if (empty($errors))
            {
            	 // let us inform the user that the payment is successful
        		require_once (JPATH_SITE.'/components/com_j2store/helpers/orders.php');
        		J2StoreOrdersHelper::sendUserEmail($order->user_id, $order->order_id, $payment_status, $order->order_state, $order->order_state_id);

                $return = JText::_( "J2STORE_CCIDEAL_MESSAGE_PAYMENT_SUCCESS" );
                return $return;
            } else {
            	$error = count($errors) ? implode("\n", $errors) : '';
            	$this->_sendErrorEmails($error, $order->transaction_details);
            }
		}		
		return;
	}
	
	public function j2store_return($order_number,$result){		
		
		$date				= date("md");
		$orderpayment_id	= str_replace("$date","",$order_number);	
		
		require_once (JPATH_SITE.'/components/com_j2store/helpers/cart.php');		
				
		// remove items from cart 
		J2StoreHelperCart::removeOrderItems( $orderpayment_id );				

		$slmsg 			= new ccIdealplatformController();
		$statuslayout 	= $slmsg->statuslayout($result);
	}
	
	public function techjoomla_notify($order_number,$paid_status,$transaction_id,$method = "ideal"){
		
		$date	= date("dm");
		$order_number=str_replace("$date","",$order_number);
		
		$db 		= JFactory::getDBO();
		$post 		= array();		
		
		$query 		= "SELECT payment_total,article_extra_text,payment_custom FROM #__ccidealplatform_payments WHERE order_id = '".$order_number."'";
		$db->setQuery( $query );
		$payments = $db->loadObject();	
		
		$post['order_id'] 		= $order_number;
		$post['txn_id'] 		= $transaction_id;
		$post['txn_type'] 		= $method;
		$post['gross_total'] 	= $payments->payment_total;
		$post['payment_status'] = $paid_status;			
		
		$post['plugin_payment_method'] = "";
		
		$client 				= $payments->article_extra_text;
		$payment_custom			= $payments->payment_custom;
		$custom_payment 		= explode(':',$payment_custom);			
		
		if(empty($client)){
			$client 			= $custom_payment[0];
		}		
		
		// Load the language files
		$jlang = JFactory::getLanguage();
		
		if($client == "jticketing"){		
			require_once JPATH_SITE . '/components/com_jticketing/models/payment.php';
			require_once JPATH_SITE . '/components/com_jticketing/helpers/main.php';			
			
			$jlang->load('com_jticketing', JPATH_SITE, 'en-GB', true);
			$jlang->load('com_jticketing', JPATH_SITE, $jlang->getDefault(), true);	
			
			$payment_ccideal 	= new jticketingModelpayment();
			
			$response 			= $payment_ccideal->processPayment($post,'ccideal',$order_number);
			
		}elseif($client == "com_jgive"){			
			require_once JPATH_SITE . '/components/com_jgive/models/donations.php';
			require_once JPATH_SITE . '/components/com_jgive/helper.php';
			require_once JPATH_SITE . '/components/com_jgive/helpers/campaign.php';
			
			$jlang->load('com_jgive', JPATH_SITE, 'en-GB', true);
			$jlang->load('com_jgive', JPATH_SITE, $jlang->getDefault(), true);			
			
			$payment_ccideal 	= new jgiveModelDonations();
			
			$response 			= $payment_ccideal->processPayment($post,'ccideal',$order_number);
			
		}elseif($client == "socialads"){		
		
			$jlang->load('com_socialads', JPATH_SITE, 'en-GB', true);
			$jlang->load('com_socialads', JPATH_SITE, $jlang->getDefault(), true);
			
			require_once JPATH_SITE . '/components/com_socialads/helper.php';
		
			if(trim($custom_payment[1]) == "showad"){
				require_once JPATH_SITE . '/components/com_socialads/models/showad.php';
				
				if($custom_payment[3] == 1){
					JRequest::setVar('adminCall','1');
				}
				
				$payment_ccideal 	= new socialadsModelShowad();				
				$response 			= $payment_ccideal->processPayment($post,'ccideal','onTP_Processpayment',$order_number);
			}
			
			if(trim($custom_payment[1]) == "payment"){			
				
				require_once JPATH_SITE . '/components/com_socialads/models/payment.php';
				
				$payment_ccideal 	= new socialadsModelpayment();				
				$response 			= $payment_ccideal->processPayment($post,'ccideal','onTP_Processpayment',$order_number,$custom_payment[2]);				
			}		
		}elseif($client == "com_quick2cart"){		
			
			require_once JPATH_SITE . '/components/com_quick2cart/models/payment.php';
			require_once JPATH_SITE . '/components/com_quick2cart/models/cart.php';
			require_once JPATH_SITE . '/components/com_quick2cart/controller.php';			
			
			$jlang->load('com_quick2cart', JPATH_SITE, 'en-GB', true);
			$jlang->load('com_quick2cart', JPATH_SITE, $jlang->getDefault(), true);			
			
			$payment_ccideal 	= new Quick2cartModelpayment();
			//For cart declaration
			new Quick2cartModelcart();	
			
			$response 			= $payment_ccideal->processPayment($post,'ccideal',$order_number);		
		}
		
		//$app->redirect($response['return'],$response['msg']);			
	}

	public function techjoomla_return($order_number,$result){

		$slmsg 			= new ccIdealplatformController();
		$statuslayout 	= $slmsg->statuslayout($result);
	}	

//End

	//Offline Email

	public function notifyMailProcess()
	{
		$mail = JFactory::getMailer();
		$app = JFactory::getApplication();
		$db = JFactory :: getDBO();
		$FromName= $app->getCfg('sitename');
		$MailFrom= $app->getCfg('mailfrom');
		$sitename= $app->getCfg('sitename');

		$select = "SELECT emailnotify,emailsss,emailsubject,emailbody FROM #__ccidealplatform_config WHERE id = '1'";
		$db->setQuery($select);
		$rows = $db->loadObject();
		if(trim($rows->emailnotify)=="1")
		{
			$emails = explode(",",$rows->emailsss);
			$email_subject=$rows->emailsubject;
			$email_body=$rows->emailbody;

			// David - 15 July 2015 - cciDEAL 4.4.9
			// Use default mail, subject and body if people enable
			// email notifications but don't add (required) information
			if (empty($rows->emailsss)) { $emails[0] = $MailFrom; }
			if (empty($email_subject)) { $email_subject = "Betaling gestart op " . $sitename; }
			if (empty($email_body)) { $email_body = "Iemand is gestart met een betaling via cciDEAL op " . $sitename . ". Let op: er is wellicht nog niet betaald, alleen gestart! Deze e-mail kun je aanpassen in Componenten > cciDEAL > E-mail instellingen.";}

			$mail->addRecipient($emails);
			$mail->setSender(array($MailFrom, $FromName));
			$mail->setSubject($email_subject);
			$mail->setBody($email_body);
			$sent = $mail->Send();
		}
	}


	/** _sendRequest functions for sending request to the bank **/


	function _sendRequest($host, $port, $path, $data) {

			$hostname = str_replace('ssl://', '', $host);
			$fp = @ fsockopen($host, $port, $errno, $errstr);
			$buf = '';

		if (!$fp) {
			$this->error_message = 'Kon geen verbinding maken met server: ' . $errstr;
			$this->error_code = 0;

		return false;
		}

			@ fputs($fp, "POST $path HTTP/1.0\n");
			@ fputs($fp, "Host: $hostname\n");
			@ fputs($fp, "Content-type: application/x-www-form-urlencoded\n");
			@ fputs($fp, "Content-length: " . strlen($data) . "\n");
			@ fputs($fp, "Connection: close\n\n");
			@ fputs($fp, $data);

		while (!feof($fp)) {
			$buf .= fgets($fp, 128);
		}

		fclose($fp);

		if (empty ($buf)) {
		$this->error_message = 'Zero-sized reply';
		return false;
		} else
		list ($headers, $body) = preg_split("/(\r?\n){2}/", $buf, 2);

		return $body;
	}


	/**
	 *
	 * checkPayment functions for check whether the payment is payed or not
	 *
	 */


	function checkPayment($transaction_id) {

		if (!$this->setTransactionId($transaction_id)) {
		$this->error_message = "Er is een onjuist transactie ID opgegeven";
		return false;
		}

		$check_xml = $this->_sendRequest($this->api_host, $this->api_port, '/xml/ideal/', 'a=check' .
		'&partnerid=' . urlencode($this->getPartnerId()) .
		'&transaction_id=' . urlencode($this->getTransactionId()) .
		(($this->testmode) ? '&testmode=true' : ''));

		if (empty ($check_xml))
		return false;

		$check_object = $this->_XMLtoObject($check_xml);

		if (!$check_object || $this->_XMLisError($check_object))
		return false;

		$this->status 	= $check_object->order->status;

		$this->paid_status = ($check_object->order->payed == 'true');
		$this->amount = $check_object->order->amount;
		$this->consumer_info = (isset ($check_object->order->consumer)) ? (array) $check_object->order->consumer : array ();

		return true;
	}


	/**
	 *
	 * setpartnerid functions for checking the partner id
	 *
	 */


	function setpartnerid($partner_id, $api_host = 'ssl://secure.mollie.nl', $api_port = 443) {
		$this->partner_id = $partner_id;
		$this->api_host = $api_host;
		$this->api_port = $api_port;
	}


	function _XMLtoObject($xml) {
		try {
		$xml_object = new SimpleXMLElement($xml);
		if ($xml_object == false) {
		$this->error_message = "Kon XML resultaat niet verwerken";
		return false;
		}
		} catch (Exception $e) {
		return false;
		}

		return $xml_object;
	}

	protected function _XMLisError($xml) {

		if (isset ($xml->item)) {
			$attributes = $xml->item->attributes();

			if ($attributes['type'] == 'error') {
				$this->error_message = (string) $xml->item->message;
				$this->error_code = (string) $xml->item->errorcode;

				return true;
			}
		}
		return false;
	}

	public function getPartnerId() {
		return $this->partner_id;
	}

	public function setTestmode($enable = true) {
		return ($this->testmode = $enable);
	}

	public function setBankId($bank_id) {
		if (!is_numeric($bank_id))
			return false;

		return ($this->bank_id = $bank_id);
	}

	public function getBankId() {
		return $this->bank_id;
	}

	public function setAmount($amount) {

		$amount = $amount*100;

		if (!preg_match('~^[0-9]+$~', $amount)) {
			return false;
		}

		if (self::MIN_TRANS_AMOUNT >= $amount) {
			return false;
		}

		$amount = $amount/100;

		return ($this->amount = $amount);
	}

	public function getAmount() {

		return $this->amount;
	}

	public function setDescription($description) {
		$description = substr($description, 0, 29);

		return ($this->description = $description);
	}

	public function getDescription() {
		return $this->description;
	}

	public function setReturnURL($return_url) {
		if (!preg_match('|(\w+)://([^/:]+)(:\d+)?(.*)|', $return_url)) {

			return false;
		}

		return ($this->return_url = $return_url);
	}

	public function getReturnURL() {
		return $this->return_url;
	}

	/*public function setReportURL($report_url) {
		return $this->report_url;
	}

	public function getReportURL() {
		return $this->report_url;
	}*/

	public function setTransactionId($transaction_id) {
		if (empty ($transaction_id))
			return false;

		return ($this->transaction_id = $transaction_id);
	}

	public function getTransactionId() {
		return $this->transaction_id;
	}

	public function getBankURL() {
		return $this->bank_url;
	}

	public function getPaidStatus() {
		return $this->paid_status;
	}

	public function getConsumerInfo() {
		return $this->consumer_info;
	}

	public function getErrorMessage() {
		return $this->error_message;
	}

	public function getErrorCode() {
		return $this->error_code;
	}

	public function setProfilekey($profilekey) {
		return ($this->profile_key = $profilekey);
	}
	public function getProfilekey() {
		return $this->profile_key;
	}

	//OmniKassa verification

	function getLanguageCode()
	{
		$user = JFactory::getUser();

		$languageCode="en";

			$newarra=explode("\n",$user->get( 'params' ));
			$i=0;
			$flag=false;
			foreach($newarra as $newarras)
			{
				$newarra1[$i]=explode("=",$newarras);
				if(trim($newarra1[$i][0])=="language")
				{
					$languageCode=substr($newarra1[$i][1],0,2);
					if(strlen(trim($languageCode))>0)
					{
						$flag=true;
					}
				}
				$i++;
			}
			if($flag==false OR trim($languageCode)=="")
			{
				$lang = JFactory::getLanguage();
				$languageCode=substr($lang->getTag(),0,2);
			}

			$languageCode=$this->validLanguageCode($languageCode);

		return $languageCode;
	}
	function validLanguageCode($code)
	{
		if($code=="en")
			$code="en";
		elseif($code=="fr")
			$code="fr";
		elseif($code=="de")
			$code="de";
		elseif($code=="it")
			$code="it";
		elseif($code=="es")
			$code="es";
		elseif($code=="nl")
			$code="nl";
		else
			$code="en";

		return $code;
	}

	// Generate transactionReference for Rabo OmniKassa
	// and possibly other PSPs
	public function random_generator($digits)
	{
		srand ((double) microtime() * 10000000);
		//Array of number
		$input = array ("1","2","3","4","5","6","7","8","9","0","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P"
		,"Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t"
		,"u","v","w","x","y","z");

		$random_generator="";// Initialize the string to store random numbers
		for($i=1;$i<$digits+1;$i++){ // Loop the number of times of required digits

		if(rand(1,2) == 1){// to decide the digit should be numeric or alphabet
		// Add one random alphabet
		$rand_index = array_rand($input);
		$random_generator .=$input[$rand_index]; // One char is added

		}else{

		// Add one numeric digit between 1 and 10
		$random_generator .=rand(1,10); // one number is added
		} // end of if else

		} // end of for loop

		return $random_generator;
	}
	
	function raboomnikassa_notify() {		

		$db = JFactory::getDBO();		

		$data1				= explode("|",@JRequest::getVar('Data','','post'));
		
		$newData=array();
		foreach($data1 as $data2)
		{
			$tmpval=explode("=",$data2);
			@$newData[$tmpval[0]]=$tmpval[1];			
		}

		$transaction_id = @$newData['transactionReference'];
		$method			= @$newData['paymentMeanBrand'];
		$order_number	= @$newData['orderId'];

		if(count(JRequest::get('post'))=="0" OR JRequest::get('post')==NULL OR trim(JRequest::getVar('Data','','post'))=="")
		{
			echo JText::_('CCOMNIKASSA_AUTH');
		}else{
			//successfull payment
			if(@$newData['responseCode']=="00"){
				$pay_status = "Success";
			}else if(@$newData['responseCode']=="17"){
				$pay_status = "Cancelled";				
			}else if(@$newData['responseCode']=="60"){
				$pay_status = "Pending";				
			}else{
				$pay_status = "Failed";
			}
		}
		
		$query = "SELECT extension FROM #__ccidealplatform_payments WHERE order_id = '".$order_number."'";
		$db->setQuery($query);
		$component = $db->loadResult();

		$this->ccideal_notify($pay_status,$transaction_id,$method,$order_number,$component);
	}

	function raboomnikassa_return() {
	
		$db = JFactory::getDBO();

		$data1=explode("|",@JRequest::getVar('Data','','post'));
		$newData=array();
		foreach($data1 as $data2)
		{
			$tmpval=explode("=",$data2);
			@$newData[$tmpval[0]]=$tmpval[1];
		}
		
		$order_number = @$newData['orderId'];	
		
		$query = "SELECT extension FROM #__ccidealplatform_payments WHERE order_id = '".$order_number."'";
		$db->setQuery($query);
		$component = $db->loadResult();

		$this->ccideal_return($component,$order_number);
	}
	
	
	function sisow_process() {

		$db 			= JFactory::getDBO();	 	
		$app 			= JFactory::getApplication();
		# Set ideal amount in cents so 500 cent will be 5 euro
		
		$component 		= JRequest::getVar('extn');
	 	$amount 		= JRequest::getVar('amount');
	 	$order_id 		= JRequest::getVar('item_number');
	 	$description 	= JRequest:: getVar('description');		
		@$getiDEALSession 	= JRequest::getVar('iDEALVMPayment_SESSION');
		$retry_payment 		= JRequest::getVar('retry');
		
		//Payment total security checking for sisow payment
		
		$query = "SELECT payment_total FROM #__ccidealplatform_payments WHERE order_id = '".$order_id."'";
		$db->setQuery($query);
		$payment_total = $db->loadResult();

		if($amount != $payment_total)
		{
			$amount = $payment_total;
		}

 //For menu Item ID
       $app = JFactory::getApplication();
       $menu = $app->getMenu();
       $menuItem = $menu->getActive();
       $itemid = $menuItem->id;
	   
		//For Virtuemart
		if(!empty($getiDEALSession)){
			$update_params = "UPDATE #__ccidealplatform_payments SET `cciDEAL_params`='$getiDEALSession' WHERE `order_id`='$order_id '";
			$db->setQuery($update_params);$db->Query();
		}		

	 	$query = "SELECT * FROM #__ccidealplatform_config";
		$db->setQuery($query);
		$ideal_config = $db->loadObject();
		
		$sisow_merchantid = $ideal_config->sisow_merchantid;
		$sisow_merchantkey = $ideal_config->sisow_merchantkey;

		$ideal_paymentmethod 	= JRequest::getVar('ideal_paymentmethod', 'ideal');		

		# Set return url from your website
		$iReturnurl = JURI::ROOT()."index.php?option=com_ccidealplatform&task=sisow_return&com=".$component;

		# Set report url to recieve the status of transactions not retreived by the returnurl
		$notifyUrl = JURI::ROOT()."index.php?option=com_ccidealplatform&task=sisow_notify&extn=".$component."&paymentmethod=".$ideal_paymentmethod;		

		$amount = $amount*100;		

		require_once(JPATH_COMPONENT . '/ccideal_sisow/sisow.php');
		
		$sisow = new Sisow($sisow_merchantid, $sisow_merchantkey);
		
		if($ideal_config->IDEAL_Mode == "TEST"){
			$_POST["issuerid"] = "99";
		}	
		
		$sisow->purchaseId = $order_id;
		$sisow->description = $description;
		$sisow->amount = $amount;
		$sisow->payment = $ideal_paymentmethod;
		$sisow->issuerId = @$_POST["issuerid"];
		$sisow->returnUrl = $iReturnurl;
		$sisow->notifyUrl = $notifyUrl;
		
		if (($ex = $sisow->TransactionRequest()) < 0) {
			echo "Exception code: ".$ex."<br />Error code: ".$sisow->errorCode."<br />Error message: ".$sisow->errorMessage;
			//header("Location: ".$error_url."&ex=" . $ex . "&ec=" . $sisow->errorCode . "&em=" . $sisow->errorMessage);
			exit;
		}			
		header("Location: " . $sisow->issuerUrl);
		exit;
		
	}
	
	function sisow_notify() {

		$db = JFactory::getDBO();		
		
		if(isset($_GET['ec'])){
			$order_number = $_GET['ec'];
		}
		if(isset($_GET['trxid'])){
			$trxid = $_GET['trxid'];
		}
		
		$query = "SELECT * FROM #__ccidealplatform_config";
		$db->setQuery($query);
		$ideal_config = $db->loadObject();
		
		$sisow_merchantid = $ideal_config->sisow_merchantid;
		$sisow_merchantkey = $ideal_config->sisow_merchantkey;
		
		require_once(JPATH_COMPONENT . '/ccideal_sisow/sisow.php');
			
		$sisow = new Sisow($sisow_merchantid, $sisow_merchantkey);

		$sisow->StatusRequest($_GET["trxid"]);
		
		$component			= $_GET["extn"];
		$method				= $_GET["paymentmethod"];		

		if ($sisow->status == "Success"){
			$pay_status = "Success";
		}else{
			$pay_status = "Cancelled";
		}

		$this->ccideal_notify($pay_status,$trxid,$method,$order_number,$component);
	}
	
	function sisow_return() {

		$component 		= JRequest::getVar('com');
		$order_number 	= JRequest::getVar('ec');		

		$this->ccideal_return($component,$order_number);
	}
	
	function targetpay_notify(){
		
		$order_number 	= JRequest::getVar('orderID');		
		$tmp_orderID	= $order_number;
		$transaction_id	= JRequest::getVar('trxid');
		$transactionid	= $transaction_id;
		$ec				= JRequest::getVar('ec');
		$custom			= "";

		$db = JFactory::getDBO();

		//Get the Payment details
		$query1 = "SELECT * FROM #__ccidealplatform_payments WHERE `order_id`='$order_number'";
		$db->setQuery($query1);
		$payment_data = $db->loadObject();

		//Get the configuration details
		$query = "SELECT * FROM #__ccidealplatform_config ";
		$db->setQuery($query);
		$conf_data = $db->loadObject();

		//test Mode
		if($conf_data->IDEAL_Mode == "TEST"){
			$iTest = 1;
		}else{
			$iTest = 0;
		}	
		
		$component 	= $payment_data->extension;
		$iRtlo 		= $conf_data->IDEAL_Targetpay_ID;
		$pay_method	= $payment_data->pay_method;
		
		//Same order status can be break using this code
		
		$query = "SELECT order_id FROM #__ccidealplatform_payments WHERE payment_status='paid' AND `trans_id`='$transaction_id'";
		$db->setQuery($query);
		$status_check = $db->loadResult();

		if($status_check){
			return false;
		}
			
		if($payment_data->cciDEAL_params != 1 || empty($payment_data->cciDEAL_params)){ //check the status,if already updted or not.
		
			if($pay_method == "ideal"){
				
				if(isset($transaction_id) && isset ($ec)) {

					require_once( JPATH_COMPONENT.'/ccideal_targetpay/targetpayideal.class.php' );
					
					# Init the class
					$oIdeal = new TargetPayIdeal ( $iRtlo );			

					if($oIdeal->validatePayment($transaction_id,$iOnce = 0, $iTest) == true) {
					
						$paid_status = "1";
						
					}else{
						$paid_status = "0";
					}
				}
		
			}elseif($pay_method == "mistercash"){
			
				if(isset($transaction_id)) {
				
					require_once( JPATH_COMPONENT.'/ccideal_targetpay/targetpaymrcash.class.php' );
					
					# Init the class
					$oMrCash = new TargetPayMrCash ( $iRtlo );			

					if($oMrCash->validatePayment($transaction_id,$iOnce = 0, $iTest) == true) {
					
						$paid_status = "1";
						
					}else{
						$paid_status = "0";
					}
				}		
			}elseif($pay_method == "sofort"){	
				if(isset($transaction_id)) {
				
					require_once( JPATH_COMPONENT.'/ccideal_targetpay/targetpaysofort.class.php' );
					
					# Init the class
					$obj = new TargetPaySofort ( $iRtlo );			

					if($obj->validatePayment($transaction_id,$iOnce = 0, $iTest) == true) {
					
						$paid_status = "1";
						
					}else{
						$paid_status = "0";
					}
				}		
			}						

			if($paid_status == "1"){				
				$pay_status = "Success";
			}else{					
				$pay_status = "Cancelled";
			}
			
			$this->ccideal_notify($pay_status,$transaction_id,$pay_method,$order_number,$component);		

			$redirect = JFactory::getApplication();					
			$redirect->redirect(JRoute::_('index.php?option=com_ccidealplatform&task=targetpay_return&component='.$component.'&order_number='.$order_number));
		}		
	}
	public function targetpay_return(){	
	
		$order_number 	= JRequest::getVar('order_number');
		$component 		= JRequest::getVar('component');
		
		$db 			= JFactory::getDBO();
		//Upadte the params
					
		$query1 = "UPDATE #__ccidealplatform_payments SET `cciDEAL_params` = '1' WHERE order_id='".$order_number."'";
		$db->setQuery($query1);
		$db->Query();
			
		$this->ccideal_return($component,$order_number);
	}
}
?>