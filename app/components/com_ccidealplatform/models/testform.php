<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

$document = JFactory :: getDocument();
$document->addStyleSheet('components/com_ccidealplatform/assets/css/style.css');

// extend the JController class for newsletter specific tasks
class ccIdealplatformModeltestform extends JModelLegacy {

	var $_query;
	var $_data;
	var $_total=null;
	var $_pagination=null;

	function __construct()
	{
		parent::__construct();

			$this->_buildQuery();
			$mainframe = JFactory::getApplication();

			// Get pagination request variables
			$limit = $mainframe->getUserStateFromRequest('com_ccidealplatform.testform.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
			$limitstart = $mainframe->getUserStateFromRequest('com_ccidealplatform.testform.limitstart', 'limitstart', 0, 'int');

			// In case limit has been changed, adjust it
			$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

			$this->setState('com_ccidealplatform.testform.limit', $limit);
			$this->setState('com_ccidealplatform.testform.limitstart', $limitstart);

	}

	function _buildQuery()
	{
		$mainframe = JFactory::getApplication();
		$db = JFactory::getDBO();

		$filter				= JRequest::getVar('jwd_filter');
		$sortColumn		= $mainframe->getUserStateFromRequest('com_ccidealplatform.testform.sortColumn','filter_order','ordering');
		$sortOrder		= $mainframe->getUserStateFromRequest('com_ccidealplatform.testform.sortOrder','filter_order_Dir','asc');
		$mainframe->setUserState('com_ccidealplatform.testform.sortColumn',$sortColumn);
		$mainframe->setUserState('com_ccidealplatform.testform.sortOrder',$sortOrder);
		$this->_query="SELECT * FROM #__jwd_students WHERE name LIKE '%".$db->getEscaped($filter)."%' ORDER BY $sortColumn $sortOrder";

	}

	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}

	function &getData()
	{
		$row =& $this->getTable();
		$row->load( $this->_id );
		return $row;

	}

	function getPagination()
	{
		if (empty($this->_pagination))
		{
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('com_ccidealplatform.testform.limitstart'), $this->getState('com_jwdintermediate.students.limit'));
		}
		return $this->_pagination;
	}

}
?>
