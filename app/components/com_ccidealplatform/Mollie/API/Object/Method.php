<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Mollie_API_Object_Method
{
	const IDEAL        = "ideal";
	const PAYSAFECARD  = "paysafecard";
	const CREDITCARD   = "creditcard";
	const MISTERCASH   = "mistercash";
	const BANKTRANSFER = "banktransfer";
	const PAYPAL       = "paypal";

	/**
	 * Id of the payment method.
	 *
	 * @var string
	 */
	public $id;

	/**
	 * More legible description of the payment method.
	 *
	 * @var string
	 */
	public $description;

	/**
	 * The $amount->minimum and $amount->maximum supported by this method and the used API key.
	 *
	 * @var object
	 */
	public $amount;

	/**
	 * The $image->normal and $image->bigger to display the payment method logo.
	 *
	 * @var object
	 */
	public $image;

	/**
	 * @return float|null
	 */
	public function getMinimumAmount ()
	{
		if (empty($this->amount))
		{
			return NULL;
		}

		return (float) $this->amount->minimum;
	}

	/**
	 * @return float|null
	 */
	public function getMaximumAmount ()
	{
		if (empty($this->amount))
		{
			return NULL;
		}

		return (float) $this->amount->maximum;
	}
}
