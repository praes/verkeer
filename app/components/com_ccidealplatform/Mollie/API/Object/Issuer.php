<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Mollie_API_Object_Issuer
{
	/**
	 * Id of the issuer.
	 *
	 * @var string
	 */
	public $id;

	/**
	 * Name of the issuer.
	 *
	 * @var string
	 */
	public $name;

	/**
	 * The payment method this issuer belongs to.
	 *
	 * @see Mollie_API_Object_Method
	 * @var string
	 */
	public $method;
}
