<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Mollie_API_Object_List extends ArrayObject
{
	/**
	 * Total number of available objects on the Mollie platform.
	 *
	 * @var int
	 */
	public $totalCount;

	/**
	 * Numeric offset from which this list of object was created.
	 *
	 * @var int
	 */
	public $offset;

	/**
	 * Total number of retrieved objects.
	 *
	 * @var int
	 */
	public $count;
}
