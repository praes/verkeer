<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Mollie_API_Object_Payment_Refund
{
	/**
	 * Id of the payment method.
	 *
	 * @var string
	 */
	public $id;

	/**
	 * The $amount that was refunded.
	 *
	 * @var float
	 */
	public $amountRefunded;

	/**
	 * The $amount that is remaining on the payment.
	 *
	 * @var float
	 */
	public $amountRemaining;

	/**
	 * The payment that was refunded.
	 *
	 * @var Mollie_API_Object_Payment
	 */
	public $payment;

	/**
	 * Date and time the payment was cancelled in ISO-8601 format.
	 *
	 * @var string|null
	 */
	public $refundedDatetime;
}
