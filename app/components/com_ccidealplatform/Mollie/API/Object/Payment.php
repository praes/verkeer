<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Mollie_API_Object_Payment
{
	const STATUS_OPEN      = "open";
	const STATUS_CANCELLED = "cancelled";
	const STATUS_EXPIRED   = "expired";
	const STATUS_PAID      = "paid";

	/**
	 * Id of the payment (on the Mollie platform).
	 *
	 * @var string
	 */
	public $id;

	/**
	 * Mode of the payment, either "live" or "test" depending on the API Key that was used.
	 *
	 * @var string
	 */
	public $mode;

	/**
	 * The amount of the payment in EURO with 2 decimals.
	 *
	 * @var float
	 */
	public $amount;

	/**
	 * Description of the payment that is shown to the customer during the payment, and
	 * possibly on the bank or credit card statement.
	 *
	 * @var string
	 */
	public $description;

	/**
	 * If method is empty/null, the customer can pick his/her preferred payment method.
	 *
	 * @see Mollie_API_Object_Method
	 * @var string|null
	 */
	public $method;

	/**
	 * The status of the payment.
	 *
	 * @var string
	 */
	public $status = self::STATUS_OPEN;

	/**
	 * Date and time the payment was created in ISO-8601 format.
	 *
	 * @example "2013-12-25T10:30:54.0Z"
	 * @var string|null
	 */
	public $createdDatetime;

	/**
	 * Date and time the payment was paid in ISO-8601 format.
	 *
	 * @var string|null
	 */
	public $paidDatetime;

	/**
	 * Date and time the payment was cancelled in ISO-8601 format.
	 *
	 * @var string|null
	 */
	public $cancelledDatetime;

	/**
	 * Date and time the payment was cancelled in ISO-8601 format.
	 *
	 * @var string|null
	 */
	public $expiredDatetime;

	/**
	 * During creation of the payment you can set custom metadata that is stored with
	 * the payment, and given back whenever you retrieve that payment.
	 *
	 * @var object|mixed|null
	 */
	public $metadata;

	/**
	 * Details of a successfully paid payment are set here. For example, the iDEAL
	 * payment method will set $details->consumerName and $details->consumerAccount.
	 *
	 * @var object
	 */
	public $details;

	/**
	 * @var object
	 */
	public $links;

	/**
	 * Is this payment still open / ongoing?
	 *
	 * @return bool
	 */
	public function isOpen ()
	{
		return $this->status == self::STATUS_OPEN;
	}

	/**
	 * Is this payment paid for?
	 *
	 * @return bool
	 */
	public function isPaid ()
	{
		return !empty($this->paidDatetime);
	}

	/**
	 * Get the payment URL where the customer can complete the payment.
	 *
	 * @return string|null
	 */
	public function getPaymentUrl ()
	{
		if (empty($this->links->paymentUrl))
		{
			return NULL;
		}

		return $this->links->paymentUrl;
	}
}
