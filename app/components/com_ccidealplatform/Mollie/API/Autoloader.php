<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Mollie_API_Autoloader
{
	/**
	 * @param string $class_name
	 */
	public static function autoload ($class_name)
	{
		if (strpos($class_name, "Mollie_") === 0)
		{			
			$file_name = str_replace("_", "/", $class_name);
			$file_name = realpath(dirname(__FILE__) . "/../../{$file_name}.php");	

			if (file_exists($file_name))
			{
				require $file_name;
			}
		}
	}

	/**
	 * @return bool
	 */
	public static function register ()
	{
		return spl_autoload_register(array(__CLASS__, "autoload"));
	}

	/**
	 * @return bool
	 */
	public static function unregister ()
	{
		return spl_autoload_unregister(array(__CLASS__, "autoload"));
	}
}

Mollie_API_Autoloader::register();
