<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Mollie_API_Resource_Methods extends Mollie_API_Resource_Base
{
	/**
	 * @return Mollie_API_Object_Method
	 */
	protected function getResourceObject ()
	{
		return new Mollie_API_Object_Method;
	}
}
