<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Mollie_API_Resource_Payments_Refunds extends Mollie_API_Resource_Base
{
	/**
	 * @var string
	 */
	private $payment_id;

	/**
	 * @return Mollie_API_Object_Method
	 */
	protected function getResourceObject ()
	{
		return new Mollie_API_Object_Payment_Refund;
	}

	/**
	 * @return string
	 */
	protected function getResourceName ()
	{
		return "payments/" . urlencode($this->payment_id) . "/refunds";
	}

	/**
	 * Set the resource to use a certain payment. Use this method before performing a get() or all() call.
	 *
	 * @param Mollie_API_Object_Payment $payment
	 * @return self
	 */
	public function with(Mollie_API_Object_Payment $payment)
	{
		$this->payment_id = $payment->id;
		return $this;
	}
}
