<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Mollie_API_Resource_Payments extends Mollie_API_Resource_Base
{
	/**
	 * @return Mollie_API_Object_Payment
	 */
	protected function getResourceObject ()
	{
		return new Mollie_API_Object_Payment;
	}

	/**
	 * @param Mollie_API_Object_Payment $payment
	 * @return Mollie_API_Object_Payment_Refund
	 */
	public function refund (Mollie_API_Object_Payment $payment)
	{
		$resource = "{$this->getResourceName()}/" . urlencode($payment->id) . "/refunds";

		$result = $this->performApiCall(self::REST_CREATE, $resource, new stdClass());

		return $this->copy($result, new Mollie_API_Object_Payment_Refund);
	}
}
