<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

## no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Mollie_API_Exception extends Exception
{
	/**
	 * @var string
	 */
	protected $_field;

	/**
	 * @return string
	 */
	public function getField ()
	{
		return $this->_field;
	}

	/**
	 * @param string $field
	 */
	public function setField ($field)
	{
		$this->_field = (string) $field;
	}
}
