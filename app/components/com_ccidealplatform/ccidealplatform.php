<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/


// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

// Require the base controller

require_once (JPATH_COMPONENT.DS.'controller.php');

// Require specific controller if requested
if($controller = JRequest::getVar('controller')) {
	require_once (JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php');
}

$jversion 			= new JVersion();
$current_version 	= $jversion->getShortVersion();
$jversion_sht		= substr($current_version,0,1);

	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return substr($current_version,0,1);
	}

//Styles & Script
$document = JFactory :: getDocument();
$document->addStyleSheet('components/com_ccidealplatform/assets/css/style.css');
$document->addScript('components/com_ccidealplatform/assets/js/ccvalidate.js');

	// David - 2 April 2015 - cciDEAL 4.4.4
	// Removing Akeeba Strapper and FOF, adding ccBootstrap
	if(versionCompare()<3)
	{
		require_once JPATH_ROOT . '/media/chillcreations_bootstrap/ccbootstrap.php';
		if(class_exists('ccbootstrap')){
			$chillcreations_bootsrap = new ccbootstrap();
		}
	}

$language = JFactory::getLanguage();
$language->load('com_ccidealplatform', JPATH_SITE, 'en-GB', true);
$language->load('com_ccidealplatform', JPATH_SITE, null, true);

// Create the controller
$classname	= 'ccIdealplatformController'.$controller;
$controller = new $classname( );

// Perform the Request task
$controller->execute( JRequest::getVar('task'));

// Redirect if set by the controller
$controller->redirect();

?>
