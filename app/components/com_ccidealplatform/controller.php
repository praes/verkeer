<?php
/**
* @package	cciDEAL Platform
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.
**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');
jimport('joomla.utilities.date');
jimport('joomla.mail.helper');

$jversion 			= new JVersion();
$current_version 	= $jversion->getShortVersion();
$jversion			= substr($current_version,0,1);

// extend the JController class for newsletter specific tasks
class ccIdealplatformController extends JControllerLegacy {

		const MIN_TRANS_AMOUNT = 1;

		function display($cachable = false, $urlparams = false) {

			parent :: display();
		}

		function bankform(){
			
			$db = JFactory :: getDBO();
			$query = "SELECT * FROM #__ccidealplatform_config";
			$db->setQuery($query);
 			$ideal_bank = $db->loadObject();
			
			$user 		= JFactory::getUser();
			@$user_type = $user->get( 'usertype' );
			@$user_id   = $user->get('id');			
			$groups 	= JUserHelper::getUserGroups($user_id);
			
			if($ideal_bank->IDEAL_Enable == 1 || ($ideal_bank->IDEAL_Enable == 2 && (@$user_type =="Super Administrator" || @$user_type =="Administrator" ||  @$groups['8']=="8" ||  @$groups['7']=="7" || @$groups['Super Users']=="8" || @$groups['Administrator']=="7"))){			

				//Admin test form

				$grandtotal 	= JRequest::getVar( 'grandtotal');
				$ordernumber 	= JRequest::getVar( 'ordernumber');
				$extn 			= JRequest::getVar( 'extn');

				if(($grandtotal == "") && ($ordernumber == "") && ($extn == "") && (JRequest::getVar('id') == base64_encode(date('dm')))){
					$date = date('Ymds');
					JRequest::setVar( 'grandtotal', '2.50');
					JRequest::setVar( 'ordernumber', $date);
					JRequest::setVar( 'extn', 'testpayment');

					$_POST['ordernumber'] 	= $date;
					$_POST['grandtotal']	= '2.50';
					$_POST['extn'] 			= 'testpayment';
				}

				JRequest::setVar( 'view', 'ccideal');

			if(@$ideal_bank->IDEAL_Bank){

				if(JRequest::getVar( 'grandtotal')){
					$this->addorder(strtolower($ideal_bank->IDEAL_Bank));
				}

				if(strtolower($ideal_bank->IDEAL_Bank) == 'mollie'){

					JRequest::setVar( 'layout', 'mollie');
				}

				if(strtolower($ideal_bank->IDEAL_Bank) == 'abnamrotest'){

					JRequest::setVar( 'layout', 'abnamro_easy_sent');
				}

				if(strtolower($ideal_bank->IDEAL_Bank) == 'rabobanktest'){

					JRequest::setVar( 'layout', 'ccideal_rabo_lite_sent');
				}

				if(strtolower($ideal_bank->IDEAL_Bank) == 'postbasic'){

					JRequest::setVar( 'layout', 'ccideal_post_basic_sent');
				}

				if(strtolower($ideal_bank->IDEAL_Bank) == 'targetpay'){

					if(($extn == "virtuemart") || ($extn == "hikashop") || ($extn == "rsepro")){
						$ideal_paymentmethod = JRequest::getVar( 'ideal_paymentmethod', 'ideal');
					}else{
						$ideal_paymentmethod = 'ideal';
					}

					JRequest::setVar( 'com', $extn);
					JRequest::setVar( 'ideal_paymentmethod', $ideal_paymentmethod);

					JRequest::setVar( 'layout', 'targetpay');
				}

				if(strtolower($ideal_bank->IDEAL_Bank) == strtolower('ing') || strtolower($ideal_bank->IDEAL_Bank) ==strtolower('RABOPROF')){
					if($ideal_bank->iDEAL_ProVersion==1){
						JRequest::setVar( 'layout', 'ccideal_adv_pro_idealv3');
					}
				}
				if(strtolower($ideal_bank->IDEAL_Bank) == 'raboomnikassa'){

					if(($extn == "virtuemart") || ($extn == "hikashop") || ($extn == "rsepro")){
						$ideal_paymentmethod = JRequest::getVar( 'ideal_paymentmethod', 'ideal');

						//Added cciDEAL 4.3.0
						if($ideal_paymentmethod == "ALL"){
							$ideal_paymentmethod = '';
						}
					}else{
						$ideal_paymentmethod = '';
					}

					JRequest::setVar( 'ideal_paymentmethod', $ideal_paymentmethod);

					JRequest::setVar( 'layout', 'rabo_omnikassa');
				}
				if(strtolower($ideal_bank->IDEAL_Bank) == 'sisow'){

					if(($extn == "virtuemart") || ($extn == "hikashop") || ($extn == "rsepro")){
						$ideal_paymentmethod = JRequest::getVar( 'ideal_paymentmethod', 'ideal');
					}else{
						$ideal_paymentmethod = 'ideal';
					}

					JRequest::setVar( 'ideal_paymentmethod', $ideal_paymentmethod);

					JRequest::setVar( 'layout', 'sisow');
				}
			}
			
			if(!JRequest::getVar( 'grandtotal')){
				JRequest::setVar( 'layout', 'testpayment');
			}
			}else{

				JRequest::setVar( 'view', 'ccideal');
				JRequest::setVar( 'layout', 'default_bankform');
			}

			parent :: display();

		}

	function addorder($bank){

 		$custom_membership = "0";
		$post="0";
		$post = JRequest ::get('post');

		if(empty($post)){

			$order_id 			= $_GET['ordernumber'];
			$total				= $_GET['grandtotal'];
			$extn				= $_GET['extn'];
			$custom_membership 	= @$_GET['custom'];
			$custom_payment		= @$_GET['custom_payment'];
			$extra_textfield 	= @$_GET['extra_textfield'];
			$itemid				= @$post['itemid'];
		}else{
			$order_id 			= $post['ordernumber'];
			$total 				= $post['grandtotal'];
			$custom_membership 	= @$post['custom'];
			$extn 				= $post['extn'];
			$custom_payment		= @$post['custom_payment'];
			$extra_textfield 	= @$post['extra_textfield'];
			$itemid				= @$post['itemid'];
		}
		
		//It is mainly used for TechJoomla extensions
		$payment_custom = "";
		
		if(JRequest::getVar('payment_custom')){
			$payment_custom = JRequest::getVar('payment_custom');
		}

		if(($extn == "virtuemart") || ($extn == "hikashop") || ($extn == "rsepro") || ($extn == "rsformpro")){
			$ideal_paymentmethod = JRequest::getVar( 'ideal_paymentmethod', 'ideal');

			//Added cciDEAL 4.3.0
			if($ideal_paymentmethod == "ALL"){
				$ideal_paymentmethod = '';
			}
		}else{
			if($bank == 'targetpay'){
				$ideal_paymentmethod = 'ideal';
			}else{
				$ideal_paymentmethod = '';
			}
		}

		$c=explode(" ",$total);
		if(count($c)<=2){

			@$total = $post['grandtotal'];
			if(empty($total)){
				$total		= $_GET['grandtotal'];
			}
			if($bank=="mollie" && $total<1.20){
				$total = 1.20;
			}elseif($bank=="targetpay" && $total<0.84){
				$total = 0.84;
			}else{
					@$total = $post['grandtotal'];
					if(empty($total)){
						$total		= $_GET['grandtotal'];
					}
			}

		}else{

			$total = $c[1];
			if($bank=="mollie" && $total<1.20){
				$total = 1.20;
			}elseif($bank=="targetpay" && $total<0.84){
				$total = 0.84;
			}else{
				$total = $post['grandtotal'];
					if(empty($total)){
					$total		= $_GET['grandtotal'];
				}
			}

		}
			//Store the Menu Item ID in Session
			@$myMenuItemID = JRequest::getVar('myMenuItemID');
			if(empty($myMenuItemID)){
				$myMenuItemID =$itemid;
			}
			if(empty($_SESSION['CC_MenuItemID'])){
				$_SESSION['CC_MenuItemID']=$myMenuItemID;
			}else{
				$_SESSION['CC_MenuItemID'] = "";
				$_SESSION['CC_MenuItemID'] = $myMenuItemID;
			}

			if($extn=='hikashop'){
				$this->fetch_template("com_hikashop");
				//$date=date("ym");
				//$extn_id=str_replace("$date","",$order_id);
				$extn_id	= $order_id;

			}elseif($extn=='Content_plugin'){

				if(@$post['cont_extn']!=""){
					@$extn_id=$post['cont_extn'];
					}else{
					$extn_id= $order_id;
				}

			}elseif($extn=='rsformpro'){

					$date=date("Ymdd");
					$extn_id=str_replace("$date","",$order_id);

			}elseif($extn=='rsmembership'){

					$date=date("Yd");
					$extn_id=str_replace("$date","",$order_id);

			}elseif($extn=='caddy'){

					$extn_id= $order_id;

			}elseif($extn=='redshop'){

					$extn_id= $order_id;

			}elseif($extn=='akeebasubs'){

					$date=date("Ymm");
					$extn_id=str_replace("$date","",$order_id);

			}elseif($extn=='joomisp'){

					$date=date("mY");
					$extn_id=str_replace("$date","",$order_id);
			}elseif($extn=='testform1' || $extn=='testform2' || $extn=='testform3' || $extn=='testform4' || $extn=='testform5' || $extn=='testform6' || $extn=='testform7' || $extn == 'testpayment'){

					$extn_id= $order_id;
			}elseif($extn=='virtuemart'){

					$extn_id=$order_id;
			}elseif(@$custom_payment && $order_id!=""){

					$extn_id = $order_id;
			}elseif(@$extn=='rsepro'){

					$date=date("ddy");
					$extn_id=str_replace("$date","",$order_id);

			}elseif(@$extn=='sobipro'){

					$date=date("yddy");
					$extn_id=str_replace("$date","",$order_id);

			}elseif(@$extn=='ticketmaster'){

					$date=date("yddm");
					$extn_id=str_replace("$date","",$order_id);

					$db = JFactory :: getDBO();

					$select = "SELECT orderid FROM #__ticketmaster_orders WHERE  ordercode='$extn_id'";
					$db->setQuery($select);
					$extn_id = $db->loadResult();

			}elseif(@$extn=='ccinvoices'){

					$date=date("mddy");
					$extn_id=str_replace("$date","",$order_id);

			}elseif(@$extn=='rsdirectory'){

					$date=date("mdyy");
					$extn_id=str_replace("$date","",$order_id);

			}elseif(@$extn=='j2store'){

					$date=date("md");
					$extn_id=str_replace("$date","",$order_id);

			}elseif(@$extn=='techjoomla'){

					$date=date("dm");
					$extn_id=str_replace("$date","",$order_id);

			}elseif(!empty($extn)){
					$extn = $extn;
					$extn_id = $order_id;
			}else{
				$extn_id = "Unknown";
			}



			if($bank=="mollie"){
				$total = $post['grandtotal'];
					if(empty($total)){
						$total		= $_GET['grandtotal'];
					}
				if($total<1.20){
					$total = 1.20;
				}else{
					$total = $post['grandtotal'];
					if(empty($total)){
						$total		= $_GET['grandtotal'];
					}
				}
				$total = str_replace(",",".",$total);
			}

		$status = 'pending';
		$date = date('Y-m-d H:i:s');

		$db = JFactory :: getDBO();
		$userID =  JFactory::getUser();

		//If user test again it will update only the Date

		if($extn=='testform1' || $extn=='testform2' || $extn=='testform3' || $extn=='testform4' || $extn=='testform5' || $extn=='testform6' || $extn=='testform7' || $extn=='testpayment'){

					$select = "SELECT order_id,extension_id,payment_date FROM #__ccidealplatform_payments WHERE  order_id='$order_id' AND extension_id='$extn_id'";
					$db->setQuery($select);
					$result = $db->loadResult();

					if(!empty($result)){
						$update_date = "UPDATE #__ccidealplatform_payments SET payment_date = '$date' ,user_id ='$userID->id' WHERE order_id='$order_id' AND extension_id='$extn_id'";
						$db->setQuery($update_date);
						$db->Query();
					}
		}


		//Remove spl.Characters
		$model = $this->getModel('ccideal');
		$extn_id = $model->cciDEALremoveSplChr($extn_id);
		//$order_id = $model->cciDEALremoveSplChr($order_id);

			$select = "SELECT order_id,extension_id FROM #__ccidealplatform_payments WHERE  order_id='$order_id' AND extension_id='$extn_id'";
			$db->setQuery($select);
			$result = $db->loadResult();

		if($result==""){
			
			//Used the previously used code, I am not sure in this code
			if($extn != 'hikashop'){
				$total = str_replace(',','',$total);
			}

			$query = "INSERT INTO #__ccidealplatform_payments(id,order_id,extension_id,trans_id,user_id,account,payment_total,payment_date,payment_status,extension,article_extra_text,pay_method,payment_custom ) VALUES ('','$order_id','$extn_id','','$userID->id','".$bank."','".$total."','".$date."','".$status."','".$extn."','".$extra_textfield."','".$ideal_paymentmethod."','".$payment_custom."')";
			$db->setQuery($query);
			$db->query();

			$model->notifyMailProcess();
		}

		$update_query = "UPDATE #__ccidealplatform_conf SET tmp_orderid = '" . $order_id . "' WHERE id = 1";
		$db->setQuery($update_query);
		$db->query();

	}

	//For validating the content
	function checkval(){

			$amtbyuser					=	JRequest::getVar('amtbyuser');
			$paybyuser					= 	JRequest::getVar('paybyuser');
		 	$extra_textfield			=	JRequest::getVar('extra_textfield');
			$extra_textarea				=	JRequest::getVar('extra_textarea');
			$extra_textfield_required	=	JRequest::getVar('extra_textfield_required');
			$extra_textarea_required	= 	JRequest::getVar('extra_textarea_required');

			$cnt_usr = substr_count($amtbyuser,',');
			$cnt_usr_dot = substr_count($amtbyuser,'.');

			if(isset($amtbyuser)) {

				  $amtbyuser  = trim($amtbyuser);

				if($amtbyuser == '' || $amtbyuser== 'undefined') {
					echo "1|-1" ;
				//}else if (!preg_match( '/^[\-+]?[0-9]*\.*\,?[0-9]+$/', $amtbyuser))  {
				}else if (!preg_match( '/^[0-9,.]*$/', $amtbyuser)) {
					echo "1|-1";
				}else{
					if(($cnt_usr > 1) || ($cnt_usr_dot > 1) || (($cnt_usr == 1) && ($cnt_usr_dot == 1))){
						echo "1|-1";
					}else{
						if(($cnt_usr == 1) || ($cnt_usr_dot ==1)){
							$te_str_rl = substr($amtbyuser,-3);
							$te_usr_cm = substr_count($te_str_rl,',');
							$te_usr_dot = substr_count($te_str_rl,'.');

							if(($te_usr_cm == 1) || ($te_usr_dot == 1)){
								echo "1|3";
							}else{
								echo "1|-1";
							}
						}else{
							echo "1|3";
						}
					}
				}
		 	}//Amount by user in cciDEAL

			else if(isset($paybyuser)) {

					$paybyuser  = trim($paybyuser);

					if($paybyuser == '' || $paybyuser == 'undefined') {
						echo "2|-1" ;

					}else if (!preg_match( "@[^a-z0-9]+@i", $paybyuser))  {
						echo "2|5";

					} else{
						echo "2|-1";
					}

				}//extra text field

	  		else if(isset($extra_textfield)){

				if(trim($extra_textfield)==trim($_GET['extra_textfield'])){ //For our secure process,I have used $_GET for validation
					$extra_textfield = trim($extra_textfield);
				}else{
					$extra_textfield = trim($_GET['extra_textfield']); //For our secure process,I have used $_GET for validation
				}

				$regexp = "/^[A-Za-z0-9-_@ .,#!?]+$/";
		  		if(preg_match($regexp,$extra_textfield)){
					echo "3|1";
		  		}else if($extra_textfield=="" || empty($extra_textfield)){
		  			echo "3|2";
		  		}else{
		  			echo "3|3";
		  		}

	  		}//extra text field required

			  else if(isset($extra_textfield_required)){

			  		$extra_textfield_required = trim($extra_textfield_required);

					if(trim($extra_textfield_required)==trim($_GET['extra_textfield_required'])){ //For our secure process,I have used $_GET for validation
						$extra_textfield_required = trim($extra_textfield_required);
					}else{
						$extra_textfield_required = trim($_GET['extra_textfield_required']); //For our secure process,I have used $_GET for validation
					}
					//echo $extra_textfield_required;
			  		$regexp = "/^[A-Za-z0-9_\s@.,#!?]+$/";
			  		if(preg_match($regexp,$extra_textfield_required)){
						echo "4|1";
			  		}else if($extra_textfield_required=="" || empty($extra_textfield_required)){
			  			echo "4|2";
			  		}else{
			  			echo "4|3";
			  		}

			  }
			  	//extra text area
		 	 	else if(isset($extra_textarea)){

		  		$extra_textarea = trim($extra_textarea);
		  		if(preg_match('/^[A-Za-z0-9_@ .,#!?]+$/',$extra_textarea)){
					echo "5|1";
		  		}else if($extra_textarea=="" || empty($extra_textarea)){
		  			echo "5|2";
		  		}else{
		  			echo "5|3";
		  		}

		 	 }
		 	 //extra text area required

			  else if(isset($extra_textarea_required)){

			  		$extra_textarea_required = trim($extra_textarea_required);
			  		if(preg_match('/^[A-Za-z0-9_@ .,#!?]+$/',$extra_textarea_required)){
						echo  "6|1";
			  		}else if($extra_textarea_required=="" || empty($extra_textarea_required)){
			  			echo "6|2";
			  		}else{
			  			echo "6|3";
			  		}

			  }
			   //Other conditions
				else if(!isset($amtbyuser)){
					echo "7|7";
				}else if(!isset($paybyuser)){
					echo "8|8";
				}else if(!isset($amtbyuser) && !isset($paybyuser) && $extra_textfield){
					echo "NULL";
				}
				exit;
		}

	function fetch_template($extn){

		if($extn!=""){
			$db = JFactory::getDBO();
			$select = "select * from #__menu where menutype='mainmenu'";
			$db->setQuery($select);  $result = $db->loadObjectlist();
				foreach($result as $result){
						if (preg_match("/\b$extn\b/i", "$result->link")) {
						    if($result->template_style_id!=0){
						    	$select = "select * from #__menu where menutype='main'";
								$db->setQuery($select);  $result_ideal = $db->loadObjectlist();
								foreach($result_ideal as $result_ideal){
								  if (preg_match("/\bcom_ccidealplatform\b/i", "$result_ideal->link")) {
								  		  $update = "update #__menu set template_style_id = '$result->template_style_id' where id='$result_ideal->id'";
								  		  $db->setQuery($update); $db->Query();
								  }
								}
						    }
						}
					}
		}

	}

	function checkccideal() {

		$component = JRequest :: getVar('extn');
		$amount = JRequest :: getVar('grandtotal');
		$db = JFactory :: getDBO();
		$model = $this->getModel('ccideal');
		$getMyItemID = $model->getccPaymentItemID($component);

		$query = "SELECT * FROM #__ccidealplatform_config";
		$db->setQuery($query);
		$ideal_bank = $db->loadObject();

		$merchant_retrunurl = JURI :: ROOT() . "index.php?option=com_ccidealplatform&task=checkccidealresult&amt=$amount&Itemid=$getMyItemID";

		if ($component == 'caddy') {

			$merchant_retrunurl = $merchant_retrunurl . "&com=caddy";

			$update_query = "UPDATE #__ccidealplatform_conf SET IDEAL_MerchantReturnURL = '" . $merchant_retrunurl . "' WHERE id = 1";
			$db->setQuery($update_query);
			$db->query();
		}
		if ($component == 'hikashop') {

			$merchant_retrunurl = $merchant_retrunurl . "&com=hikashop";
			$update_query = "UPDATE #__ccidealplatform_conf SET IDEAL_MerchantReturnURL = '" . $merchant_retrunurl . "' WHERE id = 1";
			$db->setQuery($update_query);
			$db->query();
		}
		if ($component == 'Content_plugin') {

			$merchant_retrunurl = $merchant_retrunurl . "&com=Content_plugin";
			$update_query = "UPDATE #__ccidealplatform_conf SET IDEAL_MerchantReturnURL = '" . $merchant_retrunurl . "' WHERE id = 1";
			$db->setQuery($update_query);
			$db->query();
		}
		if ($component == 'rsformpro') {

			$merchant_retrunurl = $merchant_retrunurl . "&com=rsformpro";
			$update_query = "UPDATE #__ccidealplatform_conf SET IDEAL_MerchantReturnURL = '" . $merchant_retrunurl . "' WHERE id = 1";
			$db->setQuery($update_query);
			$db->query();
		}
		if ($component == 'rsmembership') {

			$merchant_retrunurl = $merchant_retrunurl . "&com=rsmembership";
			$update_query = "UPDATE #__ccidealplatform_conf SET IDEAL_MerchantReturnURL = '" . $merchant_retrunurl . "' WHERE id = 1";
			$db->setQuery($update_query);
			$db->query();
		}
		if ($component == 'redshop') {

			$merchant_retrunurl = $merchant_retrunurl . "&com=redshop";
			$update_query = "UPDATE #__ccidealplatform_conf SET IDEAL_MerchantReturnURL = '" . $merchant_retrunurl . "' WHERE id = 1";
			$db->setQuery($update_query);
			$db->query();
		}
		if ($component == 'akeebasubs') {

			$merchant_retrunurl = $merchant_retrunurl . "&com=akeebasubs";
			$update_query = "UPDATE #__ccidealplatform_conf SET IDEAL_MerchantReturnURL = '" . $merchant_retrunurl . "' WHERE id = 1";
			$db->setQuery($update_query);
			$db->query();
		}
		if ($component == 'joomisp') {

			$merchant_retrunurl = $merchant_retrunurl . "&com=joomisp";
			$update_query = "UPDATE #__ccidealplatform_conf SET IDEAL_MerchantReturnURL = '" . $merchant_retrunurl . "' WHERE id = 1";
			$db->setQuery($update_query);
			$db->query();
		}

		if ($component == 'virtuemart') {

			$merchant_retrunurl = $merchant_retrunurl . "&com=virtuemart";
			$update_query = "UPDATE #__ccidealplatform_conf SET IDEAL_MerchantReturnURL = '" . $merchant_retrunurl . "' WHERE id = 1";
			$db->setQuery($update_query);
			$db->query();
		}

		//Tick Mast

		if ($component == 'ticketmaster') {

			$merchant_retrunurl = $merchant_retrunurl . "&com=ticketmaster";
			$update_query = "UPDATE #__ccidealplatform_conf SET IDEAL_MerchantReturnURL = '" . $merchant_retrunurl . "' WHERE id = 1";
			$db->setQuery($update_query);
			$db->query();
		}

		if ($component == 'testform1' || $component == 'testform2' || $component == 'testform3' || $component == 'testform4' || $component == 'testform5' || $component == 'testform6' || $component == 'testform7' || $component == 'testpayment') {

			$merchant_retrunurl = $merchant_retrunurl . "&com=$component";
			$update_query = "UPDATE #__ccidealplatform_conf SET IDEAL_MerchantReturnURL = '" . $merchant_retrunurl . "' WHERE id = 1";
			$db->setQuery($update_query);
			$db->query();
		}

		if ($component == 'rsepro') {

			$merchant_retrunurl = $merchant_retrunurl . "&com=rsepro";
			$update_query = "UPDATE #__ccidealplatform_conf SET IDEAL_MerchantReturnURL = '" . $merchant_retrunurl . "' WHERE id = 1";
			$db->setQuery($update_query);
			$db->query();
		}

		if ($component == 'sobipro') {

			$merchant_retrunurl = $merchant_retrunurl . "&com=sobipro";
			$update_query = "UPDATE #__ccidealplatform_conf SET IDEAL_MerchantReturnURL = '" . $merchant_retrunurl . "' WHERE id = 1";
			$db->setQuery($update_query);
			$db->query();
		}

		if(strtolower($ideal_bank->IDEAL_Bank) == strtolower('ing') || strtolower($ideal_bank->IDEAL_Bank) ==strtolower('RABOPROF')){
				if($ideal_bank->iDEAL_ProVersion==1){
					/*Direct the certificate path based on the bank*/
					 $bank_file = "ccideal_adv_pro_idealv3";
				}else{
					/*Direct the certificate path based on the bank*/
					$bank_file = "ccideal_" . strtolower($ideal_bank->IDEAL_Bank);
				}

		}else{
					/*Direct the certificate path based on the bank*/
					$bank_file = "ccideal_" . strtolower($ideal_bank->IDEAL_Bank);
		}

		include (JPATH_SITE . "/components/com_ccidealplatform/" . $bank_file . "/TransReq.php");
	}

	function checkccidealresult() {

		$db = JFactory :: getDBO();
		$query = "SELECT * FROM #__ccidealplatform_config";
		$db->setQuery($query);
		$ideal_bank = $db->loadObject();

		if(strtolower($ideal_bank->IDEAL_Bank) == strtolower('ing') || strtolower($ideal_bank->IDEAL_Bank) ==strtolower('RABOPROF')){
					if($ideal_bank->iDEAL_ProVersion==1){
						/*Direct the certificate path based on the bank*/
						 $bank_file = "ccideal_adv_pro_idealv3";
					}else{
						/*Direct the certificate path based on the bank*/
						$bank_file = "ccideal_" . strtolower($ideal_bank->IDEAL_Bank);
				}

			}else{
						/*Direct the certificate path based on the bank*/
						$bank_file = "ccideal_" . strtolower($ideal_bank);
			}

		include (JPATH_SITE . "/components/com_ccidealplatform/" . $bank_file . "/StatReq.php");
	}

	/* For Mollie.nl */

	function mollie_process() {

		$model = $this->getModel('ccideal');
		$process = $model->mollie_process();
	}	

	function target_process(){

		$db 			= JFactory::getDBO();
	 	$component 		= JRequest :: getVar('extn');
		$app 			= JFactory::getApplication();
		# Set ideal amount in cents so 500 cent will be 5 euro
	 	$amount 		= JRequest :: getVar('amount');

	 	$order_id 		= JRequest :: getVar('item_number');
	 	$description 	= JRequest :: getVar('description');

		$custom				=" ";
		@$getiDEALSession 	= JRequest::getVar('iDEALVMPayment_SESSION');
		$retry_payment 		= JRequest :: getVar('retry');

		//Payment total security checking for sisow payment
		$query = "SELECT payment_total FROM #__ccidealplatform_payments WHERE order_id = '".$order_id."'";
		$db->setQuery($query);
		$payment_total = $db->loadResult();

		if($amount != $payment_total)
		{
			$amount = $payment_total;
		}

		//For menu Item ID
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$menuItem = $menu->getActive();
		$itemid = $menuItem->id;

		if($retry_payment){
			$query1 = "UPDATE #__ccidealplatform_payments SET `cciDEAL_params`='' WHERE `order_id`='$order_id'";
			$db->setQuery($query1);
			$db->Query();
		}
		//For Virtuemart
		if(!empty($getiDEALSession)){
			$update_params = "UPDATE #__ccidealplatform_payments SET `cciDEAL_params`='$getiDEALSession' WHERE `order_id`='$order_id '";
			$db->setQuery($update_params);$db->Query();
		}

		$custom 			= JRequest :: getVar('custom');
		//For Custom
		if(!empty($custom)){
			$update_params = "UPDATE #__ccidealplatform_payments SET `cciDEAL_params`='$custom' WHERE `order_id`='$order_id '";
			$db->setQuery($update_params);$db->Query();
		}

		$custom_payment 	= JRequest :: getVar('custom_payment');
		//For Custom payment
		if(!empty($custom_payment)){
			$update_params = "UPDATE #__ccidealplatform_payments SET `cciDEAL_params`='$custom_payment' WHERE `order_id`='$order_id '";
			$db->setQuery($update_params);$db->Query();
		}

	 	$query = "SELECT * FROM #__ccidealplatform_config";
		$db->setQuery($query);
		$ideal_bank = $db->loadObject();

		/*Direct the certificate path based on the bank*/
		$bank_file 	= "ccideal_" . strtolower($ideal_bank->IDEAL_Bank);



		$ideal_paymentmethod 	= JRequest::getVar('ideal_paymentmethod', 'ideal');

		$iRtlo 		= $ideal_bank->IDEAL_Targetpay_ID;


		# Set return url from your website
		//$iReturnurl = JURI::root()."components/com_ccidealplatform/ccideal_targetpay/ccideal_targetpay_return.php?orderID=$order_id";
		$iReturnurl  = JURI::root()."index.php?option=com_ccidealplatform&task=targetpay_notify&orderID=".$order_id;

		# Set report url to recieve the status of transactions not retreived by the returnurl
		$iReporturl = JURI::root()."components/com_ccidealplatform/ccideal_targetpay/ccideal_targetpay_report.php?orderID=$order_id";

		if(strlen($iReporturl)>255){
			 $iReporturl = JURI::root()."components/com_ccidealplatform/ccideal_targetpay_report.php?orderID=$order_id";
		}

		$amount = $amount*100;

		/*
		*  Initiate payment
		*/

		ini_set ( 'display_errors' , '1');

		if($ideal_paymentmethod == "ideal"){

			$iIssuer 	= JRequest :: getVar("bank" );

			require_once(JPATH_SITE . "/components/com_ccidealplatform/" . $bank_file . "/targetpayideal.class.php");

			# To initiate a payment, initiate the class
			$oIdeal = new TargetPayIdeal ( $iRtlo );

			# Set ideal amount in cents so 500 cent will be 5 euro

			$oIdeal->setIdealAmount ( $amount );

			# Set ideal issuer
			$oIdeal->setIdealissuer ( $iIssuer );

			# Set ideal description
			$oIdeal->setIdealDescription ( $description);

			# Set return url, wich should return on succes
			$oIdeal->setIdealReturnUrl ( $iReturnurl );

			# Set report url
			$oIdeal->setIdealReportUrl ( $iReporturl );

			# Now we can initiate the payment
			$aReturn 	= $oIdeal->startPayment();

			# This is the transaction id
			$intTrxId 	= $aReturn[0];

			# this will be the bank url that will rederect to the bank.
			$strBankURL = $aReturn[1];

			/**
			* This haader function will redirect the browser to the bank
			*/

			header( "Location: ". $strBankURL );
			exit;

		}elseif($ideal_paymentmethod == "mistercash"){

			require_once(JPATH_SITE . "/components/com_ccidealplatform/" . $bank_file . "/targetpaymrcash.class.php");

			# To initiate a payment, initiate the class
			$oMrCash = new TargetPayMrCash ( $iRtlo );

			# Set amount in cents so 500 cent will be 5 euro
			$oMrCash->setMrCashAmount ( $amount );

			$ip_address = $_SERVER["REMOTE_ADDR"];

			# Set the client IP number
			$oMrCash->setMrCashIp ( $ip_address );

			# Set description
			$oMrCash->setMrCashDescription ( 'Dit is een test betaling');

			# Set return url, wich should return on succes
			$oMrCash->setMrCashReturnUrl ( $iReturnurl );

			# Set report url
			$oMrCash->setMrCashReportUrl ( $iReporturl );

			# Now we can initiate the payment
			$aReturn = $oMrCash->startPayment();

			# This is the transaction id
			$intTrxId = $aReturn[0];

			# this will be the bank url that will rederect to the bank.
			$strBankURL = $aReturn[1];

			/**
			* This haader function will redirect the browser to the bank
			*/
			header( "Location: ". $strBankURL );
			exit;

		}elseif($ideal_paymentmethod == "sofort"){

			require_once(JPATH_SITE . "/components/com_ccidealplatform/" . $bank_file . "/targetpaysofort.class.php");

			# To initiate a payment, initiate the class
			$obj = new TargetPaySofort( $iRtlo );

			# Set amount in cents so 500 cent will be 5 euro
			$obj->setDIRECTebankingAmount( $amount );

			$ip_address = $_SERVER["REMOTE_ADDR"];

			# Set the client IP number
			$obj->setDIRECTebankingIp ( $ip_address );

			# Set description
			$obj->setDIRECTebankingDescription ( 'Dit is een test betaling');

			# Set return url, wich should return on succes
			$obj->setDIRECTebankingReturnUrl ( $iReturnurl );

			# Set report url
			$obj->setDIRECTebankingReportUrl ( $iReporturl );

			# Set country
			$iCountry 	= JRequest::getVar("sofort_country" );

			# Set type
			$iType = 1;

			# Set country
			$obj->setDIRECTebankingCountry ( $iCountry );

			# Set type
			$obj->setDIRECTebankingType ( $iType );

			# Now we can initiate the payment
			$aReturn = $obj->startPayment();

			# This is the transaction id
			$intTrxId = $aReturn[0];

			# this will be the bank url that will rederect to the bank.
			$strBankURL = $aReturn[1];

			/**
			* This haader function will redirect the browser to the bank
			*/
			header( "Location: ". $strBankURL );
		}
	}

	function mollie_notify() {

		$model = $this->getModel('ccideal');
		$process = $model->mollie_notify();
	}

	function mollie_return() {

		$orderid=@ JRequest::getVar('orderid');
		$amt=@ JRequest::getVar('amt');
		$extn=@ JRequest::getVar('com');
		$transaction_id =@JRequest::getVar('transaction_id');
		$document = JFactory :: getDocument();
		$app =  JFactory::getApplication();
		$document->addStyleSheet('components/com_ccidealplatform/assets/css/style.css');
		$model = $this->getModel('ccideal');
		$process = $model->mollie_return();
		$getMyItemID = $model->getccPaymentItemID($extn);
		$ccItemID	=  $_SESSION['CC_MenuItemID'];
		if(empty($getMyItemID)){
			$cciItemID = $ccItemID;
		}

		if($process == '0'){

			JRequest::setVar( 'view', 'ccideal');
			JRequest::setVar( 'layout', 'mollie');

			parent :: display();
		}
	}
	public function statuslayout($result){

		$app = JFactory::getApplication();

		$db = JFactory :: getDBO();

		$query = "SELECT * FROM #__ccidealplatform_config LIMIT 0,1";
		$db->setQuery($query);
		$conf_data = $db->loadAssoc();

		$status_message = "";
		$art_id 		= "";
		$art_value 		= "";
		$lay_value		= "";


		if($result == 'paid'){
			$status_message = $conf_data['IDEAL_Status_ThankYou'];
			$art_id 		= $conf_data['IDEAL_pay_sucs_articleid'];
			$art_value 		= $conf_data['IDEAL_pay_sucs_value'];
			$lay_value		= "1";

		}elseif($result == 'pending'){
			$status_message = $conf_data['IDEAL_Status_pending'];
			$art_id 		= $conf_data['IDEAL_pay_pend_articleid'];
			$art_value 		= $conf_data['IDEAL_pay_pend_value'];
			$lay_value		= "1";

		}elseif($result == 'cancelled'){
			$status_message = $conf_data['IDEAL_Status_Cancelled'];
			$art_id 		= $conf_data['IDEAL_pay_cancel_articleid'];
			$art_value 		= $conf_data['IDEAL_pay_cancel_value'];
			$lay_value		= "0";
		}elseif($result == 'failed'){
			$status_message = $conf_data['IDEAL_Status_Failed'];
			$art_id 		= $conf_data['IDEAL_pay_fail_articleid'];
			$art_value 		= $conf_data['IDEAL_pay_failed_value'];
			$lay_value		= "0";
		}else{
			$status_message = $conf_data['IDEAL_Status_pending'];
			$art_id 		= $conf_data['IDEAL_pay_pend_articleid'];
			$art_value 		= $conf_data['IDEAL_pay_pend_value'];
			$lay_value		= "1";
			$result = 'pending';
		}

		if($art_value){

			$sel_succ="SELECT id FROM #__menu WHERE published=1 AND link='index.php?option=com_content&view=article&id=".$art_id."' Order by id desc";
			$db->setQuery($sel_succ);
			$itemid=$db->loadResult();

			$app->redirect(JRoute::_('index.php?option=com_content&view=article&id='.$art_id.'&Itemid='.$itemid));

			$app->redirect(JRoute::_('index.php?option=com_content&view=article&id='.$art_id));
		}else{

			$smsg = new ccIdealplatformController();
			$html = $smsg->shortmessage($status_message,$result);
			echo $html;

		}
		return $lay_value;
	}

	public function shortmessage($status_message,$result){

		$jversion 			= new JVersion();
		$current_version 	= $jversion->getShortVersion();
		$jversion			= substr($current_version,0,1);

		$html = "";

		$html .="<div class=\"chillcreations-bootstrap\">";

			if($result == 'paid'){
				$html .="<div class=\"alert alert-success\">";
			}elseif($result == 'pending'){
				$html .="<div class=\"alert\">";
			}else{
				$html .="<div class=\"alert alert-error\">";
			}

			$html .="<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>";
			$html .="".nl2br($status_message)."";
			$html .="</div>";

		$html .="</div>";

		return $html;
	}
	function raboomnikassa_notify() {
		$model = $this->getModel('ccideal');
		$process = $model->raboomnikassa_notify();
	}
	function raboomnikassa_return() {
		$model = $this->getModel('ccideal');
		$process = $model->raboomnikassa_return();
	}
	function sisow_process() {
		$model = $this->getModel('ccideal');
		$process = $model->sisow_process();
	}
	function sisow_notify() {
		$model = $this->getModel('ccideal');
		$process = $model->sisow_notify();
	}
	function sisow_return() {
		$model = $this->getModel('ccideal');
		$process = $model->sisow_return();
	}
	function targetpay_notify() {
		$model = $this->getModel('ccideal');
		$process = $model->targetpay_notify();
	}
	function targetpay_return(){
		$model = $this->getModel('ccideal');
		$process = $model->targetpay_return();
	}
}
?>