<?php
/**
* @package	cciDEAL Platform 
* @author 	Chill Creations <info@chillcreations.com>
* @link 	http://www.chillcreations.com
* @copyright	Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license	GNU/GPL, see LICENSE.php for full license.   
**/

// no direct access
defined('_JEXEC') or die('Restricted access');
chdir("../../../");
getcwd();
define('JPATH_BASE', getcwd() );
define('DS', DIRECTORY_SEPARATOR);
define('JPATH_COMPONENT', JPATH_BASE . '/components/');

require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );
//require_once( 'targetpayideal.class.php' );

$app = JFactory::getApplication('site');
jimport('joomla.application.component.model');

/*

$db = JFactory::getDBO();

//Get the Payment details
	$query1 = "SELECT * FROM #__ccidealplatform_payments WHERE `order_id`='$order_number'";
	$db->setQuery($query1);
	$payment_data = $db->loadObject();

//Get the configuration details
	$query = "SELECT * FROM #__ccidealplatform_config ";
	$db->setQuery($query);
	$conf_data = $db->loadObject();

//test Mode
	if($conf_data->IDEAL_Mode == "TEST"){
		$iTest = 1;
	}else{
		$iTest = 0;
	}
	
	$component 	= $payment_data->extension;
	$iRtlo 		= $conf_data->IDEAL_Targetpay_ID;

 	$transID		= $_POST['trxid'];
 	$order_number 	= $_POST['orderID'];
 	$iRtlo 			= $_POST['rtlo'];

	if(empty($transID)){
		$transID = JRequest::getVar('trxid');
	}
	if(empty($order_number)){
		$order_number = JRequest::getVar('orderID');
	}	

	if(isset( $transID )) {
		# Init the class
		$oIdeal = new TargetPayIdeal ( $iRtlo );
		
		if($payment_data->cciDEAL_params != 1 || empty($payment_data->cciDEAL_params)){ //check the status,if already updted or not.
			if ( $oIdeal->validatePayment ( $transID,$iOnce=0,$mode) == true ) {
			
				$pay_status = "Success";
				$paid_status = "1";
				
				//status update in cciDEALPlatform
					
				$query = "UPDATE #__ccidealplatform_payments SET payment_status = 'paid',`trans_id`='".$transaction_id."' WHERE order_id = '" . $order_number . "'";
				$db->setQuery($query);
				$db->Query();

				$queryLog = "UPDATE #__ccidealplatform_logs SET payment_status = 'paid' WHERE purchaseID = '" . $order_number . "'";
				$db->setQuery($queryLog);
				$db->Query();
				
			}else{
			
				$pay_status = "Cancelled";
				$paid_status = "0";
		
				//Cancelled status update in cciDEALPlatform

				$query = "UPDATE #__ccidealplatform_payments SET payment_status = 'cancelled' WHERE order_id = '" . $order_number . "'";
				$db->setQuery($query);
				$db->Query();

				$queryLog = "UPDATE #__ccidealplatform_logs SET payment_status = 'cancelled' WHERE purchaseID = '" . $order_number . "'";
				$db->setQuery($queryLog);
				$db->Query();
			}
			
			$transaction_id = $transID;
			
		//Support for Akeeba subscriptions
			if ($component == 'akeebasubs') {			
				ccIdealplatformModelccideal::akeeba_notify($order_number,$pay_status,$transaction_id);			
			}
			
		//Support for hikashop
			if ($component == 'hikashop') {			
				ccIdealplatformModelccideal::hikashop_notify($order_number,$pay_status,$paid_status);
			}
			
		//Support for rsformpro
			if ($component == 'rsformpro') {			
				ccIdealplatformModelccideal::rsformpro_notify($order_number,$paid_status);			
			}
			
		//Support for RSEvents Pro
			if($component=="rsepro"){			
				ccIdealplatformModelccideal::rsepro_notify($order_number,$paid_status);
			}
			
		//Support for rsmembership
			if ($component == 'rsmembership') {
				ccIdealplatformModelccideal::rsmembership_notify($order_number,$paid_status,$transaction_id);
			}
			
		//Support for JoomISP
			if ($component == 'joomisp') {		
				ccIdealplatformModelccideal::joomisp_notify($order_number,$paid_status);			
			}
			
		//Support for Virtuemart
			if ($component == 'virtuemart') {		
				ccIdealplatformModelccideal::virtuemart_notify($order_number,$paid_status);		
			}
			
		//Support for redshop
			if ($component == 'redshop') {		
				ccIdealplatformModelccideal::redshop_notify($order_number,$paid_status);
			}
			
		//Support for SobiPro
			if($component=="sobipro"){
				ccIdealplatformModelccideal::sobipro_notify($order_number,$paid_status);
			}

		//Support for caddy
			if ($component == 'caddy') {			
				ccIdealplatformModelccideal::caddy_notify($order_number,$paid_status);			
			}
			
		//Support for ccInvoices
			if ($component == 'ccinvoices') {			
				ccIdealplatformModelccideal::ccinvoices_notify($order_number,$paid_status,$transaction_id);			
			}
		
				 
		//Upadte the params
					
			$query1 = "UPDATE #__ccidealplatform_payments SET `cciDEAL_params` = '1' WHERE order_id='$order_number'";
			$db->setQuery($query1);
			$db->Query();
		}
	}*/
?>