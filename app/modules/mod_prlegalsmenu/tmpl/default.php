<?php
/**
 * prLegalsMenu
 * 
 * @package     Praes
 * @subpackage  Modules
 * @link        http://jelgervisser.nl
 * @license     GNU/GPL latest version
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$i = 0;
$count = count($menu_items);

?>

<div class="pr-legalsmenu<?php echo $separator_class; ?>">
	
	<?php if ($separator != 2) : ?>
	<ul<?php echo $separator_class;?>>
	<?php endif; ?>
		
	<?php
	foreach ($menu_items as $menu_item) {
		$i++;
		$link = prLegalsMenuHelper::menuitemLink($menu_item);
		$title = prLegalsMenuHelper::menuitemTitle($menu_item);
		
		echo ($separator != 2) ? '<li>' : '';
		
		echo '<a href="' . $link . '">' . $title . '</a>';
		
		if ($separator == 2 ) {
			if ($i < $count) echo '<i class="' . $uikit_icon . '"></i>';
		} else {
			echo '</li>';
		}
	}
	?>
	
	<?php if ($separator != 2) : ?>
	</ul>
	<?php endif; ?>
	
</div>
