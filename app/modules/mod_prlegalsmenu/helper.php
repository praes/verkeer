<?php
/**
 * prLegalsMenu
 * 
 * @package     Praes
 * @subpackage  Modules
 * @link        http://jelgervisser.nl
 * @license     GNU/GPL latest version
 */
 
class prLegalsMenuHelper
{   
    
    public static function addAssets()
    {
		$document = JFactory::getDocument();
		$assets_path = '/~distro/modules/mod_prlegalsmenu/assets/';
		
		// add css
		$document->addStylesheet($assets_path.'css/legalsmenu.css');
    }


	public static function menuitemLink($item_id)
	{
		$app   = JFactory::getApplication();
		$menu  = $app->getMenu();
		
		$link  = $menu->getItem($item_id)->link;
		$link .= '&Itemid=' . $item_id;
		
		return $link; 
	}  


	public static function menuitemTitle($item_id)
	{
		$app   = JFactory::getApplication();
		$menu  = $app->getMenu();
		
		$title  = $menu->getItem($item_id)->title;
		
		return $title; 
	}    
    
}
?>