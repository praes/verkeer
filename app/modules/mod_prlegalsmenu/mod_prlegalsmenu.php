<?php
/**
 * prLegalsMenu
 * 
 * @package     Praes
 * @subpackage  Modules
 * @link        http://jelgervisser.nl
 * @license     GNU/GPL latest version
 */
 
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if(!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}

// include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

// add assets
prLegalsMenuHelper::addAssets();


$menu_items = array();

$menu_item = $params->get('menuitem1', 0);
if ($menu_item != 0) $menu_items[] = $menu_item;

$menu_item = $params->get('menuitem2', 0);
if ($menu_item != 0) $menu_items[] = $menu_item;

$menu_item = $params->get('menuitem3', 0);
if ($menu_item != 0) $menu_items[] = $menu_item;

$menu_item = $params->get('menuitem4', 0);
if ($menu_item != 0) $menu_items[] = $menu_item;

$menu_item = $params->get('menuitem5', 0);
if ($menu_item != 0) $menu_items[] = $menu_item;





// get params
$separator = $params->get('separator', 0);

// format separator class
if ($separator == 1) {
	$separator_class = ' class="pr-item-border"';
} elseif ($separator == 2) {
	$separator_class = ' pr-menuitem-icon';
}

$uikit_icon	= $params->get('uikit_icon', 'uk-icon-angle-right');

// render layout
require JModuleHelper::getLayoutPath('mod_prlegalsmenu', $params->get('module_layout', 'default'));

?>