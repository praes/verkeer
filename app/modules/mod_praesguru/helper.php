<?php

class modPraesGuruHelper{
 
    public static function getName($params){
	
        $user = JFactory::getUser();
		
		return $user->name;
		
    }
	
	public static function getImage($params){
	
		$user = JFactory::getUser();
        $db = JFactory::getDbo();
		
		$query = $db->getQuery(true);
		$db->setQuery($query);
		
		$query->select($db->quoteName(array('id', 'image')));
		$query->from($db->quoteName('#__guru_customer'));
		$query->where('id = ' . $user->id);
		
		$results = $db->loadObject();
		
		if($results){
		
			if($results->image){
				
				return $results;
			
			}
		
		}
		
		
    }
	
	public static function getCompany($params){
	
		$user = JFactory::getUser();
        $db = JFactory::getDbo();
		
		$query = $db->getQuery(true);
		$db->setQuery($query);
		
		$query->select($db->quoteName(array('id', 'company')));
		$query->from($db->quoteName('#__guru_customer'));
		$query->where('id = ' . $user->id);
		
		$results = $db->loadObject();
		
		if($results){
			
			if($results->company){
				
				return $results;
			
			}
		
		}
		
    }
	
}

