<?php 
// No direct access
defined('_JEXEC') or die; ?>



<div class="pr-mod-spacing-small pr-background-white-transparent">

	<div class="uk-grid">

		<div class="uk-width-1-2 uk-text-center">
		
			<?php if($image) : ?>
		
				<img class="uk-border-circle" src="<?php echo $image->image; ?>">
				
			<?php endif; ?>
			
			<?php if(!$image) : ?>
			
				<a href="index.php?option=com_guru&view=guruProfile&task=edit&Itemid=281"><img class="uk-border-circle" src="http://www.placehold.it/300x300"></a>
			
			<?php endif; ?>
			
		</div>
		
		<div class="uk-width-1-2">
		
			<div class="uk-width-1-1 uk-margin-bottom">
			
				<h4 class="pr-font-primary uk-margin-remove uk-text-bold">Welkom terug</h4>
				
				<?php echo $name; ?>
			
			</div>
			
			<div class="uk-width-1-1">
			
				<h4 class="pr-font-primary uk-margin-remove uk-text-bold">School:</h4>
			
				<?php if($company) : ?>
			
					<?php echo $company->company; ?>
			
				<?php endif; ?>
				
				<?php if(!($company)) : ?>
				
					<a href="index.php?option=com_guru&view=guruProfile&task=edit&Itemid=281" class="pr-font-black">School toevoegen &raquo;</a>
				
				<?php endif; ?>
			
			</div>
		
		</div>

	</div>
	
</div>