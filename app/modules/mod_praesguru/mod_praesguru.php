<?php
 
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
 
$name = modPraesGuruHelper::getName($params);
$image = modPraesGuruHelper::getImage($params);
$company = modPraesGuruHelper::getCompany($params);
require JModuleHelper::getLayoutPath('mod_praesguru');