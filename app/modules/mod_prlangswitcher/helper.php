<?php
/**
 * prLangSwitcher
 * 
 * @package     Praes
 * @subpackage  Modules
 * @link        http://praes.nl
 * @license     GNU/GPL latest version
 */
 
class prLangSwitcher
{   

    /**
     * Adds dependencies to the head of the current document
     *
     * @param array $params An object containing the module parameters
     * @access public
     */    
    public static function addAssets()
    {
		$document = JFactory::getDocument();
		$assets_path = '/modules/mod_prlangswitcher/assets/';
		
		// add css
		$document->addStylesheet($assets_path.'css/style.css');
		
    }   
    
}
?>