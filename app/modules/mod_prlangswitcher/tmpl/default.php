<?php
/**
 * prLangSwitcher
 * 
 * @package     Praes
 * @subpackage  Modules
 * @link        http://praes.nl
 * @license     GNU/GPL latest version
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

//var_dump($show_switcher);
//var_dump($menuitem_params);
?>

<?php if ($show_switcher == true) : ?>
<div class="pr-langswitcher">
	
	<ul>
		
		<?php if ($nl && $menuitem_params['pr_link_nl'] != "") : ?>
		<li><a class="pr-flag pr-flag-nl" href="<?php echo $menuitem_params['pr_link_nl']; ?>" <?php echo ($menuitem_params['pr_link_nl_blank'] == 1) ? 'target="_blank"' : ''; ?>></a></li>
		<?php endif; ?>
		
		<?php if ($en && $menuitem_params['pr_link_en'] != "") : ?>
		<li><a class="pr-flag pr-flag-en" href="<?php echo $menuitem_params['pr_link_en']; ?>" <?php echo ($menuitem_params['pr_link_en_blank'] == 1) ? 'target="_blank"' : ''; ?>></a></li>
		<?php endif; ?>
		
		<?php if ($de && $menuitem_params['pr_link_de'] != "") : ?>
		<li><a class="pr-flag pr-flag-de" href="<?php echo $menuitem_params['pr_link_de']; ?>" <?php echo ($menuitem_params['pr_link_de_blank'] == 1) ? 'target="_blank"' : ''; ?>></a></li>
		<?php endif; ?>
		
	</ul>

</div>
<?php endif; ?>