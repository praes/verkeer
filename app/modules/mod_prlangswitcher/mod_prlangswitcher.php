<?php
/**
 * prLangSwitcher
 * 
 * @package     Praes
 * @subpackage  Modules
 * @link        http://praes.nl
 * @license     GNU/GPL latest version
 */
 
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if(!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}

// check for installed plugin!!!
$show_switcher = JPluginHelper::isEnabled('system', 'prextendedmenu');

// include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

// add assets
prLangSwitcher::addAssets();

// get module params
$nl = $params->get('show_nl', 0);
$en = $params->get('show_en', 0);
$de = $params->get('show_de', 0);

// get params of active menu item
$app			 = JFactory::getApplication();
$menuitem		 = $app->getMenu()->getActive();
$menuitem_params = $menuitem->params;

// render layout
require JModuleHelper::getLayoutPath('mod_prlangswitcher', $params->get('module_layout', 'default'));

?>