<?php
/**
 * prCopyright
 * 
 * @package     Praes
 * @subpackage  Modules
 * @link        http://jelgervisser.nl
 * @license     GNU/GPL latest version
 */
 
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// get params
$show_first_year	= $params->get('show_first_year', 0);
$show_current_year	= $params->get('show_current_year', 0);
$first_year			= $params->get('first_year', '');
$copyright_prefix	= $params->get('copyright_prefix', '');
$copyright_suffix	= $params->get('copyright_suffix', '');
$wrap				= $params->get('wrap_p', 0);
$wrap_class			= $params->get('wrap_class', '');

// render layout
require JModuleHelper::getLayoutPath('mod_prcopyright', $params->get('module_layout', 'default'));

?>