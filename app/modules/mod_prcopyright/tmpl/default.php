<?php
/**
 * prCopyright
 * 
 * @package     Praes
 * @subpackage  Modules
 * @link        http://jelgervisser.nl
 * @license     GNU/GPL latest version
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$copyright = '';

// add prefix notice
$copyright .= $copyright_prefix . ' ';

// add first year
if ($show_first_year) {
	$copyright .= (trim($first_year) == date('Y')) ? '' : $first_year . ' - ';
}

// add current year
$copyright .= ($show_current_year) ? date('Y') . ' ' : '';

// add suffix notice
$copyright .= $copyright_suffix;

// wrap copyright in paragraph tags
if ($wrap) {
	if (trim($wrap_class) != '') {
		$left_tag = '<p class="' . $wrap_class . '">';
	} else {
		$left_tag = '<p>';
	}
}

?>

<div class="pr-copyright">
	
	<?php echo ($wrap) ? $left_tag : ''; ?>
	<?php echo trim($copyright); ?>
	<?php echo ($wrap) ? '</p>' : ''; ?>

</div>
