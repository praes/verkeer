<?php
/**
* @package    ccNewsletter
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) Chill Creations, see COPYRIGHT.php for details.
* @license    GNU/GPL, see LICENSE.php for full license.
**/

defined('_JEXEC') or die;

class ccbootstrap{

	function __construct()
	{
		$document = JFactory::getDocument();
		$document->addScript(JURI::ROOT().'media/chillcreations_bootstrap/js/jquery.min.js');
		$document->addStyleSheet(JURI::ROOT().'media/chillcreations_bootstrap/css/bootstrap.min.css');
		$document->addStyleSheet(JURI::ROOT().'media/chillcreations_bootstrap/css/icomoon.css');
		$document->addScript(JURI::ROOT().'media/chillcreations_bootstrap/js/bootstrap.min.js');		
	}
}