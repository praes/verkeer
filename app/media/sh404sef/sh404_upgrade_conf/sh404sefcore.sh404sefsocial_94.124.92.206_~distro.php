<?php // Extension params save file for sh404sef
    //
    if (!defined('_JEXEC')) die('Direct Access to this location is not allowed.');
$shConfig = array (
  'extension_id' => '10269',
  'name' => 'PLG_SH404SEFCORE_SH404SEFSOCIAL',
  'type' => 'plugin',
  'element' => 'sh404sefsocial',
  'folder' => 'sh404sefcore',
  'client_id' => '0',
  'enabled' => '1',
  'access' => '1',
  'protected' => '0',
  'manifest_cache' => '{"name":"PLG_SH404SEFCORE_SH404SEFSOCIAL","type":"plugin","creationDate":"2015-07-14","author":"Yannick Gaultier","copyright":"(c) Yannick Gaultier - Weeblr llc - 2015","authorEmail":"welcome@weeblr.com","authorUrl":"https:\\/\\/weeblr.com","version":"4.6.0.2718","description":"PLG_SH404SEFCORE_SH404SEFSOCIAL_XML_DESCRIPTION","group":"","filename":"sh404sefsocial"}',
  'params' => '{"enableSocialAnalyticsIntegration":"1","enableGoogleSocialEngagement":"1","onlyDisplayOnCanonicalUrl":"1","buttonsContentLocation":"onlytags","useShurl":"1","enabledCategories":"show_on_all","enableFbLike":"1","fbAction":"like","fbShowFaces":"1","fbLayout":"button_count","enableFbShare":"1","fbShareLayout":"button_count","enableFbSend":"1","fbColorscheme":"","fbWidth":"","fbUseHtml5":"0","enableTweet":"1","tweetLayout":"none","viaAccount":"","enablePinterestPinIt":"1","pinItCountLayout":"none","pinItButtonText":"","enablePlusOne":"1","plusOneSize":"","plusOneAnnotation":"none","enableGooglePlusPage":"1","googlePlusPage":"","googlePlusCustomText":"","googlePlusCustomText2":"","googlePlusPageSize":"medium","enableLinkedIn":"1","linkedinlayout":"none"}',
  'custom_data' => '',
  'system_data' => '',
  'checked_out' => '0',
  'checked_out_time' => '0000-00-00 00:00:00',
  'ordering' => '10',
  'state' => '0',
);
