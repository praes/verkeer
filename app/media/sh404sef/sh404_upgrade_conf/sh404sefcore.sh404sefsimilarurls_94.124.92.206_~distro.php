<?php // Extension params save file for sh404sef
    //
    if (!defined('_JEXEC')) die('Direct Access to this location is not allowed.');
$shConfig = array (
  'extension_id' => '10268',
  'name' => 'sh404sef - Similar urls plugin',
  'type' => 'plugin',
  'element' => 'sh404sefsimilarurls',
  'folder' => 'sh404sefcore',
  'client_id' => '0',
  'enabled' => '1',
  'access' => '1',
  'protected' => '0',
  'manifest_cache' => '{"name":"sh404sef - Similar urls plugin","type":"plugin","creationDate":"2015-07-14","author":"Yannick Gaultier","copyright":"(c) Yannick Gaultier - Weeblr llc - 2015","authorEmail":"welcome@weeblr.com","authorUrl":"https:\\/\\/weeblr.com","version":"4.6.0.2718","description":"PLG_SH404SEFCORE_SH404SEFSIMILARURLS_XML_DESCRIPTION","group":"","filename":"sh404sefsimilarurls"}',
  'params' => '{"max_number_of_urls":"5","min_segment_length":"3","include_pdf":"0","include_print":"0","excluded_words_sef":"sh404sef-core-content","excluded_words_non_sef":""}',
  'custom_data' => '',
  'system_data' => '',
  'checked_out' => '0',
  'checked_out_time' => '0000-00-00 00:00:00',
  'ordering' => '10',
  'state' => '0',
);
