<?php // Extension params save file for sh404sef
    //
    if (!defined('_JEXEC')) die('Direct Access to this location is not allowed.');
$shConfig = array (
  'extension_id' => '10266',
  'name' => 'sh404sef - Analytics plugin',
  'type' => 'plugin',
  'element' => 'sh404sefanalytics',
  'folder' => 'sh404sefcore',
  'client_id' => '0',
  'enabled' => '1',
  'access' => '1',
  'protected' => '0',
  'manifest_cache' => '{"name":"sh404sef - Analytics plugin","type":"plugin","creationDate":"2015-07-14","author":"Yannick Gaultier","copyright":"(c) Yannick Gaultier - Weeblr llc - 2015","authorEmail":"welcome@weeblr.com","authorUrl":"https:\\/\\/weeblr.com","version":"4.6.0.2718","description":"Create analytics custom tags\\n\\t","group":"","filename":"sh404sefanalytics"}',
  'params' => '{}',
  'custom_data' => '',
  'system_data' => '',
  'checked_out' => '0',
  'checked_out_time' => '0000-00-00 00:00:00',
  'ordering' => '10',
  'state' => '0',
);
