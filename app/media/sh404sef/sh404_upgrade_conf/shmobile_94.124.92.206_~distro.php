<?php // Extension params save file for sh404sef
    //
    if (!defined('_JEXEC')) die('Direct Access to this location is not allowed.');
$shConfig = array (
  'name' => 'sh404sef - System mobile template switcher',
  'type' => 'plugin',
  'element' => 'shmobile',
  'folder' => 'system',
  'client_id' => '0',
  'enabled' => '1',
  'access' => '1',
  'protected' => '0',
  'manifest_cache' => '{"name":"sh404sef - System mobile template switcher","type":"plugin","creationDate":"2015-07-14","author":"Yannick Gaultier","copyright":"(c) Yannick Gaultier - Weeblr llc - 2015","authorEmail":"welcome@weeblr.com","authorUrl":"https:\\/\\/weeblr.com","version":"4.6.0.2718","description":"Switch site template for mobile devices","group":"","filename":"shmobile"}',
  'params' => '{"mobile_switch_enabled":"0","mobile_template":""}',
  'custom_data' => '',
  'system_data' => '',
  'checked_out' => '0',
  'checked_out_time' => '0000-00-00 00:00:00',
  'ordering' => '10',
  'state' => '0',
);
