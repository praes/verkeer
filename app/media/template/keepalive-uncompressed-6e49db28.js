!(function(){'use strict';document.addEventListener('DOMContentLoaded',function(){var keepaliveOptions=Joomla.getOptions('system.keepalive'),keepaliveUri=keepaliveOptions&&keepaliveOptions.uri?keepaliveOptions.uri.replace(/&amp;/g,'&'):'',keepaliveInterval=keepaliveOptions&&keepaliveOptions.interval?keepaliveOptions.interval:45*1000;if(keepaliveUri==='')
{var systemPaths=Joomla.getOptions('system.paths');keepaliveUri=(systemPaths?systemPaths.root+'/index.php':window.location.pathname)+'?option=com_ajax&format=json';}
window.setInterval(function(){Joomla.request({url:keepaliveUri,onSuccess:function(response,xhr)
{},onError:function(xhr)
{}});},keepaliveInterval);});})(window,document,Joomla);