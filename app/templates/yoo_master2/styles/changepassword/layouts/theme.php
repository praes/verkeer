<?php
/**
* @package   yoo_master2
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// get theme configuration
include($this['path']->path('layouts:theme.config.php'));

?>
<!DOCTYPE HTML>
<html lang="<?php echo $this['config']->get('language'); ?>" dir="<?php echo $this['config']->get('direction'); ?>"  data-config='<?php echo $this['config']->get('body_config','{}'); ?>'>

<head>
<?php echo $this['template']->render('head'); ?>
</head>

<body class="<?php echo $this['config']->get('body_classes'); ?>">



	<div class="pr-block-toolbar">
	
		<?php if ($this['widgets']->count('logo')) : ?>
		
			<div class="uk-text-center"><a class="tm-logo" href="<?php echo $this['config']->get('site_url'); ?>"><?php echo $this['widgets']->render('logo'); ?></a></div>
		
		<?php endif; ?>

		<?php if ($this['widgets']->count('logo-small')) : ?>
		
			<div class="uk-navbar-content uk-navbar-center uk-visible-small"><a class="tm-logo-small" href="<?php echo $this['config']->get('site_url'); ?>"><?php echo $this['widgets']->render('logo-small'); ?></a></div>
		
		<?php endif; ?>
		
	</div>
	
	<?php if ($this['widgets']->count('menu + search + offcanvas')) : ?>
	<div class="pr-block-header">
	
		<div class="uk-container uk-container-center">

			<?php if ($this['widgets']->count('menu + search')) : ?>
			<nav class="tm-navbar uk-navbar uk-margin-remove">

				<?php if ($this['widgets']->count('menu')) : ?>
				<?php echo $this['widgets']->render('menu'); ?>
				<?php endif; ?>

				<?php if ($this['widgets']->count('offcanvas')) : ?>
				<a href="#offcanvas" class="uk-navbar-toggle uk-visible-small uk-width-1-1 uk-text-center" data-uk-offcanvas>Hoofdmenu</a>
				<?php endif; ?>

				<?php if ($this['widgets']->count('search')) : ?>
				<div class="uk-navbar-flip">
					<div class="uk-navbar-content uk-hidden-small"><?php echo $this['widgets']->render('search'); ?></div>
				</div>
				<?php endif; ?>

			</nav>
			<?php endif; ?>
			
		</div>
		
	</div>
	<?php endif; ?>
	
	
	<?php if ($this['widgets']->count('main-top + main-bottom + sidebar-a + sidebar-b') || $this['config']->get('system_output', true)) : ?>
	<div class="pr-block-main">
	
		<div class="uk-container uk-container-center">

			<?php if ($this['widgets']->count('main-top + main-bottom + sidebar-a + sidebar-b') || $this['config']->get('system_output', true)) : ?>
			<div id="tm-middle" class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>
			
				<?php if ($this['widgets']->count('main-top + main-bottom') || $this['config']->get('system_output', true)) : ?>
				<div class="<?php echo $columns['main']['class'] ?>">

					<?php if ($this['widgets']->count('main-top')) : ?>
					<section id="tm-main-top" class="<?php echo $grid_classes['main-top']; echo $display_classes['main-top']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('main-top', array('layout'=>$this['config']->get('grid.main-top.layout'))); ?></section>
					<?php endif; ?>

					<?php if ($this['config']->get('system_output', true)) : ?>
					<main id="tm-content" class="tm-content">
		
							<?php if ($this['widgets']->count('breadcrumbs')) : ?>
							<?php echo $this['widgets']->render('breadcrumbs'); ?>
							<?php endif; ?>
		
							<?php echo $this['template']->render('content'); ?>
					</main>
					<?php endif; ?>

					<?php if ($this['widgets']->count('main-bottom')) : ?>
					<section id="tm-main-bottom" class="<?php echo $grid_classes['main-bottom']; echo $display_classes['main-bottom']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('main-bottom', array('layout'=>$this['config']->get('grid.main-bottom.layout'))); ?></section>
					<?php endif; ?>

				</div>
				<?php endif; ?>

				<?php foreach($columns as $name => &$column) : ?>
				<?php if ($name != 'main' && $this['widgets']->count($name)) : ?>
				<aside class="<?php echo $column['class'] ?>"><?php echo $this['widgets']->render($name) ?></aside>
				<?php endif ?>
				<?php endforeach ?>
			</div>
			<?php endif; ?>
			
		</div>
		
	</div>
	<?php endif; ?>
	
	
	<?php if ($this['widgets']->count('footer')) : ?>
	<div class="pr-block-footer">

			<footer id="tm-footer" class="tm-footer">
			
					<?php echo $this['widgets']->render('footer'); ?>
	
			</footer>
		
	</div>
	<?php endif; ?>



	<?php echo $this->render('footer'); ?>

	<?php if ($this['widgets']->count('offcanvas')) : ?>
	<div id="offcanvas" class="uk-offcanvas">
		<div class="uk-offcanvas-bar"><?php echo $this['widgets']->render('offcanvas'); ?></div>
	</div>
	<?php endif; ?>

</body>
</html>