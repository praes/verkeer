<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
?>

<div class="uk-width-medium-2-5 uk-align-center pr-login-form">

	<div class="pr-background-white-transparent pr-mod-spacing-large">
	
		<form action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post" class="uk-form">
		
			<div class="uk-width-1-1 uk-margin-large-bottom uk-text-center">
			
				<h1 class="pr-font-black pr-title-primary-border uk-display-inline-block uk-margin-remove">Inloggen</h1>
			
			</div>
			
			<div class="uk-width-1-1 uk-margin-bottom">
			
				<input class="uk-width-1-1" type="text" name="username" size="18" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_USERNAME') ?>">
			
			</div>
			
			<div class="uk-width-1-1 uk-margin-bottom">
			
				<input class="uk-width-1-1" type="password" name="password" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>">

			</div>
				
			<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
			
				<div class="uk-width-1-1 uk-margin-bottom">
			
					<input id="remember" type="checkbox" name="remember" class="inputbox uk-margin-small-right" value="yes"/><label><?php echo JText::_('COM_USERS_LOGIN_REMEMBER_ME') ?></label>
				
				</div>
				
			<?php endif; ?>

			<div class="uk-width-1-1 uk-margin-large-bottom">
			
				<button type="submit" class="uk-button uk-button-primary uk-width-1-1">
					<?php echo JText::_('JLOGIN'); ?>
				</button>
				
			</div>
			
			<div class="uk-width-1-1">
			
				<a  class="pr-font-black uk-width-1-1 uk-display-block" href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>"><?php echo JText::_('COM_USERS_LOGIN_RESET'); ?></a>
					
				<a class="pr-font-black  uk-width-1-1 uk-display-block" href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>"><?php echo JText::_('COM_USERS_LOGIN_REMIND'); ?></a>
			
			</div>

			<input type="hidden" name="return" value="<?php echo base64_encode($this->params->get('login_redirect_url', $this->form->getValue('return'))); ?>" />
			<?php echo JHtml::_('form.token'); ?>

		</form>

	</div>
	
</div>