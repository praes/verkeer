<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');
JHtml::_('formbehavior.chosen', 'select');

// Load user_profile plugin language
$lang = JFactory::getLanguage();
$lang->load('plg_user_profile', JPATH_ADMINISTRATOR);

//get user
$user = JFactory::getUser();

// Get the form fieldsets.
$fieldsets = $this->form->getFieldsets();

?>

<?php //check if password is still the default. If not redirect the user. ?>

<?php if($user->password !=  '$2y$10$ouJWEBwFJfFoXYfvghHN/O9aLHpBrm0bBIRau4Z19uEEMkHNEtwjC') : ?>

	<?php
	
		$db = JFactory::getDbo();
		
		$query = $db->getQuery(true);
		
		//select the __user_usergroup_map table to see if it's a teacher or student
		$query->select("*");
		$query->from($db->quoteName('#__user_usergroup_map'));
		$query->where("user_id =" . $user->id);
		
		$db->setQuery($query);
		
		$results = $db->loadObjectList();
		
		foreach($results as $result){
		
			//if it's a student
			if($result->group_id == "15"){
				
				header("Location: index.php?option=com_guru&view=guruorders&layout=mycourses&Itemid=282");
				die();
			
			}
			
			//if it's a teacher
			if($result->group_id == "14"){
			
				header("Location: index.php?option=com_guru&view=guruauthor&layout=authormycourses&Itemid=280");
				die();
	
			}
		
		}
		
	?>

<?php endif; ?>

<?php //check if password is still the default. If so, force a password change ?>

<?php if($user->password ===  '$2y$10$ouJWEBwFJfFoXYfvghHN/O9aLHpBrm0bBIRau4Z19uEEMkHNEtwjC') : ?>

<div class="uk-width-medium-2-5 uk-align-center">

	<div class="pr-mod-spacing-large pr-background-white-transparent">

			<h1 class="pr-font-black pr-title-primary-border uk-display-inline-block uk-margin-remove">Hallo <?php echo $user->name; ?></h1>
			
			<p class="uk-margin-large-bottom"><?php echo JText::_("PRAES_PASSWORD_CHANGE"); ?></p>


		<div class="profile-edit<?php echo $this->pageclass_sfx?>">


			<script type="text/javascript">
				Joomla.twoFactorMethodChange = function(e)
				{
					var selectedPane = 'com_users_twofactor_' + jQuery('#jform_twofactor_method').val();

					jQuery.each(jQuery('#com_users_twofactor_forms_container>div'), function(i, el) {
						if (el.id != selectedPane)
						{
							jQuery('#' + el.id).hide(0);
						}
						else
						{
							jQuery('#' + el.id).show(0);
						}
					});
				}
			</script>

			<form id="member-profile" action="<?php echo JRoute::_('index.php?option=com_users&task=profile.save'); ?>" method="post" class="uk-form" enctype="multipart/form-data">
			<?php // Iterate through the form fieldsets and display each one. ?>
			<?php foreach ($this->form->getFieldsets() as $group => $fieldset) : ?>
				<?php $fields = $this->form->getFieldset($group); ?>
				<?php if (count($fields)) : ?>
				<fieldset>

					<?php // Iterate through the fields in the set and display them. ?>
					<?php foreach ($fields as $field) : ?>
					<?php // If the field is hidden, just display the input. ?>
						<?php if ($field->hidden) : ?>
							<?php echo $field->input; ?>
						<?php else : ?>
							<div class="control-group">

								<div class="controls">
									<?php if ($field->fieldname == 'password1') : ?>
										<?php // Disables autocomplete ?> <input type="text" style="display:none">
									<?php endif; ?>
									<?php echo $field->input; ?>
								</div>
							</div>
						<?php endif;?>
					<?php endforeach;?>
				</fieldset>
				<?php endif;?>
			<?php endforeach;?>

			<div class="uk-margin-top">
			
			<button type="submit" class="uk-button uk-button-primary uk-width-1-1 validate"><span><?php echo JText::_('PRAES_SUBMIT_PASSWORD'); ?></span></button>
			
			</div>
		
			<input type="hidden" name="option" value="com_users" />
			<input type="hidden" name="task" value="profile.save" />

			<?php echo JHtml::_('form.token'); ?>
			
			</form>
		</div>

	</div>
	
</div>

<?php endif;?>


