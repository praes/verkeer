<?php

/*------------------------------------------------------------------------
# com_guru
# ------------------------------------------------------------------------
# author    iJoomla
# copyright Copyright (C) 2013 ijoomla.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.ijoomla.com
# Technical Support:  Forum - http://www.ijoomla.com/forum/index/
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
JHTML::_('behavior.tooltip');
$div_menu = $this->authorGuruMenuBar();

$id = JRequest::getVar("id", "0");
$course_name = $this->course_name;
$total_students = $this->total_students;
$student_complete = $this->student_complete;
$quizzes = $this->quizzes;
$score = $this->score;
$final_exam = $this->final_exam;
$array = $this->pass;
$pass = @$array["percent"];
$total_pass = @$array["total"];

?>

<script type="text/javascript" language="javascript">
	document.body.className = document.body.className.replace("modal", "");
</script>

<div class="pr-mod-spacing-large pr-background-white-transparent">
    
    <h1 class="pr-font-black pr-title-primary-border uk-display-inline-block"><?php echo JText::_('GURU_STATS_REPORT').' "'.$course_name.'"'; ?></h1>
    
    <div id="g_course_stats" class="g_sect clearfix">
        <form action="index.php" class="form-horizontal" id="adminForm" method="post" name="adminForm" enctype="multipart/form-data">
            <table class="uk-table uk-table-striped">
                <tr>
                    <th class="g_cell_1 uk-text-center">Leerlingen</th>
                    <th class="g_cell_2 uk-text-center">Aantal voltooid</th>
                    <th class="g_cell_3 uk-text-center">Aantal niet votooid</th>
                    <th class="g_cell_4 uk-text-center">Aantal opdrachten</th>
                </tr>
                
                <tr class="guru_row">
                    <td class="g_cell_1 uk-text-center">
                        <a href="<?php echo JRoute::_("index.php?option=com_guru&view=guruauthor&task=mystudents&layout=mystudents&cid=".intval($id)); ?>">
                            <?php echo $total_students; ?>
                        </a>
                    </td>
                    <td class="g_cell_2 uk-text-center">
                        <?php
                            if($student_complete == 0){
                                echo "0";
                            }
                            else{
                        ?>
                                <a href="<?php echo JRoute::_("index.php?option=com_guru&view=guruauthor&task=mystudents&layout=mystudents&cid=".intval($id)."&action=complete"); ?>">
                                    <?php echo $student_complete; ?>
                                </a>
                        <?php
                            }
                            $percent = 0;
                            if(intval($total_students) != 0){
                                $percent = ($student_complete * 100) / $total_students;
                            }
                            $percent = number_format($percent, 2, '.', '');
                            //echo " (".$percent."%)";
                        ?>
                    </td>
                    <td class="g_cell_3 uk-text-center">
                        <?php
                            $not_complete = intval($total_students - $student_complete);
                            if($not_complete < 0 ){
                                $not_complete = 0;
                            }
                        ?>
                        <a href="<?php echo JRoute::_("index.php?option=com_guru&view=guruauthor&task=mystudents&layout=mystudents&cid=".intval($id)."&action=notcomplete"); ?>">
                            <?php echo $not_complete; ?>
                        </a>
                        <?php
                            $percent = 0;
                            if(intval($total_students) != 0){
                                $percent = ($not_complete * 100) / $total_students;
                            }
                            $percent = number_format($percent, 2, '.', '');
                            //echo " (".$percent."%)";
                        ?>
                    </td>
                    <td class="g_cell_4 uk-text-center">
                        <?php
                            if(intval($quizzes) == 0){
                                echo "0";
                            }
                            else{
                        ?>
                                
                                    <?php echo $quizzes; ?>
                               
                        <?php
                            }
                            /*$score = number_format($score, 2, '.', '');
                            echo " / ";
                            echo "(".$score."%)";*/
                        ?>
                    </td>

                  </tr>
            </table>
            <input type="hidden" name="task" value="<?php echo JRequest::getVar("task", ""); ?>" />
            <input type="hidden" name="option" value="com_guru" />
            <input type="hidden" name="controller" value="guruAuthor" />
        </form>
   </div>  
</div>               