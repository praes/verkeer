<?php

/*------------------------------------------------------------------------
# com_guru
# ------------------------------------------------------------------------
# author    iJoomla
# copyright Copyright (C) 2013 ijoomla.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.ijoomla.com
# Technical Support:  Forum - http://www.ijoomla.com/forum/index/
-------------------------------------------------------------------------*/
defined( '_JEXEC' ) or die( 'Restricted access' );
JHTML::_('behavior.tooltip');
$div_menu = $this->authorGuruMenuBar();

$details = $this->details;
$userid = JRequest::getVar("userid", "0");
$user_name = $this->userName($userid);
$user_email = $this->userEmail($userid);
$itemid = JRequest::getVar("Itemid", "0");
$doc = JFactory::getDocument();
$image = $this->userImage($userid);

?>

<script type="text/javascript" language="javascript">
	document.body.className = document.body.className.replace("modal", "");
</script>

<div id="g_myquizzesstats" class="gru-myquizzesstats">
    <?php echo $div_menu; //MENU TOP OF AUTHORS ?>
	
    
    <form  action="index.php" class="form-horizontal" id="adminForm" method="post" name="adminForm" enctype="multipart/form-data">
            <h4 class="uk-margin-large-bottom">
                <?php
                    if(trim($image) == ""){
                        $grav_url = "http://www.gravatar.com/avatar/".md5(strtolower(trim($user_email)))."?d=mm&s=40";
                        echo '<img src="'.$grav_url.'" alt="'.$user_name.'" title="'.$user_name.'"/>&nbsp;';
                    }
                    else{
                        echo '<img src="'.JURI::root().trim($image).'" style="width:40px;" alt="'.$user_name.'" title="'.$user_name.'" />&nbsp;';
                    }
                    echo $user_name;
                ?>
            </h4>
            <table class="uk-table uk-table-striped">
                <tr class="pr-background-primary pr-font-white">
                    <th class="g_cell_1"><?php echo JText::_("GURU_COURSE_NAME"); ?></th>
                    <th class="g_cell_6">Voortgang</th>
                    <th class="g_cell_3 hidden-phone">Opdrachten</th>
                    <th class="g_cell_4">Gemiddelde score</th>
                    <th class="g_cell_2 hidden-phone uk-text-center">Voltooid</th>
<!--                     <th class="g_cell_6">Voortgang</th> -->
                </tr>
                <?php
                    if(isset($details) && count($details) > 0){
                        foreach($details as $key=>$detail){
							
							
							
							//var_dump($detail);
							
                ?>
                            <tr class="guru_row">
                                <td class="g_cell_1">
                                    <a target="_blank" class="pr-font-primary" href="<?php echo JRoute::_("index.php?option=com_guru&view=guruPrograms&task=view&cid=".intval($detail["course_id"])."&Itemid=".$itemid); ?>">
                                    <?php echo $detail["name"]; ?></a>
                                </td>
								<td class="g_cell_4">
                                    <?php
									
									
									//var_dump($this->getAllLessons($detail["course_id"]));
									
                                        $all_lessons = $this->getAllLessons($detail["course_id"]);
                                        $viewed_lessons = $this->getAllViewedLessons($detail["course_id"], $userid);
                                        $todo_lessons = $all_lessons - $viewed_lessons;
                                        if($all_lessons != 0 && $viewed_lessons != 0){
                                            //echo '<a class="pr-font-primary" href="'.JURI::root().'index.php?option=com_guru&view=guruauthor&layout=studentdetailslesson&userid='.intval($userid).'&pid='.intval($detail["course_id"]).'&tmpl=component">'.$viewed_lessons.' les(sen) gedaan</a>';
                                            echo '<span>'.$viewed_lessons.' les(sen) bekeken</span>';
                                        }
                                        else{
                                            echo '0 van 0';
                                        }
                                    ?>
                                    <div class="uk-text-small">Nog te doen: <?php echo $todo_lessons; ?> lessen</div>
                                </td>
                                <td class="g_cell_3 hidden-phone">
                                    <div>
	                                    <?php 
	                                    	echo '<a class="pr-font-primary" href="'.JURI::root().'index.php?option=com_guru&view=guruauthor&task=studentquizes&layout=studentquizes&pid='.intval($detail["course_id"]).'&userid='.intval($userid).'&tmpl=component">'.$detail["taken"].' opdracht(en) gedaan</a>';
	                                    ?>
	                                </div>
                                    <div class="uk-text-small">Nog te doen: <?php echo $detail["quizes"] - $detail["taken"]; ?> opdrachten</div>
                                </td>
                                <td class="g_cell_4">
	                                <div>
                                    <?php
                                        if(isset($detail["taken_percent"])){
                                            echo " score behaald:" . $detail["taken_percent"] / 10;
                                        }
                                        else{
                                            echo "Score behaald: 0";
                                        }
                                    ?>
	                                </div>
                                    <div class="uk-text-small">Benodigd om te slagen: <?php echo $detail["avg"] / 10; ?></div>
                                </td>
                                <td class="g_cell_2 hidden-phone uk-text-center">
                                    <?php
										if($detail["completed"] == "1" and ($detail["quizes"] - $detail["taken"] == 0) and ( $detail["taken_percent"] >= $detail["avg"] )){
                                            //echo JText::_("JYES");
                                            echo '<i class="uk-icon-check uk-text-success"></i>';
                                        }
                                        else{
                                            //echo JText::_("JNO");
                                            echo '<i class="uk-icon-close uk-text-danger"></i>';
                                        }
                                    ?>
                                </td>

<!--
                                <td class="g_cell_4 text-centered">
                                    <input type="button" class="uk-button uk-button-success" value="<?php echo JText::_("GURU_RESULT"); ?>" onclick="window.location='<?php echo JURI::root()."index.php?option=com_guru&view=guruauthor&task=studentquizes&layout=studentquizes&pid=".intval($detail["course_id"])."&userid=".intval($userid)."&tmpl=component"; ?>'" />
                                </td>
-->
                            </tr>
                <?php
                        }
                    }
                ?>
        </table>
        
        <input type="hidden" name="task" value="<?php echo JRequest::getVar("task", ""); ?>" />
        <input type="hidden" name="option" value="com_guru" />
        <input type="hidden" name="controller" value="guruAuthor" />
    </form>
</div>                 