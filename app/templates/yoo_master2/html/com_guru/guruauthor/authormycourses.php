<?php

/*------------------------------------------------------------------------
# com_guru
# ------------------------------------------------------------------------
# author    iJoomla
# copyright Copyright (C) 2013 ijoomla.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.ijoomla.com
# Technical Support:  Forum - http://www.ijoomla.com/forum/index/
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
JHTML::_('behavior.tooltip');
$db = JFactory::getDBO();
$div_menu = $this->authorGuruMenuBar();
$my_courses = $this->mycoursesth;
$config = $this->config;
$allow_teacher_action = json_decode($config->st_authorpage);//take all the allowed action from administator settings
@$teacher_add_courses = $allow_teacher_action->teacher_add_courses; //allow or not action Add courses
@$teacher_edit_courses = $allow_teacher_action->teacher_edit_courses; //allow or not action Add courses
$Itemid = JRequest::getVar("Itemid", "0");
$doc = JFactory::getDocument();
$doc->setTitle(trim(JText::_('GURU_AUTHOR'))." ".trim(JText::_('GURU_AUTHOR_MY_COURSE')));

?>



<script type="text/javascript" language="javascript">
	document.body.className = document.body.className.replace("modal", "");
</script>

<script language="javascript" type="application/javascript">
	function deleteAuthorCourse(){
		if (document.adminForm['boxchecked'].value == 0) {
			alert( "<?php echo JText::_("GURU_MAKE_SELECTION_FIRT");?>" );
		} 
		else{
			if(confirm("<?php echo JText::_("GURU_REMOVE_AUTHOR_COURSES"); ?>")){
				document.adminForm.task.value='removeCourse';
				document.adminForm.submit();
			}
		}	
	}
	
	function newAuthorCourse(){
		document.adminForm.task.value='addCourse';
		document.adminForm.action = '<?php echo JRoute::_("index.php?option=com_guru&view=guruauthor&task=addCourse&layout=addCourse&Itemid=".intval($Itemid)); ?>';
		document.adminForm.submit();	
	}
	
	function duplicateCourse(){
		if (document.adminForm['boxchecked'].value == 0) {
			alert( "<?php echo JText::_("GURU_MAKE_SELECTION_FIRT");?>" );
		} 
		else{
			document.adminForm.task.value='duplicateCourse';
			document.adminForm.submit();
		}	
	}
	
	function unpublishCourse(){
		if (document.adminForm['boxchecked'].value == 0) {
			alert( "<?php echo JText::_("GURU_MAKE_SELECTION_FIRT");?>" );
		} 
		else{
			document.adminForm.task.value='unpublishCourse';
			document.adminForm.submit();
		}	
	}
	
	function publishCourse(){
		if (document.adminForm['boxchecked'].value == 0) {
			alert( "<?php echo JText::_("GURU_MAKE_SELECTION_FIRT");?>" );
		} 
		else{
			document.adminForm.task.value='publishCourse';
			document.adminForm.submit();
		}	
	}
</script>	



<div class="gru-mycoursesauthor pr-mod-spacing-large pr-background-white-transparent">
    
    <h1 class="pr-font-black pr-title-primary-border uk-display-inline-block">Lesbrieven</h1>
    
    <div id="g_mycoursesauthorcontent" class="g_sect clearfix">
        <form  action="index.php" class="form-horizontal" id="adminForm" method="post" name="adminForm" enctype="multipart/form-data">

			<div class="clearfix"></div>
            <div class="g_table_wrap">
                <table id="g_authorcourse" class="uk-table uk-table-striped">
                    <tr>
                        <th class=""><?php echo JText::_('GURU_TI_VIEW_COURSE'); ?></th>
                        <th class=""><?php echo JText::_("GURU_STATS"); ?></th>
						<th class=""><?php echo JText::_("GURU_T_MY_STUDENTS"); ?></th>
                    </tr>
    
        
                    <?php 
                    $n =  count($my_courses);
                    for ($i = 0; $i < $n; $i++):
                        $id = $my_courses[$i]->id;
                        $checked = JHTML::_('grid.id', $i, $id);
                        //$published = JHTML::_('grid.published', $my_courses[$i], $i );
                        $alias = isset($my_courses[$i]->alias) ? trim($my_courses[$i]->alias) : JFilterOutput::stringURLSafe($my_courses[$i]->course_name);
                    ?>
					
						
                        <tr class="guru_row">	
                            <td class=""><a href="<?php echo JRoute::_("index.php?option=com_guru&view=guruPrograms&task=view&cid=".$id."-".$alias."&Itemid=".$Itemid); ?>"><?php echo $my_courses[$i]->name; ?></a></td>
                            
							
                            <?php if ($my_courses[$i]->lessontype == 2 ) :  ?>
							
								<td>          
									<a href="index.php?option=com_guru&view=guruauthor&task=course_stats&id=<?php echo intval($my_courses[$i]->id); ?>">Bekijk statistieken</a>
								</td>

								<td>
								
									<a href="index.php?option=com_guru&view=guruauthor&task=mystudents&layout=mystudents&Itemid=290">Mijn leerlingen</a>
								
								</td>
								
							<?php else :  ?>
							
							
								<td></td>
								
								<td></td>
							
							<?php endif; ?>
							

							
                       </tr>             
                    <?php
                        endfor;
                    ?>	
                </table>
        </div>
       
            
            <input type="hidden" name="task" value="<?php echo JRequest::getVar("task", "authormycourses"); ?>" />
            <input type="hidden" name="option" value="com_guru" />
            <input type="hidden" name="controller" value="guruAuthor" />
            <input type="hidden" name="boxchecked" value="" />
        </form>
   </div>  
</div>