<?php

/*------------------------------------------------------------------------
# com_guru
# ------------------------------------------------------------------------
# author    iJoomla
# copyright Copyright (C) 2013 ijoomla.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.ijoomla.com
# Technical Support:  Forum - http://www.ijoomla.com/forum/index/
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
JHTML::_('behavior.tooltip');
$db = JFactory::getDBO();
$div_menu = $this->authorGuruMenuBar();
$students = $this->students;
$doc = JFactory::getDocument();
$doc->setTitle(trim(JText::_('GURU_AUTHOR'))." ".trim(JText::_('GURU_AUTHOR_MY_STUDENTS')));
$isteacher = $this->isTeacherOrNot();
$config = $this->config;
$allow_teacher_action = json_decode($config->st_authorpage);//take all the allowed action from administator settings
$teacher_add_students = @$allow_teacher_action->teacher_add_students; //allow or not action Add students
@$from = JRequest::getVar("from", "");

$doc->addScript('components/com_guru/js/guru_modal.js');
$doc->addStyleSheet("components/com_guru/css/tabs.css");

?>
<script type="text/javascript" language="javascript">
	document.body.className = document.body.className.replace("modal", "");
</script>

<script type="text/javascript" language="javascript">
	function guruExport(type){
		course = document.getElementById('filter_course').value;
		url = '<?php echo JURI::root(); ?>'+'index.php?option=com_guru&controller=guruAuthor&task=export_'+type+'&course='+course+'&tmpl=component&format=raw';
		var win = window.open(url, '_blank');
		win.focus();
	}
</script>

<div class="pr-mod-spacing-large pr-background-white-transparent">
    <div class="uk-panel">
	    <h1 class="pr-font-black pr-title-primary-border uk-display-inline-block"><?php echo JText::_('GURU_AUTHOR_MY_STUDENTS');?></h1>
	  	<a href="<?php echo JRoute::_("index.php?option=com_guru&view=guruauthor&layout=authoraddstudent&Itemid=289"); ?>" class="uk-button uk-button-mini uk-button-primary uk-float-right uk-margin-top">
		  	Leerling(en) toevoegen
	  	</a>
    </div>
    
    <?php 
    if($isteacher >0){
    ?>
        <div id="g_mystudentsauthor" class="g_sect clearfix">
            <form class="form-horizontal" id="adminForm" method="post" name="adminForm" enctype="multipart/form-data" action="index.php">
                
                <?php /* ?>
                 <div class="gru-page-filters uk-margin-large-bottom">
					<div class="gru-filter-item uk-margin-right">
                        <input type="text" class="form-control" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" />
                        <button class="uk-button-primary" type="submit"><?php echo JText::_("GURU_SEARCH"); ?></button>
					</div>
					
                    <div class="gru-filter-item uk-margin-right"> 
                        <?php
                            $filter_course = $this->escape($this->state->get('filter.course'));
                            $my_courses = $this->getMyCourses();
                        ?>
                        <select name="filter_course" id="filter_course" onchange="document.adminForm.submit();">
                            <option value="0"> <?php echo JText::_("GURU_SELECT_COURSE"); ?> </option>
                            <?php
                                if(isset($my_courses) && count($my_courses) > 0){
                                    foreach($my_courses as $key=>$value){
                                        if($value["published"] == 0){
                                            continue;
                                        }
                                        $selected = "";
                                        if($value["id"] == $filter_course){
                                            $selected = 'selected="selected"';
                                        }
                                        echo '<option value="'.$value["id"].'" '.$selected.'>'.$value["name"].'</option>';
                                    }
                                }
                            ?>
                        </select>
					</div>
                    
                    <div class="gru-filter-item">
                        <a href="#" onclick="javascript:guruExport('pdf'); return false;" title="<?php echo JText::_("GURU_EXPORT_PDF"); ?>">
                            <img src="<?php echo JURI::root(); ?>components/com_guru/images/pdf.png">
                        </a>
                        
                        <a href="#" onclick="javascript:guruExport('csv'); return false;" title="<?php echo JText::_("GURU_EXPORT_CSV"); ?>">
                            <img src="<?php echo JURI::root(); ?>components/com_guru/images/excel.png">
                        </a>
                   </div> 
                </div>
*/ ?>
                
                <!-- /input-group -->
                <!-- End Search -->
                <div class="clearfix"></div>
                <div class="g_table_wrap g_margin_top">
                    <table id="g_authorstudent" class="uk-table uk-table-striped">
                        <tr>
                            <th></th>
                            <th class=""><?php echo JText::_('GURU_FULL_NAME'); ?></th>
                            <th class="">E-mailadres</th>
                            <th class="">Klas</th>
                            <th class="uk-text-center">Statistieken</th>
<!--                             <th class="g_cell_6 hidden-phone uk-text-center"><?php echo JText::_("GURU_PROGRAM_PROGRAMS"); ?></th> -->
                        </tr>
                        <?php 
						
						
                        if(isset($students) && count($students) > 0 && $students !== FALSE){
                            $i = 0;
                            foreach($students as $key=>$student){
                                $id = $student->id;
                                $checked = JHTML::_('grid.id', $i, $id);
                                $itemid = JRequest::getVar("Itemid", "0");
                        ?>
                            <tr class="guru_row">
                                <td>
                                    <?php
                                        if(trim($student->image) == ""){
                                            $grav_url = "http://www.gravatar.com/avatar/".md5(strtolower(trim($student->email)))."?d=mm&s=40";
                                            echo '<img src="'.$grav_url.'" alt="'.$student->name.'" title="'.$student->name.'"/>';
                                        }
                                        else{
                                            echo '<img src="'.JURI::root().trim($student->image).'" style="width:40px;" />';
                                        }
                                    ?>
                                </td>
                                <td class=""><?php echo $student->name; ?></td>
                                <td class=""><?php echo $student->email;?></td>
                                <td class=""><?php echo $student->group;?></td>
                                <td class=" uk-text-center">
                                    <?php
                                        $link_modal = JURI::root()."index.php?option=com_guru&view=guruauthor&layout=studentdetails&userid=".intval($id)."&tmpl=component";
                                        $itemid = JRequest::getVar("Itemid", "0");
                                        $link_phone = JRoute::_("index.php?option=com_guru&view=guruauthor&layout=studentdetails&userid=".intval($id)."&Itemid=".intval($itemid));
                                    ?>
                                    <a class="hidden-phone" onclick="javascript:openMyModal(0, 0, '<?php echo $link_modal; ?>'); return false;" href="#">
                                        <i class="uk-icon-line-chart"></i>
                                    </a>
                                    
                                    <a class="uk-hidden-large uk-hidden-medium" href="<?php echo $link_phone; ?>">
                                        <i class="uk-icon-line-chart"></i>
                                    </a>
                                    
                                </td>
<!--
                                <td class="g_cell_6 hidden-phone uk-text-center">
                                    <?php
                                        $courses = $student->courses;
                                        $courses = explode("-", $courses);
                                        $courses = array_unique($courses);
                                        $sum = count($courses);
                                    
                                        $itemid = JRequest::getVar("Itemid", "0");
                                    ?>
                                        <a class="hidden-phone" onclick="javascript:openMyModal(0, 0, '<?php echo $link_modal; ?>'); return false;" href="#">
                                            <?php echo intval($sum); ?>
                                        </a>
                                        
                                        <a class="uk-hidden-large uk-hidden-medium" href="<?php echo $link_phone; ?>">
                                            <?php echo intval($sum); ?>
                                        </a>
                               </td>
-->
                           </tr>
                        <?php
                                $i ++;
                            }
                        }
                        ?>	
                       </table>
                </div>
               
               
                <input type="hidden" name="option" value="com_guru" />
                <input type="hidden" name="controller" value="guruAuthor" />
                <input type="hidden" name="view" value="guruauthor" />
                <input type="hidden" name="task" value="mystudents" />
                <input type="hidden" name="action" value="<?php echo JRequest::getVar("action", ""); ?>" />
                <input type="hidden" name="qid" value="<?php echo JRequest::getVar("qid", ""); ?>" />
                <input type="hidden" name="cid" value="<?php echo JRequest::getVar("cid", ""); ?>" />
            </form>
       </div> 
  <?php
  }
  else{
    ?>
    <div class="g_table_row">
        <div class="g_cell span1 g_table_cell">
            <div>
                <div>
                    <?php echo JText::_("GURU_ONLY_AUTHORS");?>
                </div>
            </div>
        </div>
   </div>     
  <?php  
  }
  ?>   
</div>