<?php
/*------------------------------------------------------------------------
# com_guru
# ------------------------------------------------------------------------
# author    iJoomla
# copyright Copyright (C) 2013 ijoomla.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.ijoomla.com
# Technical Support:  Forum - http://www.ijoomla.com.com/forum/index/
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
JHTML::_('behavior.modal');

	$document = JFactory::getDocument();


	$document->setTitle(trim(JText::_('GURU_MYCOURSES')));
	$document->setMetaData( 'viewport', 'width=device-width, initial-scale=1.0' );

	
	require_once(JPATH_SITE.DS."components".DS."com_guru".DS."helpers".DS."generate_display.php");
	require_once(JPATH_BASE . "/components/com_guru/helpers/Mobile_Detect.php");
	$guruModelguruOrder = new guruModelguruOrder();
	
	function get_time_difference($start, $end){
		$uts['start'] = $start;
		$uts['end'] = $end;
		if( $uts['start'] !== -1 && $uts['end'] !== -1){
			if($uts['end'] >= $uts['start']){
				$diff = $uts['end'] - $uts['start'];
				if($days=intval((floor($diff/86400)))){
					$diff = $diff % 86400;
				}
					
				if($hours=intval((floor($diff/3600)))){
					$diff = $diff % 3600;
				}	
				
				if($minutes=intval((floor($diff/60)))){
					$diff = $diff % 60;
				}	
				$diff = intval($diff);
				return( array('days'=>$days, 'hours'=>$hours, 'minutes'=>$minutes, 'seconds'=>$diff));
			}
			else{
				return false;
			}
		}
		return false;
	}
		
	$document = JFactory::getDocument();
	$document->addScript("components/com_guru/js/programs.js");
	$db = JFactory::getDBO();
	$my_courses = $this->my_courses;
	$Itemid = JRequest::getVar("Itemid", "0");
	$search = JRequest::getVar("search_course", "");
	$config = $this->getConfigSettings();
	
	
	
	$sql = "Select datetype FROM #__guru_config where id=1 ";
	$db->setQuery($sql);
	$format_date = $db->loadResult();
	

	$detect = new Mobile_Detect;
	$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');	
	if($deviceType !='phone'){
		$cname= JText::_("GURU_COURSES_DETAILS");
		$class_title = 'class="guruml20"';

	}
	else{
		$cname= JText::_("GURU_DAYS_NAME");
		$class_title = 'class="guruml0"';

	}
	
	?>
    
<script type="text/javascript" language="javascript">
	document.body.className = document.body.className.replace("modal", "");
</script> 
   
<script language="javascript">
	function showContentVideo(href){
		jQuery('#myModal .modal-body iframe').attr('src', href);
	}
	
	jQuery('#myModal').on('hide', function () {
		 jQuery('#myModal .modal-body iframe').attr('src', '');
	});
</script>
    <?php
	$return_url = base64_encode("index.php?option=com_guru&view=guruorders&layout=mycourses&Itemid=".intval(@$itemid));
	
	if($config->gurujomsocialprofilestudent == 1){
		$link = "index.php?option=com_community&view=profile&task=edit&Itemid=".$Itemid;
	}
	else{
		$link = "index.php?option=com_guru&view=guruProfile&task=edit&Itemid=".$Itemid;
	}
	
	
	
	include_once(JPATH_SITE.DS."components".DS."com_guru".DS."helpers".DS."helper.php");
	$helper = new guruHelper();
	
?>
<div class="pr-mod-spacing-large pr-background-white-transparent">


    <form action="index.php" name="adminForm" method="post" class="uk-margin-remove">
   
        <div class="tm-bg-white tm-padding">
        <h1 class="pr-font-black pr-title-primary-border uk-display-inline-block">Mijn opdrachten</h1>
        <table class="uk-table uk-table-striped tm-table-content">
            <tr>
                <th class="g_cell_1"><?php echo $cname; ?></th>
                <th class="g_cell_2 hidden-phone"><?php echo JText::_("GURU_COURSE_PROGRESS"); ?></th>
                <th class="g_cell_3 hidden-phone"><?php echo JText::_("GURU_LAST_VISIT"); ?></th>
                <th class="g_cell_5"></th>
            </tr>
            <style>
                  div.guru-content .btn_renew{
                    height:25px;!important; 
                  }
            </style>
            <?php
            $k = 0;
            $already_edited = array();
            
            foreach($my_courses as $key=>$course){
                $bool_expired = false;
                $jnow = JFactory::getDate();
                $date_current = $jnow->toSQL();									
                $int_current_date = strtotime($date_current);
                $no_renew = false;
                $course = (object)$course;
                
                $id = $course->course_id;
                $alias = isset($course->alias) ? trim($course->alias) : JFilterOutput::stringURLSafe($course->course_name);
                
                if(!in_array($id, $already_edited)){
                    $already_edited[] = $id;
        ?>
                <tr class="guru_row">	
                        <td class="guru_product_name g_cell_1"><a href="<?php echo JRoute::_("index.php?option=com_guru&view=guruPrograms&task=view&cid=".$id."-".$alias."&Itemid=".$Itemid); ?>"><?php echo $course->course_name; ?></a>
                        
                        </td>
                        <td class="g_cell_2 hidden-phone"> 
                            <?php
                                $user = JFactory::getUser();
                                $user_id = $user->id;
                                $completed_progress = $guruModelguruOrder->courseCompleted($user_id,$id);
                                $date_completed = $guruModelguruOrder->dateCourseCompleted($user_id, $id);
                                $date_completed = date("".$format_date."", strtotime($date_completed));
                        
                                $style_color = "";
                                if($completed_progress == true){
                                    if($deviceType !="phone"){
                                        $var_lang = JText::_('GURU_COMPLETED');
                                        $lesson_module_progress = $var_lang." ". "(".$date_completed.")" ;	
                                        $style_color = 'style="color:#669900"';
                                    }
                                    else{
                                        $var_lang = JText::_('GURU_COMPLETED');
                                        $lesson_module_progress = $var_lang;
                                    }
                                }
                                else{
                                    $lesson_module_progress = $guruModelguruOrder->getLastViewedLessandMod($user_id, $id);	
                                }
                                
                                if(isset($lesson_module_progress)){
                                     echo $lesson_module_progress; 
                                } 
                                else{
                                    echo "";
                                }	
                                
                            ?>								
                            </td>
                            <?php
                                $date_last_visit = $guruModelguruOrder->dateLastVisit($user_id, $id);
                                if($date_last_visit !="0000-00-00" && $date_last_visit !=NULL ){
                                    $date_last_visit = date("".$format_date."", strtotime($date_last_visit));
                                }
                                else{
                                    $date_last_visit = "";
                                }
                                $count_quizz_taken = $guruModelguruOrder->countQuizzTakenF($user_id, $id);
                                
                            ?>
                            <td class="g_cell_3 hidden-phone"><?php echo $date_last_visit;  ?></td>
                          
                        </tr>
                    
        <?php
                }
            }
        ?>
            </table>
        </div>
        
        <input type="hidden" name="option" value="com_guru" />
        <input type="hidden" name="controller" value="guruOrders" />
        <input type="hidden" name="task" value="mycourses" />
        <input type="hidden" name="order_id" value="" />
        <input type="hidden" name="course_id" value="" />
    </form>
</div>
