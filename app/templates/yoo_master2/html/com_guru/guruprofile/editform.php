<?php
/*------------------------------------------------------------------------
# com_guru
# ------------------------------------------------------------------------
# author    iJoomla
# copyright Copyright (C) 2013 ijoomla.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.ijoomla.com
# Technical Support:  Forum - http://www.ijoomla.com.com/forum/index/
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
$user = JFactory::getUser();

$user_id = "";
$user_username = "";
$user_email = "";
$firstname = "";
$lastname = "";
$company = "";
$returnpage = JRequest::getVar("returnpage", "");
$Itemid = JRequest::getVar("Itemid", "0");
$image = "";

$document = JFactory::getDocument();
$document->setTitle(trim(JText::_('GURU_MY_ACCOUNT')));

if(isset($user)){
	$user_id = $user->id;
	$user_username = $user->username;
	$user_email = $user->email;
	
	$customer_profile = $this->getCustomerProfile();
	
	if(isset($customer_profile) && count($customer_profile) > 0){
		$firstname = $customer_profile["0"]["firstname"];
		$lastname = $customer_profile["0"]["lastname"];
		$company = $customer_profile["0"]["company"];
		$insertion = $customer_profile["0"]["insertion"];
		$image = $customer_profile["0"]["image"];
	}
	else{
		$name = $user->name;
		$temp = explode(" ", $name);
		if(count($temp) == 1){
			$firstname = $name;
		}
		else{
			$firstname = $temp["0"];
			unset($temp["0"]);
			$lastname = implode(" ", $temp);
		}
	}
}

include_once(JPATH_SITE.DS."components".DS."com_guru".DS."helpers".DS."helper.php");
$helper = new guruHelper();
$div_menu = $helper->createStudentMenu();
$page_title_cart = $helper->createPageTitleAndCart();

?>

<script language="javascript" type="text/javascript">
	function validateForm(){
		var first_name = document.adminForm.firstname.value;
		var last_name = document.adminForm.lastname.value;
		if(first_name == ""){
			alert("First Name is mandatory!");
			return false;
		}
		else if(last_name == ""){
			alert("Last Name is mandatory!");
			return false;
		}		
        if(document.adminForm.password.value != document.adminForm.password_confirm.value){
			alert("<?php echo JText::_("DSCONFIRM_PASSWORD_MSG");?>");
            return false;
        }   				
		return true;
	}
	
	function deleteImage(){
		document.getElementById("view_imagelist23").src = "components/com_guru/images/blank.png";
		document.getElementById("image").value = "";
	}
</script>


<div class="pr-mod-spacing-large pr-background-white-transparent">

	<form onsubmit="return validateForm();" id="adminForm" name="adminForm" method="post" action="index.php" class="uk-form uk-form-horizontal">

		<div class="uk-grid">
		
			<div class="uk-width-medium-3-4">

				<h1 class="pr-font-black pr-title-primary-border uk-display-inline-block">Mijn profiel</h1>
			
			</div>
		
			<div class="uk-width-medium-1-4 uk-text-right uk-hidden-small">
					
				<div class="uk-width-1-1 uk-margin-top">
				
					<input type="submit" value="Opslaan" class="uk-button uk-button-primary">
				
				</div>
			
			</div>
			
		</div>
					
		<p class="uk-article-lead uk-margin-large-bottom"><?php echo JText::_("COM_GURU_PROFILE_INTRO"); ?></p>
	
	
		<div class="uk-grid">
		
			<div class="uk-width-medium-1-2">
			
				<h3 class="pr-font-black pr-title-primary-border">Persoonlijke informatie</h3>
			
				<div class="uk-grid uk-margin-bottom">
				
					<div class="uk-width-medium-1-4">
					
						<?php echo JText::_("GURU_FIRS_NAME");?>:
					
					</div>
					
					<div class="uk-width-medium-3-4">
					
						<input type="text" class="inputbox uk-width-1-1" size="30" id="firstname" name="firstname" value="<?php echo $firstname; ?>" readonly/>
						
					</div>
				
				</div>
				
				<div class="uk-grid uk-margin-bottom uk-margin-top-remove">
				
					<div class="uk-width-medium-1-4">
					
						<?php echo JText::_("GURU_INSERTION");?>:
					
					</div>
					
					<div class="uk-width-medium-3-4">
				
						<input type="text" class="inputbox uk-width-1-1" size="30" id="insertion" name="insertion" value="<?php echo $insertion; ?>" readonly/>
					
					</div>
					
				</div>
				
				<div class="uk-grid uk-margin-bottom uk-margin-top-remove">
				
					<div class="uk-width-medium-1-4">
					
						<?php echo JText::_("GURU_LAST_NAME");?>:
					
					</div>
					
					<div class="uk-width-medium-3-4">
				
						<input type="text" class="inputbox uk-width-1-1" size="30" id="lastname" name="lastname" value="<?php echo $lastname; ?>" readonly/>
					
					</div>
					
				</div>
				
				<div class="uk-grid uk-margin-bottom uk-margin-top-remove">
				
					<div class="uk-width-medium-1-4">
					
						<?php echo JText::_("GURU_COMPANY");?>:
					
					</div>
					
					<div class="uk-width-medium-3-4">
				
						<input type="text" class="inputbox uk-width-1-1" size="30" id="company" name="company" placeholder="School" value="<?php echo $company; ?>" readonly required/>
					
					</div>
				
				</div>
				
				<div class="uk-grid uk-margin-bottom uk-margin-top-remove">
				
					<div class="uk-width-medium-1-4">
					
							<?php echo JText::_("GURU_UPLOAD_IMAGE");?>:
					
					</div>
					
					<div class="uk-width-medium-3-4">
					
						<?php
						$config = $this->configs;
						
						$max_upload = (int)(ini_get('upload_max_filesize'));
						$max_post = (int)(ini_get('post_max_size'));
						$memory_limit = (int)(ini_get('memory_limit'));
						$upload_mb = min($max_upload, $max_post, $memory_limit);
						if($upload_mb == 0){
							$upload_mb = 10;
						}
						$upload_mb *= 1048576; //transform in bytes
						$doc = JFactory::getDocument();
						
						$config_author = json_decode($config["0"]["authorpage"]);
						$author_t_prop = $config_author->author_image_size_type == "0" ? "width" : "heigth";

						$doc->addScriptDeclaration('
							jQuery.noConflict();
							jQuery(function(){
								function createUploader(){
									var uploader = new qq.FileUploader({
										element: document.getElementById(\'fileUploader\'),
										action: \''.JURI::root().'index.php?option=com_guru&controller=guruLogin&tmpl=component&format=raw&task=upload_ajax_image\',
										params:{
											folder:\'customers\',
											mediaType:\'image\',
											size: '.$config_author->author_image_size.',
											type: \''.$author_t_prop.'\',
										},
										onSubmit: function(id,fileName){
											jQuery(\'.qq-upload-list li\').css(\'display\',\'none\');
										},
										onComplete: function(id,fileName,responseJSON){
											//alert(\'id: \'+ id + \'; filename:\' + fileName);
											if(responseJSON.success == true){						
												jQuery(\'.qq-upload-success\').append(\'- <span style="color:#387C44;"></span>\');
												if(responseJSON.locate) {
													jQuery(\'#view_imagelist23\').attr("src", \''.JURI::root().'\'+responseJSON.locate +"/"+ fileName+"?timestamp=" + new Date().getTime());
													jQuery(\'#image\').val("/"+responseJSON.locate +"/"+ fileName);
												}
											}
										},
										allowedExtensions: [\'jpg\', \'jpeg\', \'png\', \'gif\', \'JPG\', \'JPEG\', \'PNG\', \'GIF\'],
										sizeLimit: '.$upload_mb.',
										multiple: false,
										maxConnections: 1
									});           
								}
								createUploader();
							});
						');
						$doc->addScript('components/com_guru/js/fileuploader.js');
						$doc->addStyleSheet('components/com_guru/css/fileuploader.css');
						?>

						<?php if(!($image)){   ?>
						
							<div id="fileUploader" class="pr-file-upload uk-margin-bottom"></div>
							<input type="hidden" name="image" id="image" value="<?php echo $image; ?>" />
						
						<?php } ?>
						
						<?php
						if(isset($image) && $image != ""){ 	?>
						
							<div class="uk-grid">
							
									<div class="uk-width-medium-3-5">
								
									<div id="fileUploader" class="pr-file-upload uk-margin-bottom"></div>
									<input type="hidden" name="image" id="image" value="<?php echo $image; ?>" />
								
									<input type="button" class="uk-button uk-button-danger" value="<?php echo JText::_('Verwijderen'); ?>" onclick="return deleteImage();"/>
									<input type="hidden" value="<?php echo $image; ?>" name="img_name" id="img_name" />
									
								</div>
							
								<div class="uk-width-medium-2-5">

									<div id='authorImageSelected'>
										<img id="view_imagelist23" class="uk-margin-bottom" name="view_imagelist" src='<?php echo JURI::root().$image; ?>'/><br />
									</div>
								
								</div>
								
							</div>

						<?php 
							}
							else{
						?>		
										<div id='authorImageSelected'>
											<img id='view_imagelist23' name='view_imagelist' src="<?php echo JURI::root(); ?>components/com_guru/images/blank.png"/>
										</div>

						<?php
							}
						?>

					
					</div>
				
				</div>
			
			</div>
			
			<div class="uk-width-medium-1-2">
			
				<h3 class="pr-font-black pr-title-primary-border">Login informatie</h3>
				
				<div class="uk-grid uk-margin-bottom uk-margin-top-remove">
				
					<div class="uk-width-medium-1-3">
					
						<?php echo JText::_("GURU_PROFILE_USERNAME");?>:
						
					</div>
					
					<div class="uk-width-medium-2-3">
					
						<input type="text" class="inputbox uk-width-1-1" size="30" id="username" disabled="disabled" name="username"  value="<?php echo $user_username; ?>" />
					
					</div>
				
				</div>
				
				<div class="uk-grid uk-margin-bottom uk-margin-top-remove">
				
					<div class="uk-width-medium-1-3">
					
						<?php echo JText::_("GURU_EMAIL");?>:
					
					</div>
					
					<div class="uk-width-medium-2-3">
					
						<input type="text" class="inputbox uk-width-1-1" size="30" id="email" name="email" disabled="disabled" value="<?php echo $user_email; ?>"/>
					
					</div>
				
				</div>
				
				<div class="uk-grid uk-margin-bottom uk-margin-top-remove">
				
					<div class="uk-width-medium-1-3">
					
						<?php echo JText::_("GURU_PROFILE_REG_PSW");?>:
					
					</div>
					
					<div class="uk-width-medium-2-3">
					
						<input type="password" class="inputbox uk-width-1-1" size="30" id="password" name="password" />
					
					</div>
				
				</div>
				
				<div class="uk-grid uk-margin-bottom uk-margin-top-remove">
				
					<div class="uk-width-medium-1-3">
					
						<?php echo JText::_("GURU_PROFILE_REG_PSW2");?>
					
					</div>
					
					<div class="uk-width-medium-2-3">
					
						<input type="password" class="inputbox uk-width-1-1" size="30" id="password_confirm" name="password_confirm"/>
					
					</div>
				
				</div>
				
				<div class="uk-width-1-1 uk-visible-small uk-margin-top">
				
					<input type="submit" value="Opslaan" class="uk-button uk-button-primary">
				
				</div>
			
			</div>
			
		</div>

		
		<input type="hidden" value="0" name="Itemid" />
		<input type="hidden" value="com_guru" name="option" />
		<input type="hidden" value="<?php echo $user_id; ?>" name="id" />
		<input type="hidden" value="saveCustomer" name="task" />
		<input type="hidden" value="<?php echo $returnpage; ?>" name="returnpage" />
		<input type="hidden" value="guruProfile" name="controller" />
		<input type="hidden" value="<?php echo $user_username; ?>" name="username" />
		<input type="hidden" value="<?php echo $user_email; ?>" name="email" />
		</form>

	
</div>