<?php
/*------------------------------------------------------------------------
# com_guru
# ------------------------------------------------------------------------
# author    iJoomla
# copyright Copyright (C) 2013 ijoomla.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.ijoomla.com
# Technical Support:  Forum - http://www.ijoomla.com.com/forum/index/
-------------------------------------------------------------------------*/
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once JPATH_SITE . '/components/com_users/helpers/route.php';
JHtml::_('behavior.keepalive');

$lang = JFactory::getLanguage();
$extension = 'mod_login';
$base_dir = JPATH_SITE;
$language_tag = 'en-GB';
$lang->load($extension, $base_dir, '', true);

$Itemid = JRequest::getVar("Itemid", "0");
$document = JFactory::getDocument();
$document->addStyleSheet("components/com_guru/css/guru_style.css");
$document->setMetaData( 'viewport', 'width=device-width, initial-scale=1.0' );

require_once(JPATH_BASE . "/components/com_guru/helpers/Mobile_Detect.php");

$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');  
$document->setTitle(JText::_("GURU_ALREADY_MEMBER"));
$returnpageoR = JRequest::getVar("returnpage", "");

if($returnpageoR == 'authorprofile' || $returnpageoR == "authormymedia" || $returnpageoR == "authormymediacategories" || $returnpageoR == "mystudents" || $returnpageoR == "authormycourses"){
	$returnpageo = 'authorprofile';
}
else{
	$returnpageo = $returnpageoR;
}
?>

<div class="uk-width-medium-2-5 uk-align-center">

	<div class="pr-background-white-transparent pr-mod-spacing-large">
	
		<div class="uk-width-1-1 uk-margin-large-bottom uk-text-center">

			<h1 class="pr-font-black pr-title-primary-border uk-display-inline-block uk-margin-remove">Inloggen</h1>
			
		</div>

		<form name="loginForm" method="post" class="uk-form" action="index.php">
		
			<div class="uk-width-1-1 uk-margin-bottom">

				<input class="uk-width-1-1" type="text" id="username" name="username" placeholder="Gebruikersnaam" />

			</div>
			
			<div class="uk-width-1-1 uk-margin-bottom">
			
				<input class="uk-width-1-1" type="password" id="passwd" name="passwd" placeholder="Wachtwoord" />

			</div>
			
			<div class="uk-width-1-1 uk-margin-bottom">
			
				<input type="checkbox" name="remember" value="1" /> Onthoud mij
				
			</div>
			
			<div class="uk-width-1-1 uk-margin-large-bottom">
			
				<a onclick="document.loginForm.submit();" class="uk-button uk-button-primary uk-width-1-1" name="submit_button" value="Inloggen">Inloggen</a>
			
			</div>

			<a class="pr-font-black uk-text-small uk-display-block uk-width-1-1" href="<?php echo JRoute::_('index.php?option=com_users&view=remind&Itemid=' . UsersHelperRoute::getRemindRoute()); ?>">
			<?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_USERNAME'); ?></a>

			<a class="pr-font-black uk-text-small uk-display-block uk-width-1-1" href="<?php echo JRoute::_('index.php?option=com_users&view=reset&Itemid=' . UsersHelperRoute::getResetRoute()); ?>">
			<?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_PASSWORD'); ?></a>

			

			<input type="hidden" name="option" value="com_guru" />
			<input type="hidden" name="controller" value="guruLogin" />
			<input type="hidden" name="Itemid" value="<?php echo $Itemid;?>" />
			<input type="hidden" name="task" value="log_in_user" />
			<input type="hidden" name="returnpage" value="<?php echo JRequest::getVar("returnpage", ""); ?>" /> 
			<input type="hidden" name="cid" value="<?php echo JRequest::getVar("cid", "0"); ?>" />
			
		</form>
	  
	</div>

</div>